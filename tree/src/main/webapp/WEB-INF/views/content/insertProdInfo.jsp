<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>[상품메타 INSERT]</title>
<head>

    <script type="text/javascript" src="<%=webRoot %>/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery-form.js" ></script>
    <script type="text/javascript" src="<%=webRoot%>/js/ajaxupload.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery.validate.js" ></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/spin.min.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery.cookie.js"></script>
    <%
    //webRoot = "http://10.30.0.166:8080/tree";
    
    %>
    <script type="text/javascript">
        $(document).ready(function(){
        		    
        		    
<%--             //파일 업로드 테스트 
              $("#prodForm").ajaxForm({
                beforeSubmit : function(){
                },
                dataType : "json",
                url: '<%=webRoot%>/contents/InsertProdInfo.do',
                success : function(data){
                    var code = data.code;
                    var msg = data.msg;
                    
                    if(code == "0000"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                    }
                },
                error : function(jqXHR, textStatus, errorThrown){
                    alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                }    
            });    --%>

              //파일 업로드 테스트 
              $("#prodForm").ajaxForm({
                beforeSubmit : function(){
                },
                dataType : "json",
                <%-- url: '<%=webRoot%>/contents/InsertProdInfo.do', --%>
                url: '<%=webRoot%>/authoring/addProdCard.do',
                success : function(data){
                    var code = data.code;
                    var msg = data.msg;
                    
                    if(code == "0000"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                    }
                },
                error : function(jqXHR, textStatus, errorThrown){
                    alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                }    
            });  

            
        });


        // 로그인 
        function callLogin(){
        	var id = $("#userId").val();
        	var pwd = $("#pwd").val();
        	
        	var callUrl = "<%=webRoot %>/member/signInTree.do";
        	var data = {userId: id, userPwd : pwd, treeformat : "json"};
        	
        	callAjaxData('post',callUrl, data, "rstTag1");
        }
        
        // 상품메타 저장 처리 
        function callSave(){
        	$("#prodForm").submit();
        }


        // 카드 정보 조회
        function cardInfo() {
            var pid = $("#pid1").val();
            var mbrId = $("#mbrId1").val();
            
            var callUrl = "<%=webRoot %>/authoring/cardInfo.do";
            var data = {pid: pid, mbrId : mbrId, treeformat : "json"};
            
            callAjaxData('post',callUrl, data, "rstTag2");
        }

        // 북 리스트 조회
        function bookList() {
            var callUrl = "<%=webRoot %>/authoring/bookList.do";
            var data = {treeformat : "json"};
            
            callAjaxData('post',callUrl, data, "rstTag3");
        }

        // 레슨 리스트 조회
        function lessonList() {
            var bookCd = $("#bookCd4").val();
            
            var callUrl = "<%=webRoot %>/authoring/lessonList.do";
            var data = {bookCd: bookCd, treeformat : "json"};
            
            callAjaxData('post',callUrl, data, "rstTag4");
        }
        function callAjaxData(type, callUrl, pData, showId){
            
            $.ajax({            
                type : type,
                xhrFields: {
                    withCredentials: true
                 },
                crossDomain : true,
                url : callUrl,
                async : false,
                cache : false,
                headers: {        
                    Accept : "application/json"      
                },
                datatype : "json",
                data : pData,
                success : function(data){
                    $("#"+showId).text(JSON.stringify(data));
                },
                error: function (xhr, ajaxOptions, thrownError){
                       alert("code:"+xhr.status+"\n"+"message:"+xhr.responseText+"\n"+"error:"+thrownError);
                }, 
                complete:function (xhr, textStatus){
                    //
                }  
            });
            
        }
    </script>
</head>
    <style>
        body{
         padding-top:20px;
         padding-left:20px;
        }
        .green{
            color:green;
        }
        #txtState{
            font-weight:bold;
        }
        #ptmode.on{
            background-color: green;
        }
        #ptmode.off{
            background-color: red;
        }
    </style>
<body>
        <h1>*카드(액티비티) 정보 입력</h1>
        <!--  
        제이슨 데이터 확인!!
        <iframe src="http://jsonviewer.stack.hu/" style="height:300px;width:100%"></iframe>
    -->
      <a href="javascript: callLogin();"></a><p>
      <b>데이터를 입력하기전에 로그인 해주세요.</b>
      
      <form id="prodForm" name="prodForm" method="post">
      <select name="userId" id="userId">
	      <!-- <option value="vsadmin">vsadmin(관리자)</option> -->
	      <option value="tbs001">tbs001(선생님)</option>
	      <option value="tal001">tal001(선생님)</option>
	      <option value="tkl001">tkl001(선생님)</option>
	      <option value="bs001">bs001(학생)</option>
          <option value="al001">al001(학생)</option>
      </select>
            <!-- userId:<input type="text" name="userId" id="userId" value="t" /> -->
            <input type="hidden" name="pwd"  id="pwd" value="1234" />
            <input Type="button" value="로그인" onclick="javascript: callLogin();" />
            <p>
      데이터 결과 값: <b id="rstTag1"></b></p>
      <!-- <b>  *itemId는 <font color="red">10</font>, quesNo는  <font color="red">Step.C</font>로 고정값 입니다.</b></p> -->
    <hr/>
    
    <h2><font color="red">***카드 정보만 입력하는 용도로 사용합니다.</font></h2>
        <table>

        <tr>
            <td>코드 (pid - 키 값) </td>
            <td><input type="text" name="pid" id="pid" value=""/></td>
        </tr>
        <tr>
            <td>제목 (prodTitle)</td>
            <td><input type="text" name="prodTitle" id="prodTitle" value=""/></td>
        </tr>
        <tr>
            <td>썸네일 경로(thmbPath) </td>
            <td><input type="text" name="thmbPath" id="thmbPath" value=""/></td>
        </tr>
        <tr>
            <td>회원아이디(mbrId) : 여기에 입려한 아이디로 메타 데이터가 입력됩니다. </td>
            <td><input type="text" name="mbrId" id="mbrId" value=""/></td>
        </tr>
        <tr>
            <td>북코드(bookCd)</td>
            <td><input type="text" name="bookCd" id="bookCd" value=""/></td>
        </tr>
        <tr>
            <td>레슨코드(lessonCd)</td>
            <td><input type="text" name="lessonCd" id="lessonCd" value=""/></td>
        </tr>
        <tr>
            <td>카드타입 ( LECTURE / QUIZ / ACTIVITY ) (cardType)</td>
            <td><input type="text" name="cardType" id="cardType" value="LECTURE"/></td>
        </tr>
        <tr>
            <td>스킬 (cardSkill)</td>
            <td><input type="text" name="cardSkill" id="cardSkill" value="Speaking"/></td>
        </tr>
        <tr>
            <td>스터디 모드 ( LECTURE / QUIZ / ACTIVITY ) (studyMode)</td>
            <td><input type="text" name="studyMode" id="studyMode" value="lecturing"/></td>
        </tr>
        <tr>
            <td>카드레벨 (1~5) (cardLevel)</td>
            <td><input type="text" name="cardLevel" id="cardLevel" value="1"/></td>
        </tr>

        <tr>
            <td>시간 (time)</td>
            <td><input type="text" name="time" id="time" value="1"/></td>
        </tr>
        <tr>
            <td>수정 가능 여부 (editYn)</td>
            <td><input type="text" name="editYn" id="editYn" value="Y"/></td>
        </tr>
        <tr>
            <td>공유 가능 여부 (openYn)</td>
            <td><input type="text" name="openYn" id="openYn" value="Y"/></td>
        </tr>
        <tr>
            <td>키워드 (keyword)</td>
            <td><input type="text" name="keyword" id="keyword" value=""/></td>
        </tr>
        <tr>
            <td>grading(grading) </td>
            <td><input type="text" name="grading" id="grading" value=""/></td>
        </tr>
        <tr>
            <td>디렉션 언어(direcLang1) </td>
            <td><input type="text" name="direcLang1" id="direcLang1" value="Eng"/></td>
        </tr>
        <tr>
            <td>디렉션(direc1) </td>
            <td><input type="text" name="direc1" id="direc1" value="한국어"/></td>
        </tr>
        <tr>
            <td>디렉션 언어?(direcLang2) </td>
            <td><input type="text" name="direcLang2" id="direcLang2" value="Eng"/></td>
        </tr>
        <tr>
            <td>디렉션(direc2) </td>
            <td><input type="text" name="direc2" id="direc2" value="한국어"/></td>
        </tr>
        <tr>
            <td>실제 파일경로(filePath)</td>
            <td><input type="text" name="filePath" id="filePath" value="/Savefilepath"/></td>
        </tr>
        </table>





        
<!-- 		<table>
		<tr>
			<td>제목 (prodTitle)</td>
			<td><input type="text" name="prodTitle" id="prodTitle" value=""/></td>
		</tr>
		<tr>
		    <td>썸네일 경로(thmbPath) </td>
		    <td><input type="text" name="thmbPath" id="thmbPath" value=""/></td>
		</tr>
		<tr>
		    <td>회원아이디(memberId) : 여기에 입려한 아이디로 메타 데이터가 입력됩니다. </td>
		    <td><input type="text" name="memberId" id="memberId" value=""/></td>
		</tr>
		<tr>
		    <td>상세설명(dtlCntt)</td>
		    <td><input type="text" name="dtlCntt" id="dtlCntt" value=""/></td>
		</tr>
		<tr>
		    <td>북코드(bookCd)</td>
		    <td><input type="text" name="bookCd" id="bookCd" value=""/></td>
		</tr>
		<tr>
		    <td>레슨코드(lessonCd)</td>
		    <td><input type="text" name="lessonCd" id="lessonCd" value=""/></td>
		</tr>
		<tr>
		    <td>차시코드(daynoCd)</td>
		    <td><input type="text" name="daynoCd" id="daynoCd" value=""/></td>
		</tr>
		<tr>
		    <td>모듈코드(mdlCd)</td>
		    <td><input type="text" name="mdlCd" id="mdlCd" value=""/></td>
		</tr>
		<tr>
		    <td>카드코드(cardCd)</td>
		    <td><input type="text" name="cardCd" id="cardCd" value=""/></td>
		</tr>
		<tr>
		    <td>정렬순서(sort)</td>
		    <td><input type="text" name="sort" id="sort" value="1"/></td>
		</tr>
		<tr>
		    <td>상품구분자(레퍼런스 데이터만 'R' 로 들어간다.)(prodSect)</td>
		    <td><input type="text" name="prodSect" id="prodSect" value=""/></td>
		</tr> 
		<tr>
		    <td>상품타입 (상품유형( TXT / IMG / MOV / URL )_ 레퍼런스 컨텐츠에서 사용함) (prodType)</td>
		    <td><input type="text" name="prodType" id="prodType" value=""/></td>
		</tr>
		
		<tr>
		    <td>카드타입 ( LECTURE / QUIZ / ACTIVITY ) (cardType)</td>
		    <td><input type="text" name="cardType" id="cardType" value="LECTURE"/></td>
		</tr>
		<tr>
		    <td>카드레벨 (1~5) (cardLevel)</td>
		    <td><input type="text" name="cardLevel" id="cardLevel" value="1"/></td>
		</tr>
		<tr>
		    <td>카드 사용 용도 (강의용/개인학습용/그룹학습용) (useType)</td>
		    <td><input type="text" name="useType" id="useType" value="강의용"/></td>
		</tr>
		<tr>
		    <td>실제 파일경로(filePath)</td>
		    <td><input type="text" name="filePath" id="filePath" value="/Savefilepath"/></td>
		</tr>
		<tr>
		    <td>영어 디렉션(direcEng) </td>
		    <td><input type="text" name="direcEng" id="direcEng" value="Eng"/></td>
		</tr>
		<tr>
		    <td>한국어 디렉션(direcKor) </td>
		    <td><input type="text" name="direcKor" id="direcKor" value="한국어"/></td>
		</tr>
		</table> -->
</form>
<br/>
<input Type="button" value="SAVE" onclick="javascript: callSave();" />
    <hr/>
        <h1>*카드(액티비티) 정보 조회</h1>
        <table>
        <tr>
            <td>코드 (pid - 키 값) </td>
            <td><input type="text" name="pid1" id="pid1" value="1"/></td>
        </tr>
        <tr>
            <td>회원 ID (mbrId)</td>
            <td><input type="text" name="mbrId1" id="mbrId1" value="tkl001"/></td>
        </tr>
        <tr>
            <td colspan="2"><a href="javascript:cardInfo();">조회</a></td>
        </tr>
        </table>
        데이터 결과 값: <b id="rstTag2"></b></p>
    <hr/>
        <h1>북 리스트 조회</h1>
        <table>
        <tr>
            <td colspan="2"><a href="javascript:bookList();">북 리스트 조회</a></td>
        </tr>
        </table>
        데이터 결과 값: <b id="rstTag3"></b></p>
        <h1>레슨 리스트 조회</h1>
        <table>
        <tr>
            <td>북 코드 (bookCd)</td>
            <td><input type="text" name="bookCd4" id="bookCd4" value="108"/></td>
        </tr>
        <tr>
            <td colspan="2"><a href="javascript:lessonList();">레슨 리스트 조회</a></td>
        </tr>
        </table>
        데이터 결과 값: <b id="rstTag4"></b></p>
<br/>
<hr/>
</body>
</html>
