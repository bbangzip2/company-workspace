<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>서버데이터 확인</title>
<head>

    <script type="text/javascript" src="<%=webRoot %>/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery-form.js" ></script>
    <script type="text/javascript" src="<%=webRoot%>/js/ajaxupload.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery.validate.js" ></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/spin.min.js"></script>
    <script type="text/javascript" src="<%=webRoot %>/js/jquery.cookie.js"></script>
    <%
    //webRoot = "http://10.30.0.166:8080/tree";
    
    %>
    <style type="text/css">
    /*
    * 그냥 무지개 색깔이 넣고 싶었어요. 홍홍홍  class="rainbow" 만 넣어서 사용하세요.
    */
    .rainbow {
	  background-image: -webkit-gradient( linear, left top, right top, color-stop(0, #f22), color-stop(0.15, #f2f), color-stop(0.3, #22f), color-stop(0.45, #2ff), color-stop(0.6, #2f2),color-stop(0.75, #2f2), color-stop(0.9, #ff2), color-stop(1, #f22) );
	  background-image: gradient( linear, left top, right top, color-stop(0, #f22), color-stop(0.15, #f2f), color-stop(0.3, #22f), color-stop(0.45, #2ff), color-stop(0.6, #2f2),color-stop(0.75, #2f2), color-stop(0.9, #ff2), color-stop(1, #f22) );
	  color:transparent;
	  -webkit-background-clip: text;
	  background-clip: text;
	}
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
        		    
        		    
            //파일 업로드 테스트 
              $("#upForm").ajaxForm({
                beforeSubmit : function(){
                },
                dataType : "json",
                url: '<%=webRoot%>/upload/noteFileUpload.do',
                success : function(data){
                    var code = data.code;
                    var msg = data.msg;
                    
                    if(code == "0000"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                    }
                },
                error : function(jqXHR, textStatus, errorThrown){
                    alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                }    
            });   
            
            //파일 업로드 테스트 (파일만)
              $("#testForm").ajaxForm({
                beforeSubmit : function(){
                },
                dataType : "json",
                url: '<%=webRoot%>/upload/onlyFileUpload.do',
                success : function(data){
                    var code = data.code;
                    var msg = data.msg;
                    
                    if(code == "0000"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                    }
                },
                error : function(jqXHR, textStatus, errorThrown){
                    alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                }    
            });   
            
             
              
            
            
              
              // 포트 폴리오 저장 처리  
                $("#portForm").ajaxForm({
                  beforeSubmit : function(){
                  },
                  dataType : "json",
                  url: '<%=webRoot%>/upload/portFolioInsert.do',
                  success : function(data){
                      var code = data.code;
                      var msg = data.msg;
                      
                      if(code == "0000"){
                          alert("저장 되었습니다.");
                      }else{
                          alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                      }
                  },
                  error : function(jqXHR, textStatus, errorThrown){
                      alert("저장하지 못했습니다.\n잠시후 다시 이용해 주세요");
                  }    
              });   
              
              
            
        });


        // 로그인 
        function callLogin(){
        	var id = $("#userId").val();
        	var pwd = $("#pwd").val();
        	
        	var callUrl = "<%=webRoot %>/member/signInTree.do";
        	var data = {userId: id, userPwd : pwd, treeformat : "json"};
        	
        	callAjaxData('post',callUrl, data, "rstTag1");
        }
        
        function callLoginTest(){
            var id = $("#t_userId").val();
            var pwd = $("#t_pwd").val();
            
            var callUrl = "<%=webRoot %>/member/signInTreeTest.do";
            var data = {userId: id, userPwd : pwd, treeformat : "json"};
            
            callAjaxData('post',callUrl, data, "rstTag999");
        }
        
        
        // 사용자 정보 조회  
        function callMemberInfo(){
            var id = $("#if2_userId").val();
            
            var callUrl = "<%=webRoot %>/member/selectMemberInfo.do";
            var data = {userId: id, treeformat : "json"};

            callAjaxData('post',callUrl, data, "rstTag2");
        }

        // 런처T > 메인
        function getMain() {
            var todayYmd = $("#todayYmd1").val();
        	
        	var callUrl = "<%=webRoot %>/treeMain.do";
        	var data = {todayYmd : todayYmd, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getMain");
        }

        // 런처T > 메인 > Today's Classes (swipe)
        function getClasses() {
        	var todayYmd = $("#todayYmd2").val();

        	var callUrl = "<%=webRoot %>/treeMain/classes.do";
        	var data = {todayYmd:todayYmd, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getClasses");
        }

        // 런처T/S > 메인 > 컨텐츠 버전 체크 (다운로드 URL 요청)
        function getContents() {
            var lessonCd = $("#lessonCd").val();
            var version = $("#version").val();

            var callUrl = "<%=webRoot %>/contents/versionCheck.do";
            var data = {lessonCd : lessonCd, version : version, treeformat : "json"};

            callAjaxData('post',callUrl, data, "getContents");
        }

        // 해당 Lesson의 Day 리스트
        function getDayList() {
        	var lessonCd = $("#dayList_lessonCd").val();
        	var tmbrId = $("#dayList_tmbrId").val();

        	var callUrl = "<%=webRoot %>/classroom/dayList.do";

        	var data = {lessonCd : lessonCd, tmemberId  :  tmbrId};
        	callAjaxData('post',callUrl, data, "getDayList");
        }
        
        // 카드맵 리스트
        function getCardMapList() {
        	var mbrId = $("#cardMapList_mbrId").val();
        	var lessonCd = $("#cardMapList_lessonCd").val();
        	var dayNo = $("#cardMapList_dayNo").val();
        	var classSeq = $("#cardMapList_classSeq").val();
        	var classLocalIp = $("#cardMapList_classLocalIp").val();
            
        	var callUrl = "<%=webRoot %>/classroom/cardMapList.do";

        	var data = {memberId: mbrId, lessonCd : lessonCd, dayNo:dayNo, classSeq:classSeq, classLocalIp:classLocalIp};
        	callAjaxData('post',callUrl, data, "getCardMapList");
        }

        // 카드맵 서브 리스트
        function getCardMapSubList() {
            var mbrId = $("#cardMapList_mbrId1").val();
            var lessonCd = $("#cardMapList_lessonCd1").val();
            var dayNo = $("#cardMapList_dayNo1").val();

            var callUrl = "<%=webRoot %>/classroom/cardMapSubList.do";

            var data = {memberId: mbrId, lessonCd : lessonCd, dayNo:dayNo};
            callAjaxData('post',callUrl, data, "getCardMapSubList");
        }

        // 해당 카드 상세 데이터
        function getCardInfo() {
        	var prodSeq = $("#cardInfo_prodSeq").val();
        	var callUrl = "<%=webRoot %>/classroom/cardInfo.do";

        	var data = {prodSeq: prodSeq};
        	callAjaxData('post',callUrl, data, "getCardInfo");
        }

        function setCardSort() {
        	var memberId = $("#cardSort_memberId").val();
        	var dayCd = $("#cardSort_dayCd").val();
        	var sortData = $("#cardSort_sortData").val();
        	var callUrl = "<%=webRoot %>/classroom/cardSort.do";

        	var data = {memberId: memberId, dayCd: dayCd, sortData: sortData};
        	callAjaxData('post',callUrl, data, "setCardSort");
        }

        // 티칭룸/스터디룸
        function getClassroomList() {
        	var sortKey = $("#sortKey9").val();
        	var pageNo = $("#pageNo9").val();

        	var callUrl = "<%=webRoot %>/classroom/getClassroomList.do";

        	var data = {sortKey : sortKey, pageNo:pageNo, treeformat : "json"};
        	callAjaxData('post',callUrl, data, "getClassroomList");
        }

        // 포트폴리오 -- 강사 
        function getPortfolio(idx) {
        	var memberId = $("#memberId").val();
        	var clsSeq = $("#clsSeq").val();
        	var lessonCd = $("#portlessonCd").val();
        	var inclsRsltSeq = $("#inclsRsltSeq").val();
        	var sortKey = $("#sortKey").val();
        	var dayNo = $("#dayNo").val();

        	var callUrl;
        	var data;
        	
        	if (idx == '1') {
        		data = {clsSeq:clsSeq};
        		callUrl = "<%=webRoot %>/portfolio/portfolioLesson.do";
        	}
        	if (idx == '2') {
        		data = {clsSeq:clsSeq, lessonCd:lessonCd, inclsRsltSeq:inclsRsltSeq, sortKey:sortKey, memberId:memberId, dayNo:dayNo};
        		callUrl = "<%=webRoot %>/portfolio/portfolioDay.do";
        	}
        	if (idx == '3') {
        		data = {clsSeq:clsSeq, lessonCd:lessonCd, inclsRsltSeq:inclsRsltSeq, sortKey:sortKey, memberId:memberId, dayNo:dayNo};
        		callUrl = "<%=webRoot %>/portfolio/portfolioList.do";
        	}
        	if (idx == '4') {
        		data = {memberId:memberId};
        		callUrl = "<%=webRoot %>/portfolio/portfolioList.do";
        	}
        	if (idx == '5') {
        		data = {clsSeq:clsSeq, memberId:memberId};
        		callUrl = "<%=webRoot %>/portfolio/portfolioList.do";
        	}
        	if (idx == '6') {
        		data = {clsSeq:clsSeq};
        		callUrl = "<%=webRoot %>/portfolio/portfolioClassList.do";
        	}
        	if (idx == '7') {
        		data = {clsSeq:clsSeq, dayNo:dayNo};
        		callUrl = "<%=webRoot %>/portfolio/portfolioList.do";
        	}


        	
        	callAjaxData('post',callUrl, data, "getPortfolio");
        }

        function getPortFolioStudent() {
        	var memberId = $("#memberId").val();

        	var callUrl = "<%=webRoot %>/portfolio/portfolioStudent.do";
        	var data = {memberId:memberId};

        	callAjaxData('get',callUrl, data, "getPortfolio");
        }
        
        function getPortFolioStudentClasswork() {
        	var memberId = $("#memberId").val();
        	var clsSeq = $("#clsSeq").val();

        	var callUrl = "<%=webRoot %>/portfolio/portfolioStudentClasswork.do";
        	var data = {memberId:memberId, clsSeq:clsSeq};

        	callAjaxData('get',callUrl, data, "getPortfolio");
        }        
        
        function getPortFolioDetailInfo() {

        	var inclsRsltSeq = $("#inclsRsltSeq").val();
        	
        	var callUrl = "<%=webRoot %>/portfolio/getPortfolioInfo.do";
        	var data = {inclsRsltSeq : inclsRsltSeq};

        	callAjaxData('get',callUrl, data, "getPortfolio");
        }

        
        function getClassPortfolioNOKCount() {

        	var callUrl = "<%=webRoot %>/portfolio/getClassPortfolioNOKCount.do";
        	var data = {};

        	callAjaxData('get',callUrl, data, "getPortfolio");
        }        
        
        // 북쉘프
        function getBookshelf(idx) {
        	var prodSeq = $("#prodSeq").val();
        	var type = $("#type").val();
        	var code = $("#code").val();
        	var keyword = $("#keyword").val();

        	var callUrl;
        	if (idx == '1') {
        		callUrl = "<%=webRoot %>/bookshelf/bookList.do";
        	}
        	if (idx == '2') {
        		callUrl = "<%=webRoot %>/bookshelf/lessonList.do";
        	}
        	if (idx == '3') {
        		callUrl = "<%=webRoot %>/bookshelf/bookLessonInfo.do";
        	}
        	if (idx == '4') {
        		callUrl = "<%=webRoot %>/bookshelf/bookshelfSearch.do";
        	}

        	if (idx == '5') {
        		callUrl = "<%=webRoot %>/bookshelf/getBookshelfClassList.do";
        	}
        	
        	var data = {prodSeq : prodSeq, type:type, code:code, keyword:keyword, treeformat : "json"};
        	callAjaxData('post',callUrl, data, "getBookshelf");
        }


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        // IF #3.메세지 박스 - InBox
        function getInbox() {
        	var memberId = $("#3_memberId").val();
        	var name = $("#3_name").val();
        	var pageNo = $("#3_pageNo").val();
        	
        	var callUrl = "<%=webRoot %>/message/getInboxList.do";
        	var data = {memberId : memberId, name : name, pageNo : pageNo, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getInbox");
        }

        // IF #4.메세지 박스 - Message Compose
        function composeMesseage() {
			var memberId = $("#4_memberId").val();
			var senderId = $("#4_senderId").val();
			var msgCntt = $("#4_msgCntt").val();
			var msgType = $("#4_msgType").val();
			var pageNo = $("#4_pageNo").val();
			var callUrl;

			// 메시지 등록 / 조회 구분
			if (msgCntt == null || msgCntt == "") {
				callUrl = "<%=webRoot %>/message/getMessageList.do";
			} else {
				callUrl = "<%=webRoot %>/message/composeMessage.do";
			}

			var data = {memberId: memberId, senderId : senderId, msgCntt : msgCntt, msgType : msgType, pageNo : pageNo, treeformat : "json"};
			callAjaxData('post',callUrl, data, "composeMesseage");
        }

        // IF #5.메세지 박스 - 구성원 리스트 조회
        function getMemberList() {
        	var memberId = $("#5_memberId").val();
        	var memberNm = $("#5_memberNm").val();
        	var callUrl = "<%=webRoot %>/classroom/getClassMemberList.do";
        	var data = {memberId : memberId, memberNm : memberNm, treeformat : "json"};

        	//callAjaxData('post',callUrl, data, "getMemberList");
        	alert("미사용\\n클래스 구성원 리스트는 IF #7 사용");
        }

        // IF #6.티칭룸 - 클래스 목록
        function getClassList() {
        	var memberId = $("#6_memberId").val();
        	var callUrl = "<%=webRoot %>/classroom/getClassSimpleList.do";
        	var data = {memberId : memberId, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getClassList");
        }

        // IF #7.클래스의 학생목록 정보
        function getClassMemberList() {
        	var memberId = $("#7_memberId").val();
        	var classSeq = $("#7_classSeq").val();
        	var callUrl = "<%=webRoot %>/message/getClassMemberMessageList.do";
        	var data = {memberId : memberId, classSeq : classSeq, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getClassMemberList");
        }
        
  	   // IF #18-1.Bookshelf 메인 Book List 가져오기.
        function getBookList() {
        	var memberId = $("#18_1_memberId").val();
        	var memberType = $("#18_1_memberType").val();
        	var callUrl = "<%=webRoot %>/bookshelf/bookList.do";
        	var data = {memberId : memberId, memberType : memberType};
			
        	callAjaxData('post',callUrl, data, "getBookList");
        }
  	   
     	// IF #18-2.Bookshelf 메인 해당 Book의 Lesson List 가져오기.
        function getLessonList() {
        	var memberId = $("#18_2_memberId").val();
        	var prodSeq = $("#18_2_prodSeq").val();
        	var callUrl = "<%=webRoot %>/bookshelf/lessonList.do";
        	var data = {memberId : memberId, prodSeq : prodSeq, treeformat : "json"};
			
        	callAjaxData('post',callUrl, data, "getLessonList");
        }
     	
     	// IF #18-3.Bookshelf 메인 해당 Book과 Lesson의 상세 데이터 가져오기.
     	function getBookLessonInfo() {
     		var prodSeq = $("#18_3_prodSeq").val();
        	var callUrl = "<%=webRoot %>/bookshelf/bookLessonInfo.do";
        	var data = {code : prodSeq};
			
        	callAjaxData('post',callUrl, data, "getBookLessonInfo");
     	}
     	
     	// IF #18-4.Bookshelf 메인 특정 조건(타이틀, 코멘트 검색어) Lesson List 가져오기.
     	function getSearchLessonList() {
     		var memberId = $("#18_4_memberId").val();
     		var memberType = $("#18_4_memberType").val();
     		var keyword = $("#18_4_keyword").val();
        	var callUrl = "<%=webRoot %>/bookshelf/bookshelfSearch.do";
        	var data = {memberId : memberId, memberType : memberType, keyword : keyword};
			
        	callAjaxData('post',callUrl, data, "getSearchLessonList");
     	}
     	
        function getBookshelfClassList() {
        	var prodSeq = $("#18_5_prodSeq").val();
        	var callUrl = "<%=webRoot %>/bookshelf/getBookshelfClassList.do";
        	var data = { prodSeq : prodSeq, treeformat : "json"};
			
        	callAjaxData('post',callUrl, data, "getBookshelfClassList");
        }	
     	
     	
     	
     	
   		// IF #32. 뱃지주기
        function setBadge() {
        	var memberId = $("#32_memberId").val();
        	var targetId = $("#32_targetId").val();
        	var type = $("#32_type").val();
        	var code = $("#32_code").val();
        	var contentSeq = $("#32_contentSeq").val();
        	var contentType = $("#32_contentType").val();
        	var callUrl = "<%=webRoot %>/badge/setBadge.do";
        	var data = {memberId : memberId, targetId : targetId, type : type, code : code, contentSeq : contentSeq, contentType : contentType};

        	callAjaxData('post',callUrl, data, "setBadge");
        }
   		
        function getBadgeList() {
        	var memberId = $("#badgeList_mbrId").val();

        	var callUrl = "<%=webRoot %>/badge/badgeList.do";
        	var data = {
        			memberId: memberId
        			};

        	callAjaxData('get',callUrl, data, "badgeList_Result");
        }      		

        // IF #46.메인 - 미열람 메시지 카운트
        function getNewMessageCount() {
        	var memberId = $("#46_memberId").val();
        	var msgType = $("#46_msgType").val();

        	var callUrl = "<%=webRoot %>/message/getNewMessageCount.do";
        	var data = {memberId : memberId, msgType : msgType, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getNewMsgCount");
        }


        
        function getClassInfoList() {
        	var memberId = $("#memberId").val();

        	var callUrl = "<%=webRoot %>/teaching/getClassInfoList.do";
        	var data = {memberId : memberId, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getClassInfoList");
        }

        function getClassLessonList() {
        	var memberId = $("#memberId99").val();
        	var classSeq = $("#classSeq99").val();

        	var callUrl = "<%=webRoot %>/classroom/getClassLessonList.do";
        	var data = {memberId : memberId, classSeq:classSeq, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getClassLessonList");
        }

        function getClassLessonInfoList() {
        	var memberId = $("#memberId").val();
        	var classSeq = $("#classSeq").val();
        	var lessonCd = $("#lessonCd").val();

        	var callUrl = "<%=webRoot %>/teaching/getClassLessonInfoList.do";
        	var data = {memberId : memberId, classSeq:classSeq, lessonCd:lessonCd, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getClassLessonInfoList");
        }

        function getPortFolioInfo() {

        	var memberId = $("#memberId").val();
        	var classSeq = $("#classSeq").val();
        	var lessonCd = $("#lessonCd").val();
        	var portSeq = $("#portSeq").val();
        	
        	var callUrl = "<%=webRoot %>/teaching/getPortFolioInfo.do";
        	var data = {memberId : memberId, classSeq:classSeq, lessonCd:lessonCd, portSeq:portSeq, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getPortFolioInfo");
        }


        
        function getLessonCardMapList() {
        	var memberId = $("#memberId").val();
        	var lessonCd = $("#lessonCd").val();
        	var dayNo = $("#dayNo").val();

        	var callUrl = "<%=webRoot %>/teaching/cardMapList.do";
        	var data = {memberId : memberId, lessonCd:lessonCd, dayNo:dayNo, treeformat : "json"};

        	callAjaxData('post',callUrl, data, "getLessonCardMapList");
        }



        
        // IF #1396 
        function get1396List() {
            var c_id = $("#1396_id").val();
            var refType = $("#refType").val();

            var callUrl = "<%=webRoot %>/contents/ReferenceList.do";

            var data = {cid : c_id, refType : refType,  treeformat : "json"};
            callAjaxData('post',callUrl, data, "1396_result");
        }
        
        // 레퍼런스 파일 총 카운트 
        function getRefListCnt() {
            var c_id = $("#ref_cnt_id").val();

            var callUrl = "<%=webRoot %>/contents/ReferenceListCnt.do";

            var data = {cid : c_id, treeformat : "json"};
            callAjaxData('post',callUrl, data, "ref_cnt_result");
        }
        
        // 레퍼런스 파일 타입 목록 조회 
        function getRefTypeCnt() {
            var c_id = $("#ref_type_id").val();

            var callUrl = "<%=webRoot %>/contents/ReferenceTypeList.do";

            var data = {cid : c_id, treeformat : "json"};
            callAjaxData('post',callUrl, data, "ref_type_result");
        }
        
        // IF#1645
        function get1645() {
        	var classSeq = $("#1645_1").val();

        	var callUrl = "<%=webRoot %>/classroom/getClassRelayIp.do";
        	var data = {classSeq : classSeq};

        	callAjaxData('get',callUrl, data, "1645_result");
        }
        
        
        function removeComment() {
        	var cmmtSeq = $("#cmmt_cmmtSeq").val();

        	var callUrl = "<%=webRoot %>/comment/removeComment.do";
        	var data = {cmmtSeq : cmmtSeq};

        	callAjaxData('post',callUrl, data, "cmmt_removeResult");
        }
        
        function addComment() {
        	var cmmtCntt = $("#cmmt_cmmtCntt").val();
        	var contentType = $("#cmmt_contentType").val();
        	var contentId = $("#cmmt_contentId").val();

        	var callUrl = "<%=webRoot %>/comment/addComment.do";
        	var data = {cmmtCntt : cmmtCntt
        			,contentType: contentType
        			,contentId: contentId        			
        			};

        	callAjaxData('post',callUrl, data, "cmmt_addResult");
        }        
        
        
        function getContentComment() {
        	var contentType = $("#contentcmmt_contentType").val();
        	var contentId = $("#contentcmmt_contentId").val();

        	var callUrl = "<%=webRoot %>/comment/getContentComment.do";
        	var data = {
        			contentType: contentType
        			,contentId: contentId        			
        			};

        	callAjaxData('get',callUrl, data, "cmmt_getContentComment");
        }        

        function toggleLike() {
        	var contentType = $("#like_contentType").val();
        	var contentId = $("#like_contentId").val();

        	var callUrl = "<%=webRoot %>/comment/toggleLike.do";
        	var data = {
        			contentType: contentType
        			,contentId: contentId        			
        			};

        	callAjaxData('post',callUrl, data, "like_Result");
        }     
        
        function existDoLike() {
        	var contentType = $("#like_contentType").val();
        	var contentId = $("#like_contentId").val();

        	var callUrl = "<%=webRoot %>/comment/existDoLike.do";
        	var data = {
        			contentType: contentType
        			,contentId: contentId        			
        			};

        	callAjaxData('get',callUrl, data, "like_Result");
        }     
        
        /**
        * [IF] $25 파일 업로드 테스트 
        */
        function fileUploadTest(){
        	$("#upForm").submit();
        }
        
        // 포트폴리오 저장 
        function portFolioInsert(){
        	$("#portForm").submit();
        }
        
        function onlyFileInsert(){
            $("#testForm").submit();
        }
        
        
        // 노트 정보 조회 및 첨부파일 다운로드 경로 목록 조회 
        function getNoteData(){
        	
            var note_cid = $("#note_cid2").val();
            var note_prodSeq = $("#note_prodSeq2").val();
            var note_memberId = $("#note_memberId2").val();
            var note_curPage = $("#note_curPage2").val();
            

            var callUrl = "<%=webRoot %>/upload/getNoteData.do";
            var data = {
            		note_cid: note_cid
            		, note_prodSeq : note_prodSeq
                    ,note_memberId: note_memberId
                    ,note_curPage: note_curPage       
                    };

            callAjaxData('post',callUrl, data, "note_result");
        }
        
        
       /**
       * 노트가 등록된 레슨 목록 조회 
       */
       function getNoteLessonList(){
            
            var note_memberId = $("#note_memberId3").val();
            var callUrl = "<%=webRoot %>/mynote/getLessonList.do";
            var data = {
                    note_memberId: note_memberId
                    };

            callAjaxData('post',callUrl, data, "note_result3");
        }
       
       function getNoteList(){
           
           var note_memberId = $("#note_memberId4").val();
           var note_lessonCd = $("#note_lessonCd").val();
           var callUrl = "<%=webRoot %>/mynote/getMyNoteList.do";
           var data = {
                   note_memberId: note_memberId
                   , note_lessonCd: note_lessonCd
                   };

           callAjaxData('post',callUrl, data, "note_result4");
       }
       
        
       function addDataFirstLesson(){
           var clsSeq = $("#clsSeq11").val();
           if(clsSeq == ''){
        	   alert('클래스 SEQ 입력바람.');
        	   return;
           }
           var callUrl = "<%=webRoot %>/classroom/addDataTodayClass.do";
           var data = {
        		   clsSeq: clsSeq
                   };

           callAjaxData('post',callUrl, data, "result11");
       }

       function modifyClassNextSchedule(){
           var clsSeq = $("#clsSeq12").val();
           if(clsSeq == ''){
               alert('클래스 SEQ 입력바람.');
               return;
           }
           var callUrl = "<%=webRoot %>/classroom/modifyClassNextSchedule.do";
           var data = {
                   clsSeq: clsSeq
                   };

           callAjaxData('post',callUrl, data, "result12");
       }

       
        function callAjaxData(type, callUrl, pData, showId){
        	
            $.ajax({            
                type : type,
                xhrFields: {
                    withCredentials: true
                 },
                crossDomain : true,
                url : callUrl,
                async : false,
                cache : false,
                headers: {        
                	Accept : "application/json"      
                },
                datatype : "json",
                data : pData,
                success : function(data){
                	$("#"+showId).text(JSON.stringify(data));
                	$("#edit").val(JSON.stringify(data));
                },
                error: function (xhr, ajaxOptions, thrownError){
                	   alert("code:"+xhr.status+"\n"+"message:"+xhr.responseText+"\n"+"error:"+thrownError);
                }, 
                complete:function (xhr, textStatus){
                	//
                }  
            });
        }
    </script>
</head>
    <style>
        body{
         padding-top:20px;
         padding-left:20px;
        }
        .green{
            color:green;
        }
        #txtState{
            font-weight:bold;
        }
        #ptmode.on{
            background-color: green;
        }
        #ptmode.off{
            background-color: red;
        }
    </style>
<body>
    <hr/>
        <b>[ Check Yourself!!! ]</b><p>
        
        제이슨 데이터 확인!!
        <iframe src="http://jsonviewer.stack.hu/" style="height:300px;width:100%"></iframe>
    
    <hr/>
      <a href="javascript:callLogin();"><b>IF #1.로그인</b></a><p>
            userId:<input type="text" name="userId" id="userId" value="tbs001" />
            password:<input type="text" name="pwd"  id="pwd" value="1234" />
            <input Type="button" value="저장" onclick="javascript: callLogin();" />
            선생님: (tbs001/1234, tal001/1234, tkl001/1234)
            학생: (bs001/1234, al001/1234, kl001/1234)
            <p>
      데이터 결과 값: <b id="rstTag1"></b></p>
      <!-- <b>  *itemId는 <font color="red">10</font>, quesNo는  <font color="red">Step.C</font>로 고정값 입니다.</b></p> -->
    <hr/>
    
      <a href="javascript:callLoginTest();"><b>로그인_test</b></a><p>
            userId:<input type="text" name="userId" id="t_userId" value="tch001" />
            password:<input type="text" name="pwd"  id="t_pwd" value="1234" />
            <input Type="button" value="저장" onclick="javascript: callLoginTest();" />
            선생님: (tbs001/1234, tal001/1234, tkl001/1234, tch001/1234)
            학생: (bs001/1234, al001/1234, kl001/1234, ch001/1234)
            <p>
      데이터 결과 값: <b id="rstTag999"></b></p>
      <!-- <b>  *itemId는 <font color="red">10</font>, quesNo는  <font color="red">Step.C</font>로 고정값 입니다.</b></p> -->
    <hr/>
        
      <a href="javascript: callMemberInfo();"><b>IF #2.메인화면 정보(선생님): 사용자 기본정보 조회</b></a><p>
      <!-- <b>  *itemId는 <font color="red">10</font>, quesNo는  <font color="red">Step.C</font>로 고정값 입니다.</b></p> -->
            userId:<input type="text" name="userId" id="if2_userId" value="t" />
            <input Type="button" value="저장" onclick="javascript: callMemberInfo();" /><p>
            데이터 결과 값: <b id="rstTag2"></b></p>
    <hr/>
    <hr/>
      <a href="javascript:getMain();"><b>런처T > 메인</b></a>
      <p>
          todayYmd:<input type="text" name="todayYmd" id="todayYmd1" value="20140221" />(20140217/20140219/20140221/20140224/20140226/20140228)<br/>
          <p>데이터 결과 값:<br/><b id="getMain"></b></p>
      </p>
    <hr/>
      <a href="javascript:getClasses();"class="rainbow"><b>런처T > 메인 > Today's Classes (swipe)</b></a>
      <p>
          todayYmd:<input type="text" name="todayYmd" id="todayYmd2" value="20140221" class="rainbow"/>(20140217/20140219/20140221/20140224/20140226/20140228)<br/>
          <p>데이터 결과 값:<br/><b id="getClasses"></b></p>
      </p>
    <hr/>
    <hr/>
      <a href="javascript:getContents();"><b>런처T/S > 메인 > 컨텐츠 버전 체크 (다운로드 URL 요청)</b></a>
      <p>
          lessonCd:<input type="text" name="lessonCd" id="lessonCd" value="2" />(2/33/109)<br/>
          version:<input type="text" name="version" id="version" value="1.0" />(1.0/1.1)<br/>
          <p>데이터 결과 값:<br/><b id="getContents"></b></p>
      </p>
    <hr/>
    	<a href="javascript:getDayList();"><b>해당 Lesson의 Day 리스트</b></a>
      <p>
          lessonCd:<input type="text" name="lessonCd" id="dayList_lessonCd" value="22" /><br/>
          dayList_tmbrId : <input type="text" name="dayList_tmbrId" id="dayList_tmbrId" value="tch001" /><br/>
          <p>데이터 결과 값:<br/><b id="getDayList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getCardMapList();"><b>카드 맵 리스트</b></a>
      <p>
      --------- sample parameter ---------------------<br/>
      booster Book | 선생 아이디 ( tbs001 ) L1 코드:( 2 )-D1 코드:( 3 )<br/>
      alphabet | 선생 아이디 ( tal001 ) L1 코드:( 33 )-D1 코드:( 39 )<br/>
      --------- sample parameter ---------------------<br/>
      
      <b>결과값중 레슨 관련 데이터는 paramMap.lessonInfo 에 있고, 카드맵 목록은 result에 들어가 있슴.</b> <br/>
      	  mbrId:<input type="text" name="mbrId" id="cardMapList_mbrId" value="tal001" /><br/>
          lessonCd:<input type="text" name="lessonCd" id="cardMapList_lessonCd" value="33" /><br/>
          dayNo:<input type="text" name="dayNo" id="cardMapList_dayNo" value="39" /><br/>

            강사 일 경우 클래스/IP 정보 추가<br/>
            classSeq:<input type="text" name="classSeq" id="cardMapList_classSeq" value="" /><br/>
            classLocalIp:<input type="text" name="classLocalIp" id="cardMapList_classLocalIp" value="" /><br/>

          <p>데이터 결과 값:<br/><b id="getCardMapList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getCardMapSubList();"><b>카드 맵 서브 리스트</b></a>
      <p>
      --------- sample parameter ---------------------<br/>
      booster Book | 선생 아이디 ( tbs001 ) L1 코드:( 2 )-D1 코드:( 3 )<br/>
      alphabet | 선생 아이디 ( tal001 ) L1 코드:( 33 )-D1 코드:( 39 )<br/>
      --------- sample parameter ---------------------<br/>

          mbrId:<input type="text" name="mbrId" id="cardMapList_mbrId1" value="tal001" /><br/>
          lessonCd:<input type="text" name="lessonCd" id="cardMapList_lessonCd1" value="33" /><br/>
          dayNo:<input type="text" name="dayNo" id="cardMapList_dayNo1" value="39" /><br/>
          <p>데이터 결과 값:<br/><b id="getCardMapSubList"></b></p>
      </p>
    <hr/>
    <a href="javascript:getCardInfo();"><b>해당 카드 상세 데이터</b></a>
      <p>
      	  prodSeq(카드 ID):<input type="text" name="prodSeq" id="cardInfo_prodSeq" value="34" /><br/>
      <p>데이터 결과 값:<br/><b id="getCardInfo"></b></p>
    <hr/>
      <a href="javascript:getClassLessonList();"><b>카드맵 레슨 목록 조회</b></a>
      <p>결과 항목 : lessonCd(레슨 코드), lessonNm(레슨 명), dayNoCd(레슨 1 차시 코드)<br/>
          memberId:<input type="text" name="memberId" id="memberId99" value="tbs001" /><br/>
          classSeq:<input type="text" name="classSeq" id="classSeq99" value="1" /><br/>

          <p>데이터 결과 값:<br/><b id="getClassLessonList"></b></p>
      </p>
    <hr/>
    <a href="javascript:setCardSort();"><b>해당 카드 리스트 정렬 값 저장.</b></a>
      <p>
      	  카드 리스트 정렬 값 저장.
      	  콤마로 구분된 카드 코드 값을 전달
      	  예시) 1,2,3,4,5<br/>
      	  memberId(선생 ID):<input type="text" name="memberId" id="cardSort_memberId" value="tal001" /><br/>
      	  dayCd(Day ID):<input type="text" name="dayCd" id="cardSort_dayCd" value="39" /><br/>
      	  sortData(정렬 데이터):<input type="text" name="sortData" id="cardSort_sortData" value="45,46,47,48,49,50,51,52,53,54" /><br/>
      <p>데이터 결과 값:<br/><b id="setCardSort"></b></p>
    <hr/>
      <b>포트폴리오</b>
<pre>
강사 - 레슨 리스트				
									clsSeq			클래스 코드
강사 - 레슨 별 포트폴리오 리스트
									clsSeq			클래스 코드
									lessonCd		레슨 코드
									dayNo			차시
강사 - 학생 포트폴리오 리스트		
									memberId		학생 코드
									sortKey			정렬 코드
특정 학생에 해당하는 모든 포트폴리오 리스트
									memberId		학생 코드
특정 학생의 특정 클래스에 해당하는 모든 포트폴리오 리스트		
									memberId		학생 코드
									clsSeq			클래스 코드
특정 클래스의 모든 포트폴리오 리스트
									clsSeq			클래스 코드
특정 클래스의 특정 차시에 해당하는 모든 포트폴리오 리스트
									clsSeq			클래스 코드
									dayNo			차시
학생 포트폴리오 기본 정보
									memberId		학생 코드
학생 포트폴리오 Classwork 기본 정보
									memberId		학생 코드
									clsSeq			클래스 코드
포트폴리오 상세
									inclsRsltSeq 포트폴리오 코드
</pre>
      <a href="javascript:getPortfolio('1');"><b>강사 - 포트폴리오 레슨</b></a><br/>
      <a href="javascript:getPortfolio('2');"><b>강사 - 포트폴리오 차시 </b></a><br/>
      <a href="javascript:getPortfolio('3');"><b>강사 - 포트폴리오 리스트 </b></a><br/>
      <a href="javascript:getClassPortfolioNOKCount()"><b>강사 - 미확인 학생 포트폴리오 건수 조회(파라미터 없음)</b></a><br/>
      <!-- <a href="javascript:getPortfolio('4');"><b>강사 - 포트폴리오 리스트 Day</b></a><br/> -->
      <a href="javascript:getPortfolio('4');"><b>특정 학생에 해당하는 모든 포트폴리오 리스트 </b></a><br/>
      <a href="javascript:getPortfolio('5');"><b>특정 학생의 특정 클래스에 해당하는 모든 포트폴리오 리스트	</b></a><br/>
      <a href="javascript:getPortfolio('6');"><b>특정 클래스의 모든 포트폴리오 리스트 </b></a><br/>
      <a href="javascript:getPortfolio('7');"><b>특정 클래스의 특정 차시에 해당하는 모든 포트폴리오 리스트 </b></a><br/>
      
      <a href="javascript:getPortFolioStudent()"><b>학생 포트폴리오 기본 정보</b></a><br/>
      <a href="javascript:getPortFolioStudentClasswork()"><b>학생 포트폴리오 Classwork 기본 정보</b></a><br/>
      <a href="javascript:getPortFolioDetailInfo()"><b>포트폴리오 상세 정보</b></a><br/>
      
      1) 포트폴리오 레슨 가져오기 dayNo : 67 <Br/>
      
      2) 포트폴리오 차시 가져오기 LESSON_CD : 59<Br/>
          레슨에서 가져온 Lesson값<Br/>
      3) 포트폴리오 리스트  CLS_SEQ : 1        <Br/>
      4) 포트폴리오 리스트  차시별 출력 CLS_SEQ : 1 / dayNo :67        <Br/>
      5) 포트폴리오 리스트  학생 포트폴리오 전체 출력    / memberId : s4 or memberId : esl <Br/>
      
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="" />MemberId <br/>
          clsSeq:<input type="text" name="clsSeq" id="clsSeq" value="" />clsSeq : 1<br/>
          lessonCd:<input type="text" name="lessonCd" id="portlessonCd" value="" />LessonCD : 59<br/>
          inclsRsltSeq:<input type="text" name="inclsRsltSeq" id="inclsRsltSeq" value="" /><br/>
          sortKey:<input type="text" name="sortKey" id="sortKey" value="Tile" />Thread, Tile<br/>
          dayNo:<input type="text" name="dayNo" id="dayNo" value="" />dayNo : 67<br/>
          <p>데이터 결과 값:<br/><b id="getPortfolio"></b></p>
      </p>
    <hr/>
    <hr/>
      <b>티칭룸 / 스터디룸</b>
<pre>
강사 - 티칭룸
									sortKey			정렬 코드 (전체:ALL, 현재 진행:'')
									pageNo			페이지 번호
학생 - 스터디룸
									sortKey			정렬 코드 (전체:ALL, 현재 진행:'')
									pageNo			페이지 번호
</pre>
      <a href="javascript:getClassroomList();"><b>티칭룸 / 스터디룸</b></a>
      <p>
          sortKey:<input type="text" name="sortKey9" id="sortKey9" value="" /><br/>
          pageNo:<input type="text" name="pageNo9" id="pageNo9" value="" /><br/>
          <p>데이터 결과 값:<br/><b id="getClassroomList"></b></p>
      </p>
    <hr/>
<!--
     <hr/>
      <b>북쉘프</b>
<pre>
북 리스트							로그인 정보
레슨 리스트
									prodSeq			북 코드
북/레슨 상세 보기
									type			북/레슨 (book/lesson)
									code			북/레슨 코드
레슨 검색
									keyword			레슨 검색 키워드
</pre>
      <a href="javascript:getBookshelf('1');"><b>북 리스트</b></a><br/>
      <a href="javascript:getBookshelf('2');"><b>레슨 리스트</b></a><br/>
      <a href="javascript:getBookshelf('3');"><b>북/레슨 상세 보기</b></a><br/>
      <a href="javascript:getBookshelf('4');"><b>레슨 검색</b></a><br/>
      <p>
          prodSeq:<input type="text" name="prodSeq" id="prodSeq" value="" /><br/>
          type:<input type="text" name="type" id="type" value="" /><br/>
          code:<input type="text" name="code" id="code" value="" /><br/>
          keyword:<input type="text" name="keyword" id="keyword" value="" /><br/>
          <p>데이터 결과 값:<br/><b id="getBookshelf"></b></p>
      </p>
    <hr/>
 -->

<!-- 
      <a href="javascript:getLessonCardMapList();"><b>런처 T - Lesson 카드 맵</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="TCHR1" /><br/>
          lessonCd:<input type="text" name="prodSeq" id="lessonCd" value="1" /><br/>
          dayNo:<input type="text" name="prodSeq" id="dayNo" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="getLessonCardMapList"></b></p>
      </p>
    <hr/>
    <hr/>
      <a href="javascript:getBookList();"><b>런처 T - 북쉘프 (북 리스트)</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="TCHR1" /><br/>
          <p>데이터 결과 값:<br/><b id="getBookList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getLessonList();"><b>런처 T - 북쉘프 - 북 - Lesson 리스트</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="TCHR1" /><br/>
          prodSeq:<input type="text" name="prodSeq" id="prodSeq" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="getLessonList"></b></p>
      </p>
    <hr/>
 -->











    <hr/>
      <a href="javascript:getInbox();"><b>IF #3.메세지 박스 - InBox</b></a><p>
      1. inBox<br/>
      2. inBox 이름 검색<br/>
      (메시지 열람여부 : MSG_OK_YN - Y/N)<br/>
      memberId(수신자 ID):<input type="text" name="memberId" id="3_memberId" value="" />(tbs001, tal001, tkl001),<br/>
      name(이름 검색):<input type="text" name="name" id="3_name" value="" />(bs001, al001, kl001),<br/>
      pageNo:<input type="text" name="pageNo" id="3_pageNo" value="1" />(pageNo - 0:전체)<br/>
      <p>데이터 결과 값:<br/><b id="getInbox"></b></p>
    <hr/>
      <a href="javascript:composeMesseage();"><b>IF #4.메세지 박스 - Message Compose</b></a>
      <p>
      1.메시지 내역 조회 : msgCntt - null<br/>
      memberId:<input type="text" name="memberId" id="4_memberId" value="" />,<br/>
      senderId:<input type="text" name="senderId" id="4_senderId" value="" />,<br/>
      pageNo:<input type="text" name="pageNo" id="4_pageNo" value="1" />(pageNo - 0:전체)<br/><br/>
      2.필수 입력 : msgCntt(메시지 내용), msgType(메시지 타입)<br/>
      (msyType - 1:강사comment, 2:대화, 3:review학생 질문, 4:review강사 답변)<br/>
      msgCntt:<input type="text" name="msgCntt" id="4_msgCntt" value="" />,<br/>
      msgType:<input type="text" name="msgType" id="4_msgType" value="" />,<br/>
      <p>데이터 결과 값:<br/><b id="composeMesseage"></b></p>
      </p>
    <hr/>
      <a href="javascript:getMemberList();"><b>IF #5.메세지 박스 - 구성원 리스트 조회 (사용 안함)</b></a>
      <p>
          강사가 담당하는 클래스의 모든 학생 목록<br/>
          memberId(강사ID):<input type="text" name="memberId" id="5_memberId" value="t" /><br/>
          memberNm(학생 이름):<input type="text" name="memberNm" id="5_memberNm" value="" />(학생 이름 검색)<br/>
          <p>데이터 결과 값:<br/><b id="getMemberList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getClassList();"><b>IF #6.티칭룸 - 클래스 목록</b></a>
      <p>
          강사가 담당하는 클래스 목록<br/>
          memberId(강사ID):<input type="text" name="memberId" id="6_memberId" value="t" /><br/>
          <p>데이터 결과 값:<br/><b id="getClassList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getClassMemberList();"><b>IF #7.클래스의 학생목록 정보</b></a>
      <p>
          강사가 담당하는 클래스 별 학생 목록<br/>
          memberId(강사ID):<input type="text" name="memberId" id="7_memberId" value="t" />,<br/>
          classSeq(클래스 ID):<input type="text" name="classSeq" id="7_classSeq" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="getClassMemberList"></b></p>
      </p>
    <hr/>
    <a href="javascript:getBookList();"><b>IF #18-1.Bookshelf 메인 Book List 가져오기.</b></a>
      <p>
          해당 ID의 Book List 가져오기.<br/>
          memberId(유저 ID):<input type="text" name="memberId" id="18_1_memberId" value="tal001" />,<br/>
          memberType(유저 타입):<input type="text" name="memberType" id="18_1_memberType" value="T" /><br/>
          <p>데이터 결과 값:<br/><b id="getBookList"></b></p>
      </p>
    <hr/>
    <a href="javascript:getLessonList();"><b>IF #18-2.Bookshelf 메인 해당 Book의 Lesson List 가져오기.</b></a>
      <p>
          해당 Book ID의 Lesson List 가져오기.<br/>
          memberId(유저 ID):<input type="text" name="memberId" id="18_2_memberId" value="tal001" />,<br/>
          prodSeq(Book ID):<input type="text" name="prodSeq" id="18_2_prodSeq" value="58" /><br/>
          <p>데이터 결과 값:<br/><b id="getLessonList"></b></p>
      </p>
    <hr/>
    <a href="javascript:getBookLessonInfo();"><b>IF #18-3.Bookshelf 메인 해당 Book과 Lesson의 상세 데이터 가져오기.</b></a>
      <p>
          해당 Book과 Lesson의 상세 데이터 가져오기.<br/>
          code(Book/Lesson ID):<input type="text" name="code" id="18_3_prodSeq" value="58" /><br/>
          <p>데이터 결과 값:<br/><b id="getBookLessonInfo"></b></p>
      </p>
    <hr/>
    <a href="javascript:getSearchLessonList();"><b>IF #18-4.Bookshelf 메인 특정 조건(타이틀, 코멘트 검색어) Lesson List 가져오기.</b></a>
      <p>
          특정 조건(타이틀, 코멘트 검색어) Lesson List 가져오기.<br/>
          memberId(유저 ID):<input type="text" name="memberId" id="18_4_memberId" value="tal001" />,<br/>
          memberType(유저 타입):<input type="text" name="memberType" id="18_4_memberType" value="T" />,<br/>
          keyword(검색어):<input type="text" name="keyword" id="18_4_keyword" value="Lesson" /><br/>
          <p>데이터 결과 값:<br/><b id="getSearchLessonList"></b></p>
      </p>
    <hr/>
    <a href="javascript:getBookshelfClassList();"><b>IF #18-5.Bookshelf 레슨 선택시 보여질 클래스 목록 가져오기 </b></a>
      <p>
          bookshelf에서 해당 책을 사용하는 클래스 목록 가져오기 (로그인한 아이디로 조회함. )<br/>
          prodSeq(Book ID):<input type="text" name="prodSeq" id="18_5_prodSeq" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="getBookshelfClassList"></b></p>
      </p>
    <hr/>
        
    
    <a href="javascript:setBadge();"><b>IF #32. 뱃지주기</b></a>
      <p>
      	기본 값 예 ) 강사(memberId: tal001)가 학생(targetId: bs001)에게 11(contentSeq: 11) 번 카드(contentType: BOOK)에 학습을 다 마친 학습자에게 주는 상(type: BG100, code: 1)으로 배지를 줌.<br/>
          memberId(강사ID):<input type="text" name="memberId" id="32_memberId" value="tal001" />,<br/>
          targetId(학생 ID):<input type="text" name="targetId" id="32_targetId" value="bs001" /><br/>
          type(뱃지 타입):<input type="text" name="type" id="32_type" value="BG100" /><br/>
          code(뱃지 코드):<input type="text" name="code" id="32_code" value="7" /><br/>
          contentSeq(컨텐츠 ID):<input type="text" name="contentSeq" id="32_contentSeq" value="6" /><br/>
          contentType(컨텐츠 유형):<input type="text" name="contentType" id="32_contentType" value="CARD" /><br/>
          <p>데이터 결과 값:<br/><b id="setBadge"></b></p>
      </p>
    <hr/>
    <a href="javascript:getBadgeList();"><b>mbrId 사용자의 뱃지 리스트 조회</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="badgeList_mbrId" value="" />,<br/>
          <p>데이터 결과 값:<br/><b id="badgeList_Result"></b></p>
      </p>
    <hr/>    
      <a href="javascript:getNewMessageCount();"><b>IF #46.메인 - 미열람 메시지 카운트</b></a>
      <p>
          memberId(수신자 ID):<input type="text" name="memberId" id="46_memberId" value="t" />,<br/>
          msgType:<input type="text" name="msgType" id="46_msgType" value="0" readonly />(메시지 타입 - 0:강사comment, 1:conversation, 2:학생review Q(추후), 3:강사review A(추후))<br/>
          <p>데이터 결과 값:<br/><b id="getNewMsgCount"></b></p>
      </p>
    <hr/>


    <hr/>
      <a href="javascript:getClassInfoList();"><b>티칭룸 - 클래스 목록</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="t" /><br/>

          <p>데이터 결과 값:<br/><b id="getClassInfoList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getClassLessonList();"><b>티칭룸 - 클래스 레슨 목록</b></a>
      <p>
          memberId:<input type="text" name="memberId99" id="memberId99" value="tbs001" /><br/>
          classSeq:<input type="text" name="classSeq99" id="classSeq99" value="1" /><br/>

          <p>데이터 결과 값:<br/><b id="getClassLessonList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getClassLessonInfoList();"><b>티칭룸 - 클래스 레슨 포트폴리오</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="tbs001" /><br/>
          classSeq:<input type="text" name="classSeq" id="classSeq" value="1" /><br/>
          lessonCd:<input type="text" name="lessonCd" id="lessonCd" value="2" /><br/>
          <p>데이터 결과 값:<br/><b id="getClassLessonInfoList"></b></p>
      </p>
    <hr/>
      <a href="javascript:getPortFolioInfo();"><b>티칭룸 - 클래스 레슨 포트폴리오 상세 보기</b></a>
      <p>
          memberId:<input type="text" name="memberId" id="memberId" value="t" /><br/>
          classSeq:<input type="text" name="classSeq" id="classSeq" value="1" /><br/>
          lessonCd:<input type="text" name="lessonCd" id="lessonCd" value="1" /><br/>
          portSeq:<input type="text" name="portSeq" id="portSeq" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="getPortFolioInfo"></b></p>
      </p>
    <hr/>

      <a href="javascript: get1396List();"><b>IF #1396.공통기능 전자칠판[서버연동]_ 퀵메뉴- 레퍼런스 파일 목록 조회</b></a>

      <p>
      호출 url = /contents/ReferenceList.do<br/>
      [파라메터 설명] <br/>
      prodTitle : 파일명<br/>
      thmbPath : 썸네일 경로<br/>
      dtlCntt : 컨텐츠 경로 (webRoot path = http://10.30.0.246:9090/VisangNAS/tree 로 해주면 보입니다.)<br/>
      prodType : 컨텐츠 구분 (이미지= FT001 , 동영상= FT003, 텍스트= FT005, url= FT004)<br/>
      </p>
      <p>
          컨텐츠 ID(card ID):<input type="text" name="1396_id" id="1396_id" value="9" /> Booster Book > The Hat 1차시 > Story Words  <br/>
          컨텐츠 type(card ID):<input type="text" name="refType" id="refType" value="FT001" /><br/>
          <p>데이터 결과 값:<br/><b id="1396_result"></b></p>
      </p>
    <hr/>

      <a href="javascript: getRefListCnt()"><b>공통기능 전자칠판[서버연동]_ 퀵메뉴- 레퍼런스 파일 count  조회</b></a>
      
      <p>
      호출 url = /contents/ReferenceListCnt.do<br/>
      
      [파라메터 설명] <br/>
      card Id 는 prodSeq를 넘겨주면 끝! 
      result 에 조회된 목록 숫자 리턴 함. 
      </p>
      <p>
          컨텐츠 ID(card ID):<input type="text" name="ref_cnt_id" id="ref_cnt_id" value="9" /> Booster Book > The Hat 1차시 > Story Words <br/>
          <p>데이터 결과 값:<br/><b id="ref_cnt_result"></b></p>
      </p>
    <hr/>
    
          <a href="javascript: getRefTypeCnt()"><b>IF #1396.공통기능 전자칠판[서버연동]_ 퀵메뉴- 레퍼런스 파일 목록 조회</b></a>
      
      <p>
      호출 url = /contents/ReferenceTypeList.do <br/>
      
      [파라메터 설명] <br/>
      prodTitle : 파일명<br/>
      thmbPath : 썸네일 경로<br/>
      dtlCntt : 컨텐츠 경로 (webRoot path = http://10.30.0.246:9090/tree 로 해주면 보입니다.)<br/>
      prodType : 컨텐츠 구분 (이미지= IMG , 동영상= MOV, 텍스트= TXT, url= URL)<br/>
      </p>
      <p>
          컨텐츠 ID(card ID):<input type="text" name="ref_type_id" id="ref_type_id" value="9" /><br/>
          <p>데이터 결과 값:<br/><b id="ref_type_result"></b></p>
      </p>
    <hr/>
    

      <a href="javascript: get1645();"><b>IF #1645. 서버 local IP 전달</b></a>
      
      <p>
      [파라메터 설명] <br/>
      classSeq : 클래스 번호<br/>
      </p>
      <p>
          클래스 번호:<input type="text" name="classSeq" id="1645_1" value="15" /><br/>
          <p>데이터 결과 값:<br/><b id="1645_result"></b></p>
      </p>
    <hr/>

      <a href="javascript: addComment();"><b>댓글 쓰기</b></a>
      
      <p>
      [파라메터 설명] <br/>
      cmmtCntt : 댓글내용<br/>
      contentType : 컨텐츠 종류 (CT001 : 상품, CT002 : 포트폴리오)<br/>
      contentId : 컨텐츠 번호<br/>
      </p>
      <p>
          댓글내용 :<input type="text" name="cmmtCntt" id="cmmt_cmmtCntt" value="댓글댓글" /><br/>
          컨텐츠 종류:<input type="text" name="contentType" id="cmmt_contentType" value="CT002" /><br/>
          컨텐츠 번호:<input type="text" name="contentId" id="cmmt_contentId" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="cmmt_addResult"></b></p>
      </p>
    <hr/>

	<a href="javascript: removeComment();"><b>댓글 삭제</b></a>

      <p>
      [파라메터 설명] <br/>
      cmmtSeq : 댓글 번호<br/>
      </p>
      <p>
          댓글 번호:<input type="text" name="cmmtSeq" id="cmmt_cmmtSeq" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="cmmt_removeResult"></b></p>
      </p>
    <hr/>

	<a href="javascript: getContentComment();"><b>댓글 목록</b></a>

      <p>
      [파라메터 설명] <br/>
      contentType : 컨텐츠 종류 (CT001 : 상품, CT002 : 포트폴리오)<br/>
      contentId : 컨텐츠 번호<br/>
      </p>
      <p>
          컨텐츠 종류:<input type="text" name="contentType" id="contentcmmt_contentType" value="CT002" /><br/>
          컨텐츠 번호:<input type="text" name="contentId" id="contentcmmt_contentId" value="1" /><br/>
          <p>데이터 결과 값:<br/><b id="cmmt_getContentComment"></b></p>
      </p>
    <hr/>
    
      <a href="javascript: void(0);" onClick="javascript: fileUploadTest();"><b>[IF]$25 노트 파일 업로드 요청.</b></a>
      <p>
      <b>
      호출하는 url: http://10.30.0.246:9090/tree/upload/noteFileUpload.do <br/>
      multipart/form-data로 호출해야하고, file이 넘어오는 파라메터명은 uploadfile 입니다. 용량제한은 10Mb로 걸려있슴다. <br/>
      업로드 되는 서버의 위치는   /home/treeAdmin/upfiledir/note 이며, 파일명은 임의로 생성하고 있어요. <br/>
      </b>
          <form id="upForm" name="upForm" method="post" action="<%=webRoot%>/upload/noteFileUpload.do" enctype="multipart/form-data" accept-charset="utf-8">
          파라메터명(회원아이디 세션에서 불러옴.) = note_memberId : <input type="text" name="note_memberId" id="note_memberId" value="sal001"/><br/>
          파라메터명(카드(액티비티순번)) = note_cid: <input type="text" name="note_cid" id="note_cid" value="46"/>[북 (Alphabet) > 레슨 (A) > 카드(Today's Letter)]  <br/> 
          파라메터명(노트에서 입력한 텍스트) = note_noteText: <input type="text" name="note_noteText" id="note_noteText" value="울랄라랄랄라라라라라라라라라라라"/><br/>
          파라메터명(페이지는 안넘기면 1이 기본임.) = note_page: <input type="text" name="note_page" id="note_page" value="1"/><br/>
          <br/>
	          file1: <input type="file" name="uploadfile" id="uploadfile" value="" /><br/>
	          file2: <input type="file" name="uploadfile" id="uploadfile" value="" /><br/>
          </form>
          
          <p>데이터 결과 값:<br/><b id="message"></b></p>
      </p>
    <hr/>
    
    
     <a href="javascript: void(0);" onClick="javascript: portFolioInsert();"><b>[IF]$25-1 포트폴리오 파일 업로드 요청. </b></a>
      <p>
      <b>
      호출하는 url: http://10.30.0.246:9090/tree/upload/portFolioInsert.do <br/>
      multipart/form-data로 호출해야하고, file이 넘어오는 파라메터명은 uploadfile 입니다. 용량제한은 10Mb로 걸려있슴다. <br/>
      업로드 되는 서버의 위치는   /home/treeAdmin/upfiledir/portfolio 이며, 파일명은 임의로 생성하고 있어요. <br/>
      </b>
      
          <form id="portForm" name="portForm" method="post" action="<%=webRoot%>/upload/portFolioInsert.do" enctype="multipart/form-data" accept-charset="utf-8">
          파라메터명(클래스 순번) = clsSeq <input type="text" name="clsSeq" id="clsSeq" value="5"/><br/>
          파라메터명(수업 순번) = clsSchdlSeq <input type="text" name="clsSchdlSeq" id="clsSchdlSeq" value="6"/><br/>
          파라메터명(카드 순번) = prodSeq <input type="text" name="prodSeq" id="prodSeq" value="6"/><br/><br/>
          파라메터명(파일) = uploadfile <input type="file" name="uploadfile" id="uploadfile" value="" /><br/>
          </form>
          <p>데이터 결과 값:<br/><b id="message"></b></p>
      </p>
    <hr/>
    
    <a href="javascript: void(0);" onClick="javascript: getNoteData();"><b>[IF] 노트 정보 및 파일 다운로드 목록 요청 </b></a>
      <p>
      <b>
      호출하는 url: http://10.30.0.246:9090/tree/upload/getNoteData.do <br/>
      [ 넘어온 데이터 설명 ]
      노트정보는 paramMap.noteInfo 객체에 들어있고, 파일목록은 result에 목록으로 들어있어요. <br/>
      파일 목록 중에서 filePath가 실제 서버의 파일 경로이고, filePath 앞에 서버의 url을 넣어주면 다운로드 가능합니다. <br/>
      ex: http://10.30.0.246:9090/upfiledir/note/1419138675.jpg <br/>
      FILE_TYPE : FT001(IMAGE), FT002 (AUDIO), FT003 (VIDEO) , FT999(기타)
      
      </b>
          파라메터명(노트 순번 ) = note_cid <input type="text" name="note_cid" id="note_cid2" value=""/><br/>
          파라메터명(카드 순번 ) = note_prodSeq <input type="text" name="note_prodSeq" id="note_prodSeq2" value="45"/><br/>
          파라메터명(회원 아이디 ) = note_memberId <input type="text" name="note_memberId" id="note_memberId2" value="s1"/><br/>
          파라메터명(노트 현재 페이지 ** 기본1페이지이며, 추후 사용될 파라메터 같아서 추가함. ) = note_curPage <input type="text" name="note_curPage2" id="note_curPage" value="1"/><br/><br/>
          <p>데이터 결과 값:<br/><b id="note_result"></b></p>
      </p>
    <hr/>
    
    <a href="javascript: void(0);" onClick="javascript: onlyFileInsert();"><b>[IF] 파일만 업로드 하는 것.  </b></a>
      <p>
      
      호출하는 url: http://10.30.0.246:9090/tree/upload/onlyFileUpload.do <br/>
      올라간 파일에 접근 하는 방법 : http://10.30.0.246:9090/upfiledir/test/1419138675.jpg <br/>
      *** 올라가는 파일명은 임의로 바뀌게 되므로, 실제 파일은 서버에서 확인할수 있음.<br/>
      *** 해당 IF는 파일만 여러건 서버로 업로드 되며, 테이블에 어떤 입력도 하지 않고 있음.<br/>
      
          <form id="testForm" name="testForm" method="post" action="<%=webRoot%>/upload/onlyFileUpload.do" enctype="multipart/form-data" accept-charset="utf-8">
          파라메터명(파일) = uploadfile <input type="file" name="uploadfile" id="uploadfile" value="" /><br/>
          파라메터명(파일) = uploadfile <input type="file" name="uploadfile" id="uploadfile" value="" /><br/>
          </form>
    <hr/>
        
    <br/><br/>
    <font class="rainbow">[NEW]</font>
    <a href="/tree/content/ProdInfoInsert.do" ><font color="">**  [IF] 카드(액티비티) 메타 정보 업로드 하러 가기!  </font></a>
    <br/><br/>
    <hr/>
            
        
    
    
    <a href="javascript: void(0);" onClick="javascript: getNoteLessonList();"><b>[IF] My note > Lesson 목록 조회  </b></a>
      <p>
      <b>
      </b>
          파라메터명(회원 아이디 ) = note_memberId <input type="text" name="note_memberId" id="note_memberId3" value="al001"/><br/>
          <p>데이터 결과 값:<br/><b id="note_result3"></b></p>
      </p>
    <hr/>
    
    <a href="javascript: void(0);" onClick="javascript: getNoteList();"><b>[IF] My note > Lesson 별 노트 목록 조회  </b></a>
      <p>
      <b>
      </b>
          파라메터명(회원 아이디 ) = note_memberId <input type="text" name="note_memberId" id="note_memberId4" value="al001"/><br/>
          파라메터명(레슨 아이디 ) = note_lessonId <input type="text" name="note_lessonCd" id="note_lessonCd" value="33"/><br/>
          <p>데이터 결과 값:<br/><b id="note_result4"></b></p>
      </p>
    <hr/>    
    
    
    
    <a href="javascript: void(0);" onClick="javascript: addDataFirstLesson();"><b>[IF] TODAY_CLASS INSERT (클래스에 프로그램 등록시 해당 책의 첫레슨/첫차시를 TODAY_CLASS 테이블에 등록해준다.)  </b></a>
      <p>
      <b>
      ex 클래스SEQ : booster class (1), alphabet class(2) , 한류 class(3)
      </b>
          파라메터명(클래스 SEQ) = clsSeq <input type="text" name="clsSeq11" id="clsSeq11" value=""/><br/>
          <p>데이터 결과 값:<br/><b id="result11"></b></p>
      </p>
    <hr/>

    <a href="javascript: void(0);" onClick="javascript: modifyClassNextSchedule();"><b>[IF] 수업 종료 (다음 차시 코드 세팅)</b></a>
      <p>
      <b>
      ex 클래스SEQ : booster class (1), alphabet class(2), 한류 class(3)
      </b>
          파라메터명(클래스 SEQ) = clsSeq <input type="text" name="clsSeq12" id="clsSeq12" value=""/><br/>
          <p>데이터 결과 값:<br/><b id="result12"></b></p>
      </p>
    <hr/>


    <hr/>

    <br/><br/><br/><br/>
        
    
    
    
    
      <a href="http://14.49.42.98:8080/digitalbook/writing/getMaxWritingStep?userId=3"><b>사용자별 글쓰기 단계 제한값 취득</b></a><p>
      <b>  *담임교사가 지정한 반별 글쓰기 단계를 취득해오는 연동규격입니다. 4 ~ 6의 값이 취득됩니다.</b></p>
    <hr/>
        <b>글쓰기 저장</b><p>
        *글쓰기 저장 시의 연동규격입니다. prewriting ~ final Draft까지 저장시에 사용합니다.</p>
        userId, unitId, seq는 항시 들어와야 하는 값이고, 그외는 글쓰기 단계에 맞춰들어오는 선택적인 값입니다.</p>
        <form name="nform" id="nform" method="post" enctype="multipart/form-data" action="http://14.49.42.98:8080/digitalbook/writing/save">
            userId:<input type="text" name="userId" value="3"><b>  *userId는 숫자로 이루어져 있습니다. <font color="red">1 ~ 5</font>중에 골라 사용합니다.</b></p>
            unitId:<input type="text" name="unitId" value="10"><b>  *unitId는 <font color="red">10으로 고정</font>합니다.</b></p>
            seq:<input type="text" name="seq" value="0"><b>  *seq는 prewriting일 경우 0, 그이후로 단계별로 1씩 증가된 값 입니다.</b></p>
            subject:<input type="text" name="subject" value="제목"><p>
            words:<input type="text" name="words" value="단어1,단어2,단어3"><b>  *단어는 사용자가 입력한 단어를 구분자 ',(콤마)'를 이용하여 연결한 문자열입니다.</b></p>
            imagemapData:<input type="text" name="imagemapData"><p>
            sampleMapFlg:<input type="text" name="sampleMapFlg"><p>
            title:<input type="text" name="title" value="Who am I?"><p>
            text:<input type="text" name="text" value="I am a boy. you are a girl."><p>
            <input Type="submit" value="저장"><p>
        </form>

</body>
</html>
