<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>Tree</title>
	<link type='text/css' rel="stylesheet" href="${pageContext.request.contextPath}/common/css/common.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery-ui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/design.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){

			$(".btnRed").click(function(){

				$(location).attr('href',"${pageContext.request.contextPath}/sampleInsert.do");
			});

		});


		function sampleInfo(id) {

			$("#mbrId").val(id);

			$("#listForm").attr("action", "${pageContext.request.contextPath}/sampleInfo.do");
			$("#insertForm").attr("method", "POST");
			$("#listForm").submit();
		}
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%-- <%@ include file="include/incAdmHeader.jsp" %> --%>
	<!-- //header -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Administrator</h2>
		</div>
		<form name="listForm" id="listForm" method="post" action="">
		<input type="hidden" name="mbrId" id="mbrId" />
		<!-- selectArea -->
		<div class="selectArea">
			<select name="" id="">
				<option value="">User Type</option>
			</select>
			<select name="" id="">
				<option value="">대치 Campus</option>
			</select>
			<select name="" id="">
				<option value="">Status</option>
			</select>

			<input type="text" class="text" style="width: 120px;" value="Name/ID 검색" />

			<a href="javascript:void(0);" class="btnGray"><span><img src="${pageContext.request.contextPath}/images/common/icon/zoom.gif" alt="" />Search</span></a>
			<a href="javascript:void(0);" class="btnRed"><span>+ New</span></a>
		</div>
		<!-- //selectArea -->

		<!-- 리스트 타입 -->
		<div class="tbList1">
			<table>
				<colgroup>
					<col width="6%" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="6%" />
				</colgroup>

				<thead>
					<tr>
						<th>NO</th>
						<th><a href="">User Type ▲</a></th>
						<th>ID</th>
						<th>소속</th>
						<th>Name</th>
						<th>연락처</th>
						<th>Status</th>
						<th><a href="">등록일▼</a></th>
						<th>View</th>
					</tr>
				</thead>

				<tbody>
					<c:if test="${empty sampleList}">
						<tr>
							<td colspan="9">조회된 데이터가 없습니다.</td>
						</tr>
					</c:if>
					<c:if test="${!empty sampleList}">
						<c:forEach var="item" items="${sampleList}" varStatus="status">
							<tr>
								<td>${item.rn}</td>
								<td><strong>SuperAdmin</strong></td>
								<td>${item.nickName}</td>
								<td>본사</td>
								<td>${item.nm}</td>
								<td>${item.telNo}</td>
								<td>
									<c:choose>
										<c:when test="${item.useYn == 'Y'}"><span class="txtBlue">Active</span></c:when>
										<c:otherwise><span class="txtRed">Inactive</span></c:otherwise>
									</c:choose>
								</td>
								<td>${item.regDttm}</td>
								<td><a href="javascript:sampleInfo('${item.mbrId}');" class="btnBlue"><span>view</span></a></td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //리스트 타입 -->

		<div class="paging">
			<a href="" class="btn"><img src="${pageContext.request.contextPath}/images/common/btn/first.gif" alt="first" /></a><a href="" class="btn"><img src="${pageContext.request.contextPath}/images/common/btn/prev.gif" alt="prev" /></a>
			<span>
				<a href="" class="on">1</a><a href="">10</a><a href="">1</a><a href="">1</a>
			</span>
			<a href="" class="btn"><img src="${pageContext.request.contextPath}/images/common/btn/next.gif" alt="next" /></a><a href="" class="btn"><img src="${pageContext.request.contextPath}/images/common/btn/last.gif" alt="last" /></a>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>