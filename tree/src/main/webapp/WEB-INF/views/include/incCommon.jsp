<%/*
--------------------------------------------------------------------------------
    PROJECT NAME : nuri
--------------------------------------------------------------------------------
    - 단위업무명 : incCommon
    - 파일명    : incCommon.jsp
    - 서비스명   : 공통 프로그램
    - 처리설명   : 모든 jsp에서 include되는 파일
    - 최초작성일 : 2014-02-05
    - 관련사항   : 
    - 작성자     : hong
    - 소속       : 비상ESL 
    - 비고       : 
--------------------------------------------------------------------------------
    - 수정일자   :  
    - 수정자     : 
    - 소속       : 
    - 수정내용   : 
--------------------------------------------------------------------------------
*/%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.springframework.web.bind.ServletRequestUtils"%>
<%@ page import="java.util.*" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<%
	String webRoot = request.getContextPath();
	request.setCharacterEncoding("UTF-8");
	
	String JS_PATH              = webRoot+"/js"; 
	String IMG_PATH             = webRoot+"/images";
	String CSS_PATH             = webRoot+"/common/css";
	String MAIN_TITLE           = "NURI";

%>