/* LNB 리사이징 */
$(function() {
	var header = $('#header').height();
	winResize();

	$(window).resize(function() {
		winResize();
	});

	function winResize() {
		var docH = $(document).height() - header;
		$('#lnb').css('height', docH+'px');
	}
})

$(function() {
	$( ".datepicker" ).datepicker({
		dateFormat: "yy-mm-dd",
		dayNamesMin: ['일','월','화','수','목','금','토'],
		showMonthAfterYear: true,
		monthNames: [ "1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월" ]
	});
});