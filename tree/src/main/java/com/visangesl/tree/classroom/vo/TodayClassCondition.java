package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.vo.VSObject;

public class TodayClassCondition extends VSObject {

	private String mbrId;
    private String clsSeq;
    private String dayNoCd;
    private String sortOrd;
    private String nextDayNoCd;

	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String sortOrd) {
		this.sortOrd = sortOrd;
	}
    public String getNextDayNoCd() {
        return nextDayNoCd;
    }
    public void setNextDayNoCd(String nextDayNoCd) {
        this.nextDayNoCd = nextDayNoCd;
    }


}
