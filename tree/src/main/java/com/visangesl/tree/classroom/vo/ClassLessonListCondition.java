package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.vo.VSObject;

public class ClassLessonListCondition extends VSObject {

    private String clsSeq;


    public String getClsSeq() {
        return clsSeq;
    }

    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }



}
