package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class ClassRoomVo extends VSObject {

    private String mbrId;
    private String mbrGrade;
    private String nm;
    private String profilePhotoPath;
    private String portfolioNewCnt;

    private String prodSeq;
    private String prodTitle;
    private String thmbPath;
    private String bookCd;
    private String bookNm;
    private String lessonCd;
    private String dayNoCd;
    private String mdlCd;
    private String curriCd;

    private String clsSeq;
    private String clsNm;
    private String curriNo;
    private String campSeq;
    private String regDttm;
    private String regId;
    private String modDttm;
    private String modId;
    private String inclsStartDt;
    private String inclsEndDt;
    private String dtlCntt;
    private String useYn;
    private String apprvYn;

    private String inclsYmd;
    private String inclsStartHm;
    private String inclsEndHm;
    private String ingStart;
    private String inclsWk;
    private String inclsWkNm;
    private String wk;
    private String inclsIngYn;
    private String dayNo;

    private String lessonNm;




    //portfolio
    private String inclsRsltSeq;
    private String okYn;
    private String inclsRsltImg;
    private String inclsRsltThmb;
    private String ActivitySeq;
    private String ActivityNm;

    private String likeCnt;
    private String postCnt;
    private String badgeCnt;
    private String badgeCd;
    private String badgeImg;

    private String campNm;


    private String portfolioOkYn;
    private String portfolioImgPath;
    private String portfolioThumbImgPath;
    private String cardSeq;
    private String cardNm;
    private String cmmtCnt;
    private String badgeSeq;
    private String badgeImgPath;


    // 숙제
    private String hWorkSeq;
    private String hWorkOkYn;
    private String hWorkCnt;


    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getProfilePhotoPath() {
        if (profilePhotoPath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotoPath;
        } else {
            return profilePhotoPath;
        }
//        return Tree_Constant.SITE_FULL_URL + profilePhotoPath;
    }
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }
    public String getPortfolioNewCnt() {
        return portfolioNewCnt;
    }
    public void setPortfolioNewCnt(String portfolioNewCnt) {
        this.portfolioNewCnt = portfolioNewCnt;
    }
    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getThmbPath() {
        if (thmbPath != null) {
            return Tree_Constant.SITE_FULL_URL + thmbPath;
        } else {
            return thmbPath;
        }
//        return Tree_Constant.SITE_FULL_URL + thmbPath;
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
    public String getMdlCd() {
        return mdlCd;
    }
    public void setMdlCd(String mdlCd) {
        this.mdlCd = mdlCd;
    }
    public String getCurriCd() {
        return curriCd;
    }
    public void setCurriCd(String curriCd) {
        this.curriCd = curriCd;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getCurriNo() {
        return curriNo;
    }
    public void setCurriNo(String curriNo) {
        this.curriNo = curriNo;
    }
    public String getCampSeq() {
        return campSeq;
    }
    public void setCampSeq(String campSeq) {
        this.campSeq = campSeq;
    }
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getModDttm() {
        return modDttm;
    }
    public void setModDttm(String modDttm) {
        this.modDttm = modDttm;
    }
    public String getModId() {
        return modId;
    }
    public void setModId(String modId) {
        this.modId = modId;
    }
    public String getInclsStartDt() {
        return inclsStartDt;
    }
    public void setInclsStartDt(String inclsStartDt) {
        this.inclsStartDt = inclsStartDt;
    }
    public String getInclsEndDt() {
        return inclsEndDt;
    }
    public void setInclsEndDt(String inclsEndDt) {
        this.inclsEndDt = inclsEndDt;
    }
    public String getDtlCntt() {
        return dtlCntt;
    }
    public void setDtlCntt(String dtlCntt) {
        this.dtlCntt = dtlCntt;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getApprvYn() {
        return apprvYn;
    }
    public void setApprvYn(String apprvYn) {
        this.apprvYn = apprvYn;
    }
    public String getInclsYmd() {
        return inclsYmd;
    }
    public void setInclsYmd(String inclsYmd) {
        this.inclsYmd = inclsYmd;
    }
    public String getInclsStartHm() {
        return inclsStartHm;
    }
    public void setInclsStartHm(String inclsStartHm) {
        this.inclsStartHm = inclsStartHm;
    }
    public String getInclsEndHm() {
        return inclsEndHm;
    }
    public void setInclsEndHm(String inclsEndHm) {
        this.inclsEndHm = inclsEndHm;
    }
    public String getIngStart() {
        return ingStart;
    }
    public void setIngStart(String ingStart) {
        this.ingStart = ingStart;
    }
    public String getWk() {
        return wk;
    }
    public void setWk(String wk) {
        this.wk = wk;
    }
    public String getInclsIngYn() {
        return inclsIngYn;
    }
    public void setInclsIngYn(String inclsIngYn) {
        this.inclsIngYn = inclsIngYn;
    }
    public String getDayNo() {
        return dayNo;
    }
    public void setDayNo(String dayNo) {
        this.dayNo = dayNo;
    }
    public String getLessonNm() {
        return lessonNm;
    }
    public void setLessonNm(String lessonNm) {
        this.lessonNm = lessonNm;
    }
    public String getInclsRsltSeq() {
        return inclsRsltSeq;
    }
    public void setInclsRsltSeq(String inclsRsltSeq) {
        this.inclsRsltSeq = inclsRsltSeq;
    }
    public String getOkYn() {
        return okYn;
    }
    public void setOkYn(String okYn) {
        this.okYn = okYn;
    }
    public String getInclsRsltImg() {
        return inclsRsltImg;
    }
    public void setInclsRsltImg(String inclsRsltImg) {
        this.inclsRsltImg = inclsRsltImg;
    }
    public String getInclsRsltThmb() {
        return inclsRsltThmb;
    }
    public void setInclsRsltThmb(String inclsRsltThmb) {
        this.inclsRsltThmb = inclsRsltThmb;
    }
    public String getActivitySeq() {
        return ActivitySeq;
    }
    public void setActivitySeq(String activitySeq) {
        ActivitySeq = activitySeq;
    }
    public String getActivityNm() {
        return ActivityNm;
    }
    public void setActivityNm(String activityNm) {
        ActivityNm = activityNm;
    }
    public String getLikeCnt() {
        return likeCnt;
    }
    public void setLikeCnt(String likeCnt) {
        this.likeCnt = likeCnt;
    }
    public String getPostCnt() {
        return postCnt;
    }
    public void setPostCnt(String postCnt) {
        this.postCnt = postCnt;
    }
    public String getBadgeCnt() {
        return badgeCnt;
    }
    public void setBadgeCnt(String badgeCnt) {
        this.badgeCnt = badgeCnt;
    }
    public String getBadgeCd() {
        return badgeCd;
    }
    public void setBadgeCd(String badgeCd) {
        this.badgeCd = badgeCd;
    }
    public String getBadgeImg() {
        return badgeImg;
    }
    public void setBadgeImg(String badgeImg) {
        this.badgeImg = badgeImg;
    }
    public String getCampNm() {
        return campNm;
    }
    public void setCampNm(String campNm) {
        this.campNm = campNm;
    }
    public String getPortfolioOkYn() {
        return portfolioOkYn;
    }
    public void setPortfolioOkYn(String portfolioOkYn) {
        this.portfolioOkYn = portfolioOkYn;
    }
    public String getPortfolioImgPath() {
        if (portfolioImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + portfolioImgPath;
        } else {
            return portfolioImgPath;
        }
//        return Tree_Constant.SITE_FULL_URL + portfolioImgPath;
    }
    public void setPortfolioImgPath(String portfolioImgPath) {
        this.portfolioImgPath = portfolioImgPath;
    }
    public String getPortfolioThumbImgPath() {
        if (portfolioThumbImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + portfolioThumbImgPath;
        } else {
            return portfolioThumbImgPath;
        }
//        return Tree_Constant.SITE_FULL_URL + portfolioThumbImgPath;
    }
    public void setPortfolioThumbImgPath(String portfolioThumbImgPath) {
        this.portfolioThumbImgPath = portfolioThumbImgPath;
    }
    public String getCardSeq() {
        return cardSeq;
    }
    public void setCardSeq(String cardSeq) {
        this.cardSeq = cardSeq;
    }
    public String getCardNm() {
        return cardNm;
    }
    public void setCardNm(String cardNm) {
        this.cardNm = cardNm;
    }
    public String getCmmtCnt() {
        return cmmtCnt;
    }
    public void setCmmtCnt(String cmmtCnt) {
        this.cmmtCnt = cmmtCnt;
    }
    public String getBadgeSeq() {
        return badgeSeq;
    }
    public void setBadgeSeq(String badgeSeq) {
        this.badgeSeq = badgeSeq;
    }
    public String getBadgeImgPath() {
        if (badgeImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + badgeImgPath;
        } else {
            return badgeImgPath;
        }
//        return Tree_Constant.SITE_FULL_URL + badgeImgPath;
    }
    public void setBadgeImgPath(String badgeImgPath) {
        this.badgeImgPath = badgeImgPath;
    }
    public String gethWorkSeq() {
        return hWorkSeq;
    }
    public void sethWorkSeq(String hWorkSeq) {
        this.hWorkSeq = hWorkSeq;
    }
    public String gethWorkOkYn() {
        return hWorkOkYn;
    }
    public void sethWorkOkYn(String hWorkOkYn) {
        this.hWorkOkYn = hWorkOkYn;
    }
    public String gethWorkCnt() {
        return hWorkCnt;
    }
    public void sethWorkCnt(String hWorkCnt) {
        this.hWorkCnt = hWorkCnt;
    }
    public String getBookNm() {
        return bookNm;
    }
    public void setBookNm(String bookNm) {
        this.bookNm = bookNm;
    }
    public String getInclsWk() {
        return inclsWk;
    }
    public void setInclsWk(String inclsWk) {
        this.inclsWk = inclsWk;
    }
    public String getInclsWkNm() {
        return inclsWkNm;
    }
    public void setInclsWkNm(String inclsWkNm) {
        this.inclsWkNm = inclsWkNm;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }







}
