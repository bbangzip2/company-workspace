package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class ActivityVo extends VSObject {

    private String cardSeq;
    private String cardNm;
    private String cardStudyType;
    private String cardViewType;
    private String cardType;
    private String cardRunTime;
    private String cardSkill;
    private String cardLevel;
    private String cardEditable;
    private String cardSharable;
    private String cardKeyword;
    private String thmbPath;

    private String lessonCd;
    private String lessonNm;

    private String prodSeq;
    private String prodTitle;
    private String dayNo;


    public String getCardSeq() {
        return cardSeq;
    }
    public void setCardSeq(String cardSeq) {
        this.cardSeq = cardSeq;
    }
    public String getCardNm() {
        return cardNm;
    }
    public void setCardNm(String cardNm) {
        this.cardNm = cardNm;
    }
    public String getCardStudyType() {
        return cardStudyType;
    }
    public void setCardStudyType(String cardStudyType) {
        this.cardStudyType = cardStudyType;
    }
    public String getCardViewType() {
        return cardViewType;
    }
    public void setCardViewType(String cardViewType) {
        this.cardViewType = cardViewType;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardRunTime() {
        return cardRunTime;
    }
    public void setCardRunTime(String cardRunTime) {
        this.cardRunTime = cardRunTime;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getCardEditable() {
        return cardEditable;
    }
    public void setCardEditable(String cardEditable) {
        this.cardEditable = cardEditable;
    }
    public String getCardSharable() {
        return cardSharable;
    }
    public void setCardSharable(String cardSharable) {
        this.cardSharable = cardSharable;
    }
    public String getCardKeyword() {
        return cardKeyword;
    }
    public void setCardKeyword(String cardKeyword) {
        this.cardKeyword = cardKeyword;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getLessonNm() {
        return lessonNm;
    }
    public void setLessonNm(String lessonNm) {
        this.lessonNm = lessonNm;
    }
    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getDayNo() {
        return dayNo;
    }
    public void setDayNo(String dayNo) {
        this.dayNo = dayNo;
    }
    public String getThmbPath() {
        if (thmbPath != null) {
            return Tree_Constant.SITE_FULL_URL + thmbPath;
        } else {
            return thmbPath;
        }
//        return thmbPath;
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }



}
