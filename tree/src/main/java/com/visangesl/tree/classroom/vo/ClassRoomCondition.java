package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.vo.VSListCondition;

public class ClassRoomCondition extends VSListCondition {

    private String clsSeq;
    private String clsNm;
    private String mbrId;
    private String nm;
    private String mbrGrade;

    private String inclsYmd;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getInclsYmd() {
        return inclsYmd;
    }
    public void setInclsYmd(String inclsYmd) {
        this.inclsYmd = inclsYmd;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }


}
