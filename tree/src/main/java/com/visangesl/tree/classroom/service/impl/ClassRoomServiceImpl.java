package com.visangesl.tree.classroom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.classroom.service.ClassRoomService;
import com.visangesl.tree.classroom.vo.ClassLessonListCondition;
import com.visangesl.tree.classroom.vo.ClassRoomCondition;
import com.visangesl.tree.classroom.vo.TodayClassCondition;
import com.visangesl.tree.classroom.vo.TodayClassVo;
import com.visangesl.tree.mapper.ClassRoomMapper;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public class ClassRoomServiceImpl implements ClassRoomService {
    @Autowired
    ClassRoomMapper classRoomMapper;

    @Override
    public List<VSObject> getClassSimpleList(ClassRoomCondition classRoomCondition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassSimpleList(classRoomCondition);
    }

    @Override
    public List<VSObject> getClassMemberSimpleList(ClassRoomCondition classRoomCondition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassMemberSimpleList(classRoomCondition);
    }

    @Override
    public VSObject getClassRelayIp(VSCondition vsCondition) throws Exception {
    	return classRoomMapper.getClassRelayIp(vsCondition);
    }

    @Override
    public VSResult<VSObject> mergeClassRelayIp(VSObject vsObject) throws Exception {
    	VSResult<VSObject> result = new VSResult<VSObject>();

    	if (vsObject == null) {
    		result.setCode("9999");
    		result.setMessage("실패");
    	} else {
        	int affectedRows = classRoomMapper.mergeClassRelayIp(vsObject);

            if (affectedRows > 0) {
                result.setCode("0000");
    			result.setMessage("성공");
    		} else {
        		result.setCode("9999");
        		result.setMessage("실패");
            }
    	}

        return result;

    }

    @Override
    public List<VSObject> getTodayClassesList(ClassRoomCondition classRoomCondition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getTodayClassesList(classRoomCondition);
    }

    @Override
    public List<VSObject> getTeachingRoomList(ClassRoomCondition classRoomCondition) throws Exception {
        return classRoomMapper.getTeachingRoomList(classRoomCondition);
    }

    @Override
    public List<VSObject> getStudyRoomList(ClassRoomCondition classRoomCondition) throws Exception {
        return classRoomMapper.getStudyRoomList(classRoomCondition);
    }

    @Override
    public List<VSObject> getClassRoomList(ClassRoomCondition classRoomCondition) throws Exception {
        return classRoomMapper.getClassRoomList(classRoomCondition);
    }

    @Override
    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.getFirstLessonList(tcCond);
    }

    @Override
    public String getClassTeacherId(String clsSeq) throws Exception{
    	return classRoomMapper.getClassTeacherId(clsSeq);
	}

    @Override
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.getNextLessonDayNoCd(tcCond);
    }

    @Override
    public int addDataTodayClass(String clsSeq, String dayNoCd, String sortOrd) throws Exception {
    	TodayClassVo tcv = new TodayClassVo();
    	tcv.setClsSeq(clsSeq);
    	tcv.setDayNoCd(dayNoCd);
    	tcv.setSortOrd(sortOrd);
        return classRoomMapper.addDataTodayClass(tcv);

    }

    @Override
    public String getClassTodayDayNoCd(TodayClassCondition tcCond)
            throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassTodayDayNoCd(tcCond);
    }

    @Override
    public String getClassNextdayDayNoCd(TodayClassCondition tcCond)
            throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassNextdayDayNoCd(tcCond);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public int modifyClassDayNoCdInfo(TodayClassCondition tcCond) throws Exception {

        int result = 0;

        // 다음 차시 코드로 수정
        result = classRoomMapper.modifyClassTodayDayNoCd(tcCond);
        // 다음 레슨의 첫째 차시 코드 수정
        result = classRoomMapper.modifyClassNextLessonDayNoCd(tcCond);

        return result;
    }

    @Override
    public int deleteClassDayNoCd(TodayClassCondition tcCond) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.deleteClassDayNoCd(tcCond);
    }

    @Override
    public List<VSObject> getClassLessonList(
            ClassLessonListCondition classLessonListCondition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassLessonList(classLessonListCondition);
    }

}
