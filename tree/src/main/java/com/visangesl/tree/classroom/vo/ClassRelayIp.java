package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.vo.VSObject;

public class ClassRelayIp extends VSObject {
    private String clsSeq;
    private String ip;
    private String connStat;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getConnStat() {
        return connStat;
    }
    public void setConnStat(String connStat) {
        this.connStat = connStat;
    }
}
