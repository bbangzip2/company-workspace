package com.visangesl.tree.classroom.service;

import java.util.List;

import com.visangesl.tree.classroom.vo.ClassLessonListCondition;
import com.visangesl.tree.classroom.vo.ClassRoomCondition;
import com.visangesl.tree.classroom.vo.TodayClassCondition;
import com.visangesl.tree.classroom.vo.TodayClassVo;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

public interface ClassRoomService {

    public List<VSObject> getClassSimpleList(ClassRoomCondition classRoomCondition) throws Exception;
    public List<VSObject> getClassMemberSimpleList(ClassRoomCondition classRoomCondition) throws Exception;

    public VSObject getClassRelayIp(VSCondition vsCondition) throws Exception;
    public VSResult<VSObject> mergeClassRelayIp(VSObject vsObject) throws Exception;

    public List<VSObject> getTodayClassesList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<VSObject> getClassRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<VSObject> getTeachingRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<VSObject> getStudyRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception;

    public String getClassTeacherId(String clsSeq) throws Exception;

    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception;

    public int addDataTodayClass(String clsSeq, String dayNoCd, String sortOrd) throws Exception;

    public String getClassTodayDayNoCd(TodayClassCondition tcCond) throws Exception;

    public String getClassNextdayDayNoCd(TodayClassCondition tcCond) throws Exception;

    public int modifyClassDayNoCdInfo(TodayClassCondition tcCond) throws Exception;

    public int deleteClassDayNoCd(TodayClassCondition tcCond) throws Exception;

    public List<VSObject> getClassLessonList(ClassLessonListCondition classLessonListCondition) throws Exception;
}
