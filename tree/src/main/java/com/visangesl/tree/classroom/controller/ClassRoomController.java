package com.visangesl.tree.classroom.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.classroom.service.ClassRoomService;
import com.visangesl.tree.classroom.vo.ClassLessonListCondition;
import com.visangesl.tree.classroom.vo.ClassRelayIp;
import com.visangesl.tree.classroom.vo.ClassRelayIpCondition;
import com.visangesl.tree.classroom.vo.ClassRoomCondition;
import com.visangesl.tree.classroom.vo.ClassRoomVo;
import com.visangesl.tree.classroom.vo.TodayClassCondition;
import com.visangesl.tree.classroom.vo.TodayClassVo;
import com.visangesl.tree.contents.service.ContentsService;
import com.visangesl.tree.contents.vo.ContentsCondition;
import com.visangesl.tree.contents.vo.ContentsVo;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.service.MemberService;
import com.visangesl.tree.member.vo.Member;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.message.service.MessageService;
import com.visangesl.tree.message.vo.MessageVo;
import com.visangesl.tree.packaging.service.PackageService;
import com.visangesl.tree.portfolio.service.PortfolioService;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

/**
 * ClassRoomController
 * Version - 1.0
 * Copyright
 */
@Controller
public class ClassRoomController implements BaseController {
    @Autowired
    private ClassRoomService classRoomService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ContentsService contentsService;
    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private PackageService packageService;

    @Autowired
    CheckXSSService xsssvc;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * getClassSimpleList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassSimpleList", method=RequestMethod.POST)
    public VSResult getClassSimpleList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId) throws Exception {

        logger.debug("getClassRoomList");
        logger.debug(mbrId);

        VSResult result = new VSResult();

        // 로그인 정보 조회
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrGrade = userSession.getMbrGrade();

        try {
            ClassRoomCondition classRoomCondition = new ClassRoomCondition();
            classRoomCondition.setMbrId(mbrId);
            classRoomCondition.setMbrGrade(mbrGrade);

            List<VSObject> classRoomList = classRoomService.getClassSimpleList(classRoomCondition);

            result.setResult(classRoomList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * getClassMemberSimpleList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassMemberSimpleList", method=RequestMethod.POST)
    public VSResult getClassMemberSimpleList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "name", required = false, defaultValue = "") String nm
            ) throws Exception {

        logger.debug("getClassMemberSimpleList");
        logger.debug(mbrId);
        logger.debug(clsSeq);
        logger.debug(nm);

        VSResult result = new VSResult();

        // 로그인 정보 조회
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrGrade = userSession.getMbrGrade();

        try {
            ClassRoomCondition classRoomCondition = new ClassRoomCondition();
            classRoomCondition.setMbrId(mbrId);
            classRoomCondition.setClsSeq(clsSeq);
            classRoomCondition.setNm(nm);
            classRoomCondition.setMbrGrade(mbrGrade);

            List<VSObject> classRoomList = classRoomService.getClassMemberSimpleList(classRoomCondition);

            result.setResult(classRoomList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }


    @ResponseBody
    @RequestMapping(value="/treeMain")
    public VSResult getLauncherMainInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "todayYmd", required = false, defaultValue = "") String inclsYmd
            ) throws Exception {

        logger.debug("/treeMain");
        logger.debug("inclsYmd: " + inclsYmd);

        VSResult result = new VSResult();
        ClassRoomCondition classRoomCondition = new ClassRoomCondition();

        try {

            // 로그인 정보 조회
            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

            // 현재 단말 날짜 세션 저장
            if (userSession.getTodayYmd() == null && !inclsYmd.equals("")) {
                userSession.setTodayYmd(inclsYmd);
                session.setAttribute("VSUserSession", userSession);
            }

            if (!inclsYmd.equals("")) {
                // 로그인 회원 정보 조회
                String mbrGrade = userSession.getMbrGrade();
                String mbrId = userSession.getMemberId();


                // Member
                Member memberVo = new Member();
                memberVo.setMbrId(mbrId);
                memberVo = (Member)memberService.getData(memberVo);


                // Today's Classes List
                classRoomCondition.setMbrId(mbrId);
                classRoomCondition.setMbrGrade(mbrGrade);
                classRoomCondition.setInclsYmd(inclsYmd);
                List<VSObject> classRoomList = classRoomService.getTodayClassesList(classRoomCondition);


                // New Message Count
                MessageVo messageVo = new MessageVo();
                messageVo.setRecvMbrId(mbrId);
                int message = messageService.getNewMessageCount(messageVo);


                // result
                HashMap pMap = new HashMap();
                pMap.put("member", memberVo);
                pMap.put("classes", classRoomList);
//                pMap.put("classes", classList);
                pMap.put("message", message);


                // 회원 등급 체크
                if (mbrGrade.equals("MG004")) {    // 강사
                    // New Materials
                    String materials = "43";

                    pMap.put("materials", materials);

                } else if (mbrGrade.equals("MG005")) { // 학생
                    // Not Submitted Homework Count
                    String homework = "5";
                    pMap.put("homework", homework);
                }

                result.setResult(pMap);
                result.setCode("0000");
                result.setMessage("성공");
            } else {
                result.setCode("9998");
                result.setMessage("파라미터 확인!");
            }

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value="/treeMain/classes")
    public VSResult getTodayClasses(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "todayYmd", required = false, defaultValue = "") String inclsYmd
            ) throws Exception {

        logger.debug("/treeMain/classes");
        logger.debug("inclsYmd: " + inclsYmd);

        VSResult result = new VSResult();
        ClassRoomCondition classRoomCondition = new ClassRoomCondition();

        try {

            if (!inclsYmd.equals("")) {
                // 로그인 정보 조회
                HttpSession session = request.getSession();
                UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

                // 로그인 회원 정보 조회
                String mbrGrade = userSession.getMbrGrade();
                String mbrId = userSession.getMemberId();

                // Today's Classes List
                classRoomCondition.setMbrId(mbrId);
                classRoomCondition.setMbrGrade(mbrGrade);
                classRoomCondition.setInclsYmd(inclsYmd);
                List<VSObject> classRoomList = classRoomService.getTodayClassesList(classRoomCondition);


                // result
                HashMap pMap = new HashMap();
                pMap.put("classes", classRoomList);
//                pMap.put("classes", classList);

                result.setResult(pMap);
                result.setCode("0000");
                result.setMessage("성공");
            } else {
                result.setCode("9998");
                result.setMessage("파라미터 확인!");
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value="/classroom/dayList")
    public VSResult getLessonDayList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "tmemberId", required = false, defaultValue = "") String tMbrId,
            @RequestParam(value = "mbrGrade", required = false, defaultValue = "") String mbrGrade ) throws Exception {

        VSResult result = new VSResult();

        try {

            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

            HashMap pMap = new HashMap();


            String memberGrade = userSession.getMbrGrade();

            // 세션에 값이 없다면 넘어온 파라메터로 적용한다.
            if(memberGrade.equals("")){
            	memberGrade = mbrGrade;
            }

            logger.debug("파라메터를 확인합니다.!!!!!! 쳌잇!");
            logger.debug("userSession.getMemberId()= "+userSession.getMemberId());
            logger.debug("memberGrade = "+memberGrade);
            logger.debug("lessonCd = "+lessonCd);
            logger.debug("tMbrId = "+tMbrId);
            logger.debug("파라메터를 확인 END !");

            // 학생인 경우도 memberId


            // 학생인 경우도 memberId

            ContentsCondition contentsCondition = new ContentsCondition();
            contentsCondition.setMbrId(userSession.getMemberId());
            contentsCondition.setMbrGrade(memberGrade);
            contentsCondition.setLessonCd(lessonCd);
            contentsCondition.settMbrId(tMbrId); // 학생의 경우 선생님 아이디를 파라멭로 받는걸로 수정함 2014-04-02 이홍

            List<VSObject> cardMapList = contentsService.getDayDataList(contentsCondition);

            // 최초 기본 패키지를 다운 받은 경우에는 기본 정보를 조회
            if (cardMapList == null || cardMapList.size() == 0) {
                contentsCondition.setMbrId("admin");
                cardMapList = contentsService.getDayDataList(contentsCondition);
            }

            pMap.put("dayList", cardMapList);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(pMap);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/classroom/cardMapList")
    public VSResult getLessonCardMapList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNoCd,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "classLocalIp", required = false, defaultValue = "") String classLocalIp
            ) throws Exception {

        VSResult result = new VSResult();

        try {

            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

            // 강사인 경우 강사의 local IP를 서버에 저장한다.
            if (userSession.getMbrGrade() != null && userSession.getMbrGrade().equals("MG004")) {
                ClassRelayIp classRelayIp = new ClassRelayIp();
                if (!classLocalIp.equals("")) {
                    classRelayIp.setConnStat("C");
                    classRelayIp.setIp(classLocalIp);
                    classRelayIp.setClsSeq(clsSeq);
                    VSResult<VSObject> classRelayIpResult = new VSResult<VSObject>();
                    classRelayIpResult = classRoomService.mergeClassRelayIp(classRelayIp);
                    logger.debug("><><><><><><>< (카드맵) IP 저장 결과 = "+ classRelayIpResult.getCode() + " | " + classRelayIpResult.getMessage());
                }
            }

            // 레슨 정보 조회
            ContentsCondition lessonCond = new ContentsCondition();
            lessonCond.setMbrId(mbrId);
            lessonCond.setProdSeq(lessonCd);

            ContentsVo contentsVo = (ContentsVo) contentsService.getContentsInfoData(lessonCond);

            HashMap pMap = new HashMap();
            pMap.put("lessonInfo", contentsVo);

            ContentsCondition contentsCondition = new ContentsCondition();
            contentsCondition.setMbrId(mbrId);
            contentsCondition.setLessonCd(lessonCd);
            contentsCondition.setDayNoCd(dayNoCd);
            List<VSObject> cardMapList = contentsService.getCardMapDataList(contentsCondition);

            // 최초 기본 패키지를 다운 받은 경우에는 기본 카드맵을 조회
            if (cardMapList == null || cardMapList.size() == 0) {
                contentsCondition.setMbrId("admin");
                cardMapList = contentsService.getCardMapDataList(contentsCondition);
            }

            result.setCode("0000");
            result.setMessage("성공");
            result.setParamMap(pMap);
            result.setResult(cardMapList);

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/classroom/cardMapSubList")
    public VSResult getLessonCardMapSubList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNoCd
            ) throws Exception {

        VSResult result = new VSResult();

        try {

            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

            ContentsCondition contentsCondition = new ContentsCondition();
            contentsCondition.setMbrId(mbrId);
            contentsCondition.setLessonCd(lessonCd);
            contentsCondition.setDayNoCd(dayNoCd);
            contentsCondition.setCardGubun("MT000");
            List<VSObject> cardMapList = contentsService.getCardMapDataSubList(contentsCondition);

            // 최초 기본 패키지를 다운 받은 경우에는 기본 카드맵을 조회
            if (cardMapList == null || cardMapList.size() == 0) {
                contentsCondition.setMbrId("admin");
                cardMapList = contentsService.getCardMapDataSubList(contentsCondition);
            }

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(cardMapList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/classroom/getClassroomList")
    public VSResult getClassroomList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "sortKey", required = false, defaultValue = "") String sortKey,
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo
            ) throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        VSResult result = new VSResult();

        try {

            String mbrGrade = userSession.getMbrGrade();
            String mbrId = userSession.getMemberId();

            ClassRoomCondition classRoomCondition = new ClassRoomCondition();
            classRoomCondition.setMbrGrade(mbrGrade);
            classRoomCondition.setMbrId(mbrId);
            classRoomCondition.setSortKey(sortKey);
            classRoomCondition.setPageNo(pageNo);
            classRoomCondition.setPageSize(6);

            List<VSObject> classRoomList = classRoomService.getClassRoomList(classRoomCondition); // 티칭/스터디룸 공용
            List<VSObject> homeworkList = new ArrayList<VSObject>();


            // 홈워크가 없으므로.. 더미로 진행함.
            ClassRoomVo classRoomVo = new ClassRoomVo();
            if (mbrGrade.equals("MG005")) {
                // 숙제 상태
                classRoomVo = new ClassRoomVo();
                classRoomVo.setLessonCd("2");
                classRoomVo.setLessonNm("A-2 The Hat");
                classRoomVo.setCardSeq("6");
                classRoomVo.setCardNm("Sort the words");
                classRoomVo.setInclsStartDt("2014.02.21");
                classRoomVo.sethWorkSeq("6");
                classRoomVo.sethWorkOkYn("N");
                homeworkList.add(classRoomVo);

                classRoomVo = new ClassRoomVo();
                classRoomVo.setLessonCd("2");
                classRoomVo.setLessonNm("A-2 The Hat");
                classRoomVo.setCardSeq("5");
                classRoomVo.setCardNm("Sort the words");
                classRoomVo.setInclsStartDt("2014.02.21");
                classRoomVo.sethWorkSeq("5");
                classRoomVo.sethWorkOkYn("Y");
                homeworkList.add(classRoomVo);

                classRoomVo = new ClassRoomVo();
                classRoomVo.setLessonCd("2");
                classRoomVo.setLessonNm("A-2 The Hat");
                classRoomVo.setCardSeq("4");
                classRoomVo.setCardNm("Sort the words");
                classRoomVo.setInclsStartDt("2014.02.21");
                classRoomVo.sethWorkSeq("4");
                classRoomVo.sethWorkOkYn("Y");
                homeworkList.add(classRoomVo);

                classRoomVo = new ClassRoomVo();
                classRoomVo.setLessonCd("1");
                classRoomVo.setLessonNm("A-1Go Go Go");
                classRoomVo.setCardSeq("3");
                classRoomVo.setCardNm("Sort the words");
                classRoomVo.setInclsStartDt("2014.02.21");
                classRoomVo.sethWorkSeq("3");
                classRoomVo.sethWorkOkYn("N");
                homeworkList.add(classRoomVo);

                classRoomVo = new ClassRoomVo();
                classRoomVo.setLessonCd("1");
                classRoomVo.setLessonNm("A-1Go Go Go");
                classRoomVo.setCardSeq("2");
                classRoomVo.setCardNm("Sort the words");
                classRoomVo.setInclsStartDt("2014.02.21");
                classRoomVo.sethWorkSeq("2");
                classRoomVo.sethWorkOkYn("Y");
                homeworkList.add(classRoomVo);

                classRoomVo = new ClassRoomVo();
                classRoomVo.setLessonCd("1");
                classRoomVo.setLessonNm("A-1Go Go Go");
                classRoomVo.setCardSeq("1");
                classRoomVo.setCardNm("Sort the words");
                classRoomVo.setInclsStartDt("2014.02.21");
                classRoomVo.sethWorkSeq("1");
                classRoomVo.sethWorkOkYn("Y");
                homeworkList.add(classRoomVo);
            }

            // result
            HashMap pMap = new HashMap();
            pMap.put("classroom", classRoomList);
            pMap.put("homework", homeworkList);

            result.setResult(pMap);

            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }
        return result;

    }

    @ResponseBody
    @RequestMapping(value="/classroom/cardInfo")
    public VSResult getCardInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "prodSeq", required = false, defaultValue = "") String prodSeq) throws Exception {

        VSResult result = new VSResult();

        if (!prodSeq.equals("")) {

            try {
                HashMap pMap = new HashMap();
                ContentsCondition condition = new ContentsCondition();

                condition.setProdSeq(prodSeq);
                ContentsVo contentsVo = (ContentsVo) contentsService.getContentsInfoData(condition);

                // 카드 경로 패스를 추가로 삽입 해준다. (/bookCd/lessonCd/cardCd)
                //contentsVo.setCardPath(File.separator + contentsVo.getBookCd() + File.separator + contentsVo.getLessonCd() + File.separator + prodSeq);

                // booster 일 경우 예외처리
                if (contentsVo != null && contentsVo.getBookCd().equals("1")) {
                    contentsVo.setCardFullPath(contentsVo.getFilePath());
                }


                pMap.put("cardInfo", contentsVo);

                result.setCode("0000");
                result.setMessage("성공");
                result.setResult(pMap);
            } catch (Exception e) {
                ExceptionHandler handler = new ExceptionHandler(e);
                result.setCode(handler.getCode());
                result.setMessage(handler.getMessage());
                e.printStackTrace();
            }

        } else {
            result.setCode("9999");
            result.setMessage("필수 파라미터 누락");
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/classroom/cardSort")
    public VSResult modifyCardMapSortData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "dayCd", required = false, defaultValue = "") String daynoCd,
            @RequestParam(value = "sortData", required = false, defaultValue = "") String sortData) throws Exception {

        logger.debug("/classroom/cardSort");
        logger.debug(mbrId);
        logger.debug(daynoCd);
        logger.debug(sortData);

        ContentsCondition contentsCondition = new ContentsCondition();
        ContentsVo contents = new ContentsVo();
        VSResult result = new VSResult();
        boolean packageFlag = false;

        try {

            // 로그인 정보 조회
            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
            String mbrGrade = userSession.getMbrGrade();

            //권한 체크는 우선 학생만 아니면 진행하도록..
            if (!mbrGrade.equals("MG005") && !mbrId.equals("") && !daynoCd.equals("") && !sortData.equals("") && sortData.length() > 0) {
//            if (!mbrId.equals("") && !daynoCd.equals("") && !sortData.equals("") && sortData.length() > 0) {

                contentsCondition.setProdSeq(daynoCd);
                contents = (ContentsVo) contentsService.getContentsInfoData(contentsCondition); // 차시 코드의 북/레슨 코드 조회
                String bookCd = contents.getBookCd(); // 차시 코드의 북 코드
                String lessonCd = contents.getLessonCd(); // 차시 코드의 레슨 코드
                String bookPath = contents.getBookPath();
                String lessonPath = contents.getLessonPath();

                String[] cardSortData = sortData.split(","); // 카드 정렬 정보 배열 생성
                for (int i=0; i<cardSortData.length; i++) {
                    contentsCondition.setProdSeq(cardSortData[i]);
                    contents = (ContentsVo) contentsService.getContentsInfoData(contentsCondition); // 정렬 카드의 레슨 코드 조회

                    // 차시코드의 레슨 코드와 정렬 카드의 레슨 코드를 비교
                    if (!lessonCd.equals(contents.getLessonCd())) { // 같지 않으면 반복문 종료 후 패키징 작업 종료
                        logger.debug("차시 코드의 레슨 코드와 정렬 카드의 레슨 코드가 서로 다름");
                        packageFlag = true;
                        break;
                    }
                }

                // 차시코드의 레슨 코드와 정렬 카드의 레슨 코드가 같으면 계속 진행
                if (!packageFlag) {

                    List<VSObject> oldCardMapList;

                    // 기존 카드맵 정렬 데이터 조회
                    contentsCondition.setMbrId(mbrId);
                    contentsCondition.setDayNoCd(daynoCd);
                    contentsCondition.setBookCd(bookCd);
                    contentsCondition.setLessonCd(lessonCd);
                    contentsCondition.setMbrGrade(mbrGrade);

                    contentsCondition.setBookPath(bookPath);
                    contentsCondition.setLessonPath(lessonPath);
                    oldCardMapList = contentsService.getCardMapDataList(contentsCondition);

                    // 기존 카드맵 정렬 데이터가 없을 경우 관리자가 등록한 마스터 데이터를 조회
                    if (oldCardMapList == null) {
                        logger.debug("기존 카드맵 데이터가 없어 마스터 데이터 조회");
                        contentsCondition.setMbrId("admin");
                        oldCardMapList = contentsService.getCardMapDataList(contentsCondition);
                        packageFlag = true;
                    }

                    // 기존 카드맵 리스트 배열 처리
                    String[] oldCardMapArr = new String[oldCardMapList.size()];
                    for (int i=0; i<oldCardMapList.size(); i++) {
                        ContentsVo content = new ContentsVo();
                        content = (ContentsVo) oldCardMapList.get(i);
                        oldCardMapArr[i] = content.getProdSeq();
                    }

logger.debug("oldCardMapArr");
for (int i=0; i<oldCardMapArr.length; i++) {
    logger.debug("oldCardMapArr " + i + ": " + oldCardMapArr[i]);
}
logger.debug("cardSortData");
for (int i=0; i<cardSortData.length; i++) {
    logger.debug("cardSortData " + i + ": " + cardSortData[i]);
}

                    // 기존 카드맵 리스트와 신규 정렬 데이터가 같은지 비교
                    if (!Arrays.equals(oldCardMapArr, cardSortData)) {
                        // 기존 카드 정렬과 다르므로 다음 진행

                        // 기존 카드맵 리스트와 신규 정렬 데이터가 순서만 다른지 체크
                        boolean cardSortFlag = false;
                        String[] tempOldArr = oldCardMapArr.clone();
                        Arrays.sort(tempOldArr);
                        String[] tempNewArr = cardSortData.clone();
                        Arrays.sort(tempNewArr);
                        if (Arrays.equals(tempOldArr, tempNewArr)) {
                            cardSortFlag = true;
                            logger.debug("기존 카드맵과 순서만 다름");
                        }


                        // 마스터 데이터를 조회 해왔는지 체크 - 마스터 데이터가 아닐 경우
                        if (!packageFlag) {
                            // 정렬값 수정 전 기존 정렬값 초기화
                            contents.setMbrId(mbrId);
                            contentsService.modifyCardMapSortDataReset(contents);
                        }


                        boolean flag = false; // 기존 있던 카드인지 구분하기 위한 구분자
                        for (int i=0; i<cardSortData.length; i++) {
                            flag = false; // 구분자 초기화

                            String idx = i + 1 + "";
                            contents.setSort(idx);
                            contents.setDaynoCd(daynoCd);
                            contents.setCardCd(cardSortData[i]);

                            for (int j=0; j<oldCardMapArr.length; j++) {

                                // 기존 카드맵 리스트와 신규 카드맵 리스트 비교
                                if (oldCardMapArr[j].equals(cardSortData[i])) {
                                    flag = true;
                                    break;
                                }
                            }

                            // 마스터 데이터를 조회 해왔는지 체크
                            if (packageFlag) {
                                // 기존 데이터가 없어 마스터 데이터를 조회 해온 경우 수정없이 등록만 처리
                                contentsService.insertCardMapSortData(contents); // 등록
                            } else {
                                contentsService.insertCardMapSortData(contents); // 등록
//                                if (flag) { // 동일한 카드가 있는 경우
//                                    contentsService.modifyCardMapSortData(contents); // 업데이트
//                                } else { // 동일한 카드가 없는 경우
//                                    contentsService.insertCardMapSortData(contents); // 등록
//                                }
                            }
                        }

                        // 마스터 데이터를 조회 해왔는지 체크 - 마스터 데이터가 아닐 경우
                        if (!packageFlag) {
                            // 정렬값 등록/수정 후 제외된(정렬값이 없는) 카드 정보 삭제
                            contents.setMbrId(mbrId);
                            contentsService.deleteCardMapSortData(contents);
                        }


                        // 기존 카드맵 리스트와 신규 정렬 데이터가 순서만 다르면 패키징 패스
                        if (!cardSortFlag) {
                            logger.debug("패키징 메소드 호출");

                            //================================================================================
                            // 패키징 메소드 실행 (버전 업까지 실행~)
                            contents.setMbrId(mbrId);

                            if (packageService.packageZipper(contentsCondition)) {
                                logger.debug("0000_정상 완료");
                                result.setCode("0000");
                                result.setMessage("정상 완료");
                            } else {
                                result.setCode(TreeProperties.getProperty("error.fail.code"));
                                logger.debug("패키징 파일 생성 실패");
                                result.setMessage("패키징 파일 생성 실패");
                                throw new Exception("패키징 파일 생성 실패");
                            }
                            //================================================================================

                        } else {
                            logger.debug("0000_기존 카드맵과 순서만 다릅니다.");
                            result.setCode("0000");
                            result.setMessage("기존 카드맵과 순서만 다릅니다.");
                        }
                    } else {
                        // 기존 카드 정렬과 순서/종류 모두 같으므로 패키징 진행 X
                        logger.debug("0000_기존 카드맵과 동일 합니다.");
                        result.setCode("0000");
                        result.setMessage("기존 카드맵과 동일 합니다.");
                    }
                } else {
                    // 파라미터 차시코드의 레슨 코드와 정렬 카드의 레슨 코드가 다름
                    logger.debug("9001");
                    result.setCode("9001");
                    result.setMessage("파라미터 차시 코드와 정렬 카드의 레슨 코드가 다름");
                }
        	} else {
        	    logger.debug("9000");
        		result.setCode("9000");
                result.setMessage("필수 파라미터가 없습니다.");
        	}
        } catch (Exception e) {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage("등록 실패");
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/classroom/getClassRelayIp", method=RequestMethod.GET)
    public VSResult<Object> getClassRelayIp(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "classSeq", required = true) String classSeq
            ) throws Exception {

        VSResult result = new VSResult();

        try {
        	String clsSeq = classSeq;

            // 현재 class의 local IP를 결과값에 포함한다.
            ClassRelayIpCondition classRelayIpCondition = new ClassRelayIpCondition();
            classRelayIpCondition.setClsSeq(clsSeq);
            classRelayIpCondition.setConnStat("C");
            ClassRelayIp classRelayIpInfo = (ClassRelayIp) classRoomService.getClassRelayIp(classRelayIpCondition);

            if (classRelayIpInfo == null) {
                result.setCode("9999");
                result.setMessage("조회 오류");
                result.setResult(null);
            } else {
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
    			result.setResult(classRelayIpInfo);
            }

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 클래스의 첫번째 차시를 등록한다.
     * @param request
     * @param response
     * @param clsSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/addDataTodayClass", method=RequestMethod.POST)
    public VSResult addDataTodayClass(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq) throws Exception {

        logger.debug("addDataTodayClass");

        VSResult result = new VSResult();

        try {

        	// 해당 클래스의 선생님 아이디를 조회해온다.
        	String teacherId = classRoomService.getClassTeacherId(clsSeq);

        	logger.debug("클래스의 선생님 아이디는? "+teacherId);


        	if(!teacherId.equals("")){

            	// 클래스에 등록된 책의 첫 레슨-첫차시 코드목록 조회
        		TodayClassCondition tcCond = new TodayClassCondition();

        		tcCond.setClsSeq(clsSeq);
        		tcCond.setMbrId(teacherId);

            	List<TodayClassVo> lessonList = classRoomService.getFirstLessonList(tcCond);

            	// 책이 여러개인 경우 첫 레슨/첫차시는 여러건이 될수 있다.
            	for(int i =0; i < lessonList.size(); i ++){

            		// 레슨의 첫번째 차시 코드.
            		String dayNoCd = lessonList.get(i).getDayNoCd();

            		logger.debug("첫 레슨 첫 차시 코드는 ==="+dayNoCd);

            		// TODAY_CLASS insert
            		classRoomService.addDataTodayClass(clsSeq, dayNoCd, "1");

            		TodayClassCondition nextCond = new TodayClassCondition();

            		nextCond.setMbrId(teacherId);
            		nextCond.setDayNoCd(dayNoCd);

            		// 다음 레슨의 첫번재 차시 코드 조회
            		String nextDayNoCd = classRoomService.getNextLessonDayNoCd(nextCond);
            		logger.debug("다음 레슨 첫 차시 코드는  ==="+nextDayNoCd);

            		if(!nextDayNoCd.equals("")){
            			// TODAY_CLASS insert
                		classRoomService.addDataTodayClass(clsSeq, nextDayNoCd, "2");
            		}


            	}

                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

        	}else{
                // 선생님 아이디가 없는 경우. 최초 차시를 불러올수 없다.
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));

        	}



        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }


    /**
     * 클래스 종료 처리 (클래스의 차시 코드 정보를 업데이트)
     * @param request
     * @param response
     * @param clsSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/modifyClassNextSchedule", method=RequestMethod.POST)
    public VSResult modifyClassNextSchedule(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq) throws Exception {

        logger.debug("modifyClassNextSchedule");

        VSResult result = new VSResult();
        TodayClassCondition tcCond = new TodayClassCondition();

        try {

            if (!clsSeq.equals("")) {

                // 해당 클래스의 선생님 아이디를 조회해온다.
                String mbrId = classRoomService.getClassTeacherId(clsSeq);

                if (mbrId != null && !mbrId.equals("")) {
                    // 현재 등록된 차시 코드 조회
                    tcCond.setMbrId(mbrId);
                    tcCond.setClsSeq(clsSeq);
                    String dayNoCd = classRoomService.getClassTodayDayNoCd(tcCond);

                    if (dayNoCd != null && !dayNoCd.equals("")) {
                        // 다음 차시 코드 조회
                        tcCond.setDayNoCd(dayNoCd);
                        String nextDayNoCd = classRoomService.getClassNextdayDayNoCd(tcCond);

                        if (nextDayNoCd != null && !nextDayNoCd.equals("")) {
                            // 차시 코드 정보 수정
                            tcCond.setNextDayNoCd(nextDayNoCd);
                            classRoomService.modifyClassDayNoCdInfo(tcCond);

                        } else {
                            // nextDayNoCd 없는 경우 기존 클래스의 차시 코드 정보 삭제
                            classRoomService.deleteClassDayNoCd(tcCond);
                        }

                        result.setCode(TreeProperties.getProperty("error.success.code"));
                        result.setMessage(TreeProperties.getProperty("error.success.msg"));

                    } else {
                        // dayNoCd 없는 경우
                        result.setCode(TreeProperties.getProperty("error.fail.code"));
                        result.setMessage(TreeProperties.getProperty("error.fail.msg"));
                    }
                } else {
                    // 선생님 아이디가 없는 경우. 최초 차시를 불러올수 없다.
                    result.setCode(TreeProperties.getProperty("error.fail.code"));
                    result.setMessage(TreeProperties.getProperty("error.fail.msg"));
                }
            } else {
                // clsSeq가 없는 경우
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            }
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value="/classroom/getClassLessonList")
    public VSResult getClassLessonList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq
            ) throws Exception {

        VSResult result = new VSResult();

        try {

            if (!clsSeq.equals("")) {
                ClassLessonListCondition classLessonListCondition = new ClassLessonListCondition();
                classLessonListCondition.setClsSeq(clsSeq);

                List<VSObject> classLessonList = classRoomService.getClassLessonList(classLessonListCondition);
                result.setResult(classLessonList);

            } else {
                // clsSeq가 없는 경우
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            }
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;

    }
}
