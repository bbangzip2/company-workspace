package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.vo.VSCondition;

public class ClassRelayIpCondition extends VSCondition {
    private String clsSeq;
    private String connStat;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getConnStat() {
        return connStat;
    }
    public void setConnStat(String connStat) {
        this.connStat = connStat;
    }
}
