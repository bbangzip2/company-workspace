package com.visangesl.tree.classroom.vo;

import com.visangesl.tree.vo.VSList;

public class TodayClassVo extends VSList {

    private String clsSeq;
    private String dayNoCd;
    private String sortOrd;
    
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String sortOrd) {
		this.sortOrd = sortOrd;
	}


}
