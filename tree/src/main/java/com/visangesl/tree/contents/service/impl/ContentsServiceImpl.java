package com.visangesl.tree.contents.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.contents.service.ContentsService;
import com.visangesl.tree.contents.vo.ContentsCondition;
import com.visangesl.tree.contents.vo.ProdInfoVo;
import com.visangesl.tree.mapper.ContentsMapper;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public class ContentsServiceImpl implements ContentsService {
	private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	ContentsMapper contentsMapper;


    @Override
    public VSObject getNewContentsInfo(ContentsCondition contentsCondition)
            throws Exception {
        // TODO Auto-generated method stub
        return contentsMapper.getNewContentsInfo(contentsCondition);
    }

	@Override
    @Transactional(readOnly = true)
    public VSObject getData(VSObject vsObject) throws Exception {

        return contentsMapper.getData(vsObject);
    }

	@Override
    @Transactional(readOnly = true)
    public VSObject getContentsInfoData(VSCondition dataCondition) throws Exception {

        return contentsMapper.getContentsInfoData(dataCondition);
    }

	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getDataList(dataCondition);
    }

	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDayDataList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getDayDataList(dataCondition);
    }

	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getCardMapDataList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getCardMapDataList(dataCondition);
    }

	@Override
    @Transactional
    public VSResult modifyCardMapSortData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	// 카드 정렬 정보 업데이트
        int affectedRows = contentsMapper.modifyCardMapSortData(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = contentsMapper.modifyDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

	@Override
    @Transactional
    public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = contentsMapper.addDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

	@Override
    @Transactional
    public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	int affectedRows = contentsMapper.deleteDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

	/**
	 * 카드코드 업데이트 처리
	 */
	@Override
    @Transactional
	public int InsertProdInfo(ProdInfoVo prodInfoVo) throws Exception{

		// 상품 테이블에 인서트 후
		contentsMapper.cProdInfoData(prodInfoVo);

		// 저장된 키값을 읽어서
		String prodSeq = prodInfoVo.getProdSeq();

		// CARD_CD 컬럼에 값을 채워준다.
		int result = contentsMapper.uProdSeqCard(prodSeq);


		return result;
	}

    @Override
    public List<VSObject> getPackageDataList(VSCondition dataCondition) throws Exception {

        return contentsMapper.getPackageDataList(dataCondition);

    }

    @Override
    public VSObject getBookLessonCd(VSCondition dataCondition) throws Exception {
        // TODO Auto-generated method stub
        return contentsMapper.getBookLessonCd(dataCondition);
    }

    @Override
    @Transactional
    public int modifyCardMapSortDataReset(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return contentsMapper.modifyCardMapSortDataReset(vsObject);
    }

    @Override
    @Transactional
    public int InsertPackageVersion(ContentsCondition condition) throws Exception {

        return contentsMapper.insertPackageVersion(condition);
    }

    @Override
    public List<VSObject> getCardMapDataSubList(VSCondition dataCondition)
            throws Exception {
        return contentsMapper.getCardMapDataSubList(dataCondition);
    }

    @Override
    @Transactional
    public VSResult insertCardMapSortData(VSObject vsObject) throws Exception {
        VSResult resultCodeMsg = new VSResult();

        // 카드 정렬 정보 업데이트
        int affectedRows = contentsMapper.insertCardMapSortData(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult deleteCardMapSortData(VSObject vsObject) throws Exception {
        VSResult resultCodeMsg = new VSResult();

        // 카드 정렬 정보 삭제
        int affectedRows = contentsMapper.deleteCardMapSortData(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
    
    @Override
 	public String getReferenceListCnt(VSObject vsObejct) throws Exception{
    	return contentsMapper.getReferenceListCnt(vsObejct);
    }
    
    @Override
    public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getReferenceTypeList(dataCondition);

    }
    
}


