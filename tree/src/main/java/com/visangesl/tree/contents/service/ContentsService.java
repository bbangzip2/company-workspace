package com.visangesl.tree.contents.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.contents.vo.ContentsCondition;
import com.visangesl.tree.contents.vo.ProdInfoVo;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public interface ContentsService {

    public VSObject getNewContentsInfo(ContentsCondition contentsCondition) throws Exception;

	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;

	// select One Data - 해당 컨텐츠의 상세 데이터 조회.
	public VSObject getContentsInfoData(VSCondition dataCondition) throws Exception;

	// select List Data
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;

	// select List Data - 해당 Lesson의 Day 데이터 리스트 조회.
	public List<VSObject> getDayDataList(VSCondition dataCondition) throws Exception;

	// select List Data - 해당 차시의 카드맵 데이터 리스트 조회.
	public List<VSObject> getCardMapDataList(VSCondition dataCondition) throws Exception;

    // select List Data - 해당 차시의 카드맵 데이터 서브 리스트 조회.
    public List<VSObject> getCardMapDataSubList(VSCondition dataCondition) throws Exception;

	// Update - 해당 유저의 특정 카드의 정렬 값 Update
	public VSResult modifyCardMapSortData(VSObject vsObject) throws Exception;

	// Insert - 해당 유저의 특정 카드의 정렬 값 Insert
    public VSResult insertCardMapSortData(VSObject vsObject) throws Exception;

    // Update - 해당 유저의 특정 카드의 정렬 값 초기화~ Update
    public int modifyCardMapSortDataReset(VSObject vsObject) throws Exception;

    // Delete - 해당 유저의 기존 카드 정렬 값 Delete
    public VSResult deleteCardMapSortData(VSObject vsObject) throws Exception;

	// Update
	public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

	// Insert
	public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception;

	// Delete
	public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;

	// PORDINFO data insert
	public int InsertProdInfo(ProdInfoVo prodInfoVo) throws Exception;

    // select Package List Data
    public List<VSObject> getPackageDataList(VSCondition dataCondition) throws Exception;

    public VSObject getBookLessonCd(VSCondition dataCondition) throws Exception;

    // package version up insert
    public int InsertPackageVersion(ContentsCondition condition) throws Exception;
    
	// 컨퍼런스 목록 총 갯수 조회 
	public String getReferenceListCnt(VSObject vsObejct) throws Exception;
	
	// 컨퍼런스 타입목록 
	public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception;
}

