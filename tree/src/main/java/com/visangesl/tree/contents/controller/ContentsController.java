package com.visangesl.tree.contents.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.contents.service.ContentsService;
import com.visangesl.tree.contents.vo.ContentsCondition;
import com.visangesl.tree.contents.vo.ContentsDownloadVo;
import com.visangesl.tree.contents.vo.ContentsVo;
import com.visangesl.tree.contents.vo.ProdInfoVo;
import com.visangesl.tree.contents.vo.RefCondition;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.service.MemberService;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.util.TreeUtil;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Controller
public class ContentsController implements BaseController {

    @Autowired
    private ContentsService contentsService;

    @Autowired
    private MemberService memberService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * getClassSimpleList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/contents/versionCheck")
    public VSResult versionCheck(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "version", required = false, defaultValue = "") String version
            ) throws Exception {

        logger.debug("versionCheck");
        logger.debug("lessonCd: " + lessonCd);
        logger.debug("version: " + version);

        VSResult result = new VSResult();
        ContentsVo contentsVo = new ContentsVo();
        ContentsCondition contentsCondition = new ContentsCondition();

        try {
            // 사용자 아이디/권한 조회
            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
            String mbrId = "";
            String mbrGrade = "";
            if(userSession != null){
                mbrId = userSession.getMemberId();
                mbrGrade = userSession.getMbrGrade();
            } else {
                logger.debug("userSession NULL!");
            }

            // 파라미터 체크
            if (!lessonCd.equals("")) {
                contentsCondition.setLessonCd(lessonCd);
                contentsCondition.setMbrId(mbrId);
                contentsCondition.setMbrGrade(mbrGrade);

                ContentsDownloadVo contentsDownVo = new ContentsDownloadVo();
                // 버전 체크
                if (version.equals("")) { // 사용자 버전 정보가 없거나 버전 정보가 없을 경우 마스터 버전 정보 조회
                    contentsCondition.setMbrId("admin");
                    contentsDownVo = (ContentsDownloadVo) contentsService.getNewContentsInfo(contentsCondition);
                } else {
                    contentsDownVo = (ContentsDownloadVo) contentsService.getNewContentsInfo(contentsCondition);
                }

                // 버전 정보가 있는지 체크
                if (contentsDownVo != null) {

                    String ver = contentsDownVo.getVersion();

                    if (version.equals("") || !ver.equals(version)) {
                        result.setMessage("JYP"); // 최신 버전 다운로드 필요

                        contentsDownVo.setServerUri(Tree_Constant.SITE_URL);
                        contentsDownVo.setServerPort(Tree_Constant.SITE_PORT);
                        contentsDownVo.setProdFolder(TreeProperties.getProperty("tree_package_path"));
                        contentsDownVo.setFileExt("zip");
                        contentsDownVo.setDownloadUrl(Tree_Constant.SITE_FULL_URL + contentsDownVo.getFilePath());


                        // 로컬/테스트 서버 일 경우 경로에 SITE_CDN_PATH 추가
                        if (request.getServerName().equals("localhost") || request.getServerName().indexOf("10.30.0.246") >= 0 || request.getServerName().indexOf("106.241.5.246") >= 0) {
//                            contentsDownVo.setFilePath(Tree_Constant.TEST_SITE_CDN_PATH + contentsDownVo.getFilePath()); // 노트북 일 경우에는 주석 처리 해야 됨.
                            contentsDownVo.setFilePath(contentsDownVo.getFilePath());
                        } else {
                            contentsDownVo.setFilePath(contentsDownVo.getFilePath());
                        }


                        // 컨텐츠 정보 저장
                        result.setResult(contentsDownVo);

                    } else {
                        result.setMessage("SM"); // 이미 최신 버전
                    }
                    result.setCode(TreeProperties.getProperty("error.success.code"));

                } else {
                    result.setCode("9001");
                    result.setMessage("다운로드 정보가 존재하지 않습니다.");
                }
            } else {
                logger.debug("9000");
                result.setCode("9000");
                result.setMessage("필수 파라미터가 없습니다.");
            }

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 전자칠판 공통기능 - 레퍼런스 파일 목록 조회
     * @param request
     * @param response
     * @param cid - 엑티비티 CID
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/ReferenceList", method=RequestMethod.POST)
    public VSResult<VSObject> getReferenceList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "cid", required = false, defaultValue = "") String cid
    		, @RequestParam(value = "refType", required = false, defaultValue = "") String refType) throws Exception {

        logger.debug("getReferenceList");

        VSResult result = new VSResult();

        try {

        	String referType = TreeUtil.isNullCheck(refType);

            RefCondition refCondition = new RefCondition();
            refCondition.setProdSeq(cid);
            refCondition.setProdType(referType);

            List<VSObject> conList = contentsService.getDataList(refCondition);

            result.setResult(conList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 레퍼런스 총 갯수
     * @param request
     * @param response
     * @param cid
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/ReferenceListCnt", method=RequestMethod.POST)
    public VSResult<VSObject> getReferenceListCnt(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "cid", required = false, defaultValue = "") String cid ) throws Exception {

        logger.debug("getReferenceListCnt");

        VSResult result = new VSResult();

        try {

            RefCondition refCondition = new RefCondition();
            refCondition.setProdSeq(cid);

            String refTypeCnt = contentsService.getReferenceListCnt(refCondition);

            result.setResult(refTypeCnt);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 레퍼런스 타입별 목록
     * @param request
     * @param response
     * @param cid
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/ReferenceTypeList", method=RequestMethod.POST)
    public VSResult<VSObject> getReferenceTypeList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "cid", required = false, defaultValue = "") String cid ) throws Exception {

        logger.debug("getReferenceTypeList");

        VSResult result = new VSResult();

        try {

            RefCondition refCondition = new RefCondition();
            refCondition.setProdSeq(cid);

            List<VSObject> conList = contentsService.getReferenceTypeList(refCondition);

            result.setResult(conList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }


//  @RequestMapping(value = "/contents/InsertProdInfo", method = RequestMethod.POST)
    @ResponseBody
    @RequestMapping(value = "/contents/InsertProdInfo")
    public VSResult InsertProdInfo(HttpServletRequest request, HttpServletResponse response
            , @RequestParam(value = "memberId", required = false) String memberId
            , @RequestParam(value = "prodTitle", required = false) String prodTitle
            , @RequestParam(value = "thmbPath", required = false) String thmbPath
            , @RequestParam(value = "dtlCntt", required = false) String dtlCntt
            , @RequestParam(value = "bookCd", required = false) String bookCd
            , @RequestParam(value = "lessonCd", required = false) String lessonCd
            , @RequestParam(value = "daynoCd", required = false) String daynoCd
            , @RequestParam(value = "mdlCd", required = false) String mdlCd
            , @RequestParam(value = "cardCd", required = false) String cardCd
            , @RequestParam(value = "sort", required = false) String sort
            , @RequestParam(value = "prodSect", required = false , defaultValue="") String prodSect //(레퍼런스 데이터만 'R' 로 들어간다.)
            , @RequestParam(value = "prodType", required = false , defaultValue="") String prodType //(상품유형( TXT / IMG / MOV / URL )_ 레퍼런스 컨텐츠에서 사용함)
            , @RequestParam(value = "curriCd", required = false) String curriCd
            , @RequestParam(value = "cardType", required = false) String cardType
            , @RequestParam(value = "cardLevel", required = false) String cardLevel
            , @RequestParam(value = "useType", required = false) String useType
            , @RequestParam(value = "filePath", required = false) String filePath
            , @RequestParam(value = "direcEng", required = false) String direcEng
            , @RequestParam(value = "direcKor", required = false) String direcKor
            , Model model) throws Exception {

        logger.debug("InsertProdInfo");
        logger.debug("InsertProdInfo");
        logger.debug("InsertProdInfo");


        VSResult result = new VSResult();
        ProdInfoVo prodInfoVo = new ProdInfoVo();

        try {

            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

            if(userSession != null){
              memberId = userSession.getMemberId();
            } else {
                logger.debug("userSession NULL!");
            }

            prodInfoVo.setProdTitle(prodTitle); // 제목
            prodInfoVo.setThmbPath(thmbPath);   // 썸네일 경로
            prodInfoVo.setRegId(memberId);      // 아이디
            prodInfoVo.setDtlCntt(dtlCntt);     // 상세설명
            prodInfoVo.setBookCd(bookCd);       // 북코드
            prodInfoVo.setLessonCd(lessonCd);   // 레슨코드
            prodInfoVo.setDaynoCd(daynoCd);     // 차시코드
            prodInfoVo.setMdlCd(mdlCd);         // 모듈코드
            prodInfoVo.setCardCd(cardCd);       // 카드코드
            prodInfoVo.setSort(sort);           // 정렬순서
            prodInfoVo.setProdSect(prodSect);   // 상품구분자  (레퍼런스 데이터만 'R' 로 들어간다.)
            prodInfoVo.setProdType(prodType);   // 상품타입 (상품유형( TXT / IMG / MOV / URL )_ 레퍼런스 컨텐츠에서 사용함)
            prodInfoVo.setCurriCd(curriCd);     // 커리큘럼 아이디 (사용안함.)
            prodInfoVo.setCardType(cardType);   // 카드타입 (카드유형(LECTURE/QUIZ/ACTIVITY))
            prodInfoVo.setCardLevel(cardLevel); // 카드레벨 (1~5)
            prodInfoVo.setUseType(useType);     // 카드 사용 용도 (강의용/개인학습용/그룹학습용)
            prodInfoVo.setFilePath(filePath);   // 실제 파일경로
            prodInfoVo.setDirecEng(direcEng);   // 영어 디렉션
            prodInfoVo.setDirecKor(direcKor);   // 한국어 디렉션


            if (!memberId.equals("")) {
                // 카드 정보 저장
                //카드 정보 저장은 아래 카드 복사 진행 후 경로 재지정해서 저장되도록 수정해야~
                contentsService.InsertProdInfo(prodInfoVo);
            }


//            // 카드 복사 경로 설정
//            File dir = new File("D:\\sia\\work\\PROJECT\\TREE\\test\\CONTENTS\\authoring\\C0006"); // 원본 폴더
////            File dir = new File(filePath); // 원본 폴더
//            File toDir = new File("D:\\sia\\work\\PROJECT\\TREE\\test\\CONTENTS\\C0006"); // 복사 될 폴더
//
//            // 카드 복사
//            boolean copyResult = TreeUtil.copy(dir, toDir);
//
//            if (copyResult) {
//                // html pathesl 파서 실행
//                if (HtmlParserUtil.htmlParser(toDir, "")) {
//
//                }
//            }

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 상품 테이블에 정보 입력
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/content/ProdInfoInsert", method = RequestMethod.GET)
    public String treeLogin(HttpServletRequest request, Model model)
            throws Exception {

        return "/content/insertProdInfo";
    }
}
