package com.visangesl.tree.contents.vo;

import com.visangesl.tree.vo.VSListCondition;

public class ContentsCondition extends VSListCondition {
    private String mbrId;
    private String mbrGrade;

    private String prodSeq;
    private String curriCd;
    private String prodSect;

    private String bookCd;
    private String lessonCd;
    private String version;

    private String dayNoCd;

    private String cardGubun;
    private String packageFilePath;

    private String tMbrId;

    private String cardFullPath;
    private String bookPath;
    private String lessonPath;
    private String cardPath;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getCurriCd() {
        return curriCd;
    }
    public void setCurriCd(String curriCd) {
        this.curriCd = curriCd;
    }
    public String getProdSect() {
        return prodSect;
    }
    public void setProdSect(String prodSect) {
        this.prodSect = prodSect;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
    public String getCardGubun() {
        return cardGubun;
    }
    public void setCardGubun(String cardGubun) {
        this.cardGubun = cardGubun;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getPackageFilePath() {
        return packageFilePath;
    }
    public void setPackageFilePath(String packageFilePath) {
        this.packageFilePath = packageFilePath;
    }
    public String gettMbrId() {
        return tMbrId;
    }
    public void settMbrId(String tMbrId) {
        this.tMbrId = tMbrId;
    }
    public String getCardFullPath() {
        return cardFullPath;
    }
    public void setCardFullPath(String cardFullPath) {
        this.cardFullPath = cardFullPath;
    }
    public String getBookPath() {
        return bookPath;
    }
    public void setBookPath(String bookPath) {
        this.bookPath = bookPath;
    }
    public String getLessonPath() {
        return lessonPath;
    }
    public void setLessonPath(String lessonPath) {
        this.lessonPath = lessonPath;
    }
    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }

}
