package com.visangesl.tree.contents.vo;

import com.visangesl.tree.vo.VSListCondition;

public class RefCondition extends VSListCondition {
	private String prodType;
    private String prodSeq;
    
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
}
