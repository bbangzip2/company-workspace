package com.visangesl.tree.contents.vo;

import com.visangesl.tree.vo.VSObject;

public class ProdInfoVo extends VSObject {
	private String mbrId;
	
	private String prodSeq;
	private String prodTitle;
	private String thmbPath;
	private String useYn;
	private String regDttm;
	private String regId;
	private String dtlCntt;
	private String bookCd;
	private String lessonCd;
	private String daynoCd;
	private String mdlCd;
	private String cardCd;
	private String sort;
	private String prodSect;
	private String prodType;
	private String curriCd;
	private String cardType;
	private String cardLevel;
	private String useType;
	private String filePath;
	private String direcEng;
	private String direcKor;
	
	
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getThmbPath() {
		return thmbPath;
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getDtlCntt() {
		return dtlCntt;
	}
	public void setDtlCntt(String dtlCntt) {
		this.dtlCntt = dtlCntt;
	}
	public String getBookCd() {
		return bookCd;
	}
	public void setBookCd(String bookCd) {
		this.bookCd = bookCd;
	}
	public String getLessonCd() {
		return lessonCd;
	}
	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}
	public String getDaynoCd() {
		return daynoCd;
	}
	public void setDaynoCd(String daynoCd) {
		this.daynoCd = daynoCd;
	}
	public String getMdlCd() {
		return mdlCd;
	}
	public void setMdlCd(String mdlCd) {
		this.mdlCd = mdlCd;
	}
	public String getCardCd() {
		return cardCd;
	}
	public void setCardCd(String cardCd) {
		this.cardCd = cardCd;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getProdSect() {
		return prodSect;
	}
	public void setProdSect(String prodSect) {
		this.prodSect = prodSect;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getCurriCd() {
		return curriCd;
	}
	public void setCurriCd(String curriCd) {
		this.curriCd = curriCd;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardLevel() {
		return cardLevel;
	}
	public void setCardLevel(String cardLevel) {
		this.cardLevel = cardLevel;
	}
	public String getUseType() {
		return useType;
	}
	public void setUseType(String useType) {
		this.useType = useType;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getDirecEng() {
		return direcEng;
	}
	public void setDirecEng(String direcEng) {
		this.direcEng = direcEng;
	}
	public String getDirecKor() {
		return direcKor;
	}
	public void setDirecKor(String direcKor) {
		this.direcKor = direcKor;
	}
    
}
