package com.visangesl.tree.contents.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class ContentsVo extends VSObject {
	private String mbrId;

    private String version;
    private String downloadUrl;

    private String serverUri;
    private String serverPort;
    private String filePath;
    private String fileName;
    private String fileExt;

    private String downloadPath;
    private String prodSeq;
    private String prodTitle;
    private String thmbPath;
    private String useYn;
    private String dtlCntt;
    private String bookCd;
    private String lessonCd;
    private String daynoCd;
    private String dayNo;
    private String cardCd;
    private String mdlCd;
    private String curriCd;
    private String sort;
    private String prodSect;
    private String prodType;
    private String prodFolder;

    private String cardGubun;
    private String cardGubunNm;

    private String cardFullPath;
    private String bookPath;
    private String lessonPath;
    private String cardPath;
    private String lessonNm;
    
    private String time;
    

    public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
    public String getDownloadPath() {
        if (downloadPath != null) {
            return Tree_Constant.SITE_FULL_URL + downloadPath;
        } else {
            return downloadPath;
        }
    }
    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }
    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getThmbPath() {
        if (thmbPath != null) {
            return Tree_Constant.SITE_FULL_URL + thmbPath;
        } else {
            return thmbPath;
        }
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getDtlCntt() {
        return dtlCntt;
    }
    public void setDtlCntt(String dtlCntt) {
        this.dtlCntt = dtlCntt;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getDaynoCd() {
        return daynoCd;
    }
    public void setDaynoCd(String daynoCd) {
        this.daynoCd = daynoCd;
    }
    public String getCardCd() {
		return cardCd;
	}
	public void setCardCd(String cardCd) {
		this.cardCd = cardCd;
	}
	public String getMdlCd() {
        return mdlCd;
    }
    public void setMdlCd(String mdlCd) {
        this.mdlCd = mdlCd;
    }
    public String getCurriCd() {
        return curriCd;
    }
    public void setCurriCd(String curriCd) {
        this.curriCd = curriCd;
    }
    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }
    public String getProdSect() {
        return prodSect;
    }
    public void setProdSect(String prodSect) {
        this.prodSect = prodSect;
    }
    public String getProdType() {
        return prodType;
    }
    public void setProdType(String prodType) {
        this.prodType = prodType;
    }
    public String getDownloadUrl() {
        return downloadUrl;
    }
    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
    public String getServerUri() {
        return serverUri;
    }
    public void setServerUri(String serverUri) {
        this.serverUri = serverUri;
    }
    public String getServerPort() {
        return serverPort;
    }
    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }
    public String getFilePath() {
//        if (filePath != null) {
//            return Tree_Constant.SITE_FULL_URL + filePath;
//        } else {
//            return filePath;
//        }
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getFileExt() {
        return fileExt;
    }
    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
	public String getProdFolder() {
		return prodFolder;
	}
	public void setProdFolder(String prodFolder) {
		this.prodFolder = prodFolder;
	}
    public String getDayNo() {
        return dayNo;
    }
    public void setDayNo(String dayNo) {
        this.dayNo = dayNo;
    }
	public String getCardGubun() {
		return cardGubun;
	}
	public void setCardGubun(String cardGubun) {
		this.cardGubun = cardGubun;
	}
	public String getCardGubunNm() {
		return cardGubunNm;
	}
	public void setCardGubunNm(String cardGubunNm) {
		this.cardGubunNm = cardGubunNm;
	}
    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }
    public String getCardFullPath() {
        return cardFullPath;
    }
    public void setCardFullPath(String cardFullPath) {
        this.cardFullPath = cardFullPath;
    }
    public String getBookPath() {
        return bookPath;
    }
    public void setBookPath(String bookPath) {
        this.bookPath = bookPath;
    }
    public String getLessonPath() {
        return lessonPath;
    }
    public void setLessonPath(String lessonPath) {
        this.lessonPath = lessonPath;
    }
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}

}
