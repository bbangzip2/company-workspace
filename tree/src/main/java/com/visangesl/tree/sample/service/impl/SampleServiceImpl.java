package com.visangesl.tree.sample.service.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.mapper.SampleMapper;
import com.visangesl.tree.sample.service.SampleService;
import com.visangesl.tree.service.impl.CommonServiceImpl;

@Service
public class SampleServiceImpl extends CommonServiceImpl implements SampleService {

	@Autowired
	SampleMapper sampleMapper;

	@PostConstruct
	public void initiate() {
		super.setBaseMapper(sampleMapper);
	}

}
