package com.visangesl.tree.sample.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.sample.service.SampleService;
import com.visangesl.tree.sample.vo.Sample;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Controller
public class SampleController implements BaseController {

	@Autowired
	private SampleService sampleService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/sample")
	public String sampleList(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		logger.debug("sample");
		
		VSCondition dataCondition  = new VSCondition();
		
		List<VSObject> sampleList = sampleService.getDataList(dataCondition);
//		Sample sample = new Sample();
//		List<VSObject> sampleList = (List<VSObject>) sampleService.getDataList(vscondiiton); 

		logger.debug("sampleList cnt: " + sampleList.size());

		model.addAttribute("sampleList", sampleList);

		return "/sample";
	}

	@RequestMapping(value = "/sampleInsert")
	public String sampleInsert(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		logger.debug("sampleInsert");

		return "/sampleInsert";
	}

	@RequestMapping(value = "/sampleInfo", method = RequestMethod.POST)
	public String sampleInfo(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("sampleInfo");

		
		Sample sample = new Sample();
		sample.setMbrId(mbrId);
		sample = (Sample) sampleService.getData(sample);

		model.addAttribute("sampleInfo", sample);

		return "/sampleInsert";
	}
	
	@RequestMapping(value = "/sampleAdd", method = RequestMethod.POST)
	public ModelAndView sampleAdd(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "nickName", required = false) String nickName,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "nm", required = false) String nm,
			@RequestParam(value = "eMail", required = false) String eMail,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "memo", required = false) String memo
			) throws Exception {
		logger.debug("sampleAdd");

		Sample sample = new Sample();
		sample.setNickName(nickName);
		sample.setPwd(pwd);
		sample.setNm(nm);
		sample.seteMail(eMail);
		sample.setTelNo(telNo);
		sample.setMemo(memo);
		sample.setMbrGrade("MBR_GRADE");

		sampleService.addDataWithResultCodeMsg(sample);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/sample.do");
		return mav;
	}

	@RequestMapping(value = "/sampleModify", method = RequestMethod.POST)
	public ModelAndView sampleModify(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId,
			@RequestParam(value = "nickName", required = false) String nickName,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "nm", required = false) String nm,
			@RequestParam(value = "eMail", required = false) String eMail,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "memo", required = false) String memo,
			@RequestParam(value = "useYn", required = false) String useYn
			) throws Exception {
		logger.debug("sampleModify");

		Sample sample = new Sample();
		sample.setMbrId(mbrId);
		sample.setNickName(nickName);
		sample.setPwd(pwd);
		sample.setNm(nm);
		sample.seteMail(eMail);
		sample.setTelNo(telNo);
		sample.setMemo(memo);
		sample.setUseYn(useYn);
		sample.setMbrGrade("MBR_GRADE");

		sampleService.modifyDataWithResultCodeMsg(sample);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/sample.do");
		return mav;
	}

	@RequestMapping(value = "/sampleRemove", method = RequestMethod.POST)
	public ModelAndView sampleRemove(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("sampleRemove");

		Sample sample = new Sample();
		sample.setMbrId(mbrId);

		sampleService.deleteDataWithResultCodeMsg(sample);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/sample.do");
		return mav;
	}
	
	@RequestMapping(value = "/server_test")
	public String serverTest(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

		return "/server_test";
	}

}
