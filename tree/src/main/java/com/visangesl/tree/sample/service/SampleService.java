package com.visangesl.tree.sample.service;

import org.springframework.stereotype.Service;

import com.visangesl.tree.service.CommonService;

@Service
public interface SampleService extends CommonService {

}
