package com.visangesl.tree.mynote.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.mynote.service.MyNoteService;
import com.visangesl.tree.mynote.vo.MyNoteCondition;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

/**
 * ClassRoomController
 * Version - 1.0
 * Copyright
 */
@Controller
public class MyNoteController implements BaseController {

    @Autowired
    private MyNoteService myNoteService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


	/**
	 * My Note > 레슨 목록 조회 
	 * @param request
	 * @param response
	 * @param memberId
	 * @return
	 * @throws Exception
	 */
    @ResponseBody
    @RequestMapping(value="/mynote/getLessonList", method=RequestMethod.POST)
    public VSResult getLessonList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "note_memberId", required = false) String memberId ) throws Exception {

        logger.debug("getLessonList");

        VSResult result = new VSResult();
        
        try {

            MyNoteCondition myNote = new MyNoteCondition();
            myNote.setMbrId(memberId);

            List<VSObject> lessonList  = myNoteService.getLessonList(myNote);

            result.setResult(lessonList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * 노트 목록 조회 
     * @param request
     * @param response
     * @param memberId
     * @param lessonCd
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/mynote/getMyNoteList", method=RequestMethod.POST)
    public VSResult getMyNoteList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "note_memberId", required = false) String memberId 
    		, @RequestParam(value = "note_lessonCd", required = false) String lessonCd ) throws Exception {

        logger.debug("getMyNoteList");

        VSResult result = new VSResult();
        
        try {

            MyNoteCondition myNote = new MyNoteCondition();
            myNote.setMbrId(memberId);
            myNote.setLessonCd(lessonCd);

            List<VSObject> NoteList  = myNoteService.getMyNoteList(myNote);

            result.setResult(NoteList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }
        return result;
    }
    
}
