package com.visangesl.tree.mynote.service;

import java.util.List;

import com.visangesl.tree.mynote.vo.MyNoteCondition;
import com.visangesl.tree.vo.VSObject;

public interface MyNoteService {

    public List<VSObject> getLessonList(MyNoteCondition myNoteCondition) throws Exception;
    public List<VSObject> getMyNoteList(MyNoteCondition myNoteCondition) throws Exception;
    
}
