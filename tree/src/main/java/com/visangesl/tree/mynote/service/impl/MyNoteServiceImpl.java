package com.visangesl.tree.mynote.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.mapper.NoteMapper;
import com.visangesl.tree.mynote.service.MyNoteService;
import com.visangesl.tree.mynote.vo.MyNoteCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public class MyNoteServiceImpl implements MyNoteService {

    @Autowired
    NoteMapper noteMapper;

    @Override
    public List<VSObject> getLessonList(MyNoteCondition myNoteCondition) throws Exception {
    	return noteMapper.getMyNoteLessonList(myNoteCondition);
    }

    @Override
    public List<VSObject> getMyNoteList(MyNoteCondition myNoteCondition) throws Exception {
    	return noteMapper.getMyNoteList(myNoteCondition);
    }
}