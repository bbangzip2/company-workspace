package com.visangesl.tree.mynote.vo;

import com.visangesl.tree.vo.VSListCondition;

public class MyNoteCondition extends VSListCondition {

    private String mbrId;
    private String lessonCd;

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

	public String getLessonCd() {
		return lessonCd;
	}

	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}

    
}
