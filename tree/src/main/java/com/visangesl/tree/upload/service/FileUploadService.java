package com.visangesl.tree.upload.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.upload.vo.AttachFileListConditionVo;
import com.visangesl.tree.vo.VSObject;

public interface FileUploadService {
	
	public void noteFileUpload(String memberId, String cid, String noteText, String page, String curPage, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception;
	
	public void cPortFolioInsert(String memberId, String clsSeq,
			String clsSchdlSeq, String prodSeq, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception;
	
	public List<VSObject> getAttachFileList(AttachFileListConditionVo afListCond) throws Exception;
	public List<VSObject> getNoteAttachFileList(AttachFileListConditionVo afListCond) throws Exception;
	
	public void onlyFileUpload(List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception;
	
}
