package com.visangesl.tree.upload.service;

import com.visangesl.tree.upload.vo.NoteInfoConditionVo;
import com.visangesl.tree.vo.VSObject;

public interface NoteService {

    public VSObject getContentsInfoData(NoteInfoConditionVo noteInfoConditionVo) throws Exception;
    
}
