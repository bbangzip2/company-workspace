package com.visangesl.tree.upload.vo;

import com.visangesl.tree.vo.VSObject;

public class NoteInfoConditionVo extends VSObject {
 
	private String noteSeq;
	private String mbrId;
	private String curPage;
	private String prodSeq;
	
	public String getNoteSeq() {
		return noteSeq;
	}
	public void setNoteSeq(String noteSeq) {
		this.noteSeq = noteSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getCurPage() {
		return curPage;
	}
	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}


}
