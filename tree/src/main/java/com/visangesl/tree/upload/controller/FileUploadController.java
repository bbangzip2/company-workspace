package com.visangesl.tree.upload.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.upload.service.FileUploadService;
import com.visangesl.tree.upload.service.NoteService;
import com.visangesl.tree.upload.vo.AttachFileListConditionVo;
import com.visangesl.tree.upload.vo.NoteInfoConditionVo;
import com.visangesl.tree.upload.vo.NoteInfoVo;
import com.visangesl.tree.view.TreeMappingJacksonJsonView;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Controller
public class FileUploadController implements BaseController {

	@Autowired
	private FileUploadService service;

	@Autowired
	private NoteService noteService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 노트(스티키) 정보를 등록합니다.
	 * @param request
	 * @param response
	 * @param note_cid : 카드(액티비티) 순번
	 * @param note_noteText : 노트에서 입력한 텍스트
	 * @param note_memberId : 회원정보
	 * @param note_page : 페이지
	 * @param note_curPage : 현 페이지
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/upload/noteFileUpload", method = RequestMethod.POST)
	public ModelAndView noteFileUpload(HttpServletRequest request, HttpServletResponse response
			, @RequestParam(value = "note_cid", required = false) String cid
			, @RequestParam(value = "note_noteText", required = false) String noteText
			, @RequestParam(value = "note_memberId", required = false) String memberId
			, @RequestParam(value = "note_page", required = false, defaultValue="1") String page
			, @RequestParam(value = "note_curPage", required = false, defaultValue="1") String curPage
			, Model model) throws Exception {
		logger.debug("noteFileUpload");

	    VSResult result = new VSResult();

    	try {

//				HttpSession session = request.getSession();
//				UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
//
//				if(userSession != null){
//					memberId = userSession.getMemberId();
//				}

                MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

                List<MultipartFile> uploadFileList = multipartRequest.getFiles("uploadfile");

                // 파일이 업로드 될 절대경로의 루트를 먼저 구한다
//                String realPath = TreeProperties.getProperty("tree_fileroot") + TreeProperties.getProperty("tree_save_filepath") + TreeProperties.getProperty("tree_note_filepath");
                String realPath = TreeProperties.getProperty("tree_save_filepath") + TreeProperties.getProperty("tree_note_filepath");
                String savePath = TreeProperties.getProperty("tree_note_filepath");

                logger.debug("memberId : " + memberId);
            	logger.debug("realpath : " + realPath);
            	logger.debug("uploadFileList.size : " + uploadFileList.size());

            	// 디렉토리 생성
            	if(!new File(realPath).exists()){
        			new File(realPath).mkdirs();
        		}

            	// PORTFORLIO DATA INSERT AND FILE INSERT
                service.noteFileUpload(memberId, cid, noteText, page, curPage, uploadFileList, realPath, savePath);


                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        ModelAndView mav = new ModelAndView("jsonView", "json", result);
        TreeMappingJacksonJsonView view = new TreeMappingJacksonJsonView();
        view.setContentType("text/html");
        mav.setView(view);
        return mav;
	}


	/**
	 *
	 * @param request
	 * @param response
	 * @param clsSeq : 클래스 순번
	 * @param clsSchdlSeq : 수업 순번
	 * @param prodSeq  : 카드 순번
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload/portFolioInsert", method = RequestMethod.POST)
	public ModelAndView portfolioInsert(HttpServletRequest request, HttpServletResponse response
			, @RequestParam(value = "clsSeq", required = false) String clsSeq
			, @RequestParam(value = "clsSchdlSeq", required = false) String clsSchdlSeq
			, @RequestParam(value = "prodSeq", required = false) String prodSeq
			, Model model) throws Exception {

	    VSResult result = new VSResult();

    	String memberId = ""; // 로그인 아이디

    	logger.debug("portfolioInsert start . . . . . .");

    	try {
    			HttpSession session = request.getSession();
    			UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

    			if(userSession != null){
    				memberId = userSession.getMemberId();
    			}

                MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

                List<MultipartFile> uploadFileList = multipartRequest.getFiles("uploadfile");

                // 파일이 업로드 될 절대경로의 루트를 먼저 구한다

                // tree.properties 파일에 tree_fileroot 값을 개발자 각각의 폴더 경로로 맞춰 주어야 한다.
//                String realPath = TreeProperties.getProperty("tree_fileroot") + TreeProperties.getProperty("tree_save_filepath") + TreeProperties.getProperty("tree_portfolio_filepath");
                String realPath = TreeProperties.getProperty("tree_save_filepath") + TreeProperties.getProperty("tree_portfolio_filepath");
                String savePath = TreeProperties.getProperty("tree_portfolio_filepath");

            	if(!new File(realPath).exists()){
        			new File(realPath).mkdirs();
        		}

            	// PORTFORLIO DATA INSERT AND FILE INSERT
            	service.cPortFolioInsert( memberId,  clsSeq, clsSchdlSeq,  prodSeq, uploadFileList,  realPath, savePath);

                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            logger.error(e.toString());
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        ModelAndView mav = new ModelAndView("jsonView", "json", result);
        TreeMappingJacksonJsonView view = new TreeMappingJacksonJsonView();
        view.setContentType("text/html");
        mav.setView(view);
        return mav;
	}


	/**
	 * 노트의 정보와 첨부파일 목록 조회
	 * @param request
	 * @param response
	 * @param cid : 노트 아이디
	 * @param memberId : 회원 아이디
	 * @param curPage : 현재 페이지(추후 조건으로 들어갈 것같아서 추가함) : 기본 1
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/upload/getNoteData", method = RequestMethod.POST)
	public VSResult getNoteData(HttpServletRequest request, HttpServletResponse response
			, @RequestParam(value = "note_cid", required = false) String cid
			, @RequestParam(value = "note_memberId", required = false) String memberId
			, @RequestParam(value = "note_prodSeq", required = false) String prodSeq
			, @RequestParam(value = "note_curPage", required = false, defaultValue="1") String curPage
			, Model model) throws Exception {

        VSResult result = new VSResult();

        try {
	        // 노트 정보 조회
        	NoteInfoConditionVo noteCond = new NoteInfoConditionVo();
        	noteCond.setNoteSeq(cid); //노트 순번
        	noteCond.setCurPage(curPage); // 현재 페이지
        	noteCond.setProdSeq(prodSeq); // 액티비티  seq
        	noteCond.setMbrId(memberId);

        	NoteInfoVo rstVo = (NoteInfoVo) noteService.getContentsInfoData(noteCond);

	        HashMap pMap = new HashMap();
	        pMap.put("noteInfo", rstVo);

	        AttachFileListConditionVo attCond = new AttachFileListConditionVo();

	        attCond.setFileGubun("N"); //노트 구분자
	        attCond.setRefSeq(prodSeq); // 카드 아이디로 조회함.
//	        attCond.setServerPath(Tree_Constant.SITE_URL + ":"+ Tree_Constant.SITE_PORT + Tree_Constant.SITE_CDN_PATH);
	        attCond.setServerPath(Tree_Constant.SITE_FULL_URL);
	        attCond.setMbrId(memberId);

	        List<VSObject> attFileList = service.getNoteAttachFileList(attCond);


	        result.setCode("0000");
	        result.setMessage("성공");
	        result.setParamMap(pMap);
	        result.setResult(attFileList);


	    } catch (Exception e) {
	        ExceptionHandler handler = new ExceptionHandler(e);
	        result.setCode(handler.getCode());
	        result.setMessage(handler.getMessage());
	        e.printStackTrace();
	    }

        return result;
	}



	@ResponseBody
	@RequestMapping(value = "/upload/onlyFileUpload", method = RequestMethod.POST)
	public ModelAndView noteFileUpload(HttpServletRequest request, HttpServletResponse response
			, Model model) throws Exception {
		logger.debug("onlyFileUpload");

	    VSResult result = new VSResult();

    	try {


                MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

                List<MultipartFile> uploadFileList = multipartRequest.getFiles("uploadfile");

                // 파일이 업로드 될 절대경로의 루트를 먼저 구한다
                //String realpath = request.getSession().getServletContext().getRealPath(savepath);
//                String realPath = TreeProperties.getProperty("tree_fileroot") + "/test";
                String realPath = TreeProperties.getProperty("tree_save_filepath") + "/test";
                String savePath = TreeProperties.getProperty("tree_save_filepath") + "/test";

            	// 디렉토리 생성
            	if(!new File(realPath).exists()){
        			new File(realPath).mkdirs();
        		}

            	// PORTFORLIO DATA INSERT AND FILE INSERT
                service.onlyFileUpload(uploadFileList, realPath, savePath);


                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        ModelAndView mav = new ModelAndView("jsonView", "json", result);
        TreeMappingJacksonJsonView view = new TreeMappingJacksonJsonView();
        view.setContentType("text/html");
        mav.setView(view);
        return mav;
	}


}
