package com.visangesl.tree.upload.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.mapper.AttachFileMapper;
import com.visangesl.tree.mapper.NoteMapper;
import com.visangesl.tree.mapper.PortFolioMapper;
import com.visangesl.tree.portfolio.vo.PortfolioVo;
import com.visangesl.tree.upload.service.FileUploadService;
import com.visangesl.tree.upload.vo.AttachFileListConditionVo;
import com.visangesl.tree.upload.vo.AttachFileVo;
import com.visangesl.tree.upload.vo.NoteInfoVo;
import com.visangesl.tree.util.FileRenamePolicy;
import com.visangesl.tree.vo.VSObject;

@Service
public class FileUploadServiceImpl implements FileUploadService {
	private final Log logger = LogFactory.getLog(this.getClass());
		
	@Autowired
	PortFolioMapper portfolioMapper;
	
	@Autowired
	AttachFileMapper attachFileMapper;
	
	@Autowired
	NoteMapper noteMapper;
	
	
	@Override
	@Transactional
	public void noteFileUpload(String memberId , String cid, String noteText, String page, String curPage, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception {
		
		String org_file_name = "";
		String real_file_name = "";
		long file_size = 0;
		
		try{
		
			NoteInfoVo ntVo = new NoteInfoVo();
			
			ntVo.setMbrId(memberId);
			ntVo.setProdSeq(cid); //
			ntVo.setNoteText(noteText); //
			ntVo.setPage(page); //  
			ntVo.setCurPage(curPage);
			  
			
			int noteCount = noteMapper.getNoteCount(ntVo);
			
			System.out.println("noteCount============"+noteCount);
			
			// 저장된 포트 폴리오의 키값. 
			String refSeq = "" ;
						
			if(noteCount > 0){
				noteMapper.uNoteData(ntVo);
				refSeq = noteMapper.getNoteSeq(ntVo);
				
				
			}else{
				attachFileMapper.cNoteData(ntVo);
				refSeq = ntVo.getNoteSeq();
			}
			
			
			System.out.println("refSeq============"+refSeq);
			
			
			// 파일 INSERT  하기 전에 등록되었던 파일 데이터를 지워준다. 
       		AttachFileVo afConditionVo = new AttachFileVo();
       		
       		afConditionVo.setNotePage("1"); // 페이지는 2월 기준으로 1만 있다. 
       		afConditionVo.setRefSeq(refSeq);
       		afConditionVo.setFileGubun("N");
       		
       		attachFileMapper.dTreeAttachFile(afConditionVo);
       		
			
			// 업로드된 파일에 대한 작업을 진행한다
	        for (MultipartFile file : uploadFileList) {
	        	org_file_name = file.getOriginalFilename();
	        	file_size = file.getSize();
	        	
	            if ("".equals(org_file_name)) {
	                continue;
	            }
	            
	            String body = null;
	            String ext = null;
	            String fileType ="FT999";
	            
	            int dot = org_file_name.lastIndexOf(".");
	            
	            if (dot != -1) {                  
	                body = org_file_name.substring(0, dot);
	                ext = org_file_name.substring(dot);
	                
	                // FT001(IMAGE), FT002 (AUDIO), FT003 (VIDEO) 
	                if( ext.toUpperCase().equals(".JPG") || ext.toUpperCase().equals(".PNG") || ext.toUpperCase().equals(".GIF") || ext.toUpperCase().equals(".JPEG") ){
	                	fileType = "FT001";
	                }else if( ext.toUpperCase().equals(".MP3") || ext.toUpperCase().equals(".OGG") ){
	                	fileType = "FT002";
	                }else if( ext.toUpperCase().equals(".MP4") || ext.toUpperCase().equals(".AVI") ){
	                	fileType = "FT003";
	                }                
	            } else {
	                body = org_file_name;
	                ext = "";
	            }
	            
	            // 저장되는 파일명은 타임값에 랜덤 숫자를 더해서 만들어준다. 
	    		Random r = new Random();
	    		int k = r.nextInt(100000000);
	    		
	    		String saveFileName = Long.toString(new Date().getTime() /1000+ k)+ext;
	            
	            
	            File realFile = new File(realpath + File.separator + saveFileName);
	            realFile = new FileRenamePolicy().rename(realFile);
	            
	//            System.out.println("[renamed filename]="+realFile.getName());
	            
	            real_file_name = realFile.getName();
	            
	            // 업로드된 파일을 다른 이름으로 복사(System 타임을 사용)
	            File destination = new File(realpath + File.separator + real_file_name);
	            
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	            
	            // file vo 
	       		AttachFileVo afVo = new AttachFileVo();
	       		
	       		afVo.setFileNm(org_file_name);
	       		afVo.setFilePath(savepath + File.separator + real_file_name);
	       		afVo.setNotePage("1"); // 페이지는 2월 기준으로 1만 있다. 
	       		afVo.setRefSeq(refSeq);
	       		afVo.setSaveFileNm(saveFileName);
	       		afVo.setThmbPath(""); //썸네일은 언제 넣을지 모르겠음.
	       		afVo.setFileType(fileType);
	       		afVo.setFileGubun("N");
	       		
	       		attachFileMapper.addDataWithResultCodeMsg(afVo);
	       	
	        }
	        
        }catch(Exception e){
        	logger.error(e.toString());
        	throw e;
        }
        
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void cPortFolioInsert(String memberId, String clsSeq,
			String clsSchdlSeq, String prodSeq, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception {
		
		
		String org_file_name = "";
		String real_file_name = "";
		long file_size = 0;
		
		try{
		
			PortfolioVo portVo = new PortfolioVo();
			
			portVo.setMbrId(memberId);
			portVo.setClsSeq(clsSeq); // 클래스 순번 
			portVo.setClsSchdlSeq(clsSchdlSeq); // 수업 순번 
			portVo.setProdSeq(prodSeq); // 카드순번 
			
			portfolioMapper.insertPortfolioInfo(portVo);
			
			// 저장된 포트 폴리오의 키값. INCLS_RSLT_SEQ
			String refSeq = portVo.getInclsRsltSeq();
			
			// 업로드된 파일에 대한 작업을 진행한다
	        for (MultipartFile file : uploadFileList) {
	        	org_file_name = file.getOriginalFilename();
	        	file_size = file.getSize();
	
	            if ("".equals(org_file_name)) {
	                continue;
	            }
	            
	            
	            String body = null;
	            String ext = null;
	            String fileType = "FT999"; //파일형태 
	            
	            int dot = org_file_name.lastIndexOf(".");
	            if (dot != -1) {          
	                body = org_file_name.substring(0, dot);
	                ext = org_file_name.substring(dot);
	                // FT001(IMAGE), FT002 (AUDIO), FT003 (VIDEO) 
	                if( ext.toUpperCase().equals(".JPG") || ext.toUpperCase().equals(".PNG") || ext.toUpperCase().equals(".GIF") || ext.toUpperCase().equals(".JPEG") ){
	                	fileType = "FT001";
	                }else if( ext.toUpperCase().equals(".MP3") || ext.toUpperCase().equals(".OGG") ){
	                	fileType = "FT002";
	                }else if( ext.toUpperCase().equals(".MP4") || ext.toUpperCase().equals(".AVI") ){
	                	fileType = "FT003";
	                }   
	                
	            } else {
	                body = org_file_name;
	                ext = "";
	            }
	            
	            // 저장되는 파일명은 타임값에 랜덤 숫자를 더해서 만들어준다. 
	    		Random r = new Random();
	    		int k = r.nextInt(100000000);
	    		
	    		String saveFileName = Long.toString(new Date().getTime() /1000+ k)+ext;
	    		
	            //logger.debug("insertNoticeInfo file_size : " + file_size);
	            
	            File realFile = new File(realpath + "/" + saveFileName);
	            realFile = new FileRenamePolicy().rename(realFile);
	            real_file_name = realFile.getName();
	            
	            // 업로드된 파일을 다른 이름으로 복사(System 타임을 사용)
	            File destination = new File(realpath + "/" + real_file_name);
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	            
	            
	            
	         // file vo 
	    		AttachFileVo afVo = new AttachFileVo();
	    		
	    		afVo.setFileNm(org_file_name);
	    		afVo.setFilePath(savepath + "/" + real_file_name);
	    		afVo.setNotePage("1"); // 페이지는 2월 기준으로 1만 있다. 
	    		afVo.setRefSeq(refSeq);
	    		afVo.setSaveFileNm(saveFileName);
	    		afVo.setThmbPath(savepath + "/" + real_file_name); //썸네일은 언제 넣을지 모르겠음.
	    		afVo.setThmbPath_s(savepath + "/" + real_file_name); //썸네일 소자. 
	    		afVo.setThmbPath_l(savepath + "/" + real_file_name); //썸네일 대자.
	    		
	    		afVo.setFileType(fileType);
	    		afVo.setFileGubun("P");
	    		
	    		attachFileMapper.addDataWithResultCodeMsg(afVo);
	    		
	    		
	        }
		
		}catch(Exception e){
			logger.error(e.toString());
			throw e;
		}       
	}
	
	@Override
	public List<VSObject> getAttachFileList(AttachFileListConditionVo afListCond) throws Exception{
		return attachFileMapper.getAttachFileList(afListCond);
	}

	@Override
	public List<VSObject> getNoteAttachFileList(AttachFileListConditionVo afListCond) throws Exception{
		return attachFileMapper.getNoteAttachFileList(afListCond);
	}
	
	public void onlyFileUpload(List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception{
		String org_file_name = "";
		String real_file_name = "";
		long file_size = 0;
		
		try{
			
			// 업로드된 파일에 대한 작업을 진행한다
	        for (MultipartFile file : uploadFileList) {
	        	org_file_name = file.getOriginalFilename();
	        	file_size = file.getSize();
	
	            if ("".equals(org_file_name)) {
	                continue;
	            }
	            
	            
	            String body = null;
	            String ext = null;
	            
	            int dot = org_file_name.lastIndexOf(".");
	            if (dot != -1) {          
	                body = org_file_name.substring(0, dot);
	                ext = org_file_name.substring(dot);
	            } else {
	                body = org_file_name;
	                ext = "";
	            }
	            
	            // 저장되는 파일명은 타임값에 랜덤 숫자를 더해서 만들어준다. 
//	    		Random r = new Random();
//	    		int k = r.nextInt(100000000);
//	    		
//	    		String saveFileName = Long.toString(new Date().getTime() /1000+ k)+ext;
	    		
	            //logger.debug("insertNoticeInfo file_size : " + file_size);
	            
	            File realFile = new File(realpath + File.separator + org_file_name);
	            //realFile = new FileRenamePolicy().rename(realFile);
	            real_file_name = realFile.getName();
	            
	            // 업로드된 파일을 다른 이름으로 복사(System 타임을 사용)
	            File destination = new File(realpath + File.separator + real_file_name);
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	            
	    		
	        }
		
		}catch(Exception e){
			logger.error(e.toString());
			throw e;
		}    
	}
	
}
