package com.visangesl.tree.upload.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.mapper.NoteMapper;
import com.visangesl.tree.upload.service.NoteService;
import com.visangesl.tree.upload.vo.NoteInfoConditionVo;
import com.visangesl.tree.vo.VSObject;

@Service
public class NoteServiceImpl implements NoteService {
    @Autowired
    NoteMapper noteMapper;

    @Override
    public VSObject getContentsInfoData(NoteInfoConditionVo noteInfoConditionVo) throws Exception{
        return noteMapper.getContentsInfoData(noteInfoConditionVo);
    }

}
