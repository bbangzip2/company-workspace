package com.visangesl.tree.upload.vo;

import com.visangesl.tree.vo.VSObject;

public class NoteInfoVo extends VSObject {
	// 노트 정보 저장 
	private String noteSeq;
	private String mbrId;
	private String fileInfo;
	private String regDttm; 
	private String prodSeq; // 카드 순번이 되시겠다.  
	private String page; 
	private String curPage;
	private String noteText;
	
	public String getNoteSeq() {
		return noteSeq;
	}
	public void setNoteSeq(String noteSeq) {
		this.noteSeq = noteSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getFileInfo() {
		return fileInfo;
	}
	public void setFileInfo(String fileInfo) {
		this.fileInfo = fileInfo;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getCurPage() {
		return curPage;
	}
	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}
	public String getNoteText() {
		return noteText;
	}
	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}


}
