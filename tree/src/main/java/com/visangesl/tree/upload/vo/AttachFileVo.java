package com.visangesl.tree.upload.vo;

import com.visangesl.tree.vo.VSObject;

public class AttachFileVo extends VSObject {
	// 첨부파일 정보 저장
	private String refSeq;
	private String notePage;
	private String fileNm;
	private String saveFileNm;
	private String filePath;
	private String fileType;
	private String thmbPath;
	private String thmbPath_s;
	private String thmbPath_l;
	
	private String fileGubun;
	

	public String getRefSeq() {
		return refSeq;
	}
	public void setRefSeq(String refSeq) {
		this.refSeq = refSeq;
	}
	public String getNotePage() {
		return notePage;
	}
	public void setNotePage(String notePage) {
		this.notePage = notePage;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getSaveFileNm() {
		return saveFileNm;
	}
	public void setSaveFileNm(String saveFileNm) {
		this.saveFileNm = saveFileNm;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getThmbPath() {
		return thmbPath;
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getFileGubun() {
		return fileGubun;
	}
	public void setFileGubun(String fileGubun) {
		this.fileGubun = fileGubun;
	}
	public String getThmbPath_s() {
		return thmbPath_s;
	}
	public void setThmbPath_s(String thmbPath_s) {
		this.thmbPath_s = thmbPath_s;
	}
	public String getThmbPath_l() {
		return thmbPath_l;
	}
	public void setThmbPath_l(String thmbPath_l) {
		this.thmbPath_l = thmbPath_l;
	}


}
