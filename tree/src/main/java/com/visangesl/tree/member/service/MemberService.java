package com.visangesl.tree.member.service;

import org.springframework.stereotype.Service;

import com.visangesl.tree.member.vo.Member;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.service.CommonService;

@Service
public interface MemberService extends CommonService{

	public UserSession signIn(String userId, String userPwd) throws Exception;

	public Member getNearClassSeq(Member member) throws Exception;

}
