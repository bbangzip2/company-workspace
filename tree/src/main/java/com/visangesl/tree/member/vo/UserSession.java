package com.visangesl.tree.member.vo;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.visangesl.tree.constant.Tree_Constant;

public class UserSession implements Serializable {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     *
     */
    private static final long serialVersionUID = 1552844010665234135L;

    private String memberId = null;
    private String memberName = null;
    private String profileImgPath = null;
    private String useYn = null;
    private String name = null;
    private String mbrGrade = null;
    private String classRelayIp = null;
    private String todayYmd = null;
    private String serverPath = null;


    private String campNm = null;

    private String nickname = null;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getProfileImgPath() {
        if (profileImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + profileImgPath;
        } else {
            return profileImgPath;
        }
//        return Tree_Constant.SITE_FULL_URL + profileImgPath;
    }

    public void setProfileImgPath(String profileImgPath) {
        this.profileImgPath = profileImgPath;
    }

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMbrGrade() {
		return mbrGrade;
	}

	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}

	public String getClassRelayIp() {
		return classRelayIp;
	}

	public void setClassRelayIp(String classRelayIp) {
		this.classRelayIp = classRelayIp;
	}

    public String getCampNm() {
        return campNm;
    }

    public void setCampNm(String campNm) {
        this.campNm = campNm;
    }

    public String getTodayYmd() {
        return todayYmd;
    }

    public void setTodayYmd(String todayYmd) {
        this.todayYmd = todayYmd;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

}
