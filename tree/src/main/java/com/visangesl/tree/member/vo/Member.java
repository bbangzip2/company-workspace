package com.visangesl.tree.member.vo;

import java.io.Serializable;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class Member extends VSObject implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1552844010665234135L;


    private String mbrId;
    private String pwd;
    private String nm;
    private String nickname;
    private String email;
    private String sexsect;
    private String bymd;
    private String telNo;
    private String useYn;
    private String regDttm;
    private String modDttm;
    private String quitDt;
    private String profilePhotopath;
    private String memo;
    private String mbrGrade;
    private String mbrStatus;
    private String mbrGubun;
    private String nation;
    private String director;
    private String homepage;
    private String prorvince;

    private String campNm;
    private String clsSeq;


	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSexsect() {
		return sexsect;
	}
	public void setSexsect(String sexsect) {
		this.sexsect = sexsect;
	}
	public String getBymd() {
		return bymd;
	}
	public void setBymd(String bymd) {
		this.bymd = bymd;
	}
	public String getTelNo() {
		return telNo;
	}
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getModDttm() {
		return modDttm;
	}
	public void setModDttm(String modDttm) {
		this.modDttm = modDttm;
	}
	public String getQuitDt() {
		return quitDt;
	}
	public void setQuitDt(String quitDt) {
		this.quitDt = quitDt;
	}
	public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotopath;
        } else {
            return profilePhotopath;
        }
//	    return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getMbrGrade() {
		return mbrGrade;
	}
	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}
	public String getMbrStatus() {
		return mbrStatus;
	}
	public void setMbrStatus(String mbrStatus) {
		this.mbrStatus = mbrStatus;
	}
	public String getMbrGubun() {
		return mbrGubun;
	}
	public void setMbrGubun(String mbrGubun) {
		this.mbrGubun = mbrGubun;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getHomepage() {
		return homepage;
	}
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	public String getProrvince() {
		return prorvince;
	}
	public void setProrvince(String prorvince) {
		this.prorvince = prorvince;
	}
    public String getCampNm() {
        return campNm;
    }
    public void setCampNm(String campNm) {
        this.campNm = campNm;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }

}


