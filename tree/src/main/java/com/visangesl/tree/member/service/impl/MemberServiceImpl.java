package com.visangesl.tree.member.service.impl;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.mapper.MemberMapper;
import com.visangesl.tree.member.service.MemberService;
import com.visangesl.tree.member.vo.Member;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.service.impl.CommonServiceImpl;

@Service
public class MemberServiceImpl extends CommonServiceImpl implements MemberService {

	@Autowired
	MemberMapper memberMapper;

	@PostConstruct
	public void initiate() {
		super.setBaseMapper(memberMapper);
	}

	@Override
    @Transactional(readOnly = true)
	public UserSession signIn(String userId, String userPwd) throws Exception {
		HashMap pMap = new HashMap();
		pMap.put("mbrId", userId);
		pMap.put("pwd", userPwd);

        return memberMapper.signIn(pMap);
    }

    @Override
    public Member getNearClassSeq(Member member) throws Exception {
        return memberMapper.getNearClassSeq(member);
    }

}
