package com.visangesl.tree.member.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.classroom.service.ClassRoomService;
import com.visangesl.tree.classroom.vo.ClassRelayIp;
import com.visangesl.tree.classroom.vo.ClassRelayIpCondition;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.service.MemberService;
import com.visangesl.tree.member.vo.Member;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.util.TreeUtil;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Controller
public class MemberController implements BaseController {

    @Autowired
    MemberService service;

    @Autowired
    ClassRoomService classRoomService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 트리 어드민 로그인 페이지 이동 처리
     *
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/member/login", method = RequestMethod.GET)
    public String treeLogin(HttpServletRequest request, Model model)
            throws Exception {

        return "/member/login";
    }

    /**
     * 로그인 처리
     *
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/member/signInTree", method = RequestMethod.POST)
    public VSResult signInTree(HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {

        // String returnUrl = "/sample";
        logger.debug("===================================================");
        logger.debug("===   signIn TREE                            ===");
        logger.debug("===================================================");

        VSResult result = new VSResult();

        String userId = request.getParameter("userId");
        String userPwd = request.getParameter("userPwd");

        logger.debug("userId  : " + userId);
        logger.debug("userPwd : " + userPwd);
        logger.debug("classLocalIp : "+ request.getParameter("classLocalIp"));
        logger.debug("===================================================");

        UserSession userSession = null;

        try {

            //회원 정보 조회 (member Id/Name/Img/Nick/UseYn/Grade)
            userSession = service.signIn(userId, userPwd);

            result = new VSResult();

            if (userSession != null) {

                if (userSession.getUseYn() != null && userSession.getUseYn().equals("Y")) {
                    HttpSession session = request.getSession();

//                    session.setMaxInactiveInterval(60*60); // 60분
                    session.setAttribute("isSignIn", "Y");
                    session.setAttribute("VSUserSession", userSession);

                    logger.debug("><><><><><><>< memberId="+ userSession.getMemberId());
                    logger.debug("><><><><><><>< memberGrade="+ userSession.getMbrGrade());
                    logger.debug("><><><><><><>< TreeProperties="+ TreeProperties.getProperty("error.success.code"));


                    // ---------------------------------------------------------
                    // 접속한 서버 환경에 맞게 설정
                    TreeUtil.getServerCheck(request);
                    // ---------------------------------------------------------

                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));
                    result.setResult(userSession);


                    // 로그인 후 가까운 수업의 클래스 코드를 조회
                    String mbrGrade = userSession.getMbrGrade();
                    String mbrId = userSession.getMemberId();

                    Member member = new Member();
                    member.setMbrGrade(mbrGrade);
                    member.setMbrId(mbrId);
                    member = service.getNearClassSeq(member);

                    // 로그인 성공한 경우 relay 서버의 IP를 배포하기 위한 작업을 진행한다.
                    ClassRelayIp classRelayIp = new ClassRelayIp();
                    classRelayIp.setClsSeq(member.getClsSeq());

                    //----------------------------------------------
                    // 카드맵 리스트 호출 페이지에서 다시 한번 IP 정보를 저장함.
                    // 강사인 경우 강사의 local IP를 서버에 저장한다.
                    if (userSession.getMbrGrade() != null && userSession.getMbrGrade().equals("MG004")) {
                        String classLocalIp = request.getParameter("classLocalIp");

                        if (classLocalIp != null && !classLocalIp.equals("")) {
                            classRelayIp.setConnStat("C");
                            classRelayIp.setIp(classLocalIp);

                            VSResult<VSObject> classRelayIpResult = new VSResult<VSObject>();
                            classRelayIpResult = classRoomService.mergeClassRelayIp(classRelayIp);

                            logger.debug("><><><><><><>< IP 저장 결과 = "+ classRelayIpResult.getCode() + " | " + classRelayIpResult.getMessage());
                        }
                    }

                    // 현재 class의 local IP를 결과값에 포함한다.
                    ClassRelayIpCondition classRelayIpCondition = new ClassRelayIpCondition();
                    classRelayIpCondition.setClsSeq(classRelayIp.getClsSeq());
                    classRelayIpCondition.setConnStat("C");
                    //ClassRelayIp classRelayIpInfo = (ClassRelayIp) classRoomService.getClassRelayIp(classRelayIpCondition);
                    //userSession.setClassRelayIp(classRelayIpInfo.getIp());

                } else {
                    result.setCode("7001");
                    result.setMessage("미인증 회원");
                }
            } else {
                result.setCode("9999");
                result.setMessage("로그인 오류");
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("로그인 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 로그 아웃 처리
     *
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/member/login/logout")
    public String logOut(HttpServletRequest request, Model model)
            throws Exception {
        String viewName = "redirect:/member/login.do";
        try {
            HttpSession session = request.getSession(false);
            String memberId = null;
            String sessId = null;

            if (session != null) {
                UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

                sessId = session.getId();

                session.removeAttribute("isSignIn");
                session.removeAttribute("VSUserSession");
                // session.removeAttribute("VSMenuList");
                session.invalidate();
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }
        return viewName;
    }

    @ResponseBody
    @RequestMapping(value = "/member/selectMemberInfo", method = RequestMethod.POST)
    public VSResult selectMemberInfo(HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {

        VSResult<Member> result = new VSResult();

        String userId = request.getParameter("userId");

        logger.debug("userId  : " + userId);

        Member member = new Member();

        try {
            member.setMbrId(userId);

            member = (Member) service.getData(member);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(member);

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = "/member/selectTeacherInfo", method = RequestMethod.POST)
    public VSResult selectTeacherInfo(HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {

        VSResult<Member> result = new VSResult();

        String userId = request.getParameter("userId");

        logger.debug("userId  : " + userId);

        Member member = new Member();

        try {
            member.setMbrId(userId);

            member = (Member) service.getData(member);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(member);

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/member/signInTreeTest", method = RequestMethod.POST)
    public VSResult signInTreeTest(HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {

        // String returnUrl = "/sample";
        logger.debug("===================================================");
        logger.debug("===   signIn TREE TEST                           ===");
        logger.debug("===================================================");

        VSResult result = new VSResult();

        String userId = request.getParameter("userId");
        String userPwd = request.getParameter("userPwd");

        logger.debug("userId  : " + userId);
        logger.debug("userPwd : " + userPwd);
        logger.debug("classLocalIp : "+ request.getParameter("classLocalIp"));
        logger.debug("===================================================");

        UserSession userSession = null;

        try {

            //회원 정보 조회 (member Id/Name/Img/Nick/UseYn/Grade)
            userSession = service.signIn(userId, userPwd);

            result = new VSResult();

            if (userSession != null) {

                if (userSession.getUseYn() != null && userSession.getUseYn().equals("Y")) {
                    HttpSession session = request.getSession();

//                    session.setMaxInactiveInterval(60*60); // 60분
                    session.setAttribute("isSignIn", "Y");
                    session.setAttribute("VSUserSession", userSession);

                    logger.debug("><><><><><><>< memberId="+ userSession.getMemberId());
                    logger.debug("><><><><><><>< memberGrade="+ userSession.getMbrGrade());
                    logger.debug("><><><><><><>< TreeProperties="+ TreeProperties.getProperty("error.success.code"));


                    // ---------------------------------------------------------
                    // 접속한 서버 환경에 맞게 설정
                    TreeUtil.getServerCheck(request);
                    // ---------------------------------------------------------

                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));
                    result.setResult(userSession);


                    // 로그인 후 가까운 수업의 클래스 코드를 조회
//                    String mbrGrade = userSession.getMbrGrade();
//                    String mbrId = userSession.getMemberId();
//
//                    Member member = new Member();
//                    member.setMbrGrade(mbrGrade);
//                    member.setMbrId(mbrId);
//                    member = service.getNearClassSeq(member);
//
//                    // 로그인 성공한 경우 relay 서버의 IP를 배포하기 위한 작업을 진행한다.
//                    ClassRelayIp classRelayIp = new ClassRelayIp();
//                    classRelayIp.setClsSeq(member.getClsSeq());

                    //----------------------------------------------
                    // 카드맵 리스트 호출 페이지에서 다시 한번 IP 정보를 저장함.
                    // 강사인 경우 강사의 local IP를 서버에 저장한다.
//                    if (userSession.getMbrGrade() != null && userSession.getMbrGrade().equals("MG004")) {
//                        String classLocalIp = request.getParameter("classLocalIp");
//
//                        if (classLocalIp != null && !classLocalIp.equals("")) {
//                            classRelayIp.setConnStat("C");
//                            classRelayIp.setIp(classLocalIp);
//
//                            VSResult<VSObject> classRelayIpResult = new VSResult<VSObject>();
//                            classRelayIpResult = classRoomService.mergeClassRelayIp(classRelayIp);
//
//                            logger.debug("><><><><><><>< IP 저장 결과 = "+ classRelayIpResult.getCode() + " | " + classRelayIpResult.getMessage());
//                        }
//                    }

                    // 현재 class의 local IP를 결과값에 포함한다.
//                    ClassRelayIpCondition classRelayIpCondition = new ClassRelayIpCondition();
//                    classRelayIpCondition.setClsSeq(classRelayIp.getClsSeq());
//                    classRelayIpCondition.setConnStat("C");
                    //ClassRelayIp classRelayIpInfo = (ClassRelayIp) classRoomService.getClassRelayIp(classRelayIpCondition);
                    //userSession.setClassRelayIp(classRelayIpInfo.getIp());

                } else {
                    result.setCode("7001");
                    result.setMessage("미인증 회원");
                }
            } else {
                result.setCode("9999");
                result.setMessage("로그인 오류");
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("로그인 오류");
            e.printStackTrace();
        }

        return result;
    }
    
}
