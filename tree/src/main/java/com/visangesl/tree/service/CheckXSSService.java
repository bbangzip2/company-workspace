package com.visangesl.tree.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface CheckXSSService extends CommonService {
    public String ReplaceValue(HttpServletRequest request, String paramName, String paramValue) throws Exception;

    public String ReplaceValueNull(HttpServletRequest request, String paramName, String paramValue) throws Exception;

    public String ReplaceValueNoSingleQuote(HttpServletRequest request, String paramName, String paramValue) throws Exception;

    public Map<String, String[]> ReplaceValue(HttpServletRequest request) throws Exception;
}
