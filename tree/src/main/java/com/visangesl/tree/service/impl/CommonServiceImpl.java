package com.visangesl.tree.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.mapper.BaseMapperInterface;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.service.CommonService;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public class CommonServiceImpl implements CommonService {
	private final Log logger = LogFactory.getLog(this.getClass());
	
	public BaseMapperInterface baseMapper;
	
	@Override
    @Transactional(readOnly = true)
    public VSObject getData(VSObject vsObject) throws Exception {
		
        return getBaseMapper().getData(vsObject);
    }
    
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return getBaseMapper().getDataList(dataCondition);
    }
    
	
	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = getBaseMapper().modifyDataWithResultCodeMsg(vsObject); 
        		
        if (affectedRows < 1) {
        	
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = getBaseMapper().addDataWithResultCodeMsg(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	@Override
    @Transactional
    public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	int affectedRows = getBaseMapper().deleteDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }


	public BaseMapperInterface getBaseMapper() {
		return baseMapper;
	}


	public void setBaseMapper(BaseMapperInterface mapper) {
		baseMapper = mapper;
	}  
	
	
}

