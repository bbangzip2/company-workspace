package com.visangesl.tree.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public interface CommonService {
	
	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;
	
	// select List Data 
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;
	
	// Update
	public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// Insert 
	public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// Delete 
	public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;
		
}
