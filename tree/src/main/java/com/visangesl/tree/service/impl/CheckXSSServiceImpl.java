package com.visangesl.tree.service.impl;

import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.mapper.CommonMapper;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.util.TreeUtil;
import com.visangesl.tree.vo.SqlLog;

@Service
public class CheckXSSServiceImpl extends CommonServiceImpl implements CheckXSSService {

    
	@Autowired
	CommonMapper commonMapper;

	@PostConstruct
	public void initiate() {
		super.setBaseMapper(commonMapper);
	}
	

    @Override
    public String ReplaceValue(HttpServletRequest request, String paramName, String paramValue) {
        String returnStr = "";
        if ("".equals(TreeUtil.isNull(paramValue))) {
            return returnStr;
        }

        returnStr = paramValue;
        returnStr = TreeUtil.replace(returnStr, "'", "''");
        returnStr = returnStr.trim();

        if (CheckXSS(returnStr)) {

            returnStr = returnStr.replaceAll("(?i)script", "")
                    .replaceAll("(?i)iframe", "")
                    .replaceAll("(?i)frameset", "")
                    .replaceAll("(?i)onmouse", "")
                    .replaceAll("(?i)onerror", "")
                    .replaceAll("(?i)embed", "");

        }

        if (Check_SQLInjection(request, paramName, paramValue)) {

            returnStr = returnStr.replaceAll("(?i)select", "")
                    .replaceAll("(?i)sp_cacreate", "")
                    .replaceAll("(?i)create", "")
                    .replaceAll("(?i)delete", "")
                    .replaceAll("(?i)update", "")
                    .replaceAll("(?i)insert", "")
                    .replaceAll("(?i)drop ", "")
                    .replaceAll("(?i)drop\\(", "")              // 괄호는 패턴에서 예약 문자이므로 앞에 \\를 붙여줘야 괄호를 이용하는 패턴에서 사용할 수 있다
                    .replaceAll("(?i)execmaster", "")
                    .replaceAll("(?i)exec", "")
                    .replaceAll("(?i)xp_cmdshell", "")
                    .replaceAll("(?i)shutdown", "")
                    .replaceAll("(?i)kill", "")
                    .replaceAll("(?i)truncate", "")

                    .replaceAll("(?i)netlocalgroupadministratthens", "");

        }

        return returnStr;
    }

    @Override
    public String ReplaceValueNull(HttpServletRequest request, String paramName, String paramValue) throws Exception {
        String returnStr = "";
        if ("".equals(TreeUtil.isNull(paramValue))) {
            return null;
        }

        returnStr = paramValue;
        returnStr = TreeUtil.replace(returnStr, "'", "''");
        returnStr = returnStr.trim();

        if (CheckXSS(returnStr)) {

            returnStr = returnStr.replaceAll("(?i)script", "").replaceAll("(?i)iframe", "").replaceAll("(?i)frameset", "").replaceAll("(?i)onmouse", "").replaceAll("(?i)onerror", "").replaceAll(
                    "(?i)embed", "");

        }

        if (Check_SQLInjection(request, paramName, paramValue)) {

            returnStr = returnStr.replaceAll("(?i)select", "").replaceAll("(?i)sp_cacreate", "").replaceAll("(?i)create", "").replaceAll("(?i)delete", "").replaceAll("(?i)update", "").replaceAll(
                    "(?i)insert", "").replaceAll("(?i)drop ", "").replaceAll("(?i)drop\\(", "")              // 괄호는 패턴에서 예약 문자이므로 앞에 \\를 붙여줘야 괄호를 이용하는 패턴에서 사용할 수 있다
                    .replaceAll("(?i)execmaster", "").replaceAll("(?i)exec", "").replaceAll("(?i)xp_cmdshell", "").replaceAll("(?i)shutdown", "").replaceAll("(?i)kill", "").replaceAll("(?i)truncate",
                            "")

                    .replaceAll("(?i)netlocalgroupadministratthens", "");

        }

        return returnStr;
    }

    @Override
    public String ReplaceValueNoSingleQuote(HttpServletRequest request, String paramName, String paramValue) throws Exception {
        String returnStr = "";
        if ("".equals(TreeUtil.isNull(paramValue))) {
            return null;
        }

        returnStr = paramValue;
        returnStr = returnStr.trim();

        if (CheckXSS(returnStr)) {

            returnStr = returnStr.replaceAll("(?i)script", "")
                    .replaceAll("(?i)iframe", "")
                    .replaceAll("(?i)frameset", "")
                    .replaceAll("(?i)onmouse", "")
                    .replaceAll("(?i)onerror", "")
                    .replaceAll("(?i)embed", "");

        }

        if (Check_SQLInjection(request, paramName, paramValue)) {

            returnStr = returnStr.replaceAll("(?i)select", "")
                    .replaceAll("(?i)sp_cacreate", "")
                    .replaceAll("(?i)create", "")
                    .replaceAll("(?i)delete", "")
                    .replaceAll("(?i)update", "")
                    .replaceAll("(?i)insert", "")
                    .replaceAll("(?i)drop ", "")
                    .replaceAll("(?i)drop\\(", "")              // 괄호는 패턴에서 예약 문자이므로 앞에 \\를 붙여줘야 괄호를 이용하는 패턴에서 사용할 수 있다
                    .replaceAll("(?i)execmaster", "")
                    .replaceAll("(?i)exec", "")
                    .replaceAll("(?i)xp_cmdshell", "")
                    .replaceAll("(?i)shutdown", "")
                    .replaceAll("(?i)kill", "")
                    .replaceAll("(?i)truncate", "")

                    .replaceAll("(?i)netlocalgroupadministratthens", "");

        }

        return returnStr;
    }

    @Override
    public Map<String, String[]> ReplaceValue(HttpServletRequest request) throws Exception {
        // TODO Auto-generated method stub
        Map<String, String[]> result = request.getParameterMap();
        Iterator<String> iterator = result.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String[] val = result.get(key);
            int length = val.length;
            for (int i = 0; i < length; i++) {
                val[i] = ReplaceValue(request, key, val[i]);
            }
            result.put(key, val);
        }
        return result;
    }

    private boolean CheckXSS(String param) {
        boolean isXSS = false;
        String temp = param;
        temp = temp.toUpperCase();
        temp = TreeUtil.replace(temp, " ", "");
        temp = TreeUtil.replace(temp, "&#x09", "");
        temp = TreeUtil.replace(temp, "&#x0A", "");
        temp = TreeUtil.replace(temp, "&#x0D", "");

        if ((temp.indexOf("SCRIPT") == -1) && (temp.indexOf("IFRAME") == -1) && (temp.indexOf("FRAMESET") == -1) && (temp.indexOf("+ONMOUSEOVER=") == -1) && (temp.indexOf("ONERROR") == -1)
                && (temp.indexOf("EMBED") == -1)) {
            isXSS = false;
        } else {
            isXSS = true;
        }

        return isXSS;
    }

    private boolean Check_SQLInjection(HttpServletRequest request, String paramName, String paramValue) {
        boolean Check = false;
        int cnt = 0;
        String checkcode = "SIJ";
        String temp = paramValue;

        temp = temp.toUpperCase();
//-----------------------------------------------------------------------        
//수정자 : 김남배 (2013.01.27) - 1,3 조건으로 관리자 입력단 오류로 조건 완화 ' 하나만 체
//-----------------------------------------------------------------------        
//        if ((temp.indexOf("'") > -1) || (temp.indexOf("\"\"") > -1)) {
//-----------------------------------------------------------------------        
        if (temp.indexOf("'") > -1) {
            cnt = cnt + 1;
            checkcode = checkcode + ",1";
        }
        if (temp.indexOf(" OR ") > -1) {
            cnt = cnt + 2;
            checkcode = checkcode + ",2";
        }
        if (temp.indexOf("--") > -1) {
            cnt = cnt + 2;
            checkcode = checkcode + ",3";
        }

        if ((temp.indexOf("SELECT") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",4";
        }

        if ((temp.indexOf("CREATE") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",5";
        }

        if ((temp.indexOf("DELETE") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",6";
        }

        if ((temp.indexOf("UPDATE") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",7";
        }

        if ((temp.indexOf("INSERT") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",8";
        }

        if ((temp.indexOf("DROP") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",9";
        }

        if ((temp.indexOf("EXEC") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",10";
        }

        if (temp.indexOf("XP_CMDSHELL") > -1) {
            cnt = cnt + 3;
            checkcode = checkcode + ",11";
        }

        if (temp.indexOf("SP_OACREATE") > -1) {
            cnt = cnt + 3;
            checkcode = checkcode + ",12";
        }

        if ((temp.indexOf("SHUTDOWN") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",13";
        }

        if ((temp.indexOf("ISHOTKILL") > -1) && (temp.indexOf("KILL") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",14";
        }

        if ((temp.indexOf("TRUNCATE") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",15";
        }

        if (temp.indexOf("EXECMASTER") > -1) {
            cnt = cnt + 3;
            checkcode = checkcode + ",16";
        }

        if (temp.indexOf("NETLOCALGROUPADMINISTRATTHENS") > -1) {
            cnt = cnt + 3;
            checkcode = checkcode + ",17";
        }

        if ((temp.indexOf("DROP") > -1) && ((temp.indexOf(" ") > -1) || (temp.indexOf("%20") > -1))) {
            cnt = cnt + 3;
            checkcode = checkcode + ",18";
        }

        if (cnt < 3) {            // 정상
            Check = false;
        } else {
            Check = true;
            // 로그 생성
            String Remote_IP = request.getRemoteAddr();
            String Server_IP = request.getLocalAddr();
            String Server_Name = request.getServerName();
            String Join_Link = request.getRequestURI().trim();
            String port_no = request.getServerPort() == 80 ? "" : String.valueOf(request.getServerPort());
            String scheme = request.getScheme();

            // temp는 위에서 toUpperCase() 함수를 통하여 대문자로 변환이 되어버렸기 때문에 사용자가 입력한 값이 유지가 되지 않은 상태다
            // 그래서 paramValue값을 다시 넣음으로써 사용자가 입력한 값으로 환원한 뒤에 replace를 해준다
            temp = paramValue;
            temp = temp.replaceAll("--", "**").replaceAll(";", "|").replaceAll("'", "''");

            
            SqlLog sqlLog = new SqlLog();
            sqlLog.setRemote_ip(Remote_IP);
            sqlLog.setServer_ip(Server_IP);
            sqlLog.setServer_name(Server_Name);
            sqlLog.setJoin_link(Join_Link);
            sqlLog.setParam_name(paramName);
            sqlLog.setTemp(temp);
            sqlLog.setCheckcode(checkcode);
            sqlLog.setPort_no(port_no);
            sqlLog.setScheme(scheme);
            
            commonMapper.insertSQLINJECTION_LOG(sqlLog);
        }

        return Check;
    }

}
