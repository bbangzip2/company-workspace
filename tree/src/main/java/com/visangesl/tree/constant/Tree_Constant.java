package com.visangesl.tree.constant;

import java.net.InetAddress;

public class Tree_Constant {


    public static final String RESULT_SUCCESS = "0000";
    public static final String RESULT_FAIL = "9999";

    // TEST

    //- 내부 10.
    public static final String TEST_IN_SITE_DOMAIN = "http://10.30.0.246";

    // 외부 106.
    public static final String TEST_OUT_SITE_DOMAIN = "http://106.241.5.246";

    public static final String TEST_SITE_PORT = "9090";
    public static final String TEST_SITE_CDN_PATH = "/VisangNAS/tree";


    // REAL
    /*
    public static final String SITE_DOMAIN = "http://api.treebooster.com";
//    public static final String SITE_URL = "http://" + SITE_DOMAIN;
    public static final String SITE_CDN_PATH = ""; // 다운로드 받을때 서버의 폴더명
    public static final String CDN_DOMAIN = "http://cdn.treebooster.com";
    */

    // China
    public static final String SERVER_IP = getServerIP();
    public static final String SITE_DOMAIN = "http://" + SERVER_IP;
  public static final String SITE_CDN_PATH = ""; // 다운로드 받을때 서버의 폴더명
  public static final String TEST_CDN_DOMAIN = "http://tstcdn.treebooster.com";
  public static final String CDN_DOMAIN = "http://cdn.treebooster.com";



    public static String SITE_URL = "";
    public static String SITE_FULL_URL = "";
    public static String SITE_PORT = "80";

    public static final String SECURE_SITE_URL = "https://" + SITE_DOMAIN;

    public static final String CDN_DOMAIN_STREAM = "";
    public static final String LP_DOMAIN = ""; // 교안 도메인

    public static final String VIDEOTYPE = "FT201";            // VS_CODE 테이블에서 REF_CODE가 FT200인 동영상
    public static final String AUDIOTYPE = "FT202";            // VS_CODE 테이블에서 REF_CODE가 FT200인 오디오
    public static final String IMAGETYPE = "FT203";            // VS_CODE 테이블에서 REF_CODE가 FT200인 이미지
    public static final String FLASHTYPE = "FT204";            // VS_CODE 테이블에서 REF_CODE가 FT200인 플래시
    public static final String DOCTYPE = "FT205";              // VS_CODE 테이블에서 REF_CODE가 FT200인 문서
    public static final String URLTYPE = "FT206";              // VS_CODE 테이블에서 REF_CODE가 FT200인 URL
    public static final String ETCTYPE = "FT207";              // VS_CODE 테이블에서 REF_CODE가 FT200인 기타
    public static final String PROFILE = "FT208";              // VS_CODE 테이블에서 REF_CODE가 FT200인 참고자료
    public static final String THREEDTYPE = "FT209";           // VS_CODE 테이블에서 REF_CODE가 FT200인 3D

    private static String getServerIP() {
    	String serverIP = null;
    	try {
    		serverIP = InetAddress.getLocalHost().getHostAddress();
    	} catch (Exception e) {

    	}

    	return serverIP;
    }
}
