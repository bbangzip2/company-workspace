package com.visangesl.tree.bookshelf.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.bookshelf.service.BookshelfService;
import com.visangesl.tree.mapper.BookshelfMapper;
import com.visangesl.tree.service.impl.CommonServiceImpl;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public class BookshelfServiceImpl extends CommonServiceImpl implements BookshelfService {

	@Autowired
	BookshelfMapper bookshelfMapper;

	@PostConstruct
	public void initiate() {
		super.setBaseMapper(bookshelfMapper);
	}

	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getBookList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getBookList(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getLessonList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getLessonList(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getSearchLessonList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getSearchLessonList(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getBookshelfClassList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getBookshelfClassList(dataCondition);
    }	
}
