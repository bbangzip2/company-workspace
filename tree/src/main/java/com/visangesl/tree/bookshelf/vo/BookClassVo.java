package com.visangesl.tree.bookshelf.vo;

import com.visangesl.tree.vo.VSObject;

public class BookClassVo extends VSObject {

    private String clsSeq;
    private String clsNm;


    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }

}
