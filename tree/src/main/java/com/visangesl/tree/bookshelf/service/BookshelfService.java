package com.visangesl.tree.bookshelf.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.service.CommonService;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public interface BookshelfService extends CommonService {

	// select Book List Data - 책의 데이터 리스트를 가져온다.
	public List<VSObject> getBookList(VSCondition dataCondition) throws Exception;
	
	// select Lesson List Data - 책의 Lesson 데이터 리스트 조회.
	public List<VSObject> getLessonList(VSCondition dataCondition) throws Exception;
	
	// select Lesson List Data - 특정 조건의 Lesson 데이터 리스트 조회.
	public List<VSObject> getSearchLessonList(VSCondition dataCondition) throws Exception;
	
	//  신규 API 레슨 목록 선택시 해당 클래스 목록 조회 
	public List<VSObject> getBookshelfClassList(VSCondition dataCondition) throws Exception;
}
