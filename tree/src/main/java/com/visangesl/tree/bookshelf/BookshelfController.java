package com.visangesl.tree.bookshelf;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.bookshelf.service.BookshelfService;
import com.visangesl.tree.bookshelf.vo.BookshelfCondition;
import com.visangesl.tree.bookshelf.vo.BookshelfVo;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Controller
public class BookshelfController implements BaseController {

    @Autowired
    private BookshelfService bookshelfService;

    @Autowired
    CheckXSSService xsssvc;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final VSResult result = new VSResult();

    @ResponseBody
    @RequestMapping(value = "/bookshelf/bookList")
    public VSResult getBookList(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "memberType", required = false, defaultValue = "") String mbrType)
            throws Exception {

        VSResult result = new VSResult();

        try {
            if (mbrType.compareTo("S") == 0 || mbrType.compareTo("T") == 0) {
                BookshelfCondition condition = new BookshelfCondition();
                condition.setMemberId(mbrId);
                condition.setMemberType(mbrType);

                List<VSObject> bookList = bookshelfService.getBookList(condition);

                result.setCode("0000");
                result.setMessage("성공");
                result.setResult(bookList);
            } else {
                result.setCode("9000");
                result.setMessage("memberType의 값이 잘못되었습니다.");
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/bookshelf/lessonList")
    public VSResult getLessonList(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "prodSeq", required = false, defaultValue = "") String prodSeq)
            throws Exception {

        VSResult result = new VSResult();

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session
                .getAttribute("VSUserSession");
        String mbrGrade = "";

        if (userSession != null) {
            mbrId = userSession.getMemberId();
            mbrGrade = userSession.getMbrGrade();
        }

        try {
            BookshelfCondition condition = new BookshelfCondition();
            condition.setMemberId(mbrId);
            condition.setProdSeq(prodSeq);
            condition.setMbrGrade(mbrGrade);

            List<VSObject> bookLessonList = bookshelfService.getLessonList(condition);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(bookLessonList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/bookshelf/bookLessonInfo")
    public VSResult getBookLessonInfo(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "code", required = false, defaultValue = "") String prodSeq)
            throws Exception {

        VSResult result = new VSResult();

        try {
            BookshelfCondition condition = new BookshelfCondition();
            condition.setProdSeq(prodSeq);

            BookshelfVo bookshelfVo = (BookshelfVo) bookshelfService.getData(condition);
            // StringBuffer sb = new StringBuffer();
            // sb.append("Level/ No:L1_①<br/>");
            // sb.append("Author: Judith Bauer Stamper<br/>");
            // sb.append("Illustrator: Mavis Smith<br/>");
            // sb.append("Genre: Sight Word Picture Book<br/>");
            // sb.append("Publisher: Scholastics<br/>");
            // sb.append("Target Word: good-bye, cat, hat, bat, mat, rat, like, put on<br/>");
            // sb.append("Target Expressions: I am a cat. I see a hat. I put on the hat. The hat is for me.<br/>");
            // sb.append("Summary: A cat, a rat and a bat all love the hat. Find out how Al solves this problem. Have fun!");
            //
            // if (code.equals("book")) {
            // bookshelfVo.setLessonCd("1");
            // bookshelfVo.setLessonNm("BoosterBook");
            // bookshelfVo.setThmbPath("http://10.30.0.246:9090/images/dummy/book/book_03.jpg");
            // bookshelfVo.setComment(sb.toString());
            // } else {
            // bookshelfVo.setLessonCd("1");
            // bookshelfVo.setLessonNm("A-1. The Hat");
            // bookshelfVo.setThmbPath("http://10.30.0.246:9090/images/dummy/book/lesson_03.jpg");
            // bookshelfVo.setComment(sb.toString());
            // }

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(bookshelfVo);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value = "/bookshelf/bookshelfSearch")
    public VSResult getBookshelfSearch(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "memberType", required = false, defaultValue = "") String mbrType,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword)
            throws Exception {

        VSResult result = new VSResult();

        try {
            HttpSession session = request.getSession();
            UserSession userSession = (UserSession) session
                    .getAttribute("VSUserSession");
            String mbrGrade = "";

            if (userSession != null) {
                mbrId = userSession.getMemberId();
                mbrGrade = userSession.getMbrGrade();
            }

            BookshelfCondition condition = new BookshelfCondition();
            condition.setMemberId(mbrId);
            condition.setMemberType(mbrType);
            condition.setKeyword(keyword);
            condition.setMbrGrade(mbrGrade);

            List<VSObject> bookLessonList = bookshelfService
                    .getSearchLessonList(condition);

            // BookshelfVo bookshelfVo = new BookshelfVo();
            // bookshelfVo.setLessonCd("1");
            // bookshelfVo.setLessonNm("A-1 Run");
            // bookshelfVo.setThmbPath("http://img.naver.net/static/www/u/2013/0731/nmms_224940510.gif");
            // bookshelfVo.setInLessonYn("Y");
            // bookLessonList.add(bookshelfVo);
            //
            // bookshelfVo = new BookshelfVo();
            // bookshelfVo.setLessonCd("2");
            // bookshelfVo.setLessonNm("A-2 WORKING DOG");
            // bookshelfVo.setThmbPath("http://img.naver.net/static/www/u/2013/0731/nmms_224940510.gif");
            // bookshelfVo.setInLessonYn("Y");
            // bookLessonList.add(bookshelfVo);
            //
            // bookshelfVo = new BookshelfVo();
            // bookshelfVo.setLessonCd("3");
            // bookshelfVo.setLessonNm("A-3 A HARD");
            // bookshelfVo.setThmbPath("http://img.naver.net/static/www/u/2013/0731/nmms_224940510.gif");
            // bookshelfVo.setInLessonYn("N");
            // bookLessonList.add(bookshelfVo);
            //
            // bookshelfVo = new BookshelfVo();
            // bookshelfVo.setLessonCd("4");
            // bookshelfVo.setLessonNm("A-4 WORKING");
            // bookshelfVo.setThmbPath("http://img.naver.net/static/www/u/2013/0731/nmms_224940510.gif");
            // bookshelfVo.setInLessonYn("N");
            // bookLessonList.add(bookshelfVo);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(bookLessonList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }
    
    /**
     * book shelf 에서 레슨 선택시 보여질 현재 진행중인 클래스 목록 조회 (2014-06-30 추가함. )
     * @param request
     * @param response
     * @param mbrId
     * @param prodSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/bookshelf/getBookshelfClassList")
    public VSResult getBookshelfClassList(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "prodSeq", required = false, defaultValue = "") String prodSeq)
            throws Exception {

        VSResult result = new VSResult();

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session
                .getAttribute("VSUserSession");
        String mbrGrade = "";
        String mbrId = "";

        if (userSession != null) {
            mbrId = userSession.getMemberId();
            mbrGrade = userSession.getMbrGrade();
        }

        try {
            BookshelfCondition condition = new BookshelfCondition();
            condition.setMemberId(mbrId);
            condition.setProdSeq(prodSeq);
            condition.setMbrGrade(mbrGrade);

            List<VSObject> bookClassList = bookshelfService.getBookshelfClassList(condition);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(bookClassList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }
    
}
