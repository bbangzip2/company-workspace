package com.visangesl.tree.bookshelf.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class BookshelfVo extends VSObject {

    private String prodSeq;
    private String prodTitle;
    private String lessonCd;
    private String lessonNm;
    private String thmbPath;
    private String imgPath;
    private String regDttm;
    private String modDttm;
    private String dtlCntt;
    private String filePath;

    private String comment;

    private String inLessonYn;

    private String dayNoCd;
    private String clsSeq;
    private String clsNm;

    public String getProdSeq() {
        return prodSeq;
    }


    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }


    public String getProdTitle() {
        return prodTitle;
    }


    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }


    public String getLessonCd() {
        return lessonCd;
    }


    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }


    public String getLessonNm() {
        return lessonNm;
    }


    public void setLessonNm(String lessonNm) {
        this.lessonNm = lessonNm;
    }


    public String getThmbPath() {
        if (thmbPath != null) {
            return Tree_Constant.SITE_FULL_URL + thmbPath;
        } else {
            return thmbPath;
        }
//        return thmbPath;
    }


    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }


    public String getImgPath() {
        if (imgPath != null) {
            return Tree_Constant.SITE_FULL_URL + imgPath;
        } else {
            return imgPath;
        }
//        return imgPath;
    }


    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }


    public String getRegDttm() {
        return regDttm;
    }


    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }


    public String getModDttm() {
        return modDttm;
    }


    public void setModDttm(String modDttm) {
        this.modDttm = modDttm;
    }


    public String getDtlCntt() {
		return dtlCntt;
	}


	public void setDtlCntt(String dtlCntt) {
		this.dtlCntt = dtlCntt;
	}


	public String getFilePath() {
        if (filePath != null) {
            return Tree_Constant.SITE_FULL_URL + filePath;
        } else {
            return filePath;
        }
//	    return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String getComment() {
        return comment;
    }


    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getInLessonYn() {
        return inLessonYn;
    }


    public void setInLessonYn(String inLessonYn) {
        this.inLessonYn = inLessonYn;
    }


    public String getDayNoCd() {
        return dayNoCd;
    }


    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }


    public String getClsSeq() {
        return clsSeq;
    }


    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }


    public String getClsNm() {
        return clsNm;
    }


    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }



}
