package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSListCondition;
import com.visangesl.tree.vo.VSObject;

public class PortfolioCondition extends VSListCondition {
	 private String clsSeq;
	    private String inclsRsltSeq;
	    private String mbrId;
	    
	    private String lessonCd;

	    private String prodTitle;
	    private String dayNo;
	    private String prodSeq;

	    private String cmmtCnt;
	    private String likeCnt;
	    private String portfolioCnt;
	    private String badgeCnt;
	    private String badgeCd;
	    private String badgeImg;

	    private String nm;
	    private String profilePhotoPath;
	    private String clsNm;
        
	    private String prodSect;
	    private String prodType;
	    
	    
	    
		public String getClsSeq() {
			return clsSeq;
		}
		public void setClsSeq(String clsSeq) {
			this.clsSeq = clsSeq;
		}
		public String getInclsRsltSeq() {
			return inclsRsltSeq;
		}
		public void setInclsRsltSeq(String inclsRsltSeq) {
			this.inclsRsltSeq = inclsRsltSeq;
		}
		public String getMbrId() {
			return mbrId;
		}
		public void setMbrId(String mbrId) {
			this.mbrId = mbrId;
		}
		public String getLessonCd() {
			return lessonCd;
		}
		public void setLessonCd(String lessonCd) {
			this.lessonCd = lessonCd;
		}
		public String getProdTitle() {
			return prodTitle;
		}
		public void setProdTitle(String prodTitle) {
			this.prodTitle = prodTitle;
		}
		public String getDayNo() {
			return dayNo;
		}
		public void setDayNo(String dayNo) {
			this.dayNo = dayNo;
		}
		public String getProdSeq() {
			return prodSeq;
		}
		public void setProdSeq(String prodSeq) {
			this.prodSeq = prodSeq;
		}
		public String getCmmtCnt() {
			return cmmtCnt;
		}
		public void setCmmtCnt(String cmmtCnt) {
			this.cmmtCnt = cmmtCnt;
		}
		public String getLikeCnt() {
			return likeCnt;
		}
		public void setLikeCnt(String likeCnt) {
			this.likeCnt = likeCnt;
		}
		public String getPortfolioCnt() {
			return portfolioCnt;
		}
		public void setPortfolioCnt(String portfolioCnt) {
			this.portfolioCnt = portfolioCnt;
		}
		public String getBadgeCnt() {
			return badgeCnt;
		}
		public void setBadgeCnt(String badgeCnt) {
			this.badgeCnt = badgeCnt;
		}
		public String getBadgeCd() {
			return badgeCd;
		}
		public void setBadgeCd(String badgeCd) {
			this.badgeCd = badgeCd;
		}
		public String getBadgeImg() {
			return badgeImg;
		}
		public void setBadgeImg(String badgeImg) {
			this.badgeImg = badgeImg;
		}
		public String getNm() {
			return nm;
		}
		public void setNm(String nm) {
			this.nm = nm;
		}
		public String getProfilePhotoPath() {
			return profilePhotoPath;
		}
		public void setProfilePhotoPath(String profilePhotoPath) {
			this.profilePhotoPath = profilePhotoPath;
		}
		public String getClsNm() {
			return clsNm;
		}
		public void setClsNm(String clsNm) {
			this.clsNm = clsNm;
		}
		public String getProdSect() {
			return prodSect;
		}
		public void setProdSect(String prodSect) {
			this.prodSect = prodSect;
		}
		public String getProdType() {
			return prodType;
		}
		public void setProdType(String prodType) {
			this.prodType = prodType;
		}
	    
	    
	    
	    
}
