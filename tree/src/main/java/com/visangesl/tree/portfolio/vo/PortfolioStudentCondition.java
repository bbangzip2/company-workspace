package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

public class PortfolioStudentCondition extends VSCondition {
	private String mbrId;
	private int clsSeq;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public int getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(int clsSeq) {
		this.clsSeq = clsSeq;
	}
	
	
}
