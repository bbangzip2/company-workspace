package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.constant.Tree_Constant;

public class ClassPortfolioListInfo extends PortfolioListInfo {
	private String mbrId;
	private String mbrNm;
	private String profilePhotopath;
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getMbrNm() {
		return mbrNm;
	}
	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}
	public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotopath;
        } else {
            return profilePhotopath;
        }
//		return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}


}
