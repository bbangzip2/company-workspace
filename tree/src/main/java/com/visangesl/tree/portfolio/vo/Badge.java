package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSObject;

public class Badge extends VSObject {
	private int badgeSeq;
	private String badgeType;
	private int badgeCd;
	private String badgeNm;
	
	public int getBadgeSeq() {
		return badgeSeq;
	}
	public void setBadgeSeq(int badgeSeq) {
		this.badgeSeq = badgeSeq;
	}
	public String getBadgeType() {
		return badgeType;
	}
	public void setBadgeType(String badgeType) {
		this.badgeType = badgeType;
	}
	public int getBadgeCd() {
		return badgeCd;
	}
	public void setBadgeCd(int badgeCd) {
		this.badgeCd = badgeCd;
	}
	public String getBadgeNm() {
		return badgeNm;
	}
	public void setBadgeNm(String badgeNm) {
		this.badgeNm = badgeNm;
	}
	
	
}
