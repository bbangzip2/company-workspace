package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

public class ClassStudentResultCondition extends VSCondition {
	private int classStudentResultSeq;
	private String myMbrId;

	public int getClassStudentResultSeq() {
		return classStudentResultSeq;
	}

	public void setClassStudentResultSeq(int classStudentResultSeq) {
		this.classStudentResultSeq = classStudentResultSeq;
	}

	public String getMyMbrId() {
		return myMbrId;
	}

	public void setMyMbrId(String myMbrId) {
		this.myMbrId = myMbrId;
	}
	
	
}
