package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

public class TeacherCondition extends VSCondition {
	private String mbrId;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	
	
}
