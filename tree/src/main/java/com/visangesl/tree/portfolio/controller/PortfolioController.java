package com.visangesl.tree.portfolio.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.portfolio.service.PortfolioService;
import com.visangesl.tree.portfolio.vo.ClassStudentResultCondition;
import com.visangesl.tree.portfolio.vo.InquiryPortfolio;
import com.visangesl.tree.portfolio.vo.PortfolioCondition;
import com.visangesl.tree.portfolio.vo.PortfolioListCondition;
import com.visangesl.tree.portfolio.vo.PortfolioStudentCondition;
import com.visangesl.tree.portfolio.vo.PortfolioVo;
import com.visangesl.tree.portfolio.vo.TeacherCondition;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.util.TreeUtil;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

/**
 * PortfolioController Version - 1.0 Copyright
 */
@Controller
public class PortfolioController implements BaseController {

    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    CheckXSSService xsssvc;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final VSResult result = new VSResult();

    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioLesson")
    public VSResult getPortfolioLesson(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNo,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq)
            throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrGrade = userSession.getMbrGrade();

        VSResult result = new VSResult();
        PortfolioVo portfolioVo = new PortfolioVo();
        HashMap pMap = new HashMap();

        try {

            PortfolioCondition condition = new PortfolioCondition();
            condition.setClsSeq(clsSeq);

            // 쓰나??
            condition.setDayNo(dayNo);

            List<VSObject> PortfolioLesson = portfolioService.getPortfolioLesson(condition);
            result.setResult(PortfolioLesson);
            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioDay")
    public VSResult getPortfolioDay(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNo,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq)
            throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrGrade = userSession.getMbrGrade();

        VSResult result = new VSResult();
        PortfolioVo portfolioVo = new PortfolioVo();
        HashMap pMap = new HashMap();

        try {

            PortfolioCondition condition = new PortfolioCondition();
            condition.setLessonCd(lessonCd);

            List<VSObject> PortfolioDay = portfolioService
                    .getPortfolioDay(condition);
            result.setResult(PortfolioDay);
            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioList")
    public VSResult<Object> getPortfolioList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false) String tmpClsSeq,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String tmpDayNo,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId)
            throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        VSResult<Object> result = new VSResult<Object>();
        PortfolioListCondition condition = new PortfolioListCondition();

        int clsSeq = TreeUtil.isNumber(tmpClsSeq, -1);
        int dayNo = TreeUtil.isNumber(tmpDayNo, -1);
        logger.debug("clsSeq==" + clsSeq);

        try {

            condition.setClsSeq(clsSeq);
            condition.setMbrId(mbrId);
            condition.setDayNo(dayNo);

            List<VSObject> PortfolioList = null;

            if (dayNo != -1) {
                // 특정 clsSeq에 해당하고 특정 차시에 해당하는 전체 포트폴리오 목록
                PortfolioList = portfolioService.getPortfolioClassList(condition);
            } else {
                // 특정 clsSeq에 해당하는 전체 포트폴리오 목록
                PortfolioList = portfolioService.getPortfolioList(condition);
            }

            result.setResult(PortfolioList);
            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioClassList")
    public VSResult<Object> getPortfolioClassList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = true) int clsSeq)
            throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        VSResult<Object> result = new VSResult<Object>();
        PortfolioListCondition condition = new PortfolioListCondition();

        logger.debug("clsSeq==" + clsSeq);

        try {

            condition.setClsSeq(clsSeq);

            List<VSObject> PortfolioList = portfolioService.getPortfolioClassList(condition);
            result.setResult(PortfolioList);
            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    /*
     * @ResponseBody
     *
     * @RequestMapping(value="/portfolio/portfolioInfo") public VSResult
     * getPortfolioInfo(HttpServletRequest request, HttpServletResponse
     * response,
     *
     * @RequestParam(value = "memberId", required = false, defaultValue = "")
     * String mbrId,
     *
     * @RequestParam(value = "inclsRsltSeq", required = false, defaultValue =
     * "") String inclsRsltSeq,
     *
     * @RequestParam(value = "lessonCd", required = false, defaultValue = "")
     * String lessonCd ) throws Exception {
     *
     * HttpSession session = request.getSession(); UserSession userSession =
     * (UserSession) session.getAttribute("VSUserSession");
     *
     * String mbrGrade = userSession.getMbrGrade();
     *
     * // 사용자 권한 체크 if (userSession.getMbrGrade().equals("")) {
     *
     * }
     *
     * VSResult result = new VSResult(); PortfolioVo portfolioVo = new
     * PortfolioVo(); HashMap pMap = new HashMap();
     *
     * try {
     *
     * if (!mbrId.equals("")) { // 넘어온 회원 정보가 있을 경우 학생 정보
     * portfolioVo.setMbrId("STDNT1"); portfolioVo.setNm("학생1");
     * portfolioVo.setCampNm("Beijing campus");
     * portfolioVo.setPortfolioCnt("21"); portfolioVo.setLikeCnt("135");
     * portfolioVo.setBadgeCnt("12"); } else { // 넘어온 회원 정보가 없을 경우 if
     * (userSession.getMbrGrade().equals("MG004")) { // 강사일 경우 // 레슨 별 포트폴리오 정보
     * portfolioVo.setLessonCd("2"); portfolioVo.setLessonNm("A-2.The hat");
     * portfolioVo.setProdTitle("Scholastic booster");
     * portfolioVo.setDayNo("2");
     *
     * } else { // 학생 일 경우 // 클래스 별 포트폴리오 정보 portfolioVo.setClsSeq("1");
     * portfolioVo.setClsNm("Class3"); portfolioVo.setCampNm("Beijing campus");
     * portfolioVo.setPortfolioCnt("81"); portfolioVo.setLikeCnt("435");
     * portfolioVo.setBadgeCnt("82"); } } pMap.put("profile", portfolioVo);
     *
     * // Portfolio portfolioVo = new PortfolioVo();
     *
     * if (userSession.getMbrGrade().equals("MG005") && mbrId.equals("")) {
     * portfolioVo.setNm("May Ching"); portfolioVo.setProfilePhotoPath(
     * "http://icon.daumcdn.net/w/icon/1312/19/152729032.png"); }
     * portfolioVo.setInclsRsltSeq("1"); portfolioVo.setInclsRsltImg(
     * "http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
     * portfolioVo.setInclsRsltThmb
     * ("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
     * portfolioVo.setActivitySeq("1"); portfolioVo.setActivityNm("Drawing");
     * portfolioVo
     * .setProfilePhotoPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png"
     * ); pMap.put("portfolio", portfolioVo);
     *
     * // Comment List List<VSObject> cmmtList = new ArrayList<VSObject>();
     *
     * CmmtVo cmmtVo = new CmmtVo(); cmmtVo.setCmmtSeq("1");
     * cmmtVo.setCmmtCntt("Lorem ipsum.."); cmmtVo.setLikeYn("Y");
     * cmmtVo.setRegId("STDNT1"); cmmtVo.setRegDttm("14.04.04");
     * cmmtVo.setProfilePhotopath
     * ("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
     * cmmtList.add(cmmtVo);
     *
     * cmmtVo = new CmmtVo(); cmmtVo.setCmmtSeq("1");
     * cmmtVo.setCmmtCntt("Lorem ipsum.."); cmmtVo.setLikeYn("Y");
     * cmmtVo.setRegId("STDNT1"); cmmtVo.setRegDttm("14.04.04");
     * cmmtVo.setProfilePhotopath
     * ("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
     * cmmtList.add(cmmtVo);
     *
     * pMap.put("comment", cmmtList);
     *
     * result.setResult(pMap); result.setCode("0000"); result.setMessage("성공");
     * } catch (Exception e) { ExceptionHandler handler = new
     * ExceptionHandler(e); result.setCode(handler.getCode());
     * result.setMessage(handler.getMessage()); e.printStackTrace(); }
     *
     * return result;
     *
     * }
     */

    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioStudent")
    public VSResult<Object> getPortfolioStudentInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = true) String mbrId)
            throws Exception {
        /*
         * HttpSession session = request.getSession(); UserSession userSession =
         * (UserSession) session.getAttribute("VSUserSession");
         *
         * String mbrId = null;
         *
         * if (userSession != null) { mbrId = userSession.getMemberId(); }
         */
        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            PortfolioStudentCondition condition = new PortfolioStudentCondition();
            condition.setMbrId(mbrId);

            result.setResult(portfolioService.getPortfolioStudentInfo(condition));

            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioStudentClasswork")
    public VSResult<Object> getPortfolioStudentClasswork(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = true) String mbrId,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "-1") String tmpClsSeq)
            throws Exception {

        /*
         * HttpSession session = request.getSession(); UserSession userSession =
         * (UserSession) session.getAttribute("VSUserSession");
         *
         * String mbrId = null;
         *
         * if (userSession != null) { mbrId = userSession.getMemberId(); }
         */
        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            int clsSeq = Integer.valueOf(tmpClsSeq);
            PortfolioStudentCondition condition = new PortfolioStudentCondition();
            condition.setMbrId(mbrId);
            condition.setClsSeq(clsSeq);

            result.setResult(portfolioService.getPortfolioStudentClassInfo(condition));

            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value = "/portfolio/getClassPortfolioNOKCount")
    public VSResult<Object> getClassPortfolioNOKCount(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrId = null;

        if (userSession != null) {
            mbrId = userSession.getMemberId();
        }

        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            TeacherCondition condition = new TeacherCondition();
            condition.setMbrId(mbrId);

            result.setResult(Integer.valueOf(portfolioService.getClassPortfolioNOKCount(condition)));

            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/portfolio/getPortfolioInfo")
    public VSResult<Object> getPortfolioInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "inclsRsltSeq", required = true) int inclsRsltSeq)
            throws Exception {

        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrId = null;

        if (userSession != null) {
            mbrId = userSession.getMemberId();
        }

        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            ClassStudentResultCondition condition = new ClassStudentResultCondition();
            condition.setClassStudentResultSeq(inclsRsltSeq);
            condition.setMyMbrId(mbrId);

            result.setResult(portfolioService.getPortfolioInfo(condition));

            if (result.getResult() != null) {
                InquiryPortfolio inquiryPortfolio = new InquiryPortfolio();
                inquiryPortfolio.setInclsRsltSeq(inclsRsltSeq);
                portfolioService.inquiryPortfolio(inquiryPortfolio);
            }

            result.setCode("0000");
            result.setMessage("성공");

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }

}