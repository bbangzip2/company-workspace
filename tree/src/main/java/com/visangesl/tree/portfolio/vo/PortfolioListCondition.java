package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

public class PortfolioListCondition extends VSCondition {
	private String mbrId;
	private int clsSeq;
	private int dayNo;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public int getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(int clsSeq) {
		this.clsSeq = clsSeq;
	}

	public int getDayNo() {
		return dayNo;
	}

	public void setDayNo(int dayNo) {
		this.dayNo = dayNo;
	}
	
	
	
}
