package com.visangesl.tree.portfolio.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.portfolio.vo.PortfolioCondition;
import com.visangesl.tree.portfolio.vo.PortfolioVo;
import com.visangesl.tree.service.CommonService;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public interface PortfolioService  extends CommonService {

	public List<VSObject> getPortfolioList(VSCondition condition);
	public List<VSObject> getPortfolioClassList(VSCondition condition);
	
	public VSObject getPortfolioStudentInfo(VSCondition condition);
	
	public VSObject getPortfolioStudentClassInfo(VSCondition condition);
	
	public List<VSObject> getPortfolioLesson(PortfolioCondition condition);

	public List<VSObject> getPortfolioDay(PortfolioCondition condition);
	
	// 포트폴리오 입력 
	public int insertPortfolioInfo(PortfolioVo portfolioVo)throws Exception;
	
	public int getClassPortfolioNOKCount(VSCondition condition);
	
	public VSObject getPortfolioInfo(VSCondition condition);
	
	public int inquiryPortfolio(VSObject object);
}
