package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class PortfolioVo extends VSObject {

    private String clsSeq;
    private String inclsRsltSeq;
    private String mbrId;

    private String inclsYmd;


    private String ActivitySeq;
    private String ActivityNm;

    private String lessonCd;
    private String lessonNm;
    private String prodTitle;
    private String dayNo;

    private String cmmtCnt;
    private String likeCnt;
    private String portfolioCnt;
    private String badgeCnt;
    private String badgeCd;
    private String badgeImg;

    private String nm;
    private String profilePhotoPath;
    private String clsNm;
    private String prodSeq;
    // 이홍 추가 포트 폴리오 insert 에서 사용
    private String clsSchdlSeq;

	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getInclsRsltSeq() {
		return inclsRsltSeq;
	}
	public void setInclsRsltSeq(String inclsRsltSeq) {
		this.inclsRsltSeq = inclsRsltSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getInclsYmd() {
		return inclsYmd;
	}
	public void setInclsYmd(String inclsYmd) {
		this.inclsYmd = inclsYmd;
	}
	public String getActivitySeq() {
		return ActivitySeq;
	}
	public void setActivitySeq(String activitySeq) {
		ActivitySeq = activitySeq;
	}
	public String getActivityNm() {
		return ActivityNm;
	}
	public void setActivityNm(String activityNm) {
		ActivityNm = activityNm;
	}
	public String getLessonCd() {
		return lessonCd;
	}
	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getDayNo() {
		return dayNo;
	}
	public void setDayNo(String dayNo) {
		this.dayNo = dayNo;
	}
	public String getCmmtCnt() {
		return cmmtCnt;
	}
	public void setCmmtCnt(String cmmtCnt) {
		this.cmmtCnt = cmmtCnt;
	}
	public String getLikeCnt() {
		return likeCnt;
	}
	public void setLikeCnt(String likeCnt) {
		this.likeCnt = likeCnt;
	}
	public String getPortfolioCnt() {
		return portfolioCnt;
	}
	public void setPortfolioCnt(String portfolioCnt) {
		this.portfolioCnt = portfolioCnt;
	}
	public String getBadgeCnt() {
		return badgeCnt;
	}
	public void setBadgeCnt(String badgeCnt) {
		this.badgeCnt = badgeCnt;
	}
	public String getBadgeCd() {
		return badgeCd;
	}
	public void setBadgeCd(String badgeCd) {
		this.badgeCd = badgeCd;
	}
	public String getBadgeImg() {
		return badgeImg;
	}
	public void setBadgeImg(String badgeImg) {
		this.badgeImg = badgeImg;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	public String getProfilePhotoPath() {
        if (profilePhotoPath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotoPath;
        } else {
            return profilePhotoPath;
        }
//	    return profilePhotoPath;
	}
	public void setProfilePhotoPath(String profilePhotoPath) {
		this.profilePhotoPath = profilePhotoPath;
	}
	public String getClsNm() {
		return clsNm;
	}
	public void setClsNm(String clsNm) {
		this.clsNm = clsNm;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getClsSchdlSeq() {
		return clsSchdlSeq;
	}
	public void setClsSchdlSeq(String clsSchdlSeq) {
		this.clsSchdlSeq = clsSchdlSeq;
	}






}
