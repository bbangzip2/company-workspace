package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSObject;

public class DayNo extends VSObject {
	private int dayNo;
	private String dayNm;
	public int getDayNo() {
		return dayNo;
	}
	public void setDayNo(int dayNo) {
		this.dayNo = dayNo;
	}
	public String getDayNm() {
		return dayNm;
	}
	public void setDayNm(String dayNm) {
		this.dayNm = dayNm;
	}
	
	
}
