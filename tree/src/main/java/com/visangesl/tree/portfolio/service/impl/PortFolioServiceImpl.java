package com.visangesl.tree.portfolio.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.mapper.PortFolioMapper;
import com.visangesl.tree.portfolio.service.PortfolioService;
import com.visangesl.tree.portfolio.vo.PortfolioCondition;
import com.visangesl.tree.portfolio.vo.PortfolioVo;
import com.visangesl.tree.service.impl.CommonServiceImpl;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public class PortFolioServiceImpl extends  CommonServiceImpl  implements PortfolioService{

    @Autowired
    PortFolioMapper portfolioMapper;

    @PostConstruct
    public void initiate() {
        super.setBaseMapper(portfolioMapper);
    }

	
	
	@Override
	public List<VSObject> getPortfolioList(VSCondition condition) {
	    return portfolioMapper.getPortfolioList(condition);
	}
	
	@Override
	public List<VSObject> getPortfolioClassList(VSCondition condition) {
	    return portfolioMapper.getPortfolioClassList(condition);
	}
	
	@Override
	public VSObject getPortfolioStudentInfo(VSCondition condition) {
		return portfolioMapper.getPortfolioStudentInfo(condition);
	}
	
	@Override
	public VSObject getPortfolioStudentClassInfo(VSCondition condition) {
		return portfolioMapper.getPortfolioStudentClassInfo(condition);
	}	

	@Override
	public List<VSObject> getPortfolioLesson(PortfolioCondition condition) {
		 return portfolioMapper.getPortfolioLesson(condition);
	}



	@Override
	public List<VSObject> getPortfolioDay(PortfolioCondition condition) {
		 return portfolioMapper.getPortfolioDay(condition);
	}
	
	@Override
	@Transactional
	public int insertPortfolioInfo(PortfolioVo portfolioVo)throws Exception{
		return portfolioMapper.insertPortfolioInfo(portfolioVo);
	}
	@Override
	public int getClassPortfolioNOKCount(VSCondition condition) {
		return portfolioMapper.getClassPortfolioNOKCount(condition);
	}
	
	@Override
	public VSObject getPortfolioInfo(VSCondition condition) {
		return portfolioMapper.getPortfolioInfo(condition);
	}
	
	@Override
	public int inquiryPortfolio(VSObject object) {
		return portfolioMapper.inquiryPortfolio(object);
	}
}
