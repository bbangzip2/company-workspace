package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class PortfolioStudentClass extends VSObject {
	private String mbrNm;
	private int clsSeq;
	private String campNm;
	private String clsNm;
	private int postCnt = 0;
	private int likeCnt = 0;
	private int badgeCnt = 0;
	private String profilePhotoPath;
	private String lessonCd;
	private String dayNoCd;

	public String getMbrNm() {
		return mbrNm;
	}

	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}

	public int getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(int clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getCampNm() {
		return campNm;
	}
	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}
	public String getClsNm() {
		return clsNm;
	}
	public void setClsNm(String clsNm) {
		this.clsNm = clsNm;
	}
	public int getPostCnt() {
		return postCnt;
	}
	public void setPostCnt(int postCnt) {
		this.postCnt = postCnt;
	}
	public int getLikeCnt() {
		return likeCnt;
	}
	public void setLikeCnt(int likeCnt) {
		this.likeCnt = likeCnt;
	}
	public int getBadgeCnt() {
		return badgeCnt;
	}
	public void setBadgeCnt(int badgeCnt) {
		this.badgeCnt = badgeCnt;
	}

    public String getProfilePhotoPath() {
        if (profilePhotoPath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotoPath;
        } else {
            return profilePhotoPath;
        }
//        return profilePhotoPath;
    }

    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }

    public String getLessonCd() {
        return lessonCd;
    }

    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }

    public String getDayNoCd() {
        return dayNoCd;
    }

    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }

}
