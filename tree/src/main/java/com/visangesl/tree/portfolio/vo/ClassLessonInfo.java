package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class ClassLessonInfo extends VSObject {
	private int lessonSeq;
	private String lessonNm;
	private String lessonThmb;

	public int getLessonSeq() {
		return lessonSeq;
	}
	public void setLessonSeq(int lessonSeq) {
		this.lessonSeq = lessonSeq;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getLessonThmb() {
        if (lessonThmb != null) {
            return Tree_Constant.SITE_FULL_URL + lessonThmb;
        } else {
            return lessonThmb;
        }
//		return lessonThmb;
	}
	public void setLessonThmb(String lessonThmb) {
		this.lessonThmb = lessonThmb;
	}


}
