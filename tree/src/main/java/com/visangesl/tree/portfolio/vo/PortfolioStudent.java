package com.visangesl.tree.portfolio.vo;

import java.util.List;

import com.visangesl.tree.campus.vo.CampusVo;
import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class PortfolioStudent extends VSObject {
	private String mbrNm;
	private int postCnt = 0;
	private int likeCnt = 0;
	private int badgeCnt = 0;
	
    private String profileImgPath;
	
	List<CampusVo> campus;

	
	public String getMbrNm() {
		return mbrNm;
	}

	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}

	public int getPostCnt() {
		return postCnt;
	}

	public void setPostCnt(int postCnt) {
		this.postCnt = postCnt;
	}

	public int getLikeCnt() {
		return likeCnt;
	}

	public void setLikeCnt(int likeCnt) {
		this.likeCnt = likeCnt;
	}

	public int getBadgeCnt() {
		return badgeCnt;
	}

	public void setBadgeCnt(int badgeCnt) {
		this.badgeCnt = badgeCnt;
	}

	public List<CampusVo> getCampus() {
		return campus;
	}

	public void setCampus(List<CampusVo> campus) {
		this.campus = campus;
	}

	public String getProfileImgPath() {
        if (profileImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + profileImgPath;
        } else {
            return profileImgPath;
        }
	}

	public void setProfileImgPath(String profileImgPath) {
		this.profileImgPath = profileImgPath;
	}

	
}
