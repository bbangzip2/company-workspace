package com.visangesl.tree.portfolio.vo;

import com.visangesl.tree.vo.VSObject;

public class InquiryPortfolio extends VSObject {
	private int inclsRsltSeq;

	public int getInclsRsltSeq() {
		return inclsRsltSeq;
	}

	public void setInclsRsltSeq(int inclsRsltSeq) {
		this.inclsRsltSeq = inclsRsltSeq;
	}
	
	
}
