package com.visangesl.tree.portfolio.vo;

import java.util.List;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class PortfolioInfo extends VSObject {
	private String mbrNm;
	private String mbrId;
	private String profilePhotopath;
	private String lessonNm;
	private String cardNm;
	private String filePath;
	private String fileType;
	private String likeYn;
	private String regDttm;
	List<Badge> badge;

	public String getMbrNm() {
		return mbrNm;
	}
	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotopath;
        } else {
            return profilePhotopath;
        }
//		return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getCardNm() {
		return cardNm;
	}
	public void setCardNm(String cardNm) {
		this.cardNm = cardNm;
	}
	public String getFilePath() {
        if (filePath != null) {
            return Tree_Constant.SITE_FULL_URL + filePath;
        } else {
            return filePath;
        }
//		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getLikeYn() {
		return likeYn;
	}
	public void setLikeYn(String likeYn) {
		this.likeYn = likeYn;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public List<Badge> getBadge() {
		return badge;
	}
	public void setBadge(List<Badge> badge) {
		this.badge = badge;
	}



}
