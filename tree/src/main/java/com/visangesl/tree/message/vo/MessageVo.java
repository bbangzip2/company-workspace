package com.visangesl.tree.message.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class MessageVo extends VSObject {

    private String msgSeq;
    private String sendMbrId;
    private String recvMbrId;
    private String msgSendDt;
    private String msgOkYn;
    private String msgType;
    private String msgTitle;
    private String msgCntt;
    private String msgImgPath;
    private String msgOkYnCnt;

    private String clsNm;
    private String mbrId;
    private String nm;
    private String profilePhotoPath;

    private String mbrGrade;
    private String clsSeq;


    public String getMsgSeq() {
        return msgSeq;
    }
    public void setMsgSeq(String msgSeq) {
        this.msgSeq = msgSeq;
    }
    public String getSendMbrId() {
        return sendMbrId;
    }
    public void setSendMbrId(String sendMbrId) {
        this.sendMbrId = sendMbrId;
    }
    public String getRecvMbrId() {
        return recvMbrId;
    }
    public void setRecvMbrId(String recvMbrId) {
        this.recvMbrId = recvMbrId;
    }
    public String getMsgSendDt() {
        return msgSendDt;
    }
    public void setMsgSendDt(String msgSendDt) {
        this.msgSendDt = msgSendDt;
    }
    public String getMsgOkYn() {
        return msgOkYn;
    }
    public void setMsgOkYn(String msgOkYn) {
        this.msgOkYn = msgOkYn;
    }
    public String getMsgType() {
        return msgType;
    }
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }
    public String getMsgTitle() {
        return msgTitle;
    }
    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }
    public String getMsgCntt() {
        return msgCntt;
    }
    public void setMsgCntt(String msgCntt) {
        this.msgCntt = msgCntt;
    }
    public String getMsgImgPath() {
        if (msgImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + msgImgPath;
        } else {
            return msgImgPath;
        }
//        return msgImgPath;
    }
    public void setMsgImgPath(String msgImgPath) {
        this.msgImgPath = msgImgPath;
    }
    public String getMsgOkYnCnt() {
        return msgOkYnCnt;
    }
    public void setMsgOkYnCnt(String msgOkYnCnt) {
        this.msgOkYnCnt = msgOkYnCnt;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getProfilePhotoPath() {
        if (profilePhotoPath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotoPath;
        } else {
            return profilePhotoPath;
        }
//        return profilePhotoPath;
    }
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }


}
