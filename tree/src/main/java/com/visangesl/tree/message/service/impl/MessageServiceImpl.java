package com.visangesl.tree.message.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.mapper.MessageMapper;
import com.visangesl.tree.message.service.MessageService;
import com.visangesl.tree.message.vo.MessageCondition;
import com.visangesl.tree.message.vo.MessageVo;
import com.visangesl.tree.service.impl.CommonServiceImpl;
import com.visangesl.tree.vo.VSObject;

@Service
public class MessageServiceImpl extends CommonServiceImpl implements MessageService {

    @Autowired
    MessageMapper messageMapper;

    @PostConstruct
    public void initiate() {
        super.setBaseMapper(messageMapper);
    }

    @Override
    public List<VSObject> getMessageList(MessageCondition messageCondition) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getMessageList(messageCondition);
    }

    @Override
    public int getNewMessageCount(MessageVo messageVo) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getNewMessageCount(messageVo);
    }

    @Override
    public List<VSObject> getClassList(MessageCondition messageCondition) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getClassList(messageCondition);
    }

    @Override
    public List<VSObject> getClassMemberMessageList(MessageCondition messageCondition) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getClassMemberMessageList(messageCondition);
    }


}
