package com.visangesl.tree.message.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.message.service.MessageService;
import com.visangesl.tree.message.vo.MessageCondition;
import com.visangesl.tree.message.vo.MessageVo;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

/**
 * MessageController
 * Version - 1.0
 * Copyright
 */
@Controller
public class MessageController implements BaseController {
    @Autowired
    private MessageService messageService;

    @Autowired
    CheckXSSService xsssvc;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private VSResult result = new VSResult();


    /**
     * getInboxList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/message/getInboxList", method=RequestMethod.POST)
    public VSResult getInboxList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "name", required = false, defaultValue = "") String nm,
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo
            ) throws Exception {

        logger.debug("getInboxList");
        logger.debug(mbrId);
        logger.debug(pageNo+"");

        int pageSize = 5;

        // 로그인 정보 조회
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrGrade = userSession.getMbrGrade();

        try {
            MessageCondition messageCondition = new MessageCondition();
            messageCondition.setMbrId(mbrId);
            messageCondition.setMbrGrade(mbrGrade);
            messageCondition.setNm(nm);
            messageCondition.setPageSize(pageSize);

            // 메시지 상세 내역 보기 일 경우 페이징 처리
            if (pageNo != 0) {
                messageCondition.setPageNo((pageNo - 1) * pageSize);
            }

            List<VSObject> messageList = messageService.getDataList(messageCondition);

            result.setResult(messageList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * getMessageList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/message/getMessageList", method=RequestMethod.POST)
    public VSResult getMessageList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String recvMbrId,
            @RequestParam(value = "senderId", required = false, defaultValue = "") String sendMbrId,
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo
            ) throws Exception {

        logger.debug("getMessageList");
        logger.debug(recvMbrId);
        logger.debug(sendMbrId);
        logger.debug(pageNo+"");

        int pageSize = 15;

        try {
            MessageCondition messageCondition = new MessageCondition();
            messageCondition.setRecvMbrId(recvMbrId);
            messageCondition.setSendMbrId(sendMbrId);
            messageCondition.setPageSize(pageSize);

            // 메시지 상세 내역 보기 일 경우 페이징 처리
            if (pageNo != 0) {
                messageCondition.setPageNo((pageNo - 1) * pageSize);
            }

            // 메시지 상세 내역 보기 일 경우 체크
            if (!sendMbrId.equals("")) {
                VSResult resultCodeMsg = new VSResult();
                // 안읽은 메시지 읽음 처리
                messageService.modifyDataWithResultCodeMsg(messageCondition);
            }

            List<VSObject> messageList = messageService.getMessageList(messageCondition);

            result.setResult(messageList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * composeMessage
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/message/composeMessage", method=RequestMethod.POST)
    public VSResult composeMessage(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String recvMbrId,
            @RequestParam(value = "senderId", required = false, defaultValue = "") String sendMbrId,
            @RequestParam(value = "msgCntt", required = false, defaultValue = "") String msgCntt,
            @RequestParam(value = "msgType", required = false, defaultValue = "") String msgType
            ) throws Exception {
        logger.debug("composeMessage");
        logger.debug(recvMbrId);
        logger.debug(sendMbrId);
        logger.debug(msgCntt);
        logger.debug(msgType);

        String msgImgPath = "";

        try {

            MessageVo messageVo = new MessageVo();
            messageVo.setSendMbrId(sendMbrId);
            messageVo.setRecvMbrId(recvMbrId);
            messageVo.setMsgCntt(msgCntt);
            messageVo.setMsgImgPath(msgImgPath);
            messageVo.setMsgType(msgType);

            result = messageService.addDataWithResultCodeMsg(messageVo);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("등록 오류");
            e.printStackTrace();
        }

        return result;
    }


    /**
     * getMessageList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/message/getClassMemberMessageList", method=RequestMethod.POST)
    public VSResult getClassMemberMessageList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "name", required = false, defaultValue = "") String nm
            ) throws Exception {

        logger.debug("getClassMemberMessageList");
        logger.debug(mbrId);
        logger.debug(clsSeq);
        logger.debug(nm);

        VSResult result = new VSResult();

        // 로그인 정보 조회
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

        String mbrGrade = userSession.getMbrGrade();

        try {
            MessageCondition messageCondition = new MessageCondition();
            messageCondition.setMbrId(mbrId);
            messageCondition.setClsSeq(clsSeq);
            messageCondition.setMbrGrade(mbrGrade);

            List<VSObject> classMemberMessageList = messageService.getClassMemberMessageList(messageCondition);

            result.setResult(classMemberMessageList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }


    /**
     * getNewMessageCount
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/message/getNewMessageCount", method=RequestMethod.POST)
    public VSResult getNewMessageCount(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String recvMbrId
            ) throws Exception {
//    public String getMessageList(HttpServletRequest request @PathVariable("memberId") String memberId, @PathVariable("messageType") String messageType) throws Exception {

        logger.debug("getNewMessageCount");
        logger.debug(recvMbrId);

        try {
            MessageVo messageVo = new MessageVo();
            messageVo.setRecvMbrId(recvMbrId);

            result.setResult(messageService.getNewMessageCount(messageVo));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("메시지 조회 오류");
            e.printStackTrace();
        }

        return result;
    }


//
//    /**
//     * getClassList
//     * Version - 1.0
//     * Copyright
//     */
//    @RequestMapping(value="/message/getClassList", method=RequestMethod.POST)
//    public VSResult getClassList(HttpServletRequest request, HttpServletResponse response,
//            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId
//            ) throws Exception {
//        logger.debug("getClassList");
//        logger.debug(mbrId);
//
//        try {
//            MessageCondition messageCondition = new MessageCondition();
//            messageCondition.setMbrId(mbrId);
//
//            List<VSObject> resultList = messageService.getClassList(messageCondition);
//            result.setResult(resultList);
//            result.setCode(TreeProperties.getProperty("error.success.code"));
//            result.setMessage(TreeProperties.getProperty("error.success.msg"));
//        } catch (Exception e) {
//            result.setCode("9999");
//            result.setMessage("메시지 조회 오류");
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    /**
//     * getClassMemberList
//     * Version - 1.0
//     * Copyright
//     */
//    @RequestMapping(value="/message/getClassMemberList", method=RequestMethod.POST)
//    public VSResult getClassMemberList(HttpServletRequest request, HttpServletResponse response,
//            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
//            @RequestParam(value = "memberNm", required = false, defaultValue = "") String nm,
//            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq
//            ) throws Exception {
//        logger.debug("getClassMemberList");
//        logger.debug(mbrId);
//        logger.debug(clsSeq);
//
//        try {
//            MessageCondition messageCondition = new MessageCondition();
//            messageCondition.setMbrId(mbrId);
//            messageCondition.setNm(nm);
//            messageCondition.setClsSeq(clsSeq);
//
//            List<VSObject> resultList = messageService.getDataList(messageCondition);
//            result.setResult(resultList);
//            result.setCode(TreeProperties.getProperty("error.success.code"));
//            result.setMessage(TreeProperties.getProperty("error.success.msg"));
//        } catch (Exception e) {
//            result.setCode("9999");
//            result.setMessage("메시지 조회 오류");
//            e.printStackTrace();
//        }
//
//        return result;
//    }
}
