package com.visangesl.tree.message.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.message.vo.MessageCondition;
import com.visangesl.tree.message.vo.MessageVo;
import com.visangesl.tree.service.CommonService;
import com.visangesl.tree.vo.VSObject;

@Service
public interface MessageService extends CommonService {

    public List<VSObject> getMessageList(MessageCondition messageCondition) throws Exception;
    public List<VSObject> getClassList(MessageCondition messageCondition) throws Exception;
    public int getNewMessageCount(MessageVo messageVo) throws Exception;
    public List<VSObject> getClassMemberMessageList(MessageCondition messageCondition) throws Exception;

}
