package com.visangesl.tree.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.HtmlUtils;

public class HtmlParserUtil {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

//    public static void main(String args[]) {
//
//        File sourceF = new File("D:\\VisangNAS\\tree\\card\\boosterBook\\L003\\C0001");
//
//        String replaceUrl = "content/boosterBook/L003/C0001";
//        String cardPath = "pathesl='content/boosterBook/L003/C0001'";
//        String path = "content";
//        String bookCd = "boosterBook";
//        String lessonCd = "L003";
//        String resultStr = "C0001";
//
//        try {
//            htmlParser(sourceF, replaceUrl, cardPath, path, bookCd, lessonCd, resultStr);
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }


//    public static boolean htmlParser(File sourceF, String replaceUrl, String cardPath) throws IOException {
    public static boolean htmlParser(File sourceF, String replaceUrl, String cardPath, String path, String bookCd, String lessonCd, String resultStr) throws IOException {

        List<String> fileList = new ArrayList<String>();
        boolean result = true;

        String filePath = "";    // 파일 경로
        String fileName = "";    // 파일 명

        String keyAttr = "pathesl";                 // 파싱 대상 키 Attr
//        replaceUrl = "http://10.30.0.246:9090";     // 파싱 값 url


        // 대상 폴더의 html/js/css 파일 리스트 추출
        File[] listFiles = sourceF.listFiles();
        for (File file : listFiles) {
            generateFileList(fileList, file);
        }


        // html/js/css 변환
        for (String htmlFile : fileList) {
            File file = new File(htmlFile);

            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            Document doc = null;

            try {
//System.out.println("file.getName(): " + file.getName());

                // pageLoad.html 파일 여부 체크
                if (file.getName().equals("pageLoad.html") || file.getName().equals("pageLoad.htm")) {
                    // doc에 pathesl 변수(카드 경로) 추가
                    doc = Jsoup.parse("<script>" + cardPath + "</script>");
                } else {
                    doc = Jsoup.parse("");
                }


                // doc에 html 파일 읽어서 붙이기
                StringBuffer sb = new StringBuffer();
                String ch;
                while((ch = bufferedReader.readLine()) != null) {

                    // pathesl 주석이 있는 줄이 있으면 다음줄을 읽어와 리플레이스 한다.
                    if (ch != null && ch.indexOf("//pathesl") > 0) {
                        // 주석이 있는 줄 저장
                        sb.append(ch + System.getProperty("line.separator"));
                        ch = bufferedReader.readLine();
System.out.println("asis: " + ch);
                        ch = replacePath(ch, path, bookCd, lessonCd, resultStr);
System.out.println("tobe: " + ch);
                    }

                    if (TreeUtil.fileUploadExt(file.getName()).equals("json")) {
                        if (ch.indexOf("../") >= 0) {
                            String temp = path + "/" + bookCd + "/" + lessonCd + "/" + resultStr + "/";
//                            System.out.println(ch);
//                            System.out.println(temp);
                            ch = ch.replaceAll("\\.\\./", temp);
                        }
                    }

                    sb.append(ch + System.getProperty("line.separator"));
                }
                doc.append(sb.toString());

//System.out.println(doc.toString());
                doc.outputSettings().prettyPrint(false);


                // html 파일 여부 체크
                if (TreeUtil.fileUploadExt(file.getName()).equals("html") || TreeUtil.fileUploadExt(file.getName()).equals("htm")) {

                    // keyElement 항목을 가진 Elements 조회
                    Elements elements = doc.select("[" + keyAttr + "]");

                    // 조회 결과 카운트 체크
                    if (elements.size() > 0) {
                        String keylVal = "";    // src / href ...
                        String attrVal = "";    // 변경될 attr의 value (img src="attrVal")

                        for (Element e : elements) {

                            boolean pathFlag = false;

                            // keyAttr 의 값 조회(pathesl="src" / pathesl="href")
                            keylVal = e.attr(keyAttr);
                            System.out.println("keyAttr: " + keyAttr + ", keylVal: " + keylVal);

                            if (keylVal != null && !keylVal.equals("")) {
                                // 변경할 Element의 attr(src/href) 내용 조회
                                attrVal = e.attr(keylVal);
                                System.out.println("replaceUrl:" + replaceUrl + ", attrVal: " + attrVal);

                                String temp = "";
                                if (attrVal.indexOf("../") >= 0) {
                                    Pattern p = Pattern.compile("\\.\\./");
                                    Matcher m = p.matcher(attrVal);

                                    int count = 0;
                                    while (m.find()) {
                                        count++;
                                    }

                                    temp = attrVal.substring(attrVal.lastIndexOf("../")+3, attrVal.length());

                                    switch (count) {
                                        case 1:
                                            temp = path + File.separator + bookCd + File.separator + lessonCd + File.separator + resultStr + File.separator + temp;
                                            break;
                                        case 2:
                                            temp = path + File.separator + bookCd + File.separator + lessonCd + File.separator + temp;
                                            break;
                                        case 3:
                                            temp = path + File.separator + bookCd + File.separator + temp;
                                            break;
                                        case 4:
                                            temp = path + File.separator + temp;
                                            break;
                                        default:
                                            break;
                                    }

                                    // 변경 값 저장
                                    e.attr(keylVal, temp);

                                    pathFlag = true;
                                }

                                if (attrVal.indexOf("./") >= 0) {
                                    Pattern p = Pattern.compile("\\./");
                                    Matcher m = p.matcher(attrVal);

                                    int count = 0;
                                    while (m.find()) {
                                        count++;
                                    }

                                    temp = attrVal.substring(attrVal.lastIndexOf("./")+2, attrVal.length());

                                    switch (count) {
                                        case 1:
                                            temp = path + File.separator + bookCd + File.separator + lessonCd + File.separator + resultStr + File.separator + temp;
                                            break;
                                        case 2:
                                            temp = path + File.separator + bookCd + File.separator + lessonCd + File.separator + temp;
                                            break;
                                        case 3:
                                            temp = path + File.separator + bookCd + File.separator + temp;
                                            break;
                                        case 4:
                                            temp = path + File.separator + temp;
                                            break;
                                        default:
                                            break;
                                    }

                                    // 변경 값 저장
                                    e.attr(keylVal, temp);

                                    pathFlag = true;
                                }

//                                // 변경 값 저장
//                                e.attr(keylVal, temp);

                                if (!pathFlag) {
                                    e.attr(keylVal, replaceUrl + attrVal);
                                }
                            }
                        }
                    }

                }


                // head 내용이 없을 경우 자동으로 삽입되는 html/head/body를 삭제!
                String head = doc.head().text();
                if (head != null && head.equals("")) {
                    doc.select("html, head, body").unwrap();
                }


                // 변환 처리된 document를 string으로 반환
                String resultHtml = HtmlUtils.htmlUnescape(doc.toString());
//              System.out.println(resultHtml);


                // 파일 저장
                File output = new File(file.getAbsolutePath());
                PrintWriter writer = new PrintWriter(output,"UTF-8");
                writer.write(resultHtml);
                writer.flush();
                writer.close();


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                result = false;
            }

        }

        return result;

    }


    public static void generateFileList(List<String> fileList, File node) {

        if (node.isFile()) {

            // 파일 확장자 확인
            String ext = TreeUtil.fileUploadExt(node.getName());

            // html 파일 경우 파싱 함수 호출
//            if (ext != null && (ext.equals("html") || ext.equals("htm") || ext.equals("js") || ext.equals("css") || ext.equals("json"))) {
            if (ext != null && (ext.equals("html") || ext.equals("htm") || ext.equals("css") || ext.equals("json"))) {
                fileList.add(node.getAbsolutePath().toString());
            }
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(fileList, new File(node, filename));
            }
        }
    }


    public static String replacePath(String pathStr, String path, String bookCd, String lessonCd, String resultStr) {

        if (pathStr.indexOf("'pathesl'") > 0) {
            pathStr = pathStr.replace("'pathesl'", "pathesl");

        } else {

            String temp = "";

            Pattern p = Pattern.compile("\\.\\./");
            Matcher m = p.matcher(pathStr);

            int count = 0;
            while (m.find()) {
                count++;
            }

            switch (count) {
                case 1:
                    temp = path + File.separator + bookCd + File.separator + lessonCd + File.separator + resultStr;
                    pathStr = pathStr.replace("..", temp);
                    break;
                case 2:
                    temp = path + File.separator + bookCd + File.separator + lessonCd;
                    pathStr = pathStr.replace("../..", temp);
                    break;
                case 3:
                    temp = path + File.separator + bookCd;
                    pathStr = pathStr.replace("../../..", temp);
                    break;
                case 4:
                    temp = path;
                    pathStr = pathStr.replace("../../../..", temp);
                    break;
                default:
                    break;
            }

        }

        return pathStr;
    }

}
