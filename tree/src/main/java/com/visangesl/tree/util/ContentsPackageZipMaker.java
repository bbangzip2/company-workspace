package com.visangesl.tree.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ContentsPackageZipMaker {

//    ContentsPackageZipMaker.packageZip(contentsFolderList, packagePath, packageFileName);
//    ContentsPackageZipMaker.packageZip(contentsFolderList, packageBasePath, packageEntryPath, packageFileName);
    public static void packageZip(List<String> filePathList, String packageFile) {

        List<String> fileList = new ArrayList<String>();

        String output = packageFile;

        for (String list : filePathList) {
            String[] file = list.split(";;"); // file[0]: 기준 코드, file[1]: 엔트리 경로, file[2]: 컨텐츠 경로
//            System.out.println("list: " + list);
//            System.out.println("0: " + file[0]);
//            System.out.println("1: " + file[1]);
//            System.out.println("2: " + file[2]);

            generateFileList(fileList, file[0], file[1], new File(file[2]));
        }

        byte[] buffer = new byte[1024];
        try {
            FileOutputStream fos = new FileOutputStream(output);
            ZipOutputStream zos = new ZipOutputStream(fos);

            System.out.println("ZipFile : " + output);
            System.out.println("압축 파일 카운트: " + fileList.size());

            // 중복 제거 (HashSet 사용) - 카드맵에 중복 카드가 적용시 카드 하나만 패키징에 적용
            fileList = new ArrayList<String>(new HashSet<String>(fileList));

            for (int i=0; i<fileList.size(); i++) {
                String[] file = fileList.get(i).split(";;");
                String fileEntry = file[0];

//                System.out.println(file[1]);
//                System.out.println("File 추가 : " + fileEntry);

                ZipEntry ze = new ZipEntry(fileEntry);

                zos.putNextEntry(ze);

                FileInputStream in = new FileInputStream(file[1]);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            zos.close();

            System.out.println("압축이 완료되었습니다.");
            System.out.println(packageFile);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void generateFileList(List<String> fileList, String splitKey, String packageEntryPath, File node) {

        if (node.isFile()) {
            String filePath = node.getAbsoluteFile().toString();
            String temp = "";

            temp = packageEntryPath + filePath.substring(filePath.indexOf(File.separator + splitKey + File.separator), filePath.length());

            fileList.add(temp + ";;" + filePath);
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(fileList, splitKey, packageEntryPath, new File(node, filename));
            }
        }

    }

//    private static String generateZipEntry(String entryPath, String file) {
//        return file.substring(entryPath.length() + 1, file.length());
//    }
}
