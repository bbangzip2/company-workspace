package com.visangesl.tree.packaging.service;

import org.springframework.stereotype.Service;

import com.visangesl.tree.contents.vo.ContentsCondition;

@Service
public interface PackageService {

    public boolean packageZipper(ContentsCondition condition) throws Exception;
}
