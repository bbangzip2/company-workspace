package com.visangesl.tree.packaging.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.contents.vo.ContentsCondition;
import com.visangesl.tree.contents.vo.ContentsVo;
import com.visangesl.tree.mapper.ContentsMapper;
import com.visangesl.tree.packaging.service.PackageService;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.util.ContentsPackageZipMaker;
import com.visangesl.tree.vo.VSObject;

@Service
public class PackageServiceImpl implements PackageService {

    @Autowired
    ContentsMapper contentsMapper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    @Transactional(rollbackFor=Exception.class)
    public boolean packageZipper(ContentsCondition condition) {

        boolean result = false;

        try {
            // 패키징 대상 데이터 리스트 조회
            List<VSObject> packageDataList = contentsMapper.getPackageDataList(condition);

            // 북/레슨 정보 조회
            String bookCd = condition.getBookCd();
            String lessonCd = condition.getLessonCd();
            String bookPath = condition.getBookPath();
            String lessonPath = condition.getLessonPath();
            String cardPath = "";
            boolean contentsType = false;
            boolean packageFlag = false;

            // 패키징 대상 & 북/레슨 정보 조회 결과 체크
            if (packageDataList != null && bookCd != null && lessonCd != null) {

                // tree file 저장 경로
                String tree_save_filepath = TreeProperties.getProperty("tree_save_filepath");
                // 컨텐츠 파일 기본 경로
                String treeContentsBase = TreeProperties.getProperty("tree_card_path");
                // 상품 파일 기본 경로
                String treePackageBase = TreeProperties.getProperty("tree_package_path");
                // 패키지 생성 경로 (북 폴더)
                String packageFilePath = tree_save_filepath + treePackageBase + File.separator + "USERS" + File.separator + condition.getMbrId() + File.separator + bookCd;

                // 관리자 등급일 경우 패키지 생성 경로를 상위로
                if (condition.getMbrGrade().equals("MG001")) {
                    packageFilePath = tree_save_filepath + treePackageBase + File.separator + bookCd;
                }

                // 패키지 파일 명 (레슨 코드)
                String packageFile = packageFilePath + File.separator + lessonCd + ".zip";

                String packageEntryPath = "";


                // 패키지 경로 체크
                if (!new File(packageFilePath).exists()) {
                    // 패키지 경로 생성
                    new File(packageFilePath).mkdirs();
                }


                // 패키징 대상 카드 리스트 생성
                List<String> contentsFolderList = new ArrayList<String>();

                for (VSObject contentList : packageDataList) {
                    ContentsVo content = (ContentsVo) contentList;

                    logger.debug("=======================================");
                    logger.debug(content.getCardCd());
                    logger.debug(content.getCardFullPath());
                    logger.debug(content.getCardPath());
                    logger.debug(content.getLessonPath());
                    logger.debug(content.getBookPath());


                    // 컨텐츠 수정하여 사용 하는 경우
                    if (content.getCardFullPath() == null || content.getCardFullPath().equals("")) {
                        packageEntryPath = File.separator + content.getBookCd() + File.separator + content.getLessonCd();
                        contentsFolderList.add(content.getCardCd() + ";;" + packageEntryPath + ";;" + tree_save_filepath + content.getFilePath());

                    } else { // 외주 컨텐츠 그대로 사용하는 경우
                      contentsType = true;

                        packageEntryPath = File.separator + content.getBookPath() + File.separator + content.getLessonPath();
                        contentsFolderList.add(content.getCardPath() + ";;" + packageEntryPath + ";;" + tree_save_filepath + treeContentsBase + content.getCardFullPath());
                    }


                    // 대상 카드 폴더 유무 체크
                    logger.debug(tree_save_filepath + content.getFilePath());
                    if (!new File(tree_save_filepath + content.getFilePath()).exists()) {
                        logger.debug("폴더가 없네");
//                        packageFlag = true;
//                        break;
                    }

                }


                if (contentsType) {
                    // 레슨 공통 폴더 추가 COMMON *********************
                    if (new File(tree_save_filepath + treeContentsBase + File.separator + bookPath + File.separator + "common").exists()) {
                        packageEntryPath = File.separator + bookPath;
                        contentsFolderList.add("common;;" + packageEntryPath + ";;" + tree_save_filepath + treeContentsBase + "/" + bookPath + "/common");
                    }

                    // 카드 공통 폴더 추가
                    if (new File(tree_save_filepath + treeContentsBase + File.separator + bookPath + File.separator + lessonPath + File.separator + "common").exists()) {
                        packageEntryPath = File.separator + bookPath + File.separator + lessonPath;
                        contentsFolderList.add("common;;" + packageEntryPath + ";;" + tree_save_filepath + treeContentsBase + "/" + bookPath + "/" + lessonPath + "/common");
                    }
                } else {
                    // 레슨 공통 폴더 추가 COMMON *********************
                    if (new File(tree_save_filepath + treeContentsBase + File.separator + bookCd + File.separator + "common").exists()) {
                        packageEntryPath = File.separator + bookCd;
                        contentsFolderList.add("common;;" + packageEntryPath + ";;" + tree_save_filepath + treeContentsBase + "/" + bookCd + "/common");
                    }

                    // 카드 공통 폴더 추가
                    if (new File(tree_save_filepath + treeContentsBase + File.separator + bookCd + File.separator + lessonCd + File.separator + "common").exists()) {
                        packageEntryPath = File.separator + bookCd + File.separator + lessonCd;
                        contentsFolderList.add("common;;" + packageEntryPath + ";;" + tree_save_filepath + treeContentsBase + "/" + bookCd + "/" + lessonCd + "/common");
                    }
                }


                if (!packageFlag) { // 대상 폴더 정상

                    // 패키징 실행
                    ContentsPackageZipMaker.packageZip(contentsFolderList, packageFile);


                    // 패키지 기존 버전 조회
                    ContentsVo contents = new ContentsVo();
                    contents = (ContentsVo) contentsMapper.getPackageVersion(condition);


                    // 패키징 파일 경로 재지정 (/package....)
                    packageFile = treePackageBase + File.separator + bookCd;


                    // 패키지 버전 저장
                    condition.setPackageFilePath(packageFile);
                    if (contents != null) {
                        logger.debug("패키지 버전업");
                        contentsMapper.updatePackageVersion(condition); // 버전 업!
                    } else {
                        logger.debug("패키지 버전 등록 및 파일 정보 등록");
                        contentsMapper.insertPackageVersion(condition); // 버전 신규 등록

                        // 패키지 생성 경로 (북 폴더)
                        String packagePath = treePackageBase + File.separator + "USERS" + File.separator + condition.getMbrId() + File.separator + bookCd;

                        // 관리자 등급일 경우 패키지 생성 경로를 상위로
                        if (condition.getMbrGrade().equals("MG001")) {
                            packagePath = treePackageBase + File.separator + bookCd;
                        }
                        condition.setPackageFilePath(packageFile);
                        contentsMapper.insertPackageFileInfo(condition); // 패키지 파일 정보 등록
                    }
                } else { // 대상 폴더 없음
                    result = false;
                }
                result = true;
            } else {
                result = false;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            result = false;
        }

        return result;
    }


//    public boolean exists(String URLName) {
//        try {
//            HttpURLConnection.setFollowRedirects(false);
//            // note : you may also need
//            //        HttpURLConnection.setInstanceFollowRedirects(false)
//            HttpURLConnection con = (HttpURLConnection) new URL(URLName).openConnection();
//            con.setRequestMethod("HEAD");
//
//            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
//        } catch (Exception e) {
//           e.printStackTrace();
//           return false;
//        }
//    }

}
