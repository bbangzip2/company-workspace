package com.visangesl.tree.badge.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public class TreasureBadgeInfo extends VSObject {
	private String badgeType;
	private int badgeSeq;
	private String badgeName;
	private String badgeMean;
	private List<BestowalMember> bestowalMbr;
	
	public String getBadgeType() {
		return badgeType;
	}
	public void setBadgeType(String badgeType) {
		this.badgeType = badgeType;
	}
	public int getBadgeSeq() {
		return badgeSeq;
	}
	public void setBadgeSeq(int badgeSeq) {
		this.badgeSeq = badgeSeq;
	}
	public String getBadgeName() {
		return badgeName;
	}
	public void setBadgeName(String badgeName) {
		this.badgeName = badgeName;
	}
	public String getBadgeMean() {
		return badgeMean;
	}
	public void setBadgeMean(String badgeMean) {
		this.badgeMean = badgeMean;
	}
	public List<BestowalMember> getBestowalMbr() {
		return bestowalMbr;
	}
	public void setBestowalMbr(List<BestowalMember> bestowalMbr) {
		this.bestowalMbr = bestowalMbr;
	}	
}