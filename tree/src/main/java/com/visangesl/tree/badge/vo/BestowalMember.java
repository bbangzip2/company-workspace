package com.visangesl.tree.badge.vo;

import com.visangesl.tree.vo.VSObject;

public class BestowalMember extends VSObject {
	private String mbrId;
	private String sexSect;
	private String nm;
	private String bestowalDate;
	
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getSexSect() {
		return sexSect;
	}
	public void setSexSect(String sexSect) {
		this.sexSect = sexSect;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	public String getBestowalDate() {
		return bestowalDate;
	}
	public void setBestowalDate(String bestowalDate) {
		this.bestowalDate = bestowalDate;
	}
	
	
}
