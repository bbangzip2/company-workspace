package com.visangesl.tree.badge.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.badge.service.BadgeService;
import com.visangesl.tree.mapper.BadgeMapper;
import com.visangesl.tree.service.impl.CommonServiceImpl;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public class BadgeServiceImpl extends CommonServiceImpl implements BadgeService {

	@Autowired
	BadgeMapper badgeMapper;

	@PostConstruct
	public void initiate() {
		super.setBaseMapper(badgeMapper);
	}

	@Override
	public List<VSObject> getBadgeList(VSCondition condition) throws Exception {
		return badgeMapper.getBadgeList(condition);
	}

}
