package com.visangesl.tree.badge.vo;

import com.visangesl.tree.vo.VSObject;

public class BadgeVo extends VSObject {

    private String badgeCd;
    private String badgeType;
    private String useYn;
    private String regId;
    private String regDttm;
    private String badgeDescr;
    private String badgeMean;
    private String badgeNm;

    private String badgeSeq;
    private String recvMbrId;
    
    private String contentSeq;
    private String contentType;


    public String getBadgeCd() {
        return badgeCd;
    }
    public void setBadgeCd(String badgeCd) {
        this.badgeCd = badgeCd;
    }
    public String getBadgeType() {
        return badgeType;
    }
    public void setBadgeType(String badgeType) {
        this.badgeType = badgeType;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
    public String getBadgeDescr() {
        return badgeDescr;
    }
    public void setBadgeDescr(String badgeDescr) {
        this.badgeDescr = badgeDescr;
    }
    public String getBadgeMean() {
        return badgeMean;
    }
    public void setBadgeMean(String badgeMean) {
        this.badgeMean = badgeMean;
    }
    public String getBadgeNm() {
        return badgeNm;
    }
    public void setBadgeNm(String badgeNm) {
        this.badgeNm = badgeNm;
    }
    public String getBadgeSeq() {
        return badgeSeq;
    }
    public void setBadgeSeq(String badgeSeq) {
        this.badgeSeq = badgeSeq;
    }
    public String getRecvMbrId() {
        return recvMbrId;
    }
    public void setRecvMbrId(String recvMbrId) {
        this.recvMbrId = recvMbrId;
    }
	public String getContentSeq() {
		return contentSeq;
	}
	public void setContentSeq(String contentSeq) {
		this.contentSeq = contentSeq;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
