package com.visangesl.tree.badge.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.service.CommonService;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public interface BadgeService extends CommonService {
	public List<VSObject> getBadgeList(VSCondition condition) throws Exception;
}
