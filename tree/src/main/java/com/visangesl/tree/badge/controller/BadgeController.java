package com.visangesl.tree.badge.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.badge.service.BadgeService;
import com.visangesl.tree.badge.vo.BadgeListCondition;
import com.visangesl.tree.badge.vo.BadgeVo;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Controller
public class BadgeController implements BaseController {

    @Autowired
    private BadgeService badgeService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//    private final VSResult result = new VSResult();

    @ResponseBody
    @RequestMapping(value="/badge/badgeList")
    public VSResult getBadgeList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = true, defaultValue = "") String recvMbrId
            ) throws Exception {

        VSResult result = new VSResult();

        try {
        	BadgeListCondition condition = new BadgeListCondition();
        	condition.setMbrId(recvMbrId);
        	
            List<VSObject> badgeList = badgeService.getBadgeList(condition);

            BadgeVo badgeVo = new BadgeVo();

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(badgeList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/badge/setBadge")
    public VSResult setBadge(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String memberId,
            @RequestParam(value = "targetId", required = false, defaultValue = "") String targetId,
            @RequestParam(value = "type", required = false, defaultValue = "") String type,
            @RequestParam(value = "code", required = false, defaultValue = "") String code,
            @RequestParam(value = "contentSeq", required = false, defaultValue = "") String contentSeq,
            @RequestParam(value = "contentType", required = false, defaultValue = "") String contentType) throws Exception {
    	VSResult result = new VSResult();

		 try {
			 String regId = null;

			 if(memberId != null || memberId.length() > 0) {
				 regId = memberId;
			 } else {
				 HttpSession session = request.getSession(false);
				 String sessId = null;
				 if(session != null) {
					 sessId = session.getId();
				}
			}

			 BadgeVo badgeVo = new BadgeVo();
			 badgeVo.setBadgeType(type);
			 badgeVo.setUseYn("Y");
			 badgeVo.setRegId(regId);
			 badgeVo.setRecvMbrId(targetId);
			 badgeVo.setBadgeCd(code);
			 badgeVo.setContentSeq(contentSeq);
			 badgeVo.setContentType(contentType);

			 badgeService.addDataWithResultCodeMsg(badgeVo);

			 BadgeVo resultVo = new BadgeVo();
			 resultVo.setBadgeType(type);
			 result.setCode("0000");
			 result.setMessage("성공");
			 result.setResult(resultVo);
		 } catch (Exception e) {
			 ExceptionHandler handler = new ExceptionHandler(e);
			 result.setCode(handler.getCode());
			 result.setMessage(handler.getMessage());
			 e.printStackTrace();
		}

		return result;
    }
}
