package com.visangesl.tree.badge.vo;

import com.visangesl.tree.vo.VSListCondition;

public class BadgeListCondition extends VSListCondition {
	private String mbrId;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	
	
}
