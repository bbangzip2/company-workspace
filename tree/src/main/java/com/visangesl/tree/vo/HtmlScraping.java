package com.visangesl.tree.vo;

public class HtmlScraping {
    private String imgurl;      // 이미지 주소
    private String title;       // 제목
    private String url;         // 사이트 url
    private String content;     // 내용

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
