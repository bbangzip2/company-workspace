package com.visangesl.tree.vo;


public class VSListCondition extends VSCondition {
	private int pageNo;
	private int pageSize;
	private String schoolLvl;
	private String sortKey;


	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getSchoolLvl() {
		return schoolLvl;
	}
	public void setSchoolLvl(String schoolLvl) {
		this.schoolLvl = schoolLvl;
	}
    public String getSortKey() {
        return sortKey;
    }
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

}
