package com.visangesl.tree.view;

import java.util.Map;

import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

public class TreeMappingJacksonJsonView extends MappingJacksonJsonView {

	@SuppressWarnings("unchecked")
	@Override
	protected Object filterModel(Map<String, Object> model) {
		// TODO Auto-generated method stub
		Object result = super.filterModel(model);
		if (!(result instanceof Map)) {
		    return result;
		}
		Map map = (Map) result;
		if (map.size() == 1) {
			return map.values().toArray()[0];
		}
		return map;
	}

}
