package com.visangesl.tree.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TreeResponsHeaderSetControllerInterceptor extends HandlerInterceptorAdapter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {

        logger.debug("Tree Response Header Set Interceptor");
        response.setHeader("access-control-allow-origin", request.getHeader("Origin"));
        response.setHeader("access-Control-Allow-Credentials","true");
        response.setHeader("P3P", "CP=\"ALL CURa ADMa DEVa TAIa OUR BUS IND PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA POL HEA PRE LOC OTC\"");
//        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");

        return true;
    }

        //이 밑에 부분도 설정 할 수 있음. 자세한 설명은 다시 검색 해보기!
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {

//        logger.debug("Tree Response Header Set..2");
//        response.setHeader("access-control-allow-origin", "*");
//        response.setHeader("P3P", "CP=\"ALL CURa ADMa DEVa TAIa OUR BUS IND PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA POL HEA PRE LOC OTC\"");

        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
     }

}
