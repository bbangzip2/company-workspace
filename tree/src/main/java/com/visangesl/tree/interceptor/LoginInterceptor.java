package com.visangesl.tree.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.member.vo.UserSession;

public class LoginInterceptor implements HandlerInterceptor {
	final static Log logger = LogFactory.getLog(LoginInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession();
        // 세션에서 로그인 정보를 가져온다
        UserSession userSession = (UserSession) (session.getAttribute("VSUserSession"));

        boolean sendMessage = false;
        StringBuffer sb = null;

        if (userSession == null) {
        	logger.debug("false");
        	response.setStatus(HttpStatus.UNAUTHORIZED.value());

            return false;
        }

        logger.debug("true");
        return true;
    }

    @Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
        // TODO Auto-generated method stub
        // logger.debug("VivasamLoginControllerInterceptor의 postHandle에서 출력");
    }

    @Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
        // TODO Auto-generated method stub
        // logger.debug("VivasamLoginControllerInterceptor의 afterCompletion에서 출력");
    }
}
