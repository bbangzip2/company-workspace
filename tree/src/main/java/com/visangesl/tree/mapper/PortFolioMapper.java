package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.portfolio.vo.PortfolioCondition;
import com.visangesl.tree.portfolio.vo.PortfolioVo;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

public interface PortFolioMapper extends BaseMapperInterface {
	//학생 포트폴리오 목록
	public List<VSObject> getPortfolioList(VSCondition condition);
	
	//특정 클래스에 해당하는 모든 포트폴리오 목록
	public List<VSObject> getPortfolioClassList(VSCondition condition);
	
	//학생 포트폴리오 기본 정보
	public VSObject getPortfolioStudentInfo(VSCondition condition);
	
	//학생 포트폴리오 클래스웍 기본 정보
	public VSObject getPortfolioStudentClassInfo(VSCondition condition);
	
	public List<VSObject> getPortfolioLesson(PortfolioCondition condition);

	public List<VSObject> getPortfolioDay(PortfolioCondition condition);
	
	public int insertPortfolioInfo(PortfolioVo portfolioVo)throws Exception;
	
	public int getClassPortfolioNOKCount(VSCondition condition);
	
	public VSObject getPortfolioInfo(VSCondition condition);
	
	public int inquiryPortfolio(VSObject object);
}

