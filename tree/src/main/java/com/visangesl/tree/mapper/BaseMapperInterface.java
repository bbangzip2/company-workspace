package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public interface BaseMapperInterface {
	
	public VSObject getData(VSObject vsObject);
	public List<VSObject> getDataList(VSObject vsObject);
	public int modifyDataWithResultCodeMsg(VSObject vsObject);
	public int addDataWithResultCodeMsg(VSObject vsObject);
	public int deleteDataWithResultCodeMsg(VSObject vsObject);
	
}
