package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.upload.vo.AttachFileListConditionVo;
import com.visangesl.tree.upload.vo.AttachFileVo;
import com.visangesl.tree.upload.vo.NoteInfoVo;
import com.visangesl.tree.vo.VSObject;


public interface AttachFileMapper extends BaseMapperInterface {

	// 노트 정보 저장 
	public int cNoteData(NoteInfoVo noteInfoVo) throws Exception;
	// 첨부 파일 저장 
	public int cTreeAttachFile(AttachFileVo attachFileVo) throws Exception;
	
	// 첨부 파일 삭제
    public int dTreeAttachFile(AttachFileVo attachFileVo) throws Exception;
	
	// 첨부 파일 목록 조회 
	public List<VSObject> getAttachFileList(AttachFileListConditionVo afListCond) throws Exception;
	
	// 노트 첨부파일 목록 조회 
	public List<VSObject> getNoteAttachFileList(AttachFileListConditionVo afListCond) throws Exception;
	
	
	
}
