package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public interface NoteMapper extends BaseMapperInterface{

	// 해당 컨텐츠의 상세 데이터 조회.
	public VSObject getContentsInfoData(VSObject vsObject) throws Exception;
	
	// 노트 정보 업데이트 처리 
	public int uNoteData(VSObject vsObject) throws Exception;
	// 노트 정보 있는지 확인 
	public int getNoteCount(VSObject vsObject) throws Exception;
	// 노트 키값 가져오기 
	public String getNoteSeq(VSObject vsObject) throws Exception;
	// 노트가 등록된 레슨 목록 조회 
    public List<VSObject> getMyNoteLessonList(VSObject vsObject) throws Exception;
    // 레슨별 노트 목록 조회 
    public List<VSObject> getMyNoteList(VSObject vsObject) throws Exception;
    // 노트 상세 정보 조회 (녹음/이미지)
    public List<VSObject> getMyNoteInfo(VSObject vsObject) throws Exception;
    
}
