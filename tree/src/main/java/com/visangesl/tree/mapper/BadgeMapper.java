package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;


public interface BadgeMapper extends BaseMapperInterface{
	public List<VSObject> getBadgeList(VSCondition condition) throws Exception;
}
