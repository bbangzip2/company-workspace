package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.message.vo.MessageCondition;
import com.visangesl.tree.message.vo.MessageVo;
import com.visangesl.tree.vo.VSObject;

public interface MessageMapper extends BaseMapperInterface {

    public List<VSObject> getMessageList(MessageCondition messageCondition) throws Exception;
    public List<VSObject> getClassList(MessageCondition messageCondition) throws Exception;
    public int getNewMessageCount(MessageVo messageVo) throws Exception;
    public List<VSObject> getClassMemberMessageList(MessageCondition messageCondition) throws Exception;
}
