package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.authoring.vo.AuthoringCondition;
import com.visangesl.tree.vo.VSObject;

public interface AuthoringMapper extends BaseMapperInterface {

    // 북 리스트
    public List<VSObject> getBookList(AuthoringCondition authoringCondition) throws Exception;

    // 레슨 리스트
    public List<VSObject> getLessonList(AuthoringCondition authoringCondition) throws Exception;

    // PRODINFO 테이블에 카드정보 업데이트
    public int uProdSeqCard (String prodSeq) throws Exception;

//    // 상세 조회
//    public List<VSObject> getData(VSCondition dataCondition) throws Exception;
//
//    // 등록
//    @Override
//    public int addDataWithResultCodeMsg(VSObject vsObject);

}
