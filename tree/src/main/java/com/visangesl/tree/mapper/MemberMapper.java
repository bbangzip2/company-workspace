package com.visangesl.tree.mapper;

import java.util.HashMap;

import com.visangesl.tree.member.vo.Member;
import com.visangesl.tree.member.vo.UserSession;

public interface MemberMapper extends BaseMapperInterface{

	public UserSession signIn(HashMap tMap) throws Exception;

    public Member getNearClassSeq(Member member) throws Exception;

}
