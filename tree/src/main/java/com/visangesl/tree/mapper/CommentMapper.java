package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

public interface CommentMapper extends BaseMapperInterface {
	public int addComment(VSObject vsObject);
	public int removeComment(VSCondition condition);
	public List<VSObject> getContentComment(VSCondition condition);
	public String existDoLike(VSCondition condition);
	public int doLike(VSObject vsObject);
	public int undoLike(VSCondition condition);
}
