package com.visangesl.tree.mapper;

import com.visangesl.tree.vo.SqlLog;


public interface CommonMapper extends BaseMapperInterface{
	// sql inject insert 
	public int insertSQLINJECTION_LOG(SqlLog sqlLog); 
}
