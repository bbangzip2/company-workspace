package com.visangesl.tree.mapper;

import java.util.List;

import com.visangesl.tree.contents.vo.ContentsCondition;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

public interface ContentsMapper extends BaseMapperInterface{

	// 해당 컨텐츠의 상세 데이터 조회.
	public VSObject getContentsInfoData(VSCondition dataCondition) throws Exception;

    public VSObject getNewContentsInfo(ContentsCondition contentsCondition) throws Exception;

	// 엑티비티별 레퍼런스 컨텐츠 목록 조회
	public List<VSObject> getDataList(String cid) throws Exception;

	// 해당 Lesson의 Day 데이터 리스트 조회.
	public List<VSObject> getDayDataList(VSCondition dataCondition) throws Exception;

	// 해당 차시의 카드맵 데이터 리스트 조회.
	public List<VSObject> getCardMapDataList(VSCondition dataCondition) throws Exception;

    public List<VSObject> getCardMapDataSubList(VSCondition dataCondition) throws Exception;

	// Update - 해당 유저의 특정 카드의 정렬 값 Update
	public int modifyCardMapSortData(VSObject vsObject) throws Exception;

    // Update - 해당 유저의 특정 카드의 정렬 값 초기화~ Update
    public int modifyCardMapSortDataReset(VSObject vsObject) throws Exception;

    // Insert - 해당 유저의 특정 카드의 정렬 값 Insert
    public int insertCardMapSortData(VSObject vsObject) throws Exception;

    // Delete - 해당 유저의 기존 카드 정렬 값 Delete
    public int deleteCardMapSortData(VSObject vsObject) throws Exception;

	// PRODINFO 테이블에 정보 등록
	public int cProdInfoData (VSObject vsObject) throws Exception;

	// PRODINFO 테이블에 카드정보 업데이트
	public int uProdSeqCard (String prodSeq) throws Exception;

    // select Package List Data
    public List<VSObject> getPackageDataList(VSCondition dataCondition) throws Exception;

    public VSObject getBookLessonCd(VSCondition dataCondition) throws Exception;

    // package version up insert
    public int insertPackageVersion(ContentsCondition condition) throws Exception;

    // package version up insert
    public int updatePackageVersion(ContentsCondition condition) throws Exception;

    // package file info insert
    public int insertPackageFileInfo(ContentsCondition condition) throws Exception;

    public VSObject getPackageVersion(ContentsCondition condition) throws Exception;
    
    public String getReferenceListCnt(VSObject vsObejct) throws Exception;
    
    public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception;
    	
}
