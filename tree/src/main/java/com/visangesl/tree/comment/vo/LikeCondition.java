package com.visangesl.tree.comment.vo;

import com.visangesl.tree.vo.VSCondition;

public class LikeCondition extends VSCondition {
	private String contentType;
	private int contentId;
	private String mbrId;
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	
	
}
