package com.visangesl.tree.comment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.comment.service.CommentService;
import com.visangesl.tree.comment.vo.Like;
import com.visangesl.tree.comment.vo.LikeCondition;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.mapper.CommentMapper;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	CommentMapper commentMapper;

	@Override
	public VSResult<VSObject> addComment(VSObject vsObject) {
		// TODO Auto-generated method stub
		VSResult<VSObject> result = new VSResult<VSObject>();
		
		int affectedRows = commentMapper.addComment(vsObject);
		
		if (affectedRows > 0) {
			result.setCode("0000");
			result.setMessage("댓글 쓰기 성공");
		} else {
			result.setCode("9999");
			result.setMessage("댓글 쓰기 실패");
		}
		
		return result;
	}

	@Override
	public VSResult<VSObject> removeComment(VSCondition condition) {
		// TODO Auto-generated method stub
		VSResult<VSObject> result = new VSResult<VSObject>();
		
		int affectedRows = commentMapper.removeComment(condition);
		
		if (affectedRows > 0) {
			result.setCode("0000");
			result.setMessage("댓글 삭제 성공");
		} else {
			result.setCode("9999");
			result.setMessage("댓글 삭제 실패");
		}
		
		return result;
	}

	@Override
	public List<VSObject> getContentComment(VSCondition condition) throws Exception {
		// TODO Auto-generated method stub
		return commentMapper.getContentComment(condition);
	}

	@Override
	public VSResult<Object> toggleLike(VSCondition condition) {
		VSResult<Object> result = new VSResult<Object>();
		
		try {
			String existDoLike = commentMapper.existDoLike(condition);
			if (existDoLike != null && existDoLike.equals("Y")) {
				//현재 do like 상태 그러므로 undo like처리
				int affectedRows = commentMapper.undoLike(condition);
				
				if (affectedRows > 0) {
					result.setCode("0000");
					result.setMessage("LIKE 취소 성공");
					result.setResult("N");
				} else {
					throw new Exception("LIKE 취소 처리 실패");
				}

			} else {
				//현재 undo like 상태 그러므로 do like 처리
				Like like = new Like();
				LikeCondition likeCondition = (LikeCondition)condition;
				like.setContentId(likeCondition.getContentId());
				like.setContentType(likeCondition.getContentType());
				like.setMbrId(likeCondition.getMbrId());
				
				int affectedRows = commentMapper.doLike(like);
				
				if (affectedRows > 0) {
					result.setCode("0000");
					result.setMessage("LIKE 성공");
					result.setResult("Y");
				} else {
					throw new Exception("LIKE 처리 실패");
				}

			}			
		} catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode("9999");
            result.setMessage(handler.getMessage());
            e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public String existDoLike(VSCondition condition) {
		// TODO Auto-generated method stub
		return commentMapper.existDoLike(condition);
	}

	/*
	@Override
	public int doLike(VSObject vsObject) {
		// TODO Auto-generated method stub
		return commentMapper.doLike(vsObject);
	}

	@Override
	public int undoLike(VSCondition condition) {
		// TODO Auto-generated method stub
		return commentMapper.undoLike(condition);
	}
	*/
}
