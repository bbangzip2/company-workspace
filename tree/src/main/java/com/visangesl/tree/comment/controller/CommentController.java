package com.visangesl.tree.comment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.comment.service.CommentService;
import com.visangesl.tree.comment.vo.Comment;
import com.visangesl.tree.comment.vo.CommentCondition;
import com.visangesl.tree.comment.vo.ContentCondition;
import com.visangesl.tree.comment.vo.LikeCondition;
import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.util.TreeUtil;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Controller
public class CommentController implements BaseController {
	@Autowired
	private CommentService commentService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value="/comment/addComment", method=RequestMethod.POST)
	@ResponseBody
	public VSResult<VSObject> addComment(HttpServletRequest request
			, @RequestParam(value = "cmmtCntt", required = true, defaultValue = "") String cmmtCntt
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId
			) {
		Comment vsObject = new Comment();

        String mbrId = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        }

        vsObject.setCmmtCntt(cmmtCntt);
        vsObject.setContentType(contentType);
        vsObject.setContentId(contentId);
        vsObject.setMbrId(mbrId);

		return commentService.addComment(vsObject);
	}

	@RequestMapping(value="/comment/removeComment", method=RequestMethod.POST)
	@ResponseBody
	public VSResult<VSObject> removeComment(HttpServletRequest request
			, @RequestParam(value = "cmmtSeq", required = true, defaultValue = "") int cmmtSeq
			) {
		CommentCondition vsCondition = new CommentCondition();

        String mbrId = null;
        String mbrGrade = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        	mbrGrade = userSession.getMbrGrade();
        }

        vsCondition.setCmmtSeq(cmmtSeq);
        vsCondition.setMbrId(mbrId);
        vsCondition.setMbrGrade(mbrGrade);

		return commentService.removeComment(vsCondition);
	}

	@RequestMapping(value="/comment/getContentComment", method=RequestMethod.GET)
	@ResponseBody
	public VSResult<Object> getContentComment(HttpServletRequest request
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId
			) {
		ContentCondition vsCondition = new ContentCondition();

		/*
        String mbrId = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        }
        */
        vsCondition.setContentType(contentType);
        vsCondition.setContentId(contentId);

        VSResult<Object> result = new VSResult<Object>();

        try {
	        result.setResult(commentService.getContentComment(vsCondition));
	        result.setCode("0000");
			result.setMessage("성공");
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("조회 오류");
            e.printStackTrace();
        }

		return result;
	}

	@RequestMapping(value="/comment/toggleLike", method=RequestMethod.POST)
	@ResponseBody
	public VSResult<Object> toggleLike(HttpServletRequest request
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId
			) {
		LikeCondition vsCondition = new LikeCondition();

        String mbrId = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        }

        vsCondition.setContentType(contentType);
        vsCondition.setContentId(contentId);
        vsCondition.setMbrId(mbrId);

		return commentService.toggleLike(vsCondition);
	}

	@RequestMapping(value="/comment/existDoLike", method=RequestMethod.GET)
	@ResponseBody
	public VSResult<Object> existDoLike(HttpServletRequest request
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId
			) {
		LikeCondition vsCondition = new LikeCondition();

        String mbrId = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        }

        vsCondition.setContentType(contentType);
        vsCondition.setContentId(contentId);
        vsCondition.setMbrId(mbrId);

        VSResult<Object> result = new VSResult<Object>();

        try {
	        result.setResult(TreeUtil.isNull(commentService.existDoLike(vsCondition), "N"));
	        result.setCode("0000");
			result.setMessage("성공");
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("조회 오류");
            e.printStackTrace();
        }

		return result;
	}
}
