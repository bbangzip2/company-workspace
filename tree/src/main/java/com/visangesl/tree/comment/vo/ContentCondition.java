package com.visangesl.tree.comment.vo;

import com.visangesl.tree.vo.VSCondition;

public class ContentCondition extends VSCondition {
	private String contentType;
	private int contentId;
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	
	
}
