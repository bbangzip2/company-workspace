package com.visangesl.tree.comment.vo;

import com.visangesl.tree.vo.VSCondition;

public class CommentCondition extends VSCondition {
	private int cmmtSeq;
	private String mbrId;
	private String mbrGrade;

	public int getCmmtSeq() {
		return cmmtSeq;
	}

	public void setCmmtSeq(int cmmtSeq) {
		this.cmmtSeq = cmmtSeq;
	}

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

    public String getMbrGrade() {
        return mbrGrade;
    }

    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }


}
