package com.visangesl.tree.exception;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.BadSqlGrammarException;

import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.exception.TreeException;
import com.visangesl.tree.exception.TreeRuntimeException;

public class ExceptionHandler {

    private String code;
    private String message;

    private Exception e;

    public ExceptionHandler(Exception e) {
        this.e = e;
        processException();

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Exception getE() {
        return e;
    }

    public void setE(Exception e) {
        this.e = e;
        processException();
    }

    public void processException() {
        if (e instanceof DuplicateKeyException) {
            code = TreeProperties.getProperty("error.duplicatekey.code");
            message = TreeProperties.getProperty("error.duplicatekey.msg");

        } else if (e instanceof BadSqlGrammarException) {
            code = TreeProperties.getProperty("error.badsqlgrammar.code");
            message = TreeProperties.getProperty("error.badsqlgrammar.msg");

        } else if (e instanceof TreeException) {
            TreeException se = (TreeException) e;
            code = se.getCode();
            message = se.getMsg();
        } else if (e instanceof TreeRuntimeException) {
            TreeRuntimeException se = (TreeRuntimeException) e;
            code = se.getCode();
            message = se.getMsg();
        } else {
            code = TreeProperties.getProperty("error.etc.code");
            message = TreeProperties.getProperty("error.etc.msg");
        }
    }
}
