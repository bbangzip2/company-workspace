package com.visangesl.tree.exception;

/**
 * checked exception을 위해 사용할 상위 class
 * 
 * @author 정남용
 * 
 */
public class TreeException extends Exception {
    /**
     * generated serialVersionUID by eclipse
     */
    private static final long serialVersionUID = -3024642556586579973L;

    /**
     * Error Code
     */
    private String code;

    /**
     * Error Message
     */
    private String msg;

    public TreeException() {
        super();
    }
    
    public TreeException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(super.getMessage());
        sb.append(":");
        sb.append(this.code);
        return sb.toString();
    }
}
