package com.visangesl.tree.authoring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.authoring.service.AuthoringService;
import com.visangesl.tree.authoring.vo.AuthoringCondition;
import com.visangesl.tree.authoring.vo.AuthoringVo;
import com.visangesl.tree.mapper.AuthoringMapper;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public class AuthoringServiceImpl implements AuthoringService {

    @Autowired
    AuthoringMapper authoringMapper;

    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getBookList(AuthoringCondition authoringCondition)
            throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getBookList(authoringCondition);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getLessonList(AuthoringCondition authoringCondition)
            throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getLessonList(authoringCondition);
    }

    @Override
    @Transactional(readOnly = true)
    public VSObject getData(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getData(vsObject);
    }

    @Override
    @Transactional(readOnly = true)
    public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        VSResult resultCodeMsg = new VSResult();
        AuthoringVo authoringVo = new AuthoringVo();

        // 등록/수정 구분을 위한 선 조회 진행
        authoringVo = (AuthoringVo) authoringMapper.getData(vsObject);

        int affectedRows = 0;
        if (authoringVo != null) { // 수정
            affectedRows = authoringMapper.modifyDataWithResultCodeMsg(vsObject);
        } else {    // 등록
            affectedRows = authoringMapper.addDataWithResultCodeMsg(vsObject);
            authoringVo = (AuthoringVo) vsObject;

            // 등록 후 CARD_CD 컬럼에 값을 채워준다.
            affectedRows = authoringMapper.uProdSeqCard(authoringVo.getProdSeq());
        }

        // 결과값 (prodseq)
        resultCodeMsg.setCode(authoringVo.getProdSeq());
//        if (affectedRows < 1) {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }

        return resultCodeMsg;
    }

    @Override
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

}
