package com.visangesl.tree.authoring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.authoring.vo.AuthoringCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

@Service
public interface AuthoringService {

    // 북 리스트
    public List<VSObject> getBookList(AuthoringCondition authoringCondition) throws Exception;

    // 레슨 리스트
    public List<VSObject> getLessonList(AuthoringCondition authoringCondition) throws Exception;

    // 상세 조회
    public VSObject getData(VSObject vsObject) throws Exception;

    // 등록
    public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // 수정
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

}
