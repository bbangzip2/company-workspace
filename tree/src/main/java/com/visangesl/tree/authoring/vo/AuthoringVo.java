package com.visangesl.tree.authoring.vo;

import com.visangesl.tree.vo.VSObject;

public class AuthoringVo extends VSObject {

    private String prodSeq;

    private String pid;

    private String bookCd;
    private String lessonCd;
    private String prodTitle;
    private String thmbPath;
    private String filePath;
    private String cardType;
    private String cardSkill; //
    private String studyMode;
    private String cardLevel;
    private String time;
    private String editYn;
    private String openYn;
    private String keyword;
    private String grading;
    private String direcLang1;
    private String direc1;
    private String direcLang2;
    private String direc2;

    private String useYn;
    private String regDttm;

    private String mbrId;
    private String cardGubun;


    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getBookCd() {
        return bookCd;
    }

    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }

    public String getLessonCd() {
        return lessonCd;
    }

    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }

    public String getProdTitle() {
        return prodTitle;
    }

    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }

    public String getThmbPath() {
        return thmbPath;
    }

    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardSkill() {
        return cardSkill;
    }

    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }

    public String getStudyMode() {
        return studyMode;
    }

    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }

    public String getCardLevel() {
        return cardLevel;
    }

    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEditYn() {
        return editYn;
    }

    public void setEditYn(String editYn) {
        this.editYn = editYn;
    }

    public String getOpenYn() {
        return openYn;
    }

    public void setOpenYn(String openYn) {
        this.openYn = openYn;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getGrading() {
        return grading;
    }

    public void setGrading(String grading) {
        this.grading = grading;
    }


    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getRegDttm() {
        return regDttm;
    }

    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

    public String getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }

    public String getDirecLang1() {
        return direcLang1;
    }

    public void setDirecLang1(String direcLang1) {
        this.direcLang1 = direcLang1;
    }

    public String getDirec1() {
        return direc1;
    }

    public void setDirec1(String direc1) {
        this.direc1 = direc1;
    }

    public String getDirecLang2() {
        return direcLang2;
    }

    public void setDirecLang2(String direcLang2) {
        this.direcLang2 = direcLang2;
    }

    public String getDirec2() {
        return direc2;
    }

    public void setDirec2(String direc2) {
        this.direc2 = direc2;
    }

    public String getCardGubun() {
        return cardGubun;
    }

    public void setCardGubun(String cardGubun) {
        this.cardGubun = cardGubun;
    }




}
