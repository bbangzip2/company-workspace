package com.visangesl.tree.authoring.vo;

import com.visangesl.tree.vo.VSListCondition;

public class AuthoringCondition extends VSListCondition {

    private String mbrId;
    private String bookCd;
    private String lessonCd;
    private String pid;


    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }



}
