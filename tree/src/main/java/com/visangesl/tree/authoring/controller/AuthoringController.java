package com.visangesl.tree.authoring.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.authoring.service.AuthoringService;
import com.visangesl.tree.authoring.vo.AuthoringCondition;
import com.visangesl.tree.authoring.vo.AuthoringVo;
import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.member.vo.UserSession;
import com.visangesl.tree.property.TreeProperties;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.util.HtmlParserUtil;
import com.visangesl.tree.util.TreeUtil;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

/**
 * ClassRoomController
 * Version - 1.0
 * Copyright
 */
@Controller
public class AuthoringController {

    @Autowired
    private AuthoringService authoringService;

    @Autowired
    CheckXSSService xsssvc;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * getBookList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/authoring/bookList", method=RequestMethod.POST)
    public VSResult getBookList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId
            ) throws Exception {

        logger.debug("getBookList");
        logger.debug(mbrId);

        VSResult result = new VSResult();

        try {

            AuthoringCondition authoringCondition = new AuthoringCondition();
            authoringCondition.setMbrId(mbrId);

            List<VSObject> list = authoringService.getBookList(authoringCondition);

            result.setResult(list);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * getLessonList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/authoring/lessonList", method=RequestMethod.POST)
    public VSResult getLessonList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd
            ) throws Exception {

        logger.debug("getLessonList");
        logger.debug(mbrId);
        logger.debug(bookCd);

        VSResult result = new VSResult();

        try {

            AuthoringCondition authoringCondition = new AuthoringCondition();
            authoringCondition.setMbrId(mbrId);
            authoringCondition.setBookCd(bookCd);

            List<VSObject> list = authoringService.getLessonList(authoringCondition);

            result.setResult(list);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * addProdCard
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/authoring/addProdCard", method=RequestMethod.POST)
    public VSResult addProdCard(HttpServletRequest request, HttpServletResponse response
            , @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId
            , @RequestParam(value = "pid", required = false, defaultValue = "") String pid
            , @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd
            , @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd
            , @RequestParam(value = "prodTitle", required = false, defaultValue = "") String prodTitle
            , @RequestParam(value = "thmbPath", required = false, defaultValue = "") String thmbPath
            , @RequestParam(value = "filePath", required = false, defaultValue = "") String filePath
            , @RequestParam(value = "cardType", required = false, defaultValue = "") String cardType
            , @RequestParam(value = "cardSkill", required = false, defaultValue = "") String cardSkill
            , @RequestParam(value = "studyMode", required = false, defaultValue = "") String studyMode
            , @RequestParam(value = "cardLevel", required = false, defaultValue = "") String cardLevel
            , @RequestParam(value = "time", required = false, defaultValue = "") String time
            , @RequestParam(value = "editYn", required = false, defaultValue = "") String editYn
            , @RequestParam(value = "openYn", required = false, defaultValue = "") String openYn
            , @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword
            , @RequestParam(value = "grading", required = false, defaultValue = "") String grading
            , @RequestParam(value = "direcLang1", required = false, defaultValue = "") String direcLang1
            , @RequestParam(value = "direc1", required = false, defaultValue = "") String direc1
            , @RequestParam(value = "direcLang2", required = false, defaultValue = "") String direcLang2
            , @RequestParam(value = "direc2", required = false, defaultValue = "") String direc2
            , @RequestParam(value = "useYn", required = false, defaultValue = "Y") String useYn
            , Model model) throws Exception {

        logger.debug("addProdCard");
        logger.debug(mbrId);

        VSResult result = new VSResult();
        AuthoringVo authoringVo = new AuthoringVo();

        if (!mbrId.equals("") && !pid.equals("") && !bookCd.equals("") && !lessonCd.equals("") && !prodTitle.equals("") && !thmbPath.equals("") && !filePath.equals("")) {

            try {

                HttpSession session = request.getSession();
                UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

                if(userSession != null){
                    mbrId = userSession.getMemberId();
                } else {
                    logger.debug("userSession NULL!");
                }

                authoringVo.setPid(pid);
                authoringVo.setMbrId(mbrId);
                authoringVo.setBookCd(bookCd);
                authoringVo.setLessonCd(lessonCd);
                authoringVo.setProdTitle(prodTitle);
                authoringVo.setThmbPath(thmbPath);
                authoringVo.setFilePath(filePath);
                authoringVo.setCardType(cardType);
                authoringVo.setCardSkill(cardSkill);
                authoringVo.setStudyMode(studyMode);
                authoringVo.setCardLevel(cardLevel);
                authoringVo.setTime(time);
                authoringVo.setEditYn(editYn);
                authoringVo.setOpenYn(openYn);
                authoringVo.setKeyword(keyword);
                authoringVo.setGrading(grading);
                authoringVo.setDirecLang1(direcLang1);
                authoringVo.setDirec1(direc1);
                authoringVo.setDirecLang2(direcLang2);
                authoringVo.setDirec2(direc2);
                authoringVo.setUseYn(useYn);

//                prodInfoVo.setProdTitle(prodTitle); // 제목
//                prodInfoVo.setThmbPath(thmbPath);   // 썸네일 경로
//                prodInfoVo.setRegId(memberId);      // 아이디
//                prodInfoVo.setDtlCntt(dtlCntt);     // 상세설명
//                prodInfoVo.setBookCd(bookCd);       // 북코드
//                prodInfoVo.setLessonCd(lessonCd);   // 레슨코드
//                prodInfoVo.setDaynoCd(daynoCd);     // 차시코드
//                prodInfoVo.setMdlCd(mdlCd);         // 모듈코드
//                prodInfoVo.setCardCd(cardCd);       // 카드코드
//                prodInfoVo.setSort(sort);           // 정렬순서
//                prodInfoVo.setProdSect(prodSect);   // 상품구분자  (레퍼런스 데이터만 'R' 로 들어간다.)
//                prodInfoVo.setProdType(prodType);   // 상품타입 (상품유형( TXT / IMG / MOV / URL )_ 레퍼런스 컨텐츠에서 사용함)
//                prodInfoVo.setCurriCd(curriCd);     // 커리큘럼 아이디 (사용안함.)
//                prodInfoVo.setCardType(cardType);   // 카드타입 (카드유형(LECTURE/QUIZ/ACTIVITY))
//                prodInfoVo.setCardLevel(cardLevel); // 카드레벨 (1~5)
//                prodInfoVo.setUseType(useType);     // 카드 사용 용도 (강의용/개인학습용/그룹학습용)
//                prodInfoVo.setFilePath(filePath);   // 실제 파일경로
//                prodInfoVo.setDirecEng(direcEng);   // 영어 디렉션
//                prodInfoVo.setDirecKor(direcKor);   // 한국어 디렉션


                // 썸네일 체크
                if (!thmbPath.equals("")) { // 넘어온 이미지 사용
                    String thmbFileName = "thumbnail_" + pid + "." + TreeUtil.fileUploadExt(thmbPath);
                    String basePath = TreeProperties.getProperty("tree_save_filepath");
                    String thmbFilePath = TreeProperties.getProperty("tree_thumbnail_path") + TreeProperties.getProperty("tree_card_path") + TreeProperties.getProperty("tree_edit_card_path") + File.separator + mbrId;
                    String thmbFileFullPath = basePath + thmbFilePath;

                    fileUrlReadAndDownload(thmbPath, thmbFileName, thmbFileFullPath);
                    authoringVo.setThmbPath(thmbFilePath + File.separator + thmbFileName);
                } else { // 기본 이미지 사용

                }

                // 저작도구로 제작한 카드 정보 등록
                result = authoringService.addDataWithResultCodeMsg(authoringVo);
                String cardCode = result.getCode(); // 등록된 카드 코드 저장


                // 카드 복사 경로 설정
                File dir = new File(filePath); // 원본 폴더

                // 파일 폴더 존재 여부 체크
                if (dir.exists()) {

                    // 복사 될 폴더
                    String treeBase = TreeProperties.getProperty("tree_save_filepath"); // 컨텐츠 파일 기본 경로
                    String toDirPath = TreeProperties.getProperty("tree_card_path") + TreeProperties.getProperty("tree_edit_card_path") + File.separator + mbrId + File.separator + cardCode;
                    File toDir = new File(treeBase + toDirPath);

                    // 복사 경로 생성 체크
                    if (!toDir.exists()) {
                        toDir.mkdirs();
                    }

                    // 카드 폴더 복사
                    boolean copyResult = TreeUtil.copy(dir, toDir);
                    System.out.println("카드 폴더 복사: " + copyResult);


                    if (copyResult) {
                        String toURL = Tree_Constant.SITE_FULL_URL;
                        String cardPath = "pathesl='" + TreeProperties.getProperty("tree_client_content_path") + File.separator + bookCd + File.separator + lessonCd + File.separator + cardCode + "';";

                        // html pathesl 파서 실행
                        if (HtmlParserUtil.htmlParser(toDir, toURL, cardPath, TreeProperties.getProperty("tree_client_content_path"), bookCd, lessonCd, cardCode)) {
                            result.setCode(TreeProperties.getProperty("error.success.code"));
                            result.setMessage(TreeProperties.getProperty("error.success.msg"));
                        }
                    }


                    // 복사 / 파싱 완료 후 카드 실제 경로 업데이트
                    if (!mbrId.equals("") && !pid.equals("")) {
                        authoringVo.setFilePath(toDirPath);
                        authoringVo.setCardGubun("MT005");
                        result = authoringService.addDataWithResultCodeMsg(authoringVo);
                    }
                    cardCode = result.getCode();

                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));

                } else {
                    result.setCode(TreeProperties.getProperty("error.fail.code"));
                    result.setMessage("등록 실패 - 해당 컨텐츠의 파일 폴더가 존재하지 않습니다.");
                }

            } catch (Exception e) {
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage("등록 실패");
                e.printStackTrace();
            }
        } else {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage("등록 실패 - 필수 파라미터 확인 (mbrId, pid, bookCd, lessonCd, prodTitle, thmbPath, filePath)");
        }

        return result;
    }

    /**
     * getData
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value="/authoring/cardInfo", method=RequestMethod.POST)
    public VSResult getData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "pid", required = false, defaultValue = "") String pid
            ) throws Exception {

        logger.debug("cardInfo");
        logger.debug(mbrId);
        logger.debug(pid);

        VSResult result = new VSResult();

        try {

            AuthoringCondition authoringCondition = new AuthoringCondition();
            authoringCondition.setMbrId(mbrId);
            authoringCondition.setPid(pid);

            AuthoringVo authoringVo = new AuthoringVo();
            authoringVo = (AuthoringVo) authoringService.getData(authoringCondition);

            result.setResult(authoringVo);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return result;
    }



    public int fileUrlReadAndDownload(String fileUrl, String localFileName, String downloadDir) {
        int bufferSize = 1024;
        OutputStream outStream = null;
        URLConnection uCon = null;

        InputStream is = null;
        int byteWritten = 0;
        try {

            if (!new File(downloadDir).exists()) {
                new File(downloadDir).mkdirs();
            }

            if (localFileName == null || localFileName.equals("")) {
                int slashIndex = fileUrl.lastIndexOf('/');
                int periodIndex = fileUrl.lastIndexOf('.');
                // 파일 어드레스에서 마지막에 있는 파일이름을 취득
                String fileName = fileUrl.substring(slashIndex + 1);
                if (periodIndex >= 1 && slashIndex >= 0 && slashIndex < fileUrl.length() - 1) {
                    fileUrlReadAndDownload(fileUrl, fileName, downloadDir);
                }
            }

            URL Url;
            byte[] buf;
            int byteRead;
            Url = new URL(fileUrl);
            outStream = new BufferedOutputStream(new FileOutputStream(downloadDir + File.separator + URLDecoder.decode(localFileName, "UTF-8")));

            uCon = Url.openConnection();
            is = uCon.getInputStream();
            buf = new byte[bufferSize];
            while ((byteRead = is.read(buf)) != -1) {
                outStream.write(buf, 0, byteRead);
                byteWritten += byteRead;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return byteWritten;
    }


}
