package com.visangesl.tree.teaching.vo;

import com.visangesl.tree.vo.VSObject;

public class TeachingVo extends VSObject {

    private String clsSeq;
    private String clsNm;
    private String inClsYmd;
    private String inClsStartHm;
    private String inClsEndHm;
    private String wk;
    private String thmbPath;
    private String portFolioNewCnt;

    private String lessonCd;
    private String lessonNm;

    private String prodSeq;
    private String prodTitle;
    private String dayNo;
    private String portfolioSeq;
    private String regDttm;
    private String portfolioOkYn;
    private String portfolioImgPath;
    private String portfolioThumbImgPath;

    private String bookNm;

    private String cardSeq;
    private String cardNm;
    private String cardStudyType;
    private String cardViewType;
    private String cardType;
    private String cardRunTime;
    private String cardSkill;
    private String cardLevel;
    private String cardEditable;
    private String cardSharable;
    private String cardKeyword;


    private String cmmtCnt;
    private String likeCnt;
    private String badgeSeq;
    private String badgeImgPath;
    private String mbrId;
    private String profilePhotoPath;
    private String nm;



    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getInClsYmd() {
        return inClsYmd;
    }
    public void setInClsYmd(String inClsYmd) {
        this.inClsYmd = inClsYmd;
    }
    public String getInClsStartHm() {
        return inClsStartHm;
    }
    public void setInClsStartHm(String inClsStartHm) {
        this.inClsStartHm = inClsStartHm;
    }
    public String getInClsEndHm() {
        return inClsEndHm;
    }
    public void setInClsEndHm(String inClsEndHm) {
        this.inClsEndHm = inClsEndHm;
    }
    public String getWk() {
        return wk;
    }
    public void setWk(String wk) {
        this.wk = wk;
    }
    public String getThmbPath() {
        return thmbPath;
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getPortFolioNewCnt() {
        return portFolioNewCnt;
    }
    public void setPortFolioNewCnt(String portFolioNewCnt) {
        this.portFolioNewCnt = portFolioNewCnt;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getLessonNm() {
        return lessonNm;
    }
    public void setLessonNm(String lessonNm) {
        this.lessonNm = lessonNm;
    }
    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getDayNo() {
        return dayNo;
    }
    public void setDayNo(String dayNo) {
        this.dayNo = dayNo;
    }
    public String getPortfolioSeq() {
        return portfolioSeq;
    }
    public void setPortfolioSeq(String portfolioSeq) {
        this.portfolioSeq = portfolioSeq;
    }
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
    public String getPortfolioOkYn() {
        return portfolioOkYn;
    }
    public void setPortfolioOkYn(String portfolioOkYn) {
        this.portfolioOkYn = portfolioOkYn;
    }
    public String getPortfolioImgPath() {
        return portfolioImgPath;
    }
    public void setPortfolioImgPath(String portfolioImgPath) {
        this.portfolioImgPath = portfolioImgPath;
    }
    public String getPortfolioThumbImgPath() {
        return portfolioThumbImgPath;
    }
    public void setPortfolioThumbImgPath(String portfolioThumbImgPath) {
        this.portfolioThumbImgPath = portfolioThumbImgPath;
    }
    public String getBookNm() {
        return bookNm;
    }
    public void setBookNm(String bookNm) {
        this.bookNm = bookNm;
    }
    public String getCardSeq() {
        return cardSeq;
    }
    public void setCardSeq(String cardSeq) {
        this.cardSeq = cardSeq;
    }
    public String getCardNm() {
        return cardNm;
    }
    public void setCardNm(String cardNm) {
        this.cardNm = cardNm;
    }
    public String getCardStudyType() {
        return cardStudyType;
    }
    public void setCardStudyType(String cardStudyType) {
        this.cardStudyType = cardStudyType;
    }
    public String getCardViewType() {
        return cardViewType;
    }
    public void setCardViewType(String cardViewType) {
        this.cardViewType = cardViewType;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardRunTime() {
        return cardRunTime;
    }
    public void setCardRunTime(String cardRunTime) {
        this.cardRunTime = cardRunTime;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getCardEditable() {
        return cardEditable;
    }
    public void setCardEditable(String cardEditable) {
        this.cardEditable = cardEditable;
    }
    public String getCardSharable() {
        return cardSharable;
    }
    public void setCardSharable(String cardSharable) {
        this.cardSharable = cardSharable;
    }
    public String getCardKeyword() {
        return cardKeyword;
    }
    public void setCardKeyword(String cardKeyword) {
        this.cardKeyword = cardKeyword;
    }
    public String getCmmtCnt() {
        return cmmtCnt;
    }
    public void setCmmtCnt(String cmmtCnt) {
        this.cmmtCnt = cmmtCnt;
    }
    public String getLikeCnt() {
        return likeCnt;
    }
    public void setLikeCnt(String likeCnt) {
        this.likeCnt = likeCnt;
    }
    public String getBadgeSeq() {
        return badgeSeq;
    }
    public void setBadgeSeq(String badgeSeq) {
        this.badgeSeq = badgeSeq;
    }
    public String getBadgeImgPath() {
        return badgeImgPath;
    }
    public void setBadgeImgPath(String badgeImgPath) {
        this.badgeImgPath = badgeImgPath;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getProfilePhotoPath() {
        return profilePhotoPath;
    }
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }





}
