package com.visangesl.tree.teaching.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.controller.BaseController;
import com.visangesl.tree.exception.ExceptionHandler;
import com.visangesl.tree.message.service.MessageService;
import com.visangesl.tree.message.vo.MessageVo;
import com.visangesl.tree.service.CheckXSSService;
import com.visangesl.tree.teaching.vo.BadgeVo;
import com.visangesl.tree.teaching.vo.CmmtVo;
import com.visangesl.tree.teaching.vo.TeachingVo;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.tree.vo.VSResult;

/**
 * TeachingController
 * Version - 1.0
 * Copyright
 */
@Controller
public class TeachingController implements BaseController {

    @Autowired
    private MessageService messageService;

    @Autowired
    CheckXSSService xsssvc;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final VSResult result = new VSResult();


    @ResponseBody
    @RequestMapping(value="/teaching/getClassInfoList")
    public VSResult getClassInfoList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "sort", required = false, defaultValue = "-1") int sort
            ) throws Exception {

        VSResult result = new VSResult();

        List<VSObject> classInfoList = new ArrayList<VSObject>();

        TeachingVo teachingVo = new TeachingVo();
        teachingVo.setClsSeq("2");
        teachingVo.setClsNm("CLS2");
        teachingVo.setThmbPath("http://img.naver.net/static/www/u/2013/0731/nmms_224940510.gif");
        teachingVo.setWk("MON;WED;FRI;");
        teachingVo.setInClsStartHm("14:00");
        teachingVo.setInClsEndHm("15:00");
        teachingVo.setPortFolioNewCnt("2");

        classInfoList.add(teachingVo);

        teachingVo = new TeachingVo();
        teachingVo.setClsSeq("1");
        teachingVo.setClsNm("CLS1");
        teachingVo.setThmbPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setWk("MON;WED;FRI;");
        teachingVo.setInClsStartHm("14:00");
        teachingVo.setInClsEndHm("15:00");
        teachingVo.setPortFolioNewCnt("5");

        classInfoList.add(teachingVo);

        result.setResult(classInfoList);

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/teaching/getClassLessonList")
    public VSResult getClassLessonList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq
            ) throws Exception {

        VSResult result = new VSResult();

        List<VSObject> classLessonList = new ArrayList<VSObject>();
        TeachingVo teachingVo = new TeachingVo();

        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");

        classLessonList.add(teachingVo);

        teachingVo = new TeachingVo();
        teachingVo.setLessonCd("1");
        teachingVo.setLessonNm("A-1.Go Go Go");

        classLessonList.add(teachingVo);

        result.setResult(classLessonList);

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/teaching/getClassLessonInfoList")
    public VSResult getClassLessonInfoList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd
            ) throws Exception {

        VSResult result = new VSResult();

        List<VSObject> classLessonList = new ArrayList<VSObject>();

        TeachingVo teachingVo = new TeachingVo();
        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");
        teachingVo.setProdTitle("Scholastic booster");
        teachingVo.setDayNo("2");
//        teachingVo.setPortFolioSeq("1");
        teachingVo.setPortfolioOkYn("N");
        teachingVo.setPortfolioThumbImgPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setCardSeq("1");
        teachingVo.setCardNm("Drawing");
        teachingVo.setCmmtCnt("4");
        teachingVo.setLikeCnt("1");
        teachingVo.setBadgeSeq("");
        teachingVo.setBadgeImgPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png;http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setMbrId("STDNT1");
        teachingVo.setNm("학생1");
        teachingVo.setProfilePhotoPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");

        classLessonList.add(teachingVo);

        teachingVo = new TeachingVo();
        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");
        teachingVo.setProdTitle("Scholastic booster");
        teachingVo.setDayNo("2");
//      teachingVo.setPortFolioSeq("1");
        teachingVo.setPortfolioOkYn("Y");
        teachingVo.setPortfolioThumbImgPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setCardSeq("1");
        teachingVo.setCardNm("Drawing");
        teachingVo.setCmmtCnt("5");
        teachingVo.setLikeCnt("3");
        teachingVo.setBadgeSeq("");
        teachingVo.setBadgeImgPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setMbrId("STDNT2");
        teachingVo.setNm("학생2");
        teachingVo.setProfilePhotoPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");

        classLessonList.add(teachingVo);

        result.setResult(classLessonList);

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/teaching/getPortFolioInfo")
    public VSResult getPortFolioInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "portSeq", required = false, defaultValue = "") String actSeq
            ) throws Exception {

        VSResult result = new VSResult();

        List<VSObject> classLessonList = new ArrayList<VSObject>();

        TeachingVo teachingVo = new TeachingVo();
        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");
        teachingVo.setDayNo("2");
        teachingVo.setCardSeq("1");
        teachingVo.setCardNm("Drawing");
        teachingVo.setRegDttm("2014.03.03");

        teachingVo.setPortfolioSeq("1");
        teachingVo.setPortfolioImgPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");


        List<VSObject> cmmtList = new ArrayList<VSObject>();

        CmmtVo cmmtVo = new CmmtVo();
        cmmtVo.setCmmtSeq("1");
        cmmtVo.setCmmtCntt("Lorem ipsum..");
        cmmtVo.setRegId("STDNT1");
        cmmtVo.setRegDttm("14.04.04");
        cmmtVo.setProfilePhotopath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");

        cmmtList.add(cmmtVo);

        cmmtVo = new CmmtVo();
        cmmtVo.setCmmtSeq("1");
        cmmtVo.setCmmtCntt("Lorem ipsum..");
        cmmtVo.setRegId("STDNT1");
        cmmtVo.setRegDttm("14.04.04");
        cmmtVo.setProfilePhotopath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");

        cmmtList.add(cmmtVo);

        BadgeVo badgeVo = new BadgeVo();
        badgeVo.setBadgeSeq("1");
        badgeVo.setBadgeCd("1");
        badgeVo.setBadgeImgPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");

        HashMap pMap = new HashMap();
        pMap.put("portFolioInfo", teachingVo);
        pMap.put("cmmtList", cmmtList);
        pMap.put("badgeInfo", badgeVo);

        result.setParamMap(pMap);
        result.setResult(teachingVo);

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/main")
    public VSResult getClassScheduleList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId
            ) throws Exception {

        VSResult result = new VSResult();

        List<VSObject> classList = new ArrayList<VSObject>();

        TeachingVo teachingVo = new TeachingVo();

        teachingVo.setClsSeq("CLS1");
        teachingVo.setClsNm("CLS1");
        teachingVo.setInClsStartHm("10:30");
        teachingVo.setThmbPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");
        teachingVo.setDayNo("2");
        classList.add(teachingVo);

        teachingVo = new TeachingVo();
        teachingVo.setClsSeq("CLS2");
        teachingVo.setClsNm("CLS2");
        teachingVo.setInClsStartHm("14:30");
        teachingVo.setThmbPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");
        teachingVo.setDayNo("2");
        classList.add(teachingVo);

        teachingVo = new TeachingVo();
        teachingVo.setClsSeq("CLS3");
        teachingVo.setClsNm("CLS3");
        teachingVo.setInClsStartHm("18:30");
        teachingVo.setThmbPath("http://icon.daumcdn.net/w/icon/1312/19/152729032.png");
        teachingVo.setLessonCd("2");
        teachingVo.setLessonNm("A-2.The hat");
        teachingVo.setDayNo("2");
        classList.add(teachingVo);


        MessageVo messageVo = new MessageVo();

        messageVo.setRecvMbrId(mbrId);
//        int newMsgCnt = messageService.getNewMessageCount(messageVo);
        int newMsgCnt = 0;

        newMsgCnt = 2;

        HashMap pMap = new HashMap();
        pMap.put("classes", classList);
        pMap.put("message", newMsgCnt);

//        result.setParamMap(pMap);
        result.setResult(pMap);

        return result;

    }

    @ResponseBody
    @RequestMapping(value="/teaching/cardMapList")
    public VSResult getLessonCardMapList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNo
            ) throws Exception {

        VSResult result = new VSResult();

        try {

            HashMap pMap = new HashMap();

            TeachingVo teachingVo = new TeachingVo();

            teachingVo.setProdSeq("1");
            teachingVo.setProdTitle("Scholastic booster");
            teachingVo.setLessonCd("1");
            teachingVo.setLessonNm("A-2. GO,GO,GO");
            teachingVo.setDayNo("1");

            pMap.put("book", teachingVo);

            List<VSObject> cardMapList = new ArrayList<VSObject>();

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("1");
            teachingVo.setCardType("default");
            teachingVo.setCardStudyType("1");
            teachingVo.setCardViewType("1");
            teachingVo.setCardNm("Opening");
            teachingVo.setCardRunTime("6");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("2");
            teachingVo.setCardType("default");
            teachingVo.setCardStudyType("2");
            teachingVo.setCardViewType("2");
            teachingVo.setCardNm("Objectives");
            teachingVo.setCardRunTime("3");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("3");
            teachingVo.setCardType("default");
            teachingVo.setCardStudyType("3");
            teachingVo.setCardViewType("3");
            teachingVo.setCardNm("Sort the words");
            teachingVo.setCardRunTime("5");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("4");
            teachingVo.setCardType("default");
            teachingVo.setCardStudyType("1");
            teachingVo.setCardViewType("1");
            teachingVo.setCardNm("Pair work Activity");
            teachingVo.setCardRunTime("10");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("5");
            teachingVo.setCardType("default");
            teachingVo.setCardStudyType("2");
            teachingVo.setCardViewType("3");
            teachingVo.setCardNm("Class activity");
            teachingVo.setCardRunTime("10");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("6");
            teachingVo.setCardType("extra");
            teachingVo.setCardStudyType("3");
            teachingVo.setCardViewType("1");
            teachingVo.setCardNm("extra1");
            teachingVo.setCardRunTime("6");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("7");
            teachingVo.setCardType("extra");
            teachingVo.setCardStudyType("1");
            teachingVo.setCardViewType("2");
            teachingVo.setCardNm("extra2");
            teachingVo.setCardRunTime("7");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("8");
            teachingVo.setCardType("extra");
            teachingVo.setCardStudyType("2");
            teachingVo.setCardViewType("1");
            teachingVo.setCardNm("extra3");
            teachingVo.setCardRunTime("8");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("9");
            teachingVo.setCardType("edited");
            teachingVo.setCardStudyType("3");
            teachingVo.setCardViewType("2");
            teachingVo.setCardNm("edited1");
            teachingVo.setCardRunTime("5");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("10");
            teachingVo.setCardType("edited");
            teachingVo.setCardStudyType("1");
            teachingVo.setCardViewType("3");
            teachingVo.setCardNm("edited2");
            teachingVo.setCardRunTime("5");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            teachingVo = new TeachingVo();
            teachingVo.setCardSeq("11");
            teachingVo.setCardType("homework");
            teachingVo.setCardStudyType("2");
            teachingVo.setCardViewType("1");
            teachingVo.setCardNm("homework1");
            teachingVo.setCardRunTime("5");
            teachingVo.setCardSkill("Speaking");
            teachingVo.setCardLevel("A");
            teachingVo.setCardEditable("Y");
            teachingVo.setCardSharable("N");
            teachingVo.setCardKeyword("Speaking, Role play");
            cardMapList.add(teachingVo);

            pMap.put("cardMap", cardMapList);

            result.setCode("0000");
            result.setMessage("성공");
            result.setResult(pMap);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;

    }
}

