package com.visangesl.tree.teaching.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class BadgeVo extends VSObject {

    private String badgeSeq;
    private String badgeCd;
    private String badgeImgPath;
    private String typeSect;
    private String contentId;
    private String useYn;
    private String regId;
    private String regDttm;


    public String getBadgeSeq() {
        return badgeSeq;
    }
    public void setBadgeSeq(String badgeSeq) {
        this.badgeSeq = badgeSeq;
    }
    public String getBadgeCd() {
        return badgeCd;
    }
    public void setBadgeCd(String badgeCd) {
        this.badgeCd = badgeCd;
    }
    public String getTypeSect() {
        return typeSect;
    }
    public void setTypeSect(String typeSect) {
        this.typeSect = typeSect;
    }
    public String getContentId() {
        return contentId;
    }
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
    public String getBadgeImgPath() {
        if (badgeImgPath != null) {
            return Tree_Constant.SITE_FULL_URL + badgeImgPath;
        } else {
            return badgeImgPath;
        }
//        return badgeImgPath;
    }
    public void setBadgeImgPath(String badgeImgPath) {
        this.badgeImgPath = badgeImgPath;
    }

}
