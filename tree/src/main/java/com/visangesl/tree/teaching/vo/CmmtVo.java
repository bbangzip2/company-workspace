package com.visangesl.tree.teaching.vo;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.VSObject;

public class CmmtVo extends VSObject {

    private String cmmtSeq;
    private String cmmtCntt;
    private String cmmtNm;
    private String cmmtType;
    private String contentId;
    private String likeYn;
    private String pwd;
    private String regDttm;
    private String regId;
    private String profilePhotopath;
    private String useYn;

    public String getCmmtSeq() {
        return cmmtSeq;
    }
    public void setCmmtSeq(String cmmtSeq) {
        this.cmmtSeq = cmmtSeq;
    }
    public String getCmmtCntt() {
        return cmmtCntt;
    }
    public void setCmmtCntt(String cmmtCntt) {
        this.cmmtCntt = cmmtCntt;
    }
    public String getCmmtNm() {
        return cmmtNm;
    }
    public void setCmmtNm(String cmmtNm) {
        this.cmmtNm = cmmtNm;
    }
    public String getCmmtType() {
        return cmmtType;
    }
    public void setCmmtType(String cmmtType) {
        this.cmmtType = cmmtType;
    }
    public String getContentId() {
        return contentId;
    }
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }
    public String getLikeYn() {
        return likeYn;
    }
    public void setLikeYn(String likeYn) {
        this.likeYn = likeYn;
    }
    public String getPwd() {
        return pwd;
    }
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotopath;
        } else {
            return profilePhotopath;
        }
//        return profilePhotopath;
    }
    public void setProfilePhotopath(String profilePhotopath) {
        this.profilePhotopath = profilePhotopath;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }


}
