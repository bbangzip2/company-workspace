package com.visangesl.tree.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.visangesl.tree.constant.Tree_Constant;
import com.visangesl.tree.vo.HtmlScraping;

public class HtmlScrapingUtil {

    private final Log logger = LogFactory.getLog(this.getClass());

    public HtmlScraping getHtmlScraping(String url) throws Exception {
        // TODO Auto-generated method stub
        HtmlScraping result = new HtmlScraping();
        BufferedInputStream in = null;
        StringBuffer sb = new StringBuffer();

        URLConnection urlConnection = null;
        URL urlobj = null;
        try {
            urlobj = new URL(url);
            urlConnection = urlobj.openConnection();

            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setUseCaches(false);

            logger.debug("ContentEncoding : " + urlConnection.getContentEncoding());
            String encoding = "UTF-8";
            String content_type = urlConnection.getContentType();
            logger.debug("content_type : " + content_type);

            for (String param : content_type.replace(" ", "").split(";")) {
                if (param.startsWith("charset=")) {
                    encoding = param.split("=", 2)[1];
                    break;
                }
            }
            logger.debug("encoding : " + encoding);
            /*
            in = new BufferedInputStream(urlConnection.getInputStream());

            byte[] bufRead = new byte[4096];
            int lenRead = 0;
            while ((lenRead = in.read(bufRead)) > 0) {
                sb.append(new String(bufRead, 0, lenRead));
            }
            */

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), encoding));
            while (true) {
                String line = br.readLine();

                if (line == null)
                    break;
                else
                    sb.append(line);
            }
            br.close();

        } catch (Exception e) {
            logger.debug("Exception parsing : " + e.getMessage());
        }

        String htmlsrc = "";
        htmlsrc = sb.toString();

        logger.debug("htmlsrc : " + htmlsrc);

        Source source = null;

        try {
            source = new Source(htmlsrc);
        } catch (Exception e) {

        }
        source.fullSequentialParse();

        // 메타 태그 목록들을 뽑는다
        List<Element> metaList = source.getAllElements(HTMLElementName.META);

        // 뽑아놓은 메타 태그 목록에서 웹페이지가 사용하는 charset을 찾아낸다
        // <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        String charset = "";
        for (Element item : metaList) {
            String property = TreeUtil.isNull(item.getAttributeValue("http-equiv"));     // http-equiv 프로퍼티를 찾는다
            if (property.toLowerCase().equals("content-type")) {                              // http-equiv 프로퍼티의 값이 content-type이면
                String tmp = item.getAttributeValue("content");                             // content 프로퍼티를 읽는다
                charset = tmp.substring(tmp.lastIndexOf("charset=") + 8, tmp.length());         // charset=문자열을 찾아내어 그 뒤의 붙은 값을 추출한다
            }
        }

        // 이미지 주소를 셋팅한다
        String host = "";
        url = url.replaceFirst("http://", "");
        logger.debug("url1 : " + url);
        url = url.replaceFirst("https://", "");
        logger.debug("url2 : " + url);
        String[] urltmps = url.split("/");
        host = urltmps[0];
        logger.debug("host : " + host);

        result.setImgurl(getImageUrl(source, metaList, host));
        // 제목을 셋팅한다
        result.setTitle(getTitle(source, metaList, charset));
        // url을 셋팅한다
        result.setUrl(getUrl(url, metaList));
        // 내용을 셋팅한다
        result.setContent(getContent(source, metaList, charset));

        return result;
    }

    // 이미지 주소를 뽑아낸다
    private String getImageUrl(Source source, List<Element> metaList, String host) {
        String imgUrl = "";

        try {
            // meta 태그에서 og:Image 속성의 meta 태그를 찾아서 content 속성값을 읽는다
            for (Element item : metaList) {
                String property = item.getAttributeValue("property");
                if ("og:image".equals(property)) {
                    imgUrl = item.getAttributeValue("content");
                    break;
                }
            }

            // meta 태그에서 없을 경우 img 태그에서 찾는다
            List<Element> imgList = source.getAllElements(HTMLElementName.IMG);
            if ("".equals(imgUrl)) {
                for (Element item : imgList) {
                    String width = item.getAttributeValue("width");
                    String height = item.getAttributeValue("height");

                    // width, height 속성이 모두 있는것만 찾아낸다
                    if ("".equals(TreeUtil.isNull(width)) || "".equals(TreeUtil.isNull(height))) {
                        continue;
                    } else {
                        // 값에 px를 빈문자열로 바꿔준 후 int 형으로 변환
                        width = width.replaceAll("px", "");
                        height = height.replaceAll("px", "");

                        int intWidth = Integer.parseInt(width);
                        int intHeight = Integer.parseInt(height);

                        // 가로 크기가 186 px 에서 10 px 정도 차이가 나고 세로 크기가 138 px 에서 10 px 정도 차이가 나면 허용(URL 톡 등록시 보여주는 img 태그의 크기가 가로 186px, 세로 138px이어서 그리 작업했음)
                        if (Math.abs(intWidth - 186) < 10 && Math.abs(intHeight - 138) < 10) {
                            imgUrl = item.getAttributeValue("src");
                            break;
                        }
                    }
                }
            }

            // img 태그에서 적당한 크기의 이미지를 발견 못했으면 첫번째 나오는 이미지로 셋팅한다
            if ("".equals(imgUrl)) {
                for (Element item : imgList) {
                    String src = item.getAttributeValue("src");

                    // 이미지 확장자가 일반적인 이미지 파일인지 확인한다
                    if (TreeUtil.getFileType(src).equals(Tree_Constant.IMAGETYPE)) {
                        imgUrl = src;
                        break;
                    }
                }
            }

            logger.debug("imgUrl : " + imgUrl);
            // imgUrl 변수 앞에 http://가 없으면 host 주소를 넣어서 붙여준다
            if (imgUrl.length() > 7) {
                logger.debug("111");
                if (!(imgUrl.substring(0, 7).equals("http://"))) {
                    logger.debug("222");
                    imgUrl = "http://" + host + imgUrl;
                    logger.debug("final imgUrl : " + imgUrl);
                }
            }

        } catch (Exception e) {
            logger.error("HtmlScrapingServiceImpl.getImageUrl Exception : " + e.getMessage());
        }

        return imgUrl;
    }

    // 제목을 뽑아낸다
    private String getTitle(Source source, List<Element> metaList, String charset) {
        String title = "";

        try {
            // meta 태그에서 og:title 속성의 meta 태그를 찾아서 content 속성값을 읽는다
            for (Element item : metaList) {
                String property = item.getAttributeValue("property");
                if ("og:title".equals(property)) {
                    title = item.getAttributeValue("content");
                    break;
                }
            }

            // meta 태그중 og_title 속성이 없을 경우 meta 태그의 title 속성에서 찾는다
            if ("".equals(title)) {
                for (Element item : metaList) {
                    String property = item.getAttributeValue("property");
                    if ("title".equals(property)) {
                        title = item.getAttributeValue("content");
                        break;
                    }
                }
            }

            // meta 태그중 title 속성이 없을 경우 meta 태그의 subject 속성에서 찾는다
            if ("".equals(title)) {
                for (Element item : metaList) {
                    String property = item.getAttributeValue("property");
                    if ("subject".equals(property)) {
                        title = item.getAttributeValue("content");
                        break;
                    }
                }
            }

            // meta 태그중 subject 속성이 없을 경우 title 태그에서 읽어온다
            if ("".equals(title)) {
                List<Element> elements = source.getAllElements(HTMLElementName.TITLE);
                if (elements == null || elements.size() == 0) {

                } else {
                    title = elements.get(0).getContent().getTextExtractor().toString();
                }
            }

            String tmpstr = new String(title);
            logger.debug("euc-kr - > utf-8 : " + new String(tmpstr.getBytes("euc-kr"), "utf-8"));
            logger.debug("euc-kr - > ksc5601 : " + new String(tmpstr.getBytes("euc-kr"), "ksc5601"));
            logger.debug("euc-kr - > x-windows-949 : " + new String(tmpstr.getBytes("euc-kr"), "x-windows-949"));
            logger.debug("euc-kr - > iso-8859-1 : " + new String(tmpstr.getBytes("euc-kr"), "iso-8859-1"));

            logger.debug("utf-8 - > euc-kr : " + new String(tmpstr.getBytes("utf-8"), "euc-kr"));
            logger.debug("utf-8 - > ksc5601 : " + new String(tmpstr.getBytes("utf-8"), "ksc5601"));
            logger.debug("utf-8 - > x-windows-949 : " + new String(tmpstr.getBytes("utf-8"), "x-windows-949"));
            logger.debug("utf-8 - > iso-8859-1 : " + new String(tmpstr.getBytes("utf-8"), "iso-8859-1"));

            logger.debug("iso-8859-1 - > utf-8 : " + new String(tmpstr.getBytes("iso-8859-1"), "utf-8"));
            logger.debug("iso-8859-1 - > ksc5601 : " + new String(tmpstr.getBytes("iso-8859-1"), "ksc5601"));
            logger.debug("iso-8859-1 - > x-windows-949 : " + new String(tmpstr.getBytes("iso-8859-1"), "x-windows-949"));
            logger.debug("iso-8859-1 - > euc-kr : " + new String(tmpstr.getBytes("iso-8859-1"), "euc-kr"));

            logger.debug("x-windows-949 - > utf-8 : " + new String(tmpstr.getBytes("x-windows-949"), "utf-8"));
            logger.debug("x-windows-949 - > ksc5601 : " + new String(tmpstr.getBytes("x-windows-949"), "ksc5601"));
            logger.debug("x-windows-949 - > euc-kr : " + new String(tmpstr.getBytes("x-windows-949"), "euc-kr"));
            logger.debug("x-windows-949 - > iso-8859-1 : " + new String(tmpstr.getBytes("x-windows-949"), "iso-8859-1"));

            logger.debug("ksc5601 - > utf-8 : " + new String(tmpstr.getBytes("ksc5601"), "utf-8"));
            logger.debug("ksc5601 - > euc-kr : " + new String(tmpstr.getBytes("ksc5601"), "euc-kr"));
            logger.debug("ksc5601 - > x-windows-949 : " + new String(tmpstr.getBytes("ksc5601"), "x-windows-949"));
            logger.debug("ksc5601 - > iso-8859-1 : " + new String(tmpstr.getBytes("ksc5601"), "iso-8859-1"));

            logger.debug("변환전 title : " + title);
            logger.debug("charset : " + charset);

            // 검색된 title 값이 있을 경우 입력받은 문자셋으로 변환 작업을 거친다

            if (!("".equals(title))) {
                logger.debug("작업1");
                if (!("UTF-8".equals(charset.toUpperCase()))) { // charset이 UTF-8이 아닐 경우 지정된 charset으로 변환한다
                    logger.debug("작업2");
                    // String tmp = new String(title.getBytes(), charset);
                    // logger.debug("tmp : " + tmp);
                    title = new String(title.getBytes(), "UTF-8");
                    logger.debug("final title : " + title);
                }
            }

            logger.debug("변환후 title : " + title);

        } catch (Exception e) {
            logger.error("HtmlScrapingServiceImpl.getTitle Exception : " + e.getMessage());
        }

        return title;
    }

    // 주소를 뽑아낸다
    private String getUrl(String url, List<Element> metaList) {
        String resultUrl = "";

        try {
            // meta 태그에서 og:url 속성의 meta 태그를 찾아서 content 속성값을 읽는다
            for (Element item : metaList) {
                String property = item.getAttributeValue("property");
                if ("og:url".equals(property)) {
                    resultUrl = item.getAttributeValue("content");
                    break;
                }
            }

            // meta 태그중 og_title 속성이 없을 경우 meta 태그의 title 속성에서 찾는다
            if ("".equals(resultUrl)) {
                resultUrl = url;
            }
        } catch (Exception e) {
            logger.error("HtmlScrapingServiceImpl.getUrl Exception : " + e.getMessage());
        }

        return resultUrl;
    }

    // 내용을 뽑아낸다
    private String getContent(Source source, List<Element> metaList, String charset) {
        String content = "";

        try {
            // meta 태그에서 og:description 속성의 meta 태그를 찾아서 content 속성값을 읽는다
            for (Element item : metaList) {
                String property = item.getAttributeValue("property");
                if ("og:description".equals(property)) {
                    content = item.getAttributeValue("content");
                    break;
                }
            }

            // meta 태그중 og:description 속성이 없을 경우 p 태그에서 내용이 50자 이상인 것을 찾아 읽어온다
            List<Element> elements = source.getAllElements(HTMLElementName.P);
            if ("".equals(content)) {
                for (Element item : elements) {
                    String tmp = item.getContent().getTextExtractor().toString();
                    if (tmp.length() >= 50) {
                        content = tmp;
                        break;
                    }
                }
            }

            // p태그에서 50자 이상인 것이 없으면 첫번째 p태그 것을 골라 온다
            if ("".equals(content)) {
                if (elements == null || elements.size() == 0) {

                } else {
                    content = elements.get(0).getContent().getTextExtractor().toString();
                }
            }

            // 검색된 title 값이 있을 경우 입력받은 문자셋으로 변환 작업을 거친다
            if (!("".equals(content))) {
                if (!("UTF-8".equals(charset.toUpperCase()))) {           // charset이 UTF-8이 아닐 경우 지정된 charset으로 변환한다
                    content = new String(content.getBytes(charset), "UTF-8");
                }
            }

        } catch (Exception e) {
            logger.error("HtmlScrapingServiceImpl.getContent Exception : " + e.getMessage());
        }

        return content;
    }

}
