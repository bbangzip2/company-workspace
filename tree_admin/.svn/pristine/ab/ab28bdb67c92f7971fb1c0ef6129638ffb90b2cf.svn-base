<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="teacher">

    <select id="checkMbrId" parameterType="com.visangesl.treeadmin.amg.vo.TeacherCondition" resultType="int">
        SELECT 
            COUNT(MBR_ID)
        FROM MBR_INFO
        WHERE MBR_ID = #{mbrId}
    </select>

	<!-- Teacher List Total Count : Teacher 데이터 총 갯수를 가져온다.(조건:특정 칼럼 정렬, Status값, 검색어 등...) -->
	<select id="getDataListCnt" parameterType="com.visangesl.treeadmin.amg.vo.TeacherCondition" resultType="int">
        SELECT
             COUNT(DISTINCT MI.MBR_ID)
        FROM MBR_INFO MI
        INNER JOIN ICM_REL_INFO IRI ON
        (   
            IRI.MBR_ID = MI.MBR_ID
            AND IRI.INSTITUTE_SEQ = #{insSeq}
            <if test="campSeq != null">AND IRI.CAMP_SEQ = #{campSeq}</if>
        )
        WHERE MI.MBR_GRADE = 'MG210'
        <if test="useYn != null"> AND MI.USE_YN = #{useYn}</if>
        <if test="searchKey != null"> AND (MI.MBR_ID LIKE '%${searchKey}%' OR MI.NAME LIKE '%${searchKey}%')</if>
	</select>
	
	<!-- Teacher List : Teacher 데이터를 리스트 형식으로 가져온다.(조건:특정 칼럼 정렬, Status값, 검색어 등...) -->
	<select id="getDataList" parameterType="com.visangesl.treeadmin.amg.vo.TeacherCondition" resultMap="teacherDetailResultMap">
        SELECT 
             TCH.MBR_ID AS mbrId
            ,TCH.NAME AS name
            ,TCH.NICKNAME AS nickName
            ,TCH.SEX_SECT AS sexSect
            ,TCH.PROFILE_PHOTO_PATH AS profilePhotopath
            ,TCH.USE_YN AS useYn
            ,DATE_FORMAT(TCH.REG_DATE, '%Y.%m.%d') AS regDate
            ,CI.CAMP_SEQ AS campSeq
            ,CI.NAME AS campNm
        FROM
        (
            SELECT
                 DISTINCT MI.MBR_ID
                ,MI.NAME
                ,MI.NICKNAME
                ,MI.SEX_SECT
                ,MI.PROFILE_PHOTO_PATH
                ,MI.USE_YN
                ,MI.REG_DATE
            FROM MBR_INFO MI
            INNER JOIN ICM_REL_INFO IRI ON
            (   
                IRI.MBR_ID = MI.MBR_ID
                AND IRI.INSTITUTE_SEQ = #{insSeq}
                <if test="campSeq != null"> AND IRI.CAMP_SEQ = #{campSeq}</if>
            )
            WHERE MI.MBR_GRADE = 'MG210'
            <if test="useYn != null"> AND MI.USE_YN = #{useYn}</if>
            <if test="searchKey != null"> AND (MI.MBR_ID LIKE '%${searchKey}%' OR MI.NAME LIKE '%${searchKey}%')</if>
            <if test="pageSize > 0">
                <choose>
                    <when test="pageStartIndex != 0"> LIMIT ${pageStartIndex}, ${pageSize}</when>
                    <otherwise> LIMIT ${pageSize}</otherwise>
                </choose>
            </if>
        ) AS TCH
        INNER JOIN ICM_REL_INFO RI ON (RI.MBR_ID = TCH.MBR_ID)
        INNER JOIN CAMP_INFO CI ON (CI.CAMP_SEQ = RI.CAMP_SEQ)
        GROUP BY CI.CAMP_SEQ, TCH.MBR_ID
        ORDER BY TCH.REG_DATE DESC, TCH.MBR_ID ASC, CI.CAMP_SEQ ASC;
	</select>
	
	<!-- Teacher Data : Teacher의 데이터를 가져온다. -->
	<select id="getData" parameterType="com.visangesl.treeadmin.amg.vo.TeacherCondition" resultMap="teacherDetailResultMap">
        SELECT
             MI.MBR_ID AS mbrId
            ,MI.NAME AS name
            ,MI.PROFILE_PHOTO_PATH AS profilePhotopath
            ,MI.NICKNAME AS nickName
            ,MI.EMAIL AS email
            ,MI.SEX_SECT AS sexSect
            ,MI.TEL_NO AS telNo
            ,MI.MEMO AS memo
            ,MI.USE_YN AS useYn
            ,DATE_FORMAT(MI.REG_DATE, '%Y.%m.%d %H:%i') AS regDate
            ,DATE_FORMAT(MI.MOD_DATE, '%Y.%m.%d %H:%i') AS modDate
            ,MI.REG_ID AS regId
            ,MI.MOD_ID AS modId
            ,CI.CAMP_SEQ AS campSeq
            ,CI.NAME AS campNm
            ,CLS.CLS_SEQ AS classSeq
            ,CLS.NAME AS classNm
            ,DATE_FORMAT(CLS.START_YMD, '%Y.%m.%d') AS startYmd
            ,DATE_FORMAT(CLS.END_YMD, '%Y.%m.%d') AS endYmd
            ,CLS.USE_YN AS clsUseYn
            ,PI.PROG_SEQ AS progSeq
            ,PI.NAME AS progNm
        FROM
            MBR_INFO MI
        INNER JOIN ICM_REL_INFO IRI ON (IRI.MBR_ID = MI.MBR_ID AND IRI.INSTITUTE_SEQ = #{insSeq})
        LEFT JOIN CAMP_INFO CI ON (CI.CAMP_SEQ = IRI.CAMP_SEQ)
        LEFT JOIN CLS_INFO CLS ON (CLS.CLS_SEQ = IRI.CLS_SEQ)
        LEFT JOIN PROG_INFO PI ON (PI.INSTITUTE_SEQ = IRI.INSTITUTE_SEQ AND CLS.PROG_SEQ = PI.PROG_SEQ)
        WHERE MI.MBR_ID = #{mbrId}
        ORDER BY CLS.START_YMD DESC;
	</select>
	
	<resultMap id="teacherDetailResultMap" type="com.visangesl.treeadmin.amg.vo.Teacher">
		<id property="mbrId" column="mbrId"/>
		<result property="name" column="name" />
		<result property="profilePhotopath" column="profilePhotopath" />
		<result property="nickName" column="nickName" />
		<result property="sexSect" column="sexSect" />
		<result property="email" column="email" />
		<result property="telNo" column="telNo" />
		<result property="memo" column="memo" />
		<result property="regDate" column="regDate" />
		<result property="modDate" column="modDate" />
		<result property="regId" column="regId" />
		<result property="modId" column="modId" />
		<result property="useYn" column="useYn" />
		
		<!-- 뷰 화면에서만 사용 -->
		<collection property="classList" ofType="com.visangesl.treeadmin.amg.vo.ClassInfo">
			<id property="classSeq" column="classSeq" />
			<result property="classNm" column="classNm" />
			<result property="campSeq" column="campSeq" />
			<result property="campNm" column="campNm" />
			<result property="startYmd" column="startYmd" />
			<result property="endYmd" column="endYmd" />
			<result property="progSeq" column="progSeq" />
			<result property="progNm" column="progNm" />
			<result property="useYn" column="clsUseYn" />
		</collection>
        
        <!-- 리스트와 수정 화면에서 사용 -->
        <collection property="campList" ofType="com.visangesl.treeadmin.amg.vo.Campus">
            <id property="campSeq" column="campSeq" />
            <result property="campNm" column="campNm" />
            <collection property="classList" ofType="com.visangesl.treeadmin.amg.vo.ClassInfo">
                <id property="classSeq" column="classSeq" />
                <result property="classNm" column="classNm" />
                <result property="startYmd" column="startYmd" />
                <result property="endYmd" column="endYmd" />
                <result property="progNm" column="progNm" />
                <result property="useYn" column="clsUseYn" />
            </collection>
        </collection>
		
	</resultMap>
    
    
    <!-- Student : Search Campus ComboBox List -->
    <select id="getCampusList" parameterType="com.visangesl.treeadmin.amg.vo.TeacherCondition" resultType="com.visangesl.treeadmin.vo.CodeVo">
        <choose>
            <when test="mbrId != null">
            SELECT
                 CI.CAMP_SEQ AS cd
                ,CI.NAME AS cdSubject
            FROM
                MBR_INFO AS MI
                INNER JOIN ICM_REL AS IRI ON (MI.MBR_ID=IRI.ICM_SEQ AND IRI.REL_SECT='RE002')
                INNER JOIN CAMP_INFO AS CI ON (CI.CAMP_SEQ=IRI.REL_SEQ)
            WHERE
                MI.MBR_ID = #{mbrId} 
                AND MI.MBR_GRADE='MG122'
                AND CI.USE_YN = 'Y'
            ORDER BY CI.CAMP_SEQ;
            </when>
            <otherwise>
            SELECT 
                 CI.CAMP_SEQ AS cd
                ,CI.NAME AS cdSubject
            FROM CAMP_INFO CI
            INNER JOIN ICM_REL IR2 ON (IR2.ICM_SECT = 'RE002' AND IR2.REL_SECT = 'RE001' AND IR2.REL_SEQ = #{insSeq})
            WHERE CI.CAMP_SEQ = IR2.ICM_SEQ
                AND CI.USE_YN = 'Y'
            ORDER BY CI.CAMP_SEQ;
            </otherwise>
        </choose>
    </select>
    
     <!-- Student : campSeq -->
    <select id="getCampusSeq" parameterType="com.visangesl.treeadmin.amg.vo.StudentCondition" resultType="string">
        SELECT
            CI.CAMP_SEQ
        FROM
            CAMP_INFO CI
        INNER JOIN ICM_REL IR ON (REL_SECT='RE001' AND REL_SEQ=#{insSeq} AND ICM_SEQ=CI.CAMP_SEQ)
        WHERE
            CI.ID = binary(#{campId})
    </select>
    
</mapper>