<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//ibatis.apache.org//DTD Mapper 3.0//EN" "http://ibatis.apache.org/dtd/ibatis-3-mapper.dtd">
<mapper namespace="member">

    <!-- 로그인 처리 : 관리자 정보를 가지고 온다. -->
    <select id="signIn" parameterType="com.visangesl.treeadmin.member.vo.Member" resultType="com.visangesl.treeadmin.member.vo.UserSession">
        SELECT
            MBR_ID AS memberId
            ,NAME AS memberName
            ,NICKNAME AS nickname
            ,MBR_GRADE AS mbrGrade
            ,PROFILE_PHOTO_PATH AS profileImgPath
            ,USE_YN AS useYn
        FROM MBR_INFO
        WHERE 1=1
        AND MBR_ID = #{mbrId}
        AND PWD = SHA1(#{pwd})
        AND USE_YN = 'Y'
    </select>


    <!-- 로그인 처리 : 관리자 정보를 가지고 온다. -->
    <select id="signIn_out" parameterType="com.visangesl.treeadmin.member.vo.MemberCondition" resultType="com.visangesl.treeadmin.member.vo.UserSession">
        SELECT MBR_ID as mbrId
             , NAME as nm
             , USE_YN as useYn
          FROM MBR_INFO
         WHERE MBR_ID=#{mbrId}
           AND USE_YN = 'Y'
    </select>

    <select id="getDataList" parameterType="com.visangesl.treeadmin.member.vo.MemberCondition" resultType="com.visangesl.treeadmin.member.vo.Member">
        SELECT
            MBR_ID AS #{mbrId}
            PWD AS #{pwd}
            NAME AS #{name}
            NICKNAME AS #{nickName}
            EMAIL AS #{email}
            SEX_SECT AS #{sexSect}
            BIRTHDAY AS #{birthDay}
            TEL_NO AS #{telNo}
            USE_YN AS #{useYn}
            REG_DATE AS #{regDate}
            MOD_DATE AS #{modDate}
            QUIT_DATE_TIME AS #{quitDateTime}
            PROFILE_PHOTO_PATH AS #{profilePhotoPath}
            MEMO AS #{memo}
            MBR_GRADE AS #{mbrGrade}
            STAT AS #{stat}
            SECT AS #{sect}
            NATION AS #{nation}
            HOMEPAGE AS #{homePage}
            DIRECTOR AS #{director}
            PROVINCE AS #{province}
            REG_ID AS #{regId}
            MOD_ID AS #{modId}
        FROM MBR_INFO
        WHERE 1=1
        AND MBR_ID = #{mbrId}
    </select>

    <select id="getData" parameterType="com.visangesl.treeadmin.member.vo.MemberCondition" resultType="com.visangesl.treeadmin.member.vo.Member">
        SELECT
            MBR_ID AS mbrId
            ,PWD AS pwd
            ,NAME AS name
            ,NICKNAME AS nickName
            ,EMAIL AS email
            ,SEX_SECT AS sexSect
            ,BIRTHDAY AS birthDay
            ,TEL_NO AS telNo
            ,USE_YN AS useYn
            ,REG_DATE AS regDate
            ,MOD_DATE AS modDate
            ,QUIT_DATE_TIME AS quitDateTime
            ,PROFILE_PHOTO_PATH AS profilePhotoPath
            ,MEMO AS memo
            ,MBR_GRADE AS mbrGrade
            ,STAT AS stat
            ,SECT AS sect
            ,NATION AS nation
            ,HOMEPAGE AS homePage
            ,DIRECTOR AS director
            ,PROVINCE AS province
            ,REG_ID AS regId
            ,MOD_ID AS modId
        FROM MBR_INFO
        WHERE 1=1
        AND MBR_ID = #{mbrId}
    </select>

    <select id="getDataListCnt" parameterType="com.visangesl.treeadmin.member.vo.MemberCondition" resultType="int">
        SELECT
            COUNT(MBR_ID)
        FROM MBR_INFO
        WHERE  MBR_ID = #{mbrId}
    </select>
    
    <!-- 회원추가 : 유저의 데이터 추가. -->
	<insert id="addDataWithResultCodeMsg" parameterType="com.visangesl.treeadmin.member.vo.Member">
        INSERT INTO MBR_INFO (
            MBR_ID
            ,PWD
            ,NAME
            ,NICKNAME
            ,EMAIL
            ,SEX_SECT
            ,BIRTHDAY
            ,TEL_NO
            ,USE_YN
            ,REG_DATE
            ,MOD_DATE
<!--             ,QUIT_DATE_TIME -->
            ,PROFILE_PHOTO_PATH
            ,MEMO
            ,MBR_GRADE
            ,STAT
            ,SECT
            ,NATION
            ,HOMEPAGE
            ,DIRECTOR
            ,PROVINCE
            ,REG_ID
            ,MOD_ID
        ) VALUES (
            #{mbrId}
            ,SHA1(#{pwd})
            ,#{name}
            ,#{nickName}
            ,#{email}
            ,#{sexSect}
            ,#{birthDay}
            ,#{telNo}
            ,#{useYn}
            ,NOW()
            ,NOW()
<!--             ,#{quitDateTime} -->
            ,#{profilePhotoPath}
            ,#{memo}
            ,#{mbrGrade}
            ,#{stat}
            ,#{sect}
            ,#{nation}
            ,#{homePage}
            ,#{director}
            ,#{province}
            ,#{regId}
            ,#{modId}
        )
	</insert>

    <!-- 회원 정보 수정 -->
    <update id="modifyDataWithResultCodeMsg" parameterType="com.visangesl.treeadmin.member.vo.Member">
        UPDATE MBR_INFO
        SET
             MBR_ID = #{mbrId}
            <if test="pwd != null and pwd != ''">
                ,PWD = SHA1(#{pwd})
            </if>
            <if test="name != null and name != ''">
                ,NAME = #{name}
            </if>
            <if test="nickName != null and nickName != ''">
                ,NICKNAME = #{nickName}
            </if>
            <if test="email != null and email != ''">
                ,EMAIL = #{email}
            </if>
            <if test="sexSect != null and sexSect != ''">
                ,SEX_SECT = #{sexSect}
            </if>
            <if test="birthDay != null and birthDay != ''">
                ,BIRTHDAY = #{birthDay}
            </if>
            <if test="telNo != null and telNo != ''">
                ,TEL_NO = #{telNo}
            </if>
            <if test="useYn != null and useYn != ''">
                ,USE_YN = #{useYn}
            </if>
                ,MOD_DATE = NOW()
            <if test="quitDateTime != null and quitDateTime != ''">
                ,QUIT_DATE_TIME = #{quitDateTime}
            </if>
            <if test="profilePhotoPath != null">
                ,PROFILE_PHOTO_PATH = #{profilePhotoPath}
            </if>
            <if test="memo != null and memo != ''">
                ,MEMO = #{memo}
            </if>
            <if test="mbrGrade != null and mbrGrade != ''">
                ,MBR_GRADE = #{mbrGrade}
            </if>
            <if test="stat != null and stat != ''">
                ,STAT = #{stat}
            </if>
            <if test="sect != null and sect != ''">
                ,SECT = #{sect}
            </if>
            <if test="nation != null and nation != ''">
                ,NATION = #{nation}
            </if>
            <if test="homePage != null and homePage != ''">
                ,HOMEPAGE = #{homePage}
            </if>
            <if test="director != null and director != ''">
                ,DIRECTOR = #{director}
            </if>
            <if test="province != null and province != ''">
               ,PROVINCE = #{province}
            </if>
            <if test="modId != null and modId != ''">
                ,MOD_ID = #{modId}
            </if>
        WHERE MBR_ID = #{mbrId}
    </update>

</mapper>
