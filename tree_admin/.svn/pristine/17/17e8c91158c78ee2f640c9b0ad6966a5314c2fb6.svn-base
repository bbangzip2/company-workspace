package com.visangesl.treeadmin.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.ui.Model;

public class TreeAdminUtil {
    public static final String rowsep = "\f";     // 행 분리자
    public static final String colsep = "!^"; // 열 분리자
    public static final String arrsep = "\b"; // 배열 분리자
    public static final String rssep = "!@";  // 레코드셋 분리자

    public static final String splitrowsep = "\\f";           // java의 split 메소드에서 사용할때 행 분리자
    public static final String splitcolsep = "\\!\\^";        // java의 split 메소드에서 사용할때 열 분리자
    public static final String splitarrsep = "\\b";           // java의 split 메소드에서 사용할때 배열 분리자
    public static final String splitrssep = "\\!\\@"; // java의 split 메소드에서 사용할때 레코드셋 분리자

    public static String isNullCheck(String str) {
        if ((str == null) || (str.trim().equals("")) || (str.trim().equalsIgnoreCase("null")) || (str.trim().length() == 0) || (str.equalsIgnoreCase("undefined")))
            return "";
        else
            return str.trim();
    }

    public static String isNull(String str) {
        if ((str == null) || (str.trim().equals("")) || (str.trim().equalsIgnoreCase("null")) || (str.trim().length() == 0) || (str.equalsIgnoreCase("undefined")))
            return "";
        else
            return str.trim();
    }

    public static String isNullWithNull(String str) {
        if ((str == null) || (str.trim().equals("")) || (str.trim().equalsIgnoreCase("null")) || (str.trim().length() == 0) || (str.equalsIgnoreCase("undefined")))
            return null;
        else
            return str.trim();
    }

    public static String isNull(String str, String str2) {
        if ((str == null) || (str.trim().equals("")) || (str.trim().equalsIgnoreCase("null")) || (str.trim().length() == 0) || (str.equalsIgnoreCase("undefined")))
            return str2;
        else
            return str.trim();
    }

    public static String isNullNumber(String str) {
        if ((str == null) || (str.trim().equals("")) || (str.trim().equalsIgnoreCase("null")) || (str.trim().length() == 0) || (str.equalsIgnoreCase("undefined")))
            return "1";
        else
            return str.trim();
    }

    public static int isNumber(int num) {
        if (num < 0 || num == 0) {
            return 1;
        } else {
            return num;
        }
    }

    public static int isNumber(int num, int _num) {
        if (num < 0 || num == 0) {
            return _num;
        } else {
            return num;
        }
    }

    public static int isNumber(String num, int _num) {
        int tmpNum = 0;

        try {
            tmpNum = Integer.parseInt(num);
        } catch (NumberFormatException nfe) {
            tmpNum = _num;
        }

        return tmpNum;
    }

    public static int isNewButtonCheck(String date) {
        try {
            String today = getTodayFormat();
            date = replace(date, "-", "");
            int check = Integer.parseInt(today) - Integer.parseInt(date);

            if (check >= 0 || check <= 7) {
                return 1;
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }

    }

    public static String getKenString(String str, int len) throws UnsupportedEncodingException {

        String fStr = str;
        int sLen = str.length();

        if (sLen > len) {
            fStr = str.substring(0, len) + "...";
        }

        return fStr;
    }

    public static String getRandomString(int length) {
        length = length <= 0 ? 5 : length;
        String randomString = RandomStringUtils.random(length, true, true);

        return randomString;
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Function : getDateFormat
     * Parameter : String format - 반환 받고자하는 데이터 형식(yyyyMMdd/yyyy-MM-dd 등)
     * Description : 입력된 format Parameter와 일치하는 형식으로 현재날짜를 반환한다.
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getDateFormat(String format) {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Function : getDateFormat
     * Parameter : String format - 반환 받고자하는 데이터 형식(yyyyMMdd/yyyy-MM-dd 등)
    	   Date date_val - 반환될 날짜 데이터값
     * Description : 입력된 format Parameter와 일치하는 형식으로 입력된 날짜를 반환한다.
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getDateFormat(String format, Date date_val) {
        Date dt = date_val;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 날짜를 반환한다. (yyyyMMdd)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayFormat() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 날짜를 반환한다. (yyyy-MM-dd)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayFormat2() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 날짜를 반환한다. (yyyy-MM-dd-hh-mm-ss)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayFormat3() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 날짜를 반환한다. (yyyyMMddhhmmss)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayFormat4() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 날짜를 반환한다. (yyyy-MM-dd hh:mm:ss)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayFormat5() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayYear Parameter + Description + 오늘 년도를 반환한다. (yyyy) ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayYear() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayMonth Parameter + Description + 오늘 월를 반환한다. (MM) ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayMonth() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayDay Parameter + Description + 오늘 날짜를 반환한다. (dd) ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayDay() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        return sdf.format(dt);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 요일을 반환한다.(문자형식) (월요일)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayCalendar() {
        Calendar cal = Calendar.getInstance();
        String[] week = { "일", "월", "화", "수", "목", "금", "토" };

        return week[cal.get(Calendar.DAY_OF_WEEK) - 1] + "요일";
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 오늘 요일을 반환한다.(숫자형식) (1)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static int getTodayCalendar2() {
        Calendar cal = Calendar.getInstance();
        int[] week = { 1, 2, 3, 4, 5, 6, 7 };   // 참조 : 일요일은 1 토요일은 7

        return week[cal.get(Calendar.DAY_OF_WEEK) - 1];
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getTodayFormat Parameter + Description + 숫자형식 요일을 문자형식 요일로 변환하여 반환한다 (1:일요일~~~)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getTodayCalendar(int i_week) {
        // String[] week = {"일","월","화","수","목","금","토"};
        String week = "";
        if (i_week == 1) {
            week = "일";
        } else if (i_week == 2) {
            week = "월";
        } else if (i_week == 3) {
            week = "화";
        } else if (i_week == 4) {
            week = "수";
        } else if (i_week == 5) {
            week = "목";
        } else if (i_week == 6) {
            week = "금";
        } else if (i_week == 7) {
            week = "토";
        }

        return week + "요일";
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getSelected Parameter + param1, param2 Description + 두 값을 비교해서 같으면 "Selected" 를 반환한다.
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getSelected(String param1, String param2) {
        if (param1.equals(param2))
            return "Selected";
        else
            return "";
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getChecked Parameter + param1, param2 Description + 두 값을 비교해서 같으면 "Checked" 를 반환한다.
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String getChecked(String param1, String param2) {
        if (param1.equals(param2))
            return "Checked";
        else
            return "";
    }

    /*
     * 파일 업로드취약점
     */
    public int fileUploadCheck(String fileName) {
        int result = 0;
        String check = fileName.substring(fileName.lastIndexOf("."));

        if (check.equalsIgnoreCase(".php") || check.equalsIgnoreCase(".php3") || check.equalsIgnoreCase(".asp") || check.equalsIgnoreCase(".jsp") || check.equalsIgnoreCase(".cgi")
                || check.equalsIgnoreCase(".inc") || check.equalsIgnoreCase(".pl") || check.equalsIgnoreCase(".exe") || check.equalsIgnoreCase(".sh") || check.equalsIgnoreCase(".bat")) {
            result = 1;
        } else {
            result = 0;
        }

        return result;
    }

    /*
     * 이미지파일업로드 체크
     */
    public int fileUploadCheckJpg(String fileName) {
        int result = 0;
        String check = fileName.substring(fileName.lastIndexOf("."));

        if (check.equalsIgnoreCase(".jpg") || check.equalsIgnoreCase(".jpeg") || check.equalsIgnoreCase(".gif")) {
            result = 1;
        } else {
            result = 0;
        }

        return result;
    }

    /*
     * 동영상파일업로드 체크
     */
    public int fileUploadCheckMovie(String fileName) {
        int result = 0;
        String check = fileName.substring(fileName.lastIndexOf("."));

        if (check.equalsIgnoreCase(".wmv") || check.equalsIgnoreCase(".avi")) {
            result = 1;
        } else {
            result = 0;
        }

        return result;
    }

    /*
     * 파일 확장자 가져오기
     */
    public static String fileUploadExt(String fileName) {
        String check = fileName.substring(fileName.lastIndexOf("."));
        check = TreeAdminUtil.replace(check, ".", "");

        return check;
    }

    /*
     * 문자 치환 replace "123" ,"2", "" -> "13"
     */
    public static String replace(String _src, String _target, String _dest) {
        if (_src == null || _src.trim().length() == 0)
            return _src;
        if (_target == null)
            return _src;

        StringBuffer tmpBuffer = new StringBuffer();

        int nStart = 0;
        int nEnd = 0;
        int nLength = _src.length();
        int nTargetLength = _target.length();

        while (nEnd < nLength) {
            nStart = _src.indexOf(_target, nEnd);
            if (nStart < 0) {
                tmpBuffer.append(_src.substring(nEnd, nLength));

                break;
            } else {
                tmpBuffer.append(_src.substring(nEnd, nStart)).append(_dest);

                nEnd = nStart + nTargetLength;
            }
        }

        return tmpBuffer.toString();
    }

    /**
     * 데이터 형이 숫자인지 문자인지 체크하는 메소드
     *
     * @param value
     *            숫자면 true 문자면 false
     * @return boolean
     */
    public static boolean checkInt(String value) {
        boolean returnVal = false;
        int a = 0;
        int b = 0;
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            if (0x30 <= c && c <= 0x39) {
                a++;
            } else {
                b++;
            }
        }
        if (a > 0) {
            returnVal = true;
        } else if (b > 0) {
            returnVal = false;
        }
        return returnVal;
    }

    /**
     * 영문/ 숫자 존재 여부 체크 메서드 *
     *
     * @param value
     * @return 0:영문/숫자 미존재 1:영문/숫자 존재 2:영문 존재 3:숫자 존재
     */
    public static int checkStrInt(String value) {
        int type = 0;   // 0:영문/숫자 미존재 1:영문/숫자 존재 2:영문 존재 3:숫자 존재
        int type1 = 0; // 영문
        int type2 = 0; // 숫자
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            // 영문
            if ((0x61 <= c && c <= 0x7A) || (0x41 <= c && c <= 0x5A)) {
                type1 = 1;
                // 숫자
            } else if (0x30 <= c && c <= 0x39) {
                type2 = 1;
            }
        }

        if (type1 == 0 && type2 == 0) {   // 영문 숫자 미존재
            type = 0;
        } else if (type1 == 1 && type2 == 0) {             // 영문만 존재
            type = 2;
        } else if (type1 == 0 && type2 == 1) {             // 숫자만 존재
            type = 3;
        } else {  // 영문/숫자 존재
            type = 1;
        }

        return type;
    }

    /**
     * System.out.print를 줄이기 위한 메소드
     *
     * @param value
     */
    public static void nPrint(String value) {
        //logger.debug(value);
    }

    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Function + getSplit Parameter + str, param Description + str 문자를 param 구분자로 Split 한 후 배열로 반환한다.
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static String[] getSplit(String str, String param) {
        StringTokenizer st = new StringTokenizer(str, param);
        String[] split = new String[st.countTokens()];
        int i = 0;

        while (st.hasMoreTokens()) {
            split[i] = st.nextToken();
            i++;
        }
        return split;
    }

    /**
     * session NULL 체크
     */
    public String getSession(HttpSession session, String attrName) {
        return session.getAttribute(attrName) != null ? (String) session.getAttribute(attrName) : "";
    }

    public static String getUniqueFileName(String path, String file) {
        File tmp = new File(path + file.toLowerCase());
        String fileName = file.toLowerCase();
        int i = 0;

        //logger.debug("------------------>exist" + tmp.exists());
        if (tmp.exists()) {
            while (tmp.exists()) {
                if (fileName.indexOf(".") != -1) {
                    String lcTemp = "(" + i + ").";
                    fileName = file.toLowerCase().replaceAll(".", lcTemp);
                } else {
                    fileName = file.toLowerCase() + "(" + i + ")";
                    tmp = new File(path + fileName);
                    i++;
                }
            }
        }
        return fileName;
    }

    /**
     * 이메일 주소 유효성 체크
     *
     * @param email
     * @return boolean
     */
    public static boolean isEmail(String email) {
        if (email == null)
            return false;
        boolean b = Pattern.matches("[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+", email.trim());
        return b;
    }

    /**
     * 현재 유닉스 타임 가져오기
     *
     * @return
     */
    public static long getTodayUnixtime() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * 현재 유닉스 타임 + 초
     *
     * @param secend
     * @return
     */
    public static long getTodayUnixtime(int secend) {
        return System.currentTimeMillis() / 1000 + secend;
    }

    /**
     * 날짜 더하기 메서드
     *
     * @param dateString
     *            YYYYMMDD
     * @param addDate
     *            더할 날짜 (3 -> 3일)
     * @return YYYYMMDD
     */
    public static String getTodayAddDate(String dateString, int addDate) {
        String result = "";

        // String dateString = "20120301";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        try {
            Date date = formatter.parse(dateString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_MONTH, +(addDate - 1));
            result = formatter.format(calendar.getTime());
        } catch (ParseException e) {
        }

        return result;
    }

    /**
     * 전체 자릿수와 숫자형 데이터를 입력받아 앞에 0을 붙인다 ex) appendLeftZero("1", 4) -> 0001
     *
     * @param number
     *            숫자형 데이터
     * @param size
     *            전체 자릿수
     * @return
     */
    public static String appendLeftZero(String number, int size) {
        String result = number;
        int end = size - number.length();
        for (int i = 0; i < end; i++) {
            result = "0" + result;
        }

        return result;

    }

    /**
     * 프로퍼티 파일 경로와 프로퍼티 키를 입력받아 해당 프로퍼티 파일 안에 있는 키에 해당하는 값을 읽어온다
     *
     * @param filepath
     *            프로퍼티 파일 경로
     * @param key
     *            입력받은 프로퍼티 파일 경로에 있는 프로퍼티 파일에 있는 프로퍼티 키
     * @return 프로퍼티 값
     */
    public static String getProperties(String filepath, String key) {
        String result = "";
        Properties props = null;
        FileInputStream fis = null;

        try {
            props = new Properties();
            fis = new FileInputStream(filepath);
            props.load(new java.io.BufferedInputStream(fis));
            result = props.getProperty(key).trim();
        } catch (Exception e) {

        } finally {
            try {
                fis.close();
            } catch (Exception e) {

            }
        }

        return result;
    }

    /**
     * 프로퍼티 파일 경로를 입력받아 해당 프로퍼티 파일의 key 값들을 가져온다
     *
     * @param filepath
     *            프로퍼티 파일 경로
     * @return 프로퍼티 key값들
     */
    public static List<String> getPropertiesKeyset(String filepath) {
        List<String> result = new ArrayList<String>();
        Properties props = null;
        FileInputStream fis = null;

        try {
            props = new Properties();
            fis = new FileInputStream(filepath);
            props.load(new java.io.BufferedInputStream(fis));
            Set keyset = props.keySet();
            Iterator iter = keyset.iterator();
            while (iter.hasNext()) {
                String key = (String) (iter.next());
                result.add(key);
            }
        } catch (Exception e) {

        } finally {
            try {
                fis.close();
            } catch (Exception e) {

            }
        }

        return result;
    }


    /***************************************************
     * 작성자 : 김남배
     * 작성일 : 2012.07.26
     * 내  용 : 공백문자 제거용 함수 (예 : 키워드 검색시 사용)
     ***************************************************/
    public static String getWithoutSpace(String str) {
    	String tmp = str.trim();
    	tmp	= tmp.replace(" ", "");
    	return tmp;
    }

    /***************************************************
     * 작성자 : 이홍
     * 작성일 : 2012.08.13
     * 내  용 : 콤마(,) 를 제외한 모든 특수문자(Character Entities) 제거함수
     ***************************************************/
    public static String replaceCEstr(String str){
        int str_length = str.length();
        String strlistchar = "";
        String str_imsi = "";
        String[] filter_word = {"","\\.","\\?","\\/","\\~","\\!","\\@","\\#","\\$","\\%","\\^","\\&","\\*","\\(","\\)","\\_","\\+","\\=","\\|","\\\\","\\}","\\]","\\{","\\[","\\\"","\\'","\\:","\\;","\\<","\\>","\\.","\\?","\\/"};

        for(int i=0;i<filter_word.length;i++){
            str_imsi = str.replaceAll(filter_word[i],"");
            str = str_imsi;
        }

        return str;

    }

    public static String listToString(List<String> src, String seperator) {
        StringBuffer sb = new StringBuffer();

        for (String tb : src) {
            if ((tb != null) && (!tb.equals(""))) {
                sb.append(tb);
                sb.append(seperator);
            }
        }

        if (sb.length() != 0)
            sb.deleteCharAt(sb.length() - 1);

        return (sb.length() == 0) ? null : sb.toString();
    }



    /***************************************************
     * 작성자 : 심원보
     * 작성일 : 2012.12.26
     * 내  용 : HTML 태그 제거함수
     ***************************************************/
    public static String getClearHtmlStr(String content){
    	Pattern SCRIPTS = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>",Pattern.DOTALL);
    	Pattern STYLE = Pattern.compile("<style[^>]*>.*</style>",Pattern.DOTALL);
    	Pattern TAGS = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>");
    	Pattern nTAGS = Pattern.compile("<\\w+\\s+[^<]*\\s*>");
    	Pattern ENTITY_REFS = Pattern.compile("&[^;]+;");
    	Pattern WHITESPACE = Pattern.compile("\\s\\s+");

    	Matcher m;
    	m = SCRIPTS.matcher(content);
    	content = m.replaceAll("");
    	m = STYLE.matcher(content);
    	content = m.replaceAll("");
    	m = TAGS.matcher(content);
    	content = m.replaceAll("");
    	m = ENTITY_REFS.matcher(content);
    	content = m.replaceAll("");
    	m = WHITESPACE.matcher(content);
    	content = m.replaceAll(" ");

        return content;

    }

    public static void showSubView(HttpServletRequest request, Model model) {
        String subUrl = TreeAdminUtil.isNullWithNull(request.getParameter("subUrl"));

        model.addAttribute("subUrl", subUrl);
    }

    /**
     *
     * 이미지 사이즈 줄이기!!!
     *
     * @param originalImage
     * @param imgWidth
     * @param imgHeight
     * @param type
     * @return
     */
    public static BufferedImage resizeImage(BufferedImage originalImage, int imgWidth, int imgHeight,  int type){
		BufferedImage resizedImage = new BufferedImage(imgWidth, imgHeight, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, imgWidth, imgHeight, null);
		g.dispose();

		return resizedImage;
    }

    /***************************************************
     * 작성자 : 심원보
     * 작성일 : 2013.09.10
     * 내  용 : request가 모바일인지 PC인지 체크
     ***************************************************/
    public static String getMobileAgentYn(HttpServletRequest request) {
    	String mobileAgentYn = "N";
    	String userAgent = request.getHeader("user-agent");
    	String[] browser = {"iPhone", "iPod", "BlackBerry", "Android", "Windows CE", "LG", "MOT", "SAMSUNG", "SonyEricsson", "HTC", "Server_KO_SKT", "SKT", "IEMobile"};

    	for (int i = 0; i < browser.length; i++) {
    		if(userAgent.matches(".*" + browser[i] + ".*")) {
    			mobileAgentYn = "Y";

    			break;
    	    }
    	}

    	return mobileAgentYn;
    }

    /***************************************************
     * 작성자 : 심원보
     * 작성일 : 2013.12.16
     * 내  용 : 내부 로컬 아이피 여부(Y : 내부 로컬 아이피 / N : 외부 아이피)
     ***************************************************/
    public static String getRemoteAddrYn(HttpServletRequest request) {
    	String remoteAddrYn = "N";
    	String remoteAddr = request.getRemoteAddr();

    	if (remoteAddr.substring(0, 8).equals("10.30.0.") || remoteAddr.substring(0, 9).equals("106.241.5")) {
    		remoteAddrYn = "Y";
    	}

    	return remoteAddrYn;
    }


    /*
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Function: getDayList
     * Parameter:
     *   startDay - 기간 시작 날짜 (yyyy-mm-dd)
     *   endDay - 기간 끝 날짜 (yyyy-mm-dd)
     *   dayStr - 특정 요일 리스트 (1:일요일~~~) [1;3;5;]
     * Description: 일정 기간내 특정 요일의 일자를 반환
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public static List<String> getDayList(String startDay, String endDay, String dayStr) {

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat transDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<String> dayList = new ArrayList<String>();

        try {
            Date startDate = transDateFormat.parse(startDay);
            Date endDate = transDateFormat.parse(endDay);

            // 시작 일부터 시작해서 계산함.
            cal.setTime(startDate);

            int i = 0;
            while(cal.getTime().compareTo(endDate) <= 0) { // 끝 날짜가 될때까지 시작 날짜에 하루씩을 더해 날짜 비교 종료일까지 비교함

                // 특정일의 요일 조회 1:일요일 ~
                int day = cal.get(Calendar.DAY_OF_WEEK);

                // 요일 비교
                if (dayStr.indexOf(String.valueOf(day)) > -1) {
                    dayList.add(transDateFormat.format(cal.getTime()));
                }

                cal.add(Calendar.DATE, 1); // 날짜에 하루씩 더하기

            }
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println(e);
        }
//        for (String str : dayList) {
//            System.out.println(str);
//        }
        return dayList;
    }


    /**
     * Method to get a JSON string representing the object supplied.
     *
     * @param object for which JSON string is required
     * @return Json String
     * @throws IOException
     */
    public static String createJsonString(Object object) throws IOException {
        String result = null;
        if (object != null) {
            Writer writer = new StringWriter();
            JsonFactory factory = new JsonFactory();
            JsonGenerator generator = factory.createJsonGenerator(writer);
            generator.setCodec(new ObjectMapper());
            generator.writeObject(object);
            generator.close();

            result = writer.toString();
            System.out.println("Json string " + result);

            if (result.equals("[null]")) {
                result = null;
            }
        }

        return result;
    }


    /**
     * fileAddress에서 파일을 읽어, 다운로드 디렉토리에 다운로드
     *
     * @param fileAddress
     * @param localFileName
     * @param downloadDir
     */
    public static int fileUrlReadAndDownload(String fileUrl, String localFileName, String downloadDir) {
        int bufferSize = 1024;
        OutputStream outStream = null;
        URLConnection uCon = null;

        InputStream is = null;
        int byteWritten = 0;
        try {

            if (!new File(downloadDir).exists()) {
                new File(downloadDir).mkdirs();
            }

            if (localFileName == null || localFileName.equals("")) {
                int slashIndex = fileUrl.lastIndexOf('/');
                int periodIndex = fileUrl.lastIndexOf('.');
                // 파일 어드레스에서 마지막에 있는 파일이름을 취득
                localFileName = fileUrl.substring(slashIndex + 1);
            }

            URL Url;
            byte[] buf;
            int byteRead;
            Url = new URL(fileUrl);
            outStream = new BufferedOutputStream(new FileOutputStream(downloadDir + File.separator + URLDecoder.decode(localFileName, "UTF-8")));

            uCon = Url.openConnection();
            is = uCon.getInputStream();
            buf = new byte[bufferSize];
            while ((byteRead = is.read(buf)) != -1) {
                outStream.write(buf, 0, byteRead);
                byteWritten += byteRead;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return byteWritten;
    }


    /***************************************************
     * 작성자 :
     * 작성일 : 2014.05.15
     * 내  용 : 폴더/파일 복사
     ***************************************************/
    public static boolean fileCopy(File asIsDir, File toBeDir) {

        boolean result = true;

        File[] ff = asIsDir.listFiles();
        for (File file : ff) {
            File temp = new File(toBeDir.getAbsolutePath() + File.separator + file.getName());
            System.out.println(temp.getAbsolutePath());
            if (file.isDirectory()) {
                temp.mkdir();
                fileCopy(file, temp);
            } else {
                FileInputStream fis = null;
                FileOutputStream fos = null;
                try {
                    fis = new FileInputStream(file);
                    fos = new FileOutputStream(temp);
                    byte[] b = new byte[4096];
                    int cnt = 0;
                    while ((cnt = fis.read(b)) != -1) {
                        fos.write(b, 0, cnt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    result = false;
                    break;
                } finally {
                    try {
                        fis.close();
                        fos.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
        // 예외 발생 시 복사 진행된 폴더를 삭제
        if (!result) {
            deleteDir(toBeDir);
        }
        return result;
    }


    /***************************************************
     * 작성자 :
     * 작성일 : 2014.05.15
     * 내  용 : 폴더/파일 삭제
     ***************************************************/
    public static boolean deleteDir(File toBeDir) {

        if (!toBeDir.exists()) {
            return false;
        }

        File[] files = toBeDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                file.delete();
            }
        }
        return toBeDir.delete();
    }


}



