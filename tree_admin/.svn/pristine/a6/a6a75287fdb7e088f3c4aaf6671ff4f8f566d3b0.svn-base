<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="classinfo">

    <!-- Class Info List : 클래스 정보 리스트 count. -->
    <select id="getDataListCnt" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultType="int">
              SELECT
                     count(1)
                FROM CLS_INFO CI INNER JOIN ICM_REL IRC ON IRC.ICM_SECT ='RE003' AND CI.CLS_SEQ = IRC.ICM_SEQ
                 INNER JOIN ICM_REL CAMP ON CAMP.ICM_SECT ='RE002' AND CAMP.ICM_SEQ = IRC.REL_SEQ AND CAMP.REL_SEQ = #{insInfoSeq}
                WHERE 1 = 1
                <if test="campSeq != null and campSeq != '' ">
                    AND CAMP.ICM_SEQ = #{campSeq}
                </if>
                <if test="startDate != null and startDate != '' ">
                    AND CI.START_YMD &gt;&#61;  #{startDate}
                </if>
                <if test="endDate != null and endDate != '' ">
                    AND CI.END_YMD  &lt;&#61;  #{endDate}
                </if>      
                <if test="useYn != null and useYn != '' ">
                    AND CI.USE_YN = #{useYn}
                </if>                          
                <if test="classNm != null and classNm != '' ">
                    AND CI.NAME LIKE '%${classNm}%'
                </if>                                               
    </select>
    
    <!-- Class Info List : 클래스 정보 리스트를 반환. -->
    <select id="getDataList" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultMap="classListResultMap">
              SELECT
                      CI.CLS_SEQ AS classSeq
                    , CI.`NAME` AS classNm
                    , CI.PROG_SEQ AS progSeq
                    , (SELECT NAME FROM PROG_INFO WHERE PROG_SEQ = CI.PROG_SEQ) AS progNm
                    , IRC.REL_SEQ  AS campSeq
                    , (SELECT NAME FROM CAMP_INFO WHERE CAMP_SEQ = IRC.REL_SEQ) AS campNm
                    , MI.NAME AS name
                    , MI.MBR_ID AS mbrId    
                    , DATE_FORMAT(CI.START_YMD, '%Y.%m.%d') AS startYmd   
                    , DATE_FORMAT(CI.END_YMD, '%Y.%m.%d') AS endYmd
                    , CI.WK_NM AS wkNm
                    , CI.START_HM AS startHm
                    , CI.END_HM AS endHm
                    , CI.DETAIL_CONTENT AS detailContent
                    , CI.USE_YN AS useYn
                    , (SELECT COUNT(1)
                        FROM ICM_REL IR  INNER JOIN MBR_INFO MI ON MI.MBR_ID = IR.ICM_SEQ AND MI.MBR_GRADE ='MG220'
                        WHERE IR.ICM_SECT = 'RE004' AND REL_SECT ='RE003' AND REL_SEQ = CI.CLS_SEQ ) AS studentCount
                FROM CLS_INFO CI INNER JOIN ICM_REL IRC ON IRC.ICM_SECT ='RE003' AND CI.CLS_SEQ = IRC.ICM_SEQ
                 INNER JOIN ICM_REL CAMP ON CAMP.ICM_SECT ='RE002' AND CAMP.ICM_SEQ = IRC.REL_SEQ AND CAMP.REL_SEQ = #{insInfoSeq}
                 INNER JOIN ICM_REL TCH ON TCH.ICM_SECT ='RE004' AND TCH.REL_SECT= 'RE003' AND TCH.REL_SEQ = CI.CLS_SEQ
                 INNER JOIN MBR_INFO MI ON TCH.ICM_SEQ = MI.MBR_ID AND MI.MBR_GRADE ='MG210'                 
                WHERE 1 = 1
                <if test="campSeq != null and campSeq != '' ">
                    AND CAMP.ICM_SEQ = #{campSeq}
                </if>
                <if test="startDate != null and startDate != '' ">
                    AND CI.START_YMD &gt;&#61;  #{startDate}
                </if>
                <if test="endDate != null and endDate != '' ">
                    AND CI.END_YMD  &lt;&#61;  #{endDate}
                </if>      
                <if test="useYn != null and useYn != '' ">
                    AND CI.USE_YN = #{useYn}
                </if>                          
                <if test="classNm != null and classNm != '' ">
                    AND CI.NAME LIKE '%${classNm}%'
                </if>                                               
                ORDER BY ${orderColumn} ${sort}
        LIMIT #{pageStartIndex}, #{pageSize}
    </select>
	
    <resultMap id="classListResultMap" type="com.visangesl.treeadmin.amg.vo.ClassInfo">
        <id property="classSeq" column="classSeq"/>
        <result property="rnum" column="rnum" />
        <result property="classNm" column="classNm" />
        <result property="progSeq" column="progSeq" />
        <result property="progNm" column="progNm" />
        <result property="campSeq" column="campSeq" />
        <result property="campNm" column="campNm" />
        <result property="startYmd" column="startYmd" />
        <result property="endYmd" column="endYmd" />
        <result property="wkNm" column="wkNm" />
        <result property="startHm" column="startHm" />
        <result property="endHm" column="endHm" />
        <result property="detailContent" column="detailContent" />
        <result property="useYn" column="useYn" />
        <result property="studentCount" column="studentCount" />
        
        <!-- 해당 클래스의 정보 및 해당 클래스의 캠퍼스 정보 리스트. -->
        <collection property="teacherList" ofType="com.visangesl.treeadmin.amg.vo.Teacher">
            <id property="mbrId" column="mbrId" />
            <result property="mbrId" column="mbrId" />
            <result property="name" column="name" />
        </collection>
        
    </resultMap>
    	
	<!-- 클래스 상세 정보 조회  -->
	<select id="getData" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultType="com.visangesl.treeadmin.amg.vo.ClassInfo">
		SELECT
		        CLS_SEQ AS classSeq
              , `NAME` AS classNm
              , PROG_SEQ AS progSeq
              , IR.REL_SEQ AS campSeq
              , (SELECT NAME FROM CAMP_INFO WHERE CAMP_SEQ = IR.REL_SEQ) AS campNm
              , DATE_FORMAT(CI.START_YMD, '%Y.%m.%d') AS startYmd   
              , DATE_FORMAT(CI.END_YMD, '%Y.%m.%d') AS endYmd
              , WK_NM AS wkNm
              , START_HM AS startHm
              , END_HM AS EndHm
              , DETAIL_CONTENT AS detailContent
              , USE_YN AS useYn
		FROM CLS_INFO CI INNER JOIN ICM_REL IR ON CI.CLS_SEQ = IR.ICM_SEQ AND IR.ICM_SECT ='RE003'
		WHERE CLS_SEQ = #{classSeq}
	</select>
	
	<!-- 클래스 기본 정보 등록  (struc_mbr_id 추가 작업 -이홍 기본값을 vsadmin으로  고정함. )-->
    <insert id="addDataWithResultCodeMsg" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfo" useGeneratedKeys="true" keyProperty="classSeq">
        INSERT INTO CLS_INFO (
              NAME
            , PROG_SEQ
            , START_HM
            , END_HM
            , DETAIL_CONTENT
            , USE_YN
            , START_YMD
            , END_YMD
            , WK
            , WK_NM
            , REG_ID
            , REG_DATE
            , STRUC_MBR_ID
        ) VALUES (
              #{classNm}
            , #{progSeq}
            , #{startHm}
            , #{endHm}
            , #{detailContent}
            , #{useYn}
            , #{startYmd}
            , #{endYmd}
            , #{wk}
            , #{wkNm}
            , #{regId}
            , now()
            , 'vsadmin'
        )
    </insert>
    
    
    
    <!-- 클래스 스케쥴(TIME) 정보 조회 -->
    <select id="getSchdlDataList" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultType="com.visangesl.treeadmin.amg.vo.ClassSchdl">
		SELECT
			DISTINCT(A.CD_SUBJECT) AS wk
			 , CLS_SEQ AS clsSeq
			, START_HH AS startHH
			, START_MM AS startMM
			, END_HH AS endHH
			, END_MM AS endMM
		FROM CMM_CD A LEFT OUTER JOIN CLS_SCHDL B ON  B.CLS_SEQ = #{classSeq} AND A.CD_SUBJECT = B.WK 
		WHERE A.REL_CD ='DA000'
    </select>
    
        
    <!-- 클래스 스케쥴(TIME) 정보 등록  -->
    <insert id="addSchdlDataWithResultCodeMsg" parameterType="com.visangesl.treeadmin.amg.vo.ClassSchdl" >
        INSERT INTO CLS_SCHDL (
              CLS_SEQ
            , YMD
            , START_HH
            , START_MM
            , END_HH
            , END_MM
            , WK
            , REG_DATE
            , REG_ID
            , MOD_DATE
            , MOD_ID            
        ) VALUES (
              #{clsSeq}
            , #{ymd}
            , #{startHH}
            , #{startMM}
            , #{endHH}
            , #{endMM}
            , #{wk}
            , now()
            , #{regId}
            , now()
            , #{modId}            
        )
    </insert>
    
    <!-- 클래스 스케쥴(TIME) 정보 삭제  -->
    <delete id="deleteSchdlData" parameterType="com.visangesl.treeadmin.amg.vo.ClassSchdlCondition" >
        DELETE 
          FROM CLS_SCHDL 
         WHERE CLS_SEQ = #{clsSeq}
    </delete>
        
        
    <!-- 클래스 기본 정보 수정 처리  -->
    <update id="modifyDataWithResultCodeMsg" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfo">
        UPDATE CLS_INFO
        SET
              NAME = #{classNm}
            , PROG_SEQ = #{progSeq}
            , START_HM = #{startHm}
            , END_HM = #{endHm}
            , DETAIL_CONTENT = #{detailContent}
            , USE_YN = #{useYn}
            , START_YMD = #{startYmd}
            , END_YMD = #{endYmd}
            , WK = #{wk}
            , WK_NM = #{wkNm}
            , MOD_ID = #{modId}
            , MOD_DATE = now()
        WHERE CLS_SEQ = #{classSeq}
    </update>    
    	
    <!-- 해당 캠퍼스에 속한 선생님 목록 조회  -->	
    <select id="getCampTchList" parameterType="String" resultType="com.visangesl.treeadmin.vo.CodeVo">
		SELECT 
		  MI.MBR_ID AS cd
		, MI.NAME AS cdSubject
		FROM ICM_REL IR INNER JOIN MBR_INFO MI ON IR.ICM_SECT ='RE004' AND ICM_SEQ = MI.MBR_ID AND MI.MBR_GRADE ='MG210'
		WHERE IR.REL_SEQ = #{campSeq}
		AND IR.REL_SECT ='RE002'    
    </select>	
    
    <!-- 클래스에 속한 선생님 목록 조회  -->
    <select id="getClassTchList" parameterType="String" resultType="com.visangesl.treeadmin.vo.CodeVo">
		SELECT 
		       MI.MBR_ID AS cd
		     , MI.NAME AS cdSubject
		  FROM ICM_REL IR INNER JOIN MBR_INFO MI ON IR.ICM_SEQ = MI.MBR_ID  AND MI.MBR_GRADE ='MG210' 
		 WHERE IR.ICM_SECT ='RE004'
		   AND IR.REL_SECT ='RE003'
		   AND IR.REL_SEQ = #{classSeq}
    </select>
    
    <!-- 클래스에서 사용할수 있는 프로그램 목록 조회  -->
    <select id="getClassProgList" parameterType="String" resultType="com.visangesl.treeadmin.vo.CodeVo">
        SELECT 
               PROG_SEQ AS cd
               , `NAME` AS cdSubject
          FROM PROG_INFO   
         WHERE 1=1
           AND INSTITUTE_SEQ = #{insSeq}
           AND USE_YN ='Y'
    </select>
        
    
    
    
    
    <!-- 반배정 목록 조회 -->
    <select id="getAssignClassList" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultMap="assignListResultMap">
			SELECT 
			     II.CAMP_SEQ as campSeq
				, CI.CLS_SEQ AS classSeq
				, CI.NAME AS classNm
				, DATE_FORMAT(CI.START_YMD, '%Y.%m.%d') AS startYmd   
				, DATE_FORMAT(CI.END_YMD, '%Y.%m.%d') AS endYmd 
				, CI.WK_NM AS wkNm
				, CI.START_HM AS startHm
				, CI.END_HM AS endHm
				, CI.USE_YN AS useYn
				, TM.MBR_ID  AS teacherId
				, TM.NAME AS teacherNm
				, SM.MBR_ID AS studentId
				, SM.NAME AS studentNm
				, SM.SEX_SECT AS sexSect
				, SM.NICKNAME AS nickName
				, (SELECT COUNT(1)
				    FROM ICM_REL_INFO IR INNER JOIN MBR_INFO MI ON MI.MBR_ID = IR.MBR_ID AND MI.MBR_GRADE ='MG220'
				    WHERE IR.CLS_SEQ = CI.CLS_SEQ AND IR.MBR_ID IS NOT NULL 
				 ) AS studentCount
				, SM.MBR_GRADE
				, II.INSTITUTE_SEQ
				, II.CAMP_SEQ
			FROM  CLS_INFO CI  
			INNER JOIN ICM_REL_INFO II ON CI.CLS_SEQ = II.CLS_SEQ AND II.MBR_ID IS NOT NULL AND II.INSTITUTE_SEQ = #{insInfoSeq}
			 LEFT JOIN MBR_INFO TM ON II.MBR_ID = TM.MBR_ID AND TM.MBR_GRADE = 'MG210'
			 LEFT JOIN MBR_INFO SM ON II.MBR_ID = SM.MBR_ID AND SM.MBR_GRADE = 'MG220' 
           WHERE 1 = 1
               <if test="campSeq != null and campSeq != '' ">
                   AND II.CAMP_SEQ = #{campSeq}
               </if>
               <if test="startDate != null and startDate != '' ">
                   AND CI.START_YMD &gt;&#61;  #{startDate}
               </if>
               <if test="endDate != null and endDate != '' ">
                   AND CI.END_YMD  &lt;&#61;  #{endDate}
               </if>      
               <if test="useYn != null and useYn != '' ">
                   AND CI.USE_YN = #{useYn}
               </if>                          
               <if test="classNm != null and classNm != '' ">
                   AND CI.NAME LIKE '%${classNm}%'
               </if>                                               
               ORDER BY CI.REG_DATE ASC , SM.MBR_ID , SM.NAME
         LIMIT #{pageStartIndex}, #{pageSize}
    </select>
    
    <!-- 반배정 목록수 -->
    <select id="getAssignClassListCnt" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultType="int">
            SELECT 
                count(1)
            FROM  CLS_INFO CI  
            INNER JOIN ICM_REL_INFO II ON CI.CLS_SEQ = II.CLS_SEQ AND II.MBR_ID IS NOT NULL AND II.INSTITUTE_SEQ = '1'
               <if test="campSeq != null and campSeq != '' ">
                   AND II.CAMP_SEQ = #{campSeq}
               </if>
               <if test="startDate != null and startDate != '' ">
                   AND CI.START_YMD &gt;&#61;  #{startDate}
               </if>
               <if test="endDate != null and endDate != '' ">
                   AND CI.END_YMD  &lt;&#61;  #{endDate}
               </if>      
               <if test="useYn != null and useYn != '' ">
                   AND CI.USE_YN = #{useYn}
               </if>                          
               <if test="classNm != null and classNm != '' ">
                   AND CI.NAME LIKE '%${classNm}%'
               </if>                                               
    </select>
        
    <resultMap id="assignListResultMap" type="com.visangesl.treeadmin.amg.vo.AssignClass">
        <id property="classSeq" column="classSeq"/>
        <result property="rnum" column="rnum" />
        <result property="campSeq" column="campSeq" />
        <result property="classNm" column="classNm" />
        <result property="classSeq" column="classSeq" />
        <result property="startYmd" column="startYmd" />
        <result property="startHm" column="startHm" />
        <result property="endYmd" column="endYmd" />
        <result property="endHm" column="endHm" />
        <result property="wkNm" column="wkNm" />
        <result property="useYn" column="useYn" />
        <result property="studentCount" column="studentCount" />
        
        <collection property="teacherList" ofType="com.visangesl.treeadmin.amg.vo.AssignTeacher">
            <id property="teacherId" column="teacherId" />
            <result property="teacherId" column="teacherId" />
            <result property="teacherNm" column="teacherNm" />
        </collection>
        
        <collection property="studentList" ofType="com.visangesl.treeadmin.amg.vo.AssignStudent">
            <id property="studentId" column="studentId" />
            <result property="studentId" column="studentId" />
            <result property="studentNm" column="studentNm" />
            <result property="sexSect" column="sexSect" />
            <result property="nickName" column="nickName" />
        </collection>
                
    </resultMap>
    
    <!-- 이전 한달에 진행중인 클래스 목록  -->
    <select id="getRecentClassList" parameterType="String" resultType="com.visangesl.treeadmin.amg.vo.AssignClass">
        SELECT 
			CI.CLS_SEQ AS classSeq
			, CI.NAME AS classNm
		FROM CLS_INFO CI 
		WHERE 1=1
		AND CI.START_YMD &lt;&#61; NOW()
		AND DATE_SUB(now(), INTERVAL 1 month)  &lt;&#61; CI.END_YMD
    </select>
    
    <!-- 반배치 미정 학생 목록 -->
    <select id="getUnassignStudentList" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultType="com.visangesl.treeadmin.amg.vo.AssignStudent">
        SELECT 
				 DISTINCT(MI.MBR_ID) AS mbrId
				, MI.NAME AS mbrNm
				, MI.NICKNAME AS nickName
				, MI.SEX_SECT AS sexSect
				, IRI.CAMP_SEQ AS campSeq
				, IRI.CLS_SEQ AS classSeq
	      FROM ICM_REL_INFO IRI INNER JOIN MBR_INFO MI ON IRI.MBR_ID = MI.MBR_ID AND MI.MBR_GRADE ='MG220'
		  WHERE 1=1
		    AND CAMP_SEQ = #{campSeq}
		    AND (CLS_SEQ IS NULL AND CLS_SEQ ='')
		    ORDER BY MI.NAME ${order}
    </select>

    <!-- 캠퍼스 학생 목록/ 클래스에 속한 학생 목록  -->
    <select id="getCampusStudentList" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultMap="campusStudentMap">
        SELECT 
                 DISTINCT(MI.MBR_ID) AS mbrId
                , MI.NAME AS mbrNm
                , MI.NICKNAME AS nickName
                , MI.SEX_SECT AS sexSect
                , IRI.CAMP_SEQ AS campSeq
                , CI.CLS_SEQ AS classSeq
                , CI.NAME AS classNm
          FROM ICM_REL_INFO IRI INNER JOIN MBR_INFO MI ON IRI.MBR_ID = MI.MBR_ID AND MI.MBR_GRADE ='MG220'
			  LEFT JOIN COURSE_HISTORY CH on CH.MBR_ID = MI.MBR_ID AND  DATE_SUB(now(), INTERVAL 1 month)  &lt;&#61; CH.REG_DATE
			  LEFT JOIN CLS_INFO CI ON CH.CLS_SEQ = CI.CLS_SEQ          
          WHERE 1=1
            AND CAMP_SEQ = #{campSeq}
            <if test="classSeq != null and classSeq != '' ">
                AND IRI.CLS_SEQ  = #{classSeq}
            </if>  
            <if test="searchWord != null and searchWord != '' ">
                AND ( MI.NAME LIKE '%${searchWord}%'  OR MI.MBR_ID LIKE '%${searchWord}%' )
            </if>
            ORDER BY MI.NAME ${order}
            
    </select>
    
    
    <resultMap id="campusStudentMap" type="com.visangesl.treeadmin.amg.vo.AssignStudent">
        <id property="mbrId" column="mbrId"/>
        <result property="mbrId" column="mbrId" />
        <result property="mbrNm" column="mbrNm" />
        <result property="nickName" column="nickName" />
        <result property="sexSect" column="sexSect" />
        <result property="campSeq" column="campSeq" />
        <result property="classSeq" column="classSeq" />
        
        <collection property="clsList" ofType="com.visangesl.treeadmin.amg.vo.ClassInfo">
            <id property="classSeq" column="classSeq" />
            <result property="classSeq" column="classSeq" />
            <result property="classNm" column="classNm" />
        </collection>
                
    </resultMap>
        
    
    <!-- 반배정 팝업에서의 클래스 정보 조회  -->
    <select id="getClassDetailInfo" parameterType="com.visangesl.treeadmin.amg.vo.ClassInfoCondition" resultMap="assignListResultMap">
		SELECT
			 CI.CLS_SEQ AS classSeq
			, CI.NAME AS classNm
			, CI.WK_NM AS wkNm
			, CI.START_HM AS startHm
			, CI.END_HM AS endHm
			, MI.NAME AS teacherNm
			, II.MBR_ID AS teacherId
		FROM CLS_INFO CI INNER JOIN ICM_REL_INFO II ON CI.CLS_SEQ = II.CLS_SEQ AND II.MBR_ID IS NOT NULL
		INNER JOIN MBR_INFO MI ON II.MBR_ID = MI.MBR_ID AND MI.MBR_GRADE ='MG210'
		WHERE CI.CLS_SEQ = #{classSeq}
    </select>
    
    <!-- 수업이력 테이블 등록  -->
    <insert id="addCourseHistory" parameterType="com.visangesl.treeadmin.amg.vo.CourseHistory">
        INSERT INTO COURSE_HISTORY (
             MBR_ID
            ,CLS_SEQ
            ,REG_DATE
            ,REG_ID
        ) VALUES (
            #{mbrId}
            ,#{clsSeq}
            , now()
            ,#{regId}
        ) ON DUPLICATE KEY UPDATE MBR_ID =  #{mbrId}, CLS_SEQ = #{clsSeq}
        
    </insert> 
        
    <!-- 회원 등급 업데이트 수강/정 회원  -->
    <update id="updateMbrSect" parameterType="com.visangesl.treeadmin.amg.vo.Student">
        UPDATE MBR_INFO
        SET
               SECT = #{sect}
        WHERE MBR_ID = #{mbrId}
    </update>            
    
    <!-- 수강중인 클래스의 카운트 조회  -->
    <select id="getHaveClassCnt" parameterType="com.visangesl.treeadmin.amg.vo.Student" resultType="int">
              SELECT
                     count(1)
                FROM ICM_REL_INFO
				WHERE MBR_ID = #{mbrId}
				AND CLS_SEQ IS NOT NULL
    </select>
    
    <!-- 캠퍼스 순번 조회  -->
    <select id="getCampuseSeq" parameterType="String" resultType="String">
		SELECT  CAMP_SEQ FROM ICM_REL_INFO
		  WHERE CLS_SEQ = #{clsSeq}
		LIMIT 1
    </select>        
        
    <!-- 클래스 존재 체크  -->    
    <select id="getExistClassCnt" parameterType="String" resultType="int">
        SELECT  COUNT(1) 
          FROM CLS_INFO
         WHERE CLS_SEQ = #{clsSeq}
    </select>        
    
    <!-- 회원 존재 체크  -->
    <select id="getExistMemberCnt" parameterType="String" resultType="int">
        SELECT  COUNT(1) 
          FROM MBR_INFO
         WHERE MBR_ID = #{studentId}
    </select>        
          
    <!-- 기존에 등록된 회원 인지 체크  -->
    <select id="getExistAssignCnt" parameterType="com.visangesl.treeadmin.vo.IcmRelVo" resultType="int">
        SELECT  COUNT(1) 
          FROM ICM_REL
         WHERE 1 = 1
           AND ICM_SEQ  = #{icmSeq}
           AND ICM_SECT = #{icmSect}
           AND REL_SEQ  = #{relSeq}
           AND REL_SECT = #{relSect}
    </select>
    
    <!-- 클래스의 첫번째 레슨 첫번째 차시 코드 조회 (책이 두개인 경우가 있어서 리미트를 걸어서 하나만 조회하도록 함. 두 책의 순서가 없으므로 임시 처리함 ) -->
    <select id="getFirstLessonList" parameterType="com.visangesl.treeadmin.amg.vo.TodayClassCondition" resultType="com.visangesl.treeadmin.amg.vo.TodayClassVo">
		SELECT 
		    DAYNO.DAYNO_CD AS dayNoCd
		FROM CLS_INFO CI 
		INNER JOIN PROG_ITEM PI ON CI.CLS_SEQ = #{clsSeq} AND CI.PROG_SEQ = PI.PROG_SEQ AND CI.USE_YN='Y'
        INNER JOIN PROD_SERVICE LES ON LES.BOOK_CD = PI.ITEM_SEQ AND LES.SEQ = LES.LESSON_CD AND LES.SORT_ORD ='1' AND LES.MBR_ID = CI.STRUC_MBR_ID
		INNER JOIN PROD_SERVICE DAYNO ON DAYNO.BOOK_CD = PI.ITEM_SEQ AND DAYNO.LESSON_CD = LES.LESSON_CD AND DAYNO.SEQ = DAYNO.DAYNO_CD AND DAYNO.SORT_ORD ='1' AND DAYNO.MBR_ID = CI.STRUC_MBR_ID
		LIMIT 1
    </select>
    
    <!-- 현재 차시로 다음 레슨의 첫째 차시 코드를 조회 함. -->
    <select id="getNextLessonDayNoCd" parameterType="com.visangesl.treeadmin.amg.vo.TodayClassCondition" resultType="String">
		SELECT
			  NDAY.DAYNO_CD
		 FROM CLS_INFO CI INNER JOIN PROD_SERVICE DAYNO ON CI.STRUC_MBR_ID = DAYNO.MBR_ID
		INNER JOIN PROD_SERVICE LES ON DAYNO.BOOK_CD = LES.BOOK_CD AND DAYNO.LESSON_CD = LES.LESSON_CD AND LES.SEQ = LES.LESSON_CD AND LES.MBR_ID = CI.STRUC_MBR_ID
		INNER JOIN PROD_SERVICE NLES ON NLES.BOOK_CD = LES.BOOK_CD AND NLES.SEQ = NLES.LESSON_CD AND LES.SORT_ORD+1 = NLES.SORT_ORD AND NLES.MBR_ID = CI.STRUC_MBR_ID
		INNER JOIN PROD_SERVICE NDAY ON NDAY.BOOK_CD = NLES.BOOK_CD AND NDAY.LESSON_CD = NLES.LESSON_CD AND NDAY.SEQ = NDAY.DAYNO_CD AND NDAY.SORT_ORD = '1' AND NDAY.MBR_ID = CI.STRUC_MBR_ID
		WHERE DAYNO.SEQ = #{dayNoCd}
		 AND CI.CLS_SEQ = #{clsSeq}
    </select>
        
    <!-- 패널에서 사용할 클래스의 차시 정보 등록함.-->
    <insert id="addDataTodayClass" parameterType="com.visangesl.treeadmin.amg.vo.TodayClassVo">
        INSERT INTO CLS_INCLS_PROGRESS (
              CLS_SEQ
			, DAYNO_CD
			, SORT_ORD
			, PROGRESS_STAT
        ) VALUES (
              #{clsSeq}
            , #{dayNoCd}
            , #{sortOrd}
            , 'N'
        )
        ON DUPLICATE KEY
		UPDATE
			  CLS_SEQ=#{clsSeq}
			, SORT_ORD=#{sortOrd}
			, PROGRESS_STAT = 'N'
    </insert>
            
    <!-- 클래스의 다음 차시 등록 -->
    <insert id="insertAfterNextDayNoCd" parameterType="com.visangesl.treeadmin.amg.vo.TodayClassCondition">
	    INSERT INTO CLS_INCLS_PROGRESS (
		      CLS_SEQ
		    , DAYNO_CD
		    , SORT_ORD
		    , PROGRESS_STAT
	    )VALUES(
		      #{clsSeq}
		    , #{dayNoCd}
		    , (SELECT MAX(T1.SORT_ORD)+1 FROM CLS_INCLS_PROGRESS T1 WHERE T1.CLS_SEQ = #{clsSeq})
		    , 'N'
	    )
    </insert>
    
    
    <!-- 클래스의 진행된 차시가 있는지 확인  -->
    <select id="getClsProgressCnt" parameterType="String" resultType="int">
    	SELECT COUNT(1)
	      FROM CLS_INCLS_PROGRESS
	     WHERE CLS_SEQ = #{clsSeq}
	       AND PROGRESS_STAT ='Y'    
    </select>
    
    <!-- 클래스에 등록된 차시 정보 삭제 처리  -->
    <delete id="deleteClsProgress" parameterType="String">
        DELETE 
          FROM CLS_INCLS_PROGRESS 
         WHERE CLS_SEQ = #{clsSeq}
    </delete>
    
</mapper>