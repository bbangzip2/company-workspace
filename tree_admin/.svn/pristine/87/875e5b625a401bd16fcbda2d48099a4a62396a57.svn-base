package com.visangesl.treeadmin.lcms.card.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.CardDao;
import com.visangesl.treeadmin.lcms.card.service.CardService;
import com.visangesl.treeadmin.lcms.card.vo.CardContentInfoTempUpdateCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardContentSeqVo;
import com.visangesl.treeadmin.lcms.card.vo.CardDayNoCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardSectCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardStrucCondition;
import com.visangesl.treeadmin.lcms.card.vo.ContentStrucCondition;
import com.visangesl.treeadmin.lcms.packaging.dao.PackagingDao;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingVerCondition;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingVerVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    CardDao cardDao;

    @Autowired
    PackagingDao packagingDao;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public int getTempLessonStrucCount(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getTempLessonStrucCount(vsObject);
    }

    @Override
    public int getLessonStrucCount(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getLessonStrucCount(vsObject);
    }

    @Override
    @Transactional
    public VSResult<VSObject> addTempContent(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        int affectedRows = cardDao.addTempContentInfo(vsObject); // 컨텐츠 정보 등록 - 임시 테이블

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            affectedRows = cardDao.addTempContentStruc(vsObject); // 컨텐츠 구조 등록 - 임시 테이블

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }
        }

        return resultCodeMsg;

    }

    @Override
    public List<VSObject> getTempDayNoList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getTempDayNoList(vsObject);
    }

    @Override
    public List<VSObject> getTemp1DayNoCardList(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getTemp1DayNoCardList(vsObject);
    }

    @Override
    public List<VSObject> getTempSubCardList(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getTempSubCardList(vsObject);
    }

    @Override
    public List<VSObject> getTempDayNoCardList(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getTempDayNoCardList(vsObject);
    }

    @Override
    public VSObject getTempCardInfo(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getTempCardInfo(vsObject);
    }

    @Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(CardStrucCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        String lessonCd = condition.getLessonCd();
        String mbrId = condition.getMbrId();

        ContentStrucCondition contentStrucCondition = new ContentStrucCondition();
        contentStrucCondition.setMbrId(mbrId);

        int affectedRows = 0;


        // 카드 구분값 변경 (엑스트라 카드)
        CardSectCondition cardSectCondition = new CardSectCondition();
        cardSectCondition.setLessonCd(lessonCd);
        cardSectCondition.setCardSect(TreeProperties.getProperty("tree_extra_card"));
        cardSectCondition.setContentSeq(condition.getCardCdList().replaceAll(";", ","));
        cardSectCondition.setMbrId(mbrId);
        affectedRows = cardDao.modifyCardSect(cardSectCondition);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }


        contentStrucCondition.setUpperContentSeq(lessonCd);
        // 기존 (차시-카드) 관계 삭제
        affectedRows = cardDao.deleteContentStrucDayCard(contentStrucCondition);

        // 기존 (레슨-차시) 관계 삭제
        affectedRows = cardDao.deleteContentStrucLessonDay(contentStrucCondition);

        // 기존 차시 정보 미사용으로 변경
        CardContentInfoTempUpdateCondition cardContentInfoTempUpdateCondition = new CardContentInfoTempUpdateCondition();
        cardContentInfoTempUpdateCondition.setUseYn("N");
        cardContentInfoTempUpdateCondition.setType(TreeProperties.getProperty("tree_content_dayno"));
        affectedRows = cardDao.modifyContentInfoTempUseYn(cardContentInfoTempUpdateCondition);


        // 카드맵 데이터를 차시별로 분할
        String[] sortOrd = condition.getSortOrd().split(",");
        String[] cardMapCodeList = condition.getCardCdList().split(";");

        for (int i=0; i<cardMapCodeList.length; i++) {

            String[] temp = sortOrd[i].split(":");
            String sort = temp[0]; // sortOrd
            String dayNoCd = ""; // dayNoCd
            if (temp.length > 1) {
                dayNoCd = temp[1];
            }

            // 차시 코드 체크
            if (dayNoCd == null || dayNoCd.equals("")) { // 신규 차시 (차시 코드가 없으면 신규 추가된 차시)

                // content_info 테이블의 next auto_increment 값을 조회
                String contentInfoNextSeq = cardDao.getContentInfoNextSeq();
                CardContentSeqVo CardContentSeqVo = new CardContentSeqVo();
                CardContentSeqVo.setContentSeq(contentInfoNextSeq);

                // content_info 테이블의 auto_increment 시작값 재지정
                cardDao.modifyContentInfoAutoIncrementVal(CardContentSeqVo);


                // 신규 차시 코드 등록 진행
                CardDayNoCondition cardDayNoCondition = new CardDayNoCondition();
                cardDayNoCondition.setContentSeq(contentInfoNextSeq);
                cardDayNoCondition.setType("PT004");
                cardDayNoCondition.setTitle("DayNO");
                cardDayNoCondition.setMbrId(mbrId);

                affectedRows = cardDao.addDataWithResultCodeMsg(cardDayNoCondition);

                if (affectedRows < 1) {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                } else {
//                    dayNoCd = cardDayNoCondition.getContentSeq(); // 신규 차시 코드
                    dayNoCd = contentInfoNextSeq; // 신규 차시 코드

                    resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));

                }
            } else { // 기존 차시

                // 기존 차시 정보 사용으로 변경
                cardContentInfoTempUpdateCondition = new CardContentInfoTempUpdateCondition();
                cardContentInfoTempUpdateCondition.setUseYn("Y");
                cardContentInfoTempUpdateCondition.setType(TreeProperties.getProperty("tree_content_dayno"));
                cardContentInfoTempUpdateCondition.setContentSeq(dayNoCd);
                affectedRows = cardDao.modifyContentInfoTempUseYn(cardContentInfoTempUpdateCondition);
            }

            // (레슨-차시) 관계 등록
            contentStrucCondition.setUpperContentSeq(lessonCd);
            contentStrucCondition.setContentSeq(dayNoCd);
            contentStrucCondition.setSortOrd(sort);
            affectedRows = cardDao.addContentStrucTemp(contentStrucCondition);

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }

            // 신규 (차시-카드) 관계 등록
            contentStrucCondition.setUpperContentSeq(dayNoCd);
            contentStrucCondition.setContentSeq("");
            String[] cardCodeList = cardMapCodeList[i].split(",");
            for (int j=0; j<cardCodeList.length; j++) {

                // 관계 등록
                contentStrucCondition.setContentSeq(cardCodeList[j]);
                contentStrucCondition.setSortOrd(String.valueOf(j+1));

                affectedRows = cardDao.addContentStrucTemp(contentStrucCondition);

                if (affectedRows < 1) {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                } else {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
                }

                // 카드 구분값 변경 (일반 카드)
                cardSectCondition = new CardSectCondition();
                cardSectCondition.setLessonCd(lessonCd);
                cardSectCondition.setCardSect(TreeProperties.getProperty("tree_default_card"));
                cardSectCondition.setContentSeq(cardCodeList[j]);
                cardSectCondition.setMbrId(mbrId);
                affectedRows = cardDao.modifyCardSect(cardSectCondition);

                if (affectedRows < 1) {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                } else {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
                }
            }



            // 패키지 기존 버전 조회
            PackagingVerCondition packagingVerCondition = new PackagingVerCondition();
            packagingVerCondition.setType("P");
            packagingVerCondition.setContentSeq(lessonCd);
            packagingVerCondition.setMbrId(mbrId);

            PackagingVerVo packagingVerVo = new PackagingVerVo();
            packagingVerVo = (PackagingVerVo) packagingDao.getPackagingVer(packagingVerCondition);

            if (packagingVerVo != null) { // 패키징 여부만 수정
                packagingVerVo.setPkgingYn("N");
                packagingDao.updatePackagingVer(packagingVerVo);

            } else { // 버전 정보 등록
                packagingVerVo = new PackagingVerVo();
                packagingVerVo.setMbrId(mbrId);
                packagingVerVo.setContentSeq(lessonCd);
                packagingVerVo.setType("P");
                packagingVerVo.setVer("0");
                packagingVerVo.setPkgingYn("N");

                packagingDao.insertPackagingVer(packagingVerVo);

            }
/*
            // 패키지 기존 버전 조회 - 카드맵 json
            packagingVerCondition = new PackagingVerCondition();
            packagingVerCondition.setType("C");
            packagingVerCondition.setContentSeq(lessonCd);

            packagingVerVo = new PackagingVerVo();
            packagingVerVo = (PackagingVerVo) packagingDao.getPackagingVer(packagingVerCondition);

            if (packagingVerVo != null) { // 패치징 여부만 수정
                packagingVerVo.setPkgingYn("N");
                packagingDao.updatePackagingVer(packagingVerVo);

            } else { // 버전 등록
                packagingVerVo = new PackagingVerVo();
                packagingVerVo.setMbrId(mbrId);
                packagingVerVo.setContentSeq(lessonCd);
                packagingVerVo.setType("C");
                packagingVerVo.setVer("0");
                packagingVerVo.setPkgingYn("N");
                packagingDao.insertPackagingVer(packagingVerVo);

            }*/


        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        int affectedRows = cardDao.modifyDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> modifyContentInfoTempUseYn(CardContentSeqVo condition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

//        int affectedRows = cardDao.deleteDataWithResultCodeMsg(vsObject);

        // 기존 차시 정보 미사용으로 변경
        CardContentInfoTempUpdateCondition cardContentInfoTempUpdateCondition = new CardContentInfoTempUpdateCondition();
        cardContentInfoTempUpdateCondition.setUseYn("N");
        cardContentInfoTempUpdateCondition.setType(TreeProperties.getProperty("tree_content_card"));
        cardContentInfoTempUpdateCondition.setContentSeq(condition.getContentSeq());
        int affectedRows = cardDao.modifyContentInfoTempUseYn(cardContentInfoTempUpdateCondition);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {

            ContentStrucCondition strucCondition = new ContentStrucCondition();
            strucCondition.setContentSeq(condition.getContentSeq());
            affectedRows = cardDao.deleteContentStrucTemp(strucCondition);

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> deleteDataWithResultCodeMsg(CardContentSeqVo condition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

//        int affectedRows = cardDao.deleteDataWithResultCodeMsg(vsObject);

        // 기존 차시 정보 미사용으로 변경
        CardContentInfoTempUpdateCondition cardContentInfoTempUpdateCondition = new CardContentInfoTempUpdateCondition();
        cardContentInfoTempUpdateCondition.setUseYn("N");
        cardContentInfoTempUpdateCondition.setType(TreeProperties.getProperty("tree_content_card"));
        cardContentInfoTempUpdateCondition.setContentSeq(condition.getContentSeq());
        int affectedRows = cardDao.modifyContentInfoTempUseYn(cardContentInfoTempUpdateCondition);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {

            affectedRows = cardDao.deleteContentStruc(condition);

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> deleteContentTempData(ContentStrucCondition contentStrucCondition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();


      // 임시 컨텐츠 테이블 삭제 진행
      int affectedRows = cardDao.deleteContentInfoTempService(contentStrucCondition); // 컨텐츠 정보

      if (affectedRows < 1) {
          resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
      } else {

          List<ContentStrucCondition> contentStrucTempList = cardDao.getContentStrucTempList(contentStrucCondition); // 컨텐츠 구조 조회

          for (ContentStrucCondition condition : contentStrucTempList) {
              // 컨텐츠 구조 삭제
              cardDao.deleteContentStrucTempStrucSeq(condition);
          }
      }

        return resultCodeMsg;
    }




/*


    @Override
    public VSObject getData(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getData(vsObject);
    }

    @Override
    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getDataList(vsObject);
    }

    @Override
    public int getDataListCnt(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getDataListCnt(vsObject);
    }

    @Override
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        int affectedRows = cardDao.modifyDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(CardStrucCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        String lessonCd = condition.getLessonCd();
        String mbrId = condition.getMbrId();

        logger.debug(mbrId);
        logger.debug("mbrId: " + mbrId);


        ContentStrucCondition contentStrucCondition = new ContentStrucCondition();
        contentStrucCondition.setMbrId(mbrId);

        int affectedRows = 0;


        // 카드 구분값 변경 (엑스트라 카드)
        CardVo cardVo = new CardVo();
        cardVo.setLessonCd(lessonCd);
        cardVo.setCardSect("MT003");
        cardVo.setContentSeq(condition.getCardCdList().replaceAll(";", ","));
        cardVo.setMbrId(mbrId);
        affectedRows = cardDao.modifyCardSect(cardVo);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }


        contentStrucCondition.setUpperContentSeq(lessonCd);
        // 기존 (차시-카드) 관계 삭제
        affectedRows = cardDao.deleteContentStrucDayCard(contentStrucCondition);

        // 기존 (레슨-차시) 관계 삭제
        affectedRows = cardDao.deleteContentStrucLessonDay(contentStrucCondition);


        // 카드맵 데이터를 차시별로 분할
        String[] sortOrd = condition.getSortOrd().split(",");
        String[] cardMapCodeList = condition.getCardCdList().split(";");

        for (int i=0; i<cardMapCodeList.length; i++) {

            String[] temp = sortOrd[i].split(":");
            String sort = temp[0]; // sortOrd
            String dayNoCd = ""; // dayNoCd
            if (temp.length > 1) {
                dayNoCd = temp[1];
            }

            // 차시 코드 체크
            if (dayNoCd == null || dayNoCd.equals("")) { // 신규 차시 (차시 코드가 없으면 신규 추가된 차시)

                // 신규 차시 코드 등록 진행
                cardVo = new CardVo();
                cardVo.setType("PT004");
                cardVo.setTitle("Day" + sort);
                cardVo.setMbrId(mbrId);

                affectedRows = cardDao.addDataWithResultCodeMsg(cardVo);
                dayNoCd = cardVo.getContentSeq(); // 신규 차시 코드

            }

            // (레슨-차시) 관계 등록
            contentStrucCondition.setUpperContentSeq(lessonCd);
            contentStrucCondition.setContentSeq(dayNoCd);
            contentStrucCondition.setSortOrd(sort);
            affectedRows = cardDao.addContentStruc(contentStrucCondition);


            // 신규 (차시-카드) 관계 등록
            contentStrucCondition.setUpperContentSeq(dayNoCd);
            contentStrucCondition.setContentSeq("");
            String[] cardCodeList = cardMapCodeList[i].split(",");
            for (int j=0; j<cardCodeList.length; j++) {

                // 관계 등록
                contentStrucCondition.setContentSeq(cardCodeList[j]);
                contentStrucCondition.setSortOrd(String.valueOf(j+1));

                affectedRows = cardDao.addContentStruc(contentStrucCondition);


                // 카드 구분값 변경 (일반 카드)
                cardVo = new CardVo();
                cardVo.setCardSect("MT001");
                cardVo.setContentSeq(cardCodeList[j]);
                cardVo.setMbrId(mbrId);
                cardVo.setModId(mbrId);
                cardDao.modifyDataWithResultCodeMsg(cardVo);

            }


            // 패키지 기존 버전 조회
            PackagingVerCondition packagingVerCondition = new PackagingVerCondition();
            packagingVerCondition.setType("P");
            packagingVerCondition.setContentSeq(lessonCd);

            PackagingVerVo packagingVerVo = new PackagingVerVo();
            packagingVerVo = (PackagingVerVo) packagingDao.getPackagingVer(packagingVerCondition);

            if (packagingVerVo != null) { // 패치징 여부만 수정
                packagingVerVo.setPkgingYn("N");
                packagingDao.updatePackagingVer(packagingVerVo);

            } else { // 버전 정보 등록
                packagingVerVo = new PackagingVerVo();
                packagingVerVo.setMbrId(mbrId);
                packagingVerVo.setContentSeq(lessonCd);
                packagingVerVo.setType("P");
                packagingVerVo.setVer("0");
                packagingVerVo.setPkgingYn("N");
                packagingDao.insertPackagingVer(packagingVerVo);

            }

            // 패키지 기존 버전 조회 - 카드맵 json
            packagingVerCondition = new PackagingVerCondition();
            packagingVerCondition.setType("C");
            packagingVerCondition.setContentSeq(lessonCd);

            packagingVerVo = new PackagingVerVo();
            packagingVerVo = (PackagingVerVo) packagingDao.getPackagingVer(packagingVerCondition);

            if (packagingVerVo != null) { // 패치징 여부만 수정
                packagingVerVo.setPkgingYn("N");
                packagingDao.updatePackagingVer(packagingVerVo);

            } else { // 버전 등록
                packagingVerVo = new PackagingVerVo();
                packagingVerVo.setMbrId(mbrId);
                packagingVerVo.setContentSeq(lessonCd);
                packagingVerVo.setType("C");
                packagingVerVo.setVer("0");
                packagingVerVo.setPkgingYn("N");
                packagingDao.insertPackagingVer(packagingVerVo);

            }

        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> deleteDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        int affectedRows = cardDao.deleteDataWithResultCodeMsg(vsObject);

        affectedRows = cardDao.deleteContentStruc(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    public List<VSObject> getDayNoList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getDayNoList(vsObject);
    }

    @Override
    @Transactional
    public VSResult<VSObject> addEditCard(CardVo condition)
            throws Exception {
        // TODO Auto-generated method stub

        VSResult resultCodeMsg = new VSResult();

        // 에디트 카드 등록
        int affectedRows = cardDao.addEditCard(condition);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {

            // 에디트 카드 구조 등록
            ContentStrucCondition contentStrucCondition = new ContentStrucCondition();
            contentStrucCondition.setUpperContentSeq(condition.getLessonCd());
            contentStrucCondition.setContentSeq(condition.getContentSeq());
            contentStrucCondition.setMbrId(condition.getMbrId());
            contentStrucCondition.setSortOrd("1");
            affectedRows = cardDao.addContentStruc(contentStrucCondition);

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }
        }

        return resultCodeMsg;

    }

    @Override
    public int modifyEditCard(CardVo condition)
            throws Exception {
        // TODO Auto-generated method stub
        return cardDao.modifyDataWithResultCodeMsg(condition);
    }

    @Override
    public List<VSObject> getSubCardList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getSubCardList(vsObject);
    }

    @Override
    public List<VSObject> getCardList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getCardList(vsObject);
    }

    @Override
    public VSObject getCardInfoList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return cardDao.getCardInfoList(vsObject);
    }




*/


}
