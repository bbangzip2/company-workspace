package com.visangesl.treeadmin.lcms.card.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.lcms.card.service.CardService;
import com.visangesl.treeadmin.lcms.card.service.EditCardService;
import com.visangesl.treeadmin.lcms.card.vo.CardContentSeqVo;
import com.visangesl.treeadmin.lcms.card.vo.CardMapModelCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardStrucCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardTempCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardVo;
import com.visangesl.treeadmin.lcms.card.vo.ContentStrucCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class CardController implements BaseController {

    @Autowired
    private CardService cardService;

    @Autowired
    private EditCardService editCardService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * getCardMapList
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/card/cardMapList")
    public String getCardMapList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "lessonTitle", required = false, defaultValue = "") String lessonTitle,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId
            ) throws Exception {

        logger.debug("getCardMapList");
        logger.debug("lessonCd: " + lessonCd);
        logger.debug("lessonTitle: " + lessonTitle);
        logger.debug("mbrId: " + mbrId);

        VSResult result = new VSResult();

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        mbrId = userDetail.getUsername();

        List<VSObject> list = null;

        try {

            CardTempCondition cardTempCondition = new CardTempCondition();
            cardTempCondition.setLessonCd(lessonCd);

            // 기존 저장된 임시 카드맵 정보가 있는지 조회
            int count = cardService.getTempLessonStrucCount(cardTempCondition);

            if (count == 0) {

                // 기존 저장된 카드맵 정보 있는지 조회
                count = cardService.getLessonStrucCount(cardTempCondition);

                if (count > 0) { // 카드맵 정보 임시 테이블로 복사 진행
                    result = cardService.addTempContent(cardTempCondition); // 임시 테이블 컨텐츠 구조 등록
                }

            }

            // 카드맵 리스트 정보 조회 시작!

            // 차시 리스트 조회
            list = cardService.getTempDayNoList(cardTempCondition);
            model.addAttribute("dayNoList", list);


            // 카드맵 리스트 조회 - 1차시
            list = cardService.getTemp1DayNoCardList(cardTempCondition);
            model.addAttribute("cardMapList", list);


            // 서브 카드맵 리스트 조회
            list = cardService.getTempSubCardList(cardTempCondition);
            model.addAttribute("cardMapSubList", list);//VVV


            // 레슨 차시별 카드 리스트 조회
            list = cardService.getTempDayNoCardList(cardTempCondition);

            // 레슨 차시별 카드 리스트 조회 결과 JSON 변환
            ObjectMapper om = new ObjectMapper();
//            om.writeValueAsString(list);
//            JSONArray jsonArray = JSONArray.fromObject(list);
            model.addAttribute("cardList", om.writeValueAsString(list));


            // 모델에 레슨 정보 저장
            CardMapModelCondition modelCondition = new CardMapModelCondition();
            modelCondition.setLessonCd(lessonCd);
            modelCondition.setMbrId(mbrId);
            modelCondition.setLessonTitle(lessonTitle);
            model.addAttribute("condition", modelCondition);

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return "/lcms/card/cardList";
    }


    /**
     * getCardList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/card/cardList")
    public VSResult getCardList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "contentSeq", required = false, defaultValue = "") String contentSeq,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId
            ) throws Exception {

        logger.debug("getCardList");
        logger.debug("contentSeq: " + contentSeq);
        logger.debug("mbrId: " + mbrId);

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        mbrId = userDetail.getUsername();

        VSResult result = new VSResult();

        try {

            CardContentSeqVo condition = new CardContentSeqVo();

            String[] temp = contentSeq.split(",");
            List<VSObject> list = new ArrayList<VSObject>();
            for (int i=0; i<temp.length; i++) {
                // 카드맵 리스트 조회
                condition.setContentSeq(temp[i]);
                list.add(cardService.getTempCardInfo(condition));
            }

            model.addAttribute("condition", condition);

            result.setResult(list);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return result;
    }


    /**
     * getSubCardList
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/card/cardSubList")
    public VSResult getSubCardList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "dayNoCd", required = false, defaultValue = "") String dayNoCd,
            @RequestParam(value = "cardSect", required = false, defaultValue = "") String cardSect,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId
            ) throws Exception {

        logger.debug("getSubCardList");
        logger.debug("lessonCd: " + lessonCd);
        logger.debug("dayNoCd: " + dayNoCd);
        logger.debug("mbrId: " + mbrId);

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        mbrId = userDetail.getUsername();

        VSResult result = new VSResult();

        try {

            CardTempCondition cardTempCondition = new CardTempCondition();
            cardTempCondition.setLessonCd(lessonCd);

            model.addAttribute("condition", cardTempCondition);

            // 카드맵 리스트 조회
            List<VSObject> list = cardService.getTempSubCardList(cardTempCondition);

            result.setResult(list);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return result;
    }


    /**
     * addCardMap
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/card/cardMapInsert")
    public VSResult addCardMap(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "pSortOrd", required = false, defaultValue = "") String sortOrd,
            @RequestParam(value = "pCardCdList", required = false, defaultValue = "") String cardCdList
            ) throws Exception {

        logger.debug("addCardMap");
        logger.debug("lessonCd: " + lessonCd);
        logger.debug("mbrId: " + mbrId);
        logger.debug("sortOrd: " + sortOrd);
        logger.debug("cardCdList: " + cardCdList);

        VSResult result = new VSResult();

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        mbrId = userDetail.getUsername();

        try {

            CardStrucCondition condition = new CardStrucCondition();
            condition.setLessonCd(lessonCd);
            condition.setSortOrd(sortOrd);
            condition.setCardCdList(cardCdList);
            condition.setMbrId(mbrId);

            // 카드맵 등록
            cardService.addDataWithResultCodeMsg(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return result;
    }


    /**
     * getCardInfo
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/card/cardInfo")
    public VSResult getCardInfo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "contentSeq", required = false, defaultValue = "") String contentSeq
            ) throws Exception {

        logger.debug("getCardInfo");

        VSResult result = new VSResult();

        try {

            CardContentSeqVo condition = new CardContentSeqVo();
            condition.setContentSeq(contentSeq);

            // 카드 상세 정보 조회
            CardVo cardVo = (CardVo) cardService.getTempCardInfo(condition);

            result.setResult(cardVo);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return result;
    }


    /**
     * modifyCardInfo
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/card/cardModify")
    public String modifyCardInfo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "contentSeq", required = false, defaultValue = "") String contentSeq
            ) throws Exception {

        logger.debug("modifyCardInfo");

        VSResult result = new VSResult();

        try {

            CardContentSeqVo condition = new CardContentSeqVo();
            condition.setContentSeq(contentSeq);

            // 카드 정보 수정
            result = cardService.modifyDataWithResultCodeMsg(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return "/lcms/card/cardList";
    }


    /**
     * deleteCardInfo
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/card/cardDelete")
    public VSResult deleteCardInfo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "contentSeq", required = false, defaultValue = "") String contentSeq
            ) throws Exception {

        logger.debug("deleteCardInfo");
        logger.debug("contentSeq: " + contentSeq);

        VSResult result = new VSResult();

        try {

            CardContentSeqVo condition = new CardContentSeqVo();
            condition.setContentSeq(contentSeq);

            // 카드 정보 삭제
            cardService.modifyContentInfoTempUseYn(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return result;
    }


    /**
     * deleteCardInfo
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/card/deleteContentTempData")
    public VSResult deleteContentTempData(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd
            ) throws Exception {

        logger.debug("deleteCardInfo");
        logger.debug("lessonCd: " + lessonCd);

        VSResult result = new VSResult();

        try {

            ContentStrucCondition contentStrucCondition = new ContentStrucCondition();
            contentStrucCondition.setLessonCd(lessonCd);

            // 임시 저장 정보 삭제
            cardService.deleteContentTempData(contentStrucCondition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }
//

//
        return result;
    }



}
