package com.visangesl.treeadmin.mail.service.impl;

import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.visangesl.treeadmin.mail.service.MailService;
import com.visangesl.treeadmin.mail.vo.MailPasswordVo;
import com.visangesl.treeadmin.member.dao.MemberDao;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    MemberDao memberDao;

    @Autowired
    @Resource(name = "velocityEngine")
    private VelocityEngine velocityEngine;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public String getMailTemplate(Member member) throws Exception {
        // TODO Auto-generated method stub

        // mail template data setting
        StringWriter writer = new StringWriter();
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("siteUrl", "");
        map.put("sendDate", TreeAdminUtil.getDateFormat("yyyy.MM.dd"));
        map.put("memberName"    , member.getName());
        map.put("memberId"      , member.getMbrId());
        map.put("memberTempPwd" , member.getPwd()); // 임의 문자열(10자리)를 임시 패스워드로 전송
        map.put("memberEMail"   , member.getEmail());
        String mailTemplateMemeber = TreeProperties.getProperty("tree_mail_template_member");

        // mail template
        VelocityEngineUtils.mergeTemplate(velocityEngine, mailTemplateMemeber, map, writer);


        return writer.toString();

    }


    @Override
    public VSResult sendMail(MailPasswordVo mailPasswordVo) throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        // mail properties
        final String mailUserName = TreeProperties.getProperty("tree_mail_user_name");
        final String mailUserPwd = TreeProperties.getProperty("tree_mail_user_pwd");

        String mailProtocol = TreeProperties.getProperty("tree_mail_protocol");
        String mailType = TreeProperties.getProperty("tree_mail_type");
        String mailHost = TreeProperties.getProperty("tree_mail_host");
        String mailPort = TreeProperties.getProperty("tree_mail_port");
        String mailAddr = mailPasswordVo.getFromAddr();
        String mailName = mailPasswordVo.getFromName();


        // mail type
        String mailSect = mailPasswordVo.getMailSect();
        String mailAuth = mailPasswordVo.getMailAuthYn();

        // mail content
        String subject = mailPasswordVo.getSubject();
        String content = mailPasswordVo.getContent();


        // mail sect check
        if (mailSect != null && mailSect.equals("PASSWORD")) {

            // member pwd change
            Member member = new Member();
            member.setMbrId(mailPasswordVo.getMbrId());
            member.setPwd(mailPasswordVo.getPwd());

            memberDao.modifyDataWithResultCodeMsg(member);

        } else {

        }


        // Properties set
        Properties props = new Properties();
        props.put("mail.transport.protocol", mailProtocol);
        props.put("mail.smtp.host", mailHost);
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.port", mailPort);


        // auth type check & session set

        // =================================================================
        // 인증 여부, 인증 대상에 따라 Properties 변경!!!
        // =================================================================
        Session session = null;
        if (mailAuth.equals("Y")) {

            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.EnableSSL.enable","true");
            props.setProperty("mail.smtp.port", mailPort);
            props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.socketFactory.port", mailPort);

            session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailUserName, mailUserPwd);
                }
            });
        } else {
            session = Session.getDefaultInstance(props, null); // 비 인증
        }
        session.setDebug(true); //for debug


        // MimeMessage
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(mailAddr, mailName));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailPasswordVo.getToAddr(), mailPasswordVo.getToName()));
        message.setSubject(subject);
        message.setContent(content, mailType);
        message.setSentDate(new Date());


        // mail send
        Transport.send(message);


        resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        resultCodeMsg.setMessage(TreeProperties.getProperty("error.success.msg"));

        return resultCodeMsg;

    }



}
