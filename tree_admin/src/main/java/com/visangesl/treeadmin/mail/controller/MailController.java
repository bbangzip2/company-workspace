package com.visangesl.treeadmin.mail.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.mail.service.MailService;
import com.visangesl.treeadmin.mail.vo.MailPasswordVo;
import com.visangesl.treeadmin.member.service.MemberService;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class MailController implements BaseController {

    @Autowired
    private MailService mailService;
    @Autowired
    private MemberService memberService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * sendUserPwdMail
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/mail/sendUserPwdMail")
    public VSResult sendUserPwdMail(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId
            ) throws Exception {

        logger.debug("sendUserPwdMail");
        logger.debug("mbrId: " + mbrId);

        VSResult result = new VSResult();

        try {

            // 임의 문자열(10자리)를 임시 패스워드로 전송
            String tempPwd = TreeAdminUtil.getRandomString(10);

            // get member info
            Member member = new Member();
            member.setMbrId(mbrId);
            member = (Member) memberService.getData(member);

            // 회원 정보 조회 결과 체크
            if (member != null && member.getEmail() != null && !member.getEmail().equals("")) {
                member.setPwd(tempPwd);

                // get mail template
                String content = mailService.getMailTemplate(member);


                // send mail
                MailPasswordVo mailVo = new MailPasswordVo();
                mailVo.setMbrId(mbrId);
                mailVo.setToAddr(member.getEmail());
                mailVo.setToName(member.getName());
                mailVo.setFromAddr(TreeProperties.getProperty("tree_mail_template_member"));
                mailVo.setFromName(TreeProperties.getProperty("tree_mail_template_member"));
                mailVo.setSubject("Treebooster Password Mail");
                mailVo.setContent(content);
                mailVo.setPwd(tempPwd);
                mailVo.setMailSect("PASSWORD");
                mailVo.setMailAuthYn("N");

                result = mailService.sendMail(mailVo);

            } else {
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage("존재하지 않는 ID 입니다.");
            }


        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();

            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage("발송 실패");
        }

        return result;
    }
}
