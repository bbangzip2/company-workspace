package com.visangesl.treeadmin.mail.service;

import org.springframework.stereotype.Service;

import com.visangesl.treeadmin.mail.vo.MailPasswordVo;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface MailService {

    public String getMailTemplate(Member member) throws Exception;

    // send Mail
    public VSResult sendMail(MailPasswordVo mailPasswordVo) throws Exception;


}
