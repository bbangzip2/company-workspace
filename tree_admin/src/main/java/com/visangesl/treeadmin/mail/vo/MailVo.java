package com.visangesl.treeadmin.mail.vo;

import com.visangesl.tree.vo.VSObject;

public class MailVo extends VSObject {

    private String fromAddr;
    private String fromName;
    private String toAddr;
    private String toName;
    private String subject;
    private String content;

    private String mailSect;
    private String mailAuthYn;
    private String mbrId;

    public String getFromAddr() {
        return fromAddr;
    }
    public void setFromAddr(String fromAddr) {
        this.fromAddr = fromAddr;
    }
    public String getFromName() {
        return fromName;
    }
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
    public String getToAddr() {
        return toAddr;
    }
    public void setToAddr(String toAddr) {
        this.toAddr = toAddr;
    }
    public String getToName() {
        return toName;
    }
    public void setToName(String toName) {
        this.toName = toName;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getMailSect() {
        return mailSect;
    }
    public void setMailSect(String mailSect) {
        this.mailSect = mailSect;
    }
    public String getMailAuthYn() {
        return mailAuthYn;
    }
    public void setMailAuthYn(String mailAuthYn) {
        this.mailAuthYn = mailAuthYn;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }



}
