package com.visangesl.treeadmin.main.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
	private final Log logger = LogFactory.getLog(this.getClass());

	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String mainInfo(HttpServletRequest request, Model model) throws Exception {

		logger.debug("---------------------------------------------------------------------------");
		String redirectURL = "main";

		return redirectURL;
	}

}
