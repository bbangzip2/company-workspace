package com.visangesl.treeadmin.file.helper;

import java.io.File;
import java.io.FileOutputStream;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.treeadmin.file.helper.vo.FileUploadResult;
import com.visangesl.treeadmin.util.FileRenamePolicy;

/**
 * File Handling Helper Class
 * @author imac
 *
 */
public class TreeFileUtils {	
	/**
	 * Upload될 File의 크기를 check하여 초과되었는지 여부를 boolean으로 return한다.
	 * 
	 * @param uploadedFile MultipartFile객체
	 * @param size 최대 허용 파일 크기 (byte단위)
	 * @return
	 */
	public static boolean availableUploadSize(MultipartFile uploadedFile, long size) {
		boolean isAvailable = false;
		
		if (uploadedFile != null && uploadedFile.getSize() > 0 && uploadedFile.getSize() <= size) {
			isAvailable = true;
		}
		
		return isAvailable;
	}
	
	/**
	 * File Upload 처리 Helper Method
	 *
	 * @param uploadedFile MultipartFile 객체
	 * @param realFilePath 서버상에 저장될 경로
	 * @return FileUploadResult upload 처리 결과 정보
	 * @throws Exception
	 */
	public static FileUploadResult uploadFile(MultipartFile uploadedFile, String realFilePath) throws Exception {
		FileUploadResult result = new FileUploadResult();
		result.setUploaded(false);

		System.out.println("realFilePath : " + realFilePath);
		if (realFilePath == null) return result;

		if (!(new File(realFilePath)).exists()) {
	        System.out.println("MKDIRRRRRRRR");
			(new File(realFilePath)).mkdirs();
		}

		result.setOrgFileNm(uploadedFile.getOriginalFilename());

        System.out.println("getOrgFileNm : " + result.getOrgFileNm());
		if (result.getOrgFileNm() == null || result.getOrgFileNm().equals("")) {
			return result;
		}

		String fileSeperator = System.getProperty("file.separator");

		File realFile = new File(realFilePath + fileSeperator + result.getOrgFileNm());
        System.out.println("realFile+orgFileNm : " + realFile.getName());
		realFile = new FileRenamePolicy().rename(realFile);

        System.out.println("realFile rename : " + realFile.getName());

		if (realFile.getName() != null && uploadedFile.getSize() > 0) {
            System.out.println("FILE UPLOADEDEDEDEDEEDEDEDED");

			FileOutputStream fos = new FileOutputStream(realFile);
			FileCopyUtils.copy(uploadedFile.getInputStream(), fos);
			fos.close();

			result.setUploaded(true);
		}

		if (result.isUploaded()) {
			result.setRealFileNm(realFile.getName());
			result.setRealFilePath(realFilePath);
			result.setFileSize(Float.valueOf(String.format("%.1f" , Float.valueOf(uploadedFile.getSize())/1024/1024))); // 소수점 1자리까지 표현
		}

		return result;
	}

	/**
	 * 서버상의 File 삭제처리 Helper Method
	 *
	 * @param filePath 파일이 저장된 서버상의 경로
	 * @param fileName 삭제할 파일명
	 * @return boolean 처리 결과 여부
	 */
	public static boolean removeFile(String filePath, String fileName) {
		boolean isRemoved = false;

		if (filePath == null || fileName == null) return isRemoved;

		String fileSeperator = System.getProperty("file.separator");

		File file = new File(filePath + fileSeperator + fileName);
		if (file.exists()) {
			file.delete();
			isRemoved = true;
		}

		return isRemoved;
	}
}
