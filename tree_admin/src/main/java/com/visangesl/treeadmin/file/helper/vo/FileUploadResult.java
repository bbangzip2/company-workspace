package com.visangesl.treeadmin.file.helper.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * File Upload 처리에 대한 결과를 담는 VO
 * @author imac
 *
 */
public class FileUploadResult extends VSObject {
	/**
	 * upload 처리 성공 여부
	 */
	private boolean isUploaded;
	/**
	 * 원본 파일명
	 */
	private String orgFileNm;
	/**
	 * 실제 서버에 저장된 파일명
	 */
	private String realFileNm;
	/**
	 * 원본 파일 크기(MB)
	 */
	private float fileSize;
	
	/**
	 * 실제 서버에 저장된 경로
	 */
	private String realFilePath;
	
	public boolean isUploaded() {
		return isUploaded;
	}
	public void setUploaded(boolean isUploaded) {
		this.isUploaded = isUploaded;
	}
	public String getOrgFileNm() {
		return orgFileNm;
	}
	public void setOrgFileNm(String orgFileNm) {
		this.orgFileNm = orgFileNm;
	}
	public String getRealFileNm() {
		return realFileNm;
	}
	public void setRealFileNm(String realFileNm) {
		this.realFileNm = realFileNm;
	}

	public float getFileSize() {
		return fileSize;
	}
	public void setFileSize(float fileSize) {
		this.fileSize = fileSize;
	}
	public String getRealFilePath() {
		return realFilePath;
	}
	public void setRealFilePath(String realFilePath) {
		this.realFilePath = realFilePath;
	}
}
