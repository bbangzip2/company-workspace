package com.visangesl.treeadmin.lcms.card.vo;

import com.visangesl.tree.vo.VSObject;

public class CardTempCondition extends VSObject {

    private String lessonCd;
    private String mbrId;

    public String getLessonCd() {
        return lessonCd;
    }

    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
}
