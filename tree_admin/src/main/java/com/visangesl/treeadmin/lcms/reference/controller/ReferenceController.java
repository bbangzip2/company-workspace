package com.visangesl.treeadmin.lcms.reference.controller;

import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.lcms.card.service.CardService;
import com.visangesl.treeadmin.lcms.card.vo.CardTempCondition;
import com.visangesl.treeadmin.lcms.reference.service.ReferenceService;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceCondition;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class ReferenceController implements BaseController {

    @Autowired
    private ReferenceService referenceService;

    @Autowired
    private CardService cardService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * getReferenceList
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/reference/referenceList")
    public String getReferenceList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "lessonTitle", required = false, defaultValue = "") String lessonTitle,
            @RequestParam(value = "sortKey", required = false, defaultValue = "REG_DATE") String sortKey,
            @RequestParam(value = "sort", required = false, defaultValue = "DESC") String sort
            ) throws Exception {

        logger.debug("getReferenceList");
        logger.debug("lessonCd: " + lessonCd);
        logger.debug("lessonTitle: " + lessonTitle);
        logger.debug("sortKey: " + sortKey);
        logger.debug("sort: " + sort);

        VSResult result = new VSResult();

        try {

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 컨텐츠 정보
            CardTempCondition cardTempCondition = new CardTempCondition();
            cardTempCondition.setLessonCd(lessonCd);

            // 기존 저장된 임시 카드맵 정보가 있는지 조회
            int count = cardService.getTempLessonStrucCount(cardTempCondition);

            if (count == 0) {

                // 기존 저장된 카드맵 정보 있는지 조회
                count = cardService.getLessonStrucCount(cardTempCondition);

                if (count > 0) { // 카드맵 정보 임시 테이블로 복사 진행
                    result = cardService.addTempContent(cardTempCondition); // 임시 테이블 컨텐츠 구조 등록
                }
            }
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


            ReferenceCondition condition = new ReferenceCondition();
            condition.setLessonCd(lessonCd);

            List<ReferenceVo> referenceTempList = referenceService.getReferenceTempList(condition);

            count = referenceTempList.size();
            if (count > 0) {
                // 삭제된 차시의 레퍼런스 USE_YN 수정
                referenceService.updateReferenceFileUseYn(referenceTempList);
            }

            condition.setLessonTitle(lessonTitle);
            condition.setListSortKey(sortKey);
            condition.setListSort(sort);

            List<VSObject> referenceList = referenceService.getDataList(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

            model.addAttribute("condition", condition);
            model.addAttribute("referenceList", referenceList);

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }

        return "/lcms/reference/referenceList";
    }


    /**
     * getReferenceInfo
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/reference/referenceInfo")
    public String getReferenceInfo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonTitle", required = false, defaultValue = "") String lessonTitle,
            @RequestParam(value = "referenceSeq", required = false, defaultValue = "") String referenceSeq
            ) throws Exception {

        logger.debug("getReferenceInfo");
        logger.debug("lessonTitle:" + lessonTitle);
        logger.debug("referenceSeq:" + referenceSeq);

        VSResult result = new VSResult();
        ReferenceVo referenceVo = new ReferenceVo();
        ReferenceCondition referenceCondition = new ReferenceCondition();

        if (!referenceSeq.equals("")) {

            try {
                referenceCondition.setLessonTitle(lessonTitle);
                referenceCondition.setReferenceSeq(referenceSeq);

                referenceVo = (ReferenceVo) referenceService.getData(referenceCondition);

                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } catch (Exception e) {
                result.setCode("9999");
                result.setMessage("조회 오류");
                e.printStackTrace();
            }
        } else {
            result.setCode("1000");
            result.setMessage("필수 파라미터");
        }

        model.addAttribute("condition", referenceCondition);
        model.addAttribute("referenceInfo", referenceVo);

        return "/lcms/reference/referenceInfo";
    }


    /**
     * insertReference
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/reference/referenceInsert")
    public String insertReference(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false) String lessonCd,
            @RequestParam(value = "lessonTitle", required = false, defaultValue = "") String lessonTitle
            ) throws Exception {

        logger.debug("insertReference");
        logger.debug("lessonCd:" + lessonCd);
        logger.debug("lessonTitle:" + lessonTitle);

        VSResult result = new VSResult();

        try {

            List<VSObject> dayNoCdList = referenceService.getDayNoCdList(lessonCd);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

            ReferenceCondition condition = new ReferenceCondition();
            condition.setLessonCd(lessonCd);
            condition.setLessonTitle(lessonTitle);
            model.addAttribute("condition", condition);
            model.addAttribute("dayNoCdList", dayNoCdList);

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
            e.printStackTrace();
        }


        return "/lcms/reference/referenceEdit";
    }


    /**
     * addReference
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/reference/referenceAdd", method = RequestMethod.POST)
    public VSResult addReference(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false) String lessonCd,
            @RequestParam(value = "lessonTitle", required = false, defaultValue = "") String lessonTitle,
            @RequestParam(value = "dayNoCd", required = false) String dayNoCd,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "url", required = false) String url,
            @RequestParam(value = "text", required = false) String text
            ) throws Exception {

        logger.debug("addReference");
        logger.debug(lessonCd);
        logger.debug(lessonTitle);
        logger.debug(dayNoCd);
        logger.debug(type);
        logger.debug(url);
        logger.debug(text);

        VSResult result = new VSResult();

        if (!dayNoCd.equals("") && !type.equals("")) {

            try {
                ReferenceVo referenceVo = new ReferenceVo();
                referenceVo.setMbrId("admin");
                referenceVo.setDayNoCd(dayNoCd);

                // 기존 레퍼런스의 등록 카운트 조회
                int refTotalCount = referenceService.getDataListCnt(referenceVo);

                if (refTotalCount < 20) {
                    // 등록하려는 레퍼런스 타입의 기존 카운트 조회
                    referenceVo.setType(type);
                    int refTypeCount = referenceService.getDataListCnt(referenceVo);

                    if (refTypeCount < 5) {

                        if (type.equals(TreeProperties.getProperty("tree_file_text"))) {
                            referenceVo.setContent(text);
                        } else if (type.equals(TreeProperties.getProperty("tree_file_url"))) {
                            referenceVo.setContent(url);
                        } else {
                            // movie/image 파일 처리
                            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
                            referenceVo.setUploadFile(multipartRequest.getFiles("uploadFile"));
                        }

                        result = referenceService.addDataWithResultCodeMsg(referenceVo);
                        logger.debug(result.getCode());
                        logger.debug(result.getMessage());

                        ReferenceCondition referenceCondition = new ReferenceCondition();
                        referenceCondition.setDayNoCd(dayNoCd);
                        model.addAttribute("condition", referenceCondition);

                        result.setCode(TreeProperties.getProperty("error.success.code"));
                        result.setMessage(TreeProperties.getProperty("error.success.msg"));

                    } else {
                        result.setCode("1002");
                        result.setMessage("Reference 등록 제한 개수를 초과하였습니다. " + refTypeCount);
                    }
                } else {
                    result.setCode("1001");
                    result.setMessage("Reference 등록 제한 개수를 초과하였습니다. " + refTotalCount);
                }
            } catch (Exception e) {
                if (e.equals("same")) {
                    result.setCode("1003");
                    result.setMessage("동일 파일명");
                } else {
                    result.setCode("9999");
                    result.setMessage("등록 오류");
                }
                e.printStackTrace();
            }
        } else {
            result.setCode("1000");
            result.setMessage("필수 파라미터");
        }

//        return "redirect:/lcms/reference/referenceList.do?lessonCd=" + lessonCd + "&lessonTitle=" + URLEncoder.encode(lessonTitle);
        return result;
    }


    /**
     * deleteReference
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/reference/referenceDelete")
    public String deleteReference(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false) String lessonCd,
            @RequestParam(value = "lessonTitle", required = false, defaultValue = "") String lessonTitle,
            @RequestParam(value = "referenceSeq", required = false, defaultValue = "") String referenceSeq,
            @RequestParam(value = "type", required = false, defaultValue = "") String type,
            @RequestParam(value = "fileNm", required = false, defaultValue = "") String fileNm,
            @RequestParam(value = "filePath", required = false, defaultValue = "") String filePath
            ) throws Exception {

        logger.debug("deleteReference");
        logger.debug(referenceSeq);
        logger.debug(type);
        logger.debug(fileNm);
        logger.debug(filePath);

        VSResult result = new VSResult();

        if (!referenceSeq.equals("")) {

            try {
                ReferenceCondition referenceCondition = new ReferenceCondition();
                referenceCondition.setReferenceSeq(referenceSeq);
                referenceCondition.setFileNm(fileNm);
                referenceCondition.setFilePath(filePath);
                referenceCondition.setType(type);

                referenceService.deleteDataWithResultCodeMsg(referenceCondition);

                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } catch (Exception e) {
                result.setCode("9999");
                result.setMessage("조회 오류");
                e.printStackTrace();
            }
        } else {
            result.setCode("1000");
            result.setMessage("필수 파라미터");
        }

        model.addAttribute("result", result);

        return "redirect:/lcms/reference/referenceList.do?lessonCd=" + lessonCd + "&lessonTitle=" + URLEncoder.encode(lessonTitle);
    }


    /**
     * deleteReference
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/reference/referenceTempDataDelete")
    public VSResult deleteReferenceTempData(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false) String lessonCd
            ) throws Exception {

        logger.debug("deleteReferenceTempData");
        logger.debug(lessonCd);

        VSResult result = new VSResult();

        if (!lessonCd.equals("")) {

            try {
                ReferenceCondition referenceCondition = new ReferenceCondition();
                referenceCondition.setLessonCd(lessonCd);

                // 삭제된 차시의 레퍼런스 USE_YN 수정
                referenceService.deleteReference(referenceCondition);

                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } catch (Exception e) {
                result.setCode("9999");
                result.setMessage("조회 오류");
                e.printStackTrace();
            }
        } else {
            result.setCode("1000");
            result.setMessage("필수 파라미터");
        }

        model.addAttribute("result", result);

        return result;
    }
}
