package com.visangesl.treeadmin.lcms.program.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.program.dao.LcmsProgramDao;
import com.visangesl.treeadmin.lcms.program.service.LcmsProgramService;
import com.visangesl.treeadmin.lcms.program.vo.Item;
import com.visangesl.treeadmin.lcms.program.vo.ItemInfo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class LcmsProgramServiceImpl implements LcmsProgramService {

	@Autowired
	LcmsProgramDao dao;

	@Override
	public VSObject getItemDataInfo(VSObject vsObject) throws Exception {
		return dao.getItemDataInfo(vsObject);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getItemDataList(VSCondition dataCondition) throws Exception {
        return dao.getItemDataList(dataCondition);
    }
    
	@Override
    @Transactional(readOnly = true)
    public int getItemDataListCnt(VSCondition dataCondition) throws Exception {
        return dao.getItemDataListCnt(dataCondition);
    }
	
	
	@Override
    @Transactional
    public VSResult modifyItemInfoData(ItemInfo itemInfo , String[] prodVal) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.modifyItemInfoData(itemInfo); 
        
        
        // item delete 
        dao.deleteItemData(itemInfo);
        
        // item insert 
        List<Item> itemList = new ArrayList<Item>(); 
        
        if(prodVal != null && prodVal.length > 0){
        	
        	for(int i=0; i < prodVal.length; i ++){
        		//prodVal = prodSeq|itemType 으로 되어있음.
        		String[] itemVal = prodVal[i].split("#");
        		
	        	Item itemVo = new Item();
	        	
	        	itemVo.setProdSeq(itemVal[0]);
	        	itemVo.setItemType(itemVal[1]);
	        	itemVo.setItemInfoSeq(itemInfo.getItemInfoSeq());
	        	
	        	dao.addItemData(itemVo);
	        }	
        }
        		
        if (affectedRows < 1) {
        	
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult addItemInfoData(ItemInfo itemInfo , String[] prodVal) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addItemInfoData(itemInfo);
        
        String itemInfoSeq = itemInfo.getItemInfoSeq();
        
        System.out.println("등록된 item_info_seq==="+itemInfoSeq);
        
        
        if(prodVal != null && prodVal.length > 0){
        	
        	for(int i=0; i < prodVal.length; i ++){
        		//prodVal = prodSeq|itemType 으로 되어있음.
        		String[] itemVal = prodVal[i].split("#");
        		
	        	Item itemVo = new Item();
	        	
	        	itemVo.setProdSeq(itemVal[0]);
	        	itemVo.setItemType(itemVal[1]);
	        	itemVo.setItemInfoSeq(itemInfo.getItemInfoSeq());
	        	
	        	dao.addItemData(itemVo);
	        }	
        }
        

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	
	@Override
    @Transactional
    public VSResult addItemData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addItemData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	
    public VSResult deleteItemData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.deleteItemData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    } 
	
    
    public List<VSObject> getTreeItemList(VSObject vsObject) throws Exception {
    	return dao.getTreeItemList(vsObject);
    }
	
    public List<VSObject> getInstituteItemList(VSCondition dataCondition) throws Exception {
    	return dao.getInstituteItemList(dataCondition);
    }
    
    
}
