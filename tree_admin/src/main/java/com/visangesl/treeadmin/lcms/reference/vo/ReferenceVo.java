package com.visangesl.treeadmin.lcms.reference.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class ReferenceVo extends VSObject {

    private String referenceSeq;
    private String dayNoCd;
    private String regId;
    private String regDate;
    private String modId;
    private String modDate;
    private String type;
    private String content;
    private String fileNm;
    private String filePath;
    private String fileSize;
    private List<MultipartFile> uploadFile;

    private String mbrId;
    private String sortOrd;
    private String lessonCd;
    private String no;

    public String getReferenceSeq() {
        return referenceSeq;
    }
    public void setReferenceSeq(String referenceSeq) {
        this.referenceSeq = referenceSeq;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getRegDate() {
        return regDate;
    }
    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }
    public String getModId() {
        return modId;
    }
    public void setModId(String modId) {
        this.modId = modId;
    }
    public String getModDate() {
        return modDate;
    }
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getFileNm() {
        return fileNm;
    }
    public void setFileNm(String fileNm) {
        this.fileNm = fileNm;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getFileSize() {
        return fileSize;
    }
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
    public List<MultipartFile> getUploadFile() {
        return uploadFile;
    }
    public void setUploadFile(List<MultipartFile> uploadFile) {
        this.uploadFile = uploadFile;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }


}
