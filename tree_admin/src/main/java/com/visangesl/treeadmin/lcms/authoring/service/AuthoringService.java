package com.visangesl.treeadmin.lcms.authoring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.authoring.vo.AuthoringCondition;
import com.visangesl.treeadmin.lcms.authoring.vo.AuthoringVo;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceCondition;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface AuthoringService {

    // select One Data
    public VSObject getData(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getDataList(AuthoringCondition condition) throws Exception;

    // select List Data
    public List<VSObject> getLessonList(AuthoringCondition condition) throws Exception;

    // select List Count
    public int getDataListCnt(VSObject vsObject) throws Exception;

    // Update
    public VSResult<VSObject> modifyDataWithResultCodeMsg(AuthoringVo authoringVo) throws Exception;

    // Insert
    public VSResult<VSObject> addDataWithResultCodeMsg(AuthoringVo authoringVo) throws Exception;

    // Delete
    public VSResult<VSObject> deleteDataWithResultCodeMsg(ReferenceCondition referenceCondition) throws Exception;


}
