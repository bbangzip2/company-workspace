package com.visangesl.treeadmin.lcms.program.vo;

import com.visangesl.treeadmin.vo.VSListCondition;

public class ItemInfoCondition extends VSListCondition {

	private String itemInfoSeq;
	private String itemType;
	private String instituteSeq;
	private String orderColumn;
	private String sort;
	private int pageNo;
	private int pageSize;
	
	
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getSort() {
		return sort;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getItemInfoSeq() {
		return itemInfoSeq;
	}
	public void setItemInfoSeq(String itemInfoSeq) {
		this.itemInfoSeq = itemInfoSeq;
	}
	
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	
}
