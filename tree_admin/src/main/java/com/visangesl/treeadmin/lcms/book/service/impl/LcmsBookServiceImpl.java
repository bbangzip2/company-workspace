package com.visangesl.treeadmin.lcms.book.service.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.CardDao;
import com.visangesl.treeadmin.file.helper.TreeFileUtils;
import com.visangesl.treeadmin.file.helper.vo.FileUploadResult;
import com.visangesl.treeadmin.lcms.book.dao.LcmsBookDao;
import com.visangesl.treeadmin.lcms.book.service.LcmsBookService;
import com.visangesl.treeadmin.lcms.book.vo.ContentStruc;
import com.visangesl.treeadmin.lcms.book.vo.LcmsBookVo;
import com.visangesl.treeadmin.lcms.book.vo.LcmsLessonCondition;
import com.visangesl.treeadmin.lcms.book.vo.LcmsLessonVo;
import com.visangesl.treeadmin.lcms.card.vo.CardContentSeqVo;
import com.visangesl.treeadmin.lcms.card.vo.CardTempCondition;
import com.visangesl.treeadmin.lcms.card.vo.CardVo;
import com.visangesl.treeadmin.lcms.card.vo.ContentStrucCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class LcmsBookServiceImpl implements LcmsBookService {

    @Autowired
    LcmsBookDao dao;

    @Autowired
    CardDao cardDao;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public VSObject getData(VSObject vsObject) throws Exception {
        return dao.getData(vsObject);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return dao.getDataList(dataCondition);
    }

    @Override
    @Transactional(readOnly = true)
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
        return dao.getDataListCnt(dataCondition);
    }


    @Override
    @Transactional
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        VSResult<VSObject> resultCodeMsg = new VSResult();

        LcmsBookVo bookVo = (LcmsBookVo) vsObject;
        MultipartFile uploadFile = bookVo.getUploadFile();

        if (uploadFile != null) {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_thumbnail_path");

            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
            	bookVo.setThmbPath(savePath + "/" + result.getRealFileNm());

                // 이전 업로드된 이미지 파일을 지운다.
            	LcmsBookVo prevbookVo = (LcmsBookVo) dao.getData(bookVo);
                if (TreeAdminUtil.isNull(prevbookVo.getThmbPath()) != "") {
                    File preFile = new File(prevbookVo.getThmbPath());
                    TreeFileUtils.removeFile(preFile.getParent(), preFile.getName());
                }

            }
        }

        int affectedRows = dao.modifyDataWithResultCodeMsg(bookVo);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        VSResult<VSObject> resultCodeMsg = new VSResult();

        LcmsBookVo bookVo = (LcmsBookVo) vsObject;
        MultipartFile uploadFile = bookVo.getUploadFile();

        // 이미지 파일 업로드
        if (uploadFile != null) {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_thumbnail_path");

            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
            	bookVo.setThmbPath(savePath + "/" + result.getRealFileNm());
            }
        }


        int affectedRows = dao.addDataWithResultCodeMsg(vsObject);

        LcmsBookVo rstVo = (LcmsBookVo) vsObject;
        String bookSeq = rstVo.getBookSeq();

        ContentStruc contentStruc = new ContentStruc();
        contentStruc.setContentSeq(bookSeq);
        contentStruc.setMbrId(bookVo.getRegId());
        contentStruc.setSortOrd("1");

        dao.addDataContentStruc(contentStruc);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        VSResult<VSObject> resultCodeMsg = new VSResult();

        int affectedRows = dao.deleteDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }


    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getLessonDataList(VSObject vsObject) throws Exception {
        return dao.getLessonDataList(vsObject);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getLessonInfoList(VSObject vsObject) throws Exception {
        return dao.getLessonInfoList(vsObject);
    }

    @Override
    @Transactional
    public int modifyLessonData(List<LcmsLessonVo> insertList, List<LcmsLessonVo> updateList, List<LcmsLessonVo> deleteList)throws Exception{
    	int result = 0;

    	if(insertList.size() > 0){
        	// lesson Title insert 처리
        	for(int i=0; i < insertList.size();i++){

        		LcmsLessonVo insertVo = insertList.get(i);

        		if(!insertVo.getLessonTitle().equals("")){
        			dao.addLessonData(insertVo);

                	// content_struc insert
            		String lessonSeq = insertVo.getLessonSeq();

            		ContentStruc csVo = new ContentStruc();
            		csVo.setContentSeq(lessonSeq);
            		csVo.setMbrId(insertVo.getRegId());
            		csVo.setUpperContentSeq(insertVo.getBookSeq());
            		csVo.setSortOrd(insertVo.getSort());

            		dao.addDataContentStruc(csVo);

        		}

        	}
    	}


    	if(updateList.size() > 0){
	    	// lesson Title   update 처리
	    	for(int h=0; h < updateList.size(); h++){
	    		LcmsLessonVo updateVo = updateList.get(h);

	    		if(!updateVo.getLessonTitle().equals("")){

		    		dao.modifyLessonData(updateVo);

		    		ContentStruc csVo = new ContentStruc();
		    		csVo.setContentSeq(updateVo.getLessonSeq());
		    		csVo.setMbrId(updateVo.getModId());
		    		csVo.setUpperContentSeq(updateVo.getBookSeq());
		    		csVo.setSortOrd(updateVo.getSort());

		        	// content_struc update
		    		dao.modifyDataContentStruc(csVo);

	    		}
	    	}
    	}


    	if(deleteList.size() > 0 ){
        	// delete 처리
        	// content_struc delete
        	for(int j=0; j < deleteList.size(); j++){
        		LcmsLessonVo deleteVo = deleteList.get(j);

        		dao.deleteLessonData(deleteVo);

        		ContentStruc csVo = new ContentStruc();
        		csVo.setContentSeq(deleteVo.getLessonSeq());
        		csVo.setMbrId(deleteVo.getModId());
        		csVo.setUpperContentSeq(deleteVo.getBookSeq());

            	// content_struc update
        		dao.deleteDataContentStruc(csVo);

        	}
    	}

    	return result ;

    }

    @Override
    public VSObject getLessonData(VSCondition dataCondition) throws Exception {
        return dao.getLessonData(dataCondition);
    }


    @Override
    @Transactional
    public VSResult<VSObject> modifyLessonInfo(VSObject vsObject) throws Exception {
        VSResult<VSObject> resultCodeMsg = new VSResult();

        LcmsLessonVo lessonVo = (LcmsLessonVo) vsObject;
        MultipartFile uploadFile = lessonVo.getUploadFile();

        if (uploadFile != null) {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_thumbnail_path");

            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
            	lessonVo.setThmbPath(savePath + "/" + result.getRealFileNm());

            	LcmsLessonCondition cond = new LcmsLessonCondition();
            	cond.setLessonSeq(lessonVo.getLessonSeq());
                // 이전 업로드된 이미지 파일을 지운다.
            	LcmsLessonVo prevbookVo = (LcmsLessonVo) dao.getLessonData(cond);

                if (TreeAdminUtil.isNull(prevbookVo.getThmbPath()) != "") {
                    File preFile = new File(prevbookVo.getThmbPath());
                    TreeFileUtils.removeFile(preFile.getParent(), preFile.getName());
                }
            }
        }

        int affectedRows = dao.modifyLessonInfo(lessonVo);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));

        } else { //카드맵 정보/구조 저장 진행

            String lessonCd = lessonVo.getLessonSeq();
            String mbrId = lessonVo.getModId();

            // 임시 테이블에 미사용 컨텐츠 정보 조회
            List<CardVo> contentInfoTempUnUsedList = cardDao.getContentInfoTempUnUsedList();

            if (contentInfoTempUnUsedList.size() > 0) {

                // 컨텐츠 정보 삭제 진행
                CardContentSeqVo cardContentSeqVo = new CardContentSeqVo();
                for (CardVo cardVo : contentInfoTempUnUsedList) {

                    // @@@@@ 주의 @@@@@
                    // 다른 레슨의 미사용 컨텐츠도 삭제가 된다...;;;;;
                    // @@@@@ 주의 @@@@@
                    cardContentSeqVo.setContentSeq(cardVo.getContentSeq());
                    cardDao.deleteDataWithResultCodeMsg(cardContentSeqVo);
                }
            }


            // 임시 테이블에 해당 레슨 정보가 존재하는지 조회
            CardTempCondition cardTempCondition = new CardTempCondition();
            cardTempCondition.setLessonCd(lessonVo.getLessonSeq());
            cardTempCondition.setMbrId(mbrId);
            List<CardVo> contentInfoTempList = cardDao.getContentInfoTempList(cardTempCondition);

            if (contentInfoTempList.size() > 0) {
                for (CardVo cardVo : contentInfoTempList) {

                    if (!cardVo.getContentSeq().equals(lessonCd)) {
                        // 컨텐츠 정보 등록 (기존 있는 컨텐츠 정보면 업데이트)
                        cardVo.setDistYn("N");
                        cardDao.addContentInfo(cardVo);
                    }
                }

                // 기존 컨텐츠 레슨 구조 삭제
                ContentStrucCondition contentStrucCondition = new ContentStrucCondition();
                contentStrucCondition.setLessonCd(lessonCd);
                contentStrucCondition.setMbrId(mbrId);
                List<ContentStrucCondition> contentStrucListService = cardDao.getContentStrucListService(contentStrucCondition); // 컨텐츠 구조 조회

                for (ContentStrucCondition condition : contentStrucListService) {

                    String contentSeq = condition.getStrucSeq();
                    condition.setContentSeq(contentSeq);

                    cardDao.deleteContentStrucStrucSeq(condition); // 컨텐츠 구조 삭제
                }


                // 컨텐츠 구조 정보 등록
                contentStrucCondition = new ContentStrucCondition();
                contentStrucCondition.setLessonCd(lessonCd);
                contentStrucCondition.setMbrId(mbrId);
                List<ContentStrucCondition> contentStrucListService2 = cardDao.getContentStrucList(contentStrucCondition); // 컨텐츠 구조 조회

                for (ContentStrucCondition condition : contentStrucListService2) {
                    cardDao.addContentStruc(condition);
                }


                // 임시 컨텐츠 테이블 삭제 진행
                cardDao.deleteContentInfoTempService(contentStrucCondition); // 컨텐츠 정보

                List<ContentStrucCondition> contentStrucTempList = cardDao.getContentStrucTempList(contentStrucCondition); // 컨텐츠 구조 조회

                for (ContentStrucCondition condition : contentStrucTempList) {
                    cardDao.deleteContentStrucTempStrucSeq(condition); // 컨텐츠 구조 삭제
                }

            }

            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
}
