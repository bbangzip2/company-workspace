package com.visangesl.treeadmin.lcms.contents.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.lcms.contents.service.FetcherService;
import com.visangesl.treeadmin.lcms.contents.vo.FetcherCardVo;
import com.visangesl.treeadmin.lcms.contents.vo.FetcherCondition;
import com.visangesl.treeadmin.lcms.contents.vo.FetcherVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.CodeCondition;
import com.visangesl.treeadmin.vo.VSResult;


/**
 * 저작도구 관련 처리 Contoller
 * @author user
 *
 */
@Controller
public class FetcherController {

    @Autowired
    private FetcherService fetcherService;
    
    @Autowired
    private CommonService commonService;
    

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * fetcher list 조회 
     * @param request
     * @param response
     * @param searchWord : 검색어 
     * @return 카드 리스트
     * @throws Exception
     */
    @RequestMapping(value="/contents/fetcherList")
    public String getFetcherList(HttpServletRequest request, HttpServletResponse response , Model model
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort		    		
			, @RequestParam(value = "searchWord", required = false, defaultValue = "") String searchWord
    		) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        
        String mbrId = "";
        String mbrGrade = "";

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        if (userDetail != null) {
            mbrId = userDetail.getUsername();
            mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        }


        FetcherCondition condition = new FetcherCondition();
        
        condition.setSearchWord(searchWord);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDttm"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));


        // 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
        int pageStartIndex = (condition.getPageNo() - 1) * condition.getPageSize();
        condition.setPageStartIndex(pageStartIndex);

        
        List<VSObject> cardList = fetcherService.getFetcherList(condition);
        int totalCount =  fetcherService.getFetcherListCnt(condition);
        
        
        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < cardList.size()) {
            FetcherVo item = (FetcherVo) cardList.get(i);
            item.setRn(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
        model.addAttribute("cardList", cardList);
        model.addAttribute("totalCount", totalCount);
        model.addAttribute("condition", condition);

        return "/lcms/contents/fetcher/fetcherList";

    }

    /**
     * 카드 등록 페이지로 이동 
     * @param request
     * @param response
     * @param model
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param sort
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/contents/fetcherInsert")
    public String fetcherInsert(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	   
			, @RequestParam(value = "searchWord", required = false, defaultValue = "") String searchWord
            ) throws Exception {

    	logger.debug("fetcherInsert");
    	
    	// 1. bookList combobox
    	List<VSObject> bookList = fetcherService.getContentBookList();
    	
    	// 2. nationaility combobox 
    	CodeCondition codeCond = new CodeCondition();
    	codeCond.setRelCd(TreeProperties.getProperty("tree_lang_cd"));
    	List<VSObject> nationList = commonService.getCodeList(codeCond);
    	
    	
    	
        FetcherCondition condition = new FetcherCondition();
        condition.setSearchWord(searchWord);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDttm"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);
        model.addAttribute("bookList", bookList);
        model.addAttribute("nationList", nationList);
        
        
        return "/lcms/contents/fetcher/fetcherEdit";
    }
    
    /**
     * 카드 등록/ 수정 화면 이동 
     * @param request
     * @param response
     * @param model
     * @param bookSeq
     * @param pageNo
     * @param pageSize
     * @param bookNm
     * @param orderBy
     * @param sort
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/contents/fetcherEdit")
    public String fetcherModify(HttpServletRequest request, HttpServletResponse response, Model model
    		, @RequestParam(value = "contentSeq", required = true, defaultValue = "") String contentSeq
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	     
			, @RequestParam(value = "searchWord", required = false, defaultValue = "") String searchWord
            ) throws Exception {

        logger.debug("fetcherModify");

        
    	// 1. bookList combobox
    	List<VSObject> bookList = fetcherService.getContentBookList();
    	
    	// 2. nationaility combobox 
    	CodeCondition codeCond = new CodeCondition();
    	codeCond.setRelCd(TreeProperties.getProperty("tree_lang_cd"));
    	List<VSObject> nationList = commonService.getCodeList(codeCond);
    	
    	
//        LcmsBookVo lcmsBookVo = new LcmsBookVo();
//        lcmsBookVo.setBookSeq(bookSeq);
//        lcmsBookVo = (LcmsBookVo) fetcherService.getFetcherInfo(lcmsBookVo);

        FetcherCondition condition = new FetcherCondition();
        condition.setSearchWord(searchWord);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);
        model.addAttribute("bookList", bookList);
        model.addAttribute("nationList", nationList);

        return "/lcms/contents/fetcher/fetcherEdit";
    }
    
    /**
     * 카드 뷰 화면 이동 
     * @param request
     * @param response
     * @param model
     * @param contentSeq
     * @param searchWord
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param sort
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/contents/fetcherView")
    public String fetcherView(HttpServletRequest request, HttpServletResponse response, Model model
    		, @RequestParam(value = "contentSeq", required = true, defaultValue = "") String contentSeq
    		, @RequestParam(value = "searchWord", required = false, defaultValue = "") String searchWord
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	            
            ) throws Exception {

        logger.debug("fetcherView");

        VSObject cardInfo = fetcherService.getFecherCardInfo(contentSeq);
        
        FetcherCondition condition = new FetcherCondition();
        condition.setSearchWord(searchWord);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);
        model.addAttribute("cardInfo", cardInfo);

        return "/lcms/contents/fetcher/fetcherView";
    }
    
    /**
     * 레슨 목록 조회 JSON
     * @param request
     * @param response
     * @param mbrId
     * @param bookCd
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/lessonList")
    public VSResult<List<VSObject>> getLessonList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd
            ) throws Exception {

        logger.debug("getLessonList");

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {
        	
            List<VSObject> list = null;

	        list = fetcherService.getContentLessonList(bookCd);
	
	        result.setResult(list);
	        result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }
    

}
