package com.visangesl.treeadmin.lcms.card.vo;

public class CardDayNoCondition extends CardContentSeqVo {

    private String type;
    private String title;
    private String mbrId;

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
}
