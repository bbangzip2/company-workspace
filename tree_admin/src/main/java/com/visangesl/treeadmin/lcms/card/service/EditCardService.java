package com.visangesl.treeadmin.lcms.card.service;

import org.springframework.stereotype.Service;

import com.visangesl.treeadmin.lcms.card.vo.EditCardCondition;
import com.visangesl.treeadmin.lcms.card.vo.EditCardPathCondition;

@Service
public interface EditCardService {

    public boolean copyEditCard(EditCardPathCondition condition) throws Exception;

    public boolean parseEditCardPath(EditCardCondition condition) throws Exception;
}
