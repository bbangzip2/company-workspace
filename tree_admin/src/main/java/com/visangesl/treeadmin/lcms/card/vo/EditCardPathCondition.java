package com.visangesl.treeadmin.lcms.card.vo;

import java.io.File;

public class EditCardPathCondition extends EditCardCondition {

    private File toBeDir;
    private File asIsDir;


    public File getToBeDir() {
        return toBeDir;
    }
    public void setToBeDir(File toBeDir) {
        this.toBeDir = toBeDir;
    }
    public File getAsIsDir() {
        return asIsDir;
    }
    public void setAsIsDir(File asIsDir) {
        this.asIsDir = asIsDir;
    }

}
