package com.visangesl.treeadmin.lcms.card.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.visangesl.treeadmin.lcms.card.service.EditCardService;
import com.visangesl.treeadmin.lcms.card.vo.EditCardCondition;
import com.visangesl.treeadmin.lcms.card.vo.EditCardPathCondition;

@Service
public class EditCardServiceImpl implements EditCardService {

    @Override
    public boolean copyEditCard(EditCardPathCondition condition) throws Exception {
        // TODO Auto-generated method stub
        boolean result = false;

        // 폴더 복사
        result = fileCopy(condition.getAsIsDir(), condition.getToBeDir());

        return result;
    }

    @Override
    public boolean parseEditCardPath(EditCardCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return false;
    }



    // 폴더 복사
    public boolean fileCopy(File asIsDir, File toBeDir) {

        boolean result = true;

        // 폴더 복사
        File[] ff = asIsDir.listFiles();
        for (File file : ff) {
            File temp = new File(toBeDir.getAbsolutePath() + File.separator
                    + file.getName());
            if (file.isDirectory()) {
                temp.mkdir();
                fileCopy(file, temp);
            } else {
                FileInputStream fis = null;
                FileOutputStream fos = null;
                try {
                    fis = new FileInputStream(file);
                    fos = new FileOutputStream(temp);
                    byte[] b = new byte[4096];
                    int cnt = 0;
                    while ((cnt = fis.read(b)) != -1) {
                        fos.write(b, 0, cnt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    result = false;
                    break;
                } finally {
                    try {
                        fis.close();
                        fos.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

        // 예외 발생 시 복사 진행된 폴더를 삭제
        if (!result) {
            deleteDir(toBeDir);
        }
        return result;
    }


    // 폴더 삭제
    public boolean deleteDir(File toBeDir) {

        if (!toBeDir.exists()) {
            return false;
        }

        File[] files = toBeDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                file.delete();
            }
        }

        return toBeDir.delete();
    }

}
