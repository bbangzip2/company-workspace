package com.visangesl.treeadmin.lcms.program.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.program.vo.ItemInfo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface LcmsProgramService {
	
	public VSObject getItemDataInfo(VSObject vsObject) throws Exception ;
	
	public List<VSObject> getItemDataList(VSCondition dataCondition) throws Exception ;
	
	public int getItemDataListCnt(VSCondition dataCondition) throws Exception ;
	
	public VSResult modifyItemInfoData(ItemInfo itemInfo , String[] prodVal) throws Exception;
	
	public VSResult addItemInfoData(ItemInfo itemInfo , String[] prodVal) throws Exception;
	
	public VSResult addItemData(VSObject vsObject) throws Exception ;
	
	public VSResult deleteItemData(VSObject vsObject) throws Exception;
	
	public List<VSObject> getTreeItemList(VSObject vsObject) throws Exception ;
	
	public List<VSObject> getInstituteItemList(VSCondition dataCondition) throws Exception ;
}

