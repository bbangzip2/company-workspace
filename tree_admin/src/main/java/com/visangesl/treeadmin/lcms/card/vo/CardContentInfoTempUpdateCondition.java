package com.visangesl.treeadmin.lcms.card.vo;

public class CardContentInfoTempUpdateCondition extends CardContentSeqVo {

    private String useYn;
    private String type;

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
