package com.visangesl.treeadmin.lcms.contents.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class FetcherDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    public List<VSObject> getFetcherList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("fetcher.getFetcherList", dataCondition);
    	return result;
    }
    
    public int getFetcherListCnt(VSCondition dataCondition) throws Exception {
    	return getSqlSession().selectOne("fetcher.getFetcherListCnt", dataCondition);
    }
    
    public List<VSObject> getContentBookList() throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("fetcher.getContentBookList"); 
    	return 	result;
    }
    
    public List<VSObject> getContentLessonList(String bookCd) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("fetcher.getContentLessonList", bookCd); 
    	return result;
    }
    
    public VSObject getFecherCardInfo(String contentSeq) throws Exception{
    	return getSqlSession().selectOne("fetcher.getFecherCardInfo", contentSeq);
    }
    
    
}
