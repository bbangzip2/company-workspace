package com.visangesl.treeadmin.lcms.card.vo;

public class CardListCondition extends CardCondition {

    private String cardSect;
    private String sortOrd;
    private String lessonTitle;

    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
    public String getLessonTitle() {
        return lessonTitle;
    }
    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }


}
