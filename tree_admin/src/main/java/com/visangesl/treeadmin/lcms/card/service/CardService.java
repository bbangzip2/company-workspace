package com.visangesl.treeadmin.lcms.card.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.card.vo.CardContentSeqVo;
import com.visangesl.treeadmin.lcms.card.vo.CardStrucCondition;
import com.visangesl.treeadmin.lcms.card.vo.ContentStrucCondition;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface CardService {

    public int getTempLessonStrucCount(VSObject vsObject) throws Exception;

    public int getLessonStrucCount(VSObject vsObject) throws Exception;

    public VSResult<VSObject> addTempContent(VSObject vsObject) throws Exception;

    public List<VSObject> getTempDayNoList(VSObject vsObject) throws Exception;

    public List<VSObject> getTemp1DayNoCardList(VSObject vsObject) throws Exception;

    public List<VSObject> getTempSubCardList(VSObject vsObject) throws Exception;

    public List<VSObject> getTempDayNoCardList(VSObject vsObject) throws Exception;

    public VSObject getTempCardInfo(VSObject vsObject) throws Exception;

    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    public VSResult<VSObject> deleteDataWithResultCodeMsg(CardContentSeqVo condition) throws Exception;

    public VSResult<VSObject> addDataWithResultCodeMsg(CardStrucCondition condition) throws Exception;

    public VSResult<VSObject> modifyContentInfoTempUseYn(CardContentSeqVo condition) throws Exception;

    public VSResult<VSObject> deleteContentTempData(ContentStrucCondition contentStrucCondition) throws Exception;



/*


    // select One Data
    public VSObject getData(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getDataList(VSObject vsObject) throws Exception;

    // select List Count
    public int getDataListCnt(VSObject vsObject) throws Exception;

    // Update
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // Insert
    public VSResult<VSObject> addDataWithResultCodeMsg(CardStrucCondition condition) throws Exception;

    // Delete
    public VSResult<VSObject> deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getDayNoList(VSObject vsObject) throws Exception;


    // Insert
    public VSResult<VSObject> addEditCard(CardVo condition) throws Exception;

    // Update
    public int modifyEditCard(CardVo condition) throws Exception;

    // select List Data
    public List<VSObject> getSubCardList(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getCardList(VSObject vsObject) throws Exception;

    // select List Data
    public VSObject getCardInfoList(VSObject vsObject) throws Exception;

*/





}
