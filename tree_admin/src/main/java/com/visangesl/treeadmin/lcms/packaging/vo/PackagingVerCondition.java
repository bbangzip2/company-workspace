package com.visangesl.treeadmin.lcms.packaging.vo;


public class PackagingVerCondition extends PackagingMbrId {

    private String contentSeq;
    private String type;

    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
