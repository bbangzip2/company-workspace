package com.visangesl.treeadmin.lcms.reference.service.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.ReferenceDao;
import com.visangesl.treeadmin.file.helper.TreeFileUtils;
import com.visangesl.treeadmin.file.helper.vo.FileUploadResult;
import com.visangesl.treeadmin.lcms.reference.service.ReferenceService;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceCondition;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceFIleCondition;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class ReferenceServiceImpl implements ReferenceService {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    ReferenceDao referenceDao;

    @Override
    public VSObject getData(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return referenceDao.getData(vsObject);
    }

    @Override
    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return referenceDao.getDataList(vsObject);
    }

    @Override
    public int getDataListCnt(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return referenceDao.getDataListCnt(vsObject);
    }

    @Override
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        int affectedRows = referenceDao.modifyDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {

            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(ReferenceVo referenceVo)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        // 레퍼런스 정보 등록
        referenceDao.addDataWithResultCodeMsg(referenceVo);

        // 레퍼런스 등록 키값 조회
        String referenceSeq = referenceVo.getReferenceSeq();

        // 레퍼런스 정보 등록 결과 값 체크
        if (referenceSeq != null || !referenceSeq.equals("") || !referenceSeq.equals("0")) {

            // 업로드 파일 체크 TYPE : 이미지= FT001 , 동영상= FT003, 텍스트= FT005, url= FT004)
        	
            if (referenceVo.getType().equals("FT001") || referenceVo.getType().equals("FT003")) {

                List<MultipartFile> uploadFileList = referenceVo.getUploadFile();
                logger.debug("uploadFileList : " + uploadFileList.size());

                if (uploadFileList != null && uploadFileList.size() > 0) {

                    // 파일이 업로드 될 절대경로의 루트를 먼저 구한다
                    String savePath = TreeProperties.getProperty("tree_save_filepath") + TreeProperties.getProperty("tree_reference_filepath") + TreeProperties.getProperty("user_path") + File.separator + referenceVo.getDayNoCd();
                    String realPath = TreeProperties.getProperty("tree_reference_filepath") + TreeProperties.getProperty("user_path") + File.separator + referenceVo.getDayNoCd();

                    ReferenceFIleCondition condition = null;

                    try {
                        // 파일 등록
                        for (MultipartFile file : uploadFileList) {
                            condition = new ReferenceFIleCondition();

                        	FileUploadResult uploadResult = TreeFileUtils.uploadFile(file, savePath);
                        	if (uploadResult.isUploaded()) {

                        	    condition.setReferenceSeq(referenceSeq);
                        	    condition.setFileNm(uploadResult.getOrgFileNm());
                        	    condition.setFilePath(realPath + File.separator + uploadResult.getOrgFileNm());
                        	    condition.setFileSize(String.valueOf(uploadResult.getFileSize()));

//                                // 파일명 중복 체크 (동일 차시에 기존 등록된 같은 파일명이 있는지 체크 - 있으면 Exception 발생)
//                                int count = referenceDao.getReferenceSameFileCount(condition);
//                                if (count > 0) {
//                                    referenceVo.setFileNm(uploadResult.getOrgFileNm() + "(" + count + ")");
//                                    referenceVo.setFilePath(uploadResult.getRealFilePath() + "(" + count + ")");
//                                }

                                // 레퍼런스 파일 정보 등록
                                referenceDao.addReferenceFileInfo(condition);
                        	}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));

                        // 파일 등록 중 에러 발생시 파일 삭제 진행
                        for (MultipartFile file : uploadFileList) {
                            String fileName = file.getOriginalFilename();
                            TreeFileUtils.removeFile(savePath + File.separator + fileName, fileName);
                        }
                    }
                } else {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                }
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }

        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> deleteDataWithResultCodeMsg(ReferenceCondition referenceCondition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        int affectedRows = 0;
        if (referenceCondition.getType().equals("Text") || referenceCondition.getType().equals("URL")) {
            // 레퍼런스 정보 삭제 - 텍스트/URL 일 경우
            affectedRows = referenceDao.deleteDataWithResultCodeMsg(referenceCondition);
        } else {
            // 레퍼런스 파일 정보 삭제 - 텍스트/URL 아닐 경우
            affectedRows = referenceDao.deleteReferenceFileInfo(referenceCondition);

            // 파일 삭제
            TreeFileUtils.removeFile(referenceCondition.getFilePath(), referenceCondition.getFileNm());

            // 넘어온 레퍼런스 순번에 해당하는, 레퍼런스 정보만 남아있는 레퍼런스가 있는지 조회
            affectedRows = referenceDao.getReferenceSeqCount(referenceCondition);

            if (affectedRows < 1) {
                // 레퍼런스 정보 삭제 - 껍떼기만 남은 레퍼런스 정보
                affectedRows = referenceDao.deleteDataWithResultCodeMsg(referenceCondition);
            }
        }

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    public List<VSObject> getDayNoCdList(String lessonCd) throws Exception {
        // TODO Auto-generated method stub
        return referenceDao.getDayNoCdList(lessonCd);
    }

    @Override
    public List<ReferenceVo> getReferenceTempList(ReferenceCondition referenceCondition) throws Exception {
        // TODO Auto-generated method stub
        return referenceDao.getReferenceTempList(referenceCondition);
    }

    @Override
    @Transactional
    public VSResult<VSObject> updateReferenceFileUseYn(List<ReferenceVo> referenceTempList)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        for (ReferenceVo list : referenceTempList) {

            ReferenceCondition referenceCondition = new ReferenceCondition();
            referenceCondition.setReferenceSeq(list.getReferenceSeq());

            int affectedRows = referenceDao.updateReferenceFileUseYn(referenceCondition);

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }
        }

        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult deleteReference(ReferenceCondition referenceCondition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();

        List<ReferenceVo> referenceTempList = referenceDao.getReferenceTempList(referenceCondition);

        int count = referenceTempList.size();
        if (count > 0) {
            // 삭제된 차시의 레퍼런스 USE_YN 수정
            for (ReferenceVo list : referenceTempList) {

                ReferenceCondition condition = new ReferenceCondition();
                referenceCondition.setReferenceSeq(list.getReferenceSeq());

                int affectedRows = referenceDao.deleteDataWithResultCodeMsg(condition);

                if (affectedRows < 1) {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                } else {

                    referenceDao.deleteReferenceFileInfo(condition);

                    resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
                }
            }
        }




        return resultCodeMsg;
    }

}
