package com.visangesl.treeadmin.lcms.packaging.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingCondition;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface PackagingService {


    public VSResult<VSObject> checkMakeContentPackaging(PackagingCondition condition) throws Exception;

    public VSResult makeContentPackaging(String mbrId, String bookCd, String lessonCd, List<PackagingVo> contentList) throws Exception;

    public VSResult makeOffLineContentPackaging(String mbrId, String bookCd, String lessonCd, List<PackagingVo> contentList) throws Exception;

//    public VSObject getData(VSObject vsObject) throws Exception;

//    public VSResult<VSObject> addDataWithResultCodeMsg(VSObject vsObject) throws Exception;

//    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    public VSResult releasePackaging(PackagingCondition condition) throws Exception;

    // select List Data
    public List<VSObject> getDataList(VSObject vsObject) throws Exception;

    // select List Count
    public int getDataListCnt(VSObject vsObject) throws Exception;

    // Insert
    public VSResult<VSObject> addDataWithResultCodeMsg(PackagingCondition condition) throws Exception;

}
