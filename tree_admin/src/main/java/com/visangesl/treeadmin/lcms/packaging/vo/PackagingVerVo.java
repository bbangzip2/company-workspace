package com.visangesl.treeadmin.lcms.packaging.vo;

import com.visangesl.tree.vo.VSObject;

public class PackagingVerVo extends VSObject {

    private String verSeq;
    private String mbrId;
    private String clsSeq;
    private String contentSeq;
    private String type;
    private String ver;
    private String fileNm;
    private String filePath;
    private String pkgingYn;
    private String distYn;
    private String distDate;


    public String getVerSeq() {
        return verSeq;
    }
    public void setVerSeq(String verSeq) {
        this.verSeq = verSeq;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getVer() {
        return ver;
    }
    public void setVer(String ver) {
        this.ver = ver;
    }
    public String getFileNm() {
        return fileNm;
    }
    public void setFileNm(String fileNm) {
        this.fileNm = fileNm;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getPkgingYn() {
        return pkgingYn;
    }
    public void setPkgingYn(String pkgingYn) {
        this.pkgingYn = pkgingYn;
    }
    public String getDistYn() {
        return distYn;
    }
    public void setDistYn(String distYn) {
        this.distYn = distYn;
    }
    public String getDistDate() {
        return distDate;
    }
    public void setDistDate(String distDate) {
        this.distDate = distDate;
    }

}
