package com.visangesl.treeadmin.lcms.program.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class LcmsProgramDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getItemDataInfo(VSObject vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("program.getItemDataInfo", vsObject);
    	return result;
    }
    
    // item list 조회 
    public List<VSObject> getItemDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("program.getItemDataList", dataCondition);
    	return result;
    }
    
    
    public int getItemDataListCnt(VSCondition dataCondition) throws Exception {
    	
    	return getSqlSession().selectOne("program.getItemDataListCnt", dataCondition);
    }
    
    public int addItemInfoData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("program.addItemInfoData", vsObject);
		return result;
	}
    
    
    public int modifyItemInfoData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("program.modifyItemInfoData", vsObject);
		return result;
	}
    
    public int addItemData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("program.addItemData", vsObject);
		return result;
	}
    

    public int deleteItemData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("program.deleteItemData", vsObject);
		return result;
	}
    
    public List<VSObject> getTreeItemList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("program.getTreeItemList", vsObject);
    	return result;
    }
    
    public List<VSObject> getInstituteItemList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("program.getInstituteItemList", dataCondition);
    	return result;
    }
    
    
}
