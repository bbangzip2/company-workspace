package com.visangesl.treeadmin.lcms.packaging.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingVo;

@Repository
public class PackagingDao extends TreeSqlSessionDaoSupport {

    public List<PackagingVo> getDataList(VSObject vsObject) throws Exception {
        List<PackagingVo> result = null;
        result = getSqlSession().selectList("packaging.getDataList", vsObject);
        return result;
    }

    public List<PackagingVo> getProdInfoList(VSObject vsObject) throws Exception {
        List<PackagingVo> result = null;
        result = getSqlSession().selectList("packaging.getProdInfoList", vsObject);
        return result;
    }

    public VSObject getBookCd(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("packaging.getBookCd", vsObject);
        return result;
    }

    public VSObject getDayNoCardList(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("packaging.getDayNoCardList", vsObject);
        return result;
    }

    public int modifyContentStruc(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("packaging.modifyContentStruc", vsObject);
        return result;
    }

    public int modifyContentInfo(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("packaging.modifyContentInfo", vsObject);
        return result;
    }

    public VSObject getPackagingVer(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("packaging.getPackagingVer", vsObject);
        return result;
    }

    public int insertPackagingVer(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("packaging.insertPackagingVer", vsObject);
        return result;
    }

    public int updatePackagingVer(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("packaging.updatePackagingVer", vsObject);
        return result;
    }

}
