package com.visangesl.treeadmin.lcms.book.vo;

import com.visangesl.tree.vo.VSObject;

public class LcmsDayInfo extends VSObject {
	private String dayLessonSeq;
    private String lessonSeq;
    private String lessonTitle;
    private String dayNoCd;
    private String sort;
    private String cnt;
    private String strucSeq;
    private String refCnt;
    
    
	public String getDayLessonSeq() {
		return dayLessonSeq;
	}
	public void setDayLessonSeq(String dayLessonSeq) {
		this.dayLessonSeq = dayLessonSeq;
	}
	public String getLessonSeq() {
		return lessonSeq;
	}
	public void setLessonSeq(String lessonSeq) {
		this.lessonSeq = lessonSeq;
	}
	public String getLessonTitle() {
		return lessonTitle;
	}
	public void setLessonTitle(String lessonTitle) {
		this.lessonTitle = lessonTitle;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getCnt() {
		return cnt;
	}
	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	public String getStrucSeq() {
		return strucSeq;
	}
	public void setStrucSeq(String strucSeq) {
		this.strucSeq = strucSeq;
	}
	public String getRefCnt() {
		return refCnt;
	}
	public void setRefCnt(String refCnt) {
		this.refCnt = refCnt;
	}

}
