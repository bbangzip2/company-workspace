package com.visangesl.treeadmin.lcms.contents.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.contents.dao.FetcherDao;
import com.visangesl.treeadmin.lcms.contents.service.FetcherService;

@Service
public class FetcherServiceImpl implements FetcherService {

    @Autowired
    FetcherDao dao;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getFetcherList(VSCondition dataCondition) throws Exception {
        return dao.getFetcherList(dataCondition);
    }

    @Override
    @Transactional(readOnly = true)
    public int getFetcherListCnt(VSCondition dataCondition) throws Exception {
        return dao.getFetcherListCnt(dataCondition);
    }

    /**
     * 컨텐츠 북 리스트 조회
     */
    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getContentBookList() throws Exception {
        return dao.getContentBookList();
    }

    /**
     * 컨텐츠 레슨 리스트 조회
     */
    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getContentLessonList(String bookCd) throws Exception {
        return dao.getContentLessonList(bookCd);
    }
    
    /**
     * 카드 상세 정보 조회  
     */
    public VSObject getFecherCardInfo(String contentSeq) throws Exception{
    	return dao.getFecherCardInfo(contentSeq);
    }
    
//    @Override
//    @Transactional
//    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
//        VSResult<VSObject> resultCodeMsg = new VSResult();
//
//        LcmsBookVo bookVo = (LcmsBookVo) vsObject;
//        MultipartFile uploadFile = bookVo.getUploadFile();
//
//        if (uploadFile != null) {
//            String rootPath = TreeProperties.getProperty("tree_save_filepath");
//            String savePath = TreeProperties.getProperty("tree_thumbnail_path");
//
//            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
//            if (result.isUploaded()) {
//            	bookVo.setThmbPath(savePath + "/" + result.getRealFileNm());
//
//                // 이전 업로드된 이미지 파일을 지운다.
//            	LcmsBookVo prevbookVo = (LcmsBookVo) dao.getData(bookVo);
//                if (TreeAdminUtil.isNull(prevbookVo.getThmbPath()) != "") {
//                    File preFile = new File(prevbookVo.getThmbPath());
//                    TreeFileUtils.removeFile(preFile.getParent(), preFile.getName());
//                }
//
//            }
//        }
//
//        int affectedRows = dao.modifyDataWithResultCodeMsg(bookVo);
//
//        if (affectedRows < 1) {
//
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }
//
//    @Override
//    @Transactional
//    public VSResult<VSObject> addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
//        VSResult<VSObject> resultCodeMsg = new VSResult();
//
//        LcmsBookVo bookVo = (LcmsBookVo) vsObject;
//        MultipartFile uploadFile = bookVo.getUploadFile();
//
//        // 이미지 파일 업로드
//        if (uploadFile != null) {
//            String rootPath = TreeProperties.getProperty("tree_save_filepath");
//            String savePath = TreeProperties.getProperty("tree_thumbnail_path");
//
//            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
//            if (result.isUploaded()) {
//            	bookVo.setThmbPath(savePath + "/" + result.getRealFileNm());
//            }
//        }
//
//
//        int affectedRows = dao.addDataWithResultCodeMsg(vsObject);
//
//        LcmsBookVo rstVo = (LcmsBookVo) vsObject;
//        String bookSeq = rstVo.getBookSeq();
//
//        ContentStruc contentStruc = new ContentStruc();
//        contentStruc.setContentSeq(bookSeq);
//        contentStruc.setMbrId(bookVo.getRegId());
//        contentStruc.setSortOrd("1");
//
//        dao.addDataContentStruc(contentStruc);
//
//        if (affectedRows < 1) {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }
}
