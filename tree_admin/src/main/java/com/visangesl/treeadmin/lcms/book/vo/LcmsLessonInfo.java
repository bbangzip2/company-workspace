package com.visangesl.treeadmin.lcms.book.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public class LcmsLessonInfo extends VSObject {
    private String lessonSeq;
    private String lessonTitle;
    private String pkgingYn;
    private String distYn;
    private String distDate;

    private List<LcmsDayInfo> dayList;


	public List<LcmsDayInfo> getDayList() {
		return dayList;
	}
	public void setDayList(List<LcmsDayInfo> dayList) {
		this.dayList = dayList;
	}
	public String getLessonSeq() {
		return lessonSeq;
	}
	public void setLessonSeq(String lessonSeq) {
		this.lessonSeq = lessonSeq;
	}
	public String getLessonTitle() {
		return lessonTitle;
	}
	public void setLessonTitle(String lessonTitle) {
		this.lessonTitle = lessonTitle;
	}
	public String getDistDate() {
		return distDate;
	}
	public void setDistDate(String distDate) {
		this.distDate = distDate;
	}
    public String getPkgingYn() {
        return pkgingYn;
    }
    public void setPkgingYn(String pkgingYn) {
        this.pkgingYn = pkgingYn;
    }
    public String getDistYn() {
        return distYn;
    }
    public void setDistYn(String distYn) {
        this.distYn = distYn;
    }

}
