package com.visangesl.treeadmin.lcms.contents.vo;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class FetcherVo extends VSObject {
	private String rn;
	
    private String contentSeq;
    private String title;
    private String bookNm;
    private String bookSeq;
    private String lessonNm;
    private String lessonSeq;
    private String regDttm;
    private String useYn;
    private String testYn;
    
    private MultipartFile uploadFile;
    
	private String orderColumn;
	private String sort; // 오름차순인지 내림차순인지	
	
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getBookSeq() {
		return bookSeq;
	}
	public void setBookSeq(String bookSeq) {
		this.bookSeq = bookSeq;
	}
	public String getContentSeq() {
		return contentSeq;
	}
	public void setContentSeq(String contentSeq) {
		this.contentSeq = contentSeq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBookNm() {
		return bookNm;
	}
	public void setBookNm(String bookNm) {
		this.bookNm = bookNm;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getLessonSeq() {
		return lessonSeq;
	}
	public void setLessonSeq(String lessonSeq) {
		this.lessonSeq = lessonSeq;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getTestYn() {
		return testYn;
	}
	public void setTestYn(String testYn) {
		this.testYn = testYn;
	}
	public MultipartFile getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(MultipartFile uploadFile) {
		this.uploadFile = uploadFile;
	}
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}


}
