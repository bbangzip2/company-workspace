package com.visangesl.treeadmin.lcms.packaging.vo;


public class PackagingDistCondition extends PackagingMbrId {

    private String contentSeq;
    private String distYn;


    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getDistYn() {
        return distYn;
    }
    public void setDistYn(String distYn) {
        this.distYn = distYn;
    }

}
