package com.visangesl.treeadmin.lcms.program.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.Program;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.lcms.program.service.LcmsProgramService;
import com.visangesl.treeadmin.lcms.program.vo.Item;
import com.visangesl.treeadmin.lcms.program.vo.ItemInfo;
import com.visangesl.treeadmin.lcms.program.vo.ItemInfoCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

/**
 * LcmsBookController
 * Version - 1.0
 * Copyright
 */
@Controller
public class LcmsProgramController implements BaseController {
    @Autowired
    private LcmsProgramService service;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final VSResult result = new VSResult();


    @RequestMapping(value = "/lcms/programList")
    public String getProgramList(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") int pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
            , @RequestParam(value = "sInsSeq", required = false, defaultValue = "") String sInsSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort		            
            ) throws Exception {

        	
        ItemInfoCondition itemInfo = new ItemInfoCondition();
        itemInfo.setInstituteSeq(sInsSeq);
        itemInfo.setPageNo(pageNo);
        itemInfo.setPageSize(pageSize);
        
        // 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
        int pageStartIndex = (itemInfo.getPageNo() - 1) * itemInfo.getPageSize();
        itemInfo.setPageStartIndex(pageStartIndex);
        
        itemInfo.setOrderColumn(TreeAdminUtil.isNull(orderBy, "modDate"));
        itemInfo.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        int totalCount = service.getItemDataListCnt(itemInfo);
        List<VSObject> programList = service.getItemDataList(itemInfo);

        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < programList.size()) {
            ItemInfo item = (ItemInfo) programList.get(i);
            item.setRn(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
        
		model.addAttribute("totalCount", totalCount);
		model.addAttribute("condition", itemInfo);
		model.addAttribute("programList", programList);
        

        return "/lcms/program/programList";
    }
    
    /**
     * 프로그램 등록 화면이동 
     * @param request
     * @param response
     * @param model
     * @param instituteSeq
     * @param instituteNm
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/program/programInsert")
    public String getProgramInsert(HttpServletRequest request, HttpServletResponse response, Model model,
    		@RequestParam(value = "instituteSeq", required = false, defaultValue = "") String instituteSeq,
    		@RequestParam(value = "instituteNm", required = false, defaultValue = "") String instituteNm
            ) throws Exception {

    	// 해당 기관이 사용할수 있는 아이템 목록 조회
    	
    	
    	model.addAttribute("instituteSeq", instituteSeq);
    	model.addAttribute("instituteNm", instituteNm);
        model.addAttribute("result", "");

        return "/lcms/program/programEdit";
    }
    
    /**
     * 프로그래 수정 화면 이동
     * @param request
     * @param response
     * @param model
     * @param instituteSeq
     * @param instituteNm
     * @param itemInfoSeq
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/program/programModify")
    public String getProgramModify(HttpServletRequest request, HttpServletResponse response, Model model
    		, @RequestParam(value = "instituteSeq", required = false, defaultValue = "") String instituteSeq
    		, @RequestParam(value = "instituteNm", required = false, defaultValue = "") String instituteNm
    		, @RequestParam(value = "itemInfoSeq", required = false, defaultValue = "") String itemInfoSeq
    		, @RequestParam(value = "pInsSeq", required = false, defaultValue = "") String pInsSeq
            ) throws Exception {

    	
    	ItemInfo itemInfo = new ItemInfo();
    	itemInfo.setItemInfoSeq(itemInfoSeq);
    	
    	// item info 기본 정보 조회 
    	ItemInfo result = (ItemInfo) service.getItemDataInfo(itemInfo); 
    	
    	
    	Item itemVo = new Item();
    	itemVo.setItemInfoSeq(itemInfoSeq);
    	
    	// 해당 기관이 사용할수 있는 아이템 목록 조회 --- > AJAX로 만들것이여.. 탭 선택하면 목록을 새로 그릴랑게..
    	// List<VSObject> treeItemList = service.getTreeItemList(itemVo);
    	
    	
    	ItemInfoCondition condition = new ItemInfoCondition();
    	condition.setInstituteSeq(instituteSeq);
    	
    	// 기관에  등록된 아이템 목록 조회
    	List<VSObject> insItemList = service.getInstituteItemList(condition);
    	
    	model.addAttribute("itemInfo", result);
    	model.addAttribute("instituteSeq", instituteSeq);
    	model.addAttribute("instituteNm", instituteNm);
    	model.addAttribute("pInsSeq", pInsSeq);
    	//model.addAttribute("treeItemList", treeItemList); // 트리의 모든  아이템 목록 (PT002 (book), PT007(self-study), PT008(social) ) 
    	model.addAttribute("insItemList", insItemList); // 기관에 등록된 아이템 목록

        return "/lcms/program/programEdit";
    }
    
    @RequestMapping(value = "/lcms/program/programView")
    public String programView(HttpServletRequest request, HttpServletResponse response, Model model
    		, @RequestParam(value = "instituteSeq", required = false, defaultValue = "") String instituteSeq
    		, @RequestParam(value = "instituteNm", required = false, defaultValue = "") String instituteNm
    		, @RequestParam(value = "itemInfoSeq", required = false, defaultValue = "") String itemInfoSeq
    		, @RequestParam(value = "pInsSeq", required = false, defaultValue = "") String pInsSeq
            ) throws Exception {

    	
    	ItemInfo itemInfo = new ItemInfo();
    	itemInfo.setItemInfoSeq(itemInfoSeq);
    	
    	// item info 기본 정보 조회 
    	ItemInfo result = (ItemInfo) service.getItemDataInfo(itemInfo); 
    	
    	
    	Item itemVo = new Item();
    	itemVo.setItemInfoSeq(itemInfoSeq);
    	
    	// 해당 기관이 사용할수 있는 아이템 목록 조회 --- > AJAX로 만들것이여.. 탭 선택하면 목록을 새로 그릴랑게..
    	// List<VSObject> treeItemList = service.getTreeItemList(itemVo);
    	
    	
    	ItemInfoCondition condition = new ItemInfoCondition();
    	condition.setInstituteSeq(instituteSeq);
    	
    	// 기관에  등록된 아이템 목록 조회
    	List<VSObject> insItemList = service.getInstituteItemList(condition);
    	
    	model.addAttribute("itemInfo", result);
    	model.addAttribute("instituteSeq", instituteSeq);
    	model.addAttribute("instituteNm", instituteNm);
    	model.addAttribute("pInsSeq", pInsSeq);
    	//model.addAttribute("treeItemList", treeItemList); // 트리의 모든  아이템 목록 (PT002 (book), PT007(self-study), PT008(social) ) 
    	model.addAttribute("insItemList", insItemList); // 기관에 등록된 아이템 목록

        return "/lcms/program/programView";
    }    
    
    /**
     * 기관에 아이템 추가 처리 : AJAX 
     * @param request
     * @param response
     * @param model
     * @param instituteSeq 기관 순번 
     * @param comment 커멘트 
     * @param useYn 사용여부 
     * @param prodVal 아이템 값 (array 형태임.)
     * @return
     * @throws Exception
     */
	@RequestMapping(value = "/lcms/itemInfoAdd", method = RequestMethod.POST)
	@ResponseBody
	public VSResult<VSObject> itemInfoAdd(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "instituteSeq", required = false) String instituteSeq
			, @RequestParam(value = "comment", required = false) String comment
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "prodVal", required = false) String[] prodVal
			
			) throws Exception {
		
		VSResult result = new VSResult();
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		
		String mbrId = usertDetail.getUsername(); //회원 아이디 
        
	        
		try{
			
			// itemInfo 기본 정보 등록 
	        ItemInfo itemInfo = new ItemInfo();
	        itemInfo.setInstituteSeq(instituteSeq);
	        itemInfo.setComment(comment);
	        itemInfo.setUseYn(useYn);
	        itemInfo.setModId(mbrId);
	        itemInfo.setRegId(mbrId);
	        
	        service.addItemInfoData(itemInfo, prodVal);
	        
			result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
		}catch(Exception e){
			logger.error(e.toString());
			result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));			
		}
        
		return result;
	}    
    
	/**
	 * 기관에 아이템 수정 처리 (AJAX)
	 * @param request
	 * @param response
	 * @param model
	 * @param comment : 커멘트 
	 * @param useYn : 사용여부 
	 * @param prodVal : 아이템 값 ( Array형태)
	 * @param itemInfoSeq : 아이템 정보의 순번 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/lcms/itemInfoModify", method = RequestMethod.POST)
	@ResponseBody
	public VSResult<VSObject> itemInfoModify(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "comment", required = false) String comment
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "prodVal", required = false) String[] prodVal
			, @RequestParam(value = "itemInfoSeq", required = false) String itemInfoSeq
			
			) throws Exception {
		
		VSResult result = new VSResult();
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		
		String mbrId = usertDetail.getUsername(); //회원 아이디 
        
	        
		try{
			
			// itemInfo 기본 정보 등록 
	        ItemInfo itemInfo = new ItemInfo();
	        itemInfo.setComment(comment);
	        itemInfo.setUseYn(useYn);
	        itemInfo.setModId(mbrId);
	        itemInfo.setRegId(mbrId);
	        itemInfo.setItemInfoSeq(itemInfoSeq);
	        
	        service.modifyItemInfoData(itemInfo, prodVal);
			result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
	        
		}catch(Exception e){
			logger.error(e.toString());
			result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
		}
		
		return result;
	}    
	
	
	
	/**
	 * 트리 전체에서 사용할수 있는 아이템 목록 조회 
	 * @param request
	 * @param response
	 * @param model
	 * @param itemType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/lcms/getTreeItemList")
	@ResponseBody
	public VSResult<Item> getTreeItemList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "itemType", required = false) String itemType) throws Exception {
		
		logger.debug("getTreeItemList");
		VSResult result = new VSResult();
		
	    try {
		
			Item itemVo = new Item();
			itemVo.setItemType(itemType);
			
			List<VSObject> treeItemList = service.getTreeItemList(itemVo);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(treeItemList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            logger.error(e.toString());
	    }
		
		return result;
	}
	
	
	@RequestMapping(value = "/lcms/getInstituteItemList")
	@ResponseBody
	public VSResult<Item> getInstituteItemList(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "instituteSeq", required = false) String instituteSeq
			, @RequestParam(value = "itemType", required = false) String itemType) throws Exception {
		
		logger.debug("getTreeItemList");
		VSResult result = new VSResult();
		
	    try {
		
			ItemInfoCondition condition = new ItemInfoCondition();
			condition.setInstituteSeq(instituteSeq);
			
			List<VSObject> insItemList = service.getInstituteItemList(condition);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(insItemList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            e.printStackTrace();
	    }
		
		return result;
	}
	
	
}
