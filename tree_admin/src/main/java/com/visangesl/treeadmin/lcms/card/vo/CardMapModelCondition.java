package com.visangesl.treeadmin.lcms.card.vo;

public class CardMapModelCondition extends CardTempCondition {

    private String mbrId;
    private String lessonTitle;


    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getLessonTitle() {
        return lessonTitle;
    }
    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }
}
