package com.visangesl.treeadmin.lcms.authoring.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.CardDao;
import com.visangesl.treeadmin.lcms.authoring.dao.AuthoringDao;
import com.visangesl.treeadmin.lcms.authoring.service.AuthoringService;
import com.visangesl.treeadmin.lcms.authoring.vo.AuthoringCondition;
import com.visangesl.treeadmin.lcms.authoring.vo.AuthoringVo;
import com.visangesl.treeadmin.lcms.card.vo.CardContentSeqVo;
import com.visangesl.treeadmin.lcms.card.vo.ContentStrucCondition;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class AuthoringServiceImpl implements AuthoringService {

    @Autowired
    AuthoringDao authoringDao;

    @Autowired
    CardDao cardDao;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public VSObject getData(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<VSObject> getDataList(AuthoringCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return authoringDao.getDataList(condition);
    }

    @Override
    public List<VSObject> getLessonList(AuthoringCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return authoringDao.getLessonList(condition);
    }

    @Override
    public int getDataListCnt(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public VSResult<VSObject> modifyDataWithResultCodeMsg(AuthoringVo authoringVo)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();
        int affectedRows = authoringDao.modifyDataWithResultCodeMsg(authoringVo);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }
        return resultCodeMsg;
    }

    @Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(AuthoringVo authoringVo)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult resultCodeMsg = new VSResult();


        // content_info 테이블의 next auto_increment 값을 조회
        String contentInfoNextSeq = cardDao.getContentInfoNextSeq();
        CardContentSeqVo CardContentSeqVo = new CardContentSeqVo();
        CardContentSeqVo.setContentSeq(contentInfoNextSeq);

        // content_info 테이블의 auto_increment 시작값 재지정
        cardDao.modifyContentInfoAutoIncrementVal(CardContentSeqVo);


        // 에디트 카드 등록
        authoringVo.setContentSeq(contentInfoNextSeq);
        int affectedRows = authoringDao.addDataWithResultCodeMsg(authoringVo);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {

            // 에디트 카드 구조 등록
            ContentStrucCondition contentStrucCondition = new ContentStrucCondition();
            logger.debug(authoringVo.getLessonCd());

            contentStrucCondition.setUpperContentSeq(authoringVo.getLessonCd());
            contentStrucCondition.setContentSeq(authoringVo.getContentSeq());
            contentStrucCondition.setMbrId(authoringVo.getMbrId());
            contentStrucCondition.setSortOrd("1");
            affectedRows = cardDao.addContentStrucTemp(contentStrucCondition);

            if (affectedRows < 1) {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            }
        }

        return resultCodeMsg;
    }

    @Override
    public VSResult<VSObject> deleteDataWithResultCodeMsg(
            ReferenceCondition referenceCondition) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }



}
