package com.visangesl.treeadmin.lcms.authoring.vo;

import com.visangesl.tree.vo.VSCondition;

public class AuthoringCondition extends VSCondition {

    private String bookCd;
    private String type;


    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }




}
