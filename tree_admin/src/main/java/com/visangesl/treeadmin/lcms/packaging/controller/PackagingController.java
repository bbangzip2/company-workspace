package com.visangesl.treeadmin.lcms.packaging.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.lcms.packaging.service.PackagingService;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingCondition;
import com.visangesl.treeadmin.lcms.packaging.vo.PackagingVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class PackagingController {

    @Autowired
    private PackagingService packagingService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * makeContentPackage
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/packaging/contentPackaging")
    public VSResult makeContentPackage(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd
            ) throws Exception {

        logger.debug("makeContentPackage");
        logger.debug("bookCd: " + bookCd);
        logger.debug("lessonCd: " + lessonCd);

        VSResult result = new VSResult();

        // 관리자 id, 등급 조회
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

        // 필수 파라미터 체크
        if (!bookCd.equals("") && !lessonCd.equals("")) {

            // 관리자 id, 등급 체크
            if (mbrId != null && (mbrGrade.equals(TreeProperties.getProperty("tree_superadmin")) || mbrGrade.equals(TreeProperties.getProperty("tree_contentadmin")))) {

                try {

                    PackagingCondition condition = new PackagingCondition();
                    condition.setLessonCd(lessonCd);
                    condition.setMbrId(mbrId);

//                    model.addAttribute("condition", condition);

                    // 패키징 진행 여부 체크
                    result = packagingService.checkMakeContentPackaging(condition);

                    // 패키징 대상 컨텐츠 리스트
                    List<PackagingVo> contentList  = (List<PackagingVo>) result.getResult();
                    String code = result.getCode() == null ? "" : result.getCode();


                    if (code.equals("ALL")) {
                        logger.debug("ALL");

                        // 패키지 파일 생성
                        result = packagingService.makeContentPackaging(mbrId, bookCd, lessonCd, contentList);

                        // 패키지 json 파일 생성
                        result = packagingService.makeOffLineContentPackaging(mbrId, bookCd, lessonCd, contentList);

                        if (!result.getCode().equals("0000")) {
                            result.setCode(TreeProperties.getProperty("error.fail.code"));
                        }

                    } else if (code.equals("OFF")) {
                        logger.debug("OFF");
                        result = packagingService.makeOffLineContentPackaging(mbrId, bookCd, lessonCd, contentList);

                    } else {
                        result.setCode(TreeProperties.getProperty("error.fail.code"));

                    }

                } catch (Exception e) {
                    ExceptionHandler handler = new ExceptionHandler(e);
                    e.printStackTrace();
                    result.setCode(TreeProperties.getProperty("error.fail.code"));
                }

            } else {
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage("권한 없음");
            }
        } else {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage("필수 파라미터 누락");
        }



        return result;
    }


    /**
     * releasePackaging
     * Version - 1.0
     * Copyright
     */
    @ResponseBody
    @RequestMapping(value = "/lcms/packaging/releasePackaging")
    public VSResult releasePackaging(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq
            ) throws Exception {

        logger.debug("releasePackaging");

        logger.debug("lessonCd: " + lessonCd);
        logger.debug("mbrId: " + mbrId);
        logger.debug("clsSeq: " + clsSeq);

        VSResult result = new VSResult();

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        mbrId = userDetail.getUsername();

        try {

            PackagingCondition condition = new PackagingCondition();
            condition.setLessonCd(lessonCd);
            condition.setMbrId(mbrId);

            if (!clsSeq.equals("")) {
                condition.setClsSeq(clsSeq);
            }

            model.addAttribute("condition", condition);

            // 패키징 배포 진행
            result = packagingService.releasePackaging(condition);

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }

        return result;
    }
}
