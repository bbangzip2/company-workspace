package com.visangesl.treeadmin.lcms.contents.vo;

import com.visangesl.treeadmin.vo.VSListCondition;

public class FetcherCondition extends VSListCondition {
	private String searchWord;
	private String orderColumn;
	private String sort;
	private String sortKey;
	

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}
	
	
}
