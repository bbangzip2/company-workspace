package com.visangesl.treeadmin.lcms.book.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.lcms.book.service.LcmsBookService;
import com.visangesl.treeadmin.lcms.book.vo.LcmsBookCondition;
import com.visangesl.treeadmin.lcms.book.vo.LcmsBookVo;
import com.visangesl.treeadmin.lcms.book.vo.LcmsLessonCondition;
import com.visangesl.treeadmin.lcms.book.vo.LcmsLessonVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

/**
 * LcmsBookController
 * Version - 1.0
 * Copyright
 */
@Controller
public class LcmsBookController implements BaseController {
    @Autowired
    private LcmsBookService lcmsBookService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final VSResult result = new VSResult();

    /**
     * getBookList
     * Version - 1.0
     * Copyright
     */
    @RequestMapping(value = "/lcms/bookList")
    public String getBookList(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
            , @RequestParam(value = "bookNm", required = false, defaultValue = "") String bookNm
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort		            
            ) throws Exception {

        logger.debug("/lcms/getBookList");
        
        logger.debug("bookNm==="+bookNm);
        
        
        LcmsBookCondition condition = new LcmsBookCondition();
        condition.setBookNm(bookNm);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
		

        List<VSObject> bookList = lcmsBookService.getDataList(condition);
        int totalCount =  lcmsBookService.getDataListCnt(condition);

        // 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
        int pageStartIndex = (condition.getPageNo() - 1) * condition.getPageSize();
        condition.setPageStartIndex(pageStartIndex);


		
		
        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < bookList.size()) {
            LcmsBookVo item = (LcmsBookVo) bookList.get(i);
            item.setRn(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
        model.addAttribute("bookList", bookList);
        model.addAttribute("totalCount", totalCount);
        model.addAttribute("condition", condition);

        return "/lcms/book/bookList";
    }

    /**
     * 책 등록 화면 이동
     * @param request
     * @param response
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/bookInfoInsert")
    public String bookInfoInsert(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
            , @RequestParam(value = "bookNm", required = false, defaultValue = "") String bookNm
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	    		
            ) throws Exception {

        LcmsBookCondition condition = new LcmsBookCondition();
        condition.setBookNm(bookNm);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);
        
        return "/lcms/book/bookEdit";
    }
    
    /**
     * 책 수정 화면 이동 
     * @param request
     * @param response
     * @param model
     * @param bookSeq
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/bookInfoEdit")
    public String bookInfoModify(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "bookSeq", required = true) String bookSeq
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize
            , @RequestParam(value = "bookNm", required = false, defaultValue = "") String bookNm
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	            
            ) throws Exception {

        logger.debug("getBookList");

        LcmsBookVo lcmsBookVo = new LcmsBookVo();
        lcmsBookVo.setBookSeq(bookSeq);
        lcmsBookVo = (LcmsBookVo) lcmsBookService.getData(lcmsBookVo);

        LcmsBookCondition condition = new LcmsBookCondition();
        condition.setBookNm(bookNm);
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);
        model.addAttribute("bookInfo", lcmsBookVo);

        return "/lcms/book/bookEdit";
    }
    
	/**
	 * 책 정보 등록 
	 * @param request
	 * @param response
	 * @param model
	 * @param bookTitle
	 * @param descr
	 * @param useYn
	 * @param mbrId
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/lcms/bookInfoAdd")
    @ResponseBody
    public VSResult<VSObject> bookInfoAdd(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "bookTitle", required = false) String bookTitle,
            @RequestParam(value = "descr", required = false) String descr,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
        logger.debug("bookInfoAdd");
        
        VSResult result = new VSResult();
        LcmsBookVo bookVo = new LcmsBookVo();
        
        //bookVo.setBookSeq(bookSeq);
        
        bookVo.setBookTitle(bookTitle);
        bookVo.setDescr(descr);
        bookVo.setRegId(mbrId);
        bookVo.setUseYn(useYn);
        bookVo.setType("PT002");
        
        
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
        	bookVo.setUploadFile(multipartFile);
        }
        
        VSResult<VSObject> resultCodeMsg =  lcmsBookService.addDataWithResultCodeMsg(bookVo);
        
        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            
		    result.setCode(TreeProperties.getProperty("error.success.code"));
		    // return message값에 bookseq값을 넣어준다. 
            result.setMessage(bookVo.getBookSeq());
            
        } else {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
        }
        return result;
    }
    
    /**
     * 책 정보 수정 처리 
     * @param request
     * @param response
     * @param model
     * @param bookSeq
     * @param bookTitle
     * @param descr
     * @param useYn
     * @param mbrId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/bookInfoModify")
    @ResponseBody
    public VSResult<VSObject> bookInfoModify(HttpServletRequest request, HttpServletResponse response, Model model,
    		@RequestParam(value = "bookSeq", required = false) String bookSeq,
    		@RequestParam(value = "bookTitle", required = false) String bookTitle,
            @RequestParam(value = "descr", required = false) String descr,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
        logger.debug("bookInfoAdd");
        
        VSResult result = new VSResult();
        LcmsBookVo bookVo = new LcmsBookVo();
        
        bookVo.setBookSeq(bookSeq);
        bookVo.setBookTitle(bookTitle);
        bookVo.setDescr(descr);
        bookVo.setRegId(mbrId);
        bookVo.setUseYn(useYn);
        
        
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
        	bookVo.setUploadFile(multipartFile);
        }
        
        VSResult<VSObject> resultCodeMsg =  lcmsBookService.modifyDataWithResultCodeMsg(bookVo);
        
        
        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            
		    result.setCode(TreeProperties.getProperty("error.success.code"));
		    // return message값에 bookseq값을 넣어준다. 
            result.setMessage(bookSeq);
        } else {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
        }
        return result;
    }
    
    /**
     * 레슨 타이틀 화면 이동 
     * @param request
     * @param response
     * @param model
     * @param bookSeq
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/lessonInfoEdit")
    public String lessonInfoEdit(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "bookSeq", required = true) String bookSeq
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "bookNm", required = false, defaultValue = "") String bookNm
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	            
            ) throws Exception {

        logger.debug("lessonInfoEdit");

        LcmsBookVo lcmsBookVo = new LcmsBookVo();
        lcmsBookVo.setBookSeq(bookSeq);
        lcmsBookVo = (LcmsBookVo) lcmsBookService.getData(lcmsBookVo);
        
        
        List<VSObject> lessonList = lcmsBookService.getLessonDataList(lcmsBookVo);
        
        LcmsBookCondition condition = new LcmsBookCondition();
        condition.setBookNm(bookNm);
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);
        
        model.addAttribute("bookInfo", lcmsBookVo);
        model.addAttribute("lessonList", lessonList);

        return "/lcms/book/lessonEdit";
    }
    
    /**
     * 레슨 타이틀 등록 , 수정 처리 
     * @param request
     * @param response
     * @param model
     * @param bookSeq
     * @param lessonTitleArr
     * @param lessonSeqArr
     * @param delSeqArr
     * @param mbrId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/lessonInfoModify")
    @ResponseBody
    public VSResult<VSObject> lessonInfoModify(HttpServletRequest request, HttpServletResponse response, Model model,
    		@RequestParam(value = "bookSeq", required = false) String bookSeq,
    		@RequestParam(value = "lessonTitle", required = false) String[] lessonTitleArr,
            @RequestParam(value = "lessonSeq", required = false) String[] lessonSeqArr,
            @RequestParam(value = "delSeq", required = false) String[] delSeqArr,
            @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
        logger.debug("bookInfoAdd");
        
        VSResult result = new VSResult();
        List<LcmsLessonVo> updateList = new ArrayList<LcmsLessonVo>();
        List<LcmsLessonVo> insertList = new ArrayList<LcmsLessonVo>();
        List<LcmsLessonVo> deleteList = new ArrayList<LcmsLessonVo>();
        

        // update or  inset  List make
        if( lessonTitleArr != null && lessonTitleArr.length > 0){
            for(int i=0; i < lessonTitleArr.length; i++){
            	
            	// 화면에서 넘어온 목록 순서에 따라서 정렬 순서를 정해준다. 
            	String sortOrder = Integer.toString(i+1);
            	
            	String lessonSeq = lessonSeqArr[i];
            	String lessonTitle = lessonTitleArr[i];
            	logger.debug("[lessonSeq] "+lessonSeq+" / [lessonTitle] "+lessonTitle);
            	
            	LcmsLessonVo updateVo = new LcmsLessonVo();
            	LcmsLessonVo insertVo = new LcmsLessonVo();
            	
            	if(lessonSeq.equals("") || lessonSeq == null){
            		insertVo.setType("PT003");
            		insertVo.setBookSeq(bookSeq);
            		insertVo.setLessonSeq(lessonSeq);
            		insertVo.setLessonTitle(lessonTitle);
            		insertVo.setUseYn("Y");
            		insertVo.setRegId(mbrId);
            		insertVo.setModId(mbrId);
            		insertVo.setSort(sortOrder);
            		insertVo.setSort(Integer.toString(i+1));
            		
            		insertList.add(insertVo);
            		
            	}else{
            		updateVo.setBookSeq(bookSeq);
            		updateVo.setLessonSeq(lessonSeq);
            		updateVo.setLessonTitle(lessonTitle);
            		updateVo.setUseYn("Y");
            		updateVo.setModId(mbrId);
            		updateVo.setSort(Integer.toString(i+1));
            		updateVo.setSort(sortOrder);
            		updateList.add(updateVo);
            	}
            	
            	
            }
        }

        // 삭제할 레슨을 객체 에 담는다. 
        if( delSeqArr != null && delSeqArr.length > 0){
        	for(int h=0; h < delSeqArr.length; h++){
        		
        		LcmsLessonVo deleteVo = new LcmsLessonVo();
        		
        		logger.debug("[delSeq] "+delSeqArr[h]);
        		
        		deleteVo.setBookSeq(bookSeq);
        		deleteVo.setLessonSeq(delSeqArr[h]);
        		deleteVo.setModId(mbrId);
        		
        		deleteList.add(deleteVo);
        		
        		
        	}
        }
        
       try{
    	   
    	   lcmsBookService.modifyLessonData( insertList, updateList, deleteList);
    	   
		   result.setCode(TreeProperties.getProperty("error.success.code"));
		   // return message값에 bookseq값을 넣어준다. 
           result.setMessage(bookSeq);
       }catch(Exception e){
           result.setCode(TreeProperties.getProperty("error.fail.code"));
           result.setMessage(TreeProperties.getProperty("error.fail.msg"));
           logger.error(e.toString());
       }
        return result;

    }
    
    
    /**
     * book > lesson list & info 
     * @param request
     * @param response
     * @param model
     * @param bookSeq
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/lessonList")
    public String lessonInfoList(HttpServletRequest request, HttpServletResponse response, Model model
            , @RequestParam(value = "bookSeq", required = true) String bookSeq
            , @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo
            , @RequestParam(value = "bookNm", required = false, defaultValue = "") String bookNm
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort	 
            ) throws Exception {

        logger.debug("lessonList");

        LcmsBookVo lcmsBookVo = new LcmsBookVo();
        lcmsBookVo.setBookSeq(bookSeq);
        lcmsBookVo = (LcmsBookVo) lcmsBookService.getData(lcmsBookVo);
        
        
        List<VSObject> lessonList = lcmsBookService.getLessonInfoList(lcmsBookVo);
        
        LcmsBookCondition condition = new LcmsBookCondition();
        condition.setBookNm(bookNm);
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
        condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "regDate"));
        condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
        
        model.addAttribute("condition", condition);

        model.addAttribute("bookInfo", lcmsBookVo);
        model.addAttribute("lessonList", lessonList);

        return "/lcms/book/lessonList";
    }
    
    /**
     * book > lesson list & info popup view
     * @param request
     * @param response
     * @param model
     * @param lessonSeq
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/lcms/lessonInfoPop")
    public String lessonInfoPop(HttpServletRequest request, HttpServletResponse response, Model model
    		, @RequestParam(value = "lessonSeq", required = true) String lessonSeq
            
            ) throws Exception {

        logger.debug("lessonList");

        LcmsLessonCondition condition = new LcmsLessonCondition();
        condition.setLessonSeq(lessonSeq);
        
        LcmsLessonVo lessonInfo = (LcmsLessonVo) lcmsBookService.getLessonData(condition);

        model.addAttribute("lessonInfo", lessonInfo);

        return "/lcms/book/lessonInfoPop";
    }
    
    @RequestMapping(value = "/lcms/lessonInfoUpdate")
    @ResponseBody
    public VSResult<VSObject> lessonInfoUpdate(HttpServletRequest request, HttpServletResponse response, Model model,
    		@RequestParam(value = "lessonSeq", required = false) String lessonSeq,
            @RequestParam(value = "descr", required = false) String descr,
            @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
        logger.debug("lessonInfoModify");
        
        VSResult result = new VSResult();
        LcmsLessonVo lessonVo = new LcmsLessonVo();
        
        lessonVo.setLessonSeq(lessonSeq);
        lessonVo.setModId(mbrId);
        lessonVo.setLessonDescr(descr);
        
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
        	lessonVo.setUploadFile(multipartFile);
        }
        
        VSResult<VSObject> resultCodeMsg =  lcmsBookService.modifyLessonInfo(lessonVo);
        
        
        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            
		    result.setCode(TreeProperties.getProperty("error.success.code"));
	        result.setMessage(TreeProperties.getProperty("error.success.msg"));
            
//            ModelAndView mav = new ModelAndView();
//            mav.setViewName("redirect:/lcms/lessonInfoPop.do?lessonSeq="+lessonSeq);
//            return mav;
        } else {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
        }
        return result;
    }
    
    
    
}
