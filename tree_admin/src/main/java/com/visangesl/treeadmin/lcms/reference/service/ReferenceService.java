package com.visangesl.treeadmin.lcms.reference.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceCondition;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface ReferenceService {
    // select One Data
    public VSObject getData(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getDataList(VSObject vsObject) throws Exception;

    // select List Count
    public int getDataListCnt(VSObject vsObject) throws Exception;

    // Update
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // Insert
    public VSResult<VSObject> addDataWithResultCodeMsg(ReferenceVo ReferenceVo) throws Exception;

    // Delete
    public VSResult<VSObject> deleteDataWithResultCodeMsg(ReferenceCondition referenceCondition) throws Exception;

    // select List Data
    public List<VSObject> getDayNoCdList(String lessonCd) throws Exception;

    public VSResult<VSObject> updateReferenceFileUseYn(List<ReferenceVo> referenceTempList) throws Exception;

    public List<ReferenceVo> getReferenceTempList(ReferenceCondition condition) throws Exception;

    public VSResult<VSObject> deleteReference(ReferenceCondition referenceCondition) throws Exception;

}
