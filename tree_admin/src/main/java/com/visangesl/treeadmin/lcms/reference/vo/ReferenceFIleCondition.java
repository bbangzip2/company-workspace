package com.visangesl.treeadmin.lcms.reference.vo;


public class ReferenceFIleCondition extends ReferenceCondition {

    private String filePath;
    private String fileSize;


    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getFileSize() {
        return fileSize;
    }
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
}
