package com.visangesl.treeadmin.lcms.contents.vo;

import com.visangesl.tree.vo.VSObject;


/**
 * 저작도구 생성 카드를 저장하기 위한 정보를 담는 VO
 * @author user
 *
 */
public class FetcherCardVo extends VSObject {

    /**
     * 컨텐츠 코드
     */
    private String contentSeq;
    /**
     * 저작도구 관리 순번
     */
    private String pid;
    /**
     * 컨텐츠 타입 - 북/레슨/차시/카드
     */
    private String type;
    /**
     * 카드 구분
     */
    private String cardSect;
    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 컨텐츠 타이틀
     */
    private String prodTitle;
    /**
     * 썸네일 이미지 경로
     */
    private String thmbPath;
    /**
     * 컨텐츠 경로
     */
    private String filePath;
    /**
     * 카드 타입
     */
    private String cardType;
    /**
     * 카드 스킬
     */
    private String cardSkill;
    /**
     * 학습 모드
     */
    private String studyMode;
    /**
     * 카드 레벨
     */
    private String cardLevel;
    /**
     * 카드 시간
     */
    private String time;
    /**
     * 수정 가능 여부
     */
    private String updAbleYn;
    /**
     * 공유 가능 여부
     */
    private String openAbleYn;
    /**
     * 키워드
     */
    private String keyword;
    /**
     * 점수 산정 방식
     */
    private String grading;
    /**
     * 상세 설명 언어1
     */
    private String direcLang1;
    /**
     * 상세 설명1
     */
    private String direc1;
    /**
     * 상세 설명 언어2
     */
    private String direcLang2;
    /**
     * 상세 설명2
     */
    private String direc2;
    /**
     * 사용 여부
     */
    private String useYn;
    /**
     * 배포 여부
     */
    private String distYn;
    /**
     * 테스트 카드 여부  
     */
    private String testYn;
    /**
     * 상세설명 1 코드명  
     */
    private String direcLang1Nm;
    /**
     * 상세설명 2 코드명  
     */
    private String direcLang2Nm;
    
    private String regDate;

    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getThmbPath() {
        return thmbPath;
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getStudyMode() {
        return studyMode;
    }
    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public String getUpdAbleYn() {
		return updAbleYn;
	}
	public void setUpdAbleYn(String updAbleYn) {
		this.updAbleYn = updAbleYn;
	}
	public String getOpenAbleYn() {
		return openAbleYn;
	}
	public void setOpenAbleYn(String openAbleYn) {
		this.openAbleYn = openAbleYn;
	}
	public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getGrading() {
        return grading;
    }
    public void setGrading(String grading) {
        this.grading = grading;
    }
    public String getDirecLang1() {
        return direcLang1;
    }
    public void setDirecLang1(String direcLang1) {
        this.direcLang1 = direcLang1;
    }
    public String getDirec1() {
        return direc1;
    }
    public void setDirec1(String direc1) {
        this.direc1 = direc1;
    }
    public String getDirecLang2() {
        return direcLang2;
    }
    public void setDirecLang2(String direcLang2) {
        this.direcLang2 = direcLang2;
    }
    public String getDirec2() {
        return direc2;
    }
    public void setDirec2(String direc2) {
        this.direc2 = direc2;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getDistYn() {
        return distYn;
    }
    public void setDistYn(String distYn) {
        this.distYn = distYn;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
	public String getTestYn() {
		return testYn;
	}
	public void setTestYn(String testYn) {
		this.testYn = testYn;
	}
	public String getDirecLang1Nm() {
		return direcLang1Nm;
	}
	public void setDirecLang1Nm(String direcLang1Nm) {
		this.direcLang1Nm = direcLang1Nm;
	}
	public String getDirecLang2Nm() {
		return direcLang2Nm;
	}
	public void setDirecLang2Nm(String direcLang2Nm) {
		this.direcLang2Nm = direcLang2Nm;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

}
