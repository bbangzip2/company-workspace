package com.visangesl.treeadmin.lcms.card.vo;

public class CardSectCondition extends CardTempCondition {

    private String cardSect;
    private String contentSeq;

    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
}
