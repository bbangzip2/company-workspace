package com.visangesl.treeadmin.lcms.contents.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

@Service
public interface FetcherService{

	/**
	 * content fetcher list 조회 
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
    public List<VSObject> getFetcherList(VSCondition dataCondition) throws Exception;
    
    /**
     * content fetcher list count 조회 
     * @param dataCondition
     * @return
     * @throws Exception
     */
    public int getFetcherListCnt(VSCondition dataCondition) throws Exception;
    
    /**
     * content_info book list 조회 
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentBookList() throws Exception;
    
    /**
     * content_info lesson list 조회 
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentLessonList(String bookCd) throws Exception;
    
    
    /**
     * 카드 상세 정보 조회 
     * @param contentSeq
     * @return
     * @throws Exception
     */
    public VSObject getFecherCardInfo(String contentSeq) throws Exception;
    
    

}
