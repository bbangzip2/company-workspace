package com.visangesl.treeadmin.lcms.packaging.vo;

import com.visangesl.tree.vo.VSCondition;

public class PackagingVo extends VSCondition {

    private String bookCd;
    private String lessonCd;
    private String dayNoCd;
    private String cardCd;
    private String sortOrd;
    private String cardSect;
    private String filePath;

    private String bookPath;
    private String lessonPath;
    private String cardPath;

    private String contentSeq;
    private String title;
    private String detailDescr;
    private String cardType;
    private String cardSkill;
    private String studyMode;
    private String cardLevel;
    private String time;
    private String updAbleYn;
    private String openAbleYn;
    private String keyword;
    private String grading;


    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
    public String getCardCd() {
        return cardCd;
    }
    public void setCardCd(String cardCd) {
        this.cardCd = cardCd;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getBookPath() {
        return bookPath;
    }
    public void setBookPath(String bookPath) {
        this.bookPath = bookPath;
    }
    public String getLessonPath() {
        return lessonPath;
    }
    public void setLessonPath(String lessonPath) {
        this.lessonPath = lessonPath;
    }
    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDetailDescr() {
        return detailDescr;
    }
    public void setDetailDescr(String detailDescr) {
        this.detailDescr = detailDescr;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getStudyMode() {
        return studyMode;
    }
    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getUpdAbleYn() {
        return updAbleYn;
    }
    public void setUpdAbleYn(String updAbleYn) {
        this.updAbleYn = updAbleYn;
    }
    public String getOpenAbleYn() {
        return openAbleYn;
    }
    public void setOpenAbleYn(String openAbleYn) {
        this.openAbleYn = openAbleYn;
    }
    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getGrading() {
        return grading;
    }
    public void setGrading(String grading) {
        this.grading = grading;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
}
