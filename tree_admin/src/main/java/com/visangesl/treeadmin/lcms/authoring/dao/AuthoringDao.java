package com.visangesl.treeadmin.lcms.authoring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.lcms.authoring.vo.AuthoringVo;

@Repository
public class AuthoringDao extends TreeSqlSessionDaoSupport {

    public VSObject getData(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("authoring.getData", vsObject);
        return result;
    }

    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("authoring.getDataList", dataCondition);
        return result;
    }

    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
            result = getSqlSession().update("authoring.modifyDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int addDataWithResultCodeMsg(AuthoringVo authoringVo) throws Exception {
        int result = 0;
            result = getSqlSession().insert("authoring.addDataWithResultCodeMsg", authoringVo);
        return result;
    }

    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
            result = getSqlSession().delete("authoring.deleteDataWithResultCodeMsg", vsObject);
        return result;
    }

    public List<VSObject> getBookList(VSCondition dataCondition) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("authoring.getBookList", dataCondition);
        return result;
    }

    public List<VSObject> getLessonList(VSCondition dataCondition) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("authoring.getLessonList", dataCondition);
        return result;
    }


}
