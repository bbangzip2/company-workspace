package com.visangesl.treeadmin.lcms.packaging.vo;

public class PackagingEntryVo {

    private String basePath;
    private String contentPath;
    private String contentEntryPath;
    private String contentFullPath;

    public String getContentPath() {
        return contentPath;
    }
    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }
    public String getContentFullPath() {
        return contentFullPath;
    }
    public void setContentFullPath(String contentFullPath) {
        this.contentFullPath = contentFullPath;
    }
    public String getBasePath() {
        return basePath;
    }
    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
    public String getContentEntryPath() {
        return contentEntryPath;
    }
    public void setContentEntryPath(String contentEntryPath) {
        this.contentEntryPath = contentEntryPath;
    }

}
