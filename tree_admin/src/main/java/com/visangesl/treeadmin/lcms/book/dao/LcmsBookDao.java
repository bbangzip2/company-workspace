package com.visangesl.treeadmin.lcms.book.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.lcms.book.vo.LcmsBookVo;
import com.visangesl.treeadmin.lcms.book.vo.LcmsLessonVo;

@Repository
public class LcmsBookDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getData(VSObject vsObject) throws Exception {
    	LcmsBookVo result = null;
        result = getSqlSession().selectOne("lcmsbook.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("lcmsbook.getDataList", dataCondition);
    	return result;
    }
    
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
    	
    	return getSqlSession().selectOne("lcmsbook.getDataListCnt", dataCondition);
    }
    
    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("lcmsbook.modifyDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("lcmsbook.addDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("lcmsbook.deleteDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addDataContentStruc(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("lcmsbook.addDataContentStruc", vsObject);
		return result;
	}
    
    public int modifyDataContentStruc(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("lcmsbook.modifyDataContentStruc", vsObject);
		return result;
	}
    
    public int deleteDataContentStruc(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("lcmsbook.deleteDataContentStruc", vsObject);
		return result;
	}
    
    
    public List<VSObject> getLessonDataList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("lcmsbook.getLessonDataList", vsObject);
    	return result;
    }
    
    public List<VSObject> getLessonInfoList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("lcmsbook.getLessonInfoList", vsObject);
    	return result;
    }
    
    public int addLessonData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("lcmsbook.addLessonData", vsObject);
		return result;
	}
    
    public int modifyLessonData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("lcmsbook.modifyLessonData", vsObject);
		return result;
	}
    
    public int deleteLessonData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("lcmsbook.deleteLessonData", vsObject);
		return result;
	}    
    
    public VSObject getLessonData(VSCondition dataCondition) throws Exception {
    	LcmsLessonVo result = null;
        result = getSqlSession().selectOne("lcmsbook.getLessonData", dataCondition);
    	return result;
    }
    
    public int modifyLessonInfo(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("lcmsbook.modifyLessonInfo", vsObject);
		return result;
	}
    
    
}
