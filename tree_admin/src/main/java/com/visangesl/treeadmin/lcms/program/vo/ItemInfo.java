package com.visangesl.treeadmin.lcms.program.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public class ItemInfo extends VSObject {

	private String rn;
	private String itemInfoSeq;
	private String instituteSeq;
	private String instituteNm;
	private String comment;
	private String useYn;
	private String regId;
	private String regDate;
	private String modId;
	private String modDate;
	private List<Item> itemList;
	
	
	
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public List<Item> getItemList() {
		return itemList;
	}
	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	public String getItemInfoSeq() {
		return itemInfoSeq;
	}
	public void setItemInfoSeq(String itemInfoSeq) {
		this.itemInfoSeq = itemInfoSeq;
	}
	
	public String getInstituteNm() {
		return instituteNm;
	}
	public void setInstituteNm(String instituteNm) {
		this.instituteNm = instituteNm;
	}
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	
	
}
