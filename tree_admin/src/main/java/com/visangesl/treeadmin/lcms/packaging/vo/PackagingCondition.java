package com.visangesl.treeadmin.lcms.packaging.vo;


public class PackagingCondition extends PackagingMbrId {

    private String lessonCd;
    private String mbrId;
    private String clsSeq;


    public String getLessonCd() {
        return lessonCd;
    }

    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }

    @Override
    public String getMbrId() {
        return mbrId;
    }

    @Override
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

    public String getClsSeq() {
        return clsSeq;
    }

    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }

}
