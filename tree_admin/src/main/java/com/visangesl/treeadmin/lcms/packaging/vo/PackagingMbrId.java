package com.visangesl.treeadmin.lcms.packaging.vo;

import com.visangesl.tree.vo.VSCondition;

public class PackagingMbrId extends VSCondition {

    private String mbrId;

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

}
