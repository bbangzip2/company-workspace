package com.visangesl.treeadmin.lcms.card.vo;

public class CardMapDayNoCardListCondition extends CardTempCondition {

    private String sortOrd;

    public String getSortOrd() {
        return sortOrd;
    }

    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
}
