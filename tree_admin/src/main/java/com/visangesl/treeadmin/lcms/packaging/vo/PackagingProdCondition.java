package com.visangesl.treeadmin.lcms.packaging.vo;

import com.visangesl.tree.vo.VSCondition;

public class PackagingProdCondition extends VSCondition {

    private String lessonCd;


    public String getLessonCd() {
        return lessonCd;
    }

    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }

}
