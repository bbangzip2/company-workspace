package com.visangesl.treeadmin.lcms.card.vo;

import com.visangesl.tree.vo.VSObject;

public class CardContentSeqVo extends VSObject {

    private String contentSeq;

    public String getContentSeq() {
        return contentSeq;
    }

    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
}
