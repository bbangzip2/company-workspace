package com.visangesl.treeadmin.lcms.book.vo;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class LcmsLessonVo extends VSObject {
	private String rn;
	private String bookSeq;
	private String bookTitle;
    private String lessonSeq;
    private String lessonTitle;
    private String lessonDescr;
    private String type;
    private String useYn;
    private String regDate;
    private String regId;
    private String modDate;
    private String modId;
    private String thmbPath;
    private String sort;
    
    private MultipartFile uploadFile;
    
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	
	public String getBookSeq() {
		return bookSeq;
	}
	public void setBookSeq(String bookSeq) {
		this.bookSeq = bookSeq;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getLessonSeq() {
		return lessonSeq;
	}
	public void setLessonSeq(String lessonSeq) {
		this.lessonSeq = lessonSeq;
	}
	public String getLessonTitle() {
		return lessonTitle;
	}
	public void setLessonTitle(String lessonTitle) {
		this.lessonTitle = lessonTitle;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getThmbPath() {
		return thmbPath;
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public MultipartFile getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(MultipartFile uploadFile) {
		this.uploadFile = uploadFile;
	}
	public String getLessonDescr() {
		return lessonDescr;
	}
	public void setLessonDescr(String lessonDescr) {
		this.lessonDescr = lessonDescr;
	}

	

}
