package com.visangesl.treeadmin.lcms.book.vo;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class LcmsBookVo extends VSObject {
	private String rn;
    private String bookSeq;
    private String type;
    private String bookTitle;
    private String thmbPath;
    private String useYn;
    private String regDate;
    private String regId;
    private String modDate;
    private String modId;
    private int lessonCnt;
    private int dayNoCnt;
    private int actCnt;
    private String descr;
    
    private MultipartFile uploadFile;
    
	private String orderColumn;
	private String sort; // 오름차순인지 내림차순인지	
	
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getBookSeq() {
		return bookSeq;
	}
	public void setBookSeq(String bookSeq) {
		this.bookSeq = bookSeq;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getThmbPath() {
		return thmbPath;
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public int getLessonCnt() {
		return lessonCnt;
	}
	public void setLessonCnt(int lessonCnt) {
		this.lessonCnt = lessonCnt;
	}
	public int getDayNoCnt() {
		return dayNoCnt;
	}
	public void setDayNoCnt(int dayNoCnt) {
		this.dayNoCnt = dayNoCnt;
	}
	public int getActCnt() {
		return actCnt;
	}
	public void setActCnt(int actCnt) {
		this.actCnt = actCnt;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public MultipartFile getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(MultipartFile uploadFile) {
		this.uploadFile = uploadFile;
	}
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}


}
