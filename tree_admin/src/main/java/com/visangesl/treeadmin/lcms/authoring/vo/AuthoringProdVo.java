package com.visangesl.treeadmin.lcms.authoring.vo;

import com.visangesl.tree.vo.VSObject;

public class AuthoringProdVo extends VSObject {

    private String prodTitle;
    private String bookCd;
    private String lessonCd;


    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
}
