package com.visangesl.treeadmin.lcms.card.vo;


public class CardStrucCondition extends CardMapDayNoCardListCondition {

    private String mbrId;
    private String cardCdList;

    public String getCardCdList() {
        return cardCdList;
    }

    public void setCardCdList(String cardCdList) {
        this.cardCdList = cardCdList;
    }

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }




}
