package com.visangesl.treeadmin.lcms.reference.vo;

import com.visangesl.treeadmin.vo.VSListCondition;

public class ReferenceCondition extends VSListCondition {

    private String lessonCd;
    private String lessonTitle;
    private String referenceSeq;
    private String dayNoCd;
    private String type;
    private String fileNm;
    private String filePath;


    public String getReferenceSeq() {
        return referenceSeq;
    }
    public void setReferenceSeq(String referenceSeq) {
        this.referenceSeq = referenceSeq;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getFileNm() {
        return fileNm;
    }
    public void setFileNm(String fileNm) {
        this.fileNm = fileNm;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getLessonTitle() {
        return lessonTitle;
    }
    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }

}
