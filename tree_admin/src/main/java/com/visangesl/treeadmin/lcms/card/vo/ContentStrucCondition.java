package com.visangesl.treeadmin.lcms.card.vo;


public class ContentStrucCondition extends CardTempCondition {

    private String mbrId;
    private String upperContentSeq;
    private String contentSeq;
    private String sortOrd;
    private String type;
    private String strucSeq;

    @Override
    public String getMbrId() {
        return mbrId;
    }
    @Override
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getUpperContentSeq() {
        return upperContentSeq;
    }
    public void setUpperContentSeq(String upperContentSeq) {
        this.upperContentSeq = upperContentSeq;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getStrucSeq() {
        return strucSeq;
    }
    public void setStrucSeq(String strucSeq) {
        this.strucSeq = strucSeq;
    }


}
