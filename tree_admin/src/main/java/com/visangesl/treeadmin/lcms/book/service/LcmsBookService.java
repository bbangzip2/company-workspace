package com.visangesl.treeadmin.lcms.book.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.book.vo.LcmsLessonVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface LcmsBookService{
    // select One Data
    public VSObject getData(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;

    // select List Count
    public int getDataListCnt(VSCondition dataCondition) throws Exception;

    // Update
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // Insert
    public VSResult<VSObject> addDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // Delete
    public VSResult<VSObject> deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;
    
    // getLessonList
    public List<VSObject> getLessonDataList(VSObject vsObject) throws Exception;
    
    // book > lesson List & info list 
    public List<VSObject> getLessonInfoList(VSObject vsObject) throws Exception;
    
    // insert update delete lessonList
    public int modifyLessonData(List<LcmsLessonVo> insertList, List<LcmsLessonVo> updateList, List<LcmsLessonVo> deleteList)throws Exception;
    
    public VSObject getLessonData(VSCondition dataCondition) throws Exception;
    
    public VSResult<VSObject> modifyLessonInfo(VSObject vsObject) throws Exception;
}
