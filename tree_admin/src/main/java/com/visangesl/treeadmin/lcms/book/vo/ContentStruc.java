package com.visangesl.treeadmin.lcms.book.vo;

import com.visangesl.tree.vo.VSObject;

public class ContentStruc extends VSObject {
	private String mbrId;
    private String upperContentSeq;
    private String contentSeq;
    private String sortOrd;
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getUpperContentSeq() {
		return upperContentSeq;
	}
	public void setUpperContentSeq(String upperContentSeq) {
		this.upperContentSeq = upperContentSeq;
	}
	public String getContentSeq() {
		return contentSeq;
	}
	public void setContentSeq(String contentSeq) {
		this.contentSeq = contentSeq;
	}
	public String getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String sortOrd) {
		this.sortOrd = sortOrd;
	}

    

}
