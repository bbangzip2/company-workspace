package com.visangesl.treeadmin.vo;

import com.visangesl.tree.vo.VSCondition;


public class VSListCondition extends VSCondition {
	private int pageNo;
	private int pageSize;
	private int pageStartIndex; // paging시 사용하는 변수

	private String listSortKey;
	private String listSort;

	public int getPageStartIndex() {
		return pageStartIndex;
	}
	public void setPageStartIndex(int pageStartIndex) {
		this.pageStartIndex = pageStartIndex;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
    public String getListSortKey() {
        return listSortKey;
    }
    public void setListSortKey(String listSortKey) {
        this.listSortKey = listSortKey;
    }
    public String getListSort() {
        return listSort;
    }
    public void setListSort(String listSort) {
        this.listSort = listSort;
    }


}
