package com.visangesl.treeadmin.vo;

import com.visangesl.tree.vo.VSObject;

public class IcmRelVo extends VSObject {

    private String icmSeq;
    private String relSeq;
    private String icmSect;
    private String relSect;
    
    
    private String cond_icmSeq;
    private String cond_relSeq;
    private String cond_icmSect;
    private String cond_relSect;


    public String getIcmSeq() {
        return icmSeq;
    }
    public void setIcmSeq(String icmSeq) {
        this.icmSeq = icmSeq;
    }
    public String getRelSeq() {
        return relSeq;
    }
    public void setRelSeq(String relSeq) {
        this.relSeq = relSeq;
    }
    public String getIcmSect() {
        return icmSect;
    }
    public void setIcmSect(String icmSect) {
        this.icmSect = icmSect;
    }
	public String getRelSect() {
		return relSect;
	}
	public void setRelSect(String relSect) {
		this.relSect = relSect;
	}
	public String getCond_icmSeq() {
		return cond_icmSeq;
	}
	public void setCond_icmSeq(String cond_icmSeq) {
		this.cond_icmSeq = cond_icmSeq;
	}
	public String getCond_relSeq() {
		return cond_relSeq;
	}
	public void setCond_relSeq(String cond_relSeq) {
		this.cond_relSeq = cond_relSeq;
	}
	public String getCond_icmSect() {
		return cond_icmSect;
	}
	public void setCond_icmSect(String cond_icmSect) {
		this.cond_icmSect = cond_icmSect;
	}
	public String getCond_relSect() {
		return cond_relSect;
	}
	public void setCond_relSect(String cond_relSect) {
		this.cond_relSect = cond_relSect;
	}
	
    
}
