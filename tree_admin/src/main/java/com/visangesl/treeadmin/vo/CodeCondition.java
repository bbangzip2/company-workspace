package com.visangesl.treeadmin.vo;

import com.visangesl.tree.vo.VSObject;

public class CodeCondition extends VSObject {
	private String relCd;
	private String insInfoSeq;
	private String campInfoSeq;
	
	public String getRelCd() {
		return relCd;
	}
	public void setRelCd(String relCd) {
		this.relCd = relCd;
	}
	public String getInsInfoSeq() {
		return insInfoSeq;
	}
	public void setInsInfoSeq(String insInfoSeq) {
		this.insInfoSeq = insInfoSeq;
	}
	public String getCampInfoSeq() {
		return campInfoSeq;
	}
	public void setCampInfoSeq(String campInfoSeq) {
		this.campInfoSeq = campInfoSeq;
	}
	
	

}
