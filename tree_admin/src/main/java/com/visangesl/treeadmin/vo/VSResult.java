package com.visangesl.treeadmin.vo;

import java.util.HashMap;

import com.visangesl.tree.vo.VSObject;


public class VSResult<T> extends VSObject {
	private String code;
	private String message;

    private T result;
    HashMap<String, String> paramMap;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }
    public void setResult(T result) {
        this.result = result;
    }
    public HashMap<String, String> getParamMap() {
        return paramMap;
    }

    public void setParamMap(HashMap<String, String> paramMap) {
        this.paramMap = paramMap;
    }
}
