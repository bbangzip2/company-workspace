package com.visangesl.treeadmin.member.vo;

import com.visangesl.tree.vo.VSObject;

public class MemberCondition extends VSObject {

    private String mbrId;
    private String mbrGrade;
    private String instituteSeq;
    private String campSeq;


    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

    public String getMbrGrade() {
        return mbrGrade;
    }

    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }

    public String getInstituteSeq() {
        return instituteSeq;
    }

    public void setInstituteSeq(String instituteSeq) {
        this.instituteSeq = instituteSeq;
    }

    public String getCampSeq() {
        return campSeq;
    }

    public void setCampSeq(String campSeq) {
        this.campSeq = campSeq;
    }

}
