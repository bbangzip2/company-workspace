package com.visangesl.treeadmin.member.vo;

import java.io.Serializable;

public class UserSession implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1552844010665234135L;

    private String memberId = null;
    private String memberName = null;
    private String validYn = null;
    private String schoolName = null;
    private String teacherYn = null;
    private String grinfoId = null;
    private String grinfoName = null;
    private String mainSubject = null;
    private String mainSubjectName = null;
    private String secondSubject = null;
    private String secondSubjectName = null;
    private String profileImgPath = null;
    private String useYn = null;
    private String name = null;
    private int chalkPt = 0;
    private String mbrGrade = null;
    private String instituteSeq = null;
    
    private String insSeq = null; // 기관 순번 

	
    private String nickname = null;

    public String getInstituteSeq() {
		return instituteSeq;
	}

	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}

	public String getMainSubject() {
		return mainSubject;
	}

	public void setMainSubject(String mainSubject) {
		this.mainSubject = mainSubject;
	}

	public String getMainSubjectName() {
		return mainSubjectName;
	}

	public void setMainSubjectName(String mainSubjectName) {
		this.mainSubjectName = mainSubjectName;
	}

	public String getSecondSubject() {
		return secondSubject;
	}

	public void setSecondSubject(String secondSubject) {
		this.secondSubject = secondSubject;
	}

	public String getSecondSubjectName() {
		return secondSubjectName;
	}

	public void setSecondSubjectName(String secondSubjectName) {
		this.secondSubjectName = secondSubjectName;
	}

	public String getGrinfoId() {
		return grinfoId;
	}

	public void setGrinfoId(String grinfoId) {
		this.grinfoId = grinfoId;
	}

	public String getGrinfoName() {
		return grinfoName;
	}

	public void setGrinfoName(String grinfoName) {
		this.grinfoName = grinfoName;
	}

	public String getTeacherYn() {
		return teacherYn;
	}

	public void setTeacherYn(String teacherYn) {
		this.teacherYn = teacherYn;
	}

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getValidYn() {
        return validYn;
    }

    public void setValidYn(String validYn) {
        this.validYn = validYn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public int getChalkPt() {
        return chalkPt;
    }

    public void setChalkPt(int chalkPt) {
        this.chalkPt = chalkPt;
    }

    public String getProfileImgPath() {
        return profileImgPath;
    }

    public void setProfileImgPath(String profileImgPath) {
        this.profileImgPath = profileImgPath;
    }

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMbrGrade() {
		return mbrGrade;
	}

	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}

	public String getInsSeq() {
		return insSeq;
	}

	public void setInsSeq(String insSeq) {
		this.insSeq = insSeq;
	}

}
