package com.visangesl.treeadmin.member.service;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.member.vo.UserSession;
import com.visangesl.treeadmin.vo.VSResult;

public interface MemberService {

    public UserSession signIn(String userId, String userPwd) throws Exception;

    // select One Data
    public VSObject getData(VSObject vsObject) throws Exception;

    // select List Data
    public List<VSObject> getDataList(VSObject vsObject) throws Exception;

    // select List Count - 해당 맴버 아디이의 갯수를 가져온다.
    public int getDataListCnt(VSObject vsObject) throws Exception;

    // Update
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // Insert
    public VSResult<VSObject> addDataWithResultCodeMsg(VSObject vsObject) throws Exception;

    // Delete
    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;
}
