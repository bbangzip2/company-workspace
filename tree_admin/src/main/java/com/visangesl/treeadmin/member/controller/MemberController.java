package com.visangesl.treeadmin.member.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.CampusService;
import com.visangesl.treeadmin.amg.service.InstituteService;
import com.visangesl.treeadmin.amg.vo.CampusCondition;
import com.visangesl.treeadmin.amg.vo.Institute;
import com.visangesl.treeadmin.amg.vo.InstituteCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.member.service.MemberService;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.member.vo.MemberCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class MemberController implements BaseController {

	@Autowired
	MemberService service;
	@Autowired
	InstituteService instituteService;
	@Autowired
	CampusService campusService;
    @Autowired
    private CommonService commonService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 트리 어드민 로그인 페이지 이동 처리
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/member/login", method = RequestMethod.GET)
    public String treeAdminLogin(HttpServletRequest request, Model model) throws Exception {

        return "/member/login";
    }

    /**
     * 로그인 처리
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    /*
    @RequestMapping(value = "/member/signInTreeAdmin", method = RequestMethod.POST)
    @ResponseBody
    public VSResult<UserSession> signInVivadmin(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

    	//String returnUrl = "/sample";
        logger.debug("===================================================");
        logger.debug("===   signInVivadmin                            ===");
        logger.debug("===================================================");

        VSResult result = new VSResult();

        String userId = request.getParameter("userId");
        String userPwd = request.getParameter("userPwd");

        logger.debug("userId  : " + userId);
        logger.debug("userPwd : " + userPwd);
        logger.debug("===================================================");

        UserSession userSession = null;

        try {

            userSession = service.signIn(userId, userPwd);

            result = new VSResult();

            if (userSession != null) {

                if (userSession.getUseYn() != null && userSession.getUseYn().equals("Y")) {
                    HttpSession session = request.getSession();

                    //AdmMemberMenuListCondition admMemberMenuListCondition = new AdmMemberMenuListCondition();
                    //admMemberMenuListCondition.setAdmId(userId);

                    //List<VSObject> menuList = menuService.getDataList(admMemberMenuListCondition);

                	session.setAttribute("isSignIn", "Y");
                    session.setAttribute("VSUserSession", userSession);
                    //session.setAttribute("VSMenuList", menuList);
                    HashMap tMap = new HashMap();
                    tMap.put("mbrGrade", userSession.getMbrGrade());
                    tMap.put("instituteSeq", "6");
                    result.setParamMap(tMap);

                 logger.debug("><><><><><><>< memberId="+userSession.getMemberId());
                 logger.debug("><><><><><><>< memberId="+userSession.getMbrGrade());
                 logger.debug("><><><><><><>< TreeProperties="+TreeProperties.getProperty("error.success.code"));


                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));
                    //result.setProcResultValue(userSession.getName());
                } else {
                    result.setCode("7001");
                    result.setMessage("미인증 회원");
                }
            } else {
                result.setCode("9999");
                result.setMessage("로그인 오류");
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            //result.setCode(handler.getCode());
            //result.setMsg(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }
	*/
    /**
     * 로그 아웃 처리
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    /*
    @RequestMapping(value = "/member/login/logout")
    public String logOut(HttpServletRequest request, Model model) throws Exception {
        String viewName = "redirect:/member/login.do";
        try {
            HttpSession session = request.getSession(false);
            String memberId = null;
            String sessId = null;

            if (session != null) {
                UserSession userSession = (UserSession) session.getAttribute("VSUserSession");

                sessId = session.getId();

                session.removeAttribute("isSignIn");
                session.removeAttribute("VSUserSession");
               // session.removeAttribute("VSMenuList");
                session.invalidate();
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            e.printStackTrace();
        }
        return viewName;
    }
	*/

    /**
     * getSignUp
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/{instituteID}/signUp")
    public String getSignUp(HttpServletRequest request, HttpServletResponse response, Model model,
            @PathVariable("instituteID") String instituteID
            ) throws Exception {
        VSResult result = new VSResult();

        String returnPage = "/member/signUp";

        if (!instituteID.equals("")) {

            InstituteCondition instituteCondition = new InstituteCondition();
            CampusCondition campusCondition = new CampusCondition();

            try {
                instituteCondition.setId(instituteID);
                Institute instituteInfo = (Institute) instituteService.getData(instituteCondition);

                if (instituteInfo != null) {
                    campusCondition.setInsInfoSeq(instituteInfo.getSeq());
                    List<VSObject> campusList = campusService.getDataList(campusCondition);

                    model.addAttribute("instituteInfo", instituteInfo);
                    model.addAttribute("campusList", campusList);

                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));

                } else {
                    returnPage = "/member/notFound";
                }
            } catch(Exception e) {
                 ExceptionHandler handler = new ExceptionHandler(e);
                 e.printStackTrace();
            }

        } else {
            result.setCode("1000");
            result.setMessage("필수 파라미터");

            returnPage = "/member/notFound";
        }

        return returnPage;
    }


    @RequestMapping(value = "/member/checkMemberId", method = RequestMethod.POST)
    @ResponseBody
    public VSResult<Member> checkMemberId(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
    	VSResult result = new VSResult();

    	try{
	        Member member = new Member();
	        member.setMbrId(request.getParameter("userId"));
	        int totalCount = service.getDataListCnt(member);
	        // TODO 코드를 정의해야함.
        	result.setCode("0000");
            result.setMessage(String.valueOf(totalCount));
    	} catch(Exception e) {
    		 ExceptionHandler handler = new ExceptionHandler(e);
             e.printStackTrace();
    	}

        return result;
    }


    /**
     * getMemberList
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/member/memberList", method = RequestMethod.POST)
    @ResponseBody
    public VSResult<Member> getMemberList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "instituteSeq", required = false, defaultValue = "") String instituteSeq,
            @RequestParam(value = "campSeq", required = false, defaultValue = "") String campSeq,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "mbrGrade", required = false, defaultValue = "") String mbrGrade
            ) throws Exception {
        VSResult result = new VSResult();

        try{
            MemberCondition condition = new MemberCondition();
            condition.setMbrId(mbrId);
            condition.setMbrGrade(mbrGrade);
            condition.setInstituteSeq(instituteSeq);
            condition.setCampSeq(campSeq);

            Member member = new Member();
            member = (Member) service.getDataList(condition);

            model.addAttribute("condition", condition);
            model.addAttribute("memberInfo", member);

            // TODO 코드를 정의해야함.
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch(Exception e) {
             ExceptionHandler handler = new ExceptionHandler(e);
             e.printStackTrace();
        }

        //================================================
        // 리턴 정보는 수정 필요
        return result;
    }


    /**
     * getMemberInfo
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/member/memberInfo", method = RequestMethod.POST)
    @ResponseBody
    public VSResult<Member> getMemberInfo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId
            ) throws Exception {
        VSResult result = new VSResult();

        try{
            MemberCondition condition = new MemberCondition();
            condition.setMbrId(mbrId);

            Member member = new Member();
            member = (Member) service.getData(condition);

            model.addAttribute("condition", condition);
            model.addAttribute("memberInfo", member);

            // TODO 코드를 정의해야함.
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch(Exception e) {
             ExceptionHandler handler = new ExceptionHandler(e);
             e.printStackTrace();
        }

        //================================================
        // 리턴 정보는 수정 필요
        return result;
    }


    /**
     * addMember
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/member/memberInsert", method = RequestMethod.POST)
    @ResponseBody
    public VSResult addMember(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "instituteSeq", required = false, defaultValue = "") String instituteSeq,
            @RequestParam(value = "campSeq", required = false, defaultValue = "") String campSeq,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "pwd", required = false, defaultValue = "") String pwd,
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "nickname", required = false, defaultValue = "") String nickname,
            @RequestParam(value = "email", required = false, defaultValue = "") String email,
            @RequestParam(value = "sexSect", required = false, defaultValue = "") String sexSect,
            @RequestParam(value = "birthDay", required = false, defaultValue = "") String birthDay,
            @RequestParam(value = "telNo", required = false, defaultValue = "") String telNo,
            @RequestParam(value = "quitDateTime", required = false, defaultValue = "") String quitDateTime,
            @RequestParam(value = "profilePhotoPath", required = false, defaultValue = "") String profilePhotoPath,
            @RequestParam(value = "memo", required = false, defaultValue = "") String memo,
            @RequestParam(value = "mbrGrade", required = false, defaultValue = "") String mbrGrade,
            @RequestParam(value = "sect", required = false, defaultValue = "") String sect,
            @RequestParam(value = "nation", required = false, defaultValue = "") String nation,
            @RequestParam(value = "homePage", required = false, defaultValue = "") String homePage,
            @RequestParam(value = "director", required = false, defaultValue = "") String director,
            @RequestParam(value = "province", required = false, defaultValue = "") String province,
            @RequestParam(value = "regId", required = false, defaultValue = "") String regId

            ) throws Exception {
        VSResult result = new VSResult();

        if (mbrGrade.equals("")) {
            mbrGrade = TreeProperties.getProperty("tree_student");
        }

        try{

            logger.debug("mbrId: " + mbrId);
            logger.debug("pwd: " + pwd);
            logger.debug("name: " + name);
            logger.debug("nickname: " + nickname);
            logger.debug("email: " + email);
            logger.debug("sexSect: " + sexSect);
            logger.debug("birthDay: " + birthDay);
            logger.debug("telNo: " + telNo);
            logger.debug("quitDateTime: " + quitDateTime);
            logger.debug("profilePhotoPath: " + profilePhotoPath);
            logger.debug("memo: " + memo);
            logger.debug("mbrGrade: " + mbrGrade);
            logger.debug("sect: " + sect);
            logger.debug("nation: " + nation);
            logger.debug("homePage: " + homePage);
            logger.debug("director: " + director);
            logger.debug("province: " + province);
            logger.debug("regId: " + regId);


            Member member = new Member();
            member.setInstituteSeq(instituteSeq);
            member.setCampSeq(campSeq);
            member.setMbrId(mbrId);
            member.setPwd(pwd);
            member.setName(name);
            member.setNickName(nickname);
            member.setEmail(email);
            member.setSexSect(sexSect);
            member.setBirthDay(birthDay);
            member.setTelNo(telNo);
            member.setQuitDateTime(quitDateTime);
            member.setMemo(memo);
            member.setMbrGrade(mbrGrade);
            member.setSect(sect);
            member.setNation(nation);
            member.setHomePage(homePage);
            member.setDirector(director);
            member.setProvince(province);
            member.setRegId(regId);


            // image 파일 처리
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            member.setUploadFile(multipartRequest.getFile("uploadFile"));


            // 회원 정보 등록
            service.addDataWithResultCodeMsg(member);

            // ICM관계정보 등록
            IcmRelVo icmRel = new IcmRelVo();
            icmRel.setIcmSeq(mbrId);
            icmRel.setIcmSect("RE004");
            icmRel.setRelSeq(campSeq);
            icmRel.setRelSect("RE002");
            commonService.addIcmRelData(icmRel);

            model.addAttribute("condition", member);

            // TODO 코드를 정의해야함.
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch(Exception e) {
             ExceptionHandler handler = new ExceptionHandler(e);
             e.printStackTrace();
        }

        //================================================
        // 리턴 정보는 수정 필요
        return result;
    }


    /**
     * modifyMember
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/member/memberUpdate", method = RequestMethod.POST)
    @ResponseBody
    public VSResult<Member> modifyMember(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "instituteSeq", required = false, defaultValue = "") String instituteSeq,
            @RequestParam(value = "campSeq", required = false, defaultValue = "") String campSeq,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "pwd", required = false, defaultValue = "") String pwd,
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "nickname", required = false, defaultValue = "") String nickname,
            @RequestParam(value = "email", required = false, defaultValue = "") String email,
            @RequestParam(value = "sexSect", required = false, defaultValue = "") String sexSect,
            @RequestParam(value = "birthDay", required = false, defaultValue = "") String birthDay,
            @RequestParam(value = "telNo", required = false, defaultValue = "") String telNo,
            @RequestParam(value = "quitDateTime", required = false, defaultValue = "") String quitDateTime,
            @RequestParam(value = "profilePhotoPath", required = false, defaultValue = "") String profilePhotoPath,
            @RequestParam(value = "memo", required = false, defaultValue = "") String memo,
            @RequestParam(value = "mbrGrade", required = false, defaultValue = "") String mbrGrade,
            @RequestParam(value = "sect", required = false, defaultValue = "") String sect,
            @RequestParam(value = "nation", required = false, defaultValue = "") String nation,
            @RequestParam(value = "homePage", required = false, defaultValue = "") String homePage,
            @RequestParam(value = "director", required = false, defaultValue = "") String director,
            @RequestParam(value = "province", required = false, defaultValue = "") String province,
            @RequestParam(value = "modId", required = false, defaultValue = "") String modId
            ) throws Exception {
        VSResult result = new VSResult();

        try{

            Member member = new Member();
            member.setInstituteSeq(instituteSeq);
            member.setCampSeq(campSeq);
            member.setMbrId(mbrId);
            member.setPwd(pwd);
            member.setName(name);
            member.setNickName(nickname);
            member.setEmail(email);
            member.setSexSect(sexSect);
            member.setBirthDay(birthDay);
            member.setTelNo(telNo);
            member.setQuitDateTime(quitDateTime);
            member.setMemo(memo);
            member.setMbrGrade(mbrGrade);
            member.setSect(sect);
            member.setNation(nation);
            member.setHomePage(homePage);
            member.setDirector(director);
            member.setProvince(province);
            member.setModId(modId);

            service.modifyDataWithResultCodeMsg(member);

            model.addAttribute("condition", member);

            // TODO 코드를 정의해야함.
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch(Exception e) {
             ExceptionHandler handler = new ExceptionHandler(e);
             e.printStackTrace();
        }

        //================================================
        // 리턴 정보는 수정 필요
        return result;
    }
}
