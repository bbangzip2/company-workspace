package com.visangesl.treeadmin.member.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.member.vo.UserSession;

@Repository
public class MemberDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    public UserSession signIn(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("member.signIn", vsObject);
    }

    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("member.getDataList", vsObject);
        return result;
    }

    public VSObject getData(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("member.getData", vsObject);
        return result;
    }

    public int getDataListCnt(VSObject vsObject) throws Exception {

        return getSqlSession().selectOne("member.getDataListCnt", vsObject);
    }

    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("member.modifyDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int addIcmRelDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("member.addIcmRelDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("member.addDataWithResultCodeMsg", vsObject);
        return result;
    }

    public MemberDao() {
    	try {
	    	ApplicationContext context = new ClassPathXmlApplicationContext("/root-context.xml");
	    	SqlSessionFactoryBean sqlSessionFactoryBean = (SqlSessionFactoryBean)context.getBean("sqlSessionFactory");
	    	super.setSqlSessionFactory(sqlSessionFactoryBean.getObject());
    	} catch (Exception e) {
    		
    	}
    }
}
