package com.visangesl.treeadmin.member.service.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.file.helper.TreeFileUtils;
import com.visangesl.treeadmin.file.helper.vo.FileUploadResult;
import com.visangesl.treeadmin.member.dao.MemberDao;
import com.visangesl.treeadmin.member.service.MemberService;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.member.vo.UserSession;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class MemberServiceImpl implements MemberService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MemberDao dao;


	@Override
    @Transactional(readOnly = true)
	public UserSession signIn(String userId, String userPwd) throws Exception {
	    Member member = new Member();
	    member.setMbrId(userId);
	    member.setPwd(userPwd);

        return dao.signIn(member);
    }

	@Override
    @Transactional(readOnly = true)
    public int getDataListCnt(VSObject vsObject) throws Exception {
        return dao.getDataListCnt(vsObject);
    }

	@Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();

        Member member = (Member) vsObject;
        MultipartFile uploadFile = member.getUploadFile();

        // 이미지 파일 업로드
        if (uploadFile != null && TreeAdminUtil.isNull(uploadFile.getOriginalFilename()) != "") {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_thumbnail_path");

            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
                member.setProfilePhotoPath(savePath + "/" + result.getRealFileNm());
            }
        }

        int affectedRows = dao.addDataWithResultCodeMsg(member);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    public VSObject getData(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return dao.getData(vsObject);
    }

    @Override
    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
        // TODO Auto-generated method stub
        return dao.getDataList(vsObject);
    }

    @Override
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();

        Member member = (Member) vsObject;
        MultipartFile uploadFile = member.getUploadFile();

        // 이미지 파일 업로드
        if (uploadFile !=null && TreeAdminUtil.isNull(uploadFile.getOriginalFilename()) != "") {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_thumbnail_path");

            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
                member.setProfilePhotoPath(savePath + "/" + result.getRealFileNm());
                logger.debug("member profile path : " + member.getProfilePhotoPath());
                System.out.println("member profile path : " + member.getProfilePhotoPath());

                // 이전 업로드된 이미지 파일을 지운다.
                Member prevMember = (Member) dao.getData(member);
                if (TreeAdminUtil.isNull(prevMember.getProfilePhotoPath()) != "") {
                    File preFile = new File(rootPath + prevMember.getProfilePhotoPath());
                    TreeFileUtils.removeFile(preFile.getParent(), preFile.getName());
                }
            }
        }

        int affectedRows = dao.modifyDataWithResultCodeMsg(member);
        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    public int deleteDataWithResultCodeMsg(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        return 0;
    }
}
