package com.visangesl.treeadmin.member.vo;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class Member extends VSObject implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1552844010665234135L;

    private String instituteSeq;
    private String campSeq;

    private String mbrId;
    private String pwd;
    private String name;
    private String nickName;
    private String email;
    private String sexSect;
    private String birthDay;
    private String telNo;
    private String useYn;
    private String regDate;
    private String modDate;
    private String quitDateTime;
    private String profilePhotoPath;
    private String memo;
    private String mbrGrade;
    private String sect;
    private String nation;
    private String homePage;
    private String director;
    private String province;
    private String regId;
    private String modId;

    private MultipartFile uploadFile;


    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getPwd() {
        return pwd;
    }
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getSexSect() {
        return sexSect;
    }
    public void setSexSect(String sexSect) {
        this.sexSect = sexSect;
    }
    public String getBirthDay() {
        return birthDay;
    }
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }
    public String getTelNo() {
        return telNo;
    }
    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getRegDate() {
        return regDate;
    }
    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }
    public String getModDate() {
        return modDate;
    }
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getQuitDateTime() {
        return quitDateTime;
    }
    public void setQuitDateTime(String quitDateTime) {
        this.quitDateTime = quitDateTime;
    }
    public String getProfilePhotoPath() {
        return profilePhotoPath;
    }
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }
    public String getMemo() {
        return memo;
    }
    public void setMemo(String memo) {
        this.memo = memo;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getSect() {
        return sect;
    }
    public void setSect(String sect) {
        this.sect = sect;
    }
    public String getNation() {
        return nation;
    }
    public void setNation(String nation) {
        this.nation = nation;
    }
    public String getHomePage() {
        return homePage;
    }
    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getModId() {
        return modId;
    }
    public void setModId(String modId) {
        this.modId = modId;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
    public String getInstituteSeq() {
        return instituteSeq;
    }
    public void setInstituteSeq(String instituteSeq) {
        this.instituteSeq = instituteSeq;
    }
    public String getCampSeq() {
        return campSeq;
    }
    public void setCampSeq(String campSeq) {
        this.campSeq = campSeq;
    }
    public MultipartFile getUploadFile() {
        return uploadFile;
    }
    public void setUploadFile(MultipartFile uploadFile) {
        this.uploadFile = uploadFile;
    }


}


