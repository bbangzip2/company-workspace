package com.visangesl.treeadmin.exception;

/**
 * unchecked exception을 위해 사용할 상위 class
 * 
 * @author 정남용
 * 
 */
public class TreeRuntimeException extends RuntimeException {
    /**
     * generated serialVersionUID by eclipse
     */
    private static final long serialVersionUID = 8045573442972735983L;

    /**
     * Error Code
     */
    private String code;

    /**
     * Error Message
     */
    private String msg;

    public TreeRuntimeException() {
        super();
    }
    
    public TreeRuntimeException(Exception e) {
        super(e);
    }

    public TreeRuntimeException(String code, String msg) {

        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(super.getMessage());
        sb.append(":");
        sb.append(this.code);
        return sb.toString();
    }
}
