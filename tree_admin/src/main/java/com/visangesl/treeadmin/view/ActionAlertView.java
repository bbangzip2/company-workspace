/**
 * 
 */
package com.visangesl.treeadmin.view;

import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

/**
 * @author Administrator
 */
public class ActionAlertView extends AbstractView {
    
    /**
     * 생성자.
     */
    public ActionAlertView() {
        setContentType("text/html; charset=UTF-8");
    }
    
    /**
     * 서블릿을 이용해 알럿 메시지를 보여주고, 해당 url로 Foward(or Redirect) 하기.
     * 
     * @param model 뷰단으로 전달되는 모델. 필수 Attribute는 아래와 같다.
     *            forward : Y일 경우 포워딩, 다른 값일 경우 리다이렉트.
     *            alertMessage : Alert창에 보여질 메시지.
     *            href : 이동할 url
     *            targetObject : 이동시킬 대상 페이지. 지정하지 않으면 현재 페이지로 지정. 고정)opener,
     *            parent
     *            script : 추가 자바스크립트. 예)self.close();
     *            method : form의 전달 방식. 지정하지 않으면 post  예) post, get.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws Exception Exception
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        response.setContentType(getContentType());
        
        String method = model.containsKey("method") ? model.get("method").toString() : "post";
        
        //model의 name중 파라메터로 사용하지 않는 예약어들. 이름끝 쉼표를 반드시 붙여야 하고, 소문자로만 작성해야 함.
        String notParam = "forward,alertmessage,href,targetobject,parent,script,method,target,action,";
        
        if (model.containsKey("forward") && "Y".equals(model.get("forward").toString())) {
            // foward
            PrintWriter out = response.getWriter();
            out.println("<form name='frm' method='"+method+"' target='' action='" + model.get("href") + "'>");
            
            if (model != null) {
                Set sets = model.keySet();
                
                Object[] objs = sets.toArray();
                
                for (int i = 0; i < objs.length; i++) {
                    String name = objs[i].toString();
                    
                    if (model.get(name) instanceof String && notParam.indexOf(name.toLowerCase() + ",") < 0) {
                        String value = (String) model.get(name);
                        out.println("<input type='hidden' name='" + name + "' value='" + value + "'/>");
                    }
                }
            }
            
            out.println("</form>");
            out.println("<script language='javascript'>");
            if (model.containsKey("alertMessage") && !"".equals(model.get("alertMessage").toString())) {
                out.println("alert('" + model.get("alertMessage") + "');");
            }
            
            if (model.containsKey("targetObject") && !"".equals(model.get("targetObject").toString())) {
                if (model.get("targetObject").toString().equals("opener")) {
                    out.println("document.frm.target = opener.window.name;");
                } else if (model.get("targetObject").toString().equals("parent")) {
                    out.println("document.frm.target = '_parent';");
                } else if (model.get("targetObject").toString().equals("parent.opener")) {
                    out.println("document.frm.target = '_parent.opener.window.name;';");
                }
                
            }
            
            out.println("document.frm.submit();");
            
            if (model.containsKey("script") && !"".equals(model.get("script").toString())) {
                out.println(model.get("script").toString());
            }
            
            out.println("</script>");
            out.close();
            
        } else {
            // redirect
            PrintWriter out = response.getWriter();
            out.println("<script language='javascript'>");
            if (model.containsKey("alertMessage") && !"".equals(model.get("alertMessage").toString())) {
                out.println("alert('" + model.get("alertMessage") + "');");
            }
            String target = "";
            if (model.containsKey("targetObject") && !"".equals(model.get("targetObject").toString())) {
                target = model.get("targetObject").toString() + ".";
            }
            out.println(target + "document.location.href = '" + model.get("href") + "';");
            
            if (model.containsKey("script") && !"".equals(model.get("script").toString())) {
                out.println(model.get("script").toString());
            }
            
            out.println("</script>");
            out.close();
            
        }
        
    }
    
}
