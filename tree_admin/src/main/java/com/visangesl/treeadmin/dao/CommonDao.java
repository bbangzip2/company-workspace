package com.visangesl.treeadmin.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSObject;

@Repository
public class CommonDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    public List<VSObject> getCodeList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
        result = getSqlSession().selectList("common.getCodeList", vsObject);
    	return result;
    }
    public List<VSObject> getInstituteList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
        result = getSqlSession().selectList("common.getInstituteList", vsObject);
    	return result;
    }    
    public List<VSObject> getCampusList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
        result = getSqlSession().selectList("common.getCampusList", vsObject);
    	return result;
    }
    
    public List<VSObject> getClassList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
        result = getSqlSession().selectList("common.getClassList", vsObject);
    	return result;
    }
    
    public int addIcmRelData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("common.addIcmRelData", vsObject);
		return result;
	}
    
    public int deleteIcmRelData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("common.deleteIcmRelData", vsObject);
		return result;
	}
    
    public int updateIcmRelData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("common.updateIcmRelData", vsObject);
		return result;
	}
}
