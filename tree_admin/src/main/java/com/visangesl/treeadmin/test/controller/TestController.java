package com.visangesl.treeadmin.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/test")
public class TestController {

/*
	@RequestMapping(value="server_test.do")
	public String serverTest(HttpServletRequest request) {
		return "server_test";
	}

	@RequestMapping(value="APIFiddle.do")
	public String apiFiddle(HttpServletRequest request) {
		return "APIFiddle";
	}
*/

    @RequestMapping(value="sessionTest.do")
    public String sessionTest(HttpServletRequest request) {
        return "sessionTest";
    }
}
