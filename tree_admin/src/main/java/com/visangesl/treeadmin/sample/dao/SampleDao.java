package com.visangesl.treeadmin.sample.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.sample.vo.Sample;
import com.visangesl.treeadmin.sample.vo.SampleListCondition;

@Repository
public class SampleDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    
    public Sample getData(Sample sample) {
    	Sample result = null;
        result = getSqlSession().selectOne("sample.getData", sample);
    	return result;
    }
    
    public List<Sample> getDataList(SampleListCondition sampleListCondition) {
    	List<Sample> result = null;
    	result = getSqlSession().selectList("sample.getDataList", sampleListCondition);
    	return result;
    }
    
    
    public int getDataListCnt(SampleListCondition sampleListCondition)  {
    	
    	return getSqlSession().selectOne("sample.getDataListCnt", sampleListCondition);
    }
    
    
    public int modifyDataWithResultCodeMsg(Sample sample) {
		// TODO Auto-generated method stub
		int result = 0;
			result = getSqlSession().update("sample.modifyDataWithResultCodeMsg", sample);
		return result;
	}
    
    public int addDataWithResultCodeMsg(Sample sample) {
		// TODO Auto-generated method stub
		int result = 0;
			result = getSqlSession().insert("sample.addDataWithResultCodeMsg", sample);
		return result;
	}
    
    public int deleteDataWithResultCodeMsg(Sample sample) {
		// TODO Auto-generated method stub
		int result = 0;
			result = getSqlSession().delete("sample.deleteDataWithResultCodeMsg", sample);
		return result;
	}
    

//	public MemberInfo selectCheckVaildEndDate(String userId) {
//		// TODO Auto-generated method stub
//		Map<String, String> param = new HashMap<String, String>();
//
//		param.put("userId", userId);
//		return getSqlSession().selectOne("member.checkVaildEndDate", param);
//	}
}
