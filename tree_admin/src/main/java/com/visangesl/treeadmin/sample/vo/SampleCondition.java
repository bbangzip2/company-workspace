package com.visangesl.treeadmin.sample.vo;

import com.visangesl.tree.vo.VSCondition;

//@XmlRootElement(name="Sample")
public class SampleCondition extends VSCondition {
	private String searchNm;
	private String regDttm;
	private String stauts;
	
    private String searchType = null;

    private String searchValue = null;
	
	
	public String getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}

	public String getStauts() {
		return stauts;
	}

	public void setStauts(String stauts) {
		this.stauts = stauts;
	}

	public String getSearchNm() {
		return searchNm;
	}

	public void setSearchNm(String searchNm) {
		this.searchNm = searchNm;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

}
