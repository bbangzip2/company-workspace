package com.visangesl.treeadmin.sample.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.sample.dao.SampleDao;
import com.visangesl.treeadmin.sample.service.SampleService;
import com.visangesl.treeadmin.sample.vo.Sample;
import com.visangesl.treeadmin.sample.vo.SampleListCondition;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class SampleServiceImpl implements SampleService {

    @Autowired 
    SampleDao dao;

    
	@Override
	public Sample getData(Sample sample) throws Exception {
		return dao.getData(sample);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<Sample> getDataList(SampleListCondition sampleListCondition) throws Exception {
        return dao.getDataList(sampleListCondition);
    }
    
	@Override
    @Transactional(readOnly = true)
    public int getDataListCnt(SampleListCondition sampleListCondition) throws Exception {
        return dao.getDataListCnt(sampleListCondition);
    }
	
	
	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(Sample sample) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.modifyDataWithResultCodeMsg(sample); 
        		
        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult addDataWithResultCodeMsg(Sample sample) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addDataWithResultCodeMsg(sample); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	@Override
    @Transactional
    public VSResult deleteDataWithResultCodeMsg(Sample sample) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	int affectedRows = dao.deleteDataWithResultCodeMsg(sample);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }


}
