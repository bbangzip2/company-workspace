package com.visangesl.treeadmin.sample.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.sample.service.SampleService;
import com.visangesl.treeadmin.sample.vo.Sample;
import com.visangesl.treeadmin.sample.vo.SampleListCondition;
import com.visangesl.treeadmin.service.CodeService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.CodeVo;

@Controller
public class SampleController implements BaseController {

	@Autowired
	private SampleService sampleService;
	
//	@Autowired
//	private CommonService commonService;
	
	@Autowired
	private CodeService codeService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/sample")
	//@Interceptor("loginIntercpetor")
	public String sampleList(HttpServletRequest request, HttpServletResponse response, Model model ) throws Exception {
		logger.debug("sample");
		
		// 조회조건 vo 
		SampleListCondition sampleCondition = new SampleListCondition();
		
		sampleCondition.setStauts("DESC");
		sampleCondition.setRegDttm("");
		
		
		int pageNo = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageSize"), "3"));
		
		
		sampleCondition.setPageNo(pageNo); // 현재 페이지 
		sampleCondition.setPageSize(pageSize); // 페이지 사이즈 
		
		
		List<Sample> sampleList = sampleService.getDataList(sampleCondition);
		
		int listCnt = sampleService.getDataListCnt(sampleCondition);

		
		// 코드 서비스를 이용해서 국가 코드를 조회함.
		CodeVo codeVo = new CodeVo();
		codeVo.setRefCode("NA000");
		
//		List<VSObject> codeList = commonService.getCodeList(codeVo);
//		model.addAttribute("codeList", codeList);
		
		logger.debug("sampleList cnt: " + sampleList.size());
		
		model.addAttribute("listCondition", sampleCondition);
		model.addAttribute("sampleList", sampleList);
		model.addAttribute("listCnt", listCnt);
		
		
		

		return "/sample";
	}

	@RequestMapping(value = "/sampleInsert")
	public String sampleInsert(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		logger.debug("sampleInsert");

		return "/sampleInsert";
	}

	@RequestMapping(value = "/sampleInfo", method = RequestMethod.POST)
	public String sampleInfo(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("sampleInfo");

		Sample sample = new Sample(); 
		sample.setMbrId(mbrId);
		
		 sample = (Sample) sampleService.getData(sample);
		 
		model.addAttribute("sampleInfo", sample);

		return "/sampleInsert";
	}

	@RequestMapping(value = "/sampleAdd", method = RequestMethod.POST)
	public ModelAndView sampleAdd(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "nickName", required = false) String nickName,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "nm", required = false) String nm,
			@RequestParam(value = "eMail", required = false) String eMail,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "memo", required = false) String memo
			) throws Exception {
		logger.debug("sampleAdd");

		
		Sample sample = new Sample();
		
		sample.setNickName(nickName);
		sample.setPwd(pwd);
		sample.setNm(nm);
		sample.seteMail(eMail);
		sample.setTelNo(telNo);
		sample.setMemo(memo);
		sample.setMbrGrade("MBR_GRADE");

		sampleService.addDataWithResultCodeMsg(sample);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/sample.do");
		return mav;
	}

	@RequestMapping(value = "/sampleModify", method = RequestMethod.POST)
	public ModelAndView sampleModify(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId,
			@RequestParam(value = "nickName", required = false) String nickName,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "nm", required = false) String nm,
			@RequestParam(value = "eMail", required = false) String eMail,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "memo", required = false) String memo,
			@RequestParam(value = "useYn", required = false) String useYn
			) throws Exception {
		logger.debug("sampleModify");

		Sample sample = new Sample();
		sample.setMbrId(mbrId);
		sample.setNickName(nickName);
		sample.setPwd(pwd);
		sample.setNm(nm);
		sample.seteMail(eMail);
		sample.setTelNo(telNo);
		sample.setMemo(memo);
		sample.setUseYn(useYn);
		sample.setMbrGrade("MBR_GRADE");

		sampleService.modifyDataWithResultCodeMsg(sample);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/sample.do");
		return mav;
	}

	@RequestMapping(value = "/sampleRemove", method = RequestMethod.POST)
	public ModelAndView sampleRemove(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("sampleRemove");

		Sample sample = new Sample();
		sample.setMbrId(mbrId);

		sampleService.deleteDataWithResultCodeMsg(sample);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/sample.do");
		return mav;
	}

}
