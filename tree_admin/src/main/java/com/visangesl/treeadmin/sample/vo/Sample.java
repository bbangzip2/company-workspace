package com.visangesl.treeadmin.sample.vo;

import com.visangesl.tree.vo.VSObject;

//@XmlRootElement(name="Sample")
public class Sample extends VSObject {
	private String rn;
	private String mbrId;
	private String pwd;
	private String nm;
	private String nickName;
	private String eMail;
	private String sexSect;
	private String byMd;
	private String telNo;
	private String useYn;
	private String regDttm;
	private String modDttm;
	private String quitDt;
	private String profilePhotopath;
	private String memo;
	private String mbrGrade;


	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	public String getSexSect() {
		return sexSect;
	}
	public void setSexSect(String sexSect) {
		this.sexSect = sexSect;
	}
	public String getByMd() {
		return byMd;
	}
	public void setByMd(String byMd) {
		this.byMd = byMd;
	}
	public String getTelNo() {
		return telNo;
	}
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getModDttm() {
		return modDttm;
	}
	public void setModDttm(String modDttm) {
		this.modDttm = modDttm;
	}
	public String getQuitDt() {
		return quitDt;
	}
	public void setQuitDt(String quitDt) {
		this.quitDt = quitDt;
	}
	public String getProfilePhotopath() {
		return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getMbrGrade() {
		return mbrGrade;
	}
	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}

}
