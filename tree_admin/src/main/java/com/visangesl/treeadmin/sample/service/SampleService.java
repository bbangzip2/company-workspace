package com.visangesl.treeadmin.sample.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.treeadmin.sample.vo.Sample;
import com.visangesl.treeadmin.sample.vo.SampleListCondition;
import com.visangesl.treeadmin.vo.VSResult;

/**
 * 커먼 서비스를 이용하기 위해서 샘플 서비스에서 commonService를 상속받지 않는다. 따라서 SampleService 에서 사용할 메소드를 모두 구현한다. 
 * @author hong
 *
 */
@Service
public interface SampleService {
	
	// select One Data
	public Sample getData(Sample sample) throws Exception;
	
	// select List Data 
	public List<Sample> getDataList(SampleListCondition sampleListCondition) throws Exception;
	
	// select List Count 
	public int getDataListCnt(SampleListCondition sampleListCondition) throws Exception;
	
	// Update
	public VSResult modifyDataWithResultCodeMsg(Sample sample) throws Exception;
	
	// Insert 
	public VSResult addDataWithResultCodeMsg(Sample sample) throws Exception;
	
	// Delete 
	public VSResult deleteDataWithResultCodeMsg(Sample sample) throws Exception;
	

}
