package com.visangesl.treeadmin.tld;

import java.lang.Character.UnicodeBlock;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.SimpleHtmlSerializer;
import org.htmlcleaner.TagNode;

import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;

public class TldStringFunction {
	private static final double BASE = 1024, KB = BASE, MB = KB*BASE, GB = MB*BASE;
    private static final DecimalFormat df = new DecimalFormat("#.##");

    public static String shortString(String src, int len) {
        return shortStringWithPostFix(src, len, "...");
    }

    public static String shortStringWithPostFix(String src, int len, String postFix) {
        String rsltValue = null;
        StringBuffer sb = new StringBuffer();

        if ((src == null) || (src.length() <= len)) {
            rsltValue = src;
        } else {
            sb.append(src.substring(0, len)).append(postFix);
            rsltValue = sb.toString();
        }

        return rsltValue;
    }

    public static String shortStringWithSize(String src, int len) {
        StringBuffer sb = new StringBuffer();
        int size = 0;

        if (src == null)
            return sb.toString();
        if (len < 0)
            return sb.toString();

        try {
            for (int i = 0; i < src.length(); i++) {
                char ch = src.charAt(i);
                Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(ch);
                if (size < len) {
                    if (UnicodeBlock.HANGUL_SYLLABLES.equals(unicodeBlock) || UnicodeBlock.HANGUL_COMPATIBILITY_JAMO.equals(unicodeBlock) || UnicodeBlock.HANGUL_JAMO.equals(unicodeBlock)
                            || UnicodeBlock.HIRAGANA.equals(unicodeBlock) || UnicodeBlock.KATAKANA.equals(unicodeBlock) || UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS.equals(unicodeBlock)
                            || UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS.equals(unicodeBlock)) {
                        size = size + 2;
                        sb.append(ch);
                    } else {
                        size = size + 1;
                        sb.append(ch);
                    }
                } else {
                    break;
                }
            }
        } catch (Exception e) {

        }

        if (!src.equals(sb.toString())) {
            sb.insert(0, "<span title=\"" + src + "\">");
            sb.append("...</span>");
        }

        return sb.toString();
    }

    public static String appendAnchorTag(String src) {
        String rsltValue = null;
        if (src == null || src.length() <= 7) {
            rsltValue = src;
        } else {
            StringBuffer sb = new StringBuffer(src);

            int beginIdx = 0;
            int endIdx = 0;

            while (beginIdx != -1) {
                beginIdx = sb.indexOf("http://", beginIdx);
                if (beginIdx != -1) {
                    endIdx = sb.indexOf(" ", beginIdx);

                    if (endIdx != -1) {

                    } else {
                        endIdx = sb.length();
}

                    String hrefValue = sb.substring(beginIdx, endIdx);

                    StringBuffer anchorTag = new StringBuffer();
                    anchorTag.append("<a href=\"").append(hrefValue).append("\" target=\"_blank\">").append(hrefValue).append("</a>");
                    sb.replace(beginIdx, endIdx, anchorTag.toString());

                    beginIdx = beginIdx + anchorTag.length();
                }
            }

            rsltValue = sb.toString();
        }

        return rsltValue;
    }

    /**
     * 교과정보 참고자료 summary 필드에 htmlCleaner 적용
     * @param summary
     * @return
     * @throws Exception
     */
    public static String htmlCleaner(String summary)throws Exception{


        CleanerProperties props = new CleanerProperties();

        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(false);
        props.setOmitComments(false);
        props.setAllowMultiWordAttributes(true);
        props.setAllowHtmlInsideAttributes(true);
        props.setUseEmptyElementTags(true);
        props.setTranslateSpecialEntities(false);
        props.setOmitDoctypeDeclaration(true);
        props.setOmitXmlDeclaration(true);

        TagNode tagNode = new HtmlCleaner(props).clean(summary);
        TagNode bodyNode = tagNode.findElementByName("BODY", false);

        if (bodyNode == null)
            bodyNode = tagNode;

        try {
            SimpleHtmlSerializer simpleHtmlSerializer = new SimpleHtmlSerializer(props);
            summary = simpleHtmlSerializer.getAsString(bodyNode, true);
        } catch (Exception e) {

        }

        return summary;
    }

    //첨부 파일의 파일 사이즈 출력
    public static String printFileSize(String file_sz) {
    	double size = Double.parseDouble(file_sz);

        if(size >= GB) {
            return df.format(size / GB) + " GB";
        }
        if(size >= MB) {
            return df.format(size / MB) + " MB";
        }
        if(size >= KB) {
            return df.format(size / KB) + " KB";
        }
        return "" + (int)size + " bytes";
    }

    public static String getClearHtmlStr(String str){

        String clearHtmlStr = str.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");

        return clearHtmlStr;
    }

    public static String getClearHtmlStr2(String str){

        String clearHtmlStr = str.replaceAll("<[^>]*>", "");

        return clearHtmlStr;
    }

    /**
     * 썸네일 경로 생성
     *
     * @param fileCdnYn
     * @param contextPath
     * @param path
     * @return
     */
    public static String thumbnailPath(String fileCdnYn, String contextPath, String path) {
        StringBuffer sb = new StringBuffer();

        if (path != null && path.length() > 0) {
            if (fileCdnYn != null) {
                if (fileCdnYn.equals("Y")) {
                    sb.append(TreeProperties.getProperty("tree_cdn_url"));
                }
            } else {
                sb.append(contextPath);
            }

            sb.append(path);
        }

        return sb.toString();
    }

    public static String getFileExt(String fileName) {
        if (fileName == null)
            return null;
        int idx = fileName.lastIndexOf(".");

        if (idx == -1)
            return null;

        return fileName.substring(idx + 1).trim();
    }

    public static String siteTitle(HttpServletRequest request) {
        StringBuffer sb = new StringBuffer();

        String servletPath = request.getServletPath().toString();
        String queryString = TreeAdminUtil.isNull(request.getQueryString());

        if (servletPath != null) {
            try {
                int slashIdx = servletPath.indexOf("/", 13);

                if (slashIdx != -1) {
                    int toSlashIdx = servletPath.indexOf("/", slashIdx + 1);

                    String dir = null;
                    if (toSlashIdx != -1) {
                        dir = servletPath.substring(slashIdx + 1, toSlashIdx);

                        if (dir.equals("educourse")) {
                            sb.append("교과자료");

                            if (queryString.indexOf("schType=MS") != -1) {
                                sb.append("::중등");
                            } else if (queryString.indexOf("schType=HS") != -1) {
                                sb.append("::고등");
                            }
                        } else if (dir.equals("opendata")) {
                            sb.append("열린 자료");
                        } else if (dir.equals("creative")) {
                            sb.append("창의적 체험활동");
                        } else if (dir.equals("knowledge")) {
                            sb.append("열린 샘터");
                        } else if (dir.equals("myclass")) {
                            sb.append("나의 교실");
                        } else if (dir.equals("event")) {
                            sb.append("이벤트");
                        }
                    }
                }
            } catch (Exception e) {

            }

        }

        if (sb.length() == 0) {
            sb.append("선생님을 위한 스마트 교수학습 서비스");
        }

        sb.append(" | 비바샘 (VivaSam)");
        return sb.toString();
    }

    public static String getMenuImage(int menuSeq) {
    	String menuImage = null;

    	switch(menuSeq) {
    	case 1:
    		menuImage = "/common/gnb_01.gif";
    		break;
    	case 10:
    		menuImage = "/common/gnb_02.gif";
    		break;
    	case 11:
    		menuImage = "/common/gnb_03.gif";
    		break;

    	}

    	return menuImage;
    }
}
