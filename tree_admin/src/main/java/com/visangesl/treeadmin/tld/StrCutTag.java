package com.visangesl.treeadmin.tld;

import java.lang.Character.UnicodeBlock;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class StrCutTag extends TagSupport {

    private static final long serialVersionUID = 1L;
    
    private int length;

    private String value;

    
    public void setLength(int length)
    
    {

           this.length = length;

    }

    public void setValue(String value)

    {

           this.value = value;

    }
    
    public String containsHangulCnt(String str)
    {
    	
    	String result="";
        int size = 0;    	
        for(int i = 0 ; i < str.length() ; i++)
        {
            char ch = str.charAt(i);
            Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(ch);
            if(size<length){
                if(UnicodeBlock.HANGUL_SYLLABLES.equals(unicodeBlock) || UnicodeBlock.HANGUL_COMPATIBILITY_JAMO.equals(unicodeBlock) ||
                        UnicodeBlock.HANGUL_JAMO.equals(unicodeBlock) || UnicodeBlock.HIRAGANA.equals(unicodeBlock) ||
                        UnicodeBlock.KATAKANA.equals(unicodeBlock) || UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS.equals(unicodeBlock) ||
                        UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS.equals(unicodeBlock)){
                	size = size+2;  
                	result += ch;
                }
                else{
                	size = size+1;
                	result += ch;
                }            	
            }
            else{
            	break;
            }
        }
        return result;
    }

    @Override

    public int doEndTag() throws JspException

    {

    	length = 0;

           value = null;



           return EVAL_PAGE;

    }
    @Override
    
    public int doStartTag() throws JspException

    {

           try

           {
        	   	   String temp = containsHangulCnt(value);
            // System.out.println(temp);
                   // 지정 길이 보다 출력 String 길이가 짧으면 그냥 출력

                   if (value.equals(temp))

                   {

                	   pageContext.getOut().println(temp);

                   }
                   else{
                temp = "<span title=\"" + value + "\">" + temp + "...</span>";
                	   pageContext.getOut().println(temp);
                   }
                          
           }

           catch (Exception e)

           {

                   e.printStackTrace();

                   throw new JspException();

           }



           return SKIP_BODY;

    }
}
