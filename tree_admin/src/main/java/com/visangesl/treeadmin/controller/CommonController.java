package com.visangesl.treeadmin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.vo.CodeCondition;
import com.visangesl.treeadmin.vo.CodeVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class CommonController implements BaseController {

	@Autowired
	private CommonService commonService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	
	/**
	 * 코드 테이블 조회 
	 * @param request
	 * @param response
	 * @param model
	 * @param relCd : 상위 코드 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/common/codeList")
	@ResponseBody
	public VSResult<CodeVo> getCodeList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "relCd", required = false) String relCd) throws Exception {
		
		logger.debug("getCodeList");
		VSResult result = new VSResult();
		
		CodeCondition condition = new CodeCondition();
		condition.setRelCd(relCd);
		
	    try {
		
			List<VSObject> codeList = commonService.getCodeList(condition);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(codeList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            e.printStackTrace();
	    }
		
		return result;
	}
	
	/**
	 * 기관 목록 조회 (combo box 용) 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/common/instituteList")
	@ResponseBody
	public VSResult<CodeVo> getInstituteList(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		
		logger.debug("instituteList");
		VSResult result = new VSResult();
		
		CodeCondition condition = new CodeCondition();
		
	    try {
		
			List<VSObject> codeList = commonService.getInstituteList(condition);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(codeList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            e.printStackTrace();
	    }
		
		return result;
	}
	
	/**
	 * 캠퍼스 목록 조회 (combo box 용) 
	 * @param request
	 * @param response
	 * @param model
	 * @param intInfoSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/common/campusList")
	@ResponseBody
	public VSResult<CodeVo> getCampusList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "intInfoSeq", required = false) String intInfoSeq) throws Exception {
		
		logger.debug("campusList");
		VSResult result = new VSResult();
		
		CodeCondition condition = new CodeCondition();
		condition.setInsInfoSeq(intInfoSeq);
		
	    try {
		
			List<VSObject> codeList = commonService.getCampusList(condition);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(codeList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            e.printStackTrace();
	    }
		
		return result;
	}
	
	/**
	 * 클래스 목록 조회 (combobox 용) 
	 * @param request
	 * @param response
	 * @param model
	 * @param intInfoSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/common/classList")
	@ResponseBody
	public VSResult<CodeVo> getClassList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "campInfoSeq", required = false) String campInfoSeq) throws Exception {
		
		logger.debug("getClassList");
		VSResult result = new VSResult();
		
		CodeCondition condition = new CodeCondition();
		condition.setCampInfoSeq(campInfoSeq);
		
	    try {
		
			List<VSObject> codeList = commonService.getClassList(condition);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(codeList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            e.printStackTrace();
	    }
		
		return result;
	}
	
	
}
