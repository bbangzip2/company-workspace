package com.visangesl.treeadmin.excel;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *     	String sampleExcel = request.getServletContext().getRealPath("/sample/sample.xls");
    	
    	logger.debug("EXCEL TEST START==========================>");
    	logger.debug(sampleExcel);
    	
 * 		//엑셀 파일 경로 지정
    	TreeExcelReader reader = new TreeExcelReader(sampleExcel);
    	
    	//cell의 갯수를 지정
    	reader.setNumberOfCells(3);
    	
    	// 읽어올 시트 index를 지정
    	reader.setSheetNum(0);
    	
    	// 한줄씩 문서를 읽어와 ArrayList<Object>로 return
    	ArrayList<Object> row;
    	do {
    		row = reader.readRow();
    		if (row == null) break;

    		for (Object obj: row) {
        		if (obj != null) {
            		logger.debug(obj.toString());
        		} else {
        			logger.debug("NO_DATA");
        		}
        	}
    	} while (row!=null);    	
    	
    	reader.close();
    	logger.debug("<==========================EXCEL TEST END");
 * @author imac
 *
 */
public class TreeExcelReader {
	//Excel파일이 위치한 서버상의 경로(파일명 포함)
	private String filePath;

	/*
	최대 cell의 갯수
	Iterate처리시 중간의 cell에 값이 없는 경우 missing처리 되어 필드의 순서를 정확히 판단할 수 없다.
	예를 들면 다음과 같은 엑셀자료가 있다고 치면 2줄의 Row를 가져오게 된다.
	순서     이름     별명
	---------------------
	1 		홍길동 	가나다
	2 			 	ABC
	
	그런데 Iterate처리를 하여 가져온 결과를 array 형태의 pseudo 로 표현하면 다음과 같이 된다.
		[0][0] = 1
		[0][1] = 홍길동
		[0][2] = 가나다
		[1][0] = 2
		[1][1] = ABC
	
	중간이 missing처리되면 missing되었다는 걸 알수가 없다 이를 해결하기위해 numberOfCells변수를 사용한다.
	*/
	private int numberOfCells;
	
	// 문서의 sheet 갯수
	private int numberOfSheets;

	//읽으려는 sheet index (sheet index는 0부터 시작)
	private int sheetNum;
	
	// 사용하지 않는 row수
	private int unusedRowNum;
	
	// 내부 변수들
	private Workbook workbook;
	private Iterator<Row> rowIterator;
	
	public TreeExcelReader(String filePath) {
	    this(filePath, 1);
	}

	public TreeExcelReader(String filePath, int numberOfCells) {
		this(filePath, numberOfCells, 0);
	}

	public TreeExcelReader(String filePath, int numberOfCells, int unusedRowNum) {
		this.filePath = filePath;
		this.unusedRowNum = unusedRowNum;
		setNumberOfCells(numberOfCells);
		
		if (filePath != null) {
		    try {
    			openStream();
		    } catch (Exception e) {
		        e.printStackTrace();
		        closeStream();
		    }
		}
	}
	
	public void setNumberOfCells(int numberOfCells) {
		this.numberOfCells = (numberOfCells < 1) ? 1 : numberOfCells;
	}

	private void openStream() throws Exception {
		/*
		 * HSSFWorkbook/XSSFWorkbook사용시 File을 사용하는 것이 InputStream을 사용하는것보다 적은 메모리를 사용한다.
		 * http://poi.apache.org/spreadsheet/quick-guide.html#FileInputStream
		 */
        workbook = WorkbookFactory.create(new File(this.filePath));        
        numberOfSheets = workbook.getNumberOfSheets();
	}
	
	private void closeStream() {
	}
	
	public void close() {
		closeStream();
	}
	
	/**
	 * 문서의 sheet 갯수를 return
	 * @return
	 */
	public int getNumberOfSheets() {
		return numberOfSheets;
	}
	
	/**
	 * 현재의 sheet index
	 * @return
	 */
	public int getSheetNum() {
		return sheetNum;
	}
	
	/**
	 * 현재 sheet iterator
	 * @return
	 */
	public Iterator<Row> getIterator() {
	    return rowIterator;
	}
	
	/**
	 * Get the Sheet object at the given index.
	 * @param sheetNum - of the sheet number (0-based physical & logical)
	 */
	public void setSheetNum(int sheetNum) {
		this.sheetNum = sheetNum;
		
		Sheet sheet = workbook.getSheetAt(sheetNum);
		rowIterator = sheet.iterator();
		
		int i = 0;
		while (i < unusedRowNum) {
		    if (rowIterator.hasNext()) rowIterator.next();
		    i++;
		}
	}

	public ArrayList<Object> readRow() {
		if (workbook == null) return null;
		
		ArrayList<Object> list = null;
		try {
			if (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				list = new ArrayList<Object>();
				
				for (int j=0; j < numberOfCells; j++) {
                	Cell cell = row.getCell(j, Row.RETURN_BLANK_AS_NULL);
                	
                    //check the cell type and process accordingly
                    switch(cell.getCellType()){
                    	case Cell.CELL_TYPE_BLANK :
                    		list.add(null);
//                    		System.out.println("BLANK_DATA");
                    		break;
                        case Cell.CELL_TYPE_STRING:
                        	list.add(cell.getStringCellValue().trim());
//                        	System.out.print(cell.getRichStringCellValue());
//    						System.out.println("CELL ::"+cell.getStringCellValue().trim());
                        	break;
                        case Cell.CELL_TYPE_NUMERIC:
                        	if (DateUtil.isCellDateFormatted(cell)) {
                        		list.add(cell.getDateCellValue());
//                        		System.out.print(cell.getDateCellValue());
                        	} else {
                        		list.add(cell.getNumericCellValue());
//                        		System.out.print(cell.getNumericCellValue());
                        	}
                        	break;
                        case Cell.CELL_TYPE_BOOLEAN:
                        	list.add(cell.getBooleanCellValue());
//                        	System.out.print(cell.getBooleanCellValue());
                        	break;
                        case Cell.CELL_TYPE_FORMULA:
                        	list.add(cell.getCellFormula());
//                        	System.out.print(cell.getCellFormula());
                        	break;
                    }
				}
			} else {
				
			}
        } catch (Exception e) {
            e.printStackTrace();
            list = null;
        }
		
		return list;
	}
}
