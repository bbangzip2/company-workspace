package com.visangesl.treeadmin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface CommonService {
	// Nation List Data 
	public List<VSObject> getCodeList(VSObject vsObject) throws Exception;
	
	public List<VSObject> getInstituteList(VSObject vsObject) throws Exception;
		
	public List<VSObject> getCampusList(VSObject vsObject) throws Exception;
	
	public List<VSObject> getClassList(VSObject vsObject) throws Exception;
	
	public VSResult addIcmRelData(VSObject vsObject) throws Exception ;
	
	public VSResult deleteIcmRelData(VSObject vsObject) throws Exception ;
	
	public VSResult updateIcmRelData(VSObject vsObject) throws Exception;
	
}
