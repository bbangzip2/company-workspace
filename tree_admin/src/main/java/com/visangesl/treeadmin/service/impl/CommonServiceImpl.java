package com.visangesl.treeadmin.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.CommonDao;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class CommonServiceImpl implements CommonService {
	private final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	CommonDao dao;

	@Override
    public List<VSObject> getCodeList(VSObject vsObject) throws Exception {
        return dao.getCodeList(vsObject);
    }
	@Override
	public List<VSObject> getInstituteList(VSObject vsObject) throws Exception {
		return dao.getInstituteList(vsObject);
	}
	@Override
    public List<VSObject> getCampusList(VSObject vsObject) throws Exception {
        return dao.getCampusList(vsObject);
    }
	
	@Override
    public List<VSObject> getClassList(VSObject vsObject) throws Exception {
        return dao.getClassList(vsObject);
    }
	
	@Override
    @Transactional
    public VSResult addIcmRelData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addIcmRelData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  	
	
	@Override
    @Transactional
    public VSResult deleteIcmRelData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.deleteIcmRelData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    } 
	
	@Override
    @Transactional
    public VSResult updateIcmRelData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.updateIcmRelData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
	
}

