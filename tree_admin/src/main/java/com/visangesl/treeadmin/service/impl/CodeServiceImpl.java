package com.visangesl.treeadmin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.CommonDao;
import com.visangesl.treeadmin.service.CodeService;

@Service
public class CodeServiceImpl  implements CodeService {

	@Autowired
	CommonDao dao;

	@Override
    @Transactional
    public List<VSObject> getCodeList(VSObject vsObject) throws Exception {
        return dao.getCodeList(vsObject);
    }
	
}
