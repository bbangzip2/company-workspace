package com.visangesl.treeadmin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;

@Service
public interface CodeService {
	
	// Nation List Data 
	public List<VSObject> getCodeList(VSObject vsObject) throws Exception;
	
}
