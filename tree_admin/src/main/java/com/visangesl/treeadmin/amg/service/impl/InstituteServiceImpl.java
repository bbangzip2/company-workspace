package com.visangesl.treeadmin.amg.service.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.InstituteDao;
import com.visangesl.treeadmin.amg.service.InstituteService;
import com.visangesl.treeadmin.amg.vo.Institute;
import com.visangesl.treeadmin.amg.vo.InstituteCondition;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfo;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfoCondition;
import com.visangesl.treeadmin.file.helper.TreeFileUtils;
import com.visangesl.treeadmin.file.helper.vo.FileUploadResult;
import com.visangesl.treeadmin.member.vo.MemberCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class InstituteServiceImpl implements InstituteService {

	@Autowired
	InstituteDao dao;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public VSObject getData(VSCondition dataCondition) throws Exception {
		return dao.getData(dataCondition);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return dao.getDataList(dataCondition);
    }
    
	@Override
    @Transactional(readOnly = true)
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
        return dao.getDataListCnt(dataCondition);
    }
	
	
	@Override
    @Transactional
    public VSResult<VSObject> modifyDataWithResultCodeMsg(VSCondition dataCondition) throws Exception {
    	VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();
    	
    	InstituteCondition condition = (InstituteCondition)dataCondition;
    	MultipartFile uploadFile = condition.getUploadFile();
    	
    	// 이미지 파일 업로드
    	if (uploadFile != null && TreeAdminUtil.isNull(uploadFile.getOriginalFilename()) != "") {
    	    String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_portfolio_filepath");
            
            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
                condition.setLogoPath(savePath + "/" + result.getRealFileNm());
                
                // 이전 업로드된 이미지 파일을 지운다.
                Institute institute = (Institute) dao.getData(condition);
                if (TreeAdminUtil.isNull(institute.getLogoPath()) != "") {
                    File preFile = new File(rootPath + institute.getLogoPath());
                    TreeFileUtils.removeFile(preFile.getParent(), preFile.getName());
                }
            }
    	}
    	
        int affectedRows = dao.modifyDataWithResultCodeMsg(condition); 
        		
        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
	
	
	@Override
    @Transactional
    public VSResult<VSObject> addDataWithResultCodeMsg(VSCondition dataCondition) throws Exception {
    	VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();

        InstituteCondition condition = (InstituteCondition)dataCondition;
        MultipartFile uploadFile = condition.getUploadFile();
        
        // 이미지 파일 업로드
        if (TreeAdminUtil.isNull(uploadFile.getOriginalFilename()) != "") {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_portfolio_filepath");
            
            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
                condition.setLogoPath(savePath + "/" + result.getRealFileNm());
            }
        }
    	
        int affectedRows = dao.addDataWithResultCodeMsg(condition); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
            resultCodeMsg.setMessage(String.valueOf(affectedRows));
        }

        return resultCodeMsg;
    }  
	
	@Override
    @Transactional
    public VSResult<?> deleteDataWithResultCodeMsg(VSCondition dataCondition) throws Exception {
    	VSResult<?> resultCodeMsg = new VSResult<>();

    	int affectedRows = dao.deleteDataWithResultCodeMsg(dataCondition);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
	

    @Override
    public int getCampusCntForInstitute(VSCondition dataCondition) throws Exception {
        return dao.getCampusCntForInstitute(dataCondition);
    }

    @Override
    public String getInstituteSeq(VSObject vsObject) throws Exception {
        String insInfoSeq = null;
        
        MemberCondition condition = (MemberCondition) vsObject;
        String mbrGrade = condition.getMbrGrade();
        // 기관 관리자일 경우
        if (mbrGrade.equals(TreeProperties.getProperty("tree_instituteadmin"))) {
            insInfoSeq = dao.getInsInfoSeqForInstitute(condition);
        }
        // 캠퍼스 관리자일 경우
        else if (mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
            insInfoSeq = dao.getInsInfoSeqForCampus(condition);
        }
        
        return insInfoSeq;
    }
    
    @Override
    @Transactional(readOnly = true)
	public MemberInstituteInfo getInstituteInfoWithMemberId(MemberInstituteInfoCondition dataCondition) throws Exception {
    	MemberInstituteInfo memberInstituteInfo = dao.getInstituteInfoWithMemberId(dataCondition);
    	
    	return memberInstituteInfo;
    }
}
