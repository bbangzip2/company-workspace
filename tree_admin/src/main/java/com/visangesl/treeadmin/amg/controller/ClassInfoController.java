package com.visangesl.treeadmin.amg.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.ClassInfoService;
import com.visangesl.treeadmin.amg.vo.ClassInfo;
import com.visangesl.treeadmin.amg.vo.ClassInfoCondition;
import com.visangesl.treeadmin.amg.vo.ClassSchdl;
import com.visangesl.treeadmin.amg.vo.ClassSchdlCondition;
import com.visangesl.treeadmin.amg.vo.Teacher;
import com.visangesl.treeadmin.amg.vo.TodayClassCondition;
import com.visangesl.treeadmin.amg.vo.TodayClassVo;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.CodeCondition;
import com.visangesl.treeadmin.vo.CodeVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class ClassInfoController implements BaseController {

	@Autowired
	private ClassInfoService classInfoService;
	
	@Autowired
	private CommonService commonService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 캠퍼스 목록 조회 
	 * @param request
	 * @param response
	 * @param model
	 * @param campSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/classInfoList")
	public String classInfo(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort			
			) throws Exception {
		logger.debug("classInfoList");
		
		ClassInfoCondition classInfoCondition = new ClassInfoCondition();
		
		int pageNo = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageSize"), "20")); // 기본 갯수는 20개 
		
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 

		
		// LIMIT 함수로 페이징 처리 하니깐 1페이지 일때 0 부터 들어가야 한다. 그래서  pageNo를 계산 해준다. 
		int pageStartIndex = (pageNo * pageSize) - pageSize;
		
		
		// 조회 조건 파라메터 정의
		String startdate = TreeAdminUtil.isNull(request.getParameter("startDate")); 		// 클래스 시작일
		String enddate = TreeAdminUtil.isNull(request.getParameter("endDate"));   		// 클래스 종료일  	
		String campseq = TreeAdminUtil.isNull(request.getParameter("campSeq")); 			// 캠퍼스 코드 
		String useyn = TreeAdminUtil.isNull(request.getParameter("useYn")); 				// 상태(사용여부)
		String classNm = TreeAdminUtil.isNull(request.getParameter("classNm")); 			// 클래스명 
		
		logger.debug("startdate= "+startdate);
		logger.debug("enddate= "+enddate);
		logger.debug("campseq= "+campseq);
		logger.debug("useyn= "+useyn);
		logger.debug("classNm= "+classNm);
		
		classInfoCondition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "startYmd"));
		classInfoCondition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
		
		classInfoCondition.setStartDate(startdate);
		classInfoCondition.setEndDate(enddate);
		classInfoCondition.setCampSeq(campseq);
		classInfoCondition.setUseYn(useyn);
		classInfoCondition.setClassNm(classNm);
		classInfoCondition.setPageStartIndex(pageStartIndex); 		// 리미트 넘버 
		classInfoCondition.setPageNo(pageNo); 		// 페이지수 
		classInfoCondition.setPageSize(pageSize); 	// 페이지 사이즈
		classInfoCondition.setInsInfoSeq(insInfoSeq);
		
		// 클래스 목록 
		List<VSObject> resultList =  classInfoService.getDataList(classInfoCondition);
		int totalCount = classInfoService.getDataListCnt(classInfoCondition);
		
		
        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < resultList.size()) {
            ClassInfo item = (ClassInfo) resultList.get(i);
            item.setRnum(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
		
		// 캠퍼스 목록 (검색 조건용)
		CodeCondition codeCond = new CodeCondition();
		codeCond.setInsInfoSeq(insInfoSeq);
		List<VSObject> campList =  commonService.getCampusList(codeCond);
		
		model.addAttribute("totalCount", totalCount);
		model.addAttribute("listCondition", classInfoCondition);
		model.addAttribute("classInfoList", resultList);
		model.addAttribute("campList", campList); // 캠퍼스 목록 
		model.addAttribute("menuCode", "4");
		model.addAttribute("insInfoSeq", insInfoSeq); // 학원 순번 * 필수 파라메터 
		
		
		return "/amg/classInfoList";
	}
	
	/**
	 * 클래스 등록 페이지로 이동 
	 * @param request
	 * @param response
	 * @param model
	 * @param insInfoSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/classInsert")
	public String classInsert(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "pageNo", required = false) String pageNo	
			, @RequestParam(value = "classNm", required = false) String classNm
			, @RequestParam(value = "startDate", required = false) String startDate
			, @RequestParam(value = "endDate", required = false) String endDate
			, @RequestParam(value = "campSeq", required = false) String campSeq
			) throws Exception {
		logger.debug("classInsert");
		
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		
		// insert 시에도 빈 값을 조회해서 요일 정보를 받아온다. 
		ClassInfoCondition cCond = new ClassInfoCondition();
		// 클래스 스케쥴 정보 조회 
		List<VSObject> schdlList= classInfoService.getSchdlDataList(cCond);
		
		// 해당 기관에서 사용할수 있는 프로그램 목록 조회 
		List<VSObject> progList = classInfoService.getClassProgList(insInfoSeq);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("classNm", classNm);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("campSeq", campSeq);
		
		
		model.addAttribute("schdlList", schdlList);
		model.addAttribute("progList", progList);
		model.addAttribute("menuCode", "4");
		
		return "/amg/classInfoEdit";
	}
	
	/**
	 * 클래스 정보 등록 처리 
	 * @param request
	 * @param response
	 * @param model
	 * @param campSeq
	 * @param classNm
	 * @param progSeq
	 * @param tchMbrId
	 * @param startYmd
	 * @param endYmd
	 * @param detailContent
	 * @param useYn
	 * @param insInfoSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/classInfoAdd", method = RequestMethod.POST)
	public ModelAndView classInfoAdd(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "classNm", required = false) String classNm
			, @RequestParam(value = "progSeq", required = false) String progSeq
			, @RequestParam(value = "tchMbrId", required = false) String[] tchMbrId
			, @RequestParam(value = "startYmd", required = false) String startYmd
			, @RequestParam(value = "endYmd", required = false) String endYmd
			, @RequestParam(value = "detailContent", required = false) String detailContent
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "schdlCheck", required = false) String[] schdlCheck
			, @RequestParam(value = "dayNm", required = false) String[] dayNm
			, @RequestParam(value = "startHH", required = false) String[] startHH
			, @RequestParam(value = "startMM", required = false) String[] startMM
			, @RequestParam(value = "endHH", required = false) String[] endHH
			, @RequestParam(value = "endMM", required = false) String[] endMM
			
			) throws Exception {
		logger.debug("classInfoAdd");
		logger.debug(">>>> startYmd ="+startYmd);
    	logger.debug(">>>> endYmd ="+endYmd);
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		String mbrId = usertDetail.getUsername(); //회원 아이디 
        
        
        
		//  클래스 정보에 시작 시간은 스케쥴표에서 첫번째로 입력한 시간으로 함으로 기획팀(전주경 cp)과 협의함. 
		String startHM = ""; // 클래스 시작시분 
		String endHM = ""; // 클래스 종료 시분 
        String wk = ""; // 요일명 ex ) MON/TUE/
		String dayStr = ""; // 요일의 숫자정보 ex) MON=1, TUE=2..
		
		if(schdlCheck != null &&  schdlCheck.length > 0){
			
			startHM = startHH[0]+":"+startMM[0];
			endHM = endHH[0]+":"+endMM[0];
			
			for(int j=0; j<schdlCheck.length; j++){
				dayStr +=schdlCheck[j]+";";
				
				if(j == schdlCheck.length -1){
					//wk += dayNm[j].toUpperCase();
					wk += dayNm[j];
				}else{
					//wk += dayNm[j].toUpperCase()+"/";
					wk += dayNm[j]+"/";
				}
				
			}
		}
		
	        
		try{
			
	        // 1. 클래스 일반 정보 등록처리 (CLS_INFO)
	        ClassInfo clsInfo = new ClassInfo();
	        
	        clsInfo.setCampSeq(campSeq);
	        clsInfo.setClassNm(classNm);
	        clsInfo.setProgSeq(progSeq);
	        clsInfo.setStartYmd(startYmd);
	        clsInfo.setEndYmd(endYmd);
	        clsInfo.setDetailContent(detailContent);
	        clsInfo.setUseYn(useYn);
	        clsInfo.setRegId(mbrId);
	        
	        clsInfo.setStartHm(startHM);  // 클래스 시작 시간은 스케쥴에서 입력한 첫번째 요일의 시작 시간을 입력한다. 
	        clsInfo.setEndHm(endHM);    // 클래스 종료 시간은 스케쥴에서 입력한 첫번째 요일의 종료 시간을 입력한다.
	        clsInfo.setWkNm(wk);   // 스케쥴이 입력된  요일명칭  ex)  MON/WED/FRI
	        clsInfo.setWk(dayStr);     // 스케쥴이 입력된 요일의 숫자 값 ex) 일요일이 1로 시작함. 2;4;6;
	       
	        // 넘어온 선생님의 아이디를 리스트에 담아서 서비스에 넘긴다. 
	        List<Teacher> tList = new ArrayList<Teacher>();
	        
	        // 넘어온 선생님 아이디를 등록함. 
	        for(int i=0; i<tchMbrId.length; i++){
	        	Teacher tchVo = new Teacher();	

	        	if(!tchMbrId[i].equals("")){
	        		tchVo.setMbrId(tchMbrId[i]);
	            	logger.debug("선생님 아이디는? "+tchMbrId[i]);
	            	tList.add(i, tchVo);
	        	}
	        }
	        
	        clsInfo.setTeacherList(tList);
	        
	        classInfoService.addDataWithResultCodeMsg(clsInfo);
			
	        
	        String genKey = clsInfo.getClassSeq();
	        logger.debug("생성된 클래스의 classSeq="+genKey);
	        
	        //------------------  CLS_INCLS_PROGRESS INSERT (해당 클래스의 레슨1의 1차시와 2차시 데이터 등록 )
	       	// 클래스에 등록된 책의 첫 레슨-첫차시 코드목록 조회
			TodayClassCondition tcCond = new TodayClassCondition();
			tcCond.setClsSeq(genKey);

	    	List<TodayClassVo> lessonList = classInfoService.getFirstLessonList(tcCond);

	    	// 책이 여러개인 경우 첫 레슨/첫차시는 여러건이 될수 있다.
	    	for(int i =0; i < lessonList.size(); i ++){
	    		// 레슨의 첫번째 차시 코드.
	    		String dayNoCd = lessonList.get(i).getDayNoCd();
	    		// TODAY_CLASS insert
	    		classInfoService.addDataTodayClass(genKey, dayNoCd, "1");

	    		TodayClassCondition nextCond = new TodayClassCondition();
	    		nextCond.setClsSeq(genKey);
	    		nextCond.setDayNoCd(dayNoCd);

	    		// 다음 레슨의 첫번째 차시 코드 조회
	    		String nextDayNoCd = classInfoService.getNextLessonDayNoCd(nextCond);
	    		
	    		if(nextDayNoCd != null){
	    			classInfoService.addDataTodayClass(genKey, nextDayNoCd, "2");
	    		}
	    	}
	    	//------------------ CLS_INCLS_PROGRESS INSERT END 
	        
	        
	    	logger.debug(">>>> startYmd ="+startYmd);
	    	logger.debug(">>>> endYmd ="+endYmd);
	    	
	        
	        // 입력된 시작일과 종료일 사이의 날자를 리스트로 만들어서 그 숫자만큼 스케쥴 데이터를 입력함. 
	        List<String> clsYmdList = TreeAdminUtil.getDayList(startYmd, endYmd, dayStr);
	        Calendar cal = Calendar.getInstance();
	        SimpleDateFormat transDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	        // 기간 목록을 루프를 돌면서 insert 처리  
	        for(int h =0; h < clsYmdList.size(); h++){
	        	
	            Date nowDate = transDateFormat.parse(clsYmdList.get(h));
	            
	            // 시작 일부터 시작해서 계산함. 
	            cal.setTime(nowDate);
	            
	            // 특정일의 요일 조회 1:일요일 ~
	            int day = cal.get(Calendar.DAY_OF_WEEK); // 리스트 상의 목록의 요일을 가져온다. 
	            
	            if(schdlCheck != null &&  schdlCheck.length > 0){
		            // 넘어온 선생님 아이디를 등록함. 
		            for(int i=0; i<schdlCheck.length; i++){
		            	ClassSchdl inputVo = new ClassSchdl();
		            	
		            	
		            	System.out.println(">>>> "+clsYmdList.get(h));
		            	
		            	// 현재 날짜의 요일과 체크한 요일이 같은 경우 스케쥴을 인서트 처리함. 
		            	if(schdlCheck[i].equals(String.valueOf(day))){
		                	inputVo.setClsSeq(genKey);
		                	inputVo.setStartHH(startHH[i]);
		                	inputVo.setStartMM(startMM[i]);
		                	inputVo.setEndHH(endHH[i]);
		                	inputVo.setEndMM(endMM[i]);
		                	inputVo.setWk(dayNm[i].toUpperCase());
		                	inputVo.setIngStat("N");
		                	inputVo.setYmd(clsYmdList.get(h));
		                	inputVo.setRegId(mbrId);
		                	inputVo.setModId(mbrId);
		                	
		                	// 스케쥴 데이터 등록 처리 
		            		classInfoService.addSchdlDataWithResultCodeMsg(inputVo);
		                	
		            	}
		            }  
	            }
	        } 
        
        
		}catch(Exception e){
			logger.error(e.toString());
		}
        

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/classInfoList.do");
		
		return mav;
	}
	
	/**
	 * 클래스 정보 상세 페이지 이동
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/classInfoEdit")
	public String classInfoEdit(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "classSeq", required = false) String classSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "pageNo", required = false) String pageNo	
			, @RequestParam(value = "classNm", required = false) String classNm
			, @RequestParam(value = "startDate", required = false) String startDate
			, @RequestParam(value = "endDate", required = false) String endDate
			, @RequestParam(value = "campSeq", required = false) String campSeq
			) throws Exception {
		logger.debug("classInfoEdit");
		
		ClassInfoCondition cCond = new ClassInfoCondition();
		cCond.setClassSeq(classSeq);
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		
		// 해당 기관에서 사용할수 있는 프로그램 목록 조회 
		List<VSObject> progList = classInfoService.getClassProgList(insInfoSeq);
		
		
		// 클래스 일반정보 조회 
		ClassInfo classInfo = (ClassInfo) classInfoService.getData(cCond);
		
		// 클래스에 등록된 선생님 목록 조회 
		List<CodeVo> savetchList = classInfoService.getClassTchList(classSeq);
		
		
		// 클래스 스케쥴 정보 조회 
		List<VSObject> schdlList= classInfoService.getSchdlDataList(cCond);
		
		
		// 진행된 차시가 있는 갯수 조회
		int progressCnt = classInfoService.getClsProgressCnt(classSeq);
		
		String classProgress = "N"; // 수업 진행여부 
		if(progressCnt > 0 ){
			classProgress ="Y";
		}		
		
		//String[] dayArr = {"Sun","Mon","Tue", "Wed", "Thu", "Fri", "Sat"}; // 클래스 스케쥴을 그리기 위한 파라메터 
		//model.addAttribute("dayList",dayArr);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("classNm", classNm);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("campSeq", campSeq);
		
		model.addAttribute("schdlList", schdlList);
		model.addAttribute("progList", progList);
		model.addAttribute("tchList", savetchList);
		model.addAttribute("classInfo", classInfo);
		model.addAttribute("classProgress", classProgress);
		model.addAttribute("menuCode", "4");
		model.addAttribute("viewMode", "U"); // 화면 모드 (수정)
		
		return "/amg/classInfoEdit";
	}

	
	@RequestMapping(value = "/amg/classInfoView")
	public String classInfoView(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "classSeq", required = false) String classSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "pageNo", required = false) String pageNo	
			, @RequestParam(value = "classNm", required = false) String classNm
			, @RequestParam(value = "startDate", required = false) String startDate
			, @RequestParam(value = "endDate", required = false) String endDate
			, @RequestParam(value = "campSeq", required = false) String campSeq
			) throws Exception {
		logger.debug("classInfoView");
		
		ClassInfoCondition cCond = new ClassInfoCondition();
		cCond.setClassSeq(classSeq);
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		
		// 해당 기관에서 사용할수 있는 프로그램 목록 조회 
		List<VSObject> progList = classInfoService.getClassProgList(insInfoSeq);
		
		
		// 클래스 일반정보 조회 
		ClassInfo classInfo = (ClassInfo) classInfoService.getData(cCond);
		
		// 클래스에 등록된 선생님 목록 조회 
		List<CodeVo> savetchList = classInfoService.getClassTchList(classSeq);
		
		
		// 클래스 스케쥴 정보 조회 
		List<VSObject> schdlList= classInfoService.getSchdlDataList(cCond);
		
		
		//String[] dayArr = {"Sun","Mon","Tue", "Wed", "Thu", "Fri", "Sat"}; // 클래스 스케쥴을 그리기 위한 파라메터 
		//model.addAttribute("dayList",dayArr);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("classNm", classNm);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("campSeq", campSeq);
		
		model.addAttribute("schdlList", schdlList);
		model.addAttribute("progList", progList);
		model.addAttribute("tchList", savetchList);
		model.addAttribute("classInfo", classInfo);
		model.addAttribute("menuCode", "4");
		model.addAttribute("viewMode", "U"); // 화면 모드 (수정)
		
		return "/amg/classInfoView";
	}
	
	
	/**
	 * 클래스 정보 수정 저장 처리
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId
	 * @param nm
	 * @param nation
	 * @param memo
	 * @param useYn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/classInfoModify", method = RequestMethod.POST)
	public ModelAndView classInfoModify(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "classSeq", required = false) String classSeq
			, @RequestParam(value = "classNm", required = false) String classNm
			, @RequestParam(value = "progSeq", required = false) String progSeq
			, @RequestParam(value = "tchMbrId", required = false) String[] tchMbrId
			, @RequestParam(value = "startYmd", required = false) String startYmd
			, @RequestParam(value = "endYmd", required = false) String endYmd
			, @RequestParam(value = "detailContent", required = false) String detailContent
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "clsSchdlSeq", required = false) String clsSchdlSeq
			
			, @RequestParam(value = "schdlCheck", required = false) String[] schdlCheck
			, @RequestParam(value = "dayNm", required = false) String[] dayNm
			, @RequestParam(value = "startHH", required = false) String[] startHH
			, @RequestParam(value = "startMM", required = false) String[] startMM
			, @RequestParam(value = "endHH", required = false) String[] endHH
			, @RequestParam(value = "endMM", required = false) String[] endMM
			) throws Exception {
		logger.debug("classInfoModify");

		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		String mbrId = usertDetail.getUsername(); //회원 아이디 
		
		
		//  클래스 정보에 시작 시간은 스케쥴표에서 첫번째로 입력한 시간으로 함으로 기획팀(전주경 cp)과 협의함. 
		String startHM = ""; // 클래스 시작시분 
		String endHM = ""; // 클래스 종료 시분 
        String wk = ""; // 요일명 ex ) MON/TUE/
		String dayStr = ""; // 요일의 숫자정보 ex) MON=1, TUE=2..
		
		if(schdlCheck != null &&  schdlCheck.length > 0){
			startHM = startHH[0]+":"+startMM[0];
			endHM = endHH[0]+":"+endMM[0];
			
			for(int j=0; j<schdlCheck.length; j++){
				dayStr +=schdlCheck[j]+";";
				
				if(j == schdlCheck.length -1){
					wk += dayNm[j];	
				}else{
					wk += dayNm[j]+"/";
				}
				
			}
		}
		
	        
		try{
			
        // 1. 클래스 일반 정보 수정처리 (CLS_INFO)
        ClassInfo clsInfo = new ClassInfo();
        
        clsInfo.setClassSeq(classSeq);
        clsInfo.setCampSeq(campSeq);
        clsInfo.setClassNm(classNm);
        clsInfo.setProgSeq(progSeq);
        clsInfo.setStartYmd(startYmd);
        clsInfo.setEndYmd(endYmd);
        clsInfo.setDetailContent(detailContent);
        clsInfo.setUseYn(useYn);
        clsInfo.setModId(mbrId);
        clsInfo.setStartHm(startHM);  // 클래스 시작 시간은 스케쥴에서 입력한 첫번째 요일의 시작 시간을 입력한다. 
        clsInfo.setEndHm(endHM);    // 클래스 종료 시간은 스케쥴에서 입력한 첫번째 요일의 종료 시간을 입력한다.
        clsInfo.setWkNm(wk);   // 스케쥴이 입력된  요일명칭  ex)  MON/WED/FRI
        clsInfo.setWk(dayStr);     // 스케쥴이 입력된 요일의 숫자 값 ex) 일요일이 1로 시작함. 2;4;6;
		
        
        // 넘어온 선생님의 아이디를 리스트에 담아서 서비스에 넘긴다. 
        List<Teacher> tList = new ArrayList<Teacher>();
        
        // 넘어온 선생님 아이디를 List에  ADD
        for(int i=0; i<tchMbrId.length; i++){
        	Teacher tchVo = new Teacher();
        	
        	if(!tchMbrId[i].equals("")){
            	tchVo.setMbrId(tchMbrId[i]);
            	logger.debug("선생님 아이디는? "+tchMbrId[i]);
            	tList.add(i, tchVo);
        	}

        }
        
        clsInfo.setTeacherList(tList);
        
        
        
		// 2. 클래스 스케쥴 정보 삭제 조건 셋팅 
        ClassSchdlCondition csCond = new ClassSchdlCondition();
        csCond.setClsSeq(classSeq);

        
        // 3. 스케쥴 테이블에 넣을 데이터를 만든다. 
        List<ClassSchdl> schList = new ArrayList<ClassSchdl>();
        
        // 입력된 시작일과 종료일 사이의 날자를 리스트로 만들어서 그 숫자만큼 스케쥴 데이터를 입력함. 
        List<String> clsYmdList = TreeAdminUtil.getDayList(startYmd, endYmd, dayStr);
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat transDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        // 기간 목록을 루프를 돌면서 insert 처리  
        for(int h =0; h < clsYmdList.size(); h++){
            Date nowDate = transDateFormat.parse(clsYmdList.get(h));
            // 시작 일부터 시작해서 계산함. 
            cal.setTime(nowDate);
            
            // 특정일의 요일 조회 1:일요일 ~
            int day = cal.get(Calendar.DAY_OF_WEEK); // 리스트 상의 목록의 요일을 가져온다. 
            
            if(schdlCheck != null &&  schdlCheck.length > 0){
	            // 넘어온 선생님 아이디를 등록함. 
	            for(int i=0; i<schdlCheck.length; i++){
	            	ClassSchdl inputVo = new ClassSchdl();
	            	
	            	// 현재 날짜의 요일과 체크한 요일이 같은 경우 스케쥴을 인서트 처리함. 
	            	if(schdlCheck[i].equals(String.valueOf(day))){
	                	inputVo.setClsSeq(classSeq);
	                	inputVo.setStartHH(startHH[i]);
	                	inputVo.setStartMM(startMM[i]);
	                	inputVo.setEndHH(endHH[i]);
	                	inputVo.setEndMM(endMM[i]);
	                	inputVo.setWk(dayNm[i].toUpperCase());
	                	inputVo.setIngStat("N");
	                	inputVo.setYmd(clsYmdList.get(h));
	                	inputVo.setRegId(mbrId);
	                	inputVo.setModId(mbrId);
	                	
	                	schList.add(inputVo);
	                	
	            	}
	            }  
            }
        } 
        
        // Transaction을 위해서 서비스 단으로 모든 데이터를 쑝 보낸다. 
		classInfoService.modifyDataWithResultCodeMsg(clsInfo , csCond, schList);
        
        //--- 클래스에 등록되어있던 차시 정보 삭제 
		classInfoService.deleteClsProgress(classSeq);
		
        //------------------  CLS_INCLS_PROGRESS INSERT (해당 클래스의 레슨1의 1차시와 2차시 데이터 등록 )
       	// 클래스에 등록된 책의 첫 레슨-첫차시 코드목록 조회
		TodayClassCondition tcCond = new TodayClassCondition();
		tcCond.setClsSeq(classSeq);

    	List<TodayClassVo> lessonList = classInfoService.getFirstLessonList(tcCond);

    	// 책이 여러개인 경우 첫 레슨/첫차시는 여러건이 될수 있다.
    	for(int i =0; i < lessonList.size(); i ++){
    		// 레슨의 첫번째 차시 코드.
    		String dayNoCd = lessonList.get(i).getDayNoCd();
    		// TODAY_CLASS insert
    		classInfoService.addDataTodayClass(classSeq, dayNoCd, "1");

    		TodayClassCondition nextCond = new TodayClassCondition();
    		nextCond.setClsSeq(classSeq);
    		nextCond.setDayNoCd(dayNoCd);

    		// 다음 레슨의 첫번째 차시 코드 조회
    		String nextDayNoCd = classInfoService.getNextLessonDayNoCd(nextCond);
    		if(nextDayNoCd != null){
    			// TODAY_CLASS insert
    			classInfoService.addDataTodayClass(classSeq, nextDayNoCd, "2");
    		}
    	}
    	//------------------ CLS_INCLS_PROGRESS INSERT END 
    	
    	
		}catch(Exception e){
			logger.error(e.toString());
		}


		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/classInfoList.do");
		return mav;
	}
	
	
    @RequestMapping(value = "/amg/getCampTchList", method = RequestMethod.POST)
    @ResponseBody
    public VSResult getCampTchList(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam(value = "campSeq", required = false) String campSeq) throws Exception {

        VSResult result = new VSResult();

        try {

        	List<CodeVo> tchList = classInfoService.getCampTchList(campSeq);
        	result.setResult(tchList);
        	result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
        	result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            e.printStackTrace();
        }

        return result;
    }
    
    
}
