package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSCondition;

public class ClassSchdlCondition extends VSCondition {

	private String clsSeq;
	private String clsSchdlSeq;
	
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getClsSchdlSeq() {
		return clsSchdlSeq;
	}
	public void setClsSchdlSeq(String clsSchdlSeq) {
		this.clsSchdlSeq = clsSchdlSeq;
	}
}
