package com.visangesl.treeadmin.amg.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public class ClassInfo extends VSObject {

	private String rnum;
	private String classSeq;
	private String classNm;
	private String progSeq;
	private String progNm;
	private String campSeq;
	private String campNm;
	private String startYmd;
	private String endYmd;
	private String wkNm;
	private String wk;
	private String startHm;
	private String endHm;
	private String detailContent;
	private String useYn;
	private List<Teacher> teacherList;
	private String teacherNm;
	private String teacherId;
	private String lessonTime;
	private String studentCount;
	private String totCnt;
	private String regId;
	private String modId;
	
	
	public String getRnum() {
		return rnum;
	}
	public void setRnum(String rnum) {
		this.rnum = rnum;
	}
	public String getClassSeq() {
		return classSeq;
	}
	public void setClassSeq(String classSeq) {
		this.classSeq = classSeq;
	}
	public String getClassNm() {
		return classNm;
	}
	public void setClassNm(String classNm) {
		this.classNm = classNm;
	}
	public String getProgSeq() {
		return progSeq;
	}
	public void setProgSeq(String progSeq) {
		this.progSeq = progSeq;
	}
	public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getStartYmd() {
		return startYmd;
	}
	public void setStartYmd(String startYmd) {
		this.startYmd = startYmd;
	}
	public String getEndYmd() {
		return endYmd;
	}
	public void setEndYmd(String endYmd) {
		this.endYmd = endYmd;
	}
	public String getWkNm() {
		return wkNm;
	}
	public void setWkNm(String wkNm) {
		this.wkNm = wkNm;
	}
	public String getWk() {
		return wk;
	}
	public void setWk(String wk) {
		this.wk = wk;
	}
	public String getStartHm() {
		return startHm;
	}
	public void setStartHm(String startHm) {
		this.startHm = startHm;
	}
	public String getEndHm() {
		return endHm;
	}
	public void setEndHm(String endHm) {
		this.endHm = endHm;
	}
	public String getDetailContent() {
		return detailContent;
	}
	public void setDetailContent(String detailContent) {
		this.detailContent = detailContent;
	}
	public String getCampNm() {
		return campNm;
	}
	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}
	public String getProgNm() {
		return progNm;
	}
	public void setProgNm(String progNm) {
		this.progNm = progNm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public List<Teacher> getTeacherList() {
		return teacherList;
	}
	public void setTeacherList(List<Teacher> teacherList) {
		this.teacherList = teacherList;
	}
	public String getTeacherNm() {
		return teacherNm;
	}
	public void setTeacherNm(String teacherNm) {
		this.teacherNm = teacherNm;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getLessonTime() {
		return lessonTime;
	}
	public void setLessonTime(String lessonTime) {
		this.lessonTime = lessonTime;
	}
	public String getStudentCount() {
		return studentCount;
	}
	public void setStudentCount(String studentCount) {
		this.studentCount = studentCount;
	}
	public String getTotCnt() {
		return totCnt;
	}
	public void setTotCnt(String totCnt) {
		this.totCnt = totCnt;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	
}
