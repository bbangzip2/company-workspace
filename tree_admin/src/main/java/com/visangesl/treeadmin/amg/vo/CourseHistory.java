package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class CourseHistory extends VSObject {
	
	private String mbrId;
	private String clsSeq;
	private String regId;
	
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}

	
}
