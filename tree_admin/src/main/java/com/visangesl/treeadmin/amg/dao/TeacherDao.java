package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class TeacherDao extends TreeSqlSessionDaoSupport {
    
    @SuppressWarnings("unused")
    private final Log logger = LogFactory.getLog(this.getClass());
    
    public VSObject getData(VSObject vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("teacher.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("teacher.getDataList", vsObject);
    	return result;
    }
    
    public int getDataListCnt(VSObject vsObject) throws Exception {
    	
    	return getSqlSession().selectOne("teacher.getDataListCnt", vsObject);
    }
    
    public int deleteTeacherCampus(VSObject vsObject) throws Exception {
        return getSqlSession().delete("teacher.deleteTeacherCampus", vsObject);
    }
    
    public int addTeacherCampus(VSObject vsObject) throws Exception {
        return getSqlSession().delete("teacher.addTeacherCampus", vsObject);
    }
    
    public List<VSObject> getCampusList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("teacher.getCampusList", vsObject);
        return result;
    }
    
    public String getCampusSeq(VSCondition dataCondition) throws Exception {
        return getSqlSession().selectOne("teacher.getCampusSeq", dataCondition);
    }
}
