package com.visangesl.treeadmin.amg.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.AssignClassService;
import com.visangesl.treeadmin.amg.vo.AssignClass;
import com.visangesl.treeadmin.amg.vo.ClassInfoCondition;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.amg.vo.StudentCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class AssignClassController implements BaseController {

	@Autowired
	private AssignClassService service;
	
	@Autowired
	private CommonService commonService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 반배정  목록 조회 
	 * @param request
	 * @param response
	 * @param model
	 * @param campSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/assignClassList")
	public String assignClassList(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "startDate", required = false, defaultValue="") String startDate
			, @RequestParam(value = "endDate", required = false, defaultValue="") String endDate
			, @RequestParam(value = "campSeq", required = false, defaultValue="") String campSeq
			, @RequestParam(value = "classSeq", required = false, defaultValue="") String classSeq
			, @RequestParam(value = "useYn", required = false, defaultValue="") String useYn
			) throws Exception {
		logger.debug("assignClassList");
		
		ClassInfoCondition classInfoCondition = new ClassInfoCondition();
		
		int pageNo = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageSize"), "20")); // 기본 갯수는 20개 
		
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 

		
		// LIMIT 함수로 페이징 처리 하니깐 1페이지 일때 0 부터 들어가야 한다. 그래서  pageNo를 계산 해준다. 
		int pageStartIndex = (pageNo * pageSize) - pageSize;
		
		classInfoCondition.setStartDate(startDate);
		classInfoCondition.setEndDate(endDate);
		classInfoCondition.setCampSeq(campSeq);
		classInfoCondition.setClassSeq(classSeq);
		classInfoCondition.setUseYn(useYn);
		classInfoCondition.setPageStartIndex(pageStartIndex); 		// 리미트 넘버 
		classInfoCondition.setPageNo(pageNo); 		// 페이지수 
		classInfoCondition.setPageSize(pageSize); 	// 페이지 사이즈
		classInfoCondition.setInsInfoSeq(insInfoSeq);
		
		// 클래스 목록 
		List<VSObject> resultList =  service.getAssignClassList(classInfoCondition);
		int totalCount = service.getAssignClassListCnt(classInfoCondition);
		
		
        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < resultList.size()) {
            AssignClass item = (AssignClass) resultList.get(i);
            item.setRnum(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
		
		
		model.addAttribute("totalCount", totalCount);
		model.addAttribute("listCondition", classInfoCondition);
		model.addAttribute("classInfoList", resultList);
		
		
		return "/amg/assignClassList";
	}
	
	
	@RequestMapping(value = "/amg/classSettingPop")
	public String classSettingPop(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "classSeq", required = false) String classSeq
			, @RequestParam(value = "searchWord", required = false) String searchWord
			, @RequestParam(value = "order", required = false, defaultValue="ASC") String order
			
			) throws Exception {
		logger.debug("classSettingPop");
		
		ClassInfoCondition cond1 = new ClassInfoCondition();
		cond1.setCampSeq(campSeq);
		cond1.setSearchWord(searchWord);
		cond1.setOrder(order);
		// List 해당 캠퍼스의 수강회원/ 정회원 목록
		List<VSObject> campusStuList = service.getCampusStudentList(cond1);
		
		// 과거 1달 이내에 진행중인 클래스 목록 (recent class list)
		List<VSObject> recentClassList = service.getRecentClassList(cond1);
		
		
		ClassInfoCondition cond2 = new ClassInfoCondition();
		cond2.setCampSeq(campSeq);
		cond2.setClassSeq(classSeq);
		cond2.setOrder(order);		
		// 해당 클래스에 속한 회원 목록
		List<VSObject> classStuList = service.getCampusStudentList(cond2);
		
		// 클래스 기본 정보 (클래스명, 선생님아이디, 선생님 이름, 시간, 요일 )
		List<VSObject> classDetailInfo = service.getClassDetailInfo(cond2);
		
		model.addAttribute("campSeq", campSeq);
		model.addAttribute("classSeq", classSeq);
		model.addAttribute("recentClassList", recentClassList);
		model.addAttribute("campusStuList", campusStuList);
		model.addAttribute("classStuList", classStuList);
		model.addAttribute("classDetailInfo", classDetailInfo);
		model.addAttribute("order",order);
		
		return "/amg/classSettingPop";
	}
	
	/**
	 * 반배치 미정 학생 목록 조회
	 * @param request
	 * @param response
	 * @param model
	 * @param campSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/unassignStudentPop")
	public String unassignedPop(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "order", required = false, defaultValue="ASC") String order
			) throws Exception {
		logger.debug("unassignedPop");
		
		
		ClassInfoCondition cond = new ClassInfoCondition();
		cond.setCampSeq(campSeq);
		cond.setOrder(order);
		
		List<VSObject> resultList =  service.getUnassignStudentList(cond);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("campSeq",campSeq);
		model.addAttribute("order",order);
		
		return "/amg/unassignStudentPop";
	}
	
	
	@RequestMapping(value = "/amg/classSettingAdd")
	@ResponseBody
    public VSResult<VSObject> classSettingAdd(HttpServletRequest request, HttpServletResponse response, Model model,
    		@RequestParam(value = "campSeq", required = false) String campSeq,
    		@RequestParam(value = "classSeq", required = false) String classSeq,
            @RequestParam(value = "addId", required = false) String[] addIdArr,
            @RequestParam(value = "delId", required = false) String[] delIdArr
            ) throws Exception {
        logger.debug("classSettingAdd");
        
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		String mbrId = usertDetail.getUsername(); //회원 아이디 
		
        
        List<IcmRelVo> insertList = new ArrayList<IcmRelVo>();
        List<IcmRelVo> deleteList = new ArrayList<IcmRelVo>();
        VSResult result = new VSResult();

        // insert  List make
        if( addIdArr != null && addIdArr.length > 0){
            for(int i=0; i < addIdArr.length; i++){
            	
            	// 화면에서 넘어온 목록 순서에 따라서 정렬 순서를 정해준다. 
            	//String sortOrder = Integer.toString(i+1);
            	logger.debug("[addIdArr] "+addIdArr[i]);
            	IcmRelVo insertVo = new IcmRelVo();
            	
            	insertVo.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
            	insertVo.setIcmSeq(addIdArr[i]);  // memberId
            	insertVo.setRelSect(TreeProperties.getProperty("tree_re_class")); //RE003
            	insertVo.setRelSeq(classSeq);  // classSeq
            	
            	insertVo.setCond_icmSect(TreeProperties.getProperty("tree_re_member")); //RE004
            	insertVo.setCond_icmSeq(addIdArr[i]);
            	insertVo.setCond_relSect(TreeProperties.getProperty("tree_re_campus")); //RE003
            	insertVo.setCond_relSeq(campSeq);
        		
            	insertList.add(insertVo);
            	
            }
        }

        // 삭제할 레슨을 객체 에 담는다. 
        // 클래스 소속의 데이터를 캠퍼스 소속으로 업데이트 처리해준다.
        
        if( delIdArr != null && delIdArr.length > 0){
        	for(int h=0; h < delIdArr.length; h++){
        		
        		IcmRelVo deleteVo = new IcmRelVo();
        		
        		logger.debug("[delSeq] "+delIdArr[h]);
        		
        		deleteVo.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
        		deleteVo.setIcmSeq(delIdArr[h]);  // memberId
        		deleteVo.setRelSect(TreeProperties.getProperty("tree_re_campus")); //RE002
        		deleteVo.setRelSeq(campSeq);  // campSeq
        		
        		// 업데이트 할 조건 파라메터 셋팅 (class 조건)
        		deleteVo.setCond_icmSect(TreeProperties.getProperty("tree_re_member")); //RE004
        		deleteVo.setCond_icmSeq(delIdArr[h]);
        		deleteVo.setCond_relSect(TreeProperties.getProperty("tree_re_class")); //RE003
        		deleteVo.setCond_relSeq(classSeq);
        		
        		deleteList.add(deleteVo);
        		
        	}
        }
        
       try{
    	   
    	   service.classSettingAdd( insertList, deleteList, mbrId);
    	   
			result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
		} catch (Exception e) {
			logger.error(e.toString());
			result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            logger.error(e.toString());
       }
       
        return result;
    }
	
	
	@RequestMapping(value = "/amg/getAssignClassSearchList")
	@ResponseBody
	public VSResult<HashMap<String, Object>> getAssignClassSearchList(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "clsSeq", required = false) String clsSeq
			, @RequestParam(value = "searchWord", required = false) String searchWord
			, @RequestParam(value = "order", required = false) String order
			) throws Exception {
		logger.debug("getAssignClassSearchList");
		
		VSResult result = new VSResult();
		
		ClassInfoCondition cond1 = new ClassInfoCondition();
		cond1.setCampSeq(campSeq);
		cond1.setClassSeq(clsSeq);
		cond1.setSearchWord(searchWord);
		cond1.setOrder(order);
		// List 해당 캠퍼스의 수강회원/ 정회원 목록
		List<VSObject> resultList = service.getCampusStudentList(cond1);
		
		model.addAttribute("resultList", resultList);
		result.setResult(resultList);
		
		return result;
	}
	
    @RequestMapping(value = "/amg/assignExcelPop")
    public String assignExcelPop(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        return "/amg/assignExcelPop";
    }
    
    
	
    
    @RequestMapping(value = "/amg/assignExcelUpload", method = RequestMethod.POST)
    public ModelAndView assignExcelUpload(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("assignExcelUpload ======================================================");
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        
        // DB에 넣을 국가코드와 엑셀파일을 넘긴다.
        MemberExcelCondition excelCondition = new MemberExcelCondition();
        excelCondition.setMultipartFile(multipartFile);
        
        // 리턴 메세지 
        VSResult<?> result = null;
        
        try{
            result = service.inputExcelData(excelCondition);
        } catch (TreeRuntimeException e) {
            result = new VSResult<>();
            result.setCode(e.getCode());
            result.setMessage(e.getMsg());
            
            logger.debug(e.toString());
        }
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/amg/commonExcelResult");
        mav.addObject("subject", "Assign students");
        mav.addObject("result", result);
        return mav;
    }
	
	
    
}
