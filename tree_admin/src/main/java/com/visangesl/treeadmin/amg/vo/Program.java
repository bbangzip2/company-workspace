package com.visangesl.treeadmin.amg.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.lcms.program.vo.Item;

/**
 * PROG 테이블의 VO 클래스.
 * @author kimankim
 */
//@XmlRootElement(name="program")
public class Program extends VSObject {
	private String rn;
	private String progSeq;
	private String instituteSeq;
	private String progNm;
	private String progDescr;
	private String useYn;
	private String regDate;
	private String regId;
	private String modDate;
	private String modId;
	private List<Item> itemList;
	private int useClassCnt;
	
	
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getProgSeq() {
		return progSeq;
	}
	public void setProgSeq(String progSeq) {
		this.progSeq = progSeq;
	}
	public String getProgNm() {
		return progNm;
	}
	public void setProgNm(String progNm) {
		this.progNm = progNm;
	}
	public String getProgDescr() {
		return progDescr;
	}
	public void setProgDescr(String progDescr) {
		this.progDescr = progDescr;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public List<Item> getItemList() {
		return itemList;
	}
	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	public int getUseClassCnt() {
		return useClassCnt;
	}
	public void setUseClassCnt(int useClassCnt) {
		this.useClassCnt = useClassCnt;
	}
	
}
