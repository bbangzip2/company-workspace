package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.Program;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface ProgramService {
	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;
	
	// select List Data 
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;
	
	// select List Count 
	public int getDataListCnt(VSCondition dataCondition) throws Exception;
	
	// 기관에 등록 된 상품 목록 조회 
	public List<VSObject> getInstituteItemList(VSCondition dataCondition) throws Exception;
	
	// 프로그램에 등록된 아이템 목록 조회 
	public List<VSObject> getProgItemList(String progSeq) throws Exception;
	
	
	public VSResult modifyProgramInfoData(Program program , String[] prodVal) throws Exception;
	
	public VSResult addProgramInfoData(Program program , String[] prodVal) throws Exception;
	
	public VSResult addProgramItemData(VSObject vsObject) throws Exception ;
	
	public VSResult deleteProgramItemData(VSObject vsObject) throws Exception;
}
