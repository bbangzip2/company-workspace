package com.visangesl.treeadmin.amg.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

//@XmlRootElement(name="student")
public class Student extends VSObject {
	private String rn;
	private String mbrId;
	private String name;
	private String nickName;
	private String profilePhotopath;
	private String email;
	private String sexSect;
	private String telNo;
	private String useYn;
	private String birthday;
	private String sect;
	private String insSeq;
	private String insNm;
	private String regDate;
	private String quitDate;
	private List<ClassInfo> classInfo;
	
	public String getRn() {
        return rn;
    }
    public void setRn(String rn) {
        this.rn = rn;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getProfilePhotopath() {
        return profilePhotopath;
    }
    public void setProfilePhotopath(String profilePhotopath) {
        this.profilePhotopath = profilePhotopath;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getSexSect() {
        return sexSect;
    }
    public void setSexSect(String sexSect) {
        this.sexSect = sexSect;
    }
    public String getTelNo() {
        return telNo;
    }
    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public String getSect() {
        return sect;
    }
    public void setSect(String sect) {
        this.sect = sect;
    }
    public String getInsSeq() {
        return insSeq;
    }
    public void setInsSeq(String insSeq) {
        this.insSeq = insSeq;
    }
    public String getInsNm() {
        return insNm;
    }
    public void setInsNm(String insNm) {
        this.insNm = insNm;
    }
    public List<ClassInfo> getClassInfo() {
		return classInfo;
	}
	public void setClassInfo(List<ClassInfo> classInfo) {
		this.classInfo = classInfo;
	}
    public String getRegDate() {
        return regDate;
    }
    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }
    public String getQuitDate() {
        return quitDate;
    }
    public void setQuitDate(String quitDate) {
        this.quitDate = quitDate;
    }
}
