package com.visangesl.treeadmin.amg.vo;

import com.visangesl.treeadmin.vo.VSListCondition;


public class ClassInfoCondition extends VSListCondition {

	private String rn;
	private String instituteSeq;
	private String campSeq;
	private String classSeq;
	private String classNm;
	private String useYn;
	
	private String startDate;
	private String endDate;
	
	private String periodOrder; // 수업 기간 정렬 
	private String clasNmOrder; //캠퍼스명 정렬
	private String insInfoSeq; 
	
	private String searchWord;
	private String order;
	
	private String orderColumn;
	private String sort; // 오름차순인지 내림차순인지	
	
	
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getClassSeq() {
		return classSeq;
	}
	public void setClassSeq(String classSeq) {
		this.classSeq = classSeq;
	}
	public String getClassNm() {
		return classNm;
	}
	public void setClassNm(String classNm) {
		this.classNm = classNm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getPeriodOrder() {
		return periodOrder;
	}
	public void setPeriodOrder(String periodOrder) {
		this.periodOrder = periodOrder;
	}
	public String getClasNmOrder() {
		return clasNmOrder;
	}
	public void setClasNmOrder(String clasNmOrder) {
		this.clasNmOrder = clasNmOrder;
	}
	public String getInsInfoSeq() {
		return insInfoSeq;
	}
	public void setInsInfoSeq(String insInfoSeq) {
		this.insInfoSeq = insInfoSeq;
	}
	public String getSearchWord() {
		return searchWord;
	}
	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
	
}
