package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSCondition;

public class MemberInstituteInfoCondition extends VSCondition {
	private String mbrId;
	private int instituteSeq;
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public int getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(int instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	
	
}
