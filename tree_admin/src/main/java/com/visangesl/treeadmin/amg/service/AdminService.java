package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.AdminCondition;
import com.visangesl.treeadmin.amg.vo.AdminMember;
import com.visangesl.treeadmin.vo.VSResult;


@Service
public interface AdminService {
	// Insert 
	public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception;
    // Insert Relation  
	public VSResult addDataIcmRel(VSObject vsObject) throws Exception;
    // List 
	public List<VSObject> getDataList(VSObject VSObject) throws Exception;
	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;
	// Update
	public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	public VSObject getDataCampInfo(VSObject vsObject) throws Exception;
	

}
