package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface AssignClassService  {
	
	// select List Data 
	public List<VSObject> getAssignClassList(VSCondition dataCondition) throws Exception;
	
	// select List Count 
	public int getAssignClassListCnt(VSCondition dataCondition) throws Exception;
	
	public List<VSObject> getUnassignStudentList(VSCondition dataCondition) throws Exception;
	
	public List<VSObject> getCampusStudentList(VSCondition dataCondition) throws Exception;
	
	public List<VSObject> getClassDetailInfo(VSCondition dataCondition) throws Exception;
	
	public List<VSObject> getRecentClassList(VSCondition dataCondition) throws Exception;
	
	public VSResult classSettingAdd(List<IcmRelVo> insertList, List<IcmRelVo> deleteList, String mbrId)throws Exception;
	
	public int addCourseHistory(VSObject vsObject)throws Exception;
	
	public VSResult<?> inputExcelData(VSObject vsObject) throws Exception;
		
}
