package com.visangesl.treeadmin.amg.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.CampusService;
import com.visangesl.treeadmin.amg.service.InstituteService;
import com.visangesl.treeadmin.amg.vo.Campus;
import com.visangesl.treeadmin.amg.vo.CampusCondition;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.member.vo.MemberCondition;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class CampusController implements BaseController {

	@Autowired
	private CampusService campusService;
	
	@Autowired
    private InstituteService instituteService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 캠퍼스 목록 
	 * @param request
	 * @param response
	 * @param model
	 * @param orderBy
	 * @param sort
	 * @param useYn
	 * @param searchKey
	 * @param pageSize
	 * @param pageNo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/campusList")
	public String campusList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "orderBy", required = false) String orderBy,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "useYn", required = false) String useYn,
			@RequestParam(value = "searchKey", required = false) String searchKey,
			@RequestParam(value = "pageSize", required = false) String pageSize,
			@RequestParam(value = "pageNo", required = false) String pageNo
			) throws Exception {
		
		logger.debug("campusList");
		String insSeq =  ""; // 기관 순번 
		
		CampusCondition condition = new CampusCondition();
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 기관코드 
	    // 기관코드값이 없으면 유저아이디와 등급으로 기관코드값을 찾는다.
		if(TreeAdminUtil.isNull(insInfoSeq) == ""){
		    String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

            MemberCondition memberCondition = new MemberCondition();
            memberCondition.setMbrId(userDetail.getUsername());
            memberCondition.setMbrGrade(mbrGrade);
            
            insInfoSeq = instituteService.getInstituteSeq(memberCondition);
            userDetail.setInsSeq(insInfoSeq);
		}
		
		condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "REG_DATE"));
		condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
		
		if(useYn != null && useYn.length() > 0) {
			condition.setUseYn(useYn);
		}
		
		if(searchKey != null && searchKey.length() > 0) {
			condition.setSearchKey(searchKey);
		}

		// 현재 페이지 번호와 한페이지에 표시할 갯수 설정.
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
		
	    condition.setInsInfoSeq(insInfoSeq);
		
		
		// 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
		// LIMIT 함수로 페이징 처리 하니깐 1페이지 일때 0 부터 들어가야 한다. 그래서  pageNo를 계산 해준다. 
		int pageStartIndex = (condition.getPageNo() -  1) * condition.getPageSize();
		
		condition.setPageStartIndex(pageStartIndex);
		
		List<VSObject> campusList = campusService.getDataList(condition);
		
		// 리스트에 역순 번호 매기기.
		
		model.addAttribute("condition", condition);
		model.addAttribute("campusList", campusList);
		model.addAttribute("menuCode", "3");
		
		return "/amg/campusList";
	}
	
	/**
	 * 캠퍼스 검색 
	 * @param request
	 * @param response
	 * @param model
	 * @param useYn
	 * @param searchKey
	 * @param pageSize
	 * @param pageNo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/campusSearch")
	public String campusSearch(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "orderBy", required = false) String orderBy,
			@RequestParam(value = "sort", required = false) String sort,			
			@RequestParam(value = "useYn", required = false) String useYn,
			@RequestParam(value = "searchKey", required = false) String searchKey,
			@RequestParam(value = "pageSize", required = false) String pageSize,
			@RequestParam(value = "pageNo", required = false) String pageNo
			) throws Exception {
		
		logger.debug("campusSearch");
		
		CampusCondition condition = new CampusCondition();
		
		condition.setOrderColumn(orderBy);
		condition.setSort(sort);
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		
		
		if(useYn != null && useYn.length() > 0) {
			condition.setUseYn(useYn);
		}
		
		if(searchKey != null && searchKey.length() > 0) {
			condition.setSearchKey(searchKey);
		}
		
		// 현재 페이지 번호와 한페이지에 표시할 갯수 설정.
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
		condition.setInsInfoSeq(insInfoSeq);
		
		// 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
		int pageStartIndex = (condition.getPageNo() - 1) * condition.getPageSize();
		condition.setPageStartIndex(pageStartIndex);
		
		List<VSObject> campusList = campusService.getDataList(condition);
		
		model.addAttribute("condition", condition);
		model.addAttribute("campusList", campusList);
		model.addAttribute("menuCode", "3");
		// TODO: 임의의 멤버 ID값으로 먼저 처리.
		model.addAttribute("mbrId", "esl");
		
		return "/amg/campusList";
	}
	
	/**
	 * 캠퍼스 정보 수정 
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId
	 * @param campSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/campusEdit")
	public String campusEdit(HttpServletRequest request, HttpServletResponse response, Model model 
			, @RequestParam(value = "mbrId", required = false) String mbrId
			, @RequestParam(value = "campSeq", required = false) String campSeq
			
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "searchKey", required = false) String searchKey
			, @RequestParam(value = "pageNo", required = false) String pageNo			
			) throws Exception {
		logger.debug("campusEdit");
		
		
		CampusCondition condition = new CampusCondition();
		condition.setCampSeq(campSeq);
		
		Campus campus = new Campus();
		campus = (Campus)campusService.getData(condition);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("searchKey", searchKey);
		model.addAttribute("pageNo", pageNo);
		
		model.addAttribute("campus", campus);
		model.addAttribute("menuCode", "3");
		model.addAttribute("mbrId", mbrId);
		
		return "/amg/campusEdit";
	}
	
	@RequestMapping(value = "/amg/campusView")
	public String campusView(HttpServletRequest request, HttpServletResponse response, Model model 
			, @RequestParam(value = "mbrId", required = false) String mbrId
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "searchKey", required = false) String searchKey
			, @RequestParam(value = "pageNo", required = false) String pageNo		
			) throws Exception {
		logger.debug("campusView");
		
		
		CampusCondition condition = new CampusCondition();
		condition.setCampSeq(campSeq);
		
		Campus campus = new Campus();
		campus = (Campus)campusService.getData(condition);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("searchKey", searchKey);
		model.addAttribute("pageNo", pageNo);
		
		model.addAttribute("campus", campus);
		model.addAttribute("menuCode", "3");
		model.addAttribute("mbrId", mbrId);
		
		return "/amg/campusView";
	}
	
	
	/**
	 * 캠퍼스 등록 화면 이동 
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/campusInsert")
	public String campusInsert(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "mbrId", required = false) String mbrId
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "searchKey", required = false) String searchKey
			, @RequestParam(value = "pageNo", required = false) String pageNo	
			
			) throws Exception {
		logger.debug("campusInsert");
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("searchKey", searchKey);
		model.addAttribute("pageNo", pageNo);
		
		model.addAttribute("menuCode", "3");
		model.addAttribute("mbrId", mbrId);
		
		
		return "/amg/campusEdit";
	}
	
	/**
	 * 캠퍼스 등록 
	 * @param request
	 * @param response
	 * @param model
	 * @param campNm
	 * @param campId
	 * @param areaNm
	 * @param detailInfo
	 * @param useYn
	 * @param insInfoSeq
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/campusAdd", method = RequestMethod.POST)
	public ModelAndView campusAdd(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campNm", required = false) String campNm
			, @RequestParam(value = "campId", required = false) String campId
			, @RequestParam(value = "areaNm", required = false) String areaNm
			, @RequestParam(value = "detailInfo", required = false) String detailInfo
			, @RequestParam(value = "useYn", required = false) String useYn
			) throws Exception {
		logger.debug("campusAdd");
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		String mbrId = usertDetail.getUsername(); //회원 아이디 

		
		Campus campus = new Campus();
		campus.setCampNm(campNm);
		campus.setCampId(campId);
		campus.setAreaNm(areaNm);
		campus.setDetailInfo(detailInfo);
		campus.setUseYn(useYn);
		campus.setInsInfoSeq(insInfoSeq);
		campus.setMbrId(mbrId);
		
		campusService.addDataWithResultCodeMsg(campus);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/campusList.do");
		
		return mav;
	}
	
	/**
	 * 캠퍼스 정보 수정 
	 * @param request
	 * @param response
	 * @param model
	 * @param campSeq
	 * @param campNm
	 * @param areaNm
	 * @param detailInfo
	 * @param useYn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/campusModify", method = RequestMethod.POST)
	public ModelAndView campusModify(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "campSeq", required = false) String campSeq
			, @RequestParam(value = "campNm", required = false) String campNm
			, @RequestParam(value = "areaNm", required = false) String areaNm
			, @RequestParam(value = "detailInfo", required = false) String detailInfo
			, @RequestParam(value = "useYn", required = false) String useYn
			) throws Exception {
		logger.debug("campusModify");
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String mbrId = usertDetail.getUsername(); //회원 아이디 
        
        
		Campus campus = new Campus();
		campus.setCampSeq(campSeq);
		campus.setCampNm(campNm);
		campus.setAreaNm(areaNm);
		campus.setDetailInfo(detailInfo);
		campus.setUseYn(useYn);
		campus.setMbrId(mbrId);
		
		campusService.modifyDataWithResultCodeMsg(campus);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/campusList.do");
		
		return mav;
	}
	
	/**
	 * 캠퍼스 이름 중복 체크 
	 * @param request
	 * @param response
	 * @param model
	 * @param insInfoSeq
	 * @param campNm
	 * @param campSeq
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/amg/campNameCheck", method = RequestMethod.POST)
    @ResponseBody
    public VSResult<Campus> campNameCheck(HttpServletRequest request, HttpServletResponse response, Model model
    		, @RequestParam(value = "insInfoSeq", required = false) String insInfoSeq
			, @RequestParam(value = "campNm", required = false) String campNm
			, @RequestParam(value = "campSeq", required = false) String campSeq) throws Exception {
    	
    	VSResult result = new VSResult();
    	
    	try{
    		
    		CampusCondition campCond = new CampusCondition();
    		campCond.setInsInfoSeq(insInfoSeq);
    		campCond.setCampNm(campNm);
    		campCond.setCampSeq(campSeq);
    		
	        int resultCnt = campusService.campNameCheck(campCond);
	        
        	result.setCode("0000");
            result.setMessage(String.valueOf(resultCnt));
    	} catch(Exception e) {
    		 ExceptionHandler handler = new ExceptionHandler(e);
             e.printStackTrace();
    	}
    	
        return result;
    }
    
	
    @RequestMapping(value = "/amg/campusExcelPop")
    public String campusExcelPop(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        
        return "/amg/campusExcelPop";
    }
    
    
	
    
    @RequestMapping(value = "/amg/campExcelUpload", method = RequestMethod.POST)
    public ModelAndView campExcelUpload(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("campExcelUpload ======================================================");
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        
        // DB에 넣을 국가코드와 엑셀파일을 넘긴다.
        MemberExcelCondition excelCondition = new MemberExcelCondition();
        excelCondition.setMultipartFile(multipartFile);
        
        // 리턴 메세지 
        VSResult<?> result = null;
        
        try{
            result = campusService.inputExcelData(excelCondition);
        } catch (TreeRuntimeException e) {
            result = new VSResult<>();
            result.setCode(e.getCode());
            result.setMessage(e.getMsg());
            
            logger.debug(e.toString());
        }
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/amg/commonExcelResult");
        mav.addObject("subject", "Campus");
        mav.addObject("result", result);
        return mav;
    }
    
	
}
