package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class CampusListInfo extends VSObject {

	private String campInfoSeq;
	private String campNm;
	private String useYn;
	private String campId;
	private String instituteSeq;

	
	public String getCampInfoSeq() {
		return campInfoSeq;
	}
	public void setCampInfoSeq(String campInfoSeq) {
		this.campInfoSeq = campInfoSeq;
	}
	public String getCampNm() {
		return campNm;
	}
	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getCampId() {
		return campId;
	}
	public void setCampId(String campId) {
		this.campId = campId;
	}
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	} 
	
}
