package com.visangesl.treeadmin.amg.controller;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.AdminService;
import com.visangesl.treeadmin.amg.service.CampusService;
import com.visangesl.treeadmin.amg.vo.AdminCondition;
import com.visangesl.treeadmin.amg.vo.AdminMember;
import com.visangesl.treeadmin.amg.vo.CampusListInfo;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class AdminController implements BaseController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private CampusService campusService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/amg/adminList")
	public String adminList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "orderBy", required = false) String orderBy,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "searchKey", required = false) String searchKey,
			@RequestParam(value = "userType", required = false) String userType,
			@RequestParam(value = "sector", required = false) String sector,
			@RequestParam(value = "useYn", required = false) String useYn
			) throws Exception {
		logger.debug("adminList");
	
		TreeUserDetails userDetails = (TreeUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		final Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
		logger.debug("currentUser==" + authorities);
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 이미 userDetail에 저장된 값을 넣어준다. 

        String mbrGradeDefineSuperAdmin = "";
        mbrGradeDefineSuperAdmin = TreeProperties.getProperty("tree_superadmin");

        String mbrGradeDefineInstituteAdmin = "";
        mbrGradeDefineInstituteAdmin = TreeProperties.getProperty("tree_instituteadmin");

        String mbrGradeDefineCampus = "";
        mbrGradeDefineCampus = TreeProperties.getProperty("tree_campusadmin");
		
		logger.debug("adminList orderBy ===" + orderBy);
		logger.debug("adminList sort===" + sort);
		logger.debug("adminList insInfoSeq ===" + insInfoSeq);
		logger.debug("adminList UserName===" + userDetail.getName());
		logger.debug("adminList UserName===" + userDetail.getUsername());
		
		String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
		
		if(TreeAdminUtil.isNull(insInfoSeq) != ""){
		
			AdminCondition adminCondition = new AdminCondition();
			adminCondition.setInsInfoSeq(insInfoSeq);
			adminCondition.setMbrGrade(mbrGrade);
			
			adminCondition.setSearchUserType(userType);
			adminCondition.setSearchSect(sector);
			adminCondition.setSearchYn(useYn);
			adminCondition.setSearchNm(searchKey);
			adminCondition.setOrderBy(TreeAdminUtil.isNull(orderBy, "REGDT"));
			adminCondition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
		
			int pageNo = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageNo"), "1"));
			int pageSize = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageSize"), "20"));
			
			adminCondition.setPageNo(pageNo); // 현재 페이지 
			adminCondition.setPageSize(pageSize); // 페이지 사이즈 
			
			// 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
			// LIMIT 함수로 페이징 처리 하니깐 1페이지 일때 0 부터 들어가야 한다. 그래서  pageNo를 계산 해준다. 
			int pageStartIndex = (adminCondition.getPageNo() -  1) * adminCondition.getPageSize();
			
			adminCondition.setPageStartIndex(pageStartIndex);
	
			List<VSObject> adminList = adminService.getDataList(adminCondition);
			
			model.addAttribute("instituteSeq", insInfoSeq);
	        model.addAttribute("mbrGradeDefineSuperAdmin", mbrGradeDefineSuperAdmin);
	        model.addAttribute("mbrGradeDefineInstituteAdmin", mbrGradeDefineInstituteAdmin);
	        model.addAttribute("mbrGradeDefineCampus", mbrGradeDefineCampus);
	    	model.addAttribute("mbrGrade", mbrGrade);
			model.addAttribute("condition", adminCondition);
			model.addAttribute("adminList", adminList);
			model.addAttribute("menuCode", "8");
		}
		
		return "/amg/adminList";
	}

	@RequestMapping(value = "/amg/adminView")
	public String adminView(HttpServletRequest request, HttpServletResponse response, Model model,
			 @RequestParam(value = "mbrId", required = false) String mbrId,
			 @RequestParam(value = "iname", required = false) String iname
			 )  throws Exception {
		logger.debug("adminView");
		
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insInfoSeq = userDetail.getInsSeq();
		
        // 선생님의 기본정보를 가져오기 위해서 instituteSeq 값과 memberId가 필요하다.
        AdminCondition adminCondition = new AdminCondition();
    	adminCondition.setInsInfoSeq(insInfoSeq);
        adminCondition.setMbrId(mbrId);

        AdminMember adminData = (AdminMember)adminService.getData(adminCondition);

		logger.debug("Admin profilePath : " + adminData.getProfilePhotoPath());
		logger.debug("Admin mbrID : " + adminData.getMbrId());

		String mbrGradeDefineSuperAdmin = "";
        mbrGradeDefineSuperAdmin = TreeProperties.getProperty("tree_superadmin");

        String mbrGradeDefineInstituteAdmin = "";
        mbrGradeDefineInstituteAdmin = TreeProperties.getProperty("tree_instituteadmin");

        String mbrGradeDefineCampus = "";
        mbrGradeDefineCampus = TreeProperties.getProperty("tree_campusadmin");
        
        model.addAttribute("instituteSeq", insInfoSeq);
        model.addAttribute("mode", "View");
        model.addAttribute("iname", iname);
        model.addAttribute("mbrGradeDefineSuperAdmin", mbrGradeDefineSuperAdmin);
        model.addAttribute("mbrGradeDefineInstituteAdmin", mbrGradeDefineInstituteAdmin);
        model.addAttribute("mbrGradeDefineCampus", mbrGradeDefineCampus);

        model.addAttribute("adminData", adminData);
        model.addAttribute("menuCode", "8");

		return "/amg/adminEdit";
	}
	
	
	@RequestMapping(value = "/amg/adminEdit")
	public String adminEdit(HttpServletRequest request, HttpServletResponse response, Model model,
			 @RequestParam(value = "mbrOrgId", required = false) String mbrId,
			 @RequestParam(value = "cmpusNm", required = false) String iname
			 )  throws Exception {
		logger.debug("adminEdit");
		logger.debug("mbrId==" + mbrId);
		logger.debug("iname==" + iname);
		
		
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insInfoSeq = userDetail.getInsSeq();
		
        // 선생님의 기본정보를 가져오기 위해서 instituteSeq 값과 memberId가 필요하다.
        AdminCondition adminCondition = new AdminCondition();
    	adminCondition.setInsInfoSeq(insInfoSeq);
        adminCondition.setMbrId(mbrId);
        
        AdminMember adminData = (AdminMember)adminService.getData(adminCondition);

        String mbrGradeDefineSuperAdmin = "";
        mbrGradeDefineSuperAdmin = TreeProperties.getProperty("tree_superadmin");

        String mbrGradeDefineInstituteAdmin = "";
        mbrGradeDefineInstituteAdmin = TreeProperties.getProperty("tree_instituteadmin");

        String mbrGradeDefineCampus = "";
        mbrGradeDefineCampus = TreeProperties.getProperty("tree_campusadmin");
        
        model.addAttribute("instituteSeq", insInfoSeq);
        model.addAttribute("mode", "Edit");
        model.addAttribute("iname", iname);
        model.addAttribute("mbrGradeDefineSuperAdmin", mbrGradeDefineSuperAdmin);
        model.addAttribute("mbrGradeDefineInstituteAdmin", mbrGradeDefineInstituteAdmin);
        model.addAttribute("mbrGradeDefineCampus", mbrGradeDefineCampus);
        model.addAttribute("adminData", adminData);
        model.addAttribute("menuCode", "8");

		return "/amg/adminEdit";
	}
	

	@RequestMapping(value = "/adminInsert")
	public String adminInsert(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("adminInsert");
		
		String insInfoSeq;
        
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		insInfoSeq = userDetail.getInsSeq(); // 이미 userDetail에 저장된 값을 넣어준다. 
        
		String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
		
		// 선생의 기본 유저 데이터 저장.
        AdminCondition adminCondition = new AdminCondition();
    	adminCondition.setInsInfoSeq(insInfoSeq);
        adminCondition.setMbrId(userDetail.getUsername());
		
        String cSeq = null;
        String sector = null;
        
        if (mbrGrade.equals("MG122")){
			AdminMember adminCampData = (AdminMember)adminService.getDataCampInfo(adminCondition);
		//	List<VSObject> adminList = adminService.getDataCampInfo(adminMember);
			cSeq = adminCampData.getcSeq();
			sector = adminCampData.getSector();
        }
        
        
        logger.debug("session MbrGrade == " + mbrGrade);
        logger.debug("session instituteSeq == " + insInfoSeq);

        String mbrGradeDefineSuperAdmin = "";
        mbrGradeDefineSuperAdmin = TreeProperties.getProperty("tree_superadmin");

        String mbrGradeDefineInstituteAdmin = "";
        mbrGradeDefineInstituteAdmin = TreeProperties.getProperty("tree_instituteadmin");

        String mbrGradeDefineCampus = "";
        mbrGradeDefineCampus = TreeProperties.getProperty("tree_campusadmin");

        model.addAttribute("mbrGrade", mbrGrade);
        model.addAttribute("instituteSeq", insInfoSeq);
		model.addAttribute("cSeq",cSeq );
        model.addAttribute("sector", sector);
        model.addAttribute("mbrGradeDefineSuperAdmin", mbrGradeDefineSuperAdmin);
        model.addAttribute("mbrGradeDefineInstituteAdmin", mbrGradeDefineInstituteAdmin);
        model.addAttribute("mbrGradeDefineCampus", mbrGradeDefineCampus);
		return "/amg/adminEdit";
	}

	@RequestMapping(value = "/adminAdd", method = RequestMethod.POST)
	public ModelAndView adminAdd(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "nm", required = false) String nm,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "memo", required = false) String memo,
			@RequestParam(value = "userType", required = false) String userType,
			@RequestParam(value = "sector", required = false) String sector,
			@RequestParam(value = "cSeq", required = false) String cSeq,
			@RequestParam(value = "useYn", required = false) String useYn) throws Exception {
		logger.debug("adminAdd");
	
		TreeUserDetails userDetails = (TreeUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 이미 userDetail에 저장된 값을 넣어준다. 
		
		String icmSect = null;
		String relSect = null;
		String icmSeq = null ;
		String mbrGrade = null;
		
		// 학원관리자  MG121
		if (userType.equals("1")) {
			icmSect = "RE004";
			relSect = "RE001";
			icmSeq  =  insInfoSeq;
			mbrGrade = "MG121";
			
		// 캠퍼스 관리자 MG122	
		}  else if  (userType.equals("2") )  {
			
			if(sector == null || sector == ""){
				sector = cSeq;
			}
			
			icmSect = "RE004";
			relSect = "RE002";
			icmSeq  = sector;
			mbrGrade = "MG122";
		}

		
		logger.debug("adminAdd[useYn]=="+ useYn);
		logger.debug("adminAdd[userType]=="+ userType);
		logger.debug("adminAdd[icmSeq]=="+ icmSeq);
		logger.debug("adminAdd[icmSect]=="+ icmSect);
		logger.debug("adminAdd[relSect]=="+ relSect);
				
		// 선생의 기본 유저 데이터 저장.
		AdminMember adminMember = new AdminMember();
		adminMember.setMbrId(mbrId);
		adminMember.setPwd(pwd);
		adminMember.setNm(nm);
		adminMember.setEmail(email);
		adminMember.setTelNo(telNo);
		adminMember.setMemo(memo);
		adminMember.setUseYn(useYn);
		adminMember.setMbrGrade(mbrGrade);
		adminMember.setSector(sector);
		adminMember.setUserType(userType);
		adminMember.setIcmSeq(icmSeq);
		adminMember.setIcmSect(icmSect);
		adminMember.setRelSect(relSect);
		
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
        	adminMember.setUploadFile(multipartFile);
        }
		
		
		try {
			adminService.addDataWithResultCodeMsg(adminMember);
			adminService.addDataIcmRel(adminMember);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ExceptionHandler handler = new ExceptionHandler(e);
			e.printStackTrace();
		} 

		logger.debug("adminAdd=====OK");

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/adminList.do");

		return mav;
	}

	@RequestMapping(value = "/adminModify", method = RequestMethod.POST)
	public ModelAndView adminModify(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrOrgId", required = false) String mbrId,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "nm", required = false) String nm,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "memo", required = false) String memo,
			@RequestParam(value = "userType", required = false) String userType,
			@RequestParam(value = "sector", required = false) String sector,
			@RequestParam(value = "useYn", required = false) String useYn) throws Exception {
		logger.debug("adminModify");
	
		TreeUserDetails userDetails = (TreeUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 이미 userDetail에 저장된 값을 넣어준다. 
		
		String icmSect = null;
		String relSect = null;
		String icmSeq = null ;
		String mbrGrade = null;

		logger.debug("adminAdd[userType]=="+ userType);
		logger.debug("adminAdd[icmSeq]=="+ icmSeq);
		logger.debug("adminAdd[icmSect]=="+ icmSect);
		logger.debug("adminAdd[relSect]=="+ relSect);
				
		// 선생의 기본 유저 데이터 저장.
		AdminMember adminMember = new AdminMember();
		adminMember.setMbrId(mbrId);
		adminMember.setPwd(pwd);
		adminMember.setNm(nm);
		adminMember.setEmail(email);
		adminMember.setTelNo(telNo);
		adminMember.setMemo(memo);
		adminMember.setUseYn(useYn);
		adminMember.setMbrGrade(mbrGrade);
		adminMember.setSector(sector);
		adminMember.setUserType(userType);
		
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        
        if (multipartFile != null) {
        	adminMember.setUploadFile(multipartFile);
        }

        try {
			adminService.modifyDataWithResultCodeMsg(adminMember);
			//adminService.addDataIcmRel(adminMember);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ExceptionHandler handler = new ExceptionHandler(e);
			e.printStackTrace();
		} 

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/adminList.do");

		return mav;
	}
	
	@RequestMapping(value = "/adminGetCampus")
	@ResponseBody
	public VSResult<CampusListInfo> adminGetCampus(HttpServletRequest request, HttpServletResponse response, ModelMap model,
			@RequestParam(value = "instituteSeq", required = false) String instituteSeq) throws Exception {
		logger.debug("institute");


		ArrayList<Map> listMapSeg = new ArrayList<Map>();
		Map<String, String> resultMap = new HashMap<String, String>();
		VSResult result = new VSResult();
		try {
			CampusListInfo campusListInfo  = new CampusListInfo();
			campusListInfo.setInstituteSeq(instituteSeq);

			List<VSObject> campList=  campusService.getSelectCampusList(campusListInfo);

			result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(campList);

		} catch (Exception e) {
			ExceptionHandler handler = new ExceptionHandler(e);
			e.printStackTrace();
		}
		return  result;
	}

	@RequestMapping(value = "/amg/adminSearch")
	public String adminSearch(HttpServletRequest request, HttpServletResponse response, Model model,
		@RequestParam(value = "searchKey", required = false) String searchKey,
		@RequestParam(value = "userType", required = false) String userType,
		@RequestParam(value = "sector", required = false) String sector,
		@RequestParam(value = "useYn", required = false) String useYn) throws Exception {
		
		logger.debug("adminSearch");
		
		logger.debug("adminSearch userType===" + userType);
		logger.debug("adminSearch sector===" + sector);
		logger.debug("adminSearch useYn===" + useYn);
		logger.debug("adminSearch searchKey===" + searchKey);
		
		
		TreeUserDetails userDetails = (TreeUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		final Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
		logger.debug("currentUser==" + authorities);
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 이미 userDetail에 저장된 값을 넣어준다. 

        String mbrGradeDefineSuperAdmin = "";
        mbrGradeDefineSuperAdmin = TreeProperties.getProperty("tree_superadmin");

        String mbrGradeDefineInstituteAdmin = "";
        mbrGradeDefineInstituteAdmin = TreeProperties.getProperty("tree_instituteadmin");

        String mbrGradeDefineCampus = "";
        mbrGradeDefineCampus = TreeProperties.getProperty("tree_campusadmin");
		
		logger.debug("adminList insInfoSeq ===" + insInfoSeq);
		logger.debug("adminList UserName===" + userDetail.getName());
		logger.debug("adminList UserName===" + userDetail.getUsername());
		
		String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
		
		AdminCondition adminCondition = new AdminCondition();
		adminCondition.setInsInfoSeq(insInfoSeq);
		adminCondition.setMbrGrade(mbrGrade);

		int pageNo = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(TreeAdminUtil.isNull(request.getParameter("pageSize"), "20"));
		
		adminCondition.setPageNo(pageNo); // 현재 페이지 
		adminCondition.setPageSize(pageSize); // 페이지 사이즈 
		
		adminCondition.setSearchUserType(userType);
		adminCondition.setSearchSect(sector);
		adminCondition.setSearchYn(useYn);
		adminCondition.setSearchNm(searchKey);
		
		// 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
		// LIMIT 함수로 페이징 처리 하니깐 1페이지 일때 0 부터 들어가야 한다. 그래서  pageNo를 계산 해준다. 
		int pageStartIndex = (adminCondition.getPageNo() -  1) * adminCondition.getPageSize();
		
		adminCondition.setPageStartIndex(pageStartIndex);

		//logger.debug("test==" + adminService.toString());
		
		List<VSObject> adminList = adminService.getDataList(adminCondition);
	
		model.addAttribute("userType", userType);
		model.addAttribute("sector", sector);
		model.addAttribute("useYn", useYn);
		model.addAttribute("searchKey", searchKey);
		model.addAttribute("instituteSeq", insInfoSeq);
        model.addAttribute("mbrGradeDefineSuperAdmin", mbrGradeDefineSuperAdmin);
        model.addAttribute("mbrGradeDefineInstituteAdmin", mbrGradeDefineInstituteAdmin);
        model.addAttribute("mbrGradeDefineCampus", mbrGradeDefineCampus);
    	model.addAttribute("mbrGrade", mbrGrade);
		model.addAttribute("condition", adminCondition);
		model.addAttribute("adminList", adminList);
		model.addAttribute("menuCode", "8");
		return "/amg/adminList";
	}	
}

