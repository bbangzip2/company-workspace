package com.visangesl.treeadmin.amg.vo;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSCondition;

public class StudentCondition extends VSCondition {
	private String mbrId;
	private String insSeq;  // Institute ID
	private String campSeq; // Campus ID
	private String clsSeq; // 클래스 번호.
	private String progSeq;
	private String campId;
    private String searchColumn; // 검색 컬럼 값.
	private String searchKey; // 검색 값
	private String regStartDate; // 가입기간 검색 시작일.
	private String regEndDate; // 가입기간 검색 종료일.
	private String useYn; // 학생 사용 여부.
	private int totalCount; // 리스트 총 갯수.
	private int pageSize; // 페이지 당 총 아이템 갯수.
	private int pageNo; // 현재 페이지.
	private int pageStartIndex; // 현재 페이지의 시작 인덱스 값.
	private MultipartFile uploadFile;
	
    public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getInsSeq() {
        return insSeq;
    }
    public void setInsSeq(String insSeq) {
        this.insSeq = insSeq;
    }
    public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getProgSeq() {
        return progSeq;
    }
    public void setProgSeq(String progSeq) {
        this.progSeq = progSeq;
    }
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getSearchColumn() {
		return searchColumn;
	}
	public void setSearchColumn(String searchColumn) {
		this.searchColumn = searchColumn;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getRegStartDate() {
		return regStartDate;
	}
	public void setRegStartDate(String regStartDate) {
		this.regStartDate = regStartDate;
	}
	public String getRegEndDate() {
		return regEndDate;
	}
	public void setRegEndDate(String regEndDate) {
		this.regEndDate = regEndDate;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageStartIndex() {
		return pageStartIndex;
	}
	public void setPageStartIndex(int pageStartIndex) {
		this.pageStartIndex = pageStartIndex;
	}
    public MultipartFile getUploadFile() {
        return uploadFile;
    }
    public void setUploadFile(MultipartFile uploadFile) {
        this.uploadFile = uploadFile;
    }
    public String getCampId() {
        return campId;
    }
    public void setCampId(String campId) {
        this.campId = campId;
    }
}
