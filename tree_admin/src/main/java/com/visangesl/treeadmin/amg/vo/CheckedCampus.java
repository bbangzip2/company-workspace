package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class CheckedCampus extends VSObject {

    private String cd;          // 캠퍼스 순번
    private String cdSubject;   // 캠퍼스 이름
    private String checked;     // 체크 여부
    private String disabled;    // 비활성화 여부
    
    public String getCd() {
        return cd;
    }
    public void setCd(String cd) {
        this.cd = cd;
    }
    public String getCdSubject() {
        return cdSubject;
    }
    public void setCdSubject(String cdSubject) {
        this.cdSubject = cdSubject;
    }
    public String getChecked() {
        return checked;
    }
    public void setChecked(String checked) {
        this.checked = checked;
    }
    public String getDisabled() {
        return disabled;
    }
    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }
    
}
