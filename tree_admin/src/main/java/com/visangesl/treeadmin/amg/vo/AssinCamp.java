package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

//@XmlRootElement(name="assinCamp")
public class AssinCamp extends VSObject {
	private String assinCampSeq;
	private String campSeq;
	private String mbrId;
	
	public String getAssinCampSeq() {
		return assinCampSeq;
	}
	public void setAssinCampSeq(String assinCampSeq) {
		this.assinCampSeq = assinCampSeq;
	}
	public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
}
