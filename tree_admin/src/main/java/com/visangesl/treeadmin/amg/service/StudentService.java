package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface StudentService {
	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;
	
	// select List Data 
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;
	
	// select List Count 
	public int getDataListCnt(VSCondition dataCondition) throws Exception;
	
	// Update
	public VSResult<?> modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// Insert 
	public VSResult<?> addDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// select combo list
	public List<VSObject> getCampusList(VSObject vsObject) throws Exception;
	public List<VSObject> getProgramList(VSObject vsObject) throws Exception;
	public List<VSObject> getClassList(VSObject vsObject) throws Exception;
	
	public VSResult<?> inputExcelData(VSObject vsObject) throws Exception;
}
