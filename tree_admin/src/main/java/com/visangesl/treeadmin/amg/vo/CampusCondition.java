package com.visangesl.treeadmin.amg.vo;

import com.visangesl.treeadmin.vo.VSListCondition;

public class CampusCondition extends VSListCondition {
	private String orderColumn;
	private String sort; // 오름차순인지 내림차순인지
	private String campSeq; // Campus ID
	private String useYn; // Status 값
	private String searchKey; // 검색 값
	private int totalCount; // 리스트 총 갯수.
	
	private String campNm;
	private String insInfoSeq; // 학원 순번 
	
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getCampNm() {
		return campNm;
	}
	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}
	public String getInsInfoSeq() {
		return insInfoSeq;
	}
	public void setInsInfoSeq(String insInfoSeq) {
		this.insInfoSeq = insInfoSeq;
	}
	
}
