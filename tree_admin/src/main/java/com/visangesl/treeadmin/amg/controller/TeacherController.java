package com.visangesl.treeadmin.amg.controller;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.InstituteService;
import com.visangesl.treeadmin.amg.service.TeacherService;
import com.visangesl.treeadmin.amg.vo.Campus;
import com.visangesl.treeadmin.amg.vo.CheckedCampus;
import com.visangesl.treeadmin.amg.vo.ClassInfo;
import com.visangesl.treeadmin.amg.vo.Institute;
import com.visangesl.treeadmin.amg.vo.InstituteCondition;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.amg.vo.Teacher;
import com.visangesl.treeadmin.amg.vo.TeacherCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.member.service.MemberService;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.member.vo.MemberCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.CodeCondition;
import com.visangesl.treeadmin.vo.CodeVo;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class TeacherController implements BaseController {

    @Autowired
    private InstituteService instituteService;
    
    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private MemberService memberService;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 선생님 리스트
     * @param request
     * @param response
     * @param model
     * @param campSeq   캠퍼스 순번
     * @param useYn     상태값
     * @param searchKey 검색 키값
     * @param pageSize  한 페이지의 보여지는 리스트 수
     * @param pageNo    페이지 번호
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/teacherList", method=RequestMethod.GET)
    public String teacherList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "campSeq", required = false) String campSeq,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "searchKey", required = false) String searchKey,
            @RequestParam(value = "pageSize", required = false) String pageSize,
            @RequestParam(value = "pageNo", required = false) String pageNo) throws Exception {
        logger.debug("teacherSearch");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        TeacherCondition teacherCondition = new TeacherCondition();
        teacherCondition.setInsSeq(insSeq);
        
        // 캠퍼스 어드민일 경우 유저아이디로 캠퍼스 리스트를 검색 하기 위한 작업.
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
            teacherCondition.setMbrId(userDetail.getUsername());
        }
        // 캠퍼스 어드민이면 캠퍼스 목록이 하나만 넘어온다. 그렇지 않으면 여러개의 목록이 넘어온다.
        VSResult<List<VSObject>> campCombo = new VSResult<List<VSObject>>();
        List<VSObject> result = teacherService.getCampusList(teacherCondition);
        campCombo.setResult(result);
        
        // 캠퍼스 목록이 하나만 있으면 campSeq에 할당하고 그에 맞게 리스트를 가져온다.
        String campList = TreeAdminUtil.createJsonString(campCombo);
        if (TreeAdminUtil.isNull(teacherCondition.getMbrId()) != "" && result != null && result.size() == 1) {
            campSeq = ((CodeVo)campCombo.getResult().get(0)).getCd();
        }
        // 캠퍼스 정렬
        if(TreeAdminUtil.isNull(campSeq) != "") {
            teacherCondition.setCampSeq(campSeq);
        }
        
        // Status 값 정렬
        if(TreeAdminUtil.isNull(useYn) != "") {
            teacherCondition.setUseYn(useYn);
        }
        
        // 검색어
        if(TreeAdminUtil.isNull(searchKey) != "") {
            teacherCondition.setSearchKey(searchKey);
        }
        
        // 조건에 맞는 데이터의 총 갯수 가져오기.
        int totalCount = teacherService.getDataListCnt(teacherCondition);
        teacherCondition.setTotalCount(totalCount);
        
        // 현재 페이지 번호와 한페이지에 표시할 갯수 설정.
        teacherCondition.setPageSize(TreeAdminUtil.isNumber(pageSize, 20));
        teacherCondition.setPageNo(TreeAdminUtil.isNumber(pageNo, 1));
        
        // 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
        int pageStartIndex = (teacherCondition.getPageNo() - 1) * teacherCondition.getPageSize();
        teacherCondition.setPageStartIndex(pageStartIndex);
        
        List<VSObject> teacherList = teacherService.getDataList(teacherCondition);
        
        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < teacherList.size()) {
            Teacher item = (Teacher) teacherList.get(i);
            item.setRn(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
        model.addAttribute("campList", campList);
        model.addAttribute("condition", teacherCondition);
        model.addAttribute("teacherList", teacherList);
        
        return "/amg/teacherList";
    }
    
    /**
     * 선생님의 상세뷰
     * @param request
     * @param response
     * @param model
     * @param mbrId     선생님에 해당하는 유저아이디
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/teacherView", method=RequestMethod.GET)
    public String teacherView(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false) String mbrId,
            @RequestParam(value = "campSeq", required = false) String campSeq,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "searchKey", required = false) String searchKey,
            @RequestParam(value = "pageNo", required = false) String pageNo) throws Exception {
        logger.debug("teacherView");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        // 선생님의 기본정보를 가져오기 위해서 instituteSeq 값과 memberId가 필요하다.
        TeacherCondition teacherCondition = new TeacherCondition();
        teacherCondition.setInsSeq(insSeq);
        teacherCondition.setMbrId(mbrId);
        
        Teacher teacher = (Teacher)teacherService.getData(teacherCondition);
        
        String modId = teacher.getModId();
        MemberCondition memberCondition = new MemberCondition();
        memberCondition.setMbrId(modId);
        Member regMember = (Member) memberService.getData(memberCondition);
        
        teacherCondition.setCampSeq(campSeq);
        teacherCondition.setUseYn(useYn);
        teacherCondition.setSearchKey(searchKey);
        teacherCondition.setPageNo(TreeAdminUtil.isNumber(pageNo, 1));
        
        model.addAttribute("condition", teacherCondition);
        model.addAttribute("teacher", teacher);
        model.addAttribute("regMember", regMember);
        
        return "/amg/teacherView";
    }
    
    /**
     * 선생님 편집 페이지로 이동.
     * @param request
     * @param response
     * @param model
     * @param mbrId     선생님의 유저 아이디
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/teacherEdit")
    public String teacherEdit(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false) String mbrId,
            @RequestParam(value = "campSeq", required = false) String campSeq,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "searchKey", required = false) String searchKey,
            @RequestParam(value = "pageNo", required = false) String pageNo) throws Exception {
        logger.debug("teacherEdit");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        // 기관순번에 맞는 콤보 리스트를 가져온다.
        CodeCondition codeCondition = new CodeCondition();
        codeCondition.setInsInfoSeq(insSeq);
        List<VSObject> campus = commonService.getCampusList(codeCondition);
        
        TeacherCondition teacherCondition = new TeacherCondition();
        teacherCondition.setInsSeq(insSeq);
        
        // 캠퍼스 어드민일 경우 유저아이디로 캠퍼스 리스트를 검색 하기 위한 작업.
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
            teacherCondition.setMbrId(userDetail.getUsername());
        }
        // 캠퍼스 어드민이면 캠퍼스 목록이 하나만 넘어온다. 그렇지 않으면 여러개의 목록이 넘어온다.
        List<VSObject> adminCampList = teacherService.getCampusList(teacherCondition);
        // size가 1개이면 캠퍼스 어드민이다.
        boolean isCampusAdmin = adminCampList.size() == 1 ? true : false;
        String adminCampSeq = "";
        if (isCampusAdmin) {
            adminCampSeq = ((CodeVo) adminCampList.get(0)).getCd();
        }
        
        
        
        teacherCondition.setMbrId(mbrId);
        // 선생님의 데이터를 수정하기 위해 해당 데이터를 가져온다.
        Teacher teacher = (Teacher)teacherService.getData(teacherCondition);
        
        
        List<CheckedCampus> campusList = new ArrayList<CheckedCampus>();
        
        // 선생님의 캠퍼스 리스트를 가져온다.
        List<Campus> tCampus = teacher.getCampList();
        // 기관에 속한 캠퍼스 리스트를 loop돌린다.
        for (int i = 0; i < campus.size(); i++) {
            CodeVo cv = (CodeVo)campus.get(i);
            
            CheckedCampus camp = new CheckedCampus();
            camp.setCd(cv.getCd());
            camp.setCdSubject(cv.getCdSubject());
            
            for (int j = 0; j < tCampus.size(); j++) {
                Campus cmp = tCampus.get(j);
                // 선생님이 속한 캠퍼스 리스트와 비교한다.
                if (cv.getCd().equals(cmp.getCampSeq())) {
                    // checkbox 에 체크를 표시 하기 위해 Y값을 넣는다.
                    camp.setChecked("Y");
                    
                    for (int k = 0; k < cmp.getClassList().size(); k++) {
                        ClassInfo cls = cmp.getClassList().get(k);
                        // 해당 캠퍼스에 클래스가 묶여 있는지 확인한다.
                        if (TreeAdminUtil.isNull(cls.getClassSeq()) != "") {
                            // 묶여 있으면 비활성화 값을 Y로 할당한다.
                            camp.setDisabled("Y");
                            break;
                        }
                    }
                    break;
                }
            }
            
            // 캠퍼스 어드민이고 해당 캠퍼스와 같지 않으면 모두 비활성화
            if (isCampusAdmin && !camp.getCd().equalsIgnoreCase(adminCampSeq)) {
                camp.setDisabled("Y");
            }
            
            campusList.add(camp);
        }
        
        String modId = teacher.getModId();
        MemberCondition memberCondition = new MemberCondition();
        memberCondition.setMbrId(modId);
        Member regMember = (Member) memberService.getData(memberCondition);
        
        teacherCondition.setCampSeq(campSeq);
        teacherCondition.setUseYn(useYn);
        teacherCondition.setSearchKey(searchKey);
        teacherCondition.setPageNo(TreeAdminUtil.isNumber(pageNo, 1));
        
        model.addAttribute("condition", teacherCondition);
        model.addAttribute("campusList", campusList);
        model.addAttribute("teacher", teacher);
        model.addAttribute("regMember", regMember);
        
        return "/amg/teacherEdit";
    }
    
    /**
     * 새로운 선생님 등록 화면으로 이동.
     * @param request
     * @param response
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/teacherInsert")
    public String teacherInsert(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("teacherInsert");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        // 기관순번 값에 맞는 콤보 리스트를 가져온다.
        CodeCondition codeCondition = new CodeCondition();
        codeCondition.setInsInfoSeq(insSeq);
        List<VSObject> campus = commonService.getCampusList(codeCondition);
        
        List<CheckedCampus> campusList = new ArrayList<CheckedCampus>();
        
        for (int i = 0; i < campus.size(); i++) {
            CodeVo cv = (CodeVo)campus.get(i);
            
            CheckedCampus camp = new CheckedCampus();
            camp.setCd(cv.getCd());
            camp.setCdSubject(cv.getCdSubject());
            campusList.add(camp);
        }
        
        model.addAttribute("campusList", campusList);
        
        return "/amg/teacherEdit";
    }
    
    /**
     * 선생님 편집.
     * @param request
     * @param response
     * @param model
     * @param mbrId     유저아이디
     * @param pwd       패스워드
     * @param name      이름
     * @param nickName  별명
     * @param email     이메일
     * @param sexSect   성별
     * @param telNo     전화번호
     * @param memo      메오
     * @param useYn     상태값
     * @param addCampList  추가할 캠퍼스 리스트
     * @param delCampList  정리할 캠퍼스 리스트
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/teacherModify", method = RequestMethod.POST)
    public ModelAndView teacherModify(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false) String mbrId,
            @RequestParam(value = "pwd", required = false) String pwd,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "nickName", required = false) String nickName,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "sexSect", required = false) String sexSect,
            @RequestParam(value = "telNo", required = false) String telNo,
            @RequestParam(value = "memo", required = false) String memo,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "addCampList", required = false) String addCampList,
            @RequestParam(value = "delCampList", required = false) String delCampList) throws Exception {
        logger.debug("campusModify");
        
        // 선생의 기본 유저 데이터 저장.
        Member member = new Member();
        member.setMbrId(mbrId);
        member.setPwd(pwd);
        member.setName(name);
        member.setNickName(nickName);
        member.setEmail(email);
        member.setSexSect(sexSect);
        member.setTelNo(telNo);
        member.setMemo(memo);
        member.setUseYn(useYn);
        
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String regId = "";
        if (userDetail != null) {
            regId = userDetail.getUsername();
        }
        member.setModId(regId);
        
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
            member.setUploadFile(multipartFile);
        }
        
        VSResult<VSObject> resultCodeMsg = memberService.modifyDataWithResultCodeMsg(member);
        
        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            // 지워야할 캠퍼스 목록을 지운다.
            if(TreeAdminUtil.isNull(delCampList) != "") {
                String[] delCampArray = delCampList.split(",");
                int i = 0;
                while(i < delCampArray.length) {
                    IcmRelVo delCamp = new IcmRelVo();
                    delCamp.setIcmSeq(mbrId);
                    delCamp.setIcmSect("RE004");
                    delCamp.setRelSect("RE002");
                    delCamp.setRelSeq(delCampArray[i]);
                    commonService.deleteIcmRelData(delCamp);
                    i++;
                }
            }
            
            // 새로 추가된 캠퍼스를 추가한다.
            if(TreeAdminUtil.isNull(addCampList) != "") {
                String[] addCampArray = addCampList.split(",");
                int i = 0;
                while(i < addCampArray.length) {
                    IcmRelVo addCamp = new IcmRelVo();
                    addCamp.setIcmSeq(mbrId);
                    addCamp.setIcmSect("RE004");
                    addCamp.setRelSect("RE002");
                    addCamp.setRelSeq(addCampArray[i]);
                    commonService.addIcmRelData(addCamp);
                    i++;
                }
            }
            
            ModelAndView mav = new ModelAndView();
            mav.setViewName("redirect:/amg/teacherList.do");
            return mav;
        } else {
            //Fail
            StringBuffer sb = new StringBuffer();
            
            sb.append("<script type=\"text/javascript\">");
            sb.append("alert('처리작업이 실패하였습니다.\n다시 시도해주시기 바랍니다.');");
            sb.append("history.back();");
            sb.append("</script>");
            
            response.setBufferSize(8 * 1024); // 8K buffer
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html; charset=utf-8");
            PrintWriter out = response.getWriter();

            out.println(sb.toString());
            out.flush();
            
            return null;
        }
    }
    
    /**
     * 새로운 선생님 등록.
     * @param request
     * @param response
     * @param model
     * @param mbrId     유저아이디
     * @param pwd       패스워드
     * @param name      이름
     * @param nickName  별명
     * @param email     이메일
     * @param sexSect   성별
     * @param telNo     전화번호
     * @param memo      메모
     * @param useYn     상태값
     * @param addCampList  할당된 캠퍼스 리스트
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/teacherAdd", method = RequestMethod.POST)
    public ModelAndView teacherAdd(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false) String mbrId,
            @RequestParam(value = "pwd", required = false) String pwd,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "nickName", required = false) String nickName,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "sexSect", required = false) String sexSect,
            @RequestParam(value = "telNo", required = false) String telNo,
            @RequestParam(value = "memo", required = false) String memo,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "addCampList", required = false) String addCampList) throws Exception {
        logger.debug("teacherAdd");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        // nation 값으 가져오기 위해 기관순번으로 기관을 가져온다.
        InstituteCondition insCondition = new InstituteCondition();
        insCondition.setSeq(insSeq);
        Institute institute = (Institute) instituteService.getData(insCondition);
        
        // 선생의 기본 유저 데이터 저장.
        Member member = new Member();
        member.setInstituteSeq(insSeq);
        member.setMbrId(mbrId);
        member.setPwd(pwd);
        member.setName(name);
        member.setNickName(nickName);
        member.setEmail(email);
        member.setSexSect(sexSect);
        member.setTelNo(telNo);
        member.setMemo(memo);
        member.setUseYn(useYn);
        member.setNation(institute.getNationCd());
        member.setSect("MS002");
        member.setMbrGrade("MG210");
        
        String regId = "";
        if (userDetail != null) {
            regId = userDetail.getUsername();
        }
        // 로그인된 유저아이디를 입력한다.
        member.setRegId(regId);
        member.setModId(regId);
        
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
            member.setUploadFile(multipartFile);
        }
        
        VSResult<VSObject> resultCodeMsg = memberService.addDataWithResultCodeMsg(member);
        
        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            // 새로 추가된 캠퍼스를 추가한다.
            if(TreeAdminUtil.isNull(addCampList) != "") {
                String[] addCampArray = addCampList.split(",");
                int i = 0;
                while(i < addCampArray.length) {
                    IcmRelVo addCamp = new IcmRelVo();
                    addCamp.setIcmSeq(mbrId);
                    addCamp.setIcmSect("RE004");
                    addCamp.setRelSect("RE002");
                    addCamp.setRelSeq(addCampArray[i]);
                    commonService.addIcmRelData(addCamp);
                    i++;
                }
            }
            
            ModelAndView mav = new ModelAndView();
            mav.setViewName("redirect:/amg/teacherList.do");
            return mav;
        } else {
          //Fail
            StringBuffer sb = new StringBuffer();
            
            sb.append("<script type=\"text/javascript\">");
            sb.append("alert('처리작업이 실패하였습니다.\n다시 시도해주시기 바랍니다.');");
            sb.append("history.back();");
            sb.append("</script>");
            
            response.setBufferSize(8 * 1024); // 8K buffer
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html; charset=utf-8");
            PrintWriter out = response.getWriter();

//            int size = response.getBufferSize(); // returns 8096 or greater
            out.println(sb.toString());
            out.flush();
            
            return null;
        }
    }
    
    
    @RequestMapping(value = "/amg/teacherExcelPop", method=RequestMethod.GET)
    public String teacherExcelPop(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("teacherExcelPop");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        TeacherCondition condition = new TeacherCondition();
        condition.setInsSeq(insSeq);
        
        List<VSObject> campList = teacherService.getCampusList(condition);
        model.addAttribute("campList", campList);
        
        return "/amg/teacherExcelPop";
    }
    
    
    @RequestMapping(value = "/amg/teacherExcelUpload", method = RequestMethod.POST)
    public ModelAndView teacherExcelUpload(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("teacherExcelUpload ======================================================");
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        // nation 값으 가져오기 위해 기관순번으로 기관을 가져온다.
        InstituteCondition insCondition = new InstituteCondition();
        insCondition.setSeq(insSeq);
        Institute institute = (Institute) instituteService.getData(insCondition);
        
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        
        // DB에 넣을 국가코드와 엑셀파일을 넘긴다.
        MemberExcelCondition excelCondition = new MemberExcelCondition();
        excelCondition.setNationCd(institute.getNationCd());
        excelCondition.setMultipartFile(multipartFile);
        
        // 리턴 메세지 
        VSResult<?> result = null;
        
        try{
            result = teacherService.inputExcelData(excelCondition);
        } catch (TreeRuntimeException e) {
            result = new VSResult<>();
            result.setCode(e.getCode());
            result.setMessage(e.getMsg());
        }
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/amg/commonExcelResult");
        mav.addObject("subject", "Teacher");
        mav.addObject("result", result);
        return mav;
    }
}
