package com.visangesl.treeadmin.amg.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.AssignClassDao;
import com.visangesl.treeadmin.amg.service.AssignClassService;
import com.visangesl.treeadmin.amg.vo.Campus;
import com.visangesl.treeadmin.amg.vo.CourseHistory;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.amg.vo.Student;
import com.visangesl.treeadmin.excel.TreeExcelReader;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class AssignClassServiceImpl  implements AssignClassService {

	@Autowired
	AssignClassDao dao;
	
	@Autowired
	CommonService commService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getAssignClassList(VSCondition dataCondition) throws Exception {
        return dao.getAssignClassList(dataCondition);
    }
	
	
	@Override
    @Transactional(readOnly = true)
    public int getAssignClassListCnt(VSCondition dataCondition) throws Exception {
        return dao.getAssignClassListCnt(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getUnassignStudentList(VSCondition dataCondition) throws Exception {
        return dao.getUnassignStudentList(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getCampusStudentList(VSCondition dataCondition) throws Exception {
        return dao.getCampusStudentList(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getClassDetailInfo(VSCondition dataCondition) throws Exception {
        return dao.getClassDetailInfo(dataCondition);
    }	
	
	@Override
    @Transactional(readOnly = true)
	public List<VSObject> getRecentClassList(VSCondition dataCondition) throws Exception {
        return dao.getRecentClassList(dataCondition);
    }	
	
	
	@Override
    @Transactional
	public VSResult classSettingAdd(List<IcmRelVo> insertList, List<IcmRelVo> deleteList, String mbrId)throws Exception{
		
    	VSResult resultCodeMsg = new VSResult();

    	
    	if(insertList.size() > 0 ){
    		for(int i=0; i< insertList.size(); i++){
    			
    			IcmRelVo inserticmRel = new IcmRelVo();
    			inserticmRel =  insertList.get(i);
    			
    			
    			// LOGIC INFO
            	// 1. 이미 클래스에 등록된 회원인가 ?
            	// 2. 1의 그렇다-> 패스!
            	// 3. 1의 아니다-> 캠퍼스만 등록된 회원인가?
            	// 4. 3의 그렇다-> 클래스에 등록된 회원으로 업데이트 처리 
            	// 5. 3의 아니다-> 새로운 클래스 등록이니 ICM_REL 에 인서트 처리 
    			
    			
    			
                // 이미 기등록된 데이터 확인(기등록된 데이터의 경우는 재 등록 하지 않는다.)
            	int existClassCnt = dao.getExistAssignCnt(inserticmRel);
            	
            	if(existClassCnt <= 0){
            		
            		
            		IcmRelVo existCampusVo = new IcmRelVo();
                	
            		existCampusVo.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
            		existCampusVo.setIcmSeq(inserticmRel.getIcmSeq());
            		existCampusVo.setRelSect(TreeProperties.getProperty("tree_re_campus")); //RE002
            		existCampusVo.setRelSeq(inserticmRel.getCond_relSeq()); 
                	
            		// campus 만 등록 된 경우라면 업데이트 
            		int existCampusCnt = dao.getExistAssignCnt(existCampusVo);
            		
            		if(existCampusCnt > 0 ){
            			// 관계 테이블 update 처리 
            			resultCodeMsg = commService.updateIcmRelData(inserticmRel);
            		}else{
            			// 관계 테이블 insert 처리 
            			resultCodeMsg = commService.addIcmRelData(inserticmRel);
            		}
            		
        			
        			// class history insert  (COURSE_HISTORY table insert )
        			CourseHistory clsHis = new CourseHistory();
        			clsHis.setClsSeq(inserticmRel.getRelSeq()); // classSeq
        			clsHis.setMbrId(inserticmRel.getIcmSeq()); // mbrId
        			clsHis.setRegId(mbrId); // regId
        			
        			dao.addCourseHistory(clsHis);
        			
        			// 회원의 등급 업데이트 처리 
        			Student stuVo = new Student();
        			stuVo.setMbrId(inserticmRel.getIcmSeq());
        			stuVo.setSect(TreeProperties.getProperty("tree_ms_regist")); // 수강회원 MS001
        			
        			dao.updateMbrSect(stuVo);
        		}
            	
        	}    			

    		
    	}
    	
    	if(deleteList.size() > 0 ){
    		for(int h=0; h< deleteList.size(); h++){
    			
    			IcmRelVo deleteicmRel = new IcmRelVo();
    			deleteicmRel =  deleteList.get(h);
    			
    			// 관계 테이블 delete 처리 
    			resultCodeMsg = commService.updateIcmRelData(deleteicmRel);	
    			
    			Student stuVo = new Student();
    			stuVo.setMbrId(deleteicmRel.getIcmSeq());
    			// 수강중인 클래스 카운트 조회 
    			int haveClassCnt = dao.getHaveClassCnt(stuVo);
    			
    			if(haveClassCnt < 1){
    				// 회원의 등급 업데이트 처리 
//        			Student stuVo = new Student();
//        			stuVo.setMbrId(deleteicmRel.getIcmSeq());
        			stuVo.setSect(TreeProperties.getProperty("tree_ms_regular")); // 정지회원 MS002
        			
        			dao.updateMbrSect(stuVo);	
    			}
    			
    		}
    		
    	}
    	

        return resultCodeMsg;
	}
	
	@Override
    @Transactional
	public int addCourseHistory(VSObject vsObject)throws Exception{
		return dao.addCourseHistory(vsObject);
	}
	
	/**
	 * Excel import
	 */
    @Override
    @Transactional
    public VSResult<?> inputExcelData(VSObject vsObject) throws Exception {
        MemberExcelCondition condition = (MemberExcelCondition) vsObject;
        
        MultipartFile multipartFile = condition.getMultipartFile();
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        String userId = userDetail.getUsername();
        
        
        
       String property = "java.io.tmpdir";
        
        // Get the temporary directory and print it.
        String tempDir = System.getProperty(property);
        
        File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + 
        		multipartFile.getOriginalFilename());
        
        
        multipartFile.transferTo(tmpFile);
        
        
        //FileInputStream fis = (FileInputStream) multipartFile.getInputStream();
        
        int resultCnt = 0;
        // 위에서 부터 읽지 않는 ROW 수
        int unusedRow = 1;
        // 10은 읽어야할 CELL의 갯수
        // TreeExcelReader excelReader = new TreeExcelReader(fis,  multipartFile.getOriginalFilename(), 10, unusedRow);
        
        TreeExcelReader excelReader = new TreeExcelReader(tmpFile.getAbsolutePath(), 2, unusedRow);
        
        
        for (int index = 0; index < excelReader.getNumberOfSheets(); index++) {
            // sheet index
            excelReader.setSheetNum(index);
            ArrayList<Object> rowList = null;
            
        	do {
        		rowList = excelReader.readRow();

                
        		if (rowList == null) break;
               
        		// cell index
        		int cellIndex = 0;
                
                // 시트에 문자또는 숫자 형식으로 넘어올수 있으니 형변화 처리 해준다. 
                String cSeq = String.valueOf(rowList.get(cellIndex++)).trim();
                Double doubleSeq= Double.parseDouble(cSeq);
                int intSeq = doubleSeq.intValue();
                
                
                String classSeq     = String.valueOf(intSeq);
                String studentId    = String.valueOf(rowList.get(cellIndex++)).trim();
                
                resultCnt++;
                
                String campSeq = dao.getCampuseSeq(classSeq);
                
                logger.debug("classSeq="+classSeq);
                logger.debug("studentId="+studentId);
                logger.debug("campSeq="+campSeq);
                
                // 필수 항목 체크
                if (TreeAdminUtil.isNull(classSeq) == "") {
                	
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("classSeq is null error");
                    throw new TreeRuntimeException(code, msg);
                }
                
                // 클래스 seq 정합성 체크(존재하는 클래스 인가 확인)
                int existClass = dao.getExistClassCnt(classSeq);
                if(existClass <= 0){
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("class is not exist  error / classSeq = "+classSeq);
                    throw new TreeRuntimeException(code, msg);
                }
                
                // 회원 아이디 정합성 체크(존재하는 아이디 인가 확인)
                int existMember = dao.getExistMemberCnt(studentId);
                
                if(existMember <= 0){
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("member is not exist  error / studentId = "+studentId);
                    throw new TreeRuntimeException(code, msg);
                }
                
                try {
                	
                	IcmRelVo insertVo = new IcmRelVo();
                	
                	insertVo.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
                	insertVo.setIcmSeq(studentId);  // memberId
                	insertVo.setRelSect(TreeProperties.getProperty("tree_re_class")); //RE003
                	insertVo.setRelSeq(classSeq);  // classSeq
                	
                	insertVo.setCond_icmSect(TreeProperties.getProperty("tree_re_member")); //RE004
                	insertVo.setCond_icmSeq(studentId);
                	insertVo.setCond_relSect(TreeProperties.getProperty("tree_re_campus")); //RE003
                	insertVo.setCond_relSeq(campSeq);
                	
                    // 이미 기등록된 데이터 확인(기등록된 데이터의 경우는 재 등록 하지 않는다.)
                	int existClassCnt = dao.getExistAssignCnt(insertVo);
        			
                	
                	if(existClassCnt <= 0){
                		
                		IcmRelVo existCampusVo = new IcmRelVo();
                    	
                		existCampusVo.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
                		existCampusVo.setIcmSeq(studentId);
                		existCampusVo.setRelSect(TreeProperties.getProperty("tree_re_campus")); //RE003
                		existCampusVo.setRelSeq(campSeq);
                    	
                		// campus 만 등록 된 경우라면 업데이트 
                		int existCampusCnt = dao.getExistAssignCnt(existCampusVo);
                		
                		if(existCampusCnt > 0 ){
                			// 관계 테이블 update 처리 
                			commService.updateIcmRelData(insertVo);
                		}else{
                			// 관계 테이블 insert 처리 
                			commService.addIcmRelData(insertVo);
                		}
                		
                		
            			// class history insert  (COURSE_HISTORY table insert )
            			CourseHistory clsHis = new CourseHistory();
            			clsHis.setClsSeq(insertVo.getRelSeq()); // classSeq
            			clsHis.setMbrId(insertVo.getIcmSeq()); // mbrId
            			clsHis.setRegId(userId); // regId
            			
            			dao.addCourseHistory(clsHis);
            			
            			// 회원의 등급 업데이트 처리 
            			Student stuVo = new Student();
            			stuVo.setMbrId(studentId);
            			stuVo.setSect(TreeProperties.getProperty("tree_ms_regist")); // 수강회원 MS001
            			
            			dao.updateMbrSect(stuVo);
                	}

        			
        			
                } catch (DuplicateKeyException e) {     // 중복 데이터 롤백.
                    String code = TreeProperties.getProperty("error.duplicatekey.code");
                    String msg = String.valueOf(resultCnt + unusedRow); 
                    excelReader.close();
                    throw new TreeRuntimeException(code, msg);
                } 
                
        	} while (rowList!=null);     
        	
            
        }
        excelReader.close();
        VSResult<?> resultCodeMsg = new VSResult<>();
        resultCodeMsg.setCode(TreeProperties.getProperty("error.excel.success.code"));
        resultCodeMsg.setMessage(String.valueOf(resultCnt));
        
        return resultCodeMsg;
    }	
}
