package com.visangesl.treeadmin.amg.vo;

import com.visangesl.treeadmin.vo.VSListCondition;

public class ProgramCondition extends VSListCondition {
	
	private String progSeq;
	private String progNm;
	private String instituteSeq;
	private String itemType;
	private String useYn;
	private String orderColumn;
	private String sort;
	
	
	public String getProgNm() {
		return progNm;
	}
	public void setProgNm(String progNm) {
		this.progNm = progNm;
	}
	public String getProgSeq() {
		return progSeq;
	}
	public void setProgSeq(String progSeq) {
		this.progSeq = progSeq;
	}
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
	
	
}
