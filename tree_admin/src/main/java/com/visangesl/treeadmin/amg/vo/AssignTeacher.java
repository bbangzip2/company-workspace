package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class AssignTeacher extends VSObject {
	private String teacherId;
	private String teacherNm;
	
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherNm() {
		return teacherNm;
	}
	public void setTeacherNm(String teacherNm) {
		this.teacherNm = teacherNm;
	}
	
	
}
