package com.visangesl.treeadmin.amg.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.ProgramDao;
import com.visangesl.treeadmin.amg.service.ProgramService;
import com.visangesl.treeadmin.amg.vo.ProgItem;
import com.visangesl.treeadmin.amg.vo.Program;
import com.visangesl.treeadmin.lcms.program.vo.Item;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class ProgramServiceImpl implements ProgramService {

	@Autowired
	ProgramDao dao;

	@Override
	public VSObject getData(VSObject vsObject) throws Exception {
		return dao.getData(vsObject);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return dao.getDataList(dataCondition);
    }
    
	@Override
    @Transactional(readOnly = true)
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
        return dao.getDataListCnt(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
	public List<VSObject> getInstituteItemList(VSCondition dataCondition) throws Exception{
		return dao.getInstituteItemList(dataCondition);
	}
	
	@Override
    @Transactional(readOnly = true)
	public List<VSObject> getProgItemList(String progSeq) throws Exception{
		return dao.getProgItemList(progSeq);
	}
	
	
	@Override
    @Transactional
    public VSResult modifyProgramInfoData(Program program , String[] prodVal) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.modifyProgramInfoData(program); 
        
        
        // item delete 
        dao.deleteProgramItemData(program);
        
        // item insert 
        List<Item> itemList = new ArrayList<Item>(); 
        
        if(prodVal != null && prodVal.length > 0){
        	
        	for(int i=0; i < prodVal.length; i ++){
        		//prodVal = prodSeq|itemType 으로 되어있음.
        		String[] itemVal = prodVal[i].split("#");
        		
        		ProgItem itemVo = new ProgItem();
	        	
	        	itemVo.setProgSeq(program.getProgSeq());
	        	itemVo.setItemSeq(itemVal[0]);
	        	itemVo.setItemType(itemVal[1]);
	        	
	        	
	        	dao.addProgramItemData(itemVo);
	        }	
        }
        		
        if (affectedRows < 1) {
        	
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult addProgramInfoData(Program program , String[] prodVal) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addProgramInfoData(program);
        
        String progSeq = program.getProgSeq(); // 저장된 프로그램 순번 
        
        
        if(prodVal != null && prodVal.length > 0){
        	
        	for(int i=0; i < prodVal.length; i ++){
        		//prodVal = prodSeq|itemType 으로 되어있음.
        		String[] itemVal = prodVal[i].split("#");
        		
        		ProgItem itemVo = new ProgItem();
	        	
	        	itemVo.setProgSeq(progSeq);
	        	itemVo.setItemSeq(itemVal[0]);
	        	itemVo.setItemType(itemVal[1]);
	        	
	        	dao.addProgramItemData(itemVo);
	        }	
        }
        

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	
	@Override
    @Transactional
    public VSResult addProgramItemData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addProgramItemData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	
    public VSResult deleteProgramItemData(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.deleteProgramItemData(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    } 
    
}
