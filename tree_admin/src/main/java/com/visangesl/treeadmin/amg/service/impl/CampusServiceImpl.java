package com.visangesl.treeadmin.amg.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.CampusDao;
import com.visangesl.treeadmin.amg.service.CampusService;
import com.visangesl.treeadmin.amg.vo.Campus;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.excel.TreeExcelReader;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSListCondition;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class CampusServiceImpl implements CampusService {

	@Autowired
	CampusDao dao;
	
	@Autowired
	CommonService commService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public VSObject getData(VSObject vsObject) throws Exception {
		return dao.getData(vsObject);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return dao.getDataList(dataCondition);
    }
    
	
	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.modifyDataWithResultCodeMsg(vsObject); 
        		
        if (affectedRows < 1) {
        	
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult  addDataWithResultCodeMsg(Campus campus) throws Exception {
    	
		VSResult resultCodeMsg = new VSResult();
    	IcmRelVo icmRel = new IcmRelVo();
    	
		icmRel.setIcmSect(TreeProperties.getProperty("tree_re_campus")); //RE002 
		icmRel.setRelSeq(campus.getInsInfoSeq());
		icmRel.setRelSect(TreeProperties.getProperty("tree_re_institute")); //RE001 
		
        int affectedRows = dao.addDataWithResultCodeMsg(campus);
        
        String campSeq = campus.getCampSeq();
        
		icmRel.setIcmSeq(campSeq);
		
        
        // 관계 테이블 insert 처리 
        commService.addIcmRelData(icmRel);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	
	@Override
    @Transactional
    public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	int affectedRows = dao.deleteDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
	
	
	@Override
	public int campNameCheck(VSListCondition vsListCondition) throws Exception{
		return dao.campNameCheck(vsListCondition);
	}

	@Override
	public List<VSObject> getSelectCampusList(VSObject vsObject) throws Exception{
			
		return dao.getSelectCampusList(vsObject);
	}
	
    @Override
    @Transactional
    public VSResult<?> inputExcelData(VSObject vsObject) throws Exception {
        MemberExcelCondition condition = (MemberExcelCondition) vsObject;
        
        MultipartFile multipartFile = condition.getMultipartFile();
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        String userId = userDetail.getUsername();
        
       String property = "java.io.tmpdir";
        
        // Get the temporary directory and print it.
        String tempDir = System.getProperty(property);
        
        File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + 
        		multipartFile.getOriginalFilename());
        
        
        multipartFile.transferTo(tmpFile);
        
        
        //java.io.ByteArrayInputStream cannot be cast to java.io.FileInputStream Error 발생지점임. Y ????? 
        
//        FileInputStream fis = (FileInputStream) multipartFile.getInputStream();
        
        // Error 발생 지점 끝.
        
        
        
        int resultCnt = 0;
        // 위에서 부터 읽지 않는 ROW 수
        int unusedRow = 1;
        // 10은 읽어야할 CELL의 갯수
        //TreeExcelReader excelReader = new TreeExcelReader(fis,  multipartFile.getOriginalFilename(), 10, unusedRow);
        
        TreeExcelReader excelReader = new TreeExcelReader(tmpFile.getAbsolutePath(), 10, unusedRow);
        
        // 기관의 아이디를 조회한다. 캠퍼스 아이디를 만들기 위해서 필요함.
        String insId = dao.getInstituteId(insSeq);
        
        for (int index = 0; index < excelReader.getNumberOfSheets(); index++) {
            // sheet index
            excelReader.setSheetNum(index);
        
            ArrayList<Object> rowList = null;
            
        	do {
        		rowList = excelReader.readRow();
        		int cellIndex = 0;
        		
        		
        		if (rowList == null) break;

                String campusName  = String.valueOf(rowList.get(cellIndex++));
                String province    = String.valueOf(rowList.get(cellIndex++));
                String info        = TreeAdminUtil.isNull(String.valueOf(rowList.get(cellIndex++)), "");
                
                resultCnt++;
                
                logger.debug("campusName="+campusName);
                logger.debug("province="+province);
                logger.debug("info="+info);
                
                // 필수 항목 체크
                if (TreeAdminUtil.isNull(campusName) == "") {
                	
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("campusName is null error");
                    throw new TreeRuntimeException(code, msg);
                }
                
                // campNm max check 50 byte
                if(campusName.getBytes().length >50){
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("campusName over size 50 byte");
                	throw new TreeRuntimeException();
                }
                
                // province max check  byte check 20byte
                if(province.getBytes().length >20){
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("province over size 20 byte");
                	throw new TreeRuntimeException();                	
                	
                }
                
                // info max check  byte check 500byte
                if(info.getBytes().length >500){
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("info over size 500 byte");
                	throw new TreeRuntimeException();                	
                }
                
                
                Campus campusVo = new Campus();
                campusVo.setCampNm(campusName);
                campusVo.setUseYn("Y");
                campusVo.setMbrId(userId);
                campusVo.setAreaNm(province);
                campusVo.setDetailInfo(info);
                campusVo.setInsInfoSeq(insSeq);
                campusVo.setInsInfoId(insId); // 기관 아이디 
                
                try {
                	
            		// campus insert 
                    int campResult = dao.addDataWithResultCodeMsg(campusVo);

                    
                    if (campResult > 0 ) {
                    	
                    	IcmRelVo icmRel = new IcmRelVo();
                		icmRel.setIcmSect(TreeProperties.getProperty("tree_re_campus")); //RE002 
                		icmRel.setRelSeq(campusVo.getInsInfoSeq());
                		icmRel.setRelSect(TreeProperties.getProperty("tree_re_institute")); //RE001
                		
                        String campSeq = campusVo.getCampSeq();
                		icmRel.setIcmSeq(campSeq);
                		
                        // 관계 테이블 insert 처리 
                        commService.addIcmRelData(icmRel);
                    	
                    }
                } catch (DuplicateKeyException e) {     // 중복 데이터 롤백.
                    String code = TreeProperties.getProperty("error.duplicatekey.code");
                    String msg = String.valueOf(resultCnt + unusedRow); 
                    excelReader.close();
                    throw new TreeRuntimeException(code, msg);
                } 
        	} while (rowList!=null);               
            
        }
        //엑셀 객체 close 
        excelReader.close();
        VSResult<?> resultCodeMsg = new VSResult<>();
        resultCodeMsg.setCode(TreeProperties.getProperty("error.excel.success.code"));
        resultCodeMsg.setMessage(String.valueOf(resultCnt));
        
        return resultCodeMsg;
    }
    
}
