package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.vo.VSResult;
/**
 * 클래스의 차시 정보를 담을 Vo
 * 
 * @author hong
 *
 */
public class TodayClassVo extends VSResult<VSObject>{

    private String clsSeq;
    private String dayNoCd;
    private String sortOrd;
    
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String sortOrd) {
		this.sortOrd = sortOrd;
	}


}
