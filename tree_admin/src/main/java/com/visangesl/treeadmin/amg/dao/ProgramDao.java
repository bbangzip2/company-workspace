package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class ProgramDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getData(VSObject vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("program.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("program.getDataList", dataCondition);
    	return result;
    }
    
    
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
    	
    	return getSqlSession().selectOne("program.getDataListCnt", dataCondition);
    }
    
    
    public List<VSObject> getInstituteItemList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("program.getInstituteItemList", dataCondition);
    	return result;
    }
    
    public List<VSObject> getProgItemList(String progSeq) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("program.getProgItemList", progSeq);
    	return result;
    }
    

    public int addProgramInfoData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("program.addProgramInfoData", vsObject);
		return result;
	}
    
    
    public int modifyProgramInfoData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("program.modifyProgramInfoData", vsObject);
		return result;
	}
    
    public int addProgramItemData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("program.addProgramItemData", vsObject);
		return result;
	}
    

    public int deleteProgramItemData(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("program.deleteProgramItemData", vsObject);
		return result;
	}
    
    
}
