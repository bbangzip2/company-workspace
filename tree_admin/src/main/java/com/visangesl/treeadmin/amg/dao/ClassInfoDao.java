package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.TodayClassCondition;
import com.visangesl.treeadmin.amg.vo.TodayClassVo;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.vo.CodeVo;

@Repository
public class ClassInfoDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getData(VSObject vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("classinfo.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getDataList", dataCondition);
    	return result;
    }
    
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
    	
    	return getSqlSession().selectOne("classinfo.getDataListCnt", dataCondition);
    }
    
    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("classinfo.modifyDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("classinfo.addDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("classinfo.deleteDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public List<VSObject> getSchdlDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getSchdlDataList", dataCondition);
    	return result;
    }
    
    public int addSchdlDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("classinfo.addSchdlDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int deleteSchdlData(VSCondition dataCondition) throws Exception{
		int result = 0;
		result = getSqlSession().delete("classinfo.deleteSchdlData", dataCondition);
	return result;
    }
    
    public List<CodeVo> getClassTchList(String classSeq) throws Exception{
       	List<CodeVo> result = null;
    	result = getSqlSession().selectList("classinfo.getClassTchList", classSeq);
    	return result;
    }
    
    public List<CodeVo> getCampTchList(String campSeq) throws Exception{
       	List<CodeVo> result = null;
    	result = getSqlSession().selectList("classinfo.getCampTchList", campSeq);
    	return result;
    }
    
    
    public List<VSObject> getClassProgList(String insSeq) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getClassProgList", insSeq);
    	return result;
    }
    
    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception{
    	List<TodayClassVo> result = null;
    	result = getSqlSession().selectList("classinfo.getFirstLessonList", tcCond);
    	return result;
    }

    public String getClassNextdayDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return getSqlSession().selectOne("classinfo.getClassNextdayDayNoCd", tcCond);
    }
    
    
    
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return getSqlSession().selectOne("classinfo.getNextLessonDayNoCd", tcCond);
    }
    
    
    public int addDataTodayClass(TodayClassVo tcv) throws Exception{
    	int result = 0;
		result = getSqlSession().insert("classinfo.addDataTodayClass", tcv);
		return result;
    }
    
    
    public int insertAfterNextDayNoCd(TodayClassCondition tcCond) throws Exception{
		int result = 0;
		result = getSqlSession().delete("classinfo.insertAfterNextDayNoCd", tcCond);
	return result;
	
    }
    
    public int getClsProgressCnt(String clsSeq) throws Exception{
    	return getSqlSession().selectOne("classinfo.getClsProgressCnt", clsSeq);
    }
    
    public int deleteClsProgress(String clsSeq) throws Exception{
    	return getSqlSession().delete("classinfo.deleteClsProgress", clsSeq);
    }
    
    
    
    
}
