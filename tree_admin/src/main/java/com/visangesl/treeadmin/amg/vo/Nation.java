package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class Nation extends VSObject {
	private String mbrId;
	private String cdSubject;
	private String desCr;
	private String cd;
	private String refCode;
	private String orderNo;
	
	
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getCdSubject() {
		return cdSubject;
	}
	public void setCdSubject(String cdSubject) {
		this.cdSubject = cdSubject;
	}
	public String getDesCr() {
		return desCr;
	}
	public void setDesCr(String desCr) {
		this.desCr = desCr;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getRefCode() {
		return refCode;
	}
	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
}
