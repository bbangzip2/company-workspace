package com.visangesl.treeadmin.amg.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.StudentDao;
import com.visangesl.treeadmin.amg.service.StudentService;
import com.visangesl.treeadmin.amg.vo.CourseHistory;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.amg.vo.Student;
import com.visangesl.treeadmin.amg.vo.StudentCondition;
import com.visangesl.treeadmin.excel.TreeExcelReader;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.member.service.MemberService;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class StudentServiceImpl implements StudentService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private StudentDao dao;
	
	@Autowired
    private MemberService memberService;
	
	@Autowired
    private CommonService commonService;
	
	@Override
	public VSObject getData(VSObject vsObject) throws Exception {
		return dao.getData(vsObject);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return dao.getDataList(dataCondition);
    }
    
	@Override
    @Transactional(readOnly = true)
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
        return dao.getDataListCnt(dataCondition);
    }
	
	
	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.modifyDataWithResultCodeMsg(vsObject); 
        		
        if (affectedRows < 1) {
        	
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addDataWithResultCodeMsg(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }

    @Override
    public List<VSObject> getCampusList(VSObject vsObject) throws Exception {
        return dao.getCampusList(vsObject);
    }

    @Override
    public List<VSObject> getProgramList(VSObject vsObject) throws Exception {
        return dao.getProgramList(vsObject);
    }

    @Override
    public List<VSObject> getClassList(VSObject vsObject) throws Exception {
        return dao.getClassList(vsObject);
    }
    
    @Override
    @Transactional
    public VSResult<?> inputExcelData(VSObject vsObject) throws Exception {
        MemberExcelCondition condition = (MemberExcelCondition) vsObject;
        
        String nationCd = condition.getNationCd();
        MultipartFile multipartFile = condition.getMultipartFile();
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        String userId = userDetail.getUsername();
        
        
       String property = "java.io.tmpdir";
        
        // Get the temporary directory and print it.
        String tempDir = System.getProperty(property);
        
        File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + 
        		multipartFile.getOriginalFilename());
        
        
        multipartFile.transferTo(tmpFile);
        
        //FileInputStream fis = (FileInputStream) multipartFile.getInputStream();
        
        int resultCnt = 0;
        // 위에서 부터 읽지 않는 ROW 수
        int unusedRow = 1;
        // 10은 읽어야할 CELL의 갯수
        // TreeExcelReader excelReader = new TreeExcelReader(fis,  multipartFile.getOriginalFilename(), 10, unusedRow);
        
        TreeExcelReader excelReader = new TreeExcelReader(tmpFile.getAbsolutePath(), 10, unusedRow);
        
        
        for (int index = 0; index < excelReader.getNumberOfSheets(); index++) {
            // sheet index
            excelReader.setSheetNum(index);
        
            ArrayList<Object> rowList = null;
        	do {
        		rowList = excelReader.readRow();

        		
        		if (rowList == null) break;
                resultCnt++;
                // cell index
                int cellIndex = 0;
                String mbrId    = String.valueOf(rowList.get(cellIndex++));
                String pwd      = String.valueOf(rowList.get(cellIndex++));
                String name     = String.valueOf(rowList.get(cellIndex++));
                String nickName = String.valueOf(rowList.get(cellIndex++));
                String email    = String.valueOf(rowList.get(cellIndex++));
                String sexSect  = String.valueOf(rowList.get(cellIndex++));
                String birthDay = String.valueOf(rowList.get(cellIndex++));
                String telNo    = String.valueOf(rowList.get(cellIndex++));
                String useYn    = String.valueOf(rowList.get(cellIndex++));
                String campId   = String.valueOf(rowList.get(cellIndex++));
                
                
//                String bDay = null;
//                if (birthDay != null) {
//                    SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
//                    bDay = format.format(birthDay);
//                }
                
                // 필수 항목 체크
                if (TreeAdminUtil.isNull(mbrId) == "" || TreeAdminUtil.isNull(pwd)      == "" ||
                    TreeAdminUtil.isNull(name)  == "" || TreeAdminUtil.isNull(nickName) == "" ||
                    TreeAdminUtil.isNull(email) == "" || TreeAdminUtil.isNull(sexSect)  == "" ||
                    TreeAdminUtil.isNull(useYn) == "" || TreeAdminUtil.isNull(campId)   == "") {
                	
                	String code = TreeProperties.getProperty("error.excel.fail.code");
                	// 현재 DB에 넣는 Row 번호와 사용하지 않는 Row 번호를 더하면 엑셀파일의 라인이 나온다.
                	String msg = String.valueOf(resultCnt + unusedRow);
                	logger.error("validation error");
                    throw new TreeRuntimeException(code, msg);
                }
                
                Member member = new Member();
                member.setMbrId(mbrId);
                member.setPwd(pwd);
                member.setName(name);
                member.setNickName(nickName);
                member.setEmail(email);
                member.setSexSect(sexSect.toUpperCase());
                member.setTelNo(telNo);
                member.setUseYn(useYn.toUpperCase());
                member.setBirthDay(birthDay);
                member.setRegId(userId);
                member.setModId(userId);
                member.setNation(nationCd);
                member.setSect("MS002");        // 회원구분 : 정회원
                member.setMbrGrade("MG220");    // 사용자등급 : 학생
                
                try {
                    VSResult<VSObject> memberResult = memberService.addDataWithResultCodeMsg(member);
                    if (memberResult != null && memberResult.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
                        // 소속 캠퍼스 추가.
                        StudentCondition studentCondition = new StudentCondition();
                        studentCondition.setInsSeq(insSeq);
                        studentCondition.setCampId(campId);
                        // 해당 기관에 캠퍼스 아이디를 가지고 캠퍼스 순번을 가져온다.
                        String campSeq = dao.getCampusSeq(studentCondition);
                        // 없으면 해당 기관에 캠퍼스가 없는 것이다.
                        if (TreeAdminUtil.isNull(campSeq) == "") {
                            String code = TreeProperties.getProperty("error.excel.fail.code");
                            String msg = String.valueOf(resultCnt + unusedRow);
                            logger.error("campSeq is not exist");
                            throw new TreeRuntimeException(code, msg);
                        }
                        
                        IcmRelVo icmRel = new IcmRelVo();
                        icmRel.setIcmSeq(mbrId);
                        icmRel.setRelSeq(campSeq);
                        icmRel.setIcmSect("RE004");
                        icmRel.setRelSect("RE002");
                        commonService.addIcmRelData(icmRel);
                    }
                } catch (DuplicateKeyException e) {     // 중복 데이터 롤백.
                    String code = TreeProperties.getProperty("error.duplicatekey.code");
                    String msg = String.valueOf(resultCnt + unusedRow); 
                    logger.error("duplicatekey");
                    excelReader.close();
                    throw new TreeRuntimeException(code, msg);
                } 
                
        	} while (rowList!=null);               
            
            
        }
        excelReader.close();
        VSResult<?> resultCodeMsg = new VSResult<>();
        resultCodeMsg.setCode(TreeProperties.getProperty("error.excel.success.code"));
        resultCodeMsg.setMessage(String.valueOf(resultCnt));
        
        return resultCodeMsg;
    }
    
    public boolean isMatches(String regex, String inputTxt) {
        return Pattern.matches(regex, inputTxt);
    }
	
}
