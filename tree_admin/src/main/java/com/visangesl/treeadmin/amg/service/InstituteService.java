package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfo;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfoCondition;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface InstituteService{
	// select One Data
	public VSObject getData(VSCondition dataCondition) throws Exception;
	
	// select List Data 
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;
	
	// select List Count 
	public int getDataListCnt(VSCondition dataCondition) throws Exception;
	
	// Update
	public VSResult<VSObject> modifyDataWithResultCodeMsg(VSCondition dataCondition) throws Exception;
	
	// Insert 
	public VSResult<VSObject> addDataWithResultCodeMsg(VSCondition dataCondition) throws Exception;
	
	// Delete 
	public VSResult<?> deleteDataWithResultCodeMsg(VSCondition dataCondition) throws Exception;
	
	public int getCampusCntForInstitute(VSCondition dataCondition) throws Exception;
	
	// check institute id
	public String getInstituteSeq(VSObject vsObject) throws Exception;
	
	public MemberInstituteInfo getInstituteInfoWithMemberId(MemberInstituteInfoCondition dataCondition) throws Exception;
}
