package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class StudentDao extends TreeSqlSessionDaoSupport {

//    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getData(VSObject vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("student.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("student.getDataList", dataCondition);
    	return result;
    }
    
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
    	
    	return getSqlSession().selectOne("student.getDataListCnt", dataCondition);
    }
    
    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("student.modifyDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("student.addDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("student.deleteDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public List<VSObject> getCampusList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("student.getCampusList", vsObject);
        return result;
    }

    public List<VSObject> getProgramList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("student.getProgramList", vsObject);
        return result;
    }

    public List<VSObject> getClassList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("student.getClassList", vsObject);
        return result;
    }  
    
    public String getCampusSeq(VSCondition dataCondition) throws Exception {
        return getSqlSession().selectOne("student.getCampusSeq", dataCondition);
    }
}
