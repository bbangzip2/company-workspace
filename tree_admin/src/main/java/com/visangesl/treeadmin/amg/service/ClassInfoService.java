package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.ClassInfo;
import com.visangesl.treeadmin.amg.vo.ClassSchdl;
import com.visangesl.treeadmin.amg.vo.ClassSchdlCondition;
import com.visangesl.treeadmin.amg.vo.TodayClassCondition;
import com.visangesl.treeadmin.amg.vo.TodayClassVo;
import com.visangesl.treeadmin.vo.CodeVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface ClassInfoService  {
	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;
	
	// select List Data 
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;
	
	// 클래스에 등록된 선생목록 
	public List<CodeVo> getClassTchList(String classSeq) throws Exception;
	
	// 캠퍼스에 속한 선생목록 	
	public List<CodeVo> getCampTchList(String campSeq) throws Exception;
	
	// select List Count 
	public int getDataListCnt(VSCondition dataCondition) throws Exception;
	
	// Update
	public VSResult modifyDataWithResultCodeMsg(ClassInfo clsInfo, ClassSchdlCondition csCond, List<ClassSchdl> schList) throws Exception;
	
	// Insert 
	public VSResult addDataWithResultCodeMsg(ClassInfo classInfo) throws Exception;
	
	// Delete 
	public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// 클래스 스케쥴 목록 조회 
	public List<VSObject> getSchdlDataList(VSCondition dataCondition) throws Exception;
	
	// 클래스 스케쥴 등록 
	public VSResult addSchdlDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// 클래스 스케쥴 삭제
	public int deleteSchdlData(VSCondition dataCondition) throws Exception;
	
	// 해당 institute에서 사용 가능한 프로그램 목록 조회 
	public List<VSObject> getClassProgList(String insSeq) throws Exception ;
	
    /**
     * 클래스의 첫번째 차시 목록 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception;
    
    /**
     * 현재 차시의 다음 차시 조회 
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getClassNextdayDayNoCd(TodayClassCondition tcCond) throws Exception;
    
    /**
     * 다음 레슨의 첫 번째 차시 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception;

    /**
     * 클래스의 차시 정보 등록 처리
     *
     * @param clsSeq
     * @param dayNoCd
     * @param sortOrd
     * @return
     * @throws Exception
     */
    public int addDataTodayClass(String clsSeq, String dayNoCd, String sortOrd) throws Exception;
    
    /**
     * 다음 다음 차시 등록 
     * 
     * @param tcCond
     * @return
     * @throws Exception
     */
    public int insertAfterNextDayNoCd(TodayClassCondition tcCond) throws Exception;
    
    /**
     * 클래스의 진행된 차시 갯수 조회
     * 
     * @param clsSeq
     * @return
     * @throws Exception
     */
    public int getClsProgressCnt(String clsSeq) throws Exception;
    
    /**
     * 클래스에 등록된 차시 정보 삭제 
     * 
     * @param clsSeq
     * @return
     * @throws Exception
     */
    public int deleteClsProgress(String clsSeq) throws Exception;
}
