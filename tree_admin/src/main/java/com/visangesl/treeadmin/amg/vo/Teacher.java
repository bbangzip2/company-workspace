package com.visangesl.treeadmin.amg.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

//@XmlRootElement(name="teacher")
public class Teacher extends VSObject {
	private String rn;
	private String mbrId;
	private String pwd;
	private String clsSeq;
	private String useYn;
	private String name;
	private String nickName;
	private String regDate;
	private String modDate;
	private String regId;
	private String modId;
	private String campNm;
	private String profilePhotopath;
	private String sexSect;
	private String email;
	private String telNo;
	private String memo;
	private String mbrGrade;
	private List<Campus> campList;
	private List<ClassInfo> classList;
	
	public String getRn() {
		return rn;
	}
	public void setRn(String rn) {
		this.rn = rn;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getRegDate() {
        return regDate;
    }
    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }
    public String getModDate() {
        return modDate;
    }
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getModId() {
        return modId;
    }
    public void setModId(String modId) {
        this.modId = modId;
    }
    public String getCampNm() {
		return campNm;
	}
	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}
	public String getProfilePhotopath() {
		return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}
	public String getSexSect() {
		return sexSect;
	}
	public void setSexSect(String sexSect) {
		this.sexSect = sexSect;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelNo() {
		return telNo;
	}
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<Campus> getCampList() {
		return campList;
	}
	public void setCampList(List<Campus> campList) {
		this.campList = campList;
	}
    public List<ClassInfo> getClassList() {
        return classList;
    }
    public void setClassList(List<ClassInfo> classList) {
        this.classList = classList;
    }
    public String getMbrGrade() {
		return mbrGrade;
	}
	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}
}
