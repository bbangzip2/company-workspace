package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class ClassSchdl extends VSObject {

	private String clsSeq;
	private String clsSchdlSeq;
	private String ymd;
	private String startHH;
	private String startMM;
	private String endHH;
	private String endMM;
	private String wk;
	private String ingStat;
	private String regId;
	private String regDate;
	private String modId;
	private String modDate;
	
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getClsSchdlSeq() {
		return clsSchdlSeq;
	}
	public void setClsSchdlSeq(String clsSchdlSeq) {
		this.clsSchdlSeq = clsSchdlSeq;
	}
	public String getYmd() {
		return ymd;
	}
	public void setYmd(String ymd) {
		this.ymd = ymd;
	}
	public String getStartHH() {
		return startHH;
	}
	public void setStartHH(String startHH) {
		this.startHH = startHH;
	}
	public String getStartMM() {
		return startMM;
	}
	public void setStartMM(String startMM) {
		this.startMM = startMM;
	}
	public String getEndHH() {
		return endHH;
	}
	public void setEndHH(String endHH) {
		this.endHH = endHH;
	}
	public String getEndMM() {
		return endMM;
	}
	public void setEndMM(String endMM) {
		this.endMM = endMM;
	}
	public String getWk() {
		return wk;
	}
	public void setWk(String wk) {
		this.wk = wk;
	}
	public String getIngStat() {
		return ingStat;
	}
	public void setIngStat(String ingStat) {
		this.ingStat = ingStat;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
}
