package com.visangesl.treeadmin.amg.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.Campus;
import com.visangesl.treeadmin.vo.VSListCondition;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public interface CampusService {
	// select One Data
	public VSObject getData(VSObject vsObject) throws Exception;
	
	// select List Data 
	public List<VSObject> getDataList(VSCondition dataCondition) throws Exception;
	
	// Update
	public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	// Insert 
	public VSResult addDataWithResultCodeMsg(Campus campus) throws Exception;
	
	// Delete 
	public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception;
	
	public int campNameCheck(VSListCondition vsListCondition) throws Exception;
    
	// Select - 캠퍼스 정보 출력
	public List<VSObject> getSelectCampusList(VSObject vsObject) throws Exception;
	
	// excel upload
	public VSResult<?> inputExcelData(VSObject vsObject) throws Exception;
}
