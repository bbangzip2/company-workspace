package com.visangesl.treeadmin.amg.controller;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.InstituteService;
import com.visangesl.treeadmin.amg.vo.Institute;
import com.visangesl.treeadmin.amg.vo.InstituteCondition;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfo;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfoCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.CodeCondition;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class InstituteController implements BaseController {

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private CommonService commonService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 기관 리스트
	 * @param request
	 * @param response
	 * @param model
	 * @param viewType     뷰 타입 (thumType, listType)
	 * @param orderColumn  정렬 컬럼명
	 * @param sort         정렬 순서
	 * @param nationCd     검색 : 국가코드
	 * @param useYn        검색 : 상태값
	 * @param searchKey    검색 : 키값
	 * @return
	 * @throws Exception
	 */
   @RequestMapping(value = "/amg/instituteList", method=RequestMethod.GET)
    public String instituteList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "viewType", defaultValue="thumType") String viewType,
            @RequestParam(value = "orderColumn", defaultValue="REG_DATE") String orderColumn,
            @RequestParam(value = "sort", defaultValue="DESC") String sort,
            @RequestParam(value = "nation", required = false) String nationCd,
            @RequestParam(value = "useYn", required = false) String useYn,
            @RequestParam(value = "searchKey", required = false) String searchKey) throws Exception {
        logger.debug("instituteList");

        InstituteCondition condition = new InstituteCondition();
        condition.setOrderColumn(orderColumn);
        condition.setSort(sort);

        // 검색 나라명
        if (TreeAdminUtil.isNull(nationCd) != "") {
            condition.setNationCd(nationCd);
        }

        // 검색 상태값
        if (TreeAdminUtil.isNull(useYn) != "") {
            condition.setUseYn(useYn);
        }

        // 검색 키값
        if(TreeAdminUtil.isNull(searchKey) != "") {
            condition.setSearchKey(searchKey);
        }

        List<VSObject> insList = instituteService.getDataList(condition);
        int i = insList.size();
        Iterator<VSObject> it = insList.iterator();
        while(it.hasNext()) {
            Institute ins = ((Institute) it.next());
            ins.setRn(String.valueOf(i--));
        }



        CodeCondition codCond = new CodeCondition();
        // TODO: REF_CODE 값을 NA000으로 DB 를 조회하여 데이터 요청.
        codCond.setRelCd("NA000");
        List<VSObject> nationList = commonService.getCodeList(codCond);

        model.addAttribute("condition", condition);
        model.addAttribute("insList", insList);
        model.addAttribute("nationList", nationList);
        model.addAttribute("viewType", viewType);

        return "/amg/instituteList";
    }

   /**
    * 기관 상세 페이지
    *
    * 기관관리자, 캠퍼스관리자가 로그인시 접속하는 첫 페이지.
    * 이때 자신이 속한 기관정보(기관 순번, 이름, 로고 경로)를 세션에 담는 초기 작업을 진행한다.
    *
    * 기관관리자의 경우 /amg/instituteInfo 페이지를 열고
    * 캠퍼스관리자의 경우 /amg/campusList 페이지로 redirect 시킨다.
    *
    * @param request
    * @param response
    * @param model
    * @param insInfoSeq
    * @return
    * @throws Exception
    */
	@RequestMapping(value = "/amg/instituteInfo", method=RequestMethod.GET)
	public String instituteInfo(HttpServletRequest request, HttpServletResponse response, Model model,
	        @RequestParam(value = "insInfoSeq", required = false) String insInfoSeq) throws Exception {
		logger.debug("instituteInfo");

		String loadPage = null;

		MemberInstituteInfoCondition miInfoCondition = new MemberInstituteInfoCondition();
		boolean isGetInstituteInfo = false;

		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		if(TreeAdminUtil.isNull(insInfoSeq) != ""){
		    // 파라미터로 기관 순번이 넘어오면 세션에 할당한다.
			miInfoCondition.setInstituteSeq(Integer.parseInt(insInfoSeq));
			isGetInstituteInfo = true;
		} else if (TreeAdminUtil.isNull(userDetail.getInsSeq()) != "") {
		    // 세션에 기관 순번이 이미 있으면 변수에 할당한다.
		} else{
		    // 파라미터에도 없고 세션에도 없으면 로그인 사용자의 권한에 따라
		    // 기관 순번을 가져온다.
			miInfoCondition.setMbrId(userDetail.getUsername());
			isGetInstituteInfo = true;
		}

		if (isGetInstituteInfo) {
			MemberInstituteInfo memberInstituteInfo = instituteService.getInstituteInfoWithMemberId(miInfoCondition);

			if (memberInstituteInfo != null) {
				userDetail.setInsSeq(String.valueOf(memberInstituteInfo.getInstituteSeq()));
				userDetail.setInstituteLogoPath(memberInstituteInfo.getLogoPath());
				userDetail.setInstituteName(memberInstituteInfo.getName());
			}
		}

		if (TreeSpringSecurityUtils.getAuthoritiesToRoles().equals(TreeProperties.getProperty("tree_campusadmin"))) {
			loadPage = "redirect:/amg/campusList.do";
		} else {
			InstituteCondition condition = new InstituteCondition();
			condition.setSeq(userDetail.getInsSeq());

			Institute institute = (Institute) instituteService.getData(condition);

			model.addAttribute("insInfo", institute);

			loadPage = "/amg/instituteInfo";
		}

		return loadPage;
	}

	/**
	 * 기관 편집 페이지로 이동.
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/instituteEdit", method=RequestMethod.GET)
	public String infoEdit(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		logger.debug("instituteEdit");

		// 세션에서 기관 순번을 가져온다.
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String seq = userDetail.getInsSeq();

		InstituteCondition insCondition = new InstituteCondition();
		insCondition.setSeq(seq);

        Institute institute = (Institute) instituteService.getData(insCondition);

        String readOnlyInsCode = "N";
        // 기관이 캠퍼스와 연관이 있는지 조회 한다.
        int campCnt = instituteService.getCampusCntForInstitute(insCondition);
        logger.debug("campCnt --> " + campCnt);
        if (campCnt > 0) {
            readOnlyInsCode = "Y";
        }

        CodeCondition codCond = new CodeCondition();
		// TODO: REF_CODE 값을 NA000으로 DB 를 조회하여 데이터 요청.
        codCond.setRelCd("NA000");
		List<VSObject> nationList = commonService.getCodeList(codCond);



		model.addAttribute("insInfo", institute);
		model.addAttribute("nationList", nationList);
		model.addAttribute("readOnlyInsCode", readOnlyInsCode);

		return "/amg/instituteEdit";
	}

	/**
	 * 기관 편집
	 * @param request
	 * @param response
	 * @param model
	 * @param seq          순번
	 * @param id           아이디
	 * @param name         이름
	 * @param nationCd     국가코드
	 * @param info         인포메이션(메모기능)
	 * @param useYn        상태값
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/instituteModify", method=RequestMethod.POST)
	public ModelAndView infoModify(HttpServletRequest request, HttpServletResponse response, Model model,
	        @RequestParam(value = "insInfoSeq", required = false) String seq,
	        @RequestParam(value = "id", required = false) String id,
	        @RequestParam(value = "name", required = false) String name,
	        @RequestParam(value = "nation", required = false) String nationCd,
	        @RequestParam(value = "info", required = false) String info,
	        @RequestParam(value = "useYn", required = false) String useYn) throws Exception {
		logger.debug("instituteModify");

        InstituteCondition condition = new InstituteCondition();
        condition.setId(id);
        condition.setName(name);
        condition.setNationCd(nationCd);
        condition.setInfo(info);
        condition.setUseYn(useYn);

        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
            logger.debug("multipartFile : " + multipartFile);
            condition.setUploadFile(multipartFile);
        }

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();

        VSResult<VSObject> resultCodeMsg = null;
        // 파라미터로 기관 순번 값이 넘어오면 편집모드에서 온 것.
        boolean isModify = TreeAdminUtil.isNull(seq, null) != null;
        if (isModify) {
            condition.setSeq(seq);
		    condition.setModId(mbrId);
		    resultCodeMsg = instituteService.modifyDataWithResultCodeMsg(condition);
		} else {
		    // 기관 순번 값이 없으면 새로 등록 하는 것.
		    condition.setRegId(mbrId);
		    resultCodeMsg = instituteService.addDataWithResultCodeMsg(condition);
		    // 등록을 하고 seq 값을 받아온다.
            Institute institute = (Institute) instituteService.getData(condition);
            seq = institute.getSeq();
		}

        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            // 기관을 등록 할 때 ICM_REL의 정보도 등록한다.
            if (!isModify) {
                IcmRelVo icmRel = new IcmRelVo();
                icmRel.setIcmSeq(seq);
                icmRel.setIcmSect("RE001");
                commonService.addIcmRelData(icmRel);
            }
            // Success
    		ModelAndView mav = new ModelAndView();
    		mav.setViewName("redirect:/amg/instituteList.do");
    		// mav.addObject("insInfoSeq", seq);
    		return mav;
        } else {
        	//Fail
            StringBuffer sb = new StringBuffer();

            sb.append("<script type=\"text/javascript\">");
            sb.append("alert('처리작업이 실패하였습니다.\n다시 시도해주시기 바랍니다.');");
            sb.append("history.back();");
            sb.append("</script>");

            response.setBufferSize(8 * 1024); // 8K buffer
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html; charset=utf-8");
            PrintWriter out = response.getWriter();

//            int size = response.getBufferSize(); // returns 8096 or greater
            out.println(sb.toString());
            out.flush();

            return null;
        }
	}

	/**
	 * 기관 등록 페이지로 이동
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/instituteInsert", method=RequestMethod.GET)
	public String instituteInsert(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
	    logger.debug("instituteInsert");

        CodeCondition codCond = new CodeCondition();
        // TODO: REF_CODE 값을 NA000으로 DB 를 조회하여 데이터 요청.
        codCond.setRelCd("NA000");
        List<VSObject> nationList = commonService.getCodeList(codCond);

        model.addAttribute("nationList", nationList);
        model.addAttribute("readOnlyInsCode", "N");

	    return "/amg/instituteEdit";
	}

	/**
	 * 기관 아이디 체크 API
	 * @param request
	 * @param response
	 * @param model
	 * @param id
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/amg/checkInstituteCode")
    @ResponseBody
    public VSResult<?> checkInstituteCode(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "id", required = false) String id) throws Exception {
        logger.debug("checkInstituteCode");
        VSResult<?> result = new VSResult<>();

        InstituteCondition insCondition = new InstituteCondition();
        insCondition.setId(id);

        int totalCount = instituteService.getDataListCnt(insCondition);
        logger.debug("checkInstituteCode - totalCount : " + totalCount);
        result.setCode(TreeProperties.getProperty("error.success.code"));
        result.setMessage(String.valueOf(totalCount));

        return result;
    }
}
