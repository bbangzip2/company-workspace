package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.vo.VSListCondition;

@Repository
public class CampusDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getData(VSObject vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("campus.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("campus.getDataList", dataCondition);
    	return result;
    }
    
    
    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("campus.modifyDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("campus.addDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addAssinCampDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("campus.addAssinCampDataWithResultCodeMsg", vsObject);
		return result;
	}
    

    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("campus.deleteDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int deleteAssinCampDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("campus.deleteAssinCampDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int campNameCheck(VSListCondition vsListCondition) throws Exception{
    	int result = 0;
    	result = getSqlSession().selectOne("campus.campNameCheck", vsListCondition);	
    	return result;
    }

	public List<VSObject> getSelectCampusList(VSObject vsObject) {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("campus.getSelectCampusList", vsObject);
    	return result;
	}
	
    public String getInstituteId(String seq) throws Exception {
    	return getSqlSession().selectOne("campus.getInstituteId", seq);
    }
}
