package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.vo.IcmRelVo;

@Repository
public class AssignClassDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());
    
    public List<VSObject> getAssignClassList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getAssignClassList", dataCondition);
    	return result;
    }
    
    public int getAssignClassListCnt(VSCondition dataCondition) throws Exception {
    	
    	return getSqlSession().selectOne("classinfo.getAssignClassListCnt", dataCondition);
    }
    
    /**
     * 반배치 미정 학생 목록 
     * @param dataCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getUnassignStudentList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getUnassignStudentList", dataCondition);
    	return result;
    }
    
    /**
     * 캠퍼스 학생 목록/ 클래스에 속한 학생 목록
     * @param dataCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getCampusStudentList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getCampusStudentList", dataCondition);
    	return result;
    }
    
    /**
     * 반배정 팝업에서의 클래스 정보 조회
     * @param dataCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassDetailInfo(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getClassDetailInfo", dataCondition);
    	return result;
    }
    
    /**
     * 캠퍼스의 최근 클래스 목록 조회 
     * @param dataCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getRecentClassList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("classinfo.getRecentClassList", dataCondition);
    	return result;
    }
    
    /**
     * class history insert 
     * @param vsObject
     * @return
     * @throws Exception
     */
    public int addCourseHistory(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("classinfo.addCourseHistory", vsObject);
		return result;
	}
    
    /**
     * 회원의 등급 업데이트 처리 
     * @param vsObject
     * @return
     * @throws Exception
     */
    public int updateMbrSect(VSObject vsObject) throws Exception{
    	int result = 0;
    	result = getSqlSession().update("classinfo.updateMbrSect", vsObject);
    	return result;
    }
    
    /**
     * 수강중인 클래스수 
     * @param vsObject
     * @return
     * @throws Exception
     */
    public int getHaveClassCnt(VSObject vsObject) throws Exception {
    	
    	return getSqlSession().selectOne("classinfo.getHaveClassCnt", vsObject);
    }
    
    /**
     * 클래스 순번으로 캠퍼스 순번 구하기 
     * @param clsSeq
     * @return
     * @throws Exception
     */
    public String getCampuseSeq(String clsSeq) throws Exception{
    	return getSqlSession().selectOne("classinfo.getCampuseSeq", clsSeq);
    }
    
    /**
     * 존재하는 클래스 인지 체크 
     * @param clsSeq
     * @return
     * @throws Exception
     */
    public int getExistClassCnt(String clsSeq) throws Exception {
    	return getSqlSession().selectOne("classinfo.getExistClassCnt", clsSeq);
    }
    
    /**
     * 존재하는 아이디 인지 체크 
     * @param studentId
     * @return
     * @throws Exception
     */
    public int getExistMemberCnt(String studentId) throws Exception {
    	return getSqlSession().selectOne("classinfo.getExistMemberCnt", studentId);
    }    
    
    /**
     * 존재하는 assign student인지 체크 
     * @param icmRelVo
     * @return
     * @throws Exception
     */
    public int getExistAssignCnt(IcmRelVo icmRelVo) throws Exception {
    	return getSqlSession().selectOne("classinfo.getExistAssignCnt", icmRelVo);
    } 
        
}
