package com.visangesl.treeadmin.amg.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public class AssignStudent extends VSObject {
	
	private String instituteSeq;
	private String campSeq;
	private String classSeq;
	private String studentNm;
	private String studentId;
	private String sexSect;
	private String nickName;
	private String mbrNm;
	private String mbrId;
	
	private List<String> clsList;
	
	public List<String> getClsList() {
		return clsList;
	}
	public void setClsList(List<String> clsList) {
		this.clsList = clsList;
	}
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	
	public String getClassSeq() {
		return classSeq;
	}
	public void setClassSeq(String classSeq) {
		this.classSeq = classSeq;
	}
	public String getStudentNm() {
		return studentNm;
	}
	public void setStudentNm(String studentNm) {
		this.studentNm = studentNm;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getSexSect() {
		return sexSect;
	}
	public void setSexSect(String sexSect) {
		this.sexSect = sexSect;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getMbrNm() {
		return mbrNm;
	}
	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	

}
