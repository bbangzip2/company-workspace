package com.visangesl.treeadmin.amg.service.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.AdminDao;
import com.visangesl.treeadmin.amg.service.AdminService;
import com.visangesl.treeadmin.amg.vo.AdminMember;
import com.visangesl.treeadmin.file.helper.TreeFileUtils;
import com.visangesl.treeadmin.file.helper.vo.FileUploadResult;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class AdminServiceImpl implements AdminService  {
	 private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AdminDao dao;
		
	@Override
    @Transactional
	public VSResult addDataWithResultCodeMsg(VSObject vsObject)
		throws Exception {
    	VSResult resultCodeMsg = new VSResult();
    	
    	AdminMember admin = (AdminMember) vsObject;
        MultipartFile uploadFile = admin.getUploadFile();

        // 이미지 파일 업로드
        if (TreeAdminUtil.isNull(uploadFile.getOriginalFilename()) != "") {
            String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_portfolio_filepath");

            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
                admin.setProfilePhotoPath(savePath + "/" + result.getRealFileNm());
            }
        }
    	
        logger.debug("member profile path : " + admin.getProfilePhotoPath());
        System.out.println("member profile path : " + admin.getProfilePhotoPath());
    	
        int affectedRows = dao.addDataWithResultCodeMsg(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
	}

	@Override
    @Transactional
	public VSResult addDataIcmRel(VSObject vsObject) throws Exception {
		VSResult resultCodeMsg = new VSResult();
		 int affectedRows = dao.addDataIcmRel(vsObject); 

	        if (affectedRows < 1) {
	            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
	        } else {
	            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
	        }

	        return resultCodeMsg;
	}

	@Override
    @Transactional(readOnly = true)
	public List<VSObject> getDataList(VSObject dataCondition) throws Exception {
		 return dao.getDataList(dataCondition);
	}

	@Override
	public VSObject getData(VSObject dataCondition) throws Exception {
		return dao.getData(dataCondition);
	
	}

	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	AdminMember admin = (AdminMember) vsObject;
        MultipartFile uploadFile = admin.getUploadFile();
    	
    	// 이미지 파일 업로드
    	if (TreeAdminUtil.isNull(uploadFile.getOriginalFilename()) != "") {
    	    String rootPath = TreeProperties.getProperty("tree_save_filepath");
            String savePath = TreeProperties.getProperty("tree_portfolio_filepath");
            
            FileUploadResult result  = TreeFileUtils.uploadFile(uploadFile, rootPath + savePath);
            if (result.isUploaded()) {
                admin.setProfilePhotoPath(savePath + "/" + result.getRealFileNm());
                // 이전 업로드된 이미지 파일을 지운다.
                AdminMember adminData = (AdminMember) dao.getData(admin);
                if (TreeAdminUtil.isNull(adminData.getProfilePhotoPath()) != "") {
                    File preFile = new File(rootPath + adminData.getProfilePhotoPath());
                    TreeFileUtils.removeFile(preFile.getParent(), preFile.getName());
                }
            }
    	}
 
        int affectedRows = dao.modifyDataWithResultCodeMsg(vsObject); 
        		
        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
	
	@Override
	public VSObject getDataCampInfo(VSObject dataCondition) throws Exception {
		// TODO Auto-generated method stub
		return dao.getDataCampInfo(dataCondition);
	} 
}
