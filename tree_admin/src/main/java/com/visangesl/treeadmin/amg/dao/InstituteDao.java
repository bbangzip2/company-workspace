package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfo;
import com.visangesl.treeadmin.amg.vo.MemberInstituteInfoCondition;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class InstituteDao extends TreeSqlSessionDaoSupport {

//    private final Log logger = LogFactory.getLog(this.getClass());

    
    public VSObject getData(VSCondition vsObject) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("institute.getData", vsObject);
    	return result;
    }
    
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
    	List<VSObject> result = null;
    	result = getSqlSession().selectList("institute.getDataList", dataCondition);
    	return result;
    }
    
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
        int result = 0;
        result = getSqlSession().selectOne("institute.getDataListCnt", dataCondition);
        return result;
    }
    
    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("institute.modifyDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("institute.addDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().delete("institute.deleteDataWithResultCodeMsg", vsObject);
		return result;
	}
    
    
    public int getCampusCntForInstitute(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().selectOne("institute.getCampusCntForInstitute", vsObject);
        return result;
    }
    
    public String getInsInfoSeqForInstitute(VSObject vsObject) throws Exception {
        String result = null;
        result = getSqlSession().selectOne("institute.getInsInfoSeqForInstitute", vsObject);
        return result;
    }
    
    public String getInsInfoSeqForCampus(VSObject vsObject) throws Exception {
        String result = null;
        result = getSqlSession().selectOne("institute.getInsInfoSeqForCampus", vsObject);
        return result;
    }
    
    public MemberInstituteInfo getInstituteInfoWithMemberId(MemberInstituteInfoCondition dataCondition) throws Exception {
    	return getSqlSession().selectOne("institute.getInstituteInfoWithMemberId", dataCondition);
    }
}
