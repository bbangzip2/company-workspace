package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.lcms.card.vo.CardVo;
import com.visangesl.treeadmin.lcms.card.vo.ContentStrucCondition;

@Repository
public class CardDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());


    public int getTempLessonStrucCount(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("cardMapTemp.getTempLessonStrucCount", vsObject);
    }

    public int getLessonStrucCount(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("cardMapTemp.getLessonStrucCount", vsObject);
    }

    public int addTempContentStruc(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.addTempContentStruc", vsObject);
        return result;
    }

    public int addTempContentInfo(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.addTempContentInfo", vsObject);
        return result;
    }

    public List<VSObject> getTempDayNoList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("cardMapTemp.getTempDayNoList", vsObject);
        return result;
    }

    public List<VSObject> getTemp1DayNoCardList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("cardMapTemp.getTemp1DayNoCardList", vsObject);
        return result;
    }

    public List<VSObject> getTempSubCardList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("cardMapTemp.getTempSubCardList", vsObject);
        return result;
    }

    public List<VSObject> getTempDayNoCardList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("cardMapTemp.getTempDayNoCardList", vsObject);
        return result;
    }

    public VSObject getTempCardInfo(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("cardMapTemp.getTempCardInfo", vsObject);
        return result;
    }

    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.modifyDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.deleteDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int deleteContentStrucTemp(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.deleteContentStrucTemp", vsObject);
        return result;
    }

    public int deleteContentStruc(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.deleteContentStruc", vsObject);
        return result;
    }

    public int modifyCardSect(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.modifyCardSect", vsObject);
        return result;
    }

    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.addDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int addContentStrucTemp(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.addContentStrucTemp", vsObject);
        return result;
    }

    public int addContentStruc(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.addContentStruc", vsObject);
        return result;
    }

    public int modifyContentInfoTempUseYn(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.modifyContentInfoTempUseYn", vsObject);
        return result;
    }

    public int deleteContentStrucDayCard(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.deleteContentStrucDayCard", vsObject);
        return result;
    }

    public int deleteContentStrucLessonDay(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("cardMapTemp.deleteContentStrucLessonDay", vsObject);
        return result;
    }

    public String getContentInfoNextSeq() throws Exception {
        String result;
        result = getSqlSession().selectOne("cardMapTemp.getContentInfoNextSeq");
        return result;
    }

    public int modifyContentInfoAutoIncrementVal(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.modifyContentInfoAutoIncrementVal", vsObject);
        return result;
    }

    public List<CardVo> getContentInfoTempUnUsedList() throws Exception {
        List<CardVo> result = null;
        result = getSqlSession().selectList("cardMapTemp.getContentInfoTempUnUsedList");
        return result;
    }

    public List<CardVo> getContentInfoTempList(VSObject vsObject) throws Exception {
        List<CardVo> result = null;
        result = getSqlSession().selectList("cardMapTemp.getContentInfoTempList", vsObject);
        return result;
    }

    public int addContentInfo(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.addContentInfo", vsObject);
        return result;
    }

    public int deleteContentInfoTempService(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.deleteContentInfoTempService", vsObject);
        return result;
    }

    public List<ContentStrucCondition> getContentStrucListService(VSObject vsObject) throws Exception {
        List<ContentStrucCondition> result = null;
        result = getSqlSession().selectList("cardMapTemp.getContentStrucListService", vsObject);
        return result;
    }

    public List<ContentStrucCondition> getContentStrucList(VSObject vsObject) throws Exception {
        List<ContentStrucCondition> result = null;
        result = getSqlSession().selectList("cardMapTemp.getContentStrucList", vsObject);
        return result;
    }

    public List<ContentStrucCondition> getContentStrucTempList(VSObject vsObject) throws Exception {
        List<ContentStrucCondition> result = null;
        result = getSqlSession().selectList("cardMapTemp.getContentStrucTempList", vsObject);
        return result;
    }

    public int deleteContentStrucTempStrucSeq(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.deleteContentStrucTempStrucSeq", vsObject);
        return result;
    }

    public int deleteContentStrucStrucSeq(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.deleteContentStrucStrucSeq", vsObject);
        return result;
    }

    public int deleteContentTempData(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.deleteContentTempData", vsObject);
        return result;
    }

    public int deleteContentStrucService(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("cardMapTemp.deleteContentStrucService", vsObject);
        return result;
    }



/*


    public List<VSObject> getTempCardList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("cardMapTemp.getTempCardList", vsObject);
        return result;
    }








    public VSObject getData(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("card.getData", vsObject);
        return result;
    }

    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("card.getDataList", vsObject);
        return result;
    }

    public int getDataListCnt(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("card.getDataListCnt", vsObject);
    }







    public List<VSObject> getDayNoList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("card.getDayNoList", vsObject);
        return result;
    }

    public int addEditCard(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("card.addDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int modifyEditCard(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("card.modifyEditCard", vsObject);
        return result;
    }

    public List<VSObject> getSubCardList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("card.getSubCardList", vsObject);
        return result;
    }



    public List<VSObject> getCardList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("card.getCardList", vsObject);
        return result;
    }

    public VSObject getCardInfoList(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("card.getCardInfoList", vsObject);
        return result;
    }

*/
}
