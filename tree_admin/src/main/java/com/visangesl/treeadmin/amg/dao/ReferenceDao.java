package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceCondition;
import com.visangesl.treeadmin.lcms.reference.vo.ReferenceVo;

@Repository
public class ReferenceDao extends TreeSqlSessionDaoSupport {

    private final Log logger = LogFactory.getLog(this.getClass());

    public VSObject getData(VSObject vsObject) throws Exception {
        VSObject result = null;
        result = getSqlSession().selectOne("reference.getData", vsObject);
        return result;
    }

    public List<VSObject> getDataList(VSObject vsObject) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("reference.getDataList", vsObject);
        return result;
    }

    public int getDataListCnt(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("reference.getDataListCnt", vsObject);
    }

    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().update("reference.modifyDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("reference.addDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().delete("reference.deleteDataWithResultCodeMsg", vsObject);
        return result;
    }

    public int addReferenceFileInfo(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("reference.addReferenceFileInfo", vsObject);
        return result;
    }

    public int deleteReferenceFileInfo(VSObject vsObject) throws Exception {
        int result = 0;
        result = getSqlSession().insert("reference.deleteReferenceFileInfo", vsObject);
        return result;
    }

    public int getReferenceSeqCount(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("reference.getReferenceSeqCount", vsObject);
    }

    public List<VSObject> getDayNoCdList(String lessonCd) throws Exception {
        List<VSObject> result = null;
        result = getSqlSession().selectList("reference.getDayNoCdList", lessonCd);
        return result;
    }

    public int getReferenceSameFileCount(VSObject vsObject) throws Exception {
        return getSqlSession().selectOne("reference.getReferenceSameFileCount", vsObject);
    }

    public List<ReferenceVo> getReferenceTempList(ReferenceCondition referenceCondition) throws Exception {
        List<ReferenceVo> result = null;
        result = getSqlSession().selectList("reference.getReferenceTempList", referenceCondition);
        return result;
    }

    public int updateReferenceFileUseYn(ReferenceCondition referenceCondition) throws Exception {
        return getSqlSession().selectOne("reference.updateReferenceFileUseYn", referenceCondition);
    }

}
