package com.visangesl.treeadmin.amg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class AdminDao extends TreeSqlSessionDaoSupport {

	private final Log logger = LogFactory.getLog(this.getClass());

    public int addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().insert("adminMember.addDataWithResultCodeMsg", vsObject);
		return result;
	}

	public int addDataIcmRel(VSObject vsObject) {
		int result = 0;
		result = getSqlSession().insert("adminMember.addDataIcmRel", vsObject);
		return result;
	}

	public List<VSObject> getDataList(VSObject dataCondition) throws Exception {
	    List<VSObject> result = null;
 		result = getSqlSession().selectList("adminMember.getDataList", dataCondition);
    	return result;
	}

	public VSObject getData(VSObject dataCondition) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("adminMember.getData", dataCondition);
    	return result;
	}

    public int modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
		int result = 0;
			result = getSqlSession().update("adminMember.modifyDataWithResultCodeMsg", vsObject);
		return result;
	}

	public VSObject getDataCampInfo(VSObject dataCondition) throws Exception {
    	VSObject result = null;
        result = getSqlSession().selectOne("adminMember.getDataCampInfo", dataCondition);
    	return result;
	}
}
