package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class MemberInstituteInfo extends VSObject {
	private int instituteSeq;
	private String logoPath;
	private String name;
	
	public int getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(int instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getLogoPath() {
		return logoPath;
	}
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
