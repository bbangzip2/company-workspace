package com.visangesl.treeadmin.amg.vo;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class AdminMember extends VSObject {
		private String mbrId;
	    private String pwd;
	    private String nm;
	    private String email;
	    private String telNo;
	    private String useYn;
	    private String regDttm;
	    private String modDttm;
	    private String memo;
	    private String mbrGrade;
	    private String mbrStatus;
	    private String mbrGubun;
	    private String userType;
	    private String sector;
	    private String icmSeq;
	    private String icmSect;
	    private String relSect;
	    private String relSeq;
	    private String regId;
	    private String cSeq;
	    private String profilePhotoPath;
    
	    private MultipartFile uploadFile;
  		
		public String getProfilePhotoPath() {
			return profilePhotoPath;
		}
		public void setProfilePhotoPath(String profilePhotoPath) {
			this.profilePhotoPath = profilePhotoPath;
		}
		public MultipartFile getUploadFile() {
			return uploadFile;
		}
		public void setUploadFile(MultipartFile uploadFile) {
			this.uploadFile = uploadFile;
		}
		
		
		public String getcSeq() {
			return cSeq;
		}
		public void setcSeq(String cSeq) {
			this.cSeq = cSeq;
		}
		public String getRelSeq() {
			return relSeq;
		}
		public void setRelSeq(String relSeq) {
			this.relSeq = relSeq;
		}
		public String getRegId() {
			return regId;
		}
		public void setRegId(String regId) {
			this.regId = regId;
		}
		public String getMbrId() {
			return mbrId;
		}
		public void setMbrId(String mbrId) {
			this.mbrId = mbrId;
		}
		public String getPwd() {
			return pwd;
		}
		public void setPwd(String pwd) {
			this.pwd = pwd;
		}
		public String getNm() {
			return nm;
		}
		public void setNm(String nm) {
			this.nm = nm;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getTelNo() {
			return telNo;
		}
		public void setTelNo(String telNo) {
			this.telNo = telNo;
		}
		public String getUseYn() {
			return useYn;
		}
		public void setUseYn(String useYn) {
			this.useYn = useYn;
		}
		public String getRegDttm() {
			return regDttm;
		}
		public void setRegDttm(String regDttm) {
			this.regDttm = regDttm;
		}
		public String getModDttm() {
			return modDttm;
		}
		public void setModDttm(String modDttm) {
			this.modDttm = modDttm;
		}
		public String getMemo() {
			return memo;
		}
		public void setMemo(String memo) {
			this.memo = memo;
		}
		public String getMbrGrade() {
			return mbrGrade;
		}
		public void setMbrGrade(String mbrGrade) {
			this.mbrGrade = mbrGrade;
		}
		public String getMbrStatus() {
			return mbrStatus;
		}
		public void setMbrStatus(String mbrStatus) {
			this.mbrStatus = mbrStatus;
		}
		public String getMbrGubun() {
			return mbrGubun;
		}
		public void setMbrGubun(String mbrGubun) {
			this.mbrGubun = mbrGubun;
		}
		public String getUserType() {
			return userType;
		}
		public void setUserType(String userType) {
			this.userType = userType;
		}
		public String getSector() {
			return sector;
		}
		public void setSector(String sector) {
			this.sector = sector;
		}
		public String getIcmSeq() {
			return icmSeq;
		}
		public void setIcmSeq(String icmSeq) {
			this.icmSeq = icmSeq;
		}
		public String getIcmSect() {
			return icmSect;
		}
		public void setIcmSect(String icmSect) {
			this.icmSect = icmSect;
		}
		public String getRelSect() {
			return relSect;
		}
		public void setRelSect(String relSect) {
			this.relSect = relSect;
		}
		

	  




}
