package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSObject;

public class ProgItem extends VSObject {

	private String progItemSeq;
	private String progSeq;
	private String instituteSeq;
	private String prodSeq;
	private String prodTitle;
	private String itemSeq;
	private String itemType;
	
	public String getProgItemSeq() {
		return progItemSeq;
	}
	public void setProgItemSeq(String progItemSeq) {
		this.progItemSeq = progItemSeq;
	}
	public String getProgSeq() {
		return progSeq;
	}
	public void setProgSeq(String progSeq) {
		this.progSeq = progSeq;
	}
	public String getInstituteSeq() {
		return instituteSeq;
	}
	public void setInstituteSeq(String instituteSeq) {
		this.instituteSeq = instituteSeq;
	}
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getItemSeq() {
		return itemSeq;
	}
	public void setItemSeq(String itemSeq) {
		this.itemSeq = itemSeq;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
}
