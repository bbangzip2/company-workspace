package com.visangesl.treeadmin.amg.vo;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;

public class MemberExcelCondition extends VSObject {

    private String nationCd;
    private MultipartFile multipartFile;
    
    public String getNationCd() {
        return nationCd;
    }
    public void setNationCd(String nationCd) {
        this.nationCd = nationCd;
    }
    public MultipartFile getMultipartFile() {
        return multipartFile;
    }
    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
