package com.visangesl.treeadmin.amg.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

//@XmlRootElement(name="Campus")
public class Campus extends VSObject {
	private String rnum;
	private String campSeq;
	private String campNm;
	private String areaNm;
	private String detailInfo;
	private String useYn;
	private String regDate;
	private String regId;
	private String modDate;
	private String modId;
	private String mbrId;
	private String campId;
	private List<ClassInfo> classList;
	
	private String classCnt; 
	private String studentCnt; // 학생 수 ( 수강회원, 정회원, 정지회원, 탈퇴회원 )
	private String attendeeCnt; // 수강회원 수 
	
	private String totCnt;
	
	private String insInfoSeq; // 기관 순번  
	private String insInfoId; // 기관 ID  
	
	
	public String getRnum() {
		return rnum;
	}
	public void setRnum(String rnum) {
		this.rnum = rnum;
	}
	public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getCampNm() {
		return campNm;
	}
	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}
	public String getAreaNm() {
		return areaNm;
	}
	public void setAreaNm(String areaNm) {
		this.areaNm = areaNm;
	}
	public String getDetailInfo() {
		return detailInfo;
	}
	public void setDetailInfo(String detailInfo) {
		this.detailInfo = detailInfo;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getCampId() {
		return campId;
	}
	public void setCampId(String campId) {
		this.campId = campId;
	}
	public List<ClassInfo> getClassList() {
		return classList;
	}
	public void setClassList(List<ClassInfo> classList) {
		this.classList = classList;
	}
	public String getClassCnt() {
		return classCnt;
	}
	public void setClassCnt(String classCnt) {
		this.classCnt = classCnt;
	}
	public String getStudentCnt() {
		return studentCnt;
	}
	public void setStudentCnt(String studentCnt) {
		this.studentCnt = studentCnt;
	}
	public String getAttendeeCnt() {
		return attendeeCnt;
	}
	public void setAttendeeCnt(String attendeeCnt) {
		this.attendeeCnt = attendeeCnt;
	}
	public String getTotCnt() {
		return totCnt;
	}
	public void setTotCnt(String totCnt) {
		this.totCnt = totCnt;
	}
	public String getInsInfoSeq() {
		return insInfoSeq;
	}
	public void setInsInfoSeq(String insInfoSeq) {
		this.insInfoSeq = insInfoSeq;
	}
	public String getInsInfoId() {
		return insInfoId;
	}
	public void setInsInfoId(String insInfoId) {
		this.insInfoId = insInfoId;
	}
	
}
