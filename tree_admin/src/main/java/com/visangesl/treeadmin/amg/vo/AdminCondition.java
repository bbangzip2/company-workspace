package com.visangesl.treeadmin.amg.vo;

import com.visangesl.treeadmin.vo.VSListCondition;

public class AdminCondition extends VSListCondition {
	private String rnum;
	private String totCnt;
	private String searchNm;
	private String searchUserType;
	private String searchSect;
	private String searchYn;
	private String regDttm;
	private String stauts;
    private String searchType;
    private String searchValue;
    private String orderBy = null;
    private String sort = null;
	private String orderColumn;
   
	private String mbrId;
    private String nm;
    private String telNo;
    private String useYn;
    private String mbrGrade;
    private String mbrStatus;
    private String userType;
    private String sector;
    private String isect;
    private String insInfoSeq;
    private String iname;
    
    
    
       
    
	public String getIname() {
		return iname;
	}

	public void setIname(String iname) {
		this.iname = iname;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSearchSect() {
		return searchSect;
	}

	public void setSearchSect(String searchSect) {
		this.searchSect = searchSect;
	}

	public String getSearchUserType() {
		return searchUserType;
	}

	public void setSearchUserType(String searchUserType) {
		this.searchUserType = searchUserType;
	}

	public String getSearchYn() {
		return searchYn;
	}

	public void setSearchYn(String searchYn) {
		this.searchYn = searchYn;
	}

	public String getIsect() {
		return isect;
	}

	public void setIsect(String isect) {
		this.isect = isect;
	}

	public String getInsInfoSeq() {
		return insInfoSeq;
	}

	public void setInsInfoSeq(String insInfoSeq) {
		this.insInfoSeq = insInfoSeq;
	}

	public String getTotCnt() {
		return totCnt;
	}

	public void setTotCnt(String totCnt) {
		this.totCnt = totCnt;
	}

	public String getRnum() {
		return rnum;
	}

	public void setRnum(String rnum) {
		this.rnum = rnum;
	}

	public String getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}

	public String getStauts() {
		return stauts;
	}

	public void setStauts(String stauts) {
		this.stauts = stauts;
	}

	public String getSearchNm() {
		return searchNm;
	}

	public void setSearchNm(String searchNm) {
		this.searchNm = searchNm;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public String getNm() {
		return nm;
	}

	public void setNm(String nm) {
		this.nm = nm;
	}

	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getMbrGrade() {
		return mbrGrade;
	}

	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}

	public String getMbrStatus() {
		return mbrStatus;
	}

	public void setMbrStatus(String mbrStatus) {
		this.mbrStatus = mbrStatus;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@Override
	public String toString() {
		return "AdminCondition [rnum=" + rnum + ", totCnt=" + totCnt
				+ ", searchNm=" + searchNm + ", searchUserType="
				+ searchUserType + ", searchSect=" + searchSect + ", searchYn="
				+ searchYn + ", regDttm=" + regDttm + ", stauts=" + stauts
				+ ", searchType=" + searchType + ", searchValue=" + searchValue
				+ ", orderBy=" + orderBy + ", sort=" + sort + ", orderColumn="
				+ orderColumn + ", mbrId=" + mbrId + ", nm=" + nm + ", telNo="
				+ telNo + ", useYn=" + useYn + ", mbrGrade=" + mbrGrade
				+ ", mbrStatus=" + mbrStatus + ", userType=" + userType
				+ ", sector=" + sector + ", isect=" + isect + ", insInfoSeq="
				+ insInfoSeq + "]";
	}
}
