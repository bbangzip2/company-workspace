package com.visangesl.treeadmin.amg.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.InstituteService;
import com.visangesl.treeadmin.amg.service.StudentService;
import com.visangesl.treeadmin.amg.vo.ClassInfo;
import com.visangesl.treeadmin.amg.vo.Institute;
import com.visangesl.treeadmin.amg.vo.InstituteCondition;
import com.visangesl.treeadmin.amg.vo.MemberExcelCondition;
import com.visangesl.treeadmin.amg.vo.Student;
import com.visangesl.treeadmin.amg.vo.StudentCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.TreeRuntimeException;
import com.visangesl.treeadmin.member.service.MemberService;
import com.visangesl.treeadmin.member.vo.Member;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.CodeVo;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class StudentController implements BaseController {

	@Autowired
	private MemberService memberService;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private InstituteService instituteService;
	
	@Autowired
	private CommonService commonService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 학생 리스트 페이지.
	 * @param request
	 * @param response
	 * @param model
	 * @param pageSize     한 페이지에 보여지는 리스트 수
	 * @param pageNo       페이지 번호
	 * @param campSeq      검색:캠퍼스 순번
	 * @param progSeq      검색:프로그램 순번
	 * @param clsSeq       검색:클래스 순번
	 * @param searchColumn 검색:검색하기 위한 컬럼명
	 * @param searchKey    검색:키값
	 * @param regStartDate regDate 시작 값
	 * @param regEndDate   regDate 종료 값
	 * @param useYn        상태값
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/studentList", method=RequestMethod.GET)
	public String studentList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "pageSize", required = false) String pageSize,
			@RequestParam(value = "pageNo", required = false) String pageNo,
			@RequestParam(value = "campSeq", required = false) String campSeq,
			@RequestParam(value = "progSeq", required = false) String progSeq,
			@RequestParam(value = "clsSeq", required = false) String clsSeq,
			@RequestParam(value = "searchColumn", required = false) String searchColumn,
			@RequestParam(value = "searchKey", required = false) String searchKey,
			@RequestParam(value = "regStartDate", required = false) String regStartDate,
			@RequestParam(value = "regEndDate", required = false) String regEndDate,
			@RequestParam(value = "useYn", required = false) String useYn) throws Exception {
		logger.debug("studentList");
		
		// 콤보 리스트를 보여지기 위한 리스트 객체 생성
		VSResult<List<VSObject>> campCombo = new VSResult<List<VSObject>>();
		VSResult<List<VSObject>> progCombo = new VSResult<List<VSObject>>();
		VSResult<List<VSObject>> clssCombo = new VSResult<List<VSObject>>();
		
		// 세션에서 기관순번을 가져온다.
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        StudentCondition studentCondition = new StudentCondition();
        studentCondition.setInsSeq(insSeq);
        
        // 캠퍼스 어드민일 경우 유저아이디로 캠퍼스 리스트를 검색 하기 위한 작업.
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
            studentCondition.setMbrId(userDetail.getUsername());
        }
        // 캠퍼스 어드민이면 캠퍼스 목록이 하나만 넘어온다. 그렇지 않으면 여러개의 목록이 넘어온다.
        List<VSObject> result = studentService.getCampusList(studentCondition);
        campCombo.setResult(result);
        // 캠퍼스 목록이 하나만 있으면 campSeq에 할당하고 그에 맞게 리스트를 가져온다.
        if (TreeAdminUtil.isNull(studentCondition.getMbrId()) != "" && result != null && result.size() == 1) {
            campSeq = ((CodeVo)campCombo.getResult().get(0)).getCd();
        }
        
		// 캠퍼스 필터링.
		if(TreeAdminUtil.isNull(campSeq) != "") {
			studentCondition.setCampSeq(campSeq);
			// 캠퍼스가 필터링 되면 캠퍼스에 해당하는 프로그램 리스트를 가져온다.
			progCombo.setResult(studentService.getProgramList(studentCondition));
		}
		
		// 프로그램 필터링.
		if(TreeAdminUtil.isNull(progSeq) != "") {
			studentCondition.setProgSeq(progSeq);
			clssCombo.setResult(studentService.getClassList(studentCondition));
		}
		
		// 프로그램 필터링.
		if(TreeAdminUtil.isNull(clsSeq) != "") {
			studentCondition.setClsSeq(clsSeq);
		}
		
		// 타입별 검색어로 검색.
		if(TreeAdminUtil.isNull(searchColumn) != "" && TreeAdminUtil.isNull(searchKey) != "") {
			studentCondition.setSearchColumn(searchColumn);
			studentCondition.setSearchKey(searchKey);
		}
		
		// 기간 검색 값.
		if(TreeAdminUtil.isNull(regStartDate) != "" && TreeAdminUtil.isNull(regEndDate) != "") {
			studentCondition.setRegStartDate(regStartDate);
			studentCondition.setRegEndDate(regEndDate);
		}
		
		if(TreeAdminUtil.isNull(useYn) != "") {
			studentCondition.setUseYn(useYn);
		}
		
		// 조건에 맞는 데이터의 총 갯수 가져오기.
		int totalCount = studentService.getDataListCnt(studentCondition);
		studentCondition.setTotalCount(totalCount);
		// 현재 페이지 번호와 한페이지에 표시할 갯수 설정.
		studentCondition.setPageSize(TreeAdminUtil.isNumber(pageSize, 20));
		studentCondition.setPageNo(TreeAdminUtil.isNumber(pageNo, 1));
		
		// 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
		int pageStartIndex = (studentCondition.getPageNo() - 1) * studentCondition.getPageSize();
		studentCondition.setPageStartIndex(pageStartIndex);
		
		List<VSObject> studentList = studentService.getDataList(studentCondition);
		logger.debug("totalCount : " + totalCount);
		logger.debug("studentList.size() : " + studentList.size());
		
		// 리스트에 역순 번호 매기기.
		int i = 0;
		while(i < studentList.size()) {
			Student item = (Student) studentList.get(i);
			item.setRn(String.valueOf(totalCount - (pageStartIndex + i)));
			i++;
		}
		
		// 콤보 리스트들을 JSON STRING 으로 변환.
        String campList = TreeAdminUtil.createJsonString(campCombo);
        String progList = TreeAdminUtil.createJsonString(progCombo);
        String clssList = TreeAdminUtil.createJsonString(clssCombo);
		
        model.addAttribute("campList", campList);
        model.addAttribute("progList", progList);
        model.addAttribute("clssList", clssList);
        
		model.addAttribute("condition", studentCondition);
		model.addAttribute("studentList", studentList);
		
		return "/amg/studentList";
	}
	
	/**
	 * 학생 상세 페이지로 이동.
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId    유저 아이디
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/studentPop", method = RequestMethod.GET)
	public String studentPop(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("studentView");
		
		// 세션에서 기관순번을 가져온다.
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
		StudentCondition studentCondition = new StudentCondition();
		studentCondition.setMbrId(mbrId);
		studentCondition.setInsSeq(insSeq);
		
		Student student = (Student)studentService.getData(studentCondition);
		
		logger.debug("student profilePath : " + student.getProfilePhotopath());
		
		// 기관 순번을 이름을 가져오기 위해 기관 데이터를 가져온다.
		InstituteCondition insCondition = new InstituteCondition();
        insCondition.setSeq(insSeq);
        Institute institute = (Institute) instituteService.getData(insCondition);
        
        student.setInsSeq(insSeq);
        student.setInsNm(institute.getName());
		
		model.addAttribute("student", student);
		
		return "/amg/studentPop";
	}
	
	/**
	 * 학생 상세 페이지로 이동.
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId    유저 아이디
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/studentView", method = RequestMethod.GET)
	public String studentView(HttpServletRequest request, HttpServletResponse response, Model model,
	        @RequestParam(value = "mbrId", required = false) String mbrId,
	        @RequestParam(value = "pageSize", required = false) String pageSize,
            @RequestParam(value = "pageNo", required = false) String pageNo,
            @RequestParam(value = "campSeq", required = false) String campSeq,
            @RequestParam(value = "progSeq", required = false) String progSeq,
            @RequestParam(value = "clsSeq", required = false) String clsSeq,
            @RequestParam(value = "searchColumn", required = false) String searchColumn,
            @RequestParam(value = "searchKey", required = false) String searchKey,
            @RequestParam(value = "regStartDate", required = false) String regStartDate,
            @RequestParam(value = "regEndDate", required = false) String regEndDate,
            @RequestParam(value = "useYn", required = false) String useYn) throws Exception {
	    logger.debug("studentView");
	    
	    // 세션에서 기관순번을 가져온다.
	    TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
	    String insSeq = userDetail.getInsSeq();
	    
	    StudentCondition studentCondition = new StudentCondition();
	    studentCondition.setInsSeq(insSeq);
	    studentCondition.setMbrId(mbrId);
	    
	    Student student = (Student)studentService.getData(studentCondition);
	    // 에디트에서 리스트로 갈 때 리스트 목록 값을 전달하기 위해
	    studentCondition.setPageSize(TreeAdminUtil.isNumber(pageSize, 20));
	    studentCondition.setPageNo(TreeAdminUtil.isNumber(pageNo, 1));
	    studentCondition.setCampSeq(campSeq);
	    studentCondition.setProgSeq(progSeq);
	    studentCondition.setClsSeq(clsSeq);
	    studentCondition.setSearchColumn(searchColumn);
	    studentCondition.setSearchKey(searchKey);
	    studentCondition.setRegStartDate(regStartDate);
	    studentCondition.setRegEndDate(regEndDate);
	    studentCondition.setUseYn(useYn);
	    
	    // 기관 순번을 이름을 가져오기 위해 기관 데이터를 가져온다.
	    InstituteCondition insCondition = new InstituteCondition();
	    insCondition.setSeq(insSeq);
	    Institute institute = (Institute) instituteService.getData(insCondition);
	    
	    student.setInsSeq(insSeq);
	    student.setInsNm(institute.getName());
	    
	    model.addAttribute("student", student);
	    model.addAttribute("condition", studentCondition);
	    
	    return "/amg/studentView";
	}
    
    /**
     * 선생님 편집 페이지로 이동.
     * @param request
     * @param response
     * @param model
     * @param mbrId     선생님의 유저 아이디
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/studentEdit")
    public String teacherEdit(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "mbrId", required = false) String mbrId,
            @RequestParam(value = "pageSize", required = false) String pageSize,
            @RequestParam(value = "pageNo", required = false) String pageNo,
            @RequestParam(value = "campSeq", required = false) String campSeq,
            @RequestParam(value = "progSeq", required = false) String progSeq,
            @RequestParam(value = "clsSeq", required = false) String clsSeq,
            @RequestParam(value = "searchColumn", required = false) String searchColumn,
            @RequestParam(value = "searchKey", required = false) String searchKey,
            @RequestParam(value = "regStartDate", required = false) String regStartDate,
            @RequestParam(value = "regEndDate", required = false) String regEndDate,
            @RequestParam(value = "useYn", required = false) String useYn) throws Exception {
        logger.debug("studentEdit");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        
        // 기관순번 값에 맞는 콤보 리스트를 가져온다.
        StudentCondition studentCondition = new StudentCondition();
        studentCondition.setInsSeq(insSeq);
        
        // 캠퍼스 어드민일 경우 유저아이디로 캠퍼스 리스트를 검색 하기 위한 작업.
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
            studentCondition.setMbrId(userDetail.getUsername());
        }
        List<VSObject> campusList = studentService.getCampusList(studentCondition);
        
        studentCondition.setMbrId(mbrId);
        // 학생의 데이터를 수정하기 위해 해당 데이터를 가져온다.
        Student student = (Student)studentService.getData(studentCondition);
        
        // 해당 캠퍼스가 클래스와 묶여 있는지 확인 한다.
        String disabledCampusCombo = "N";
        List<ClassInfo> clsList = student.getClassInfo();
        for (int i = 0; i < clsList.size(); i++) {
            if (TreeAdminUtil.isNull(clsList.get(i).getClassSeq()) != "") {
                disabledCampusCombo = "Y";
                break;
            }
        }

        // 기관의 이름을 가져오기 위해 기관 데이터를 가져온다.
        InstituteCondition insCondition = new InstituteCondition();
        insCondition.setSeq(insSeq);
        Institute institute = (Institute) instituteService.getData(insCondition);
        
        studentCondition.setPageSize(TreeAdminUtil.isNumber(pageSize, 20));
        studentCondition.setPageNo(TreeAdminUtil.isNumber(pageNo, 1));
        studentCondition.setCampSeq(campSeq);
        studentCondition.setProgSeq(progSeq);
        studentCondition.setClsSeq(clsSeq);
        studentCondition.setSearchColumn(searchColumn);
        studentCondition.setSearchKey(searchKey);
        studentCondition.setRegStartDate(regStartDate);
        studentCondition.setRegEndDate(regEndDate);
        studentCondition.setUseYn(useYn);
        
        model.addAttribute("institute", institute);
        model.addAttribute("student", student);
        model.addAttribute("condition", studentCondition);
        model.addAttribute("campusList", campusList);
        model.addAttribute("disabledCampusCombo", disabledCampusCombo);
        
        return "/amg/studentEdit";
    }
	
	/**
	 * 학생 등록 페이지로 이동.
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId    유저 아이디
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/studentInsert")
	public String studentInsert(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId) throws Exception {
		logger.debug("studentInsert");

		// 세션에서 기관 순번을 가져온다.
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insSeq = userDetail.getInsSeq();
		
		InstituteCondition instituteCondition = new InstituteCondition();
		instituteCondition.setSeq(insSeq);
		Institute institute = (Institute) instituteService.getData(instituteCondition);
		
		// 기관순번 값에 맞는 콤보 리스트를 가져온다.
        StudentCondition studentCondition = new StudentCondition();
        studentCondition.setInsSeq(insSeq);
        
        // 캠퍼스 어드민일 경우 유저아이디로 캠퍼스 리스트를 검색 하기 위한 작업.
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
            studentCondition.setMbrId(userDetail.getUsername());
        }
        List<VSObject> campusList = studentService.getCampusList(studentCondition);
		
		model.addAttribute("institute", institute);
		model.addAttribute("campusList", campusList);
		
		return "/amg/studentEdit";
	}
	
	/**
	 * 학생 등록
	 * @param request
	 * @param response
	 * @param model
	 * @param mbrId        유저 아이디
	 * @param pwd          패스워드
	 * @param name         이름
	 * @param nickName     별명
	 * @param email        이메일
	 * @param sexSect      성별
	 * @param telNo        전화번호
	 * @param useYn        상태값
	 * @param bymd         생일
	 * @param insInfoSeq   기관 순번
	 * @param campSeq      캠퍼스 순번
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/studentAdd", method = RequestMethod.POST)
	public ModelAndView studentAdd(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "mbrId", required = false) String mbrId,
			@RequestParam(value = "pwd", required = false) String pwd,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "nickName", required = false) String nickName,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "sexSect", required = false) String sexSect,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "useYn", required = false) String useYn,
			@RequestParam(value = "bymd", required = false) String bymd,
			@RequestParam(value = "addCampSeq", required = false) String addCampSeq) throws Exception {
		logger.debug("studentAdd");
		
		// 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
		
		InstituteCondition insCondition = new InstituteCondition();
        insCondition.setSeq(insSeq);
        Institute institute = (Institute) instituteService.getData(insCondition);
		
		// 학생의 기본 유저 데이터 저장.
		Member member = new Member();
		member.setMbrId(mbrId);
		member.setPwd(pwd);
		member.setName(name);
		member.setNickName(nickName);
		member.setEmail(email);
		member.setSexSect(sexSect);
		member.setTelNo(telNo);
		member.setUseYn(useYn);
		member.setBirthDay(bymd);
		member.setNation(institute.getNationCd());
		member.setSect("MS002");
		member.setMbrGrade("MG220");
		
		String regId = "";
        if (userDetail != null) {
            regId = userDetail.getUsername();
        }
        member.setRegId(regId);
        member.setModId(regId);
		
        // 이미지 파일 처리
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        if (multipartFile != null) {
            member.setUploadFile(multipartFile);
        }
        
        VSResult<VSObject> resultCodeMsg = memberService.addDataWithResultCodeMsg(member);
        
        if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
            if(TreeAdminUtil.isNull(addCampSeq) != "") {
                // 캠퍼스 추가한다.
                IcmRelVo icmRel = new IcmRelVo();
                icmRel.setIcmSeq(mbrId);
                icmRel.setRelSeq(addCampSeq);
                icmRel.setIcmSect("RE004");
                icmRel.setRelSect("RE002");
                commonService.addIcmRelData(icmRel);
            }
            
            ModelAndView mav = new ModelAndView();
            mav.setViewName("redirect:/amg/studentList.do");
            return mav;
        } else {
            //Fail
            StringBuffer sb = new StringBuffer();
            
            sb.append("<script type=\"text/javascript\">");
            sb.append("alert('처리작업이 실패하였습니다.\n다시 시도해주시기 바랍니다.');");
            sb.append("history.back();");
            sb.append("</script>");
            
            response.setBufferSize(8 * 1024); // 8K buffer
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html; charset=utf-8");
            PrintWriter out = response.getWriter();

//            int size = response.getBufferSize(); // returns 8096 or greater
            out.println(sb.toString());
            out.flush();
            
            return null;
        }
	}
	
	
	@RequestMapping(value = "/amg/studentModify", method = RequestMethod.POST)
	public ModelAndView studentModify(HttpServletRequest request, HttpServletResponse response, Model model,
	        @RequestParam(value = "mbrId", required = false) String mbrId,
	        @RequestParam(value = "pwd", required = false) String pwd,
	        @RequestParam(value = "name", required = false) String name,
	        @RequestParam(value = "nickName", required = false) String nickName,
	        @RequestParam(value = "email", required = false) String email,
	        @RequestParam(value = "sexSect", required = false) String sexSect,
	        @RequestParam(value = "telNo", required = false) String telNo,
	        @RequestParam(value = "useYn", required = false) String useYn,
	        @RequestParam(value = "bymd", required = false) String bymd,
	        @RequestParam(value = "addCampSeq", required = false) String addCampSeq,
	        @RequestParam(value = "delCampSeq", required = false) String delCampSeq) throws Exception {
	    logger.debug("studentModify");
	    
	    // 학생의 기본 유저 데이터 저장.
	    Member member = new Member();
	    member.setMbrId(mbrId);
	    member.setPwd(pwd);
	    member.setName(name);
	    member.setNickName(nickName);
	    member.setEmail(email);
	    member.setSexSect(sexSect);
	    member.setTelNo(telNo);
	    member.setUseYn(useYn);
	    member.setBirthDay(bymd);
	    
	    String modId = "";
	    TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
	    if (userDetail != null) {
	        modId = userDetail.getUsername();
	    }
	    member.setModId(modId);
	    
	    // 이미지 파일 처리
	    MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
	    if (multipartFile != null) {
	        member.setUploadFile(multipartFile);
	    }
	    
	    VSResult<VSObject> resultCodeMsg = memberService.modifyDataWithResultCodeMsg(member);
	    
	    if (resultCodeMsg != null && resultCodeMsg.getCode().equals(TreeProperties.getProperty("error.success.code"))) {
	        
	        if(TreeAdminUtil.isNull(delCampSeq) != "") {
	            // 기존 학생의 캠퍼스 데이터 삭제.
	            IcmRelVo oldCamp = new IcmRelVo();
	            oldCamp.setIcmSeq(mbrId);
	            oldCamp.setIcmSect("RE004");
	            oldCamp.setRelSect("RE002");
	            oldCamp.setRelSeq(delCampSeq);
	            commonService.deleteIcmRelData(oldCamp);
	        }
	        
	        if(TreeAdminUtil.isNull(addCampSeq) != "") {
	            // 캠퍼스 추가한다.
	            IcmRelVo icmRel = new IcmRelVo();
	            icmRel.setIcmSeq(mbrId);
	            icmRel.setRelSeq(addCampSeq);
	            icmRel.setIcmSect("RE004");
	            icmRel.setRelSect("RE002");
	            commonService.addIcmRelData(icmRel);
	        }
	        
	        ModelAndView mav = new ModelAndView();
	        mav.setViewName("redirect:/amg/studentList.do");
	        return mav;
	    } else {
	        //Fail
	        StringBuffer sb = new StringBuffer();
	        
	        sb.append("<script type=\"text/javascript\">");
	        sb.append("alert('처리작업이 실패하였습니다.\n다시 시도해주시기 바랍니다.');");
	        sb.append("history.back();");
	        sb.append("</script>");
	        
	        response.setBufferSize(8 * 1024); // 8K buffer
	        response.setCharacterEncoding("utf-8");
	        response.setContentType("text/html; charset=utf-8");
	        PrintWriter out = response.getWriter();
	        
//	        int size = response.getBufferSize(); // returns 8096 or greater
	        out.println(sb.toString());
	        out.flush();
	        
	        return null;
	    }
	}
	
    @RequestMapping(value = "/amg/studentExcelPop")
    public String studentExcelPop(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("studentExcelPop");
        
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        StudentCondition condition = new StudentCondition();
        condition.setInsSeq(insSeq);
        
        List<VSObject> campList = studentService.getCampusList(condition);
        model.addAttribute("campList", campList);
        
        return "/amg/studentExcelPop";
    }
    
    @RequestMapping(value = "/amg/studentExcelUpload", method = RequestMethod.POST)
    public ModelAndView studentExcelUpload(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("studentExcelUpload ======================================================");
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        // nation 값으 가져오기 위해 기관순번으로 기관을 가져온다.
        InstituteCondition insCondition = new InstituteCondition();
        insCondition.setSeq(insSeq);
        Institute institute = (Institute) instituteService.getData(insCondition);
        
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("uploadFile");
        
        // DB에 넣을 국가코드와 엑셀파일을 넘긴다.
        MemberExcelCondition excelCondition = new MemberExcelCondition();
        excelCondition.setNationCd(institute.getNationCd());
        excelCondition.setMultipartFile(multipartFile);
        
        // 리턴 메세지 
        VSResult<?> result = null;
        
        try{
            result = studentService.inputExcelData(excelCondition);
        } catch (TreeRuntimeException e) {
            result = new VSResult<>();
            result.setCode(e.getCode());
            result.setMessage(e.getMsg());
        }
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/amg/commonExcelResult");
        mav.addObject("subject", "Students");
        mav.addObject("result", result);
        return mav;
    }
	
	/**
	 * 캠퍼스 콤보 리스트 목록 API
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/amg/studentCampusList")
	@ResponseBody
    public VSResult<?> getCampusList(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.debug("studentCampusList");
        
        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        // 세션에서 기관순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        StudentCondition studentCondition = new StudentCondition();
        studentCondition.setInsSeq(insSeq);
        
        try {
            // 캠퍼스 어드민일 경우 유저아이디로 캠퍼스 리스트를 검색 하기 위한 작업.
            String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
            if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_campusadmin"))) {
                studentCondition.setMbrId(userDetail.getUsername());
            }
            List<VSObject> campusList = studentService.getCampusList(studentCondition);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(campusList);
        } catch (Exception e) {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            e.printStackTrace();
        }
        return result;
	}
	
	/**
	 * 프로그램 콤보 리스트 API
	 * @param request
	 * @param response
	 * @param model
	 * @param campSeq
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/amg/studentProgramList")
    @ResponseBody
    public VSResult<?> getProgramList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "campSeq", required = false) String campSeq) throws Exception {
        logger.debug("studentProgramList");

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        // 세션에서 기관 순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();

        StudentCondition studentCondition = new StudentCondition();
        studentCondition.setInsSeq(insSeq);
        studentCondition.setCampSeq(campSeq);

        try {
            // 기관 순번과 캠퍼스 순번으로 프로그램 리스트를 가져온다.
            List<VSObject> programList = studentService.getProgramList(studentCondition);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(programList);
        } catch (Exception e) {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * 클래스 콤보 리스트 API
     * @param request
     * @param response
     * @param model
     * @param campSeq
     * @param progSeq
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amg/studentClassList")
    @ResponseBody
    public VSResult<?> getClassList(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "campSeq", required = false) String campSeq,
            @RequestParam(value = "progSeq", required = false) String progSeq) throws Exception {
        logger.debug("studentClassList");
        
        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        // 세션에서 기관 순번을 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String insSeq = userDetail.getInsSeq();
        
        StudentCondition studentCondition = new StudentCondition();
        studentCondition.setInsSeq(insSeq);
        studentCondition.setCampSeq(campSeq);
        studentCondition.setProgSeq(progSeq);
        
        try {
            List<VSObject> classList = studentService.getClassList(studentCondition);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(classList);
        } catch (Exception e) {
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            e.printStackTrace();
        }
        return result;
    }
	
}
