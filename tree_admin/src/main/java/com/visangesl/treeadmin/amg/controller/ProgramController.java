package com.visangesl.treeadmin.amg.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.service.ProgramService;
import com.visangesl.treeadmin.amg.vo.Program;
import com.visangesl.treeadmin.amg.vo.ProgramCondition;
import com.visangesl.treeadmin.controller.BaseController;
import com.visangesl.treeadmin.exception.ExceptionHandler;
import com.visangesl.treeadmin.lcms.program.vo.Item;
import com.visangesl.treeadmin.lcms.program.vo.ItemInfoCondition;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.util.TreeAdminUtil;
import com.visangesl.treeadmin.vo.VSResult;

@Controller
public class ProgramController implements BaseController {

	@Autowired
	private ProgramService programService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/amg/programList", method=RequestMethod.GET)
	public String programList(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "orderBy", required = false) String orderBy,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "useYn", required = false) String useYn,
			@RequestParam(value = "progNm", required = false) String progNm,
			@RequestParam(value = "pageSize", required = false) String pageSize,
			@RequestParam(value = "pageNo", required = false) String pageNo
			) throws Exception {
		
		logger.debug("programList");
		
		ProgramCondition condition = new ProgramCondition();
		
		
		TreeUserDetails usertDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = usertDetail.getInsSeq(); // 기관코드 
		
		
		condition.setOrderColumn(TreeAdminUtil.isNull(orderBy, "modDate"));
		condition.setSort(TreeAdminUtil.isNull(sort, "DESC"));
		
		
		useYn = TreeAdminUtil.isNull(useYn, "");
		
		progNm = TreeAdminUtil.isNull(progNm, "");
		

		// 현재 페이지 번호와 한페이지에 표시할 갯수 설정.
		condition.setPageSize(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageSize, "20"), 20));
		condition.setPageNo(TreeAdminUtil.isNumber(TreeAdminUtil.isNull(pageNo, "1"), 1));
	    condition.setInstituteSeq(insInfoSeq);
	    condition.setProgNm(progNm);
	    condition.setUseYn(useYn);
	    
	    
	    int totalCount = programService.getDataListCnt(condition);
	    
        // 쿼리로 데이터를 가져올때 시작 인덱스 계산 및 설정.
        int pageStartIndex = (condition.getPageNo() - 1) * condition.getPageSize();
        condition.setPageStartIndex(pageStartIndex);
        
        List<VSObject> programList = programService.getDataList(condition);
        
        
        // 리스트에 역순 번호 매기기.
        int i = 0;
        while(i < programList.size()) {
            Program item = (Program) programList.get(i);
            item.setRn(String.valueOf(totalCount - (pageStartIndex + i)));
            i++;
        }
        
		// 리스트에 역순 번호 매기기.
		model.addAttribute("condition", condition);
		model.addAttribute("programList", programList);
		model.addAttribute("totalCount", totalCount);
		
		return "/amg/programList";
	}

	
	
	@RequestMapping(value = "/amg/programInsert")
	public String programInsert(HttpServletRequest request, HttpServletResponse response, Model model 
								, @RequestParam(value = "orderBy", required = false) String orderBy
								, @RequestParam(value = "sort", required = false) String sort
								, @RequestParam(value = "useYn", required = false) String useYn
								, @RequestParam(value = "progNm", required = false) String progNm
								, @RequestParam(value = "pageNo", required = false) String pageNo) throws Exception {			
			
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 기관코드 
		String mbrId = userDetail.getUsername(); //member Id
		
		
		ItemInfoCondition pCond = new ItemInfoCondition();
		pCond.setInstituteSeq(insInfoSeq);
		
		int itemListCnt  =0 ;
		// 기관이 가진 상품 목록 조회
		List<VSObject> itemList = programService.getInstituteItemList(pCond);
		if(itemList != null){
		     itemListCnt = itemList.size();	
		}
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("progNm", progNm);
		model.addAttribute("pageNo", pageNo);
		
		
		model.addAttribute("itemListCnt", itemListCnt);
		model.addAttribute("itemList", itemList);
		
		return "/amg/programEdit";
	}
	
	
	@RequestMapping(value = "/amg/programEdit")
	public String programEdit(HttpServletRequest request, HttpServletResponse response, Model model 
			, @RequestParam(value = "progSeq", required = false) String progSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "progNm", required = false) String progNm
			, @RequestParam(value = "pageNo", required = false) String pageNo			
			) throws Exception {
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 기관코드 
		String mbrId = userDetail.getUsername(); //member Id
		
		ProgramCondition pCond = new ProgramCondition();
		pCond.setProgSeq(progSeq);
		
		pCond.setInstituteSeq(insInfoSeq);
		
		// 프로그램 기본 정보 조회 
		Program progInfo = (Program) programService.getData(pCond);
		
		// 프로그램에 등록된 아이템 목록 조회 
		List<VSObject> pItemList = programService.getProgItemList(progSeq);
		
		model.addAttribute("progInfo", progInfo);
		model.addAttribute("pItemList", pItemList);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("progNm", progNm);
		model.addAttribute("pageNo", pageNo);
		
		return "/amg/programEdit";
	}
	
	@RequestMapping(value = "/amg/programView")
	public String programView(HttpServletRequest request, HttpServletResponse response, Model model 
			, @RequestParam(value = "progSeq", required = false) String progSeq
			, @RequestParam(value = "orderBy", required = false) String orderBy
			, @RequestParam(value = "sort", required = false) String sort
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "progNm", required = false) String progNm
			, @RequestParam(value = "pageNo", required = false) String pageNo				
			) throws Exception {
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insInfoSeq = userDetail.getInsSeq(); // 기관코드 
		String mbrId = userDetail.getUsername(); //member Id
		
		ProgramCondition pCond = new ProgramCondition();
		pCond.setProgSeq(progSeq);
		
		pCond.setInstituteSeq(insInfoSeq);
		
		// 프로그램 기본 정보 조회 
		Program progInfo = (Program) programService.getData(pCond);
		
		// 프로그램에 등록된 아이템 목록 조회 
		List<VSObject> pItemList = programService.getProgItemList(progSeq);
		
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("sort", sort);
		model.addAttribute("useYn", useYn);
		model.addAttribute("progNm", progNm);
		model.addAttribute("pageNo", pageNo);
		
		model.addAttribute("progInfo", progInfo);
		model.addAttribute("pItemList", pItemList);
		
		return "/amg/programView";
	}
	
	
	@RequestMapping(value = "/amg/programAdd", method = RequestMethod.POST)
	public ModelAndView programAdd(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "progSeq", required = false) String progSeq
			, @RequestParam(value = "progNm", required = false) String progNm
			, @RequestParam(value = "descr", required = false) String descr
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "prodVal", required = false) String[] prodVal
			) throws Exception {
		logger.debug("programAdd");
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insSeq = userDetail.getInsSeq(); 
		String mbrId = userDetail.getUsername(); //회원 아이디 
        
        
		Program program = new Program();
		program.setProgSeq(progSeq);
		program.setProgNm(progNm);
		program.setProgDescr(descr);
		program.setUseYn(useYn);
		program.setRegId(mbrId);
		program.setModId(mbrId);
		program.setInstituteSeq(insSeq);
		

		programService.addProgramInfoData(program, prodVal);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/programList.do");
		
		return mav;
	}
	
	@RequestMapping(value = "/amg/programModify", method = RequestMethod.POST)
	public ModelAndView programModify(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "progSeq", required = false) String progSeq
			, @RequestParam(value = "progNm", required = false) String progNm
			, @RequestParam(value = "descr", required = false) String descr
			, @RequestParam(value = "useYn", required = false) String useYn
			, @RequestParam(value = "prodVal", required = false) String[] prodVal
			) throws Exception {
		logger.debug("programModify");
		
		TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
		String insSeq = userDetail.getInsSeq(); 
		String mbrId = userDetail.getUsername(); //회원 아이디 
        
        
		Program program = new Program();
		program.setProgSeq(progSeq);
		program.setProgNm(progNm);
		program.setProgDescr(descr);
		program.setUseYn(useYn);
		program.setModId(mbrId);
		program.setInstituteSeq(insSeq);
		

		programService.modifyProgramInfoData(program, prodVal);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/amg/programList.do");
		
		return mav;
	}
	
	@RequestMapping(value = "/amg/getInstituteItemList")
	@ResponseBody
	public VSResult<Item> getInstituteItemList(HttpServletRequest request, HttpServletResponse response, Model model
			, @RequestParam(value = "itemType", required = false) String itemType) throws Exception {
		
		logger.debug("getTreeItemList");
		VSResult result = new VSResult();
		
	    try {
			TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
			String insSeq = userDetail.getInsSeq(); 
			
			ItemInfoCondition condition = new ItemInfoCondition();
			condition.setInstituteSeq(insSeq);
			condition.setItemType(itemType);
			
			List<VSObject> insItemList = programService.getInstituteItemList(condition);
			
		    result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
			result.setResult(insItemList);
			
		} catch (Exception e) {
	            ExceptionHandler handler = new ExceptionHandler(e);
	            result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
	            e.printStackTrace();
	    }
		
		return result;
	}	
}
