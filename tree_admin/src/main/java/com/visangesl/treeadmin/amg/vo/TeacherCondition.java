package com.visangesl.treeadmin.amg.vo;

import com.visangesl.tree.vo.VSCondition;

public class TeacherCondition extends VSCondition {
	private String mbrId;
	private String insSeq;  // institute ID
	private String campSeq; // Campus ID
	private String campId;
	private String useYn; // Status 값
	private String searchKey; // 검색 값
	private int totalCount; // 리스트 총 갯수.
	private int pageSize; // 페이지 당 총 아이템 갯수.
	private int pageNo; // 현재 페이지.
	private int pageStartIndex; // 현재 페이지의 시작 인덱스 값.
	
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getInsSeq() {
        return insSeq;
    }
    public void setInsSeq(String insSeq) {
        this.insSeq = insSeq;
    }
    public String getCampSeq() {
		return campSeq;
	}
	public void setCampSeq(String campSeq) {
		this.campSeq = campSeq;
	}
	public String getCampId() {
        return campId;
    }
    public void setCampId(String campId) {
        this.campId = campId;
    }
    public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageStartIndex() {
		return pageStartIndex;
	}
	public void setPageStartIndex(int pageStartIndex) {
		this.pageStartIndex = pageStartIndex;
	}
}
