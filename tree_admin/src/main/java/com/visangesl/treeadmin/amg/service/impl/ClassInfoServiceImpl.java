package com.visangesl.treeadmin.amg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeadmin.amg.dao.ClassInfoDao;
import com.visangesl.treeadmin.amg.service.ClassInfoService;
import com.visangesl.treeadmin.amg.vo.ClassInfo;
import com.visangesl.treeadmin.amg.vo.ClassSchdl;
import com.visangesl.treeadmin.amg.vo.ClassSchdlCondition;
import com.visangesl.treeadmin.amg.vo.TodayClassCondition;
import com.visangesl.treeadmin.amg.vo.TodayClassVo;
import com.visangesl.treeadmin.property.TreeProperties;
import com.visangesl.treeadmin.service.CommonService;
import com.visangesl.treeadmin.vo.CodeVo;
import com.visangesl.treeadmin.vo.IcmRelVo;
import com.visangesl.treeadmin.vo.VSResult;

@Service
public class ClassInfoServiceImpl  implements ClassInfoService {

	@Autowired
	ClassInfoDao dao;
	
	@Autowired
	CommonService commService;

	@Override
	public VSObject getData(VSObject vsObject) throws Exception {
		return dao.getData(vsObject);
	}
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDataList(VSCondition dataCondition) throws Exception {
        return dao.getDataList(dataCondition);
    }
	
	@Override
    @Transactional(readOnly = true)
	public List<CodeVo> getClassTchList(String classSeq) throws Exception{
		return dao.getClassTchList(classSeq);
	}
	
	@Override
    @Transactional(readOnly = true)
	public List<CodeVo> getCampTchList(String campSeq) throws Exception{
		return dao.getCampTchList(campSeq);
	}
	
	
	@Override
    @Transactional(readOnly = true)
    public int getDataListCnt(VSCondition dataCondition) throws Exception {
        return dao.getDataListCnt(dataCondition);
    }
	
	
	@Override
    @Transactional
    public VSResult modifyDataWithResultCodeMsg(ClassInfo classInfo, ClassSchdlCondition csCond, List<ClassSchdl> schList) throws Exception{
    	VSResult resultCodeMsg = new VSResult();

    	IcmRelVo icmRel = new IcmRelVo();

    	
    	// 클래스 관계 정보 업데이트 

    	icmRel.setIcmSect(TreeProperties.getProperty("tree_re_class")); //RE003
    	icmRel.setIcmSeq(classInfo.getClassSeq());  // classSeq
    	icmRel.setRelSect(TreeProperties.getProperty("tree_re_campus")); //RE002
    	icmRel.setRelSeq(classInfo.getCampSeq());  // campSeq
    	
    	icmRel.setCond_icmSect(TreeProperties.getProperty("tree_re_class")); //RE003
    	icmRel.setCond_icmSeq(classInfo.getClassSeq());
    	icmRel.setCond_relSect(TreeProperties.getProperty("tree_re_campus")); //RE002
    	
    	commService.updateIcmRelData(icmRel);
    	
    	
		
		// 선생님 정보 ICM_REL Delete
		List<CodeVo> tList = dao.getClassTchList(classInfo.getClassSeq()); 
		
		
		for(int h=0; h < tList.size(); h++){
			
			IcmRelVo dIcmRel = new IcmRelVo();
			dIcmRel.setIcmSeq(tList.get(h).getCd());
			dIcmRel.setIcmSect(TreeProperties.getProperty("tree_re_member")); // RE004
			dIcmRel.setRelSeq(classInfo.getClassSeq());
			dIcmRel.setRelSect(TreeProperties.getProperty("tree_re_class")); // RE003
			
			commService.deleteIcmRelData(dIcmRel); // ICM_REL delete
		}
		 
		
        // 선생님 정보 ICM_REL INSERT
        for( int i=0; i < classInfo.getTeacherList().size(); i++){
        	String tMemberId = classInfo.getTeacherList().get(i).getMbrId();
        	
//        	System.out.println("in service tMemberId ="+tMemberId);
        	IcmRelVo tchIcmRel = new IcmRelVo();
        	
        	tchIcmRel.setIcmSeq(tMemberId); // teacher Id 
        	tchIcmRel.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
        	tchIcmRel.setRelSeq(classInfo.getClassSeq());
        	tchIcmRel.setRelSect(TreeProperties.getProperty("tree_re_class")); //RE003
        	
        	commService.addIcmRelData(tchIcmRel); // ICM_REL insert 
        	
        }
        
        
		
    	// 스케쥴 삭제처리 
		dao.deleteSchdlData(csCond);
    	
    	// TIME 미입력시 인서트 하지 않는다. 
		if(schList != null && schList.size() > 0){
	    	for(int i = 0; i < schList.size(); i++){
	    		
	    		dao.addSchdlDataWithResultCodeMsg(schList.get(i));
	    		
	    	}
		}
    	
        int affectedRows = dao.modifyDataWithResultCodeMsg(classInfo); 
        		
        if (affectedRows < 1) {
        	
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }    
	
	@Override
    @Transactional
    public VSResult addDataWithResultCodeMsg(ClassInfo classInfo) throws Exception {
    	
		VSResult resultCodeMsg = new VSResult();
    	IcmRelVo clsIcmRel = new IcmRelVo();

    	clsIcmRel.setIcmSect(TreeProperties.getProperty("tree_re_class")); //RE003
    	clsIcmRel.setRelSeq(classInfo.getCampSeq());
    	clsIcmRel.setRelSect(TreeProperties.getProperty("tree_re_campus")); //RE002
		
        int affectedRows = dao.addDataWithResultCodeMsg(classInfo); 
        
        String classSeq = classInfo.getClassSeq();
        clsIcmRel.setIcmSeq(classSeq);

        commService.addIcmRelData(clsIcmRel); // ICM_REL insert 
        
        
        // 선생님 정보 ICM_REL INSERT
        for( int i=0; i < classInfo.getTeacherList().size(); i++){
        	String tMemberId = classInfo.getTeacherList().get(i).getMbrId();
        	
//        	System.out.println("in service tMemberId ="+tMemberId);
        	IcmRelVo tchIcmRel = new IcmRelVo();
        	
        	tchIcmRel.setIcmSeq(tMemberId); // teacher Id 
        	tchIcmRel.setIcmSect(TreeProperties.getProperty("tree_re_member")); //RE004
        	tchIcmRel.setRelSeq(classSeq);
        	tchIcmRel.setRelSect(TreeProperties.getProperty("tree_re_class")); //RE003
        	
        	commService.addIcmRelData(tchIcmRel); // ICM_REL insert 
        	
        }
        
        

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }  
	
	@Override
    @Transactional
    public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
    	VSResult resultCodeMsg = new VSResult();

    	int affectedRows = dao.deleteDataWithResultCodeMsg(vsObject);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }
	
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getSchdlDataList(VSCondition dataCondition) throws Exception {
        return dao.getSchdlDataList(dataCondition);
    }
	
	@Override
    @Transactional
	public VSResult addSchdlDataWithResultCodeMsg(VSObject vsObject) throws Exception{
		
    	VSResult resultCodeMsg = new VSResult();

        int affectedRows = dao.addSchdlDataWithResultCodeMsg(vsObject); 

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
	}
	
	@Override
    @Transactional
	public int deleteSchdlData(VSCondition dataCondition) throws Exception{
		return dao.deleteSchdlData(dataCondition);
	}
	
	
	@Override
    @Transactional(readOnly = true)
	public List<VSObject> getClassProgList(String insSeq) throws Exception {
		return dao.getClassProgList(insSeq);
	}
	
    /**
     * 클래스의 첫번째 차시 목록 조회
     */
    @Override
    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception{
    	return dao.getFirstLessonList(tcCond);
    }

    /**
     * 현재 차시의 다음 차시 조회 
     */
    @Override
    public String getClassNextdayDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return dao.getClassNextdayDayNoCd(tcCond);
    }
    
    
    /**
     * 다음 레슨의 첫 번째 차시 조회
     */
    @Override
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return dao.getNextLessonDayNoCd(tcCond);
    }


    /**
     *  클래스의 차시 정보 등록 처리
     */
    @Override
    public int addDataTodayClass(String clsSeq, String dayNoCd, String sortOrd) throws Exception {
    	TodayClassVo tcv = new TodayClassVo();
    	tcv.setClsSeq(clsSeq);
    	tcv.setDayNoCd(dayNoCd);
    	tcv.setSortOrd(sortOrd);
        return dao.addDataTodayClass(tcv);

    }    
    /**
     * 다음 다음 차시 정보 등록
     */
    @Override
    public int insertAfterNextDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return dao.insertAfterNextDayNoCd(tcCond);
    }
    
    /**
     * 클래스의 진행된 차시 갯수 
     * @param clsSeq
     * @return
     * @throws Exception
     */
    public int getClsProgressCnt(String clsSeq) throws Exception{
    	return dao.getClsProgressCnt(clsSeq);
    }
    
    /**
     * 클래스에 등록된 차시 정보 삭제 
     * @param clsSeq
     * @return
     * @throws Exception
     */
    public int deleteClsProgress(String clsSeq) throws Exception{
    	return dao.deleteClsProgress(clsSeq);
    }
	
}
