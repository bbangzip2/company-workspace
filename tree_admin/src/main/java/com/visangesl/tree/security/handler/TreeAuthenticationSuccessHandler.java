package com.visangesl.tree.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;

public class TreeAuthenticationSuccessHandler implements
		AuthenticationSuccessHandler {
	protected Log logger = LogFactory.getLog(this.getClass());
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		handle(request, response, authentication);
        clearAuthenticationAttributes(request);
	}
	
	protected void handle(HttpServletRequest request, 
		HttpServletResponse response, Authentication authentication) throws IOException {
		String targetUrl = determineTargetUrl(authentication);
		
		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	/** Builds the target URL according to the logic defined in the main class Javadoc. */
	protected String determineTargetUrl(Authentication authentication) {		
		String targetUrl = null;
		
		String role = TreeSpringSecurityUtils.getAuthoritiesToRoles(authentication);
		
		logger.debug("role == " + role);
		
		if (role.equals("MG100")) {
			targetUrl = "/amg/instituteList.do";
		} else if (role.equals("MG121")) {
			targetUrl = "/amg/instituteInfo.do";
		} else if (role.equals("MG122")) {
			targetUrl = "/amg/instituteInfo.do";
		} else {
			targetUrl = "/member/login.do?status=BadCredential";
		}
	
		return targetUrl;
	}

	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}
	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}
}