package com.visangesl.tree.security.vo;

import com.visangesl.tree.vo.VSCondition;

public class UsernameCondition extends VSCondition {
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
