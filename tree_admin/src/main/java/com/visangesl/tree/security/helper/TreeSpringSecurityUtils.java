package com.visangesl.tree.security.helper;

import java.util.Collection;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.visangesl.tree.security.vo.TreeUserDetails;

/**
 * Spring Security Helper Class
 * 
 * 
 * @author imac
 *
 */
public class TreeSpringSecurityUtils {
	/**
	 * Authentication으로부터 UserDetails을 가져오는 Helper Method
	 *  
	 * Controller에서 사용시 Spring 3.2+이면 다음의 Annotation을 사용할 수도 있다.
	 * public ModelAndView someController(@AuthenticationPrincipal TreeUserDetails userDetails)
	 * @return
	 */
	public static TreeUserDetails getPrincipalAuthorities() {
		TreeUserDetails userDetails = null;
		
		Authentication auth = getAuthentication();
		
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			userDetails = (TreeUserDetails)auth.getPrincipal();
		}
		
		return userDetails;
	}
	
	/**
	 * 인증여부를 return하는 Helper Method
	 * Anonymous가 아니면 인증을 했다고 판단한다. 
	 * @return
	 */
	public static boolean isAuthenticated() {
		boolean isAuthenticated = false;
		
		Authentication auth = getAuthentication();
		
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			isAuthenticated = true;
		}
		
		return isAuthenticated;
	}
	
	/**
	 * Authentication instalce를 return하는 Helper Method
	 * @return
	 */
	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	/**
	 * Authorities로 부터 사용자의 Role을 return하는 Helper Method
	 * @return
	 */
	public static String getAuthoritiesToRoles() {
		return getAuthoritiesToRoles(getAuthentication());
	}
	
	/**
	 * Authorities로 부터 사용자의 Role을 return하는 Helper Method
	 * @param authentication
	 * @return
	 */
	public static String getAuthoritiesToRoles(Authentication authentication) {
		String role = null;
		
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			role = grantedAuthority.getAuthority();
			
			// 현재는 권한이 회원당 1개이므로 강제로 break처리한다.
			// 사실 collection이 1개이므로 break처리가 불필요하지만 개발작업시 Data부정합발생 가능성을 염두하여 처리해두기로 한다.
			break;
		}
		
		return role;
	}	
}
