package com.visangesl.tree.security.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.visangesl.tree.security.dao.SecurityDao;
import com.visangesl.tree.security.vo.TreeUserAuthority;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.security.vo.TreeUserInfo;
import com.visangesl.tree.security.vo.UsernameCondition;

public class TreeUserDetailServiceImpl implements UserDetailsService {
	@Autowired
	SecurityDao securityDao;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public TreeUserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		logger.debug("=====TreeUserDetail=====" );
		logger.debug("=====TreeUserDetail=====" + username );
		
		// TODO Auto-generated method stub
		UsernameCondition condition = new UsernameCondition();
		condition.setUsername(username);
		
		TreeUserInfo userInfo = null;
		try {
			userInfo = securityDao.getUserInfo(condition);
			logger.debug("=====TreeUserDetail===== OK"  );
		} catch (Exception e) {
			logger.debug("=====TreeUserDetail===== False" + e  );
			throw new UsernameNotFoundException("UsernameNotFoundException", e);
		}
		
		if (userInfo == null) {
			throw new UsernameNotFoundException("UsernameNotFoundException");
		}
		
		TreeUserDetails user = new TreeUserDetails();
		user.setUsername(username);
		user.setName(userInfo.getName());
		user.setPassword(userInfo.getPassword());
		user.setNickname(userInfo.getNickname());
		user.setProfilePhotoPath(userInfo.getProfilePhotoPath());
		user.setAccessMenus(userInfo.getAccessMenus());
	   // user.setInsSeq(userInfo.getInsSeq());
		
		
		logger.debug("TreeUserDetail UserName==" + username );
		

		Collection<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
		
		for (TreeUserAuthority authority : userInfo.getAuthorities()) {
			roles.add(new SimpleGrantedAuthority(authority.getRole()));
		}
		
		user.setAuthorities(roles);
		
		return user;
	}

}
