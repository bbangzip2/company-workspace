package com.visangesl.tree.security.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

public class TreeUserInfo extends VSObject {
	private String name;
	private String password;
	private String nickname;
	private String profilePhotoPath;
	private List<TreeUserMenu> accessMenus;
	
	private List<TreeUserAuthority> authorities;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getProfilePhotoPath() {
		return profilePhotoPath;
	}
	public void setProfilePhotoPath(String profilePhotoPath) {
		this.profilePhotoPath = profilePhotoPath;
	}
	
	
	public List<TreeUserMenu> getAccessMenus() {
		return accessMenus;
	}
	public void setAccessMenus(List<TreeUserMenu> accessMenus) {
		this.accessMenus = accessMenus;
	}
	public List<TreeUserAuthority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<TreeUserAuthority> authorities) {
		this.authorities = authorities;
	}
	
	
}
