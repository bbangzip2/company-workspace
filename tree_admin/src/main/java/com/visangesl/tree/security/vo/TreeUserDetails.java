package com.visangesl.tree.security.vo;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class TreeUserDetails implements UserDetails {
	private static final long serialVersionUID = 6342939453236468788L;
	
	// Tree User Information Fields
	private String name;
	private String nickname;
	private String profilePhotoPath;
	
	private List<TreeUserMenu> accessMenus;
	private String insSeq; // 기관 코드 
	private String instituteName;
	private String instituteLogoPath;
	
	private String classRelayIp;

	// Spring Security Fields
	private String username;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;
	private boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	public String getProfilePhotoPath() {
		return profilePhotoPath;
	}


	public void setProfilePhotoPath(String profilePhotoPath) {
		this.profilePhotoPath = profilePhotoPath;
	}

	public String getClassRelayIp() {
		return classRelayIp;
	}


	public void setClassRelayIp(String classRelayIp) {
		this.classRelayIp = classRelayIp;
	}
	
	public List<TreeUserMenu> getAccessMenus() {
		return accessMenus;
	}


	public void setAccessMenus(List<TreeUserMenu> accessMenus) {
		this.accessMenus = accessMenus;
	}


	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	
	public void setUsername(String username) {
		this.username = username;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	public String getInsSeq() {
		return insSeq;
	}


	public void setInsSeq(String insSeq) {
		this.insSeq = insSeq;
	}


	public String getInstituteName() {
		return instituteName;
	}


	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}


	public String getInstituteLogoPath() {
		return instituteLogoPath;
	}


	public void setInstituteLogoPath(String instituteLogoPath) {
		this.instituteLogoPath = instituteLogoPath;
	}


	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enabled;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
}
