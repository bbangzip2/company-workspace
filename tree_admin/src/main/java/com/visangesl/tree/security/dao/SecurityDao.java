package com.visangesl.tree.security.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.visangesl.tree.security.vo.TreeUserInfo;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.treeadmin.dao.TreeSqlSessionDaoSupport;

@Repository
public class SecurityDao extends TreeSqlSessionDaoSupport {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public TreeUserInfo getUserInfo(VSCondition condition) throws Exception {
		TreeUserInfo result = null;
		result = getSqlSession().selectOne("security.getUserInfo", condition);
		return result;
	}
}
