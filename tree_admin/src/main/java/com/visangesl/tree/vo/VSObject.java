package com.visangesl.tree.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * VO의 최상위 Object
 * 
 * @author imac
 *
 */
@XmlRootElement(name="VSObject")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class VSObject {

}
