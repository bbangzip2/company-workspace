<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	    var redOk = false;
	    
	    $(document).ready(function() {
	        
	    });
    
	    
		function programList() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/programList.do");
			$("#searchForm").submit();
		}
		
        function programEdit() {
            $("#itemForm").attr("action", "${CONTEXT_PATH}/lcms/program/programModify.do");
            $("#itemForm").submit();
        }
        
		
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->

    <div id="container">
          <!-- lnb -->
       <%@ include file="../../include/incLeft.jsp" %>
        <!-- //lnb -->      
    <div id="contents">
        <div class="hGroup">
            <h2>Program</h2>
            <a href="javascript:void(0);" onclick="javascript:programList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
        </div>
        
        <form name="searchForm" id="searchForm" method="GET">
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="sInsSeq" id="sInsSeq" value="${pInsSeq }"/>
        </form>
                
        <form name="itemForm" id="itemForm" method="post" action="">
        
        <input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
        <input type="hidden" name="itemInfoSeq" id="itemInfoSeq" value="${itemInfo.itemInfoSeq}"/>
        <input type="hidden" name="useYn" id="useYn" value="${itemInfo.useYn}"/>
        <input type="hidden" name="instituteSeq" id="instituteSeq" value="${instituteSeq}"/>
        <input type="hidden" name="instituteNm" id="instituteNm" value="${instituteNm}"/>
        <input type="hidden" name="pInsSeq" id="pInsSeq" value="${pInsSeq }"/>
        <h3 class="tit first">Basic Info</h3>
        <!-- tblForm -->
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Institute</strong></th>
                        <td>${instituteNm }</td>
                    </tr>

                    <tr>
                        <th><strong class="">Comment</strong></th>
                        <td>
                        <c:if test="${!empty itemInfo.comment}">
                                <c:set var="newline" value="<%= \"\n\" %>" />
                                ${fn:replace(itemInfo.comment, newline, '<br/>')}
                        </c:if>&nbsp;                        
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Status</strong></th>
                        <td>
	                        <c:if test="${itemInfo.useYn eq 'Y' }"> Active </c:if>
	                        <c:if test="${itemInfo.useYn eq 'N' }"> Inactive </c:if>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Last update</strong></th>
                        <td>${itemInfo.modDate }</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->

        <!-- 학습구성 -->
        <h3 class="tit">Program arrangement</h3>
        <div class="studyMakeup">
            <div class="sideL">
                <strong class="item">Selected Items (<em class="txtRed" id="selectItemCnt">${fn:length(insItemList) }</em>)</strong>

                 <!-- listArea -->
                <div class="listArea">
                    <ul>
                    <c:forEach items="${insItemList}" var="insItem" varStatus="status">
                        <li id="${insItem.prodSeq }-${insItem.itemType }">${insItem.prodTitle }<input type="hidden" name="itemVal" id="${insItem.prodSeq }-${insItem.itemType }" value="${insItem.prodSeq }#${insItem.itemType }"/></li>
                    </c:forEach>
                    </ul>
                </div>
                <!-- //listArea -->
            </div>
            <!-- //sideR -->
        </div>
        <!-- //학습구성 -->
        </form>
                
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript: programEdit();" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
            </div>
        </div>

    </div></div>
</div>
</body>
</html>