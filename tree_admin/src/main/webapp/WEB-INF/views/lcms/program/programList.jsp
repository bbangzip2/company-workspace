<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
		$(document).ready(function(){

			
			// institute list combobox 조회
			getAjaxInstituteCombo('${CONTEXT_PATH}', 'pInsSeq', '${condition.instituteSeq}', 'Institute')
			
			
			//console.debug(">> sessionScope mbrGrade="+'${sessionScope.VSUserSession.mbrGrade}');
/* 			$(".btnRed").click(function(){

				$(location).attr('href',"${CONTEXT_PATH}/lcm/program/programInsert.do");
			}); */

		});
		
		function itemSearch(){
			var insSeq = $("#pInsSeq").val();
			
			$("#searchForm #sInsSeq").val(insSeq);
			
	        $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/programList.do");
	        $("#searchForm").submit();
		}

        function itemView(insSeq, insNm, itemInfoSeq) {
        	$("#instituteSeq").val(insSeq);
        	$("#instituteNm").val(insNm);
        	$("#itemInfoSeq").val(itemInfoSeq);
        	
        	var url = "";
        	if(itemInfoSeq == ''){
        	    url = "${CONTEXT_PATH}/lcms/program/programInsert.do";
        	}else{
        		url = "${CONTEXT_PATH}/lcms/program/programView.do";
        	}
        	
            $("#itemForm").attr("action", url);
            $("#itemForm").submit();
        }
        
		// 페이징에서 선택시 호출되는 스크립트 
        function goPage(page_no){
            $("#searchForm #pageNo").val(page_no);
            $("#searchForm").submit();
        }
		
        function changeOrderType(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            itemSearch();
        }		
		
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->


    <div id="container">
    
    <!-- lnb -->
   <%@ include file="../../include/incLeft.jsp" %>
    <!-- //lnb -->    
        
    <div id="contents">
        <div class="hGroup">
            <h2>Item</h2>
        </div>
        <form name="searchForm" id="searchForm" method="get" action="">
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="sInsSeq" id="sInsSeq" value="${condition.instituteSeq }"/>
        </form>
        
        <form name="itemForm" id="itemForm" method="post" action="">
        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
        <input type="hidden" name="itemInfoSeq" id="itemInfoSeq" value=""/>
        <input type="hidden" name="instituteSeq" id="instituteSeq" value=""/>
        <input type="hidden" name="instituteNm" id="instituteNm" value=""/>
                
        <!-- selectArea -->
        <div class="selectArea">
            <select name="pInsSeq" id="pInsSeq">
            </select>

            <a href="javascript:void(0);" onclick="javascript: itemSearch();" class="btnGray"><span><img src="${CONTEXT_PATH }/images/common/icon/zoom.gif" alt="" />Search</span></a>
        </div>
        <!-- //selectArea -->

        <!-- 리스트 타입 -->
        <div class="tbList1">
            <table>
                <colgroup>
                    <col width="6%" />
                    <col width="20%" />
                    <col width="*" />
                    <col width="15%" />
                    <col width="15%" />
                    <col width="11%" />
                    <col width="6%" />
                </colgroup>

                <thead>
                    <tr>
                        <th rowspan="2">NO</th>
                        <th rowspan="2"><a href="javascript:changeOrderType('instituteNm');">Institute
                        <c:if test="${condition.orderColumn ne 'instituteNm'}">
                        ▲
                        </c:if>                             
                        <c:if test="${condition.orderColumn eq 'instituteNm'}">
                        <c:choose>
                            <c:when test="${condition.sort eq 'ASC'}">
                                ▲
                            </c:when>
                            <c:otherwise>
                                ▼
                            </c:otherwise>
                        </c:choose>
                        </c:if>                         
                        </a></th>
                        <th colspan="3" class="gray">Item</th>
                        <th rowspan="2"><a href="javascript:changeOrderType('modDate');">Last update
                        <c:if test="${condition.orderColumn ne 'modDate'}">
                        ▲
                        </c:if>                           
                        <c:if test="${condition.orderColumn eq 'modDate'}">
                        <c:choose>
                            <c:when test="${condition.sort eq 'ASC'}">
                                ▲
                            </c:when>
                            <c:otherwise>
                                ▼
                            </c:otherwise>
                        </c:choose>
                        </c:if>                         
                        </a></th>
                        <th rowspan="2">View</th>
                    </tr>

                    <tr class="secLine">
                        <th  class="gray">Book(In class)</th>
                        <th  class="gray">Self-study</th>
                        <th  class="gray">Social</th>
                    </tr>
                </thead>

                <tbody>
                    <c:if test="${empty programList}">
                        <tr>
                            <td colspan="7">조회된 데이터가 없습니다.</td>
                        </tr>
                    </c:if>
                    <c:if test="${!empty programList}">                
	                <c:forEach items="${programList}" var="progItem" varStatus="status">
	                    <tr>
	                        <td>${progItem.rn }</td>
	                        <td>${progItem.instituteNm }</td>
	                        <td><!-- book -->
		                         <c:forEach items="${progItem.itemList}" var="itemList" varStatus="status">
			                        <c:if test="${itemList.itemType eq 'PT002' }">
			                            <c:out value="${itemList.prodTitle }"/><br/>
			                        </c:if>
		                        </c:forEach>
	                        </td>
	                        <td> <!-- self-study (현재 정의된바 없음 으로 없는 코드 값인 PT000  으로 케이스 처리함 -->
	                             <c:forEach items="${progItem.itemList}" var="itemList" varStatus="status">
	                                <c:if test="${itemList.itemType eq 'PT007' }">
	                                    <c:out value="${itemList.prodTitle }"/><br/>
	                                </c:if>
	                            </c:forEach>
	                        </td>
	                        <td> <!-- social (현재 정의된바 없음 으로 없는 코드 값인 PT000  으로 케이스 처리함 -->
	                             <c:forEach items="${progItem.itemList}" var="itemList" varStatus="status">
	                                <c:if test="${itemList.itemType eq 'PT008' }">
	                                    <c:out value="${itemList.prodTitle }"/><br/>
	                                </c:if>
	                            </c:forEach>
	                        </td>
	                        <td>${progItem.modDate }</td>
	                        <td><a href="javascript:void(0);" onclick="javascript: itemView('${progItem.instituteSeq }', '${progItem.instituteNm }' , '${progItem.itemInfoSeq }' );" class="btnBlue"><span>View</span></a></td>
	                    </tr>
	                </c:forEach>
                    </c:if>
                </tbody>
            </table>
        </div>
        <!-- //리스트 타입 -->
        </form>
        
        <div class="paging">
            <tree:pagination page_no="${condition.pageNo }" total_cnt="${totalCount}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
        </div>

    </div></div>
</div>
</body>
</html>
</html>