<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
    var checkUnload = true;
    $(window).on("beforeunload", function(){
    	checkForm();
        if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
    });
	
	$(document).ready(function() {
        
        
        $("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리 
        
        $('.listArea').on('click', 'li', function() {
            $(this).toggleClass('on');
        });
        
        // TreeItem list 조회
        getAjaxItemList('');
        
        
        var itemInfoSeq = $("#itemInfoSeq").val();
        
        var submit_url = itemInfoSeq == "" ? "/lcms/itemInfoAdd.do" : "/lcms/itemInfoModify.do";
        
        $("#itemForm").ajaxForm({
            beforeSubmit : function(){
            },
            dataType : "json",
            url: '${CONTEXT_PATH}'+submit_url,
            success : function(data){
                var code = data.code;
                var msg = data.msg;

                if(code == "0000"){
                    alert('Saved.');
                    location.href = '${CONTEXT_PATH}/lcms/programList.do';
                }else{
                    if((code == "1001") || (code == "1002")){
                        alert(msg);
                    }else{
                        alert("error");
                    }
                }
            }
            , error : function(request, status, error){
                //alert("code: "+request.status + "\r\nmessage : "+request.responseText);
            },complete : function(){
            }
        });
        
        
        // set oldValue
    	$("#itemForm :input").each(function(){
    		var input = $(this);
    		var pId = input.attr("id");
    		$("#"+pId).setOldValue();
    	});
        
        $("input[type='radio']").click(function(){
        	var useyn = $(":radio[name=status]:checked").val();
        	$("#itemForm #useYn").val(useyn);
        });
        
        
    });
    
    // 수정된 값이 있는지 체크 
    function checkForm(){
    	// checkUnload 초기화 
    	checkUnload = false;
    	// form안의 모든 input을 뺑뺑이 돌려부러.
    	$("#itemForm :input").each(function(){
    		
    		var check = true;
    		var pId = $(this).attr("id");
    		 //console.log(pId+"="+$("#"+pId).isEdited());
    		
    		if(check){
    			// 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
    			if($("#"+pId).isEdited()){
    				check = false;
    				checkUnload = true;
    			}
    		}
    	});
    	
    	// radio button은 따로 체크를 한다. 
    	// 이미 수정된 값이 있다면 할 필요가 없소만.
    	if(!checkUnload){
    		
    		if($("#itemForm #useYn").val() != '${itemInfo.useYn}'){
    			checkUnload = true;
    		}
    	}
    	
    	 //console.log("checkUnload="+checkUnload);
		
    }
    
    function programList() {
        $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/programList.do");
        $("#searchForm").submit();
    }
    
	// 트리 전체 아이템 조회해서 아래 목록에 그려준다. 
	function getAjaxItemList(type){
		
		$("#tabDiv > a").each(function (){
			
			$(this).removeClass("on");   
			
			if(type == 'PT002'){
				$("#PT002").addClass("on");
			}else if(type == 'PT007'){
		        $("#PT007").addClass("on");
		    }else if(type == 'PT008'){
		        $("#PT008").addClass("on");
		    }else{
		    	$("#PT000").addClass("on");
		    }
			
		});
		
		
		var itemType = type; // 아이템 타입  
		
		  $.ajax({            
		        type : "post",
		        url : "${CONTEXT_PATH}/lcms/getTreeItemList.do",
		        beforeSend: function(req) {
		            req.setRequestHeader("Accept", "application/json");
		        },
		        async : false,
		        cache : false,
		        data : {itemType : itemType},
		        dataType: 'json',
		        success : function(data){
		            var code = data.code;
		            var msg = data.massage;
		            
		            var result = data.result;
		            var innerHtml= "";
		            
		            if ( code == "0000" ) {
		            	
		            	if(result.length > 0){

		            	    for(i=0; i< result.length; i++){
		            	    	
		            	    	innerHtml += "<li id=\""+result[i].prodSeq+"-"+result[i].itemType+"\">"
		            	        innerHtml += result[i].prodTitle;
		            	    	innerHtml += "<input type=\"hidden\" name=\"itemVal\" id=\""+result[i].prodSeq+"-"+result[i].itemType+"\" value=\""+result[i].prodSeq+"#"+result[i].itemType+"\" />";
		            	    	innerHtml += "</li>"
		            	    }
		            		$("#totalItemCnt").text(result.length);
		            	}
		            } 
		            $("#totalItem").html(innerHtml);
		            
		        },
		        error: function (xhr, ajaxOptions, thrownError){
		            alert("error");
		        }, 
		        complete:function (xhr, textStatus){
		            isLogging = false;
		        }  
		    });
		  
	}

    //ADD
    function addList() {
        
        $('#totalItem li.on').each(function(){
        	var samesame = false;
        	var leftId = $(this).attr('id');
        	
        	$('#selectItem li').each(function(){
        		if(leftId == $(this).attr('id')){
        			samesame = true;
        		}
        	});
        	
        	if(!samesame){
        		$(this).clone().prependTo('#selectItem');	
        	}
        	
        	
        });
        
        $('.listArea li').removeClass('on');
        selectedCntUpdate();
    };

    //DEL
    function delList() {
        $('#selectItem li.on').remove();
        selectedCntUpdate();
    };
    
    function selectedCntUpdate(){
        var cnt = $("#selectItem > li").length;
        $("#selectItemCnt").text(cnt);
        
    }
    
    
    // 아이템 저장 처리 
    function saveItem() {
    	
        var useYn = $(":radio[name=status]:checked").val();
        var itemInfoSeq = $("#itemInfoSeq").val();
        

        // Status 설정값 체크.
        if(useYn == null || useYn.length == 0) {
        	alert('Please choose Status.');
            //alert("Status를 설정해주세요.")
            return false;
        }
        
        
        if($("#selectItem > li ").length < 1){
            alert('Please select\none or more of items.');
            return false;
        }
        
        
       // item 처리함
        $("#selectItem > li ").find("input").each(function(){
            // 저장 하기 전에 name 파라메터명을 변경한다. (오른쪽 선택된 값만 서버에서 받으려공.)
            $(this).attr("name", "prodVal");
        });
       
        $("#eventBtn").hide(); // 더블클릭 방지로 이벤트 걸린 버튼 숨김 처리 
        $("#noEventBtn").show(); // 이벤트 없는 버튼을 보임. 
        
        
        var url = itemInfoSeq == "" ? "/lcms/itemInfoAdd.do" : "/lcms/itemInfoModify.do";
        $("#useYn").val(useYn);
        //$("#itemForm").attr("action", "${CONTEXT_PATH}" + url);
        //$("#itemForm").attr("method", "POST");
        checkUnload = false;
        $("#itemForm").submit();
    }
    
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->

    <div id="container" >
          <!-- lnb -->
       <%@ include file="../../include/incLeft.jsp" %>
        <!-- //lnb -->  
    <div id="contents">

            
        <div class="hGroup">
            <h2>Item</h2>
            <a href="javascript:void(0);" onclick = "javascript: programList();"class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
        </div>

        <form name="searchForm" id="searchForm" method="GET">
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="useYn" value="${useYn }"/> 
            <input type="hidden" name="sInsSeq" id="sInsSeq" value="${pInsSeq }"/>
        </form>
        
        
        <form name="itemForm" id="itemForm" method="post" action="">
        <input type="hidden" name="itemInfoSeq" id="itemInfoSeq" value="${itemInfo.itemInfoSeq}"/>
        <input type="hidden" name="useYn" id="useYn" value="${itemInfo.useYn}"/>
        
        <h3 class="tit first">Basic Info</h3>
        <!-- tblForm -->
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Institute</strong></th>
                        <td>${instituteNm }<input type="hidden" class="text" name="instituteSeq" value="${instituteSeq }" /></td>
                    </tr>

                    <tr>
                        <th><strong class="">Comment</strong></th>
                        <td><textarea name="comment" id="comment" style="width: 98%; height: 148px;" class="textarea">${itemInfo.comment }</textarea></td>
                    </tr>

                    <tr>
                        <th><strong class="star">Status</strong></th>
                        <td>
                            <input type="radio" class="rdo" name="status" value="Y" id="A" <c:if test="${itemInfo.useYn eq 'Y' }"> checked </c:if> /><label for="A">Active</label>
                            <input type="radio" class="rdo" name="status" value="N" id="B" <c:if test="${itemInfo.useYn eq 'N' }"> checked </c:if> /><label for="B">Inactive</label>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Last update</strong></th>
                        <td>${itemInfo.modDate }</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->

        <h3 class="tit">Item List</h3>
        <div class="studyMakeup">
            <div class="control">
                <a href="javascript:void(0);" class="btnGray" id="add" onclick="addList(); return false;"><span>ADD▶</span></a>
                <a href="javascript:void(0);" class="btnGray" id="del" onclick="delList(); return false;"><span>◀DEL</span></a>
            </div>

            <!-- sideL -->
            <div class="sideL">
                <strong class="item">Total Items(<em class="txtRed" id="totalItemCnt">0</em>)</strong>

                <!-- tab -->
                <div class="tab" id="tabDiv">
                    <a href="javascript:void(0);" onclick ="javascript:getAjaxItemList('');" class="on" id="PT000">All</a>
                    <a href="javascript:void(0);" onclick ="javascript:getAjaxItemList('PT002');" id="PT002">Book</a>
                    <a href="javascript:void(0);" onclick ="javascript:getAjaxItemList('PT007');" id="PT007">Self-study</a>
                    <a href="javascript:void(0);" onclick ="javascript:getAjaxItemList('PT008');" id="PT008">Social</a>
                </div>
                <!-- //tab -->

                <!-- listArea -->
                <div class="listArea">
                    <ul id="totalItem">
                    </ul>
                </div>
                <!-- //listArea -->
            </div>
            <!-- //sideL -->

            <!-- sideR -->
            <div class="sideR">
                <strong class="item">Selected Items (<em class="txtRed" id="selectItemCnt">${fn:length(insItemList) }</em>)</strong>

                <!-- listArea -->
                <div class="listArea">
                    <ul id="selectItem">
                    <c:forEach items="${insItemList}" var="insItem" varStatus="status">
                        <li id="${insItem.prodSeq }-${insItem.itemType }">${insItem.prodTitle }<input type="hidden" name="itemVal" id="${insItem.prodSeq }-${insItem.itemType }" value="${insItem.prodSeq }#${insItem.itemType }"/></li>
                    </c:forEach>
                    </ul>
                </div>
                <!-- //listArea -->
            </div>
            <!-- //sideR -->
        </div>
        <!-- //학습구성 -->
        </form>
        
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript: void(0);" onclick="javascript: saveItem();" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript: void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
            </div>
        </div>

    </div></div>
</div>
</html>