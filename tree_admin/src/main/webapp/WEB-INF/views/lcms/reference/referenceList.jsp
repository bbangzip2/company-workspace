<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <title>Reference setting | ${condition.lessonTitle}</title>

    <!--################################## common js & css include ################################## --> 
    <%@ include file="../../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    <link rel="stylesheet" type="text/css" href="${CONTEXT_PATH}/common/css/popup.css" />
    <script type="text/javascript">
        $(document).ready(function() {


        });

        function viewReferenceInfo(type, refSeq, content) {
            var url;

            if (type == 'FT005') {
            	url = "${CONTEXT_PATH}/lcms/reference/referenceInfo.do?referenceSeq=" + refSeq + "&lessonTitle=" + encodeURI('${condition.lessonTitle}');
            } else {
                url = content;
            }
            window.open(url,'popContents','width=600, height=500, top=0, left=0, resizable=no, status=no, menubar=no, toolbar=no, scrollbars=yes, location=no');
        }


        function insertReference() {
            $("#listForm").attr("action", "${CONTEXT_PATH}/lcms/reference/referenceInsert.do");
            $("#listForm").submit();

            opener.opener.location.reload();
        }

        function deleteReference(seq, type, file, filePath) {
        	$("#referenceSeq").val(seq);
            $("#type").val(type);
        	$("#fileNm").val(file);
        	$("#filePath").val(filePath);

            $("#listForm").attr("action", "${CONTEXT_PATH}/lcms/reference/referenceDelete.do");
            $("#listForm").submit();
        }

        function getList(orderBy) {
            console.log("sortList");
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#sortKey").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#sortKey").val(orderBy);
            $("#sort").val(sort);
            $("#listForm").attr("action", "${CONTEXT_PATH}/lcms/reference/referenceList.do");
            $("#listForm").submit();
        }

        function popClose() {
            window.open('', '_self').close();
        }

    </script>
</head>

<body>
<div id="popup">
    <!-- <h1>Reference setting</h1> -->
    <form name="listForm" id="listForm" method="post" action="">
        <input type="hidden" id="lessonCd" name="lessonCd" value="${condition.lessonCd}"/>
        <input type="hidden" id="lessonTitle" name="lessonTitle" value="${condition.lessonTitle}"/>
        <input type="hidden" id="referenceSeq" name="referenceSeq" />
        <input type="hidden" id="type" name="type" />
        <input type="hidden" id="fileNm" name="fileNm" />
        <input type="hidden" id="filePath" name="filePath" />
        <input type="hidden" id="sortKey" name="sortKey" value="${condition.listSortKey}"/>
        <input type="hidden" id="sort" name="sort" value="${condition.listSort}"/>
        <div class="popCnt">
            <!-- 리스트 타입 -->
            <div class="tbList2">
                <table>
                    <colgroup>
                        <col width="6%" />
                        <col width="" />
                        <col width="10%" />
                        <col width="20%" />
                        <col width="15%" />
                        <col width="14%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>Filename</th>
                            <th>
                                <a href="javascript:getList('SORT_ORD');">Day
                                    <c:choose>
                                        <c:when test="${condition.listSortKey eq 'SORT_ORD' && condition.listSort eq 'DESC'}">
                                            ▼
                                        </c:when>
                                        <c:otherwise>
                                            ▲
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </th>
                            <th>
                                <a href="javascript:getList('TYPE');">Reference format
                                    <c:choose>
                                        <c:when test="${condition.listSortKey eq 'TYPE' && condition.listSort eq 'DESC'}">
                                            ▼
                                        </c:when>
                                        <c:otherwise>
                                            ▲
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </th>
                            <th>
                                <a href="javascript:getList('REG_DATE');">Date
                                    <c:choose>
                                        <c:when test="${condition.listSortKey eq 'REG_DATE' && condition.listSort eq 'DESC'}">
                                            ▼
                                        </c:when>
                                        <c:otherwise>
                                            ▲
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:if test="${empty referenceList}">
                            <tr>
                                <td colspan="6">No data found.</td>
                            </tr>
                        </c:if>
                        <c:if test="${!empty referenceList}">
                            <c:forEach items="${referenceList}" var="item" varStatus="status">
                                <tr>
                                    <td>${item.no}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${item.type eq 'Text' || item.type eq 'URL'}">
                                                <a href="javascript:viewReferenceInfo('${item.type}', '${item.referenceSeq}', '${item.content}');">
                                                    <c:set var="content" value="${tree:getClearHtmlStr2(item.content)}" />
		                                            <c:choose>
		                                                <c:when test="${fn:length(content) > 30}">
                                                            <c:out value="${content}" />...
		                                                </c:when>
		                                                <c:otherwise>
                                                            <c:out value="${content}" />
		                                                </c:otherwise>
		                                            </c:choose>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${tree:thumbnailPath('Y',CONTEXT_PATH, item.filePath)}" download="${item.fileNm}">
                                                    <c:choose>
                                                        <c:when test="${fn:length(item.fileNm) > 30}">
                                                            <c:out value="${fn:substring(item.fileNm, 0 , 30)}" />...
                                                        </c:when>
                                                        <c:otherwise>
                                                            ${item.fileNm}
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
	                                </td>
                                    <td>${item.sortOrd} Day</td>
                                    <td>${item.type}</td>
                                    <td><c:out value="${fn:substring(item.regDate, 0 , 19)}" /></td>
                                    <td><a href="javascript:deleteReference('${item.referenceSeq}', '${item.type}', '${item.fileNm}', '${item.filePath}');" class="btnBlue"><span>Delete</span></a></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
            <!-- //리스트 타입 -->
            <div class="btnArea">
                <div class="sideR">
                    <a href="javascript:insertReference();" class="btn btnRed"><strong>New</strong></a>
                </div>
            </div>
        </div>
    </form>
    <%-- <a href="javascript:popClose();" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> --%>
</div>
</body>
</html>