<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <title>Reference setting | ${condition.lessonTitle}</title>

    <!--################################## common js & css include ################################## --> 
    <%@ include file="../../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    <link rel="stylesheet" type="text/css" href="${CONTEXT_PATH}/common/css/popup.css" />
    <link rel="stylesheet" href="${CONTEXT_PATH}/jwysiwyg-master/jquery.wysiwyg.css" type="text/css">
    <script src="${CONTEXT_PATH}/jwysiwyg-master/jquery.wysiwyg.js"></script>
    <script type="text/javascript">
        var data = new FormData();
        var editFlag = false;

        $(window).on("beforeunload", function() {
            if (editFlag) {
                return "The data haven't been saved yet. Do you still want to leave th page?";
            }
        });

        $(document).ready(function(){
			var obj = $(".dragArea");
			var uploadFile = false;


			$("#insertForm").change(function() {
				editFlag = true;
			});

			$("#type").change(function() {
				editFlag = true;
				var select = $(this).val();

				$(".referenceType").css("display","");
				//$("#img").css("display","none");
                $('#wysiwyg').wysiwyg('clear');

				if (select == 'FT005') {
					$(".referenceText").css("display","");
                    $(".referenceURL").css("display","none");
                    $(".referenceFile").css("display","none");
                    $('#wysiwyg').wysiwyg();
                    $('#wysiwyg').wysiwyg('focus');
				} else if (select == 'FT004') {
                    $(".referenceText").css("display","none");
                    $(".referenceURL").css("display","");
                    $(".referenceFile").css("display","none");
                    $('#wysiwyg').hide();
				} else if (select == 'FT003') {
                    $(".referenceText").css("display","none");
                    $(".referenceURL").css("display","none");
                    $(".referenceFile").css("display","");
                    $('#wysiwyg').hide();

                    $("#uploadFile").attr('accept', '.webm');
				} else if (select == 'FT001') {
                    $(".referenceText").css("display","none");
                    $(".referenceURL").css("display","none");
                    $(".referenceFile").css("display","");
                    //$("#img").css("display","");
                    $('#wysiwyg').hide();
                    $("#uploadFile").attr('accept', '.gif,.jpeg,.jpg,.png');
				}
	        });


			$("#uploadFile").change(function() {
				uploadFile = true;
				var type = $("#type").val();
				var size;
				$.each(this.files, function(index, file) {
					size = Math.round((file.size / 1024 / 1024) * 100) / 100;

				    if (type == 'Movie' && size > 10) {
				    	alert("Movie는 10MB 이하만 등록 가능 합니다.");
				    } else if (type == 'Image' && size > 1.5) {
				    	alert("Image는 1.5MB 이하만 등록 가능 합니다.");
				    }
                });
			});


            $("#insertForm").ajaxForm({
                beforeSubmit : function(){
                },
                dataType : "json",
                url: '${CONTEXT_PATH}/lcms/reference/referenceAdd.do',
                success : function(data){
                    var code = data.code;
                    var msg = data.message;

                    if(code == "0000"){
                        alert('Saved.');
                        var url ="${CONTEXT_PATH}/lcms/reference/referenceList.do?lessonCd=${condition.lessonCd}&lessonTitle=" + encodeURI("${lessonInfo.lessonTitle}");
                        location.href = url;
                    }else{
                        if((code == "1001") || (code == "1002")){
                            alert(msg);
                        }else{
                            alert("error");
                        }
                    }
                }
                , error : function(request, status, error){
                    //alert("code: "+request.status + "\r\nmessage : "+request.responseText);
                },complete : function(){
                }
            });
        });


		function goListPage(flag) {
			if (flag == 'list' && this.editFlag) {
				if (confirm("The data haven’t been saved yet.\nDo you still want to leave the page?")) {
	                history.back();
	                return false;
				}
			} else {
                history.back();
                return false;
			}
		}

        function insertReference() {
        	if (this.editFlag) {
        		var select = $("#type").val();

        		if ($("#dayNoCd").val() == null || $("#dayNoCd").val() == "") {
        		    alert("Please fill in the all fields.");
        		    return false;
        		}

                if (select == null) {
                    alert("Please fill in the all fields.");
                    return false;
                }

        		if (select == 'FT005') {
        			$("#text").val($("#wysiwyg").val());
                } else if (select == 'URL') {
                    if (!$("#url").val()) {
                        return false;
                    }
                } else if (select == 'FT003') {
                	if (!uploadFile) {
                		return false;
                	}
                } else if (select == 'FT001') {
                    if (!uploadFile) {
                        return false;
                    }
                }

        		editFlag = false;

                //$("#insertForm").attr("action", "${CONTEXT_PATH}/lcms/reference/referenceAdd.do");
                //$("#insertForm").attr("method", "POST");
                $("#insertForm").submit();

        	} else {
        	    alert("There is no registered reference.");
        	}
        }

        // 현재 팝업 닫기
        function popClose() {
            opener.opener.location.reload();

            window.open('', '_self').close();
        }

        function previewImage(targetObj, profile) {
        	if ($("#type").val() != "FT001") {
        		return false;
        	}

            var preview = document.getElementById(profile); //div id

            var ua = window.navigator.userAgent;

            if (ua.indexOf("MSIE") > -1) {//ie일때
            } else { //ie가 아닐때
                var files = targetObj.files;
                for ( var i = 0; i < files.length; i++) {

                    var file = files[i];

                    var imageType = /image.*/; //이미지 파일일경우만.. 뿌려준다.
                    if (!file.type.match(imageType))
                        continue;

                    var prevImg = document.getElementById("prev_" + profile); //이전에 미리보기가 있다면 삭제
                    if (prevImg) {
                        preview.removeChild(prevImg);
                    }

                    var img = document.createElement("img"); // 크롬은 div에 이미지가 뿌려지지 않는다. 그래서 자식Element를 만든다.
                    img.id = "prev_" + profile;
                    img.classList.add("obj");
                    img.file = file;
                    img.style.width = '100px'; //기본설정된 div의 안에 뿌려지는 효과를 주기 위해서 div크기와 같은 크기를 지정해준다.
                    img.style.height = '100px';
                    
                    preview.appendChild(img);

                    if (window.FileReader) { // FireFox, Chrome, Opera 확인.
                        var reader = new FileReader();
                        reader.onloadend = (function(aImg) {
                            return function(e) {
                                aImg.src = e.target.result;
                            };
                        })(img);
                        reader.readAsDataURL(file);
                    } else { // safari is not supported FileReader
                        //alert('not supported FileReader');
                        if (!document.getElementById("sfr_preview_error_" + profile)) {
                            var info = document.createElement("p");
                            info.id = "sfr_preview_error_" + previewId;
                            info.innerHTML = "not supported FileReader";
                            preview.insertBefore(info, null);
                        }
                    }
                }
            }
        }

        function clickUploadFile() {
            $("#uploadFile").trigger('click');
        }
    </script>
</head>
<body>
<div id="popup">
    <!-- <h1>Reference setting</h1> -->

    <div class="popCnt">
	    <div class="sideR">
	        <a href="javascript:goListPage();" class="btn btnGray"><span>&lt; List</span></a>
	    </div>

        <h3 class="tit first">Reference Upload(Text, URL, Movie, Image)</h3>
        <!-- tblForm -->
        <div class="tblForm">
            <form name="insertForm" id="insertForm" method="post" action="" enctype="multipart/form-data">
                <input type="hidden" id="lessonCd" name="lessonCd" value="${condition.lessonCd}"/>
                <input type="hidden" id="lessonTitle" name="lessonTitle" value="${condition.lessonTitle}"/>
                <table>
                    <colgroup>
                        <col width="125px" />
                        <col width="" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>Day</th>
                            <td>
                                <select id="dayNoCd" name="dayNoCd">
                                    <option value="">Day</option>
		                            <c:forEach items="${dayNoCdList}" var="item" varStatus="status">
                                        <option value="${item.dayNoCd}">Day ${status.count}</option>
		                            </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Reference format</th>
                            <td>
                                <select id="type" name="type">
                                    <option value="">Format</option>
                                    <option value="FT005">Text</option>
                                    <option value="FT004">URL</option>
                                    <option value="FT001">Image</option>
                                    <option value="FT003">Movie</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="referenceType" style="display:none">
                            <th>Insert reference</th>
                            <td>
                                <div class="referenceText" style="display:none">
                                    <textarea id="wysiwyg" rows="5" cols="80" style="display: none;"></textarea>
                                    <textarea class="text" id="text" name="text" style="display:none;"></textarea>
                                </div>
                                <div class="referenceURL" style="display:none">
                                    <input type="text" class="text" id="url" name="url" style="width: 404px;" value="http://" />
                                </div>
                                <div class="referenceFile" id="referenceFile" style="display:none;">
                                    <a href="javascript:void(0);" class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
                                    <input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'referenceFile');" style="display:none;" multiple accept="" maxlength="5"/><br/>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <!-- //tblForm -->
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript:insertReference();" class="btn btnRed"><strong>Save</strong></a>
                <a href="javascript:goListPage();" class="btn btnGray"><span>Cancel</span></a>
            </div>
        </div>
    </div>
    <%-- <a href="javascript:popClose();" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> --%>
</div>
</body>
</html>