<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <title>Reference setting | ${condition.lessonTitle}</title>

    <!--################################## common js & css include ################################## --> 
    <%@ include file="../../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    <link rel="stylesheet" type="text/css" href="${CONTEXT_PATH}/common/css/popup.css" />
    <script type="text/javascript">
        $(document).ready(function() {

            // 레퍼런스 데이터 출력
            var content = '${referenceInfo.content}';
            $("#content").html(content);

            $("#searchKey").keypress(function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    searchList();
                }
            });
        });

        function popClose() {
        	window.open('', '_self').close();
        }
    </script>
</head>

<body>
<div id="popup">
    <!-- <h1>Reference</h1> -->
    <div class="popCnt reference">
        <p class="referenceContent" id="content">
            <%-- ${referenceInfo.content} --%>
        </p>
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript:popClose();" class="btn btnRed"><strong>OK</strong></a>
            </div>
        </div>
    </div>
    <%-- <a href="javascript:popClose();" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> --%>
</div>
</body>
</html>