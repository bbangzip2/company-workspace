<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <title>Lesson card map setting | ${condition.lessonTitle}</title>

    <!--################################## common js & css include ################################## --> 
    <%@ include file="../../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    <%-- <link rel="stylesheet" type="text/css" href="${CONTEXT_PATH}/common/css/popup.css" /> --%>
    <style type="text/css">
        #container {padding-top: 0; padding-left: 0; padding-right: 0;}
    </style>
    <%-- <script src="${CONTEXT_PATH}/js/commonFn.js"></script> --%>
    <%-- <script src="${CONTEXT_PATH}/js/idangerous.swiper-2.1.min.js"></script> --%>
    <%-- <script src="${CONTEXT_PATH}/js/treeCardMap.js"></script> --%>
    <script type="text/javascript">

        // 레슨 카드맵 리스트 배열 변수
        var lessonCardMapList = new Array();
        var lessonCardMapListOri = new Array();
        var currentDayNoIndex = 1;

        var lessonCd = ${condition.lessonCd};
        var addCardPop;
        var checkUnload = false;

        $(window).on("beforeunload", function() {
            if (checkUnload) {
                return "The data haven't been saved yet. Do you still want to leave th page?";
            }
        });

        $(document).ready(function() {

            // 최초 로딩시 기존 카드맵 정보를 조회하여 lessonCardMapList에 담는다.
            <c:if test="${!empty cardList}" >
                var dayCardList = ${cardList};
                if (dayCardList != null && dayCardList != "null" && dayCardList.length > 0) {
                    $.each(dayCardList, function(index, list) {
                        lessonCardMapList[index] = list.contentSeq;
                        lessonCardMapListOri[index] = list.contentSeq;
                    });
                }
            </c:if>


            // 차시 추가 이벤트
            $('.add').on('click', function() {
                checkUnload = true;

                var cardList = getCardMapCodeList();

                if (lessonCardMapList.length >= 9) {
                    alert("The max registration must be between 1 and 10 days.");
                    return false;
                } else {
                    // 현재 작업 중인 차시의 카드맵 데이터 임시 저장
                    lessonCardMapList.splice(currentDayNoIndex -1, 1, cardList);

                    // 카드 리스트 화면 초기화
                    $('.cardWrap article').html('');
    
                    var count = $('.dayArea > ul > li') .size(); // 차시 사이즈

                    // 차시 UI 추가..
                    $("#dayUl > li").removeAttr("class");
                    var li = $("#dayLi_1").clone();
                    li.attr("id", "dayLi_" + (count + 1))
                    .attr("class", "on")
                    .appendTo($("#dayUl"))
                    .find(".menu")
                    .text("Day" + (count + 1))
                    .show();
                    $("#dayLi_" + (count + 1) + "> .dayNoCd").val('');

                    // 현재 차시 순번을 업데이트
                    currentDayNoIndex = count + 1;
                }

            });


            // 차시 이동 이벤트
            $(document).on('click', '#dayUl li .menu', function(e) {
                var id = $(this).parent().attr('id');
                var index = $("li .menu").index(this);
                var dayNoCd = $("#" + id + " > input[name='dayNoCd']").val();

                // 현재 작업 중인 카드맵 정보를 임시 저장
                var cardList = getCardMapCodeList();

                lessonCardMapList.splice(currentDayNoIndex -1, 1, cardList);

                // 현재 차시 순번을 업데이트
                currentDayNoIndex = index+1;
                //alert(currentDayNoIndex);

                // 임시 저장된 정보가 있으면 조회 후 카드 드로잉
                if (lessonCardMapList[currentDayNoIndex-1] != null) {
                    getDayNoCardMapList(lessonCardMapList[currentDayNoIndex-1]);
                } else {
                    // 카드 리스트 초기화
                    $('.cardWrap article').html('');
                }

                $('#dayUl > .on').removeAttr('class');
                $('#' + id).attr('class', 'on');

            });

            // 차시 삭제 이벤트
            $(document).on('click', '#dayUl li .del', function(e) {
                checkUnload = true;

                if (confirm('Do you want to delete the lesson day?')) {

                    var count = $('.dayArea > ul > li') .size();
                    var id = $(this).parent().attr('id');
                    var index = $("li .del").index(this);


                    // 차시가 1개 이상이면 삭제 진행
                    if (count > 1) {
                        // 차시 삭제
                        $('#' + id).remove();
                        // 임시 저장 정보 삭제
                        lessonCardMapList.splice(currentDayNoIndex-1, 1);

                        // 차시 순번 재정렬
                        $('#dayUl > li').each(function(index) {
                            $(this).children('.menu').text('Day' + (index + 1));
                            $(this).attr("id", "dayLi_" + (index + 1));
                        });

                        // 1차시를 활성화
                        $('#dayLi_1').attr('class', 'on');
                        currentDayNoIndex = 1;


                        // 임시 저장된 정보가 있으면 조회 후 카드 드로잉
                        if (lessonCardMapList[currentDayNoIndex-1] != null) {
                            getDayNoCardMapList(lessonCardMapList[currentDayNoIndex-1]);
                        } else {
                            // 카드 리스트 초기화
                            $('.cardWrap article').html('');
                        }

                    } else {
                        alert("Day 1 couldn't be deleted.");
                        return false;
                    }
                }
            });


            // 서브 카드 삭제 버튼 이벤트
            $(document).on('mouseenter', '.hiddenWrap .cardSet', function(e) {
                $(this).children('.card').children('.del').show();
            });
            // 서브 카드 삭제 버튼 이벤트
            $(document).on('mouseleave', '.hiddenWrap .cardSet', function() {
                $(this).children('.card').children('.del').hide();
            });
            // 서브 카드 삭제 이벤트
            $(document).on('click', '.cardSet .del', function(e) {
                checkUnload = true;

                if (confirm('Do you want to delete the card?')) {
                    // 서브 카드 정보 삭제
                    var pCardSeq = $(this).parents('div .cardSet').children('input').val();
                    var pUrl = "${CONTEXT_PATH}/lcms/card/cardDelete.do";
                    var pData = {lessonCd: lessonCd, contentSeq : pCardSeq};

                    var rData = serverRequest(pUrl, pData);
                    if (rData != null && rData.code == "0000") {
                        alert("Delete card completed.");
                        // 서브 카드 요소 삭제
                        $(this).parents('div .cardSet').remove();
                    } else {
                        alert("서브 카드 삭제 실패");
                    }
                }
            });

            // 서브 카드 등록 이벤트
            $('.addCard').on('click', function(e) {
                checkUnload = true;
                document.domain = "treebooster.com";

                var url = 'http://tstauthoring.treebooster.com/editor/main.do?';
                var mbrId = '${condition.mbrId}';
                var host = location.hostname;
                var target = "tstapi.treebooster.com";

                if (host.indexOf("localhost") >= 0 || host.indexOf("tstadmin") >= 0) {
                    target = "tstapi.treebooster.com";
                    url = 'http://tstauthoring.treebooster.com/editor/main.do?';
                } else {
                    target = "api.treebooster.com";
                    url = 'http://authoring.treebooster.com/editor/main.do?';
                }

                addCardPop = window.open(url + "userId=" + mbrId + "&target=" + target, "_blank", "location=no, width=1280, height=698, toolbar=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, resizable=no");

/* 
                // 서브 카드 정보 등록
                var pCardSeq = $(this).parents('div .cardSet').children('input').val();
                var pUrl = "${CONTEXT_PATH}/lcms/card/editCardInsert.do";
                var pData = {lessonCd: lessonCd, contentSeq : pCardSeq};

                var rData = serverRequest(pUrl, pData);
                if (rData != null && rData.code == "0000") {
                    alert("Saved!");

                    //location.href = "${CONTEXT_PATH}/lcms/card/cardMapList.do?lessonCd=lessonCd";

                    // 서브 카드 리스트 새로 고침(page reload 아님)
                    var pUrl = "${CONTEXT_PATH}/lcms/card/cardSubList.do";
                    var pData = {lessonCd : lessonCd, cardSect : 'MT000'}

                    var rData = serverRequest(pUrl, pData);
                    if (rData != null && rData.code == "0000") {
                        setSubCardList(rData.result);
                    } else {
                        alert("카드맵 리스트 조회 실패");
                    }

                } else {
                    alert("서브 카드 등록 실패");
                }
 */
            });

            // 카드 상세 보기 이벤트
            $(document).on('dblclick', '.cardSet', function(e) {

                var pCardSeq = $(this).children('input').val();
                var pUrl = "${CONTEXT_PATH}/lcms/card/cardInfo.do";
                var pData = {contentSeq : '48'};
                var rData = serverRequest(pUrl, pData);

                if (rData != null && rData.code == "0000") {
                    // 카드 상세 정보 드로잉
                    if (rData.result != null) {
                        setCardInfo(rData.result);
                        $('.cardDetailInfo').css('display', 'block');
                    }
                } else {
                    alert("카드 상세 정보 조회 실패");
                }
            });

            // 카드 상세 보기 닫기 이벤트
            $(document).on('click', '.close', function(e) {
                $('.cardDetailInfo').css('display', 'none');
            });


            $('#eventBtn').on('click', function() {
                setDayNoCardMapList();
            });



/*//////////////  카드맵 Sortable / Drop //////////////*/


            // 카드맵 리스트 Sortable 적용
            $('.cardWrap article').addClass('connectedSortable');
            $('.cardWrap .connectedSortable').sortable({
                cancel: ".ui-state-disabled"
                ,start:function(event,ui){
                    checkUnload = true;
                    $(".cardWrap article").css('overflow', 'hidden');
                	if(ui.item.data('cardSect') != 'MT000'){
                	    $('.cardCrack .trash').show();
                	};
                },
                stop:function(event,ui){
                  $('.cardCrack .trash').hide();
                  //cardMap.isCardDown = false;
                },
                update:function(event,ui){
                  if(ui.sender)
                  {
                    var tmpidx  = ui.item.index();
                    var clone   = ui.item.clone();

                    $('.cardWrap .connectedSortable .cardSet:nth-child('+(tmpidx+1)+')').after(clone);
                    $('.cardWrap .connectedSortable .cardSet:nth-child('+(tmpidx+1)+')').attr('z-index' , 99999);

                    $(ui.sender).sortable('cancel');
                  }
                }
              });

            // 서브 카드맵 리스트 Sortable 적용
            $('.cardCrack .hiddenWrap .subCard').addClass('connectedSortable');
            var subCardCnt = $('.cardCrack .hiddenWrap .subCard').find('.cardSet').length;
            $('.cardCrack .hiddenWrap .subCard').css('width', subCardCnt > 7 ? (subCardCnt * 150) + 'px' : '1070px');

            //$('.cardCrack .hiddenWrap').addClass('.ui-state-disabled');
            $(".cardCrack .hiddenWrap .subCard").sortable({
                   cancel: ".ui-state-disabled"
                   ,connectWith: ".connectedSortable"
                   ,start:function(event,ui){
                        checkUnload = true;
                     $(this).find('div:hidden').show();
                      //cardMap.isSubCardDown = true;
                      var cardCnt = $('.cardWrap .connectedSortable').find('.cardSet').length;
                      if(cardCnt > 4){
                       $('.cardWrap .connectedSortable').css({
                           width : (cardCnt + 1) * 260 + 'px'
                       });
                      }
                      $(this).find('a').hide();


                      $(".cardWrap article").css('overflow-x', 'auto');
                   }
                   ,stop:function(event,ui){
                      //cardMap.isSubCardDown = false;
                      //clearInterval(cardMap.cardEditTimer);
                	   $(".cardWrap article").css('overflow', 'hidden');
                   }
                   ,helper: function (e, item) {

                      var elements = item.clone();
                      var helper = $('<div/>');

                      if(elements.hasClass('cardSet')){
                        elements.removeClass('cardSet');
                      }

                     return helper.append(elements);
                    }
                   }).disableSelection();

            // 카드맵 카드 삭제 drop 적용
            $('.cardCrack .trash').droppable({
                hoverClass: "hover",

                over : function(event,ui){
                  $(this).removeClass('off');
                  $(this).addClass('on');
                }
                ,out : function(event,ui){
                  $(this).removeClass('on');
                  $(this).addClass('off');
                }
                ,drop : function(event,ui){
                  var tmpidx  = ui.draggable.index();
                  $('.cardWrap .connectedSortable .cardSet:nth-child('+(tmpidx+1)+')').remove();

                  var cardCnt = $('.cardWrap .connectedSortable').find('.cardSet').length;
                  if(cardCnt > 3){
                      $('.cardWrap .connectedSortable').css({
                          width : cardCnt * 260 + 'px'
                      });
                  }
                }
              });

/*//////////////  카드맵 Sortable / Drop //////////////*/

        });


        // 카드 상세 정보 드로잉
        function setCardInfo(cardData) {
            var cardHtml;

            $('.cardInfo_title').text(cardData.title);
            $('.cardInfo_book').text(cardData.book);
            $('.cardInfo_lesson').text(cardData.lesson);
            $('.cardInfo_time').text(cardData.time + "'");
            $('.cardInfo_updAbleYn').text(cardData.updAbleYn);
            $('.cardInfo_openAbleYn').text(cardData.openAbleYn);
            $('.cardInfo_keyword').text(cardData.keyword);
            $('.cardInfo_thmbPath').attr('src', '${CDN_URL}' + cardData.thmbPath);
            $('.cardInfo_cardType').text(cardData.cardType);
            $('.cardInfo_cardSkill').text(cardData.cardSkill);

            // 카드 스터디 모드
            var studyMode = cardData.studyMode;
            if (studyMode == 'Lecturing') {
                studyMode = '3 View';
            } else if (studyMode == 'Individual') {
                studyMode = '1 View';
            } else {
                studyMode = '2 View';
            }
            $('.cardInfo_studyMode').text(studyMode);


            // 카드 레벨
            var cardLevel = cardData.cardLevel;
            $('.cardInfo_cardLevel').html('');
            for (var i=0; i<cardLevel; i++) {
                $('.cardInfo_cardLevel').append('<img src="${CONTEXT_PATH}/images/common/icon/starB.png" alt="STAR" />');
            }
        }


        // 현재 작업 중인 카드맵의 카드 코드를 문자열로 반환 (구분자 ',')
        function getCardMapCodeList() {
            var cardSeqList = $('.cardWrap .cardSet input[name="cardSeq"]');
            var count = cardSeqList.length;
            var cardSeqArr = new Array(count);

            for (i=0; i<count; i++){
                cardSeqArr[i] = cardSeqList.eq(i).val();
            }

            return cardSeqArr.join(',');
        }

        // 차시 카드맵 리스트 조회
        function getDayNoCardMapList(contentSeq) {
            var pUrl = "${CONTEXT_PATH}/lcms/card/cardList.do";
            var pData = {contentSeq : contentSeq}

            if (contentSeq != null && contentSeq != "") {
                var rData = serverRequest(pUrl, pData);
                if (rData != null && rData.code == "0000") {
                    setCardList(rData.result);
                } else {
                    alert("카드맵 리스트 조회 실패");
                }
            } else {
                // 카드 리스트 화면 초기화
                $('.cardWrap article').html('');
            }
        }

        // 카드 리스트 드로잉
        function setCardList(cardListData) {
            var cardHtml = "";

            var cardCnt = cardListData.length;
            if(cardCnt > 5){
                $(".cardWrap article").css('overflow', 'hidden');
                $('.cardWrap article').css({
                    width : cardCnt * 260 + 'px'
                });
            } else {
                $(".cardWrap article").css('overflow', 'hidden');
                $('.cardWrap article').css({
                    width : '1233px'
                });
            }
            //$(".cardWrap article").css('overflow', 'hidden');

            $.each(cardListData, function(index, cardList) {
                var cardLevel; // 카드 레벨
                for (var i=0; i<cardList.cardLevel; i++) {
                    cardLevel += '             <img src="${CONTEXT_PATH}/images/common/icon/star.png" alt="">';
                }

                var updAbleYn = ''; // 카드 공유 여부
                if (cardList.updAbleYn == 'Y') {
                    updAbleYn = '         <a href="#" class="edit"><img src="${CONTEXT_PATH}/images/common/icon/pen.png" alt=""></a>';
                }

                var studyMode; // 카드 스터디 모드
                if (cardList.studyMode == 'Lecturing') {
                    studyMode = 'study';
                } else if (cardList.studyMode == 'Individual') {
                    studyMode = 'personStudy';
                } else {
                    studyMode = 'groupStudy';
                }

                var cardSect = cardList.cardSect; // 카드 종류별 색상 처리
                if (cardSect == 'MT001') {
                	cardSect = 'select';
                } else if (cardSect == 'MT002') {
                	cardSect = 'homework';
                } else if (cardSect == 'MT003') {
                	cardSect = 'extra';
                } else if (cardSect == 'MT004') {
                	cardSect = 'disable';
                } else {
                	cardSect = 'edited';
                }
            
                cardHtml += '<div class="cardSet">'
                + ' <input type="hidden" name="cardSeq" value="'+cardList.contentSeq+'" />'
                + ' <div class="gap"></div>'
                + ' <div class="card ' +cardSect+ '">'
                + '     <div class="cnt">'
                + '         <div class="thum">'
                + '             <img src="${CDN_URL}'+cardList.thmbPath+'" alt="">'
                + '         </div>'
                + '         <span class="icon '+studyMode+'"></span>'
                + '         <p class="bold">'+cardList.title+'</p>'
                + '         <span class="number">'+cardList.time+'\'</span>'
                + '     </div>'
                + '     <div class="option">'
                + '         <div class="star">'
                + cardLevel
                + '         </div>'
                + updAbleYn
                + '     </div>'
                + '     <p class="infoTip">Card info</p>'
                + ' </div>'
                + ' <div class="gap"></div>'
                + '</div>';

            });
            $('.cardWrap article').html('');
            $('.cardWrap article').append(cardHtml);
        }

        // 서브 카드 리스트 드로잉
        function setSubCardList(cardListData) {
            var cardHtml;

            $('.subCard').html('');

            $.each(cardListData, function(index, cardList) {
                var cardSeq = cardList.contentSeq;
                var cardLevel; // 카드 레벨
                for (var i=0; i<cardList.cardLevel; i++) {
                    cardLevel += '             <img src="${CONTEXT_PATH}/images/common/icon/star.png" alt="">';
                }
                
                var updAbleYn = ''; // 카드 공유 여부
                if (cardList.updAbleYn == 'Y') {
                    updAbleYn = '         <a href="#" class="edit"><img src="${CONTEXT_PATH}/images/common/icon/pen.png" alt=""></a>';
                }
                
                var studyMode; // 카드 스터디 모드
                if (cardList.studyMode == 'Lecturing') {
                    studyMode = 'study';
                } else if (cardList.studyMode == 'Individual') {
                    studyMode = 'personStudy';
                } else {
                    studyMode = 'groupStudy';
                }

                var cardSect = cardList.cardSect; // 카드 종류별 색상 처리
                if (cardSect == 'MT001') {
                    cardSect = 'disable';
                } else if (cardSect == 'MT002') {
                    cardSect = 'homework';
                } else if (cardSect == 'MT003') {
                    cardSect = 'extra';
                } else if (cardSect == 'MT004') {
                    cardSect = 'disable';
                } else {
                    cardSect = 'edited';
                }

                cardHtml = '   <div class="cardSet">'
                + '                <input type="hidden" name="cardSeq" value="' + cardSeq + '" />'
                + '                <div class="gap"></div>'
                + '                <div class="card ' +cardSect+ '">'
                + '                    <div class="cnt">'
                + '                        <div class="thum">'
                + '                            <img src="${CDN_URL}'+cardList.thmbPath+'" alt="" />'
                + '                        </div>'
                + '                        <span class="icon '+studyMode+'"></span>'
                + '                        <p class="bold">'+cardList.title+'</p>'
                + '                        <span class="number">'+cardList.time+'</span>'
                + '                    </div>'
                + '                    <div class="option">'
                + '                        <div class="star">'
                + cardLevel
                + '                        </div>'
                + updAbleYn
                + '                    </div>'
                + '                    <p class="infoTip">Card info</p>'
                + '                    <a href="#" class="del" style="display:none;"><img src="${CONTEXT_PATH}/images/cardMap/trash.png" alt="delete" /></a>'
                + '                </div>'
                + '                <div class="gap"></div>'
                + '            </div>';

                $('.cardCrack .hiddenWrap .subCard').css('width', index > 6 ? (subCardCnt*150)+'px' : '1070px');
                $('.subCard').append(cardHtml);

            });
        }

        // 카드맵 등록
        function setDayNoCardMapList() {
            checkUnload = true;

            // 현재 작업 중인 카드맵 정보를 임시 저장
            var cardList = getCardMapCodeList();
            lessonCardMapList.splice(currentDayNoIndex -1, 1, cardList);

            // 카드맵이 변경되었는지 체크
            if (lessonCardMapList.toString() == lessonCardMapListOri.toString()) {
            	alert("No changes to commit.");
            	return false;
            } else {
                for (i=0; i<lessonCardMapList.length; i++) {
                    if (lessonCardMapList[i] == "") {
                        //alert((i + 1) + '차시에 등록된 카드가 없습니다.');
                        alert("Please put one or more of cards on every lesson day.");
                        //break;
                        return false;
                    }
                }
                $('#eventBtn').hide();
                $('#noEventBtn').show();
            }


            // 카드맵 정보를 form 내 엘리먼트로 추가
            $("<input/>").attr({type:"hidden", name:"mbrId"}).val('${condition.mbrId}').appendTo($("#form"));
            $("<input/>").attr({type:"hidden", name:"lessonCd"}).val(lessonCd).appendTo($("#form"));

            $('#dayUl > li').each(function(index) {
            	var dayNoCd = $(this).children('input[name="dayNoCd"]').val();
            	dayNoCd = dayNoCd == null ? "" : dayNoCd;
                var sortOrd = (index+1);

            	$("#form").append('<input type="hidden" name="pDayNoCd" value="'+dayNoCd+'">');
            	$("#form").append('<input type="hidden" name="pSortOrd" value="'+ sortOrd + ':' + dayNoCd+'">');
            });
            $("#form").append('<input type="hidden" name="pCardCdList" value="'+lessonCardMapList.join(";")+'">');

            // 카드맵 등록 요청
            var frm = $('#form');
            var pData = frm.serialize();
            var pUrl = "${CONTEXT_PATH}/lcms/card/cardMapInsert.do";
            var rData;
            rData = serverRequest(pUrl, pData);

            if (rData != null && rData.code == "0000") {
                alert("Saved");

                // 새로고침
                location.reload();

                // 레슨 리스트 새로고침
                opener.opener.location.reload();

                // 창 닫아!
                //popClose();
            } else {
                alert("Not saved");
                $('#eventBtn').show();
                $('#noEventBtn').hide();
            }

        }

        // 서버 요청
        function serverRequest(pUrl, pData) {
            var rst ; // return result data

            $.ajax({
                type : "post",
                url : pUrl,
                beforeSend : function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                data : pData,
                dataType : 'json',
                success : function(data) {
                    var code = data.code;
                    var msg = data.message;
                    if (code == "0000") {
                       // return data;
                       rst = data;
                    }
                },
                error : function(xhr, ajaxOptions, thrownError) {
                    alert("error");
                },
                complete : function(xhr, textStatus) {
                    isLogging = false;
                }
            });
            return rst;

        }
        // 경고창
        function setDayNoCardMapListConfirm() {
            if (confirm("차시 이동시 현재 작업 중인 카드맵은 저장되지 않습니다.\n카드맵을 저장하지 않고 이동하시겠습니까?")) {
                return true;
            } else {
                return false;
            }
        }


        // 저작도구 카드 추가 작업 완료
        function addCardComplete() {

            addCardPop.close();
            alert("The card has been saved.");

            // 서브 카드 리스트 새로 고침(page reload 아님)
            var pUrl = "${CONTEXT_PATH}/lcms/card/cardSubList.do";
            var pData = {lessonCd : lessonCd, cardSect : 'MT000'}

            var rData = serverRequest(pUrl, pData);
            if (rData != null && rData.code == "0000") {
                setSubCardList(rData.result);
            } else {
                alert("카드맵 리스트 조회 실패");
            }
        }


        // 현재 팝업 닫기
        function popClose() {
            opener.opener.location.reload();

            window.open('', '_self').close();
        }

    </script>
</head>
<body>
<div id="wrapper">
    <!-- header -->
<%--     <div id="header">
        <div class="header">
            <h1><img src="${CONTEXT_PATH}/images/common/logo.jpg" alt="tree" /></h1>
            <div class="userInfo">
                <span class="pic"><img src="${CONTEXT_PATH}/images/common/thum.jpg" alt="" /></span>
                <strong><span class="txtBlue">김실장</span>(kimhj)</strong><span class="line">|</span><a href="">Logout</a>
            </div>
        </div>
    </div> --%>
    <!-- //header -->

    <div id="container">
        <form id="form">
        <div class="dayArea">
            <ul id="dayUl">
                <c:if test="${empty dayNoList}">
                    <li id="dayLi_1" class="on">
                        <a href="#" class="menu">Day1</a>
                        <a href="#" class="del"><img src="${CONTEXT_PATH}/images/cardMap/close.gif" alt="삭제" /></a>
                    </li>
                </c:if>
                <c:if test="${!empty dayNoList}">
                    <c:forEach items="${dayNoList}" var="item" varStatus="status">
                        <li id="dayLi_${status.count}" class="${status.count eq 1 ? 'on' : ''}">
                            <input type="hidden" class="dayNoCd" name="dayNoCd" value="${item.contentSeq}" />
                            <a href="#" class="menu">Day${status.count}</a>
                            <a href="#" class="del"><img src="${CONTEXT_PATH}/images/cardMap/close.gif" alt="삭제" /></a>
                        </li>
                    </c:forEach>
                </c:if>
            </ul>
            <a href="#" class="add"><img src="${CONTEXT_PATH}/images/cardMap/btn_add.gif" alt="추가" /></a>
        </div>

        <div id="contents">
            <!-- lessonMap -->
            <!-- 카드 벌어질때 margin-top: -100px 이동 -->
            <article class="lessonMap" style="display: block;">
                <section class="cardWrap">
                    <article>
                        <!-- 카드 -->
                        <c:if test="${!empty cardMapList}">
                            <c:set var="isDoing" value="true"/>
                            <c:forEach items="${cardMapList}" var="item" varStatus="status">
                                <div class="cardSet">
                                    <input type="hidden" name="cardSeq" value="${item.contentSeq}" />
                                    <div class="gap"></div>
                                    <div class="card select"><!-- homework edited disablek -->
                                        <div class="cnt">
                                            <div class="thum">
                                                <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, item.thmbPath)}" alt="" />
                                                <!-- <img src="" alt="" /> -->
                                            </div>
                                            <c:choose>
                                                <c:when test="${item.cardType eq '1'}"><span class="icon personStudy"></span></c:when>
                                                <c:when test="${item.cardType eq '2'}"><span class="icon groupStudy"></span></c:when>
                                                <c:otherwise><span class="icon study"></span></c:otherwise>
                                            </c:choose>
                                            <p class="bold">${item.title}</p>
                                            <span class="number">${item.time}'</span>
                                        </div>
                                        <div class="option">
                                            <div class="star">
                                                <c:forEach var="i" begin="1" end="${item.cardLevel}">
                                                    <img src="${CONTEXT_PATH}/images/common/icon/star.png" alt="" />
                                                </c:forEach>
                                            </div>
                                            <c:choose>
                                                <c:when test="${item.updAbleYn eq 'Y'}">
                                                    <a href="#" class="edit"><img src="${CONTEXT_PATH}/images/common/icon/pen.png" alt="" /></a>
                                                </c:when>
                                                <c:otherwise></c:otherwise>
                                            </c:choose>
                                        </div>
                                        <p class="infoTip">Card info</p>
                                    </div>
                                    <div class="gap"></div>
                                </div>

<%-- 
                                <c:if test="${flag != item.contentSeq}">
                                    <c:set var="isDoing" value="false"/>
                                </c:if>
                                <c:if test="${isDoing}">
                                </c:if>
 --%>
                            </c:forEach>
                        </c:if>
                        <!-- //카드 -->
                    </article>
                </section>
                <!-- cardCrack -->
                <!-- cardCrack 높이: 165px-->
                <!-- 높이값이 늘어나고 줄어들때 cardCrack의 margin-bottom: 30px도 같이 애니메이션 효과 줄것 -->
                <section class="cardCrack">
                    <!-- trash on일때 컬러 변경 -->
                    <div class="trash on"></div>
                    <div class="hiddenWrap">
                        <!-- small card -->
                        <!-- 카드를 위로 이동할때 class="card"만 innerHtml로 사용 아래로 내려올때는 smallCard 클래스로 감싸줄 것 -->
                        <!-- 카드 -->
                        <div class="subCard">
                        <c:if test="${!empty cardMapSubList}">
                            <c:forEach items="${cardMapSubList}" var="item" varStatus="status">
                                <div class="cardSet">
                                    <input type="hidden" name="cardSeq" value="${item.contentSeq}" />
                                    <div class="gap"></div>
                                    <c:set var="cardSect" value="select" />
                                    <c:choose>
                                        <c:when test="${item.cardSect eq 'MT001'}"><c:set var="cardSect" value="select" /></c:when>
                                        <c:when test="${item.cardSect eq 'MT002'}"><c:set var="cardSect" value="homework" /></c:when>
                                        <c:when test="${item.cardSect eq 'MT003'}"><c:set var="cardSect" value="extra" /></c:when>
                                        <c:when test="${item.cardSect eq 'MT004'}"><c:set var="cardSect" value="disable" /></c:when>
                                        <c:otherwise><c:set var="cardSect" value="edited" /></c:otherwise>
                                    </c:choose>
                                    <div class="card ${cardSect}">
                                        <div class="cnt">
                                            <div class="thum">
                                                <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, item.thmbPath)}" alt="" />
                                            </div>
                                            <c:choose>
                                                <c:when test="${item.cardType eq '1'}"><span class="icon personStudy"></span></c:when>
                                                <c:when test="${item.cardType eq '2'}"><span class="icon groupStudy"></span></c:when>
                                                <c:otherwise><span class="icon study"></span></c:otherwise>
                                            </c:choose>
                                            <p class="bold">${item.title}</p>
                                            <span class="number">${item.time}'</span>
                                        </div>
                                        <div class="option">
                                            <div class="star">
                                                <c:forEach var="i" begin="1" end="${item.cardLevel}">
                                                    <img src="${CONTEXT_PATH}/images/common/icon/star.png" alt="" />
                                                </c:forEach>
                                            </div>
                                            <c:choose>
                                                <c:when test="${item.updAbleYn eq 'Y'}">
                                                    <a href="#" class="edit"><img src="${CONTEXT_PATH}/images/common/icon/pen.png" alt="" /></a>
                                                </c:when>
                                                <c:otherwise></c:otherwise>
                                            </c:choose>
                                        </div>
                                        <p class="infoTip">Card info</p>
                                        <a href="#" class="del" style="display:none;"><img src="${CONTEXT_PATH}/images/cardMap/trash.png" alt="delete" /></a>
                                    </div>
                                    <div class="gap"></div>
                                </div>
                            </c:forEach>
                        </c:if>
                        <!-- //카드 -->

                    </div>
                    </div>
                    <a href="#" class="addCard"></a>
                </section>
                <!-- //cardCrack -->

                <div class="btnArea">
                    <div class="sideR">
                        <a href="javascript:void(0);" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="">Save</strong></a>
                        <a href="javascript:void(0);" id="noEventBtn" class="btn btnRed" style="display: none;"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="">Save</strong></a>
                        <a href="javascript:popClose();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="">cancel</span></a>
                    </div>
                </div>
            </article>

            <!-- alertPop -->
            <div class="alertPop none">
                <div class="cnt">
                    <p>Would you like to <strong>Start lesson</strong>?</p>
                </div>

                <div class="popBtnArea">
                    <a href="" class="popBtn">Cancel</a>
                    <a href="" class="popBtn type1">OK</a>
                </div>
            </div>

            <section class="cardDetailInfo" style="display:none;">
                <!-- card front -->
                <div class="card front">
                    <div class="cnt">
                        <div class="thum">
                            <img src="${CONTEXT_PATH}/images/function/thum.jpg" class="cardInfo_thmbPath" alt="" />
                        </div>
                        <span class="icon groupStudy"></span>
                        <p class="bold cardInfo_title">Story Words</p>
                        <span class="number cardInfo_time">6'</span>
                    </div>
                </div>
                <!-- //card front -->
                <!-- card back -->
                <div class="card back">
                    <div class="cnt">
                        <p class="bold cardInfo_title">Story Words</p>
                        <div class="cardTbl">
                            <table>
                                <colgroup>
                                    <col width="50%" />
                                    <col width="*" />
                                </colgroup>
                                <tr>
                                    <th>Book</th>
                                    <td class="cardInfo_book">Level 1_1</td>
                                </tr>
                                <tr>
                                    <th>Lesson</th>
                                    <td class="cardInfo_lesson">Lesson 3</td>
                                </tr>
                                <tr>
                                    <th>Card Type</th>
                                    <td class="cardInfo_cardType">Lecture</td>
                                </tr>
                                <tr>
                                    <th>Skill</th>
                                    <td class="cardInfo_cardSkill">Speaking</td>
                                </tr>
                                <tr>
                                    <th>View Type</th>
                                    <td class="cardInfo_studyMode">2View</td>
                                </tr>
                                <tr>
                                    <th>Level of Difficulty</th>
                                    <td class="cardInfo_cardLevel">
                                        <%-- <img src="${CONTEXT_PATH}/images/common/icon/starB.png" alt="STAR" /> --%>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Time</th>
                                    <td class="cardInfo_time">6′</td>
                                </tr>
                                <tr>
                                    <th>Editable</th>
                                    <td class="cardInfo_updAbleYn">N</td>
                                </tr>
                                <tr>
                                    <th>Sharable</th>
                                    <td class="cardInfo_openAbleYn">Y</td>
                                </tr>
                                <tr>
                                    <th>keyword</th>
                                    <td class="cardInfo_keyword">Speaking, Role play</td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- //card back -->

                <div class="btnArea">
                    <a href=""><img src="${CONTEXT_PATH}/images/cardMap/btn_cardEdit.png" alt="card edit" /></a>
                </div>

                <a href="#" class="close"><img src="${CONTEXT_PATH}/images/common/btn/close.png" alt="" /></a>
            </section>
            <!-- //lessonMap -->
        </div>
        </form>
    </div>
</div>
</body>
</html>