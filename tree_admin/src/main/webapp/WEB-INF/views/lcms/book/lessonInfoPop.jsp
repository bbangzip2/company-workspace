<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${lessonInfo.lessonTitle}</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">

        // 창 닫을때 임시 저장 데이터 삭제 
        $(window).load(function(){
            var doAjaxBeforeUnloadEnabled = true; // We hook into window.onbeforeunload and bind some jQuery events to confirmBeforeUnload.  This variable is used to prevent us from showing both messages during a single event.

            var doAjaxBeforeUnload = function (evt) {
                if (!doAjaxBeforeUnloadEnabled) {
                    return;
                }
                doAjaxBeforeUnloadEnabled = false;

                // 임시 레퍼런스 정보 삭제
                jQuery.ajax({
                    url: "${CONTEXT_PATH}/lcms/reference/referenceTempDataDelete.do?lessonCd=" + ${lessonInfo.lessonSeq},
                    success: function (a) {
                        console.debug("Ajax call finished");
                    },
                    async: false /* This is the dangerous part.  Your mileage may vary. */
                });

                // 임시 카드 맵 정보 삭제
                jQuery.ajax({
                    url: "${CONTEXT_PATH}/lcms/card/deleteContentTempData.do?lessonCd=" + ${lessonInfo.lessonSeq},
                    success: function (a) {
                        console.debug("Ajax call finished");
                    },
                    async: false /* This is the dangerous part.  Your mileage may vary. */
                });

            }

            $(document).ready(function () {
                window.onbeforeunload = doAjaxBeforeUnload;
                $(window).unload(doAjaxBeforeUnload);
            });
        });


	    $(document).ready(function() {

            $("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리 
            
            $("#eventBtn").click(function(){
                updateLessonInfo();
            });	  
            
	        var submit_url = "/lcms/lessonInfoUpdate.do";
	        
	        $("#lessonForm").ajaxForm({
	            beforeSubmit : function(){
	            },
	            dataType : "json",
	            url: '${CONTEXT_PATH}'+submit_url,
	            success : function(data){
	                var code = data.code;
	                var msg = data.message;

	               console.log(msg);
	                if(code == "0000"){
	                    alert('Saved.');
	                    location.reload();
	                }else{
	                    if((code == "1001") || (code == "1002")){
	                        alert(msg);
	                    }else{
	                        alert("error");
	                    }
	                }
	            }
	            , error : function(request, status, error){
	                //alert("code: "+request.status + "\r\nmessage : "+request.responseText);
	            },complete : function(){
	            }
	        });
	        
	    });

	    function closePop(){
	        window.close();
	    }

	    function cardMapPop(){
	    	var lessonSeq = $("#lessonSeq").val();
	    	var option ="width=1280, height=680";
	    	var url ="${CONTEXT_PATH}/lcms/card/cardMapList.do?lessonCd="+lessonSeq + "&lessonTitle=" + encodeURI("${lessonInfo.lessonTitle}");
	    	var pop = window.open(url, "cardMapPop", option);
            pop.focus();
        }

	    function referencePop(){
	    	var lessonSeq = $("#lessonSeq").val();
	    	var option ="width=870px, height=600px";
            var url ="${CONTEXT_PATH}/lcms/reference/referenceList.do?lessonCd="+lessonSeq + "&lessonTitle=" + encodeURI("${lessonInfo.lessonTitle}");
            var pop = window.open(url, "cardMapPop", option);
            pop.focus();
	    }

       function updateLessonInfo() {
            
            $("#eventBtn").hide(); // 더블클릭 방지로 이벤트 걸린 버튼 숨김 처리 
            $("#noEventBtn").show(); // 이벤트 없는 버튼을 보임. 
            
            var url = "/lcms/lessonInfoUpdate.do";
            
            $("#lessonForm").attr("action", "${CONTEXT_PATH}" + url);
            $("#lessonForm").attr("method", "POST");
            $("#lessonForm").submit();
        }

       function clickUploadFile() {
           $("#uploadFile").trigger('click');
       }
     
        function previewImage(targetObj, profile) {
            var ua = window.navigator.userAgent;
        
            if (ua.indexOf("MSIE") > -1) {//ie일때
            } else { //ie가 아닐때
                var files = targetObj.files;
                for ( var i = 0; i < files.length; i++) {
        
                    var file = files[i];

                    if (window.FileReader) { // FireFox, Chrome, Opera 확인.
                        var reader = new FileReader();
                        reader.onloadend = (function(aImg) {
                            return function(e) {
                                /* aImg.src = e.target.result; */
                                $("#logoImage").attr('src', e.target.result);
                                $("#logoImage").css("display", "");
                            };
                        })($("#logoImage"));
                        reader.readAsDataURL(file);
                        
                    } 
                }
            }
        }
        
	</script>
</head>

<body>
<div id="popup">
    <div class="popCnt">
        <!-- tblForm -->
        <div class="tblForm">
        <form name="lessonForm" id="lessonForm" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="bookSeq" id="bookSeq" value="${lessonInfo.bookSeq}"/>
        <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
        <input type="hidden" name="lessonSeq" id="lessonSeq" value="${lessonInfo.lessonSeq }"/>
        
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th>Book</th>
                        <td>${lessonInfo.bookTitle }</td>
                    </tr>

                    <tr>
                        <th>Lesson Title</th>
                        <td>${lessonInfo.lessonTitle }</td>
                    </tr>

                    <tr>
                        <th>Lesson thumbnail</th>
                        <td>
                            <div class="logoUpload">
                            
                                   <span class="preview">
                                   <c:choose>
                                        <c:when test="${empty lessonInfo.thmbPath}">
                                            <img id="logoImage" src="${CONTEXT_PATH }/images/common/no-image_1.png" class="thum" alt="thumbnail" />
                                        </c:when>
                                        <c:otherwise>
                                            <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, lessonInfo.thmbPath)}" alt="thumbnail" class="thum" />
                                        </c:otherwise>
                                    </c:choose>                             
                                    </span>
                                
                                    <div>
                                        <!-- <p>(Size : 100 * XXX Pixel)</p> -->
                                       <a class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
                                       <span><input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'profile')" style="display:none"/></span>
                                    </div>
                                </div>
                        </td>
                    </tr>

                    <tr>
                        <th>Comment</th>
                        <td><textarea name="descr" id="descr" style="width: 96%; height: 100px;">${lessonInfo.lessonDescr }</textarea></td>
                    </tr>

                    <tr>
                        <th>Lesson card map</th>
                        <td>
                            <a href="javascript: cardMapPop();" class="btn btnGray"><span>Setting</span></a>
                        </td>
                    </tr>

                    <tr>
                        <th>Reference</th>
                        <td>
                            <a href="javascript: referencePop();" class="btn btnGray"><span>Setting</span></a> <span style="padding-left:  10px;">Please add the Reference after making lesson day</span>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->
        </form>
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript:void(0);" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript:void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript:closePop();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />cancel</span></a>
            </div>
        </div>
    </div>

</div>
</body>
</html>