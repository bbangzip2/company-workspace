<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
    var checkUnload = true;
    $(window).on("beforeunload", function(){
    	checkForm();
        if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
    });
    
	    $(document).ready(function() {
	        
	        $("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리 
	        
	        $("#eventBtn").click(function(){
	        	saveBookInfo();
	        });
	        
	        var bookSeq = $("#bookForm #bookSeq").val();
	        var submit_url = bookSeq == "" ? "/lcms/bookInfoAdd.do" : "/lcms/bookInfoModify.do";
	        
	        $("#bookForm").ajaxForm({
	            beforeSubmit : function(){
	            },
	            dataType : "json",
	            url: '${CONTEXT_PATH}'+submit_url,
	            success : function(data){
	                var code = data.code;
	                var msg = data.message;

	               console.log(msg);
	                if(code == "0000"){
	                    alert('Saved.');
	                    location.href = '${CONTEXT_PATH}/lcms/bookInfoEdit.do?bookSeq='+msg;
	                }else{
	                    if((code == "1001") || (code == "1002")){
	                        alert(msg);
	                    }else{
	                        alert("error");
	                    }
	                }
	            }
	            , error : function(request, status, error){
	                //alert("code: "+request.status + "\r\nmessage : "+request.responseText);
	            },complete : function(){
	            }
	        });
	        
	        // set oldValue
	    	$("#bookForm :input").each(function(){
	    		var input = $(this);
	    		var pId = input.attr("id");
	    		$("#"+pId).setOldValue();
	    	});
	        
	        $("input[type='radio']").click(function(){
	        	var useyn = $(":radio[name=status]:checked").val();
	        	$("#bookForm #useYn").val(useyn);
	        });
	        
	        
	    });

	    // 수정된 값이 있는지 체크 
	    function checkForm(){
	    	// checkUnload 초기화 
	    	checkUnload = false;
	    	// form안의 모든 input을 뺑뺑이 돌려부러.
	    	$("#bookForm :input").each(function(){
	    		
	    		var check = true;
	    		var pId = $(this).attr("id");
	    		 //console.log(pId+"="+$("#"+pId).isEdited());
	    		
	    		if(check){
	    			// 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
	    			if($("#"+pId).isEdited()){
	    				check = false;
	    				checkUnload = true;
	    			}
	    		}
	    	});
	    	
	    	// radio button은 따로 체크를 한다. 
	    	// 이미 수정된 값이 있다면 할 필요가 없소만.
	    	if(!checkUnload){
	    		
	    		if($("#bookForm #useYn").val() != '${bookInfo.useYn}'){
	    			checkUnload = true;
	    		}
	    	}
	    	
	    	 //console.log("checkUnload="+checkUnload);
			
	    }
	    
        function bookList() {
             $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/bookList.do");
             $("#searchForm").submit();
        }
        
        
		function bookEdit() {
			$("#bookForm").attr("action", "${CONTEXT_PATH}/lcms/book/bookEdit.do");
			$("#bookForm").attr("method", "POST");
			$("#bookForm").submit();
		}

       function saveBookInfo() {
            var useYn = $(":radio[name=status]:checked").val();
            var bookSeq = $("#bookForm #bookSeq").val();
            var bookTitle = $("#bookTitle").val();
            var descr = $("#descr").val();
            
            // Campus Name 입력값 체크.
            if(bookTitle == null || bookTitle.length == 0) {
                alert("Please enter title.")
                $("#prodTitle").focus();
                return false;
            }
            
            if(descr == null || descr.length == 0) {
                alert("Please enter comment.")
                return false;
            }

            // Status 설정값 체크.
            if(useYn == null || useYn.length == 0) {
                alert("Please choose status.")
                return false;
            }
            
            $("#eventBtn").hide(); // 더블클릭 방지로 이벤트 걸린 버튼 숨김 처리 
            $("#noEventBtn").show(); // 이벤트 없는 버튼을 보임. 
            
            //var url = bookSeq == "" ? "/lcms/bookInfoAdd.do" : "/lcms/bookInfoModify.do";
            
            $("#useYn").val(useYn);
            //$("#bookForm").attr("action", "${CONTEXT_PATH}" + url);
            //$("#bookForm").attr("method", "POST");
            checkUnload = false;
            $("#bookForm").submit();
        }
       
       function lessonEdit(){
    	   
           var bookSeq = $("#bookForm #bookSeq").val();
           
           if(bookSeq == "" ){
               alert("There is no Book Info!");
               return false;
           }
    	   
           $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/lessonInfoEdit.do");
           $("#searchForm").attr("method", "get");
           $("#searchForm").submit();
           
       }
       
       function lessonList(){
    	   
           var bookSeq = $("#bookForm #bookSeq").val();
           
           if(bookSeq == "" ){
               alert("There is no Book Info!");
               return false;
           }
           
           $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/lessonList.do");
           $("#searchForm").attr("method", "get");
           $("#searchForm").submit();
           
       }
       
       function clickUploadFile() {
           $("#uploadFile").trigger('click');
       }
     
		function previewImage(targetObj, profile) {
		    var ua = window.navigator.userAgent;
		
		    if (ua.indexOf("MSIE") > -1) {//ie일때
		    } else { //ie가 아닐때
		        var files = targetObj.files;
		        for ( var i = 0; i < files.length; i++) {
		
		            var file = files[i];
		
		            if (window.FileReader) { // FireFox, Chrome, Opera 확인.
		                var reader = new FileReader();
		                reader.onloadend = (function(aImg) {
		                    return function(e) {
		                        /* aImg.src = e.target.result; */
		                        $("#logoImage").attr('src', e.target.result);
		                        $("#logoImage").css("display", "");
		                    };
		                })($("#logoImage"));
		                reader.readAsDataURL(file);
		                
		            } 
		        }
		    }
		}
     
	      
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->

    <div id="container">
    
        <!-- lnb -->
       <%@ include file="../../include/incLeft.jsp" %>
        <!-- //lnb -->

		<div id="contents">
			<div class="hGroup">
				<h2>Book > Book info</h2>
				<a href="javascript: bookList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
			</div>

            <c:if test="${not empty bookInfo }">
				<div class="bookInfo">
					<span class="thum">
                    <c:choose>
                         <c:when test="${empty bookInfo.thmbPath}">
                             <img src="${CONTEXT_PATH }/images/common/no-image_1.png" class="thum" alt="thumbnail" />
                         </c:when>
                         <c:otherwise>
                             <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, bookInfo.thmbPath)}" alt="thumbnail" />
                         </c:otherwise>
                     </c:choose>    					
					</span>
					<strong>${bookInfo.bookTitle }</strong>
				</div>
            </c:if>
			<div class="tabArea">
				<ul>
					<li><a href="javascript:void(0);" class="on">Book info</a></li>
					<li><a href="javascript:void(0);" onclick="javascript: lessonEdit();">Lesson title</a></li>
					<li><a href="javascript:void(0);" onclick="javascript: lessonList();">Lesson list&nbsp;&&nbsp;Info</a></li>
				</ul>
			</div>

	        <form name="searchForm" id="searchForm" method="get" action="">
	            <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	            <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	            <input type="hidden" name="bookNm"  value="${condition.bookNm}"/>
	            <input type="hidden" name="bookSeq" id="bookSeq" value="${bookInfo.bookSeq}"/>
	        </form>
        
			<!-- tblForm -->
			<div class="tblForm">
	        <form name="bookForm" id="bookForm" method="post" action="" enctype="multipart/form-data">
	        <input type="hidden" name="bookSeq" id="bookSeq" value="${bookInfo.bookSeq}"/>
	        <input type="hidden" name="useYn" id="useYn" value="${bookInfo.useYn}"/>
	        <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
				<table>
					<colgroup>
						<col width="125px" />
						<col width="" />
					</colgroup>

					<tbody>
						<tr>
							<th><strong class="star">Title</strong></th>
							<td><input type="text" class="text" style="width: 244px;" name="bookTitle" id="bookTitle" value="${bookInfo.bookTitle}" /></td>
						</tr>

						<tr>
							<th><strong class="star">Thumbnail</strong></th>
							<td>
								<div class="logoUpload">
	                               <span class="">
	                               <c:choose>
	                                    <c:when test="${empty bookInfo.thmbPath}">
	                                        <img id="logoImage" src="${CONTEXT_PATH }/images/common/no-image_1.png" class="thum"/>
	                                    </c:when>
	                                    <c:otherwise>
	                                         <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, bookInfo.thmbPath)}" alt="thumbnail" class="thum" />
	                                    </c:otherwise>
	                                </c:choose>								
								    </span>
								
									<div>
										<!-- <p>(Size : 100 * XXX Pixel)</p> -->
	                                   <a class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
	                                   <span><input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'profile')" style="display:none"/></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<th><strong class="star">Comment</strong></th>
							<td><textarea style="width: 98%; height: 148px;" class="textarea" name="descr" id="descr">${bookInfo.descr }</textarea></td>
						</tr>

						<tr>
							<th><strong class="star">Status</strong></th>
							<td>
	                            <input type="radio" class="rdo" name="status" value="Y" id="A" <c:if test="${bookInfo.useYn eq 'Y' }"> checked </c:if> /><label for="A">Active</label>
	                            <input type="radio" class="rdo" name="status" value="N" id="B" <c:if test="${bookInfo.useYn eq 'N' }"> checked </c:if> /><label for="B">Inactive</label>
							</td>
						</tr>

						<tr>
							<th><strong class="star">Last update</strong></th>
							<td>${bookInfo.modDate }</td>
						</tr>
					</tbody>
				</table>
			</form>
			</div>
			<!-- //tblForm -->

			<div class="btnArea">
				<div class="sideR">
                <a href="javascript:void(0);" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript:void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
				</div>
			</div>
		</div>

	</div>
</div>
</body>
</html>