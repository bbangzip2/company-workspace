<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
    var checkUnload = true;
	    $(window).on("beforeunload", function(){
	    	checkForm();
	        if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
	    });
    
	    $(document).ready(function() {
	            
	        $("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리 
	        
	        $("#eventBtn").click(function(){
	        	saveLessonInfo();
	        });
	        
	        var emptyHtml =  "<tr><td class=\"aLeft\"><input type=\"text\" name=\"lessonTitle\" id=\"lessonTitle\" class=\"text\" style=\"width: 80%;\" value=\"Insert lesson Title\" />";
	            emptyHtml+=" <input type=\"hidden\" name=\"lessonSeq\" id=\"lessonSeq\" class=\"text\" style=\"width: 50px;\" value=\"\" /></td><td><input type=\"radio\" class=\"rdo\" name=\"lessonRdo\"  id=\"lessonRdo\"/></td></tr>";
            
	        var submit_url = "/lcms/lessonInfoModify.do";
	        
	        $("#lessonForm").ajaxForm({
	            beforeSubmit : function(){
	            },
	            dataType : "json",
	            url: '${CONTEXT_PATH}'+submit_url,
	            success : function(data){
	                var code = data.code;
	                var msg = data.message;

	               console.log(msg);
	                if(code == "0000"){
	                    alert('Saved.');
	                    location.href = '${CONTEXT_PATH}/lcms/lessonInfoEdit.do?bookSeq='+msg;
	                }else{
	                    if((code == "1001") || (code == "1002")){
	                        alert(msg);
	                    }else{
	                        alert("error");
	                    }
	                }
	            }
	            , error : function(request, status, error){
	                //alert("code: "+request.status + "\r\nmessage : "+request.responseText);
	            },complete : function(){
	            }
	        });
	        
	            
	            
            $("#upBtn").click(function(){
                $("input[type=radio]").each(function(idx){
                    if($(this).is(":checked")){
                        var idx = $(this).parent().parent().index();
                        
                        
                        if(idx > 0){
                            
                            var innerHtml = $(this).parent().parent().clone();
                            $(this).parent().parent().remove();
                            
                            console.log(innerHtml.html());
                            if(idx == 1){
                            	$('table').find('tr').eq(idx).before(innerHtml);
                            }else{
                            	$('table').find('tr').eq(idx-1).after(innerHtml);	
                            }
                            
                        }

                    }
                });
            });
            
           $("#downBtn").click(function(){
                $("input[type=radio]").each(function(idx){
                    if($(this).is(":checked")){
                        var idx = $(this).parent().parent().index();
                        
                        if(idx+1 <= $('#lsList').find('tr').length-1){
                            
                            var innerHtml = $(this).parent().parent().clone();
                            $(this).parent().parent().remove();
                            $('table').find('tr').eq(idx+1).after(innerHtml);
                        }

                    }
                });
            });
	        
	        // add button 
	        $("#addBtn").click(function(){
	        	$("input[type=radio]").each(function(idx){
	        		if($(this).is(":checked")){
	        			var idx = $(this).parent().parent().index();
	        			console.log(emptyHtml);
	        			$('table').find('tr').eq(idx+1).after(emptyHtml);
	        		}
	        	});
	        });
	        
            $("#delBtn").click(function(){
                $("input[type=radio]").each(function(idx){
                    if($(this).is(":checked")){
                        // var idx = $(this).parent().parent().index();
                        
                        var seq = $(this).parent().parent().find("input[id=lessonSeq]").val();
                        
                        if(seq != ''){
                        	makeDeleteInput(seq);
                        }
                        
                        $(this).parent().parent().remove();
                    }
                });
            });
            
            $("#copyBtn").click(function(){
                $("input[type=radio]").each(function(idx){
                    if($(this).is(":checked")){
                        var idx = $(this).parent().parent().index();
                        var innerHtml = $(this).parent().parent().clone();
                        
                        var title = $(this).parent().parent().find("input[id=lessonTitle]").val();
                        
                        $('table').find('tr').eq(idx+1).after(emptyHtml);
                        
                        // set value
                        $('table').find('tr').eq(idx+2).find("input[id=lessonTitle]").val(title+"    copy");
                        
                    }
                });
            });
            
            
	        // set oldValue
	    	$("#lessonForm :input").each(function(){
	    		var input = $(this);
	    		var pId = input.attr("id");
	    		$("#"+pId).setOldValue();
	    	});
	        
	    });

	    // 수정된 값이 있는지 체크 
	    function checkForm(){
	    	// checkUnload 초기화 
	    	checkUnload = false;
	    	// form안의 모든 input을 뺑뺑이 돌려부러.
	    	$("#lessonForm :input").each(function(){
	    		
	    		var check = true;
	    		var pId = $(this).attr("id");
	    		 //console.log(pId+"="+$("#"+pId).isEdited());
	    		
	    		if(check){
	    			// 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
	    			if($("#"+pId).isEdited()){
	    				check = false;
	    				checkUnload = true;
	    			}
	    		}
	    	});
	    	
	    	
	    	 //console.log("checkUnload="+checkUnload);
			
	    }
	    
	    // 삭제할 레슨순번 메이크 
	    function makeDeleteInput(val){
	    	var delInput = "<input type=\"text\" name=\"delSeq\"  id=\"delSeq\" value=\""+val+"\"/>"; //삭제된 레슨 SEQ	
	    	$("#deleteDiv").append(delInput);
	    	
	    }
	    
	    // book list 페이지로 이동 
        function bookList() {
             $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/bookList.do");
             $("#searchForm").submit();
        }
        
        // book info 페이지로 이동 
		function bookEdit() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/bookInfoEdit.do");
			$("#searchForm").attr("method", "get");
			$("#searchForm").submit();
		}
        
       function lessonList(){
           
           var bookSeq = $("#lessonForm #bookSeq").val();
           
           if(bookSeq == "" ){
               alert("There is no Book Info!");
               return false;
           }
           
           $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/lessonList.do");
           $("#searchForm").attr("method", "get");
           $("#searchForm").submit();
           
       }
       

       // 레슨 목록 저장
       function saveLessonInfo() {
            var bookSeq = $("#lessonForm #bookSeq").val();
            
            $("#eventBtn").hide(); // 더블클릭 방지로 이벤트 걸린 버튼 숨김 처리 
            $("#noEventBtn").show(); // 이벤트 없는 버튼을 보임. 
            
            if(bookSeq == "" ){
            	alert("There is no Book Info!");
            	return false;
            }
            //var url =  "/lcms/lessonInfoModify.do";
            
            //$("#lessonForm").attr("action", "${CONTEXT_PATH}" + url);
            //$("#lessonForm").attr("method", "POST");
            checkUnload = false;
            $("#lessonForm").submit();
        }
     
	      
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->

    <div id="container">
    
        <!-- lnb -->
       <%@ include file="../../include/incLeft.jsp" %>
        <!-- //lnb -->

		<div id="contents">
			<div class="hGroup">
				<h2>Book > Lesson title</h2>
				<a href="javascript: bookList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
			</div>

            <c:if test="${not empty bookInfo }">
				<div class="bookInfo">
				    <span class="thum">
                     <c:choose>
                         <c:when test="${empty bookInfo.thmbPath}">
                             <img id="logoImage" src="${CONTEXT_PATH }/images/common/no-image_1.png" class="thum" />
                         </c:when>
                         <c:otherwise>
                             <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, bookInfo.thmbPath)}" alt="thumbnail" />
                         </c:otherwise>
                     </c:choose>  					
					</span>
					<strong>${bookInfo.bookTitle }</strong>
				</div>
            </c:if>
			<div class="tabArea">
				<ul>
					<li><a href="javascript:void(0);" onclick="javascript: bookEdit();">Book info</a></li>
					<li><a href="javascript:void(0);" class="on" >Lesson title</a></li>
					<li><a href="javascript:void(0);" onclick="javascript: lessonList();">Lesson list&nbsp;&&nbsp;Info</a></li>
				</ul>
			</div>

        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
            <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="bookNm" id="bookNm" value="${condition.bookNm}"/>
            <input type="hidden" name="bookSeq" id="bookSeq" value="${bookInfo.bookSeq}"/>
        </form>
        
        
            <form name="lessonForm" id="lessonForm" method="post" action="" >
            <input type="hidden" name="bookSeq" id="bookSeq" value="${bookInfo.bookSeq}"/>
            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            <!-- tblForm -->
            <div class="tbList2">
                <table>
                    <colgroup>
                        <col width="80%" />
                        <col width="20%" />
                    </colgroup>

                    <thead>
                        <tr>
                            <th>Lesson</th>
                            <th>
                                <a href="javascript:void(0);" id="upBtn" class="icon"><img src="${CONTEXT_PATH}/images/common/icon/up.png" alt="up" /></a>
                                <a href="javascript:void(0);" id="downBtn" class="icon"><img src="${CONTEXT_PATH}/images/common/icon/down.png" alt="down" /></a>
                                <a href="javascript:void(0);" id="addBtn" class="icon"><img src="${CONTEXT_PATH}/images/common/icon/add.png" alt="add" /></a>
                                <a href="javascript:void(0);" id="delBtn" class="icon"><img src="${CONTEXT_PATH}/images/common/icon/del.png" alt="delete" /></a>
                                <a href="javascript:void(0);" id="copyBtn" class="icon"><img src="${CONTEXT_PATH}/images/common/icon/copy.png" alt="copy" /></a>
                            </th>
                        </tr>
                    </thead>

                    <tbody id="lsList">
                    <!-- 데이터가 없으면 기본 10개 row 생성  -->
                        <c:if test="${empty lessonList}">
                        <c:forEach var="item" begin="1" end="10" step="1">
                            <tr>
                               <td class="aLeft">
                                    <input type="text" name="lessonTitle" id="lessonTitle" class="text" style="width: 80%;" value="" />
                                    <input type="hidden" name="lessonSeq" id="lessonSeq" class="text" style="width: 50px;" value="" />
                                </td>
                                <td>
                                    <input type="radio" class="rdo" name="lessonRdo"  id="lessonRdo"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </c:if>
                        <c:if test="${!empty lessonList}">                        
                        <c:forEach items="${lessonList}" var="lessonItem" varStatus="status">
                        <tr>
                            <td class="aLeft">
                                <input type="text" name="lessonTitle" id="lessonTitle" class="text" style="width: 80%;" value="${lessonItem.lessonTitle }" />
                                <input type="hidden" name="lessonSeq" id="lessonSeq" class="text" style="width: 50px;" value="${lessonItem.lessonSeq }" />
                            </td>
                            <td><input type="radio" class="rdo" name="lessonRdo"  id="lessonRdo"/></td>
                        </tr>
                        </c:forEach>
                        </c:if>

                    </tbody>
                </table>
            </div>
            <div id="deleteDiv" style="display:none;"></div>
            <!-- //tblForm -->
            </form>
            
            
			<div class="btnArea">
				<div class="sideR">
                <a href="javascript:void(0);" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript:void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
				</div>
			</div>
		</div>

	</div>
</div>
</body>
</html>