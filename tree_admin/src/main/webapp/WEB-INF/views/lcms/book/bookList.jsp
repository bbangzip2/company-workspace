<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
		$(document).ready(function(){


		});


        function bookSearch(){
        	
        	var bookNm = $("#bookForm #bookNm").val();
        		
        	$("#searchForm #bookNm").val(bookNm);
        	
            $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/bookList.do");
            $("#searchForm").submit();
        }
        
        function bookInsert() {
            $("#bookForm").attr("action", "${CONTEXT_PATH}/lcms/bookInfoInsert.do");
            $("#bookForm").submit();
        }
        
        function bookEdit(id) {
            $("#bookSeq").val(id);
            $("#bookForm").attr("action", "${CONTEXT_PATH}/lcms/bookInfoEdit.do");
            $("#bookForm").submit();
        }
        
        function lessonEdit(id){
        	$("#bookSeq").val(id);
            $("#bookForm").attr("action", "${CONTEXT_PATH}/lcms/lessonInfoEdit.do");
            $("#bookForm").attr("method", "POST");
            $("#bookForm").submit();
            
        }        
		
		// 페이징에서 선택시 호출되는 스크립트 
		function go_page(page_no){
		   $("#searchForm #pageNo").val(page_no);
		   $("#searchForm").submit();
		}
        function changeOrderType(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            bookSearch();
        }
        
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->

	<div id="container">
	
        <!-- lnb -->
       <%@ include file="../../include/incLeft.jsp" %>
        <!-- //lnb -->

		<div id="contents">
			<div class="hGroup">
				<h2>Book</h2>
			</div>
        
        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
            <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="bookNm" id="bookNm" value="${condition.bookNm}"/>
        </form>
                
        <form name="bookForm" id="bookForm" method="get" action="">
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="bookSeq" id="bookSeq" value=""/>
	        
			<!-- selectArea -->
			<div class="selectArea">
				<input type="text" class="text" name="bookNm" id="bookNm" style="width: 140px;" placeholder="Book Title"  value="${condition.bookNm }" />

				<a href="javascript:void(0);" onclick="javascript: bookSearch();" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
				<a href="javascript:void(0);" onclick="javascript: bookInsert();" class="btnRed"><span>+ New</span></a>
			</div>
			<!-- //selectArea -->

			<!-- 리스트 타입 -->
			<div class="tbList2">
				<table>
					<colgroup>
						<col width="6%" />
						<col width="" />
						<col width="" />
						<col width="" />
						<col width="" />
						<col width="" />
						<col width="" />
						<col width="" />
						<col width="6%" />
					</colgroup>

					<thead>
						<tr>
							<th>NO</th>
							<th><a href="javascript:changeOrderType('bookTitle');">Book title
	                        <c:if test="${condition.orderColumn ne 'bookTitle'}">
	                        ▲
	                        </c:if>							
	                        <c:if test="${condition.orderColumn eq 'bookTitle'}">
	                        <c:choose>
	                            <c:when test="${condition.sort eq 'ASC'}">
	                                ▲
	                            </c:when>
	                            <c:otherwise>
	                                ▼
	                            </c:otherwise>
	                        </c:choose>
	                        </c:if>  
							</a></th>
							<th>Thumbnail</th>
							<th>Lessons</th>
							<th>Total<br/>day</th>
							<th>Total<br/>activity</th>
							<th>Status</th>
							<th><a href="javascript:changeOrderType('regDate');">Registration<br/>date
                            <c:if test="${condition.orderColumn ne 'regDate'}">
                            ▲
                            </c:if>     							
                            <c:if test="${condition.orderColumn eq 'regDate'}">
                            <c:choose>
                                <c:when test="${condition.sort eq 'ASC'}">
                                    ▲
                                </c:when>
                                <c:otherwise>
                                    ▼
                                </c:otherwise>
                            </c:choose>
                            </c:if>  							
							</a></th>
							<th>View</th>
						</tr>
					</thead>

					<tbody>
	                    <c:if test="${empty bookList}">
	                        <tr>
	                            <td colspan="9">No data found.</td>
	                        </tr>
	                    </c:if>
	                    <c:if test="${!empty bookList}">      					
						<c:forEach items="${bookList}" var="bookItem" varStatus="status">
							<tr>
								<td>${bookItem.rn }</td>
								<td>${bookItem.bookTitle }</td>
								<td>
								<c:choose>
								    <c:when test="${empty bookItem.thmbPath }"><img  src="${CONTEXT_PATH }/images/common/no-image_1.png" class="thum" style="width:100px;"/></c:when>
								    <c:otherwise><img src="${tree:thumbnailPath('Y',CONTEXT_PATH, bookItem.thmbPath)}" alt="thumbnail" class="thum" style="width:100px;"/></c:otherwise>
								</c:choose>
								</td>
								<td>${bookItem.lessonCnt }<br/>
								     <a href="javascript:void(0);" onclick="javascript: lessonEdit('${bookItem.bookSeq}');" class="btnBlue"><span>View</span></a>
								</td>
								<td>${bookItem.dayNoCnt }</td>
								<td>${bookItem.actCnt }</td>
								<td>
                                <c:choose>
                                    <c:when test="${bookItem.useYn eq 'Y'}">
                                        <span class="txtRed">Active</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="txtBlue">Inactive</span>
                                    </c:otherwise>
                                </c:choose> 
								</td>
								<td>${bookItem.regDate }</td>
								<td><a href="javascript:void(0);" onclick="javascript: bookEdit('${bookItem.bookSeq}');" class="btnBlue"><span>View</span></a></td>
							</tr>
						</c:forEach>
                        </c:if>
					</tbody>
				</table>
			</div>
			<!-- //리스트 타입 -->
            </form>
            
            
			<!-- paging -->
			<div class="paging">
	            <tree:pagination page_no="${condition.pageNo }" total_cnt="${totalCount}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
	        </div>
			<!-- //paging -->

		</div>
	</div>
</div>
</body>
</html>