<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
	    $(document).ready(function() {
	        
	    });

	    
	    // book list 페이지로 이동 
        function bookList() {
             $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/bookList.do");
             $("#searchForm").attr("method", "get");
             $("#searchForm").submit();
        }
        
        // book info 페이지로 이동 
		function bookEdit() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/bookInfoEdit.do");
			$("#searchForm").attr("method", "get");
			$("#searchForm").submit();
		}
        
       function lessonEdit(){
           
           $("#searchForm").attr("action", "${CONTEXT_PATH}/lcms/lessonInfoEdit.do");
           $("#searchForm").attr("method", "get");
           $("#searchForm").submit();
           
       }

       function lessonInfoPop(lessonSeq){
    	   <!-- 1020*500 -->
    	   var option ="width=768, height=500";
    	   var url = "${CONTEXT_PATH}/lcms/lessonInfoPop.do?lessonSeq="+lessonSeq;
    	   var pop = window.open(url, "lessonInfoPop", option);
    	   pop.focus();
       }
       
       // 패키징 생성 호출
       function makePackaging(bookCd, lessonCd){

    	   // 로딩 안내 문구 노출
           blockUi();

    	   $('.packagingBtn').hide();
           var url = "${CONTEXT_PATH}/lcms/packaging/contentPackaging.do";
           var pData = {bookCd : bookCd, lessonCd : lessonCd};
           var result = commonServerRequest(url, pData);

           if (result != null) {

               var code = result.code;
               if (code == "0000") {
                   alert("Packaging completed");
               }
           }

           // 로딩 안내 문구 비노출
           unBlockUi();

           location.reload();
       }

       // 패키징 배포 호출
       function releasePackaging(lessonSeq){

           // 로딩 안내 문구 노출
           blockUi();

    	   $('.releaseBtn').hide();
           var url = "${CONTEXT_PATH}/lcms/packaging/releasePackaging.do";
           var pData = {lessonCd : lessonSeq};
           var result = commonServerRequest(url, pData);

           if (result != null) {
               var code = result.code;
               if (code == "0000") {
                   alert("Release completed");
               }
           }

           // 로딩 안내 문구 비노출
           unBlockUi();

           location.reload();
       }

       // 로딩 안내 문구 노출
       function blockUi() {
           
           $.blockUI({ css: { 
               border: 'none', 
               padding: '15px', 
               backgroundColor: '#000', 
               '-webkit-border-radius': '10px', 
               '-moz-border-radius': '10px', 
               opacity: .5, 
               color: '#fff' 
           } }); 
       }

       // 로딩 안내 문구 비노출
       function unBlockUi() {
    	   $.unblockUI
       }
	</script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../../include/incAdmHeader.jsp" %>
    <!-- //header -->

    <div id="container">
        <img id="loading" alt="loading.." src="${CONTEXT_PATH}/images/common/loadingbar.gif" style="display:none;">
        <!-- lnb -->
       <%@ include file="../../include/incLeft.jsp" %>
        <!-- //lnb -->

		<div id="contents">
			<div class="hGroup">
				<h2>Book > Lesson list & Info</h2>
				<a href="javascript: bookList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
			</div>

            <c:if test="${not empty bookInfo }">
				<div class="bookInfo">
                    <span class="thum">
                     <c:choose>
                         <c:when test="${empty bookInfo.thmbPath}">
                             <!-- <img id="logoImage" src="" class="thum" alt="thumbnail" style="display:none;"/> -->
                         </c:when>
                         <c:otherwise>
                             <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, bookInfo.thmbPath)}" alt="thumbnail" />
                         </c:otherwise>
                     </c:choose>                    
                    </span>
					<strong>${bookInfo.bookTitle }</strong>
				</div>
            </c:if>
			<div class="tabArea">
				<ul>
					<li><a href="javascript:void(0);" onclick="javascript: bookEdit();">Book info</a></li>
					<li><a href="javascript:void(0);" onclick="javascript: lessonEdit();">Lesson title</a></li>
					<li><a href="javascript:void(0);" class="on" >Lesson list&nbsp;&&nbsp;Info</a></li>
				</ul>
			</div>
			

            <form name="searchForm" id="searchForm" method="get" action="">
                <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
                <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
                <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
                <input type="hidden" name="bookNm" id="bookNm" value="${condition.bookNm}"/>
                <input type="hidden" name="bookSeq" id="bookSeq" value="${bookInfo.bookSeq}"/>
            </form>
            			
            <form name="lessonForm" id="lessonForm" method="get" action="" >
	            <input type="hidden" name="bookSeq" id="bookSeq" value="${bookInfo.bookSeq}"/>
	            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            </form>
            <!-- 리스트 타입 -->
            <div class="tbList2">
                <table>
                    <colgroup>
                        <col width="" />
                        <col width="7%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />
                        <col width="4%" />
                        <col width="6%" />
                        <col width="9%" />
                        <col width="12%" />
                        <col width="6%" />
                    </colgroup>

                    <thead>
                        <tr>
                            <th rowspan="2">Lesson</th>
                            <th colspan="13">Activity&Reference</th>
                            <th rowspan="2">Packaging</th>
                            <th colspan="2" rowspan="2">Last release</th>
                            <th rowspan="2">View</th>
                        </tr>
                        <tr>
                            <th>Day</th>
                            <th>D1</th>
                            <th>D2</th>
                            <th>D3</th>
                            <th>D4</th>
                            <th>D5</th>
                            <th>D6</th>
                            <th>D7</th>
                            <th>D8</th>
                            <th>D9</th>
                            <th>D10</th>
                            <th>Extra</th>
                            <th>SUM</th>
                        </tr>
                    </thead>

                    <tbody>
		                <c:if test="${empty lessonList }">
		                    <tr><td  colspan="18">There is no data. Please add the data after making the lesson.</td></tr>
		                </c:if>
                        <c:if test="${!empty lessonList}">                        
                        <c:forEach items="${lessonList}" var="lessonItem" varStatus="status">
	                        <tr>
	                            <td class="aLeft" rowspan="2">${lessonItem.lessonTitle }</td>

	                            <td>Activity</td>
	                            
	                            <c:set var="cntTot" value="0"/>
	                            <c:set var="extCnt" value="0"/>
	                            <c:forEach begin="0" end="9" step="1" varStatus="status" var="item">
		                            <td>
		                            <c:set var="cntTot" value="${cntTot+lessonItem.dayList[item].cnt }"/>
		                            <c:choose>
		                                <c:when test="${not empty lessonItem.dayList[item].dayNoCd}"><c:out value="${lessonItem.dayList[item].cnt }" /></c:when>
		                                <c:when test="${empty lessonItem.dayList[item].dayNoCd && not empty lessonItem.dayList[item].cnt}">-<c:choose><c:when test="${lessonItem.dayList[item].cnt eq 0 }">-</c:when><c:otherwise><c:set var="extCnt" value="${lessonItem.dayList[item].cnt }" /></c:otherwise></c:choose></c:when>
		                                <c:otherwise>-</c:otherwise>
		                            </c:choose>
		                            </td>
	                            </c:forEach>
	                            <td><c:choose><c:when test="${extCnt eq 0 }">-</c:when><c:otherwise><c:out value="${extCnt }"/></c:otherwise></c:choose></td>
	                            <td><c:choose><c:when test="${cntTot eq 0 }">-</c:when><c:otherwise><c:out value="${cntTot }"/></c:otherwise></c:choose></td>
	                            <td rowspan="2">
                                    <c:choose>
                                        <c:when test="${lessonItem.pkgingYn eq 'Y'}">
                                            Completed
                                        </c:when>
                                        <c:otherwise>
                                            <a href="javascript:makePackaging('${bookInfo.bookSeq}', '${lessonItem.lessonSeq}');" class="btnGray packagingBtn"><span>Packaging</span></a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
	                            <td rowspan="2"><c:out value="${lessonItem.distDate }"/></td>
	                            <td rowspan="2">
                                    <c:if test="${lessonItem.pkgingYn eq 'Y' && lessonItem.distYn eq 'N'}">
                                        <a href="javascript:releasePackaging('${lessonItem.lessonSeq}');" class="btnGrayL releaseBtn"><span>New<br />Release</span></a>
                                    </c:if>
                                    <c:if test="${lessonItem.distYn eq 'Y'}">
                                        Completed
                                    </c:if>
                                </td>
	                            <td rowspan="2"><a href="javascript:void(0);"  onclick="javascript: lessonInfoPop('${lessonItem.lessonSeq}');" class="btnBlue"><span>View</span></a></td>
	                        </tr>
                            <tr>
	                            <td>Reference</td>
	                            <c:set var="refCnt" value="0"/>
	                            <c:set var="refCntTot" value="0"/>
                                <c:forEach begin="0" end="9" step="1" varStatus="status" var="item">
                                    <td>
                                    <c:set var="refCntTot" value="${refCntTot+lessonItem.dayList[item].refCnt }"/>
                                    <c:choose>
                                        <c:when test="${not empty lessonItem.dayList[item].refCnt}"><c:choose><c:when test="${lessonItem.dayList[item].refCnt eq 0 }">-</c:when><c:otherwise><c:out value="${lessonItem.dayList[item].refCnt }"/></c:otherwise></c:choose></c:when>
                                        <c:otherwise>-</c:otherwise>
                                    </c:choose>                                    
                                    </td>
                                </c:forEach>
                                <td>-</td>
                                <td><c:choose><c:when test="${refCntTot eq 0 }">-</c:when><c:otherwise><c:out value="${refCntTot }"/></c:otherwise></c:choose></td>
                            </tr>	                        
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
            <!-- //리스트 타입 -->
            
		</div>

	</div>
</div>
</body>
</html>