<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE } [CONTENTS FETCHER]</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
		$(document).ready(function(){


		});
		
        function fetcherList() {
            $("#searchForm").attr("action", "${CONTEXT_PATH}/contents/fetcherList.do");
            $("#searchForm").submit();
        }		
		
       function goItem(itemInfoSeq) {
        	$("#contentSeq").val(itemInfoSeq);
        	
        	var url = "${CONTEXT_PATH}/contents/fetcherInsert.do";
        	
            $("#searchForm").attr("action", url);
            $("#searchForm").submit();
        }
       
		
        
	</script>
</head>

<body>
<!-- 1020*708 -->
<div id="popup">
    <!-- <h1>Contents Fetcher</h1> -->

    <div class="popCnt">

        <form name="searchForm" id="searchForm" method="get" action="">
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="searchWord" id="searchWord" value="${condition.searchWord }" /> 
	        
	        <input type="hidden" name="contentSeq" id="contentSeq" /> 
        </form>
        
        <!-- tblForm -->
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Book </strong></th>
                        <td>작업중 </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Lesson</strong></th>
                        <td></td>
                    </tr>

                    <tr>
                        <th><strong class="star">Card Title</strong></th>
                        <td>${cardInfo.prodTitle }</td>
                    </tr>

                    <tr>
                        <th><strong class="star">Thumbnail</strong></th>
                        <td><span class="preview"><img src="${cardInfo.thmbPath }" alt="" /></span></div>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Card Type</strong></th>
                        <td>${cardInfo.cardType }</td>
                    </tr>

                    <tr>
                        <th><strong class="">Skill</strong></th>
                        <td>${cardInfo.cardSkill }</td>
                    </tr>

                    <tr>
                        <th><strong class="">Study Mode</strong></th>
                        <td>${cardInfo.studyMode }</td>
                    </tr>

                    <tr>
                        <th><strong class="">Level of difficulty</strong></th>
                        <td>
                        <c:choose>
                            <c:when test="${cardInfo.cardLevel eq '1' }">★☆☆☆☆</c:when>
                            <c:when test="${cardInfo.cardLevel eq '2' }">★★☆☆☆</c:when>
                            <c:when test="${cardInfo.cardLevel eq '3' }">★★★☆☆</c:when>
                            <c:when test="${cardInfo.cardLevel eq '4' }">★★★★☆</c:when>
                            <c:when test="${cardInfo.cardLevel eq '5' }">★★★★★</c:when>
                        </c:choose>
                        
                    </tr>

                    <tr>
                        <th><strong class="">Time</strong></th>
                        <td><c:if test="${!empty cardInfo.time }">${cardInfo.time }’</c:if></td>
                    </tr>

                    <tr>
                        <th><strong class="">Editable</strong></th>
                        <td>
                        <c:choose>
                            <c:when test="${cardInfo.updAbleYn eq 'Y' }">Yes</c:when>
                            <c:otherwise>No</c:otherwise>
                        </c:choose>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Shareable</strong></th>
                        <td>
                        <c:choose>
                            <c:when test="${cardInfo.openAbleYn eq 'Y' }">Yes</c:when>
                            <c:otherwise>No</c:otherwise>
                        </c:choose>                        
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Keyword</strong></th>
                        <td>${cardInfo.keyword}</td>
                    </tr>

                    <tr>
                        <th><strong class="">Grading</strong></th>
                        <td>
                            <div>
		                        <c:choose>
		                            <c:when test="${cardInfo.grading eq '1' }">By Computer</c:when>
		                            <c:when test="${cardInfo.grading eq '2' }">By Teacher</c:when>
		                            <c:when test="${cardInfo.grading eq '3' }">Mastery learning</c:when>
		                            <c:when test="${cardInfo.grading eq '4' }">Check the per page</c:when>
		                            <c:when test="${cardInfo.grading eq '5' }">Check the last page</c:when>
		                            <c:when test="${cardInfo.grading eq '6' }">after a period of time</c:when>
		                            <c:otherwise>-</c:otherwise>
		                        </c:choose>  
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Direction 1</strong></th>
                        <td>
                            <div>${cardInfo.direcLang1Nm }</div>
                            <p><c:out value="${cardInfo.direc1 }"/></p>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Direction 2</strong></th>
                        <td>
                            <div>${cardInfo.direcLang2Nm }</div>
                            <p><c:out value="${cardInfo.direc2 }"/></p>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Registration date</strong></th>
                        <td>${cardInfo.regDate }</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->

        <div class="btnArea">
            <div class="sideL">
                <a href="javascript: void(0);" onclick="javascript: fetcherList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
            </div>
                    
            <div class="sideR">
                <a href="javascript:void(0);" onclick="javascript: goItem('${cardInfo.contentSeq}');" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit </strong></a>
            </div>
        </div>
    </div>

    <!-- <a href="#" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> -->
</div>

</body>
</html>