<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE } [CONTENTS FETCHER]</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
		$(document).ready(function(){

			
			$("#bookCd").change(function(){
				
			    var url = "${CONTEXT_PATH}/contents/lessonList.do";
	            var pData = {bookCd : $(this).val()};
	            var result = commonServerRequest(url, pData);
		        
	 /*            if(result.result.length <=0){
	            	alert('nothing');
	            } */
	            
	            makeComboBox(result, "lessonCd", "", "Lesson");
	            	
	            
		           
			});
		    
			
			
		});
		
		function itemSearch(){
			
	        $("#searchForm").attr("action", "${CONTEXT_PATH}/contents/fetcherList.do");
	        $("#searchForm").submit();
		}
		
		
       function itemView(itemInfoSeq) {
        	$("#contentSeq").val(itemInfoSeq);
        	
        	var url = "";
        	if(itemInfoSeq == ''){
        	    url = "${CONTEXT_PATH}/contents/fetcherInsert.do";
        	}else{
        		url = "${CONTEXT_PATH}/contents/fetcherView.do";
        	}
        	
            $("#itemForm").attr("action", url);
            $("#itemForm").submit();
        }
       
		
		// 페이징에서 선택시 호출되는 스크립트 
        function goPage(page_no){
            $("#searchForm #pageNo").val(page_no);
            $("#searchForm").submit();
        }
		
        function changeOrderType(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            itemSearch();
        }	
        
        function fetcherList() {
            $("#searchForm").attr("action", "${CONTEXT_PATH}/contents/fetcherList.do");
            $("#searchForm").submit();
        }
        
	</script>
</head>

<body>
<!-- 1020*708 -->
<div id="popup">
    <!-- <h1>Contents Fetcher</h1> -->

    <div class="popCnt">

        <form name="searchForm" id="searchForm" method="get" action="">
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="searchWord" id="searchWord" value="${condition.searchWord }" /> 
        </form>
        <!-- tblForm -->
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Book </strong></th>
                        <td>
                            <select style="width: 250px;" name="bookCd" id="bookCd">
                                <option value="">Book</option>
                               	<c:forEach items="${bookList}" var="item" varStatus="status">
                                    <option value="${item.cd }">${item.cdSubject }</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Lesson</strong></th>
                        <td>
                            <select style="width: 250px;" name="lessonCd" id="lessonCd">
                                <option value="">Lesson</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Card Title</strong></th>
                        <td>
                            <input type="text" class="text" style="width: 500px;" " />
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Thumbnail</strong></th>
                        <td>
                            <div class="logoUpload">
                                <span class="preview"><img src="${CONTEXT_PATH}/images/common/thum.jpg" alt="" /></span>
                                <div>
                                    <a href="#" class="btnBlue"><span>Search</span></a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Card Type</strong></th>
                        <td>
                            <select style="width: 250px;">
                                <option value="">Activity</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Skill</strong></th>
                        <td>
                            <select style="width: 250px;">
                                <option value="">Skill</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Study Mode</strong></th>
                        <td>
                            <select style="width: 250px;">
                                <option value="">Study Mode</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Level of difficulty</strong></th>
                        <td>
                            <select style="width: 250px;">
                                <option value="">Level of difficulty</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Time</strong></th>
                        <td><input type="text" class="text" style="width: 244px;" " /></td>
                    </tr>

                    <tr>
                        <th><strong class="">Editable</strong></th>
                        <td>
                            <input type="radio" class="rdo" name="" /><label for="">Yes</label>
                            <input type="radio" class="rdo" name="" /><label for="">No</label>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Shareable</strong></th>
                        <td>
                            <input type="radio" class="rdo" name="" /><label for="">Yes</label>
                            <input type="radio" class="rdo" name="" /><label for="">No</label>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Keyword</strong></th>
                        <td><input type="text" class="text" style="width: 500px;" " /></td>
                    </tr>

                    <tr>
                        <th><strong class="">Grading</strong></th>
                        <td>
                            <div>
                                <input type="radio" class="rdo" name="" /><label for="">By Computer</label>
                                <input type="radio" class="rdo" name="" /><label for="">By Teacher</label>
                            </div>

                            <div>
                                <input type="radio" class="rdo" name="" /><label for="">Mastery learning</label>
                                <input type="radio" class="rdo" name="" /><label for="">Check the per page</label>
                                <input type="radio" class="rdo" name="" /><label for="">Check the last page</label>
                                <input type="radio" class="rdo" name="" /><label for="">After a period of time</label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Direction 1</strong></th>
                        <td>
                            <div>
                            
                                <select style="width: 250px;">
	                                    <option value="">Nationality</option>
                                	<c:forEach items="${nationList}" var="item" varStatus="status">
	                                    <option value="${item.cd }">${item.cdSubject }</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <p>
                                <textarea style="width: 98%; height: 148px;" class="textarea"></textarea>
                            </p>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Direction 2</strong></th>
                        <td>
                            <div>
                                <select style="width: 250px;">
	                                    <option value="">Nationality</option>
                                    <c:forEach items="${nationList}" var="item" varStatus="status">
	                                    <option value="${item.cd }">${item.cdSubject }</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <p>
                                <textarea style="width: 98%; height: 148px;" class="textarea"></textarea>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->

        <div class="btnArea">
            <div class="sideL">
                <a href="javascript: void(0);" onclick="javascript: fetcherList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
            </div>

            <div class="sideR">
                <a href="javascript: void(0);" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
            </div>
        </div>
    </div>

    <!-- <a href="#" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> -->
</div>

</body>
</html>