<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE } [CONTENTS FETCHER]</title>
	
	<!--################################## common js & css include ################################## --> 
	<%@ include file="../../../include/include.jsp" %>
	<!--//################################## common js & css include ##################################-->
	<script type="text/javascript">
		$(document).ready(function(){


		});
		
		function itemSearch(){
			
	        $("#searchForm").attr("action", "${CONTEXT_PATH}/contents/fetcherList.do");
	        $("#searchForm").submit();
		}
		
		
       function itemView(itemInfoSeq) {
        	$("#contentSeq").val(itemInfoSeq);
        	
        	var url = "";
        	if(itemInfoSeq == ''){
        	    url = "${CONTEXT_PATH}/contents/fetcherInsert.do";
        	}else{
        		url = "${CONTEXT_PATH}/contents/fetcherView.do";
        	}
        	
            $("#searchForm").attr("action", url);
            $("#searchForm").submit();
        }
       
		
		// 페이징에서 선택시 호출되는 스크립트 
        function goPage(page_no){
            $("#searchForm #pageNo").val(page_no);
            $("#searchForm").submit();
        }
		
        function changeOrderType(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            itemSearch();
        }	
        
	</script>
</head>

<body>
<!-- 1020*708 -->
<div id="popup">
    <!-- <h1>Contents Fetcher</h1> -->

    <div class="popCnt">

        <form name="searchForm" id="searchForm" method="get" action="">
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="contentSeq" id="contentSeq" value=""/>
        
        <!-- selectArea -->
        <div class="selectArea">

            <input type="text" class="text" name="searchWord" id="searchWord" value="${condition.searchWord }" placeholder="Book / Card Title"  style="width: 120px;"/>

            <a href="#" class="btnGray" onclick="javascript: itemSearch();"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
            <a href="javascript:void(0);" onclick="javascript: itemView('');" class="btnRed"><span>+ New</span></a>
        </div>
        <!-- //selectArea -->
		</form>

        <form name="itemForm" id="itemForm" method="post" action="">
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="contentSeq" id="contentSeq" value=""/>
        </form>
        <!-- 리스트 타입 -->
        <div class="tbList2">
            <table>
                <colgroup>
                    <col width="6%" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="14%" />
                </colgroup>

                <thead>
                    <tr>
                        <th>No</th>
                        <th>Card Title</th>
                        <th><a href="javascript:changeOrderType('bookNm');">Book
	                        <c:if test="${condition.orderColumn ne 'bookNm'}">
	                        ▲
	                        </c:if>                             
	                        <c:if test="${condition.orderColumn eq 'bookNm'}">
	                        <c:choose>
	                            <c:when test="${condition.sort eq 'ASC'}">
	                                ▲
	                            </c:when>
	                            <c:otherwise>
	                                ▼
	                            </c:otherwise>
	                        </c:choose>
	                        </c:if>                        
                        </a></th>
                        <th><a href="javascript:changeOrderType('lessonNm');">Lesson
	                        <c:if test="${condition.orderColumn ne 'lessonNm'}">
	                        ▲
	                        </c:if>                             
	                        <c:if test="${condition.orderColumn eq 'lessonNm'}">
	                        <c:choose>
	                            <c:when test="${condition.sort eq 'ASC'}">
	                                ▲
	                            </c:when>
	                            <c:otherwise>
	                                ▼
	                            </c:otherwise>
	                        </c:choose>
	                        </c:if>                          
                        </a></th>
                        <th>카드 구분</th>
                        <th><a href="javascript:changeOrderType('regDttm');">Resistration date 
	                        <c:if test="${condition.orderColumn ne 'regDttm'}">
	                        ▲
	                        </c:if>                             
	                        <c:if test="${condition.orderColumn eq 'regDttm'}">
	                        <c:choose>
	                            <c:when test="${condition.sort eq 'ASC'}">
	                                ▲
	                            </c:when>
	                            <c:otherwise>
	                                ▼
	                            </c:otherwise>
	                        </c:choose>
	                        </c:if>                         
                        </a></th>
                        <th>View</th>
                    </tr>
                </thead>

                <tbody>
                    <c:if test="${empty cardList}">
                        <tr>
                            <td colspan="7">조회된 데이터가 없습니다.</td>
                        </tr>
                    </c:if>
                    <c:if test="${!empty cardList}">                
	                <c:forEach items="${cardList}" var="item" varStatus="status">
	                    <tr>
	                        <td>${item.rn }</td>
	                        <td>${item.title }</td>
	                        <td>${item.bookNm }</td>
	                        <td>${item.lessonNm }</td>
	                        <td>${item.testYn }</td>
	                        <td>${item.regDttm }</td>
	                        <td><a href="javascript:void(0);" onclick="javascript: itemView('${item.contentSeq }');" class="btnBlue"><span>view</span></a></td>
	                    </tr>	                
	                </c:forEach>
	                </c:if>                    
                
                </tbody>
            </table>
        </div>
        <!-- //리스트 타입 -->

        <!-- paging -->
        
        <div class="paging">
            <tree:pagination page_no="${condition.pageNo }" total_cnt="${totalCount}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
        </div>
        <!-- //paging -->
    </div>

    <%-- <a href="#" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> --%>
</div>

</body>
</html>