<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	    var redOk = false;
        var checkUnload = false;
        var submit = false; // 등록/수정 여부 값 

        
	    $(document).ready(function() {
            
	        $("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리 
	        
	        $("#campId").keyup(function(){
	        	alphaNumOnlyCheck(this.value);
	        });
	        
	        $("input[type='radio']").click(function(){
	        	var useyn = $(":radio[name=status]:checked").val();
	        	$("#campusForm #useYn").val(useyn);
	        });
	        
	        // set oldValue
        	$("#campusForm :input").each(function(){
        		var input = $(this);
        		var pId = input.attr("id");
        		$("#"+pId).setOldValue();
        	});
        	
	    });
    
	    
        $(window).on("beforeunload", function(){
        	if(!submit){
	        	checkForm();
			    if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";        	
        	}
          
        });
        
        // 수정된 값이 있는지 체크 
        function checkForm(){
        	// checkUnload 초기화 
        	checkUnload = false;
        	// form안의 모든 input을 뺑뺑이 돌려부러.
        	$("#campusForm :input").each(function(){
        		
        		var check = true;
        		var pId = $(this).attr("id");
        		// console.log(pId+"="+$("#"+pId).isEdited());
        		
        		if(check){
        			// 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
        			if($("#"+pId).isEdited()){
        				check = false;
        				checkUnload = true;
        			}
        		}
        	});
        	
        	// radio button은 따로 체크를 한다. 
        	// 이미 수정된 값이 있다면 할 필요가 없소만.
        	if(!checkUnload){
        		
        		if($("#campusForm #useYn").val() != '${campus.useYn}'){
        			checkUnload = true;
        		}
        	}
        	
        	// console.log("checkUnload="+checkUnload);
			
        }
        
		function campusList() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/campusList.do");
			$("#searchForm").submit();
		}
		
		function saveCampus() {
			var campNm = $("#campusForm #campNm").val();
			var campId = $("#campusForm #campId").val();
			var useYn = $(":radio[name=status]:checked").val();
			var campSeq = $("#campusForm #campSeq").val();
			var oldCampusNm = $("#oldCampNm").val();
			
			// Campus Name 입력값 체크.
			if(campNm == null || campNm.length == 0) {
			    alert('Please enter a campus name.');	
//				alert("Campus name을 입력해주세요.")
				return false;
			}
			
			if(!redOk && (oldCampusNm != campNm)){
				alert('Please check double.');
				//alert("Campus name 중복체크를 해주십시요.");
				return false;
			}
			

			// Status 설정값 체크.
			if(useYn == null || useYn.length == 0) {
				alert('Please choose status.');
				//alert("Status를 설정해주세요.")
				return false;
			}
			
	        $("#eventBtn").hide(); // 더블클릭 방지로 이벤트 걸린 버튼 숨김 처리 
	        $("#noEventBtn").show(); // 이벤트 없는 버튼을 보임. 
	        
			var url = campSeq == "" ? "/amg/campusAdd.do" : "/amg/campusModify.do";
			
			$("#campusForm #useYn").val(useYn);
			$("#campusForm").attr("action", "${CONTEXT_PATH}" + url);
			$("#campusForm").attr("method", "POST");
			submit = true;
			$("#campusForm").submit();
		}
		
		// redundantCheck
		function redundantCheck(){
			var campName = $.trim($("#campusForm #campNm").val());
			var insId = $("#campusForm #insInfoSeq").val();
			var campSeq = $("#campusForm #campSeq").val();
			
			
			if(campName == ""){
				alert('Please enter a campus name.');
				//alert("Campus name을 입력해 주세요.");
				$("#campusForm #campNm").focus();
				return false;
			}
			
			// 학원 아이디는 기관별로 화면이 열릴때 상수 값이 저장 되어야 할거 같다. 
			// 테스트를 위해서 일단 6 번에 '비상ESL' 로 테스트 했다. 
			
			
            $.ajax({            
                type : "post",
                url : "${CONTEXT_PATH}/amg/campNameCheck.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                data : { insInfoSeq : insId, campSeq : campSeq,  campNm : campName },
                dataType: 'json',
                success : function(data){
                    var code = data.code;
                    var msg = data.msg;
                    if ( code == "0000" ) {
                    	if(Number(data.message) > 0){
                    		alert("This name has been used.\nPlease use a different name.");
                    		// alert("이미 사용중인 code(또는 name) 입니다.\n다른 code(또는 name) 를 사용해 주십시요.");
                    		redOk = false;
                    	}else{
                    		alert('This name is available.');
                    		//alert("사용 가능한 code입니다.");
                    		redOk = true;
                    	}
                    } 
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                    isLogging = false;
                }  
            });
			
			
		}
		
		
		function alphaNumOnlyCheck(str){
			var err = 0; 
			  
			if( !(event.keyCode == 13) || !(event.keyCode == 8)  || !(event.keyCode == 46)  ){
				 for(var i=0;i<str.length;i++){
				 if(!((str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57) || 
		                   (str.charCodeAt(i) >= 65 && str.charCodeAt(i) <= 90) ||
		                   (str.charCodeAt(i) >= 97 && str.charCodeAt(i) <= 122))){
		                   err = err + 1;
		                  }
		         }
			 }

			if (err > 0) { 
			    alert("you can use numeric and alphabet only"); 
			    
			    var str = $("#campusForm #campId").val();
			    
			    var reStr = str.replace(/[^a-z0-9]/gi,'');
			    $("#campusForm #campId").val(reStr);
			    return;
			} 
		}
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

		<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Campus</h2>
			<a href="javascript:campusList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
		
        <form name="searchForm" id="searchForm" method="GET">
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="useYn" id="useYn" value="${useYn }"/> 
            <input type="hidden" name="searchKey" id="searchKey" value="${searchKey }"/>
        </form>
        
        		
		<form name="campusForm" id="campusForm" method="post" action="">
		<input type="hidden" name="campSeq" id="campSeq" value="${campus.campSeq}"/>
		<input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
		<input type="hidden" name="useYn" id="useYn" value="${campus.useYn}"/>
		<input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
		<input type="hidden" name="oldCampNm" id="oldCampNm" value="${campus.campNm}"/>
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Campus name</strong></th>
						<td>
						<input type="text" class="text" name="campNm" maxlength="20" id="campNm" style="width: 244px;" value="<c:if test="${campus.campNm ne null}">${campus.campNm}</c:if>" />
						  <a href="javascript:void(0);" onClick="javascript: redundantCheck();" class="btnBlue"><span>Redundant Check</span></a>
						</td>
					</tr>

					<tr>
						<th><strong>Campus code</strong></th>
						<td><c:if test="${campus.campId ne null}">${campus.campId}</c:if>
						<c:if test="${empty campus.campId}">Code is automatically generated.</c:if>
						<input type="hidden" class="text" name="campId" id="campId" maxlength="40" style="width: 244px;" value="<c:if test="${campus.campId ne null}">${campus.campId}</c:if>" /></td>
					</tr>

					<tr>
						<th><strong class="">Province</strong></th>
						<td><input type="text" class="text" name="areaNm" id="areaNm" maxlength="10" style="width: 244px;" value="<c:if test="${campus.areaNm ne null}">${campus.areaNm}</c:if>" /></td>
					</tr>

					<tr>
						<th><strong class="">Info</strong></th>
						<td><textarea name="detailInfo" id="detailInfo" style="width: 98%; height: 148px;" class="textarea" maxlength="250" ><c:if test="${campus.detailInfo ne null}">${campus.detailInfo}</c:if></textarea></td>
					</tr>

					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
							<input type="radio" class="rdo" name="status" value="Y" id="A" <c:if test="${campus.useYn eq 'Y' }"> checked </c:if> /><label for="A">Active</label>
	                        <input type="radio" class="rdo" name="status" value="N" id="B" <c:if test="${campus.useYn eq 'N' }"> checked </c:if> /><label for="B">Inactive</label>
						</td>
					</tr>
					<c:if test="${campus.regDate ne null}">
					<tr>
						<th><strong class="star">Registration date</strong></th>
						<td>${campus.regDate}</td>
					</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript: saveCampus()" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
				<a href="javascript: void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>