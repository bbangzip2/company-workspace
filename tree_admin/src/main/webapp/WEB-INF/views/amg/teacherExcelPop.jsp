<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>[ Teacher ] Excel Import</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
	
	<script type="text/javascript">
	
	function closePop(){
		window.close();
	}
	
	function uploadExcel() {
    	var file = $("#uploadFile").val();
    	var fileExt = file.slice(file.indexOf(".") +1).toLowerCase();
    	
    	if(file == ''){
    		alert("Please choose upload file.");
    		return false;
    	}
    	
    	if(fileExt != "xlsx" && fileExt  != "xls"){
    		alert("Please choose Excel Format file.");
    		return false;
    		
    	}
    	
	    $("#itemForm").attr("action", "${CONTEXT_PATH}/amg/teacherExcelUpload.do");
        $("#itemForm").submit();
	}
	
	</script>
</head>
<body>



<form name="itemForm" id="itemForm" method="POST" enctype="multipart/form-data"> 
<div id="popup">
    <!-- <h1>[ Teacher ] Excel Import</h1> -->

    <div id="cnt">
        <p class="popTxt">Please upload the file after filling in the excel file<br/>based on the sample format.</p>

        <div class="fileUpload">
            <input type="file" id="uploadFile" name="uploadFile" class="inputFile" style="width: 400px;" />
        </div>

        <div class="btnArea">
            <div class="sideL">
                <a href="${CDN_URL}/excel/teacher.xlsx" class="btnGray"><span>Download the excel sample.</span></a>
            </div>

            <div class="sideR">
                <a href="javascript:void(0);" onclick="javascript:uploadExcel();" class="btnRed"><span>Upload</span></a>
            </div>
        </div>
    </div>

    <%-- <a href="javascript:closePop();" class="close"><img src="${CONTEXT_PATH }/images/popup/close.gif" alt="close" /></a> --%>
</div>
</form>

</body>
</html>