<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

    <script type="text/javascript">
        $(document).ready(function() {
            
            // 콤보 리스트를 만든다.
            var camp = ${campList};
            var prog = ${progList};
            var clss = ${clssList};
            if (camp.result != undefined && camp.result != "") {
                makeStudentBox(camp, "itemForm #campSeq", "${condition.campSeq}", "Campus");
            }
            if (prog.result != undefined && prog.result != "") {
                makeStudentBox(prog, "itemForm #progSeq", "${condition.progSeq}", "Program");    
            } 
            if (clss.result != undefined && clss.result != "") {
                makeStudentBox(clss, "itemForm #clsSeq", "${condition.clsSeq}", "Class");    
            }
            
            // 캠퍼스 리스트가 바뀔 때 프로그램 콤보리스트를 요청한다.
            $("#itemForm #campSeq").change(function(event) {
                var insSeq  = ${condition.insSeq};
                var campSeq = $("select[name=campSeq]").val();
                getProgramCombo("${CONTEXT_PATH}", [insSeq, campSeq], "itemForm #progSeq", "", "Program");
                $("#itemForm #clsSeq").html("<option value=''>Class</option>");
            });
            
            // 프로그램 리스트가 바뀔 때 클래스 콤보리스트를 요청한다.
            $("#itemForm #progSeq").change(function(event) {
                var insSeq  = ${condition.insSeq};
                var campSeq = $("select[name=campSeq]").val();
                var progSeq = $("select[name=progSeq]").val();
                getClassCombo("${CONTEXT_PATH}", [insSeq, campSeq, progSeq], "itemForm #clsSeq", "", "Class");
            });
            
            $("#itemForm #searchKey").click(function(event) {
                $(this).select();
            });
            
            $("#itemForm #searchKey").keypress(function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    searchList();
                }
            });
            
            // searchColumn 값이 아무것도 넘어 오지 않으면 디폴트로 NAME이 선택된다.
            var column = '<c:out value="${condition.searchColumn}"/>';
            if (column == undefined || column == "") {
                $("input[name=stype][value=NAME]").attr('checked', 'checked');
            }
            
        });
        
        function getCampusCombo(contextPath, params, targetId, defaultValue, optionTag) {
            $.ajax({            
                type : "post",
                url : contextPath+"/amg/studentCampusList.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                data : {intSeq : params[0]},
                dataType: 'json',
                success : function(data){
                    makeStudentBox(data, targetId, defaultValue, optionTag)
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                    isLogging = false;
                }  
            });
        }
        
        function getProgramCombo(contextPath, params, targetId, defaultValue, optionTag) {
            $.ajax({            
                type : "post",
                url : contextPath+"/amg/studentProgramList.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                data : {intSeq : params[0], campSeq : params[1]},
                dataType: 'json',
                success : function(data){
                    makeStudentBox(data, targetId, defaultValue, optionTag)
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                    isLogging = false;
                }  
            });
        } 
        
        function getClassCombo(contextPath, params, targetId, defaultValue, optionTag){
            $.ajax({            
                type : "post",
                url : contextPath+"/amg/studentClassList.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                data : {intSeq : params[0], campSeq : params[1], progSeq : params[2]},
                dataType: 'json',
                success : function(data){
                    makeStudentBox(data, targetId, defaultValue, optionTag)
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                    isLogging = false;
                }  
            });
        }
        
        function makeStudentBox(data, targetId, defaultValue, optionTag) {
            var option = data.result.length == 1 ? "" : optionTag;
            makeComboBox(data, targetId, defaultValue, option);
        }
        
        function studentPop(id) {
            var popOption = "width=750, height=830, resizable=no, scrollbars=no, status=no;";
            var studentPop = window.open("about:blank", "studentPop", popOption);
            studentPop.focus();
            $("#itemForm").attr("target", "studentPop");
            $("#itemForm #mbrId").val(id);
            $("#itemForm").attr("action", "${CONTEXT_PATH}/amg/studentPop.do");
            $("#itemForm").submit();
        }
        
        function studentView(id) {
            /* $("#itemForm #mbrId").val(id);
            $("#itemForm").attr("target", "");
            $("#itemForm").attr("action", "${CONTEXT_PATH}/amg/studentView.do");
            $("#itemForm").submit(); */
            
            var campSeq = $("select[name=campSeq]").val();
            var progSeq = $("select[name=progSeq]").val();
            var clsSeq  = $("select[name=clsSeq]").val();
            var searchKey = $("#itemForm #searchKey").val().trim();
            var searchColumn = $(":radio[name=stype]:checked").val();
            var startDate = $("#itemForm #sDate").val();
            var endDate = $("#itemForm #eDate").val();
            var useYn = $("select[name=useYn]").val();
            
            $("#searchForm #mbrId").val(id);
            $("#searchForm #campSeq").val(campSeq);
            $("#searchForm #progSeq").val(progSeq);
            $("#searchForm #clsSeq").val(clsSeq);
            $("#searchForm #searchColumn").val(searchColumn);
            $("#searchForm #searchKey").val(searchKey);
            $("#searchForm #regStartDate").val(startDate);
            $("#searchForm #regEndDate").val(endDate);
            $("#searchForm #useYn").val(useYn);
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/studentView.do");
            $("#searchForm").submit();
        }
        
        function studentInsert() {
            $("#itemForm").attr("target", "");
            $("#itemForm").attr("action", "${CONTEXT_PATH}/amg/studentInsert.do");
            $("#itemForm").submit();
        }
        
        function searchList() {
            var campSeq = $("select[name=campSeq]").val();
            var progSeq = $("select[name=progSeq]").val();
            var clsSeq  = $("select[name=clsSeq]").val();
            var searchKey = $("#itemForm #searchKey").val().trim();
            var searchColumn = $(":radio[name=stype]:checked").val();
            var startDate = $("#itemForm #sDate").val();
            var endDate = $("#itemForm #eDate").val();
            var useYn = $("select[name=useYn]").val();
            
            $("#searchForm #campSeq").val(campSeq);
            $("#searchForm #progSeq").val(progSeq);
            $("#searchForm #clsSeq").val(clsSeq);
            $("#searchForm #searchColumn").val(searchColumn);
            $("#searchForm #searchKey").val(searchKey);
            $("#searchForm #regStartDate").val(startDate);
            $("#searchForm #regEndDate").val(endDate);
            $("#searchForm #useYn").val(useYn);
            
            goPage(1);
        }
        
        // 페이징에서 선택시 호출되는 스크립트 
        function goPage(page_no){
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/studentList.do");
            $("#searchForm #pageNo").val(page_no);
            $("#searchForm").submit();
        }
        
        function openExcelPop(){
            var w = 500;
            var h = 180;
            var left = (screen.width/2)-(w/2);
            var top  = (screen.height/2)-(h/2);
            var option = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, width="+w+", height="+h+", top="+top+", left="+left+", resizable=no, copyhistory=no";

            var url = "${CONTEXT_PATH}/amg/studentExcelPop.do";
              
            var pop = window.open(url, 'excel' , option);
            pop.focus();
        }
    </script>
</head>

<body>
<div id="wrapper">
    <!-- header -->
    <%@ include file="../include/incHeader.jsp" %>
    <!-- //header -->
    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Student</h2>
            <a href="javascript:openExcelPop();" class="btn btnGray"><span><img src="${CONTEXT_PATH }/images/common/icon/excel.png" alt="" />Excel import</span></a>
        </div>
        
        <form name="searchForm" id="searchForm" method="GET"> 
            <input type="hidden" name="mbrId" id="mbrId" value=""/>
            <input type="hidden" name="campSeq" id="campSeq" value="${condition.campSeq}"/>
            <input type="hidden" name="progSeq" id="progSeq" value="${condition.progSeq}"/>
            <input type="hidden" name="clsSeq" id="clsSeq" value="${condition.clsSeq}"/>
            <input type="hidden" name="searchColumn" id="searchColumn" value="${condition.searchColumn}"/>
            <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}"/>
            <input type="hidden" name="regStartDate" id="regStartDate" value="${condition.regStartDate}"/> 
            <input type="hidden" name="regEndDate" id="regEndDate" value="${condition.regEndDate}"/> 
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}"/> 
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
            <input type="hidden" name="pageSize" id="pageSize" value="${condition.pageSize}"/>
        </form>
        
        <form name="itemForm" id="itemForm" method="GET">
        
            <input type="hidden" name="mbrId" id="mbrId"/>
            <input type="hidden" name="searchColumn" id="searchColumn" value="${condition.searchColumn}"/>
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
            <input type="hidden" name="pageSize" id="pageSize" value="${condition.pageSize}"/>
        
        <!-- selectArea -->
        <div class="selectAreaM">
            <ul>
                <li>
                    <strong class="label">&middot; Class Info</strong>
                    <select name="campSeq" id="campSeq">
                        <option value="">Campus</option>
                    </select>

                    <select name="progSeq" id="progSeq">
                        <option value="">Program</option>
                    </select>

                    <select name="clsSeq" id="clsSeq">
                        <option value="">Class</option>
                    </select>
                </li>

                <li>
                    <strong class="label">&middot; Student Info</strong>

                    <input type="radio" name="stype" class="rdo" value="NAME" <c:if test="${condition.searchColumn eq 'NAME' }">checked</c:if>/>
                    <label for="">Name</label>

                    <input type="radio" name="stype" class="rdo" value="MBR_ID" <c:if test="${condition.searchColumn eq 'MBR_ID' }">checked</c:if> />
                    <label for="">ID</label>

                    <input type="radio" name="stype" class="rdo" value="NICKNAME" <c:if test="${condition.searchColumn eq 'NICKNAME' }">checked</c:if> />
                    <label for="">Nickname</label>

                    <input type="radio" name="stype" class="rdo" value="EMAIL" <c:if test="${condition.searchColumn eq 'EMAIL' }">checked</c:if> />
                    <label for="">Email</label>

                    <input type="text" class="text" name="searchKey" id="searchKey" style="width: 300px" value="${condition.searchKey}"/>
                </li>

                <li>
                    <strong class="label">&middot; Registration date</strong>
                    <span class="datePicker">
                        <input type="text" name="startDate" id="sDate" class="text datepicker" style="width: 80px" value="${condition.regStartDate}"/>
                        ~
                        <input type="text" name="endDate" id="eDate" class="text datepicker" style="width: 80px" value="${condition.regEndDate}"/>
                    </span>

                    <strong class="label">&middot; Status</strong>
                    <select name="useYn" id="useYn">
                        <option value=""  <c:if test="${condition.useYn eq ''}"> selected </c:if>>Status</option>
                        <option value="Y" <c:if test="${condition.useYn eq 'Y'}"> selected </c:if>>Active</option>
                        <option value="N" <c:if test="${condition.useYn eq 'N'}"> selected </c:if>>Inactive</option>
                    </select>
                </li>
            </ul>

            <div class="btn">
                <a href="javascript:searchList()" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
                <a href="javascript:studentInsert()" class="btnRed"><span>+ New</span></a>
            </div>
        </div>
        <!-- //selectArea -->

        <!-- 리스트 타입 -->
        <div class="tbList2">
            <table>
                <colgroup>
                    <col width="" />
                    <col width="12%" />
                    <col width="12%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="8%" />
                    <col width="10%" />
                    <col width="8%" />

                </colgroup>

                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th colspan="3">Basic Info</th>
                        <th colspan="3">Class Info</th>
                        <th colspan="2">Condition</th>
                        <th rowspan="2">View</th>
                    </tr>

                    <tr class="secLine">
                        <th>Name</th>
                        <th>ID</th>
                        <th>Nickname</th>

                        <th>Campus</th>
                        <th>Program</th>
                        <th>Class</th>

                        <th>Status</th>
                        <th>Member</th>
                    </tr>
                </thead>

                <tbody>
                    <c:if test="${empty studentList}">
                            <tr>
                                <td colspan="10">No data found.</td>
                            </tr>
                    </c:if>
                    <c:if test="${!empty studentList}">
                        <c:forEach items="${studentList}" var="studentItem" varStatus="status">
                        <c:set var="row" value="${fn:length(studentItem.classInfo)} "/>
                        <tr>
                            <td rowspan="${row}">${studentItem.rn}&nbsp;</td>
                            <td rowspan="${row}">
                                <a href="javascript:studentPop('${studentItem.mbrId}')" style="text-decoration:underline;">${studentItem.name}<br/>(${studentItem.sexSect})&nbsp;</a>
                            </td>
                            <td rowspan="${row}">${studentItem.mbrId}&nbsp;</td>
                            <td rowspan="${row}">${studentItem.nickName}&nbsp;</td>
                            <td>${studentItem.classInfo[0].campNm}&nbsp;</td>
                            <td>${studentItem.classInfo[0].progNm}&nbsp;</td>
                            <td>${studentItem.classInfo[0].classNm}&nbsp;</td>
                            <td rowspan="${row}">
                                <c:choose>
                                    <c:when test="${studentItem.useYn eq 'Y'}">
                                        Active
                                    </c:when>
                                    <c:otherwise>
                                        Inactive
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td rowspan="${row}">${studentItem.sect}&nbsp;</td>
                            <td rowspan="${row}">
                                <a href="javascript:studentView('${studentItem.mbrId}')" class="btnBlue"><span>View</span></a>
                            </td>
                        </tr>
                        <c:forEach items="${studentItem.classInfo}" var="clsItem" varStatus="clsStatus">
                        <c:if test="${clsStatus.count > 1}">
                        <tr>
                            <td>${clsItem.campNm}&nbsp;</td>
                            <td>${clsItem.progNm}&nbsp;</td>
                            <td>${clsItem.classNm}&nbsp;</td>
                        </tr>
                        </c:if>
                        </c:forEach>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
        </div>
        <!-- //리스트 타입 -->
        
        <div class="paging">
            <tree:pagination page_no="${condition.pageNo}" total_cnt="${condition.totalCount}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
        </div>
    </form>
    </div></div>
</div>
</body>
</html>