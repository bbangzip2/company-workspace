<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${MAIN_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		var idCheck = false;
		var orgId = '<c:out value="${insInfo.id}"/>';
		
		var isSaveBtn = false;
		var checkUnload = false;
        $(window).on("beforeunload", function(){
        	checkForm();
            if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
        });
		
		$(document).ready(function() {
			$("#id").bind("change paste keyup", function(event) {
				var inputId = $(this).val();
				if(orgId != inputId) {
				    idCheck = false;
				} else {
				    idCheck = true;
				}
				
    			checkUnload = true;
			});
			
	        $("input[type='radio']").click(function(){
	        	var useyn = $(":radio[name=status]:checked").val();
	        	$("#viewForm #useYn").val(useyn);
	        });
	        
			
	        // set oldValue
        	$("#viewForm :input").each(function(){
        		var input = $(this);
        		var pId = input.attr("id");
        		$("#"+pId).setOldValue();
        	});
	        
		});
		
		
        // 수정된 값이 있는지 체크 
        function checkForm(){
            if (isSaveBtn) {
                checkUnload = false;
                return;
            }
        	// checkUnload 초기화 
        	checkUnload = false;
        	// form안의 모든 input을 뺑뺑이 돌려부러.
        	$("#viewForm :input").each(function(){
        		
        		var check = true;
        		var pId = $(this).attr("id");
        		 console.log(pId+"="+$("#"+pId).isEdited());
        		
        		if(check){
        			// 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
        			if($("#"+pId).isEdited()){
        				check = false;
        				checkUnload = true;
        				console.log("1111111111111111   -> " + pId);
        			}
        		}
        	});
        	
        	// radio button은 따로 체크를 한다. 
        	// 이미 수정된 값이 있다면 할 필요가 없소만.
        	if(!checkUnload){
        		if($("#viewForm #useYn").val() != '${insInfo.useYn}'){
        		    console.log("22222222222222");
        			checkUnload = true;
        		}
        	}
        	
        	 console.log("checkUnload="+checkUnload);
        }
        
        
		function infoModify() {
		    var seq = $("#insInfoSeq").val();
			var id = $("#id").val();
			var name = $("#name").val();
			var nation = $("#nation").val();
			var status = $(':radio[name=status]:checked').val();
			
			if(isNullString(name)) {
				alert("Please enter a Name.");
				return;
			}
			
		    if(isNullString(id)) {
				alert("Please enter a Code.");
				return;
			}
		    
		    if(orgId != id && !idCheck) {
                alert("Please check double.");
                return;
            }
			
			if(isNullString(nation)) {
				alert("Please choose Nation.");
				return;
			}
			
			if(isNullString(status)) {
				alert("Please choose Status.");
				return;
			}
				
			$("#viewForm").attr("action", "${CONTEXT_PATH}/amg/instituteModify.do");
			$("#viewForm").attr("method", "POST");
			$("#useYn").val($(':radio[name="status"]:checked').val());
			$("#insInfoSeq").val(seq);
			
			isSaveBtn = true;
			
			$("#viewForm").submit();
		}
		
		function checkId() {
        	var id = $.trim($("#id").val());
        	
        	if(isNullString(id)) {
        		alert("Please enter a Code.");
        		return;
        	}
        	
        	if(id == orgId) {
    			alert("This ID has already existed.");
    			return;
    		}
        	
        	if (checkInsCode(id)) {
        	    alert("4~10 characters with letters(a-z.) This isn't case-sensitive.");
        	    return;
        	}
        	
            $.ajax({            
                type : "post",
                url : "${CONTEXT_PATH}/amg/checkInstituteCode.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                datatype : "json",
                data : {id : id},
                success : function(data){
                    var code = data.code;
                    var msg = data.message;
                    if ( code == "0000" ) {
                    	if(msg == "0") {
                    		alert("This code is available.");
                    		idCheck = true;
                    	} else {
                    		alert("This code the been used. Please use a different code.");
                    	}
                    }
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                }
            });
        }
		
		function isNullString(str) {
			if(str == undefined || str == "") {
				return true;
			} else {
				return false;
			}
		}
		
		function checkInsCode(text) {
		    // var regexp = /[0-9a-zA-Z]/; // 숫자,영문
            // var regexp = /[0-9a-zA-Z.;\-]/; // 숫자,영문,특수문자
            // var regexp = /[0-9]/; // 숫자만
            var regexp = /[a-zA-Z]/; // 영문만
            for( var i = 0; i < text.length; i++){
                if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){
                 /* alert(text.charAt(i) + "는 입력불가능한 문자입니다");
                 break; */
                 return true;
                }
            }
            return false;
        }
		
		function clickUploadFile() {
            $("#uploadFile").trigger('click');
        }
		
		function insList() {
		    window.opener.reloadPage();
            self.close();
        }
		
		function previewImage(targetObj, profile) {
		    checkUnload = true;
            var ua = window.navigator.userAgent;

            if (ua.indexOf("MSIE") > -1) {//ie일때
            } else { //ie가 아닐때
                var files = targetObj.files;
                for ( var i = 0; i < files.length; i++) {

                    var file = files[i];

                    if (window.FileReader) { // FireFox, Chrome, Opera 확인.
                        var reader = new FileReader();
                        reader.onloadend = (function(aImg) {
                            return function(e) {
                                /* aImg.src = e.target.result; */
                                $("#logoImage").attr('src', e.target.result);
                            };
                        })($("#logoImage"));
                        reader.readAsDataURL(file);
                        
                    } 
                }
            }
        }
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
        <c:choose>
        <c:when test="${empty insInfo}">
            <%@ include file="../include/incAdmHeader.jsp" %>
        </c:when>
        <c:otherwise>
            <%@ include file="../include/incHeader.jsp" %>
        </c:otherwise>
    </c:choose>
	
	<!-- //header -->

		<div id="container" class="infoView"  <c:if test='${empty insInfo}'> style='padding-top: 64px;'</c:if>  >
        <div id="contents">
		<div class="hGroup">
			<h2>Institute Information</h2>
            <%-- <c:choose>
                <c:when test="${empty insInfo}">
                <a href="javascript:history.back(-1);" class="btn btnGray">
                </c:when>
                <c:otherwise>
                <a href="javascript:insList();" class="btn btnGray">
                </c:otherwise>
            </c:choose> 
            <span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span>
            </a> --%>
		</div>
		
		<form name="viewForm" id="viewForm" method="post" action="" enctype="multipart/form-data">
        
		<input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${insInfo.seq}"/>
		<input type="hidden" name="orgId" id="orgId" value="${insInfo.id}"/>
		<input type="hidden" name="useYn" id="useYn" value="${insInfo.useYn}"/>

		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Institute name</strong></th>
						<td><input type="text" class="text" name="name" id="name" style="width: 244px;" value="${insInfo.name}" /></td>
					</tr>

					<tr>
						<th><strong class="star">Institute code</strong></th>
						<td>
                            <c:choose>
                                <c:when test="${readOnlyInsCode eq 'N'}">
                                    <input type="text" class="text" name="id" id="id" style="width: 244px;" value="${insInfo.id}" maxLength="10"/>
                                    <a href="javascript:checkId()" class="btnBlue"><span>Redundant Check</span></a>
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden" name="id" id="id" value="${insInfo.id}"/>
                                    ${insInfo.id}
                                </c:otherwise>
                            </c:choose>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Nationality</strong></th>
						<td>
							<select name="nation" id="nation" style="width: 250px;">
                                <option value="">Nationality</option>
								<c:forEach items="${nationList}"  var="nationItem"  varStatus="status">
									<c:choose> 
									<c:when test="${nationItem.cd eq insInfo.nationCd}"> 
										<option value="${nationItem.cd}" selected>${nationItem.cdSubject}</option>
									</c:when>
									<c:otherwise> 
									    <option value="${nationItem.cd}">${nationItem.cdSubject}</option>
									</c:otherwise> 
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Logo</strong></th>
						<td>
                            <div id="profile" class="insLogo">
                                <c:choose>
                                    <c:when test="${insInfo.logoPath eq null || insInfo.logoPath eq ''}">
                                        <img id="logoImage" src="${CONTEXT_PATH}/images/common/no-image0.png" alt="" class="thum"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, insInfo.logoPath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
                                <div>
                                    <p>(Size : 252 * 72  Pixel)</p>
                                    <a class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
                                    <input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'profile')" style="display:none"/>
                                </div>
                            </div>
						</td>
					</tr>

					<tr>
						<th><strong class="">Information</strong></th>
						<td><textarea name="info" id="info" style="width: 98%; height: 148px;" class="textarea"><c:if test="${!empty insInfo.info}">${insInfo.info}</c:if></textarea></td>
					</tr>

					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
							<input type="radio" class="rdo" name="status" value="Y" <c:if test="${insInfo.useYn eq 'Y'}"> checked</c:if>/><label for="">Active</label>
							<input type="radio" class="rdo" name="status" value="N" <c:if test="${insInfo.useYn eq 'N'}"> checked</c:if>/><label for="">Inactive</label>
						</td>
					</tr>
                    <c:if test="${!empty insInfo}">
    					<tr>
    						<th><strong class="star">Registration date</strong></th>
    						<td>${insInfo.regDate}</td>
    					</tr>
                    </c:if>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:infoModify()" id="btnSave" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
			</div>
		</div>
		</form>
	</div>
    </div>
</div>
</body>
</html>