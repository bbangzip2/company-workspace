<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
	    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    
    	<script type="text/javascript">
		$(document).ready(function() {
		    <c:choose>
		      <c:when test="${mbrGrade== mbrGradeDefineSuperAdmin}">
				   var mbrGrade = "super";		
		       </c:when>
		       <c:when test="${mbrGrade == mbrGradeDefineInstituteAdmin}">
		   		   var mbrGrade = "institute";   
		       </c:when>
		       <c:when test="${mbrGrade == mbrGradeDefineCampus}">
		 		   var mbrGrade = "campus";   
		       </c:when>
		    </c:choose>
		    
		    var searchUserType = "${userType}";
		    
		    if(searchUserType == 1 || searchUserType == 0 || searchUserType == "") {
		    	$('#selectbox2 *').remove();
		    	$("#selectbox2").append("<option value='0'>Campus</option>");
		    	$('#selectbox2').attr('disabled',true);

		    } else if (searchUserType == 2 ){
		    	getCampusList(${instituteSeq},'${sector}')
		    	
		    }
	    	
	    	$('#selectbox1').change(function(){
			 	selectVal =  $('#selectbox1 option:selected').val();
   		    	if ((mbrGrade == "super"  || mbrGrade == "institute"  ) && (selectVal =="1" || selectVal =="0") ){
   		    		$('#selectbox2 *').remove();
   		    		$("#selectbox2").append("<option value='0'>Campus</option>");
   		    		$('#selectbox2').attr('disabled',true);
   		    	} else if ((mbrGrade == "super"  || mbrGrade == "institute") && selectVal =="2" ) {
   		    		getCampusList(${instituteSeq},'')
   		    	} else if (mbrGrade == "campus"){
   		    		//alert("학원/campus")
   		    	}	
			 });
		});
		
		function searchList() {
			$("#adminForm #pageNo").val(1);
			$("#adminForm").attr("action", "${CONTEXT_PATH}/amg/adminSearch.do");
			$("#adminForm").submit();
		}
		
		function getList(orderBy) {
			var orgSort = $("#sort").val();
			var orgOrderBy = $("#orderBy").val();
			var sort = "ASC";
			if(orgOrderBy == orderBy) {
				if(orgSort == "ASC") {
					sort = "DESC";
				} else {
					sort = "ASC";
				}
			}
			$("#orderBy").val(orderBy);
			$("#sort").val(sort);
			$("#adminForm").attr("action", "${CONTEXT_PATH}/amg/adminList.do");
			$("#adminForm").submit();
		}

		function adminEdit(iname,mbrId) {
			$("#iname").val(escape(iname));
			$("#mbrId").val(mbrId);
			$("#adminForm").attr("action", "${CONTEXT_PATH}/amg/adminView.do");
			$("#adminForm").submit();
		}
		
		// 페이징에서 선택시 호출되는 스크립트 
		function goPage(page_no){
			$("#adminForm #pageNo").val(1);
			$("#adminForm #pageNo").val(page_no);
			$("#adminForm").submit();
		}
		
		function makeComboBoxAdmin(data, targetId, defaultValue, optionTag){
			var option = "";
			var result = data.result;
			
			// 첫번째 옵션을 선택 할수 있도록 처리한다. 
			if( (optionTag != undefined ) && (optionTag != '')){
				option += "<option value=''>"+optionTag+"</option>";
			}
			
			for(i=0; i< result.length; i++){
				var sel = "";
				if(defaultValue == result[i].cd){
					sel = "selected";
				}
				option +="<option value='"+result[i].cd+"' "+sel+">"+result[i].cdSubject+"</option>"
			}
			$("#"+targetId).html(option);
		} 
		
		function getCampusList(instituteSeq, campusCode){
			$.ajax({            
			     type : "post",
			     url : "${CONTEXT_PATH}/adminGetCampus.do",
			     beforeSend: function(req) {
			         req.setRequestHeader("Accept", "application/json");
			     },
			     async : false,
			     cache : false,
			     datatype : "json",
			     data : {instituteSeq : ${instituteSeq}},
			     success : function(data){
			     var code = data.code;
			     	$('#selectbox2 *').remove();
					$('#selectbox2').attr('disabled',false);
			         makeComboBoxAdmin(data, "selectbox2", campusCode, "Campus");
			     },
			     error: function (xhr, ajaxOptions, thrownError){
			         alert("error");
			     }, 
			     complete:function (xhr, textStatus){
			     }  
         	}); 
		}
		
		function b64(s) {
			  var key = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';


			  var i = 0, len = s.length,
			      c1, c2, c3,
			      e1, e2, e3, e4,
			      result = [];
			 
			  while (i < len) {
			    c1 = s.charCodeAt(i++);
			    c2 = s.charCodeAt(i++);
			    c3 = s.charCodeAt(i++);
			   
			    e1 = c1 >> 2;
			    e2 = ((c1 & 3) << 4) | (c2 >> 4);
			    e3 = ((c2 & 15) << 2) | (c3 >> 6);
			    e4 = c3 & 63;
			   
			    if (isNaN(c2)) {
			      e3 = e4 = 64;
			    } else if (isNaN(c3)) {
			      e4 = 64;
			    }
			   
			    result.push(e1, e2, e3, e4);
			  }
			 
			  return result.map(function (e) { return key.charAt(e); }).join('');
			}

		
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->
	<div id="container" class="infoView"><div id="contents">
		<form name="adminForm" id="adminForm" method="GET" action="">
		<input type="hidden" name="mbrId" id="mbrId" value="${mbrId}"/>
		<input type="hidden" name="orderBy" id="orderBy" value="${condition.orderBy}" />
		<input type="hidden" name="sort" id="sort" value="${condition.sort}" />
		<input type="hidden" name="iname" id="iname" />
		<input type="hidden" name="mbrGrade" id="mbrGrade" />
		<input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
		<input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${insInfoSeq}"/> 
	
		<div class="hGroup">
			<h2>Admin</h2>
		</div>
		<!-- selectArea -->
	
		<div class="selectArea">
	   <select id="selectbox1" name="userType">
		   	<option value="0">UserType</option>
				<c:choose>
			      <c:when test="${mbrGrade== mbrGradeDefineSuperAdmin}">
					<option value="1" <c:if test="${userType eq '1'}">selected</c:if> >Institute Admin</option>
					<option value="2" <c:if test="${userType eq '2'}">selected</c:if>>Campus Admin</option>
			       </c:when>
			       <c:when test="${mbrGrade == mbrGradeDefineInstituteAdmin}">
					<option value="1" <c:if test="${userType eq '1'}">selected</c:if>>Institute Admin</option>
					<option value="2" <c:if test="${userType eq '2'}">selected</c:if>>Campus Admin</option>
			       </c:when>
			       <c:when test="${mbrGrade == mbrGradeDefineCampus}">
	          		<option value="2" <c:if test="${userType eq '2'}">selected</c:if> >Campus Admin</option>
			       </c:when>
			   	</c:choose>
		   	</select>
			
			<select id="selectbox2" name="sector">
			</select>

			<select id="selectbox3" name="useYn">
			 	<option value="">--Status--</option>
			 	<option value="Y" <c:if test="${useYn eq 'Y'}">selected</c:if>>Active</option>
			 	<option value="N" <c:if test="${useYn eq 'N'}">selected</c:if>>InActive</option>
			</select>

			<input type="text" class="text" name="searchKey" id="searchKey"  style="width: 120px;"   value="${searchKey}" />

			<a href="javascript:searchList();" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
			<a href="${CONTEXT_PATH}/adminInsert.do" class="btnRed"><span>+ New</span></a>
		</div>
		<!-- //selectArea -->

		<!-- 리스트 타입 -->
		<div class="tbList1">
			<table>
				<colgroup>
					<col width="6%" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="6%" />
				</colgroup>

				<thead>
					<tr>
						<th>NO</th>
						<th><a href="javascript:getList('USERTYPE')">User Type <c:if test="${orderColumn eq 'USERTYPE'}">
						<c:choose>
							<c:when test="${sort eq 'ASC'}">
								▲
							</c:when>
							<c:otherwise>
								▼
							</c:otherwise>
						</c:choose>
						</c:if></a></th>
						<th>ID</th>
						<th>Campus</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Status</th>
						<th><a href="javascript:getList('REGDT')">Registration date
						<c:if test="${condition.orderBy eq 'REGDT'}">
						<c:choose>
							<c:when test="${condition.sort eq 'ASC'}">
								▲
							</c:when>
							<c:otherwise>
								▼
							</c:otherwise>
						</c:choose>
						</c:if>
						</a></th>
						<th>View</th>
					</tr>
				</thead>

				<tbody>
					<c:if test="${empty adminList}">
						<tr>
							<td colspan="9">No data found.</td>
						</tr>
					</c:if>

					<c:if test="${!empty adminList}">
						<c:forEach items="${adminList}" var="adminItem" varStatus="status">
							<tr>
								<td>${adminItem.rnum}</td>
								<td class="aLeft">
							  <c:choose>
									<c:when test="${adminItem.mbrGrade eq 'MG100'}">
										Super Admin
									</c:when>
									<c:when test="${adminItem.mbrGrade eq 'MG121'}">
										Institute Admin
									</c:when>
									<c:otherwise>
										Campus Admin
									</c:otherwise>
								</c:choose>
								</td>
								<td>${adminItem.mbrId}</td>
								<td>${adminItem.iname}</td>
								<td>${adminItem.nm}</td>
								<td>${adminItem.telNo}</td>
								<td>
								<c:choose>
									<c:when test="${adminItem.useYn eq 'Y'}">
										<span class="txtRed">Active</span>
									</c:when>
									<c:otherwise>
										<span class="txtBlue">Inactive</span>
									</c:otherwise>
								</c:choose>
								</td>
								<td>${adminItem.regDttm}</td>
								
								<td>
								<c:if test="${adminItem.mbrGrade ne 'MG100'}">
								<a href="javascript:adminEdit('${adminItem.iname}','${adminItem.mbrId}' )" class="btnBlue"><span>
								View
								</span></a>
								</c:if>
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //리스트 타입 -->

		<div class="paging">
			<tree:pagination page_no="${condition.pageNo }" total_cnt="${adminList[0].totCnt}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
		</div>
	</form>
	</div></div>
</div>
</body>
</html>