<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		function teacherEdit() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/teacherEdit.do");
			$("#searchForm").submit();
		}
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Teacher</h2>
			<a href="javascript:history.back(-1);" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
        
        <form name="searchForm" id="searchForm" method="GET"> 
            <input type="hidden" name="mbrId" id="mbrId" value="${teacher.mbrId}"/>
            <input type="hidden" name="campSeq" id="campSeq" value="${condition.campSeq}"/>
            <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}"/>
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}"/> 
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
        </form>
		
		<form name="teacherForm" id="teacherForm" method="post" action="">
		<input type="hidden" name="mbrId" id="mbrId" value="${teacher.mbrId}"/>
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">ID</strong></th>
						<td>${teacher.mbrId}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Name</strong></th>
						<td>${teacher.name}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="">Image</strong></th>
						<td>
							<div class="profile">
                            <c:choose>
                                <c:when test="${teacher.profilePhotopath eq null || teacher.profilePhotopath eq ''}">
                                    <img src="${CONTEXT_PATH}/images/common/no-image_2.png" alt="" class="thum" />
                                </c:when>
                                <c:otherwise>
    								<img src="${tree:thumbnailPath('Y',CONTEXT_PATH, teacher.profilePhotopath)}" alt="" class="thum" />
                                </c:otherwise>
                            </c:choose>
							</div>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Nickname</strong></th>
						<td>${teacher.nickName}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">E-mail</strong></th>
						<td>${teacher.email}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Male/Female</strong></th>
						<td>
                            <c:if test="${teacher.sexSect eq 'M'}">Male</c:if><c:if test="${teacher.sexSect eq 'F'}">Female</c:if>&nbsp;
                        </td>
					</tr>

					<tr>
						<th><strong class="star">Cell-phone</strong></th>
						<td>${teacher.telNo}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="">Memo</strong></th>
						<td>
                            <c:if test="${!empty teacher.memo}">
                                <c:set var="newline" value="<%= \"\n\" %>" />
                                ${fn:replace(teacher.memo, newline, '<br/>')}
                            </c:if>&nbsp;
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">Lecture Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Campus</strong></th>
						<td>
						<c:forEach items="${teacher.campList}" var="campusItem" varStatus="status">
							<c:if test="${status.count ne 1}">, </c:if>
							${campusItem.campNm}
						</c:forEach>&nbsp;
						</td>
					</tr>

					<tr>
						<th><strong class="">Lecture history</strong></th>
						<td>

							<table class="career">
								<thead>
									<tr>
										<th>Campus</th>
										<th>Class</th>
										<th>Period</th>
										<th>Program</th>
										<th>Note</th>
									</tr>
								</thead>

								<tbody>
<%-- 									<c:forEach items="${teacher.campList}" var="campusItem" varStatus="campStatus">
                                    <c:set var="row" value="${fn:length(campusItem.classList)} "/>
                                    <tr>
                                        <td rowspan="${row}">${campusItem.campNm}&nbsp;</td>
                                        <td>${campusItem.classList[0].classNm}&nbsp;</td>
                                        <td>${campusItem.classList[0].startYmd} ~ ${campusItem.classList[0].endYmd}&nbsp;</td>
                                        <td>${campusItem.classList[0].progNm}&nbsp;</td>
                                        <td>${campusItem.classList[0].useYn}&nbsp;</td>
                                    </tr>
                                    <c:forEach items="${campusItem.classList}" var="classItem" varStatus="clsStatus">
                                        <c:if test="${clsStatus.count > 1}">
                                            <tr>
                                                <td>${classItem.classNm}&nbsp;</td>
                                                <td>${classItem.startYmd} ~ ${classItem.endYmd}&nbsp;</td>
                                                <td>${classItem.progNm}&nbsp;</td>
                                                <td>${classItem.useYn}&nbsp;</td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
									</c:forEach> --%>
                                    <c:forEach items="${teacher.classList}" var="classItem" varStatus="classStatus">
                                    <tr>
                                        <td>${classItem.campNm}&nbsp;</td>
                                        <td>${classItem.classNm}&nbsp;</td>
                                        <td>${classItem.startYmd} ~ ${classItem.endYmd}&nbsp;</td>
                                        <td>${classItem.progNm}&nbsp;</td>
                                        <td>${classItem.useYn}&nbsp;</td>
                                    </tr>
                                    </c:forEach>
								</tbody>
							</table>

							<!-- //리스트 타입 -->
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">Other Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="">Status</strong></th>
						<td>
							<c:choose>
								<c:when test="${teacher.useYn eq 'Y'}">
									<span class="txtRed">Active</span>
								</c:when>
								<c:otherwise>
									<span class="txtBlue">Inactive</span>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>

					<tr>
						<th><strong class="">Last Update</strong></th>
						<td>${regMember.name}(${regMember.mbrId}) / ${teacher.modDate}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:teacherEdit()" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
			</div>
		</div>
	</form>
	</div></div>
</div>
</body>
</html>