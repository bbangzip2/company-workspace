<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
	
	<script type="text/javascript">
		<c:set var="nowYear" value="<%=Calendar.getInstance().get(Calendar.YEAR)%>"/>
		var orgId = '<c:out value="${student.mbrId}"/>';
		var orgCampSeq = '<c:out value="${student.classInfo[0].campSeq}"/>';
		var idCheck = false;
		var isSaveBtn = false;
		var birthYear;
		var birthMonth;
		
		var checkUnload = false;
		$(window).on("beforeunload", function(){
		    checkForm();
            if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
        });
		
		$(document).ready(function() {
/* 			$("#birthYear").change(function() {
				birthYear = $(this).val();
				refreshDay();
			});
			
			$("#birthMonth").change(function() {
				birthMonth = $(this).val();
				refreshDay();
			}); */
			
			$("#mbrId").bind("change paste keyup", function(event) {
				var inputId = $(this).val();
				if(orgId != inputId) {
				    idCheck = false;
				} else {
				    idCheck = true;
				}
				
				checkUnload = true;
			});
			
			$("input:radio[name=rdoSexSect]").click(function(){
			    var sex = $("input:radio[name=rdoSexSect]:checked").val();
                $("#itemForm #sexSect").val(sex);
            });
			
			$("input:radio[name=rdoUseYn]").click(function(){
			    var useyn = $("input:radio[name=rdoUseYn]:checked").val();
                $("#itemForm #useYn").val(useyn);
            });
			
			// set oldValue
            $("#itemForm :input").each(function(){
                var input = $(this);
                var pId = input.attr("id");
                $("#"+pId).setOldValue();
            });
		});
		
        // 수정된 값이 있는지 체크 
        function checkForm(){
            if (isSaveBtn) {
                checkUnload = false;
                return;
            }
            
            // checkUnload 초기화 
            checkUnload = false;
            // form안의 모든 input을 뺑뺑이 돌려부러.
            $("#itemForm :input").each(function(){
                
                var check = true;
                var pId = $(this).attr("id");
                 console.log(pId+"="+$("#"+pId).isEdited());
                
                if(check){
                    // 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
                    if($("#"+pId).isEdited()){
                        check = false;
                        checkUnload = true;
                    }
                }
            });
            
            console.log("checkUnload="+checkUnload);
        }
		
/* 		function refreshDay() {
			$("#birthDay option").remove();
			var appendHtmlStr = "<option value=''>Day</option>";
			
			if(birthYear != undefined && birthMonth != undefined) {
				var date = new Date(birthYear, birthMonth, 0);
				var i = 0;
				while(i < date.getDate()) {
					appendHtmlStr += "<option value='" + (i+1) + "'>" + (i+1) + "</option>";
					i++;
				}
			}
			
			$("#birthDay").append(appendHtmlStr);
		} */
		
		// 날짜 유효성 체크
		function isValidDate(year, month, day) {
		    if (month < 1 || month > 12) {
		        return false;
		    }
		    
		    if (day < 1 || day > 31) {
		        return false;
		    }
		    
		    // 마지막 일이 31일 체크
		    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
		        return false
		    }
		    
		    // 윤달 체크
		    if (month == 2) { 
		        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		        if (day > 29 || (day == 29 && !isleap)) {
		            return false;
		        }
		    }

		    return true;
		}
		
		function studentList() {
		    $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/studentList.do");
            $("#searchForm").submit();
		}
		
		function studentSave() {
			var mbrId      = $("#itemForm #mbrId").val();
			var pwd        = $("#itemForm #pwd").val();
			var pwdConfirm = $("#itemForm #pwdConfirm").val();
			var name       = $("#itemForm #name").val();
			var nickname   = $("#itemForm #nickName").val();
			var email      = $("#itemForm #email").val();
			var telNo      = $("#itemForm #telNo").val();
			var sexSect    = $("input:radio[name=rdoSexSect]:checked").val();
			var useYn      = $("input:radio[name=rdoUseYn]:checked").val();
			var campSeq    = $("#itemForm #campSeq").val();
			var year       = $("#itemForm #birthYear").val();
            var month      = $("#itemForm #birthMonth").val();
            var day        = $("#itemForm #birthDay").val();

            // 아이디 체크
            if(isNullString(mbrId)) {
                alert("Please enter a ID.");
                return;
            } else if(orgId != mbrId && !idCheck) {
                alert("Please check double.");
                return;
            }
            
            // 신규 가입일 때만 체크한다.
            if (orgId == "") {
                // 패스워드 체크
                if(pwd != undefined && pwd != pwdConfirm) {
                    alert("The passwords you typed don't match.");
                    return;
                } else if(isNullString(pwd)) {
                    alert("Please enter a Password.");
                    return;
                } else if (isValidPassword(pwd)) {
                    alert("4~12 characters with letters(a-z), numbers, and special letters.");
                    return;
                }
            }

            // 이름 체크
            if(isNullString(name)) {
                alert("Please enter a Name.");
                return;
            }
			
            // 별명 체크
            if(isNullString(nickname)) {
                alert("Please enter a Nickname.");
                return;
            }
			
            // 이메일 체크
            if(isNullString(email)) {
                alert("Please enter an Email.");
                return;
            } else if(!checkEmail(email)) {
                alert("This is not a valid type Email.");
                return;
            }
			
            // 성별 체크
            if(isNullString(sexSect)) {
                alert("Please choose Sex");
                return;
            }
			
            // 날짜 유효성 검사
			if (isValidDate(year, month, day)) {
                month = month < 10 ? "0" + month : month;
                day = day < 10 ? "0" + day : day;
                $("#itemForm #bymd").val(year + "." + month + "." + day);
			}
            
			// 전화번호 유효성 검사
		    if(checkPhoneNum(telNo)) {
		        // $("#itemForm #telNo").val("");
		        alert("This is not a valid type Phone Number.")
                return;
            }
			
		    // 캠퍼스 체크			
			if(isNullString(campSeq)) {
			    alert("Please choose Campus");
				return;
			}
		    
            // 상태값을 체크
            if(isNullString(useYn)) {
                alert("Please choose Status");
                return;
            }
			
			// 캠퍼스 순번이 다르면 기존것은 지우고 선택된 것을 입력한다.
			if (campSeq != orgCampSeq) {
			    $("#itemForm #delCampSeq").val(orgCampSeq);
			    $("#itemForm #addCampSeq").val(campSeq);
			}
			
			var url = orgId == "" ? "/amg/studentAdd.do" : "/amg/studentModify.do";
			
			$("#itemForm #sexSect").val(sexSect);
            $("#itemForm #useYn").val(useYn);
			$("#itemForm").attr("action", "${CONTEXT_PATH}" + url);
			$("#itemForm").attr("method", "POST");

			isSaveBtn = true;
			
			$("#itemForm").submit();
		}
		
		function checkEmail(email) {
			var format = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
			return format.test(email);
		}
		
		function checkId() {
        	var id = $.trim($("#itemForm #mbrId").val());
        	
        	if(id == undefined || id == "") {
        		alert("Please enter a ID.");
        		return;
        	}
        	
        	if(id == orgId) {
    			alert("This ID has already existed.")
    			return;
    		}
        	
        	if (isValidId(id)) {
                return;
            }
        	
            $.ajax({            
                type : "post",
                url : "${CONTEXT_PATH}/member/checkMemberId.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                datatype : "json",
                data : {userId : id},
                success : function(data){
                    var code = data.code;
                    var msg = data.message;
                    if ( code == "0000" ) {
                    	if(msg == "0") {
                    		alert("This ID is available.");
                    		idCheck = true;
                    	} else {
                    		alert("This ID has been used. Please use a different ID.");
                    	}
                    }
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                }  
            });
            
        }
		
		function isNullString(str) {
			if(str == undefined || str == "") {
				return true;
			} else {
				return false;
			}
		}
		
		function clickUploadFile() {
            $("#uploadFile").trigger('click');
      }
		
		function previewImage(targetObj, profile) {
		    checkUnload = true;
	          var ua = window.navigator.userAgent;

	          if (ua.indexOf("MSIE") > -1) {//ie일때
	          } else { //ie가 아닐때
	              var files = targetObj.files;
	              for ( var i = 0; i < files.length; i++) {

	                  var file = files[i];

	                  if (window.FileReader) { // FireFox, Chrome, Opera 확인.
	                      var reader = new FileReader();
	                      reader.onloadend = (function(aImg) {
	                          return function(e) {
	                              /* aImg.src = e.target.result; */
	                              $("#logoImage").attr('src', e.target.result);
	                          };
	                      })($("#logoImage"));
	                      reader.readAsDataURL(file);
	                      
	                  } 
	              }
	          }
	      }
		
		function sendEmailToPwd() {
	           //alert(${student.mbrId});
	           var pUrl = "${CONTEXT_PATH}/mail/sendUserPwdMail.do";
	           var pData = {mbrId: '${condition.mbrId}'};
	           var rst ; // return result data

	           $.ajax({
	               type : "post",
	               url : pUrl,
	               beforeSend : function(req) {
	                   req.setRequestHeader("Accept", "application/json");
	               },
	               async : false,
	               cache : false,
	               data : pData,
	               dataType : 'json',
	               success : function(data) {
	                   var code = data.code;
	                   var msg = data.message;
	                   if (code == "0000") {
	                      // return data;
	                      alert(msg);
	                   }
	               },
	               error : function(xhr, ajaxOptions, thrownError) {
	                   alert("error");
	               },
	               complete : function(xhr, textStatus) {
	                   isLogging = false;
	               }
	           });
	           return rst;
	       }
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Student</h2>
			<a href="javascript:studentList()" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
        
        <form name="searchForm" id="searchForm" method="GET"> 
            <input type="hidden" name="mbrId" id="mbrId" value="${condition.mbrId}"/>
            <input type="hidden" name="campSeq" id="campSeq" value="${condition.campSeq}"/>
            <input type="hidden" name="progSeq" id="progSeq" value="${condition.progSeq}"/>
            <input type="hidden" name="clsSeq" id="clsSeq" value="${condition.clsSeq}"/>
            <input type="hidden" name="searchColumn" id="searchColumn" value="${condition.searchColumn}"/>
            <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}"/>
            <input type="hidden" name="regStartDate" id="regStartDate" value="${condition.regStartDate}"/> 
            <input type="hidden" name="regEndDate" id="regEndDate" value="${condition.regEndDate}"/> 
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}"/> 
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
            <input type="hidden" name="pageSize" id="pageSize" value="${condition.pageSize}"/>
        </form>
        
		<form name="itemForm" id="itemForm" action="" enctype="multipart/form-data">
		<input type="hidden" name="useYn" id="useYn" value="${student.useYn}"/>
		<input type="hidden" name="sexSect" id="sexSect" value="${student.sexSect}"/>
		<input type="hidden" name="bymd" id="bymd"/>
		<input type="hidden" name="addCampSeq" id="addCampSeq"/>
        <input type="hidden" name="delCampSeq" id="delCampSeq"/>
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">ID</strong></th>
						<td>
							<c:choose>
                                <c:when test="${empty student.mbrId}">
                                    <input type="text" name="mbrId" id="mbrId" class="text" style="width: 140px;" maxLength="15"/>
                                    <a href="javascript:checkId()" class="btnBlue"><span>Redundant Check</span></a>
                                    <br/><div style="padding:4px 0px 0px 0px ;">Have 15 or fewer characters. Contain basic alphabet letters or numbers only. Not contain special characters.</div>
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden" name="mbrId" id="mbrId" value="${student.mbrId}"/>
                                    ${student.mbrId}
                                </c:otherwise>
                            </c:choose>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Password</strong></th>
						<td>
                            <c:choose>
                                <c:when test="${!empty student}">
                                    <a href="javascript:sendEmailToPwd();" class="btnBlue"><span>extra password</span></a>
                                </c:when>
                                <c:otherwise>
                                    <input type="password" name="pwd" id="pwd" class="text" style="width: 140px;" value="" />
                                    <label for="" class="type1 star">Password check</label> <input type="password" name="pwdConfirm" id="pwdConfirm" class="text" style="width: 140px;" value="" />
                                    <br/><div style="padding:4px 0px 0px 0px ;">4~12 characters with letters(a-z), numbers, and special letters</div>
                                </c:otherwise>
                            </c:choose>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Name</strong></th>
						<td><input type="text" name="name" id="name" class="text" style="width: 140px;" value="${student.name}" /></td>
					</tr>

					<tr>
						<th><strong class="">Image</strong></th>
						<td>
                            <div id="profile" class="profile">
                                <c:choose>
                                    <c:when test="${student.profilePhotopath eq null || student.profilePhotopath eq ''}">
                                        <img id="logoImage" src="${CONTEXT_PATH}/images/common/no-image_2.png" class="thum"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, student.profilePhotopath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
                                <a class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
                                <input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'profile')" style="display:none"/>
                            </div>
                        </td>
					</tr>

					<tr>
						<th><strong class="star">Nickname</strong></th>
						<td><input type="text" name="nickName" id="nickName" class="text" style="width: 140px;" value="${student.nickName}" /></td>
					</tr>

					<tr>
						<th><strong class="star">E-mail</strong></th>
						<td><input type="text" name="email" id="email" class="text" style="width: 244px;" value="${student.email}" /></td>
					</tr>

					<tr>
						<th><strong class="star">Male/Female</strong></th>
						<td>
							<input type="radio" class="rdo" name="rdoSexSect" value="M" <c:if test="${student.sexSect eq 'M' }">checked</c:if> /><label for="">Male</label>
							<input type="radio" class="rdo" name="rdoSexSect" value="F" <c:if test="${student.sexSect eq 'F' }">checked</c:if> /><label for="">Female</label>
						</td>
					</tr>

					<tr>
						<th><strong class="">Date of birth</strong></th>
						<td>
                        <!-- 생일 값이 2012.07.07 으로 들어오기 때문에 문자를 자른다. -->
                            <c:set var="birthday" value="${student.birthday}" />
                            <c:set var="yy" value="${fn:substring(birthday, 0, 4)}" />
                            <c:set var="mm" value="${fn:substring(birthday, 5, 7)}" />
                            <c:set var="dd" value="${fn:substring(birthday, 8, 10)}" />
                        
							<select name="birthMonth" id="birthMonth" >
								<option value="">Month</option>
								<c:forEach var="month" begin="1" end="12" varStatus="status">
									<option value="${month}" <c:if test="${mm eq month}"> selected</c:if> >${month}</option>
								</c:forEach>
							</select>

							<select name="birthDay" id="birthDay" >
								<option value="">Day</option>
                                <c:forEach var="day" begin="1" end="31" varStatus="status">
                                    <option value="${day}" <c:if test="${dd eq day}"> selected</c:if> >${day}</option>
                                </c:forEach>
							</select>
                            
                            <select name="birthYear" id="birthYear" >
                                <option value="">Year</option>
                                <c:forEach var="inc" begin="${0}" end="${100}" >
                                    <option value="${nowYear-inc}" <c:if test="${yy eq (nowYear-inc)}"> selected</c:if> >${nowYear-inc}</option>
                                </c:forEach>
                            </select>
						</td>
					</tr>

					<tr>
						<th><strong class="">Cell-phone</strong></th>
						<td><input type="text" name="telNo" id="telNo" class="text" style="width: 244px;" value="${student.telNo}" /></td>
					</tr>

					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
							<input type="radio" class="rdo" name="rdoUseYn" value="Y" <c:if test="${student.useYn eq 'Y' }">checked</c:if>/><label for="">Active</label>
							<input type="radio" class="rdo" name="rdoUseYn" value="N" <c:if test="${student.useYn eq 'N' }">checked</c:if>/><label for="">Inactive</label>
						</td>
					</tr>
<!-- 					<tr>
						<th><strong class="star">Member</strong></th>
						<td>
							<input type="radio" class="rdo" name="stat" value="1" /><label for="">Registered mdmbers</label>
							<input type="radio" class="rdo" name="stat" value="2" /><label for="">Regular member</label>
							<input type="radio" class="rdo" name="stat" value="3" /><label for="">Suspended members</label>
							<input type="radio" class="rdo" name="stat" value="4" /><label for="">Withdrew members</label>
						</td>
					</tr> -->
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">Institute Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Institute</strong></th>
						<td>${institute.name}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Campus</strong></th>
						<td>
							<select name="campSeq" id="campSeq" style="width: 248px;" <c:if test="${disabledCampusCombo eq 'Y'}">disabled="true"</c:if> >
                                <c:if test="${fn:length(campusList) ne 1 }">
								<option value="" <c:if test="${empty student.classInfo[0].campSeq}"> selected </c:if>>Campus</option>
                                </c:if>
								<c:forEach items="${campusList}"  var="campusItem"  varStatus="status">
									<c:choose> 
										<c:when test="${campusItem.cd eq student.classInfo[0].campSeq}"> 
											<option value="${campusItem.cd}" selected>${campusItem.cdSubject}</option>
										</c:when>
										<c:otherwise> 
										    <option value="${campusItem.cd}">${campusItem.cdSubject}</option>
										</c:otherwise> 
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->
        <c:if test="${!empty student}">
    		<h3 class="tit">Registration History</h3>
    		<!-- tblForm -->
    		<div class="tblForm">
    			<table>
    				<colgroup>
    					<col width="125px" />
    					<col width="" />
    				</colgroup>
    
    				<tbody>
    					<tr>
    						<th><strong class="">Registration date</strong></th>
    						<td><c:if test="${student.regDate ne ''}">${student.regDate}&nbsp;</c:if></td>
    					</tr>
                        <c:if test="${!empty student.quitDate}">
        					<tr>
        						<th><strong class="">Withdraw Date</strong></th>
        						<td>${student.quitDate}&nbsp;</td>
        					</tr>
                        </c:if>
    				</tbody>
    			</table>
    		</div>
        </c:if>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:studentSave()" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>