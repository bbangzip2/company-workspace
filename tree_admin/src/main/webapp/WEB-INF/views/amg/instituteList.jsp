<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <title>${MAIN_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

    <script type="text/javascript">
        $(document).ready(function() {
            
            $("#viewForm #searchKey").click(function(event) {
                $(this).select();
            });
            
            $("#viewForm #searchKey").keypress(function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    searchList();
                }
            });
        });
    
        function changeViewType(type) {
            // 검색 조건 초기화
            $("#nation").val("Nationality").attr("selected", "selected");
            $("#useYn").val("Status").attr("selected", "selected");
            $("#searchKey").val("");
            
            $("#viewForm").attr("action", "${CONTEXT_PATH}/amg/instituteList.do");
            $("#viewType").val(type);
            $("#viewForm").submit();
        }
        
        function changeOrderType(orderBy) {
            var oldOrderBy = $("#orderColumn").val();
            var oldSort = $("#sort").val();
            var sort = "ASC";
            if(oldOrderBy == orderBy) {
                if(oldSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }

            $("#orderColumn").val(orderBy);
            $("#sort").val(sort);
            $("#viewForm").attr("action", "${CONTEXT_PATH}/amg/instituteList.do");
            $("#viewType").val("listType");
            $("#viewForm").submit();
        }
        
        // 새로운 창으로 기관 정보 페이지를 열어준다. 
        function openInstituteInfo(id) {
            var insPop = window.open("${CONTEXT_PATH}/amg/instituteInfo.do?insInfoSeq=" + id, "insttuteInfo");
            insPop.focus();
        }
        
        function searchList() {
            $("#viewForm").attr("action", "${CONTEXT_PATH}/amg/instituteList.do");
            $("#viewForm").submit();
        }
        
        function insertInstitute() {
            $("#viewForm").attr("action", "${CONTEXT_PATH}/amg/instituteInsert.do");
            $("#viewForm").submit();
        }
        
        function reloadPage() {
            searchList();
        }
        
    </script>
</head>

<body>
<form name="viewForm" id="viewForm" method="GET" action="">
<div id="wrapper">
    <!-- header -->
    <%@ include file="../include/incAdmHeader.jsp" %>
    <!-- //header -->

    <div id="container"><div id="contents">
        
        <input type="hidden" name="viewType" id="viewType" value="${viewType}"/>
        <input type="hidden" name="orderColumn" id="orderColumn" value="${condition.orderColumn}"/>
        <input type="hidden" name="sort" id="sort" value="${condition.sort}"/>
        
        <!-- viewtype -->
        <div class="viewtype">
            <a href="javascript:changeViewType('thumType')" class="thumType<c:if test="${viewType eq 'thumType'}"> on</c:if>">thumnail type</a>
            <a href="javascript:changeViewType('listType')" class="listType<c:if test="${viewType eq 'listType'}"> on</c:if>">list type</a>
        </div>
        <!-- //viewtype -->

        <!-- selectArea -->
        <c:if test="${viewType eq 'listType'}">
        <div class="selectArea">
            <select name="nation" id="nation">
                <option value="">Nationality</option>
                <c:forEach items="${nationList}"  var="nationItem"  varStatus="status">
                    <c:choose> 
                    <c:when test="${nationItem.cd eq condition.nationCd}"> 
                        <option value="${nationItem.cd}" selected>${nationItem.cdSubject}</option>
                    </c:when>
                    <c:otherwise> 
                        <option value="${nationItem.cd}">${nationItem.cdSubject}</option>
                    </c:otherwise> 
                    </c:choose>
                </c:forEach>
            </select>

            <select name="useYn" id="useYn">
                <option value="" <c:if test="${condition.useYn eq ''}">selected</c:if>>Status</option>
                <option value="Y" <c:if test="${condition.useYn eq 'Y'}">selected</c:if>>Active</option>
                <option value="N" <c:if test="${condition.useYn eq 'N'}">selected</c:if>>Inactive</option>
            </select>

            <input type="text" class="text" name="searchKey" id="searchKey" style="width: 120px;" placeholder="Institute" value="${condition.searchKey}" />

            <a href="javascript:searchList()" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
            <a href="javascript:insertInstitute()" class="btnRed"><span>+ New</span></a>
        </div>
        </c:if>
        <!-- //selectArea -->



        <c:choose>
            <c:when test="${viewType eq 'thumType'}">
                <!-- 썸네일 타입 -->
                <div class="thumList">
                    <ul>
                        <c:forEach items="${insList}"  var="insItem"  varStatus="status">
                             <li>
                                <a href="javascript:openInstituteInfo(${insItem.seq})">
                                <c:choose>
                                    <c:when test="${insItem.logoPath eq null || insItem.logoPath eq ''}">
                                        <img src="${CONTEXT_PATH}/images/common/no-image0.png" alt="" class="thum"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, insItem.logoPath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
                                </a>
                                <strong>${insItem.name}</strong>
                            </li>                       
                        </c:forEach>
                        <li>
                            <a href="javascript:insertInstitute()"><img src="${CONTEXT_PATH}/images/common/insAdd.png" alt="" class="thum"/></a>
                        </li>
                    </ul>
                </div>
                <!-- //썸네일 타입 -->
            </c:when>
            <c:otherwise>
                <!-- 리스트 타입 -->
                <div class="tbList1">
                    <table>
                        <colgroup>
                            <col width="6%" />
                            <col width="" />
                            <col width="12%" />
                            <col width="13%" />
                            <col width="9%" />
                            <col width="8%" />
                            <col width="11%" />
                            <col width="6%" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th><a href="javascript:changeOrderType('NAME')">Institute
                                <c:if test="${condition.orderColumn ne 'NAME'}">
                                ▲
                                </c:if> 
                                <c:if test="${condition.orderColumn eq 'NAME'}">
                                <c:choose>
                                    <c:when test="${condition.sort eq 'ASC'}">
                                        ▲
                                    </c:when>
                                    <c:otherwise>
                                        ▼
                                    </c:otherwise>
                                </c:choose>
                                </c:if>
                                </a></th>
                                <th>Code</th>
                                <th><a href="javascript:changeOrderType('CD_SUBJECT')">Nationality
                                <c:if test="${condition.orderColumn ne 'CD_SUBJECT'}">
                                ▲
                                </c:if> 
                                <c:if test="${condition.orderColumn eq 'CD_SUBJECT'}">
                                <c:choose>
                                    <c:when test="${condition.sort eq 'ASC'}">
                                        ▲
                                    </c:when>
                                    <c:otherwise>
                                        ▼
                                    </c:otherwise>
                                </c:choose>
                                </c:if>
                                </a></th>
                                <th>Campus</th>
                                <th>Status</th>
                                <th><a href="javascript:changeOrderType('REG_DATE')">Registration date
                                <c:if test="${condition.orderColumn ne 'REG_DATE'}">
                                ▲
                                </c:if> 
                                <c:if test="${condition.orderColumn eq 'REG_DATE'}">
                                <c:choose>
                                    <c:when test="${condition.sort eq 'ASC'}">
                                        ▲
                                    </c:when>
                                    <c:otherwise>
                                        ▼
                                    </c:otherwise>
                                </c:choose>
                                </c:if>
                                </a></th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <c:if test="${empty insList}">
                                <tr>
                                    <td colspan="8">No data found.</td>
                                </tr>
                        </c:if>
                        <c:if test="${!empty insList}">
                        <tbody>
                            <c:forEach items="${insList}"  var="insItem"  varStatus="status">
                                 <tr>
                                    <%-- <td>${status.index + 1}</td> --%>
                                    <td>${insItem.rn}</td>
                                    <td class="aLeft">
                                        <!-- 74 50 -->
                                        <c:choose>
                                            <c:when test="${insItem.logoPath eq null || insItem.logoPath eq ''}">
                                                <img src="${CONTEXT_PATH}/images/common/no-image0.png" alt="" class="thum" style="width:74px;height:50px;"/>
                                            </c:when>
                                            <c:otherwise>
                                                <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, insItem.logoPath)}" alt="" class="thum" style="width:74px;height:50px;"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <strong class="txtBlue">${insItem.name}</strong>
                                    </td>
                                    <td>${insItem.id}</td>
                                    <td>${insItem.nationNm}</td>
                                    <td>${insItem.campCnt}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${insItem.useYn == 'Y'}">
                                                Active
                                            </c:when>
                                            <c:otherwise>
                                                Inactive
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>${insItem.regDate}</td>
                                    <td><a href="javascript:openInstituteInfo(${insItem.seq})" class="btnBlue"><span>View</span></a></td>
                                </tr>                      
                            </c:forEach>
                        </tbody>
                        </c:if>
                        
                    </table>
                </div>
                <!-- //리스트 타입 -->
            </c:otherwise>
        </c:choose>
        

    </div></div>

</div>
</form>
</body>
</html>