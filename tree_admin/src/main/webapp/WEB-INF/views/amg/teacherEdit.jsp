<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	    var newCampArray = new Array();
	    var orgCampArray = new Array();
	    // 수정모드 일 때 기본 캠퍼스 리스트를 저장한다.
    	<c:forEach items="${teacher.campList}" var="camp" varStatus="campStatus">
    	    orgCampArray.push('${camp.campSeq}');
        </c:forEach>
            orgCampArray.sort();
        
        var isSaveBtn = false;
		var idCheck = false;
		var orgId = '<c:out value="${teacher.mbrId}"/>';
		
		var checkUnload = false;
        $(window).on("beforeunload", function(){
            checkForm();
            if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
        });
        
		$(document).ready(function() {
			$("#mbrId").bind("change paste keyup", function(event) {
				var inputId = $(this).val();
				if(orgId != inputId) {
					idCheck = false;
				} else {
					idCheck = true;
				}
				
				checkUnload = true;
			});
		    
			$("input:radio[name=rdoSexSect]").click(function(){
                var sex = $("input:radio[name=rdoSexSect]:checked").val();
                $("#itemForm #sexSect").val(sex);
            });
            
            $("input:radio[name=rdoUseYn]").click(function(){
                var useyn = $("input:radio[name=rdoUseYn]:checked").val();
                $("#itemForm #useYn").val(useyn);
            });
			
			// set oldValue
            $("#itemForm :input").each(function(){
                var input = $(this);
                var pId = input.attr("id");
                $("#"+pId).setOldValue();
            });
			
		});
		
        // 수정된 값이 있는지 체크 
        function checkForm(){
            if (isSaveBtn) {
                checkUnload = false;
                return;
            }
            
            // checkUnload 초기화 
            checkUnload = false;
            // form안의 모든 input을 뺑뺑이 돌려부러.
            $("#itemForm :input").each(function(){
                
                var check = true;
                var pId = $(this).attr("id");
                 console.log(pId+"="+$("#"+pId).isEdited());
                
                if(check){
                    // 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
                    if($("#"+pId).isEdited()){
                        check = false;
                        checkUnload = true;
                    }
                }
            });
            
            // 체크박스는 따로 체크한다.
            if (!checkUnload) {
                var checkboxarray = [];
                // 체크박스에 선택되어 있는 것들을 배열에 넣는다.
                $("input[name='campusCheck']:checkbox:checked").each(function(index){
                    checkboxarray.push($(this).val());
                });
                checkboxarray.sort();
                if (orgCampArray.join(",") != checkboxarray.join(",")) {
                    checkUnload = true;
                }
            }
            
            console.log("checkUnload="+checkUnload);
        }
		
		function checkboxChange() {
		    $("#campusNm").text(""); // 텍스트에 입력된 기존 선택 캠퍼스 리스트를 초기화
            var campusCodeArray = new Array();
            var campusNmArray = new Array(); // 선택 한 캠퍼스의 코드/명을 배열에 저장
            $("input[name='campusCheck']:checkbox:checked").each(function(index){
                campusCodeArray.push($(this).val());
                campusNmArray.push($(this).next('label').text());
            });
            $("#campusNm").text(campusNmArray.join(', ')); // 배열에 저장된 캠퍼스 명을 텍스트에 입력하여 노출
            
            //checkUnload = true;
		}
		
		function teacherList() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/teacherList.do");
			$("#searchForm").attr("method", "GET");
			$("#searchForm").submit();
		}

		function teacherSave() {
			var mbrId = $("#itemForm #mbrId").val();
			var pwd = $("#itemForm #pwd").val();
			var pwdConfirm = $("#itemForm #pwdConfirm").val();
			var name = $("#itemForm #name").val();
			var nickname = $("#itemForm #nickName").val();
			var email = $("#itemForm #email").val();
			var telNo = $("#itemForm #telNo").val();
			var sexSect = $("input:radio[name=rdoSexSect]:checked").val();
			var useYn = $("input:radio[name=rdoUseYn]:checked").val();
			
			// 아이디 체크
	        if(isNullString(mbrId)) {
                alert("Please enter a ID.");
                return;
            } else if(orgId != mbrId && !idCheck) {
                alert("Please check double.");
                return;
            }
			
			//  신규 가입일 때만 체크한다.
			if (orgId == "") {
    			// 패스워드 체크
     			if(pwd != undefined && pwd != pwdConfirm) {
    				alert("The passwords you typed don't match.");
    				return;
    			} else if(isNullString(pwd)) {
    				alert("Please enter a Password.");
    				return;
    			} else if (isValidPassword(pwd)) {
                    //alert("4~12 characters with letters(a-z), numbers, and special letters.");
                    return;
                }
			}
			
			// 이름 체크
			if(isNullString(name)) {
				alert("Please enter a Name.");
				return;
			}
			
			// 별명 체크
			if(isNullString(nickname)) {
				alert("Please enter a Nickname.");
				return;
			}
			
			// 이메일 체크
			if(isNullString(email)) {
				alert("Please enter an Email.");
				return;
			} else if(!checkEmail(email)) {
				alert("This is not a valid type Email.");
				return;
			}
			
	        // 성별 체크
            if(isNullString(sexSect)) {
                alert("Please choose Sex");
                return;
            }
	         
			// 전화번호 체크
			if(isNullString(telNo)) {
				alert("Please enter a Phone Number.")
				return;
			} else if(checkPhoneNum(telNo)) {
				alert("This is not a valid type Phone Number.")
				return;
			}
			
            // 넣고 빼야할 캠퍼스를 정리한다.
            parseCampus();
			// 캠퍼스가 아무것도 없는지 체크
			if (isNullString(newCampArray)) {
			    alert("Please choose Campus");
                return;
			}
		    
			// 상태값을 체크
            if(isNullString(useYn)) {
                alert("Please choose Status");
                return;
            }
			
			var url = orgId == "" ? "/amg/teacherAdd.do" : "/amg/teacherModify.do";
			
			$("#itemForm #memo").val($("textarea").val());
			$("#itemForm #sexSect").val(sexSect);
			$("#itemForm #useYn").val(useYn);
			$("#itemForm").attr("action", "${CONTEXT_PATH}" + url);
			
			isSaveBtn = true;
			
			$("#itemForm").submit();
		}
		
		// 문자열 null 체크
		function isNullString(str) {
            if(str == undefined || str == "") {
                return true;
            } else {
                return false;
            }
        }
		
		// 이메일 유효성 검사
		function checkEmail(email) {
			var format = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
			return format.test(email);
		}
		
		// DB에 넣고 빼야할 캠퍼스 정리
		function parseCampus() {
		    newCampArray = [];
		    // 체크박스에 선택되어 있는 것들을 배열에 넣는다.
		    $("input[name='campusCheck']:checkbox:checked").each(function(index){
                newCampArray.push($(this).val());
            });
            newCampArray.sort();
            
            var delCampList = [];
            // 기존 캠퍼스와 변경된 캠퍼스를 비교하여 지울 캠퍼스를 정리한다.
            for (var i = 0; i < orgCampArray.length; i++) {
                var isDel = true;
                for (var j = 0; j < newCampArray.length; j++) {
                    if (orgCampArray[i] == newCampArray[j]) {
                        isDel = false;
                        break;
                    }
                }
                
                if (isDel) {
                    delCampList.push(orgCampArray[i]);
                }
            }
            $("#itemForm #delCampList").val(delCampList.join(","));
            
            var addCampList = [];
            // 변경된 캠퍼스와 기존 캠퍼스를 비교하여 새로이 넣을 캠퍼스를 정리한다.
            for (var i = 0; i < newCampArray.length; i++) {
                var isAdd = true;
                for (var j = 0; j < orgCampArray.length; j++) {
                    if (newCampArray[i] == orgCampArray[j]) {
                        isAdd = false;
                        break;
                    }
                }
                
                if (isAdd) {
                    addCampList.push(newCampArray[i]);
                }
            }
            $("#itemForm #addCampList").val(addCampList.join(","));
		}
		
		// 아이디 중복 체크
		function checkId() {
        	var id = $.trim($("#mbrId").val());
        	
        	if(id == undefined || id == "") {
        		alert("Please enter a ID.");
        		return;
        	}
        	
        	if(id == orgId) {
    			alert("This ID has already existed.")
    			return;
    		}
        	
        	if (isValidId(id)) {
        	    return;
        	}
        	
            $.ajax({            
                type : "post",
                url : "${CONTEXT_PATH}/member/checkMemberId.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                datatype : "json",
                data : {userId : id},
                success : function(data){
                    var code = data.code;
                    var msg = data.message;
                    if ( code == "0000" ) {
                        if(msg == "0") {
                            alert("This ID is available.");
                            idCheck = true;
                        } else {
                            alert("This ID has been used. Please use a different ID.");
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                }  
            });
            
        }
		
      function clickUploadFile() {
            $("#uploadFile").trigger('click');
      }
      
      function previewImage(targetObj, profile) {
          checkUnload = true;
          var ua = window.navigator.userAgent;

          if (ua.indexOf("MSIE") > -1) {//ie일때
          } else { //ie가 아닐때
              var files = targetObj.files;
              for ( var i = 0; i < files.length; i++) {

                  var file = files[i];

                  if (window.FileReader) { // FireFox, Chrome, Opera 확인.
                      var reader = new FileReader();
                      reader.onloadend = (function(aImg) {
                          return function(e) {
                              /* aImg.src = e.target.result; */
                              $("#logoImage").attr('src', e.target.result);
                          };
                      })($("#logoImage"));
                      reader.readAsDataURL(file);
                      
                  } 
              }
          }
      }
      
      function sendEmailToPwd() {
          //alert(${student.mbrId});
          var pUrl = "${CONTEXT_PATH}/mail/sendUserPwdMail.do";
          var pData = {mbrId: '${teacher.mbrId}'};
          var rst ; // return result data

          $.ajax({
              type : "post",
              url : pUrl,
              beforeSend : function(req) {
                  req.setRequestHeader("Accept", "application/json");
              },
              async : false,
              cache : false,
              data : pData,
              dataType : 'json',
              success : function(data) {
                  var code = data.code;
                  var msg = data.message;
                  if (code == "0000") {
                     // return data;
                     alert(msg);
                  }
              },
              error : function(xhr, ajaxOptions, thrownError) {
                  alert("error");
              },
              complete : function(xhr, textStatus) {
                  isLogging = false;
              }
          });
          return rst;

      }
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->
	
	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Teacher</h2>
			<a href="javascript:teacherList()" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
		
        <form name="searchForm" id="searchForm" method="GET"> 
            <input type="hidden" name="campSeq" id="campSeq" value="${condition.campSeq}"/>
            <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}"/>
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}"/> 
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
        </form>
        
        <form name="itemForm" id="itemForm" method="post" action="" enctype="multipart/form-data">
		<input type="hidden" name="sexSect" id="sexSect" value="${teacher.sexSect}"/>
		<input type="hidden" name="useYn" id="useYn" value="${teacher.useYn}"/>
		<input type="hidden" name="addCampList" id="addCampList"/>
		<input type="hidden" name="delCampList" id="delCampList"/>
		<input type="hidden" name="memo" id="memo" />
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">ID</strong></th>
						<td>
                        <c:choose>
                            <c:when test="${empty teacher.mbrId}">
                                <input type="text" name="mbrId" id="mbrId" class="text" style="width: 140px;" maxLength="15"/>
                                <a href="javascript:checkId()" class="btnBlue"><span>Redundant Check</span></a>
                                <br/><div style="padding:4px 0px 0px 0px ;">Have 15 or fewer characters. Contain basic alphabet letters or numbers only. Not contain special characters.</div>
                            </c:when>
                            <c:otherwise>
                                 <input type="hidden" name="mbrId" id="mbrId" value="${teacher.mbrId}"/>
                                ${teacher.mbrId}
                            </c:otherwise>
                        </c:choose>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Password</strong></th>
						<td>
                            <c:choose>
                                <c:when test="${!empty teacher}">
                                    <a href="javascript:sendEmailToPwd();" class="btnBlue"><span>extra password</span></a>
                                </c:when>
                                <c:otherwise>
        							<input type="password" name="pwd" id="pwd" class="text" style="width: 140px;" value="" />
        							<label for="" class="type1 star">Password check</label> <input type="password" name="pwdConfirm" id="pwdConfirm" class="text" style="width: 140px;" value="" />
                                    <br/><div style="padding:4px 0px 0px 0px ;">4~12 characters with letters(a-z), numbers, and special letters</div>
                                </c:otherwise>
                            </c:choose>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Name</strong></th>
						<td><input type="text" name="name" id="name" class="text" style="width: 140px;" value="${teacher.name}" /></td>
					</tr>

					<tr>
						<th><strong class="">Image</strong></th>
						<td>
                            <div id="profile" class="profile">
                                <c:choose>
                                    <c:when test="${teacher.profilePhotopath eq null || teacher.profilePhotopath eq ''}">
                                        <img id="logoImage" src="${CONTEXT_PATH}/images/common/no-image_2.png" alt="" class="thum" />
                                    </c:when>
                                    <c:otherwise>
                                        <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, teacher.profilePhotopath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
                                <a class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
                                <input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'profile')" style="display:none"/>
                            </div>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Nickname</strong></th>
						<td><input type="text" name="nickName" id="nickName" class="text" style="width: 140px;" value="${teacher.nickName}" /></td>
					</tr>

					<tr>
						<th><strong class="star">E-mail</strong></th>
						<td><input type="text" name="email" id="email" class="text" style="width: 244px;" value="${teacher.email}" /></td>
					</tr>

					<tr>
						<th><strong class="star">Male/Female</strong></th>
						<td>
							<input type="radio" class="rdo" name="rdoSexSect" value="M" <c:if test="${teacher.sexSect eq 'M' }">checked="checked"</c:if> /><label for="">Male</label>
							<input type="radio" class="rdo" name="rdoSexSect" value="F" <c:if test="${teacher.sexSect eq 'F' }">checked="checked"</c:if> /><label for="">Female</label>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Cell-phone</strong></th>
						<td><input type="text" name="telNo" id="telNo" class="text" style="width: 244px;" value="${teacher.telNo}" /></td>
					</tr>

					<tr>
						<th><strong class="">Memo</strong></th>
						<td><textarea style="width: 98%; height: 148px;" class="textarea">${teacher.memo}</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">Lecture Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Campus</strong></th>
						<td>
							<div id="campusNm" style="height:17px">
                                <c:forEach items="${teacher.campList}" var="camp" varStatus="campStatus">
                                    <c:if test="${campStatus.count ne 1}">, </c:if>
                                    ${camp.campNm}
                                </c:forEach>&nbsp;
							</div>

							<ul class="campusChk" id="campusChk">
								<c:forEach items="${campusList}" var="campusItem" varStatus="status">
									<li>
                                        <input name="campusCheck" type="checkbox" class="chk" value="${campusItem.cd}" onclick="javascript:checkboxChange()" <c:if test="${campusItem.checked eq 'Y'}">checked="true"</c:if> <c:if test="${campusItem.disabled eq 'Y'}"> disabled="true"</c:if>/>
                                        <label for="">${campusItem.cdSubject}</label>
                                    </li>
								</c:forEach>
							</ul>
                            &nbsp;
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">Other Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
                            <input type="radio" class="rdo" name="rdoUseYn" value="Y" <c:if test="${teacher.useYn eq 'Y' }">checked="checked"</c:if> /><label for="">Active</label>
                            <input type="radio" class="rdo" name="rdoUseYn" value="N" <c:if test="${teacher.useYn eq 'N' }">checked="checked"</c:if> /><label for="">Inactive</label>
						</td>
					</tr>
                    <c:if test="${!empty teacher }">
    					<tr>
    						<th><strong class="">Last Update</strong></th>
    						<td>${regMember.name}(${regMember.mbrId}) / ${teacher.modDate}&nbsp;</td>
    					</tr>
                    </c:if>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:teacherSave()" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>