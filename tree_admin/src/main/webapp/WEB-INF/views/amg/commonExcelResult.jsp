<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>[ ${subject} ] Excel Import</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
	
	<script type="text/javascript">
	
	function closePop(){
	    window.opener.document.location.reload(true);
		self.close();
	}
	
	
	</script>
</head>
<body>
<div id="popup">
    <%-- <h1>[ ${subject} ] Excel Import</h1> --%>

    <div id="cnt">
        <c:choose>
            <c:when test="${result.code eq '5001'}">
                <p class="popTxt">The data hasn’t been uploaded because of already inserted data errors of the number <strong class="txtRed" >${result.message}</strong><br/>Please check and upload the file again.</p>
            </c:when>
	        <c:when test="${result.code eq '0000'}">
	           <p class="popTxt"><strong class="txtRed" >${result.message}</strong>data has been uploaded.</p>
	        </c:when>
	        <c:otherwise>
	           <p class="popTxt">The data hasn’t been uploaded because of errors of the number <strong class="txtRed" >${result.message}</strong><br/>Please check and upload the file again.</p>
	        </c:otherwise>
        </c:choose>
        <div class="btnArea">
            <a href="javascript:closePop();" class="btnRed"><span>Close</span></a>
        </div>
    </div>

    <%-- <a href="javascript:closePop();" class="close"><img src="${CONTEXT_PATH }/images/popup/close.gif" alt="close" /></a> --%>
</div>
</body>
</html>