<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		$(document).ready(function() {
			
			
			//getAjaxCampusCombo('${CONTEXT_PATH}', 'relCd', 'targetId', 'defaultValue');
			//getAjaxCampusCombo('${CONTEXT_PATH}', 'NA000', '', 'NA005');
			
			
			$("#useYn").change(function(event) {
				$("#campusForm #pageNo").val(1);
				$("#campusForm").attr("action", "${CONTEXT_PATH}/amg/campusSearch.do");
				$("#campusForm").submit();
			});
			$("#searchKey").keypress(function(event) {
				var keycode = (event.keyCode ? event.keyCode : event.which);
			    if(keycode == '13'){
			        searchList();
			    }
			});
		});
		
		function openExcelPop(){
			var w = 500;
			var h = 180;
			var left = (screen.width/2)-(w/2);
			var top = (screen.height/2)-(h/2);
			var option = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, width="+w+", height="+h+", top="+top+", left="+left+", resizable=no, copyhistory=no";

			var url = "${CONTEXT_PATH}/amg/campusExcelPop.do";
			  
			var pop = window.open(url, 'excel' , option);
			pop.focus();
		}
		function campusInsert() {
			$("#campusForm").attr("action", "${CONTEXT_PATH}/amg/campusInsert.do");
			$("#campusForm").submit();
		}
		
		function campusView(id) {
			$("#campSeq").val(id);
			$("#campusForm").attr("action", "${CONTEXT_PATH}/amg/campusView.do");
			$("#campusForm").submit();
		}
		
		function changeOrderType(orderBy) {
			var orgSort = $("#sort").val();
			var orgOrderBy = $("#orderBy").val();
			var sort = "ASC";
			if(orgOrderBy == orderBy) {
				if(orgSort == "ASC") {
					sort = "DESC";
				} else {
					sort = "ASC";
				}
			}
			$("#orderBy").val(orderBy);
			$("#sort").val(sort);
			searchList();
		}
		
		function searchList() {
			var searchKey = $.trim($("#campusForm #searchKey").val());
			var useYn = $("#campusForm #useYn").val();
			
	/* 		if(searchKey == "") {
				alert("검색어를 입력해주세요.");
				return;
			} */
			
			$("#searchForm #searchKey").val(searchKey);
			$("#searchForm #useYn").val(useYn);
			
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/campusSearch.do");
			$("#searchForm").submit();
		}
		
		// 페이징에서 선택시 호출되는 스크립트 
		function goPage(page_no){
			$("#searchForm #pageNo").val(page_no);
			$("#searchForm").submit();
		}
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

		<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Campus</h2>
			<a href="javascript:void(0);" onclick="javascript: openExcelPop();" class="btn btnGray"><span><img src="${CONTEXT_PATH }/images/common/icon/excel.png" alt="" />Excel import</span></a>
		</div>
		
		<form name="searchForm" id="searchForm" method="get" action="">
		    <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
	        <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}" />
	        <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}" />
		</form>
		
		<form name="campusForm" id="campusForm" method="get" action="">
		<input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
		<input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
		<input type="hidden" name="sort" id="sort" value="${condition.sort}" />
		<input type="hidden" name="campSeq" id="campSeq" />
		<input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
		<!-- selectArea -->
		<div class="selectArea">
			<select name="useYn" id="useYn">
				<option value="" <c:if test="${condition.useYn eq ''}">selected</c:if>>Status</option>
				<option value="Y" <c:if test="${condition.useYn eq 'Y'}">selected</c:if>>Active</option>
				<option value="N" <c:if test="${condition.useYn eq 'N'}">selected</c:if>>Inactive</option>
			</select>

			<input type="text" class="text" name="searchKey" id="searchKey" style="width: 120px;" placeholder="Campus" value="${condition.searchKey}" />

			<a href="javascript:searchList()" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
			<a href="javascript:campusInsert()" class="btnRed"><span>+ New</span></a>
		</div>
		<!-- //selectArea -->

		<!-- 리스트 타입 -->
		<div class="tbList1">
			<table>
				<colgroup>
					<col width="6%" />
					<col width="" />
					<col width="12%" />
					<col width="13%" />
					<col width="9%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="6%" />
				</colgroup>

				<thead>
					<tr>
						<th>NO</th>
						<th><a href="javascript:changeOrderType('NAME')">Campus 
						<c:if test="${condition.orderColumn ne 'NAME'}">
						▲
						</c:if>
						<c:if test="${condition.orderColumn eq 'NAME'}">
						<c:choose>
							<c:when test="${condition.sort eq 'ASC'}">
								▲
							</c:when>
							<c:otherwise>
								▼
							</c:otherwise>
						</c:choose>
						</c:if>
						</a></th>
						<th>Code</th>
						<th><a href="javascript:changeOrderType('AREA_NM')">Province 
                        <c:if test="${condition.orderColumn ne 'AREA_NM'}">
                        ▲
                        </c:if>						
						<c:if test="${condition.orderColumn eq 'AREA_NM'}">
						<c:choose>
							<c:when test="${condition.sort eq 'ASC'}">
								▲
							</c:when>
							<c:otherwise>
								▼
							</c:otherwise>
						</c:choose>
						</c:if>
						</a></th>
						<th>Class</th>
						<th>Student</th>
						<th>Registered<br/>members</th>
						<th>Status</th>
						<th><a href="javascript: changeOrderType('REG_DATE')">Registration<br/>date
                        <c:if test="${condition.orderColumn ne 'REG_DATE'}">
                        ▲
                        </c:if>     						
						<c:if test="${condition.orderColumn eq 'REG_DATE'}">
						<c:choose>
							<c:when test="${condition.sort eq 'ASC'}">
								▲
							</c:when>
							<c:otherwise>
								▼
							</c:otherwise>
						</c:choose>
						</c:if>
						</a></th>
						<th>View</th>
					</tr>
				</thead>

				<tbody>
					<c:if test="${empty campusList}">
						<tr>
							<td colspan="10">No data found.</td>
						</tr>
					</c:if>
					<c:if test="${!empty campusList}">
						<c:forEach items="${campusList}" var="campusItem" varStatus="status">
							<tr>
								<td>${campusItem.rnum}</td>
								<td class="aLeft">${campusItem.campNm}</td>
								<td>${campusItem.campId}</td>
								<td>${campusItem.areaNm}</td>
								<td>${campusItem.classCnt}</td>
								<td>${campusItem.studentCnt}</td>
								<td>${campusItem.attendeeCnt}</td>
								<td>
								<c:choose>
									<c:when test="${campusItem.useYn eq 'Y'}">
										<span class="txtRed">Active</span>
									</c:when>
									<c:otherwise>
										<span class="txtBlue">Inactive</span>
									</c:otherwise>
								</c:choose>
								</td>
								<td>${campusItem.regDate}</td>
								<td><a href="javascript:campusView(${campusItem.campSeq})" class="btnBlue"><span>View</span></a></td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //리스트 타입 -->

		<div class="paging">
			<tree:pagination page_no="${condition.pageNo }" total_cnt="${campusList[0].totCnt}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>