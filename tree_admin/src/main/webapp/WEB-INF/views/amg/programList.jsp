<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		$(document).ready(function() {
			
			
			//getAjaxCampusCombo('${CONTEXT_PATH}', 'relCd', 'targetId', 'defaultValue');
			//getAjaxCampusCombo('${CONTEXT_PATH}', 'NA000', '', 'NA005');
			
			
			$("#useYn").change(function(event) {
				$("#campusForm #pageNo").val(1);
				$("#campusForm").attr("action", "${CONTEXT_PATH}/amg/campusSearch.do");
				$("#campusForm").submit();
			});
			$("#searchKey").keypress(function(event) {
				var keycode = (event.keyCode ? event.keyCode : event.which);
			    if(keycode == '13'){
			        searchList();
			    }
			});
		});
      
		function programInsert() {
			
			$("#itemForm").attr("action", "${CONTEXT_PATH}/amg/programInsert.do");
			$("#itemForm").submit();
		}
		
		function programView(id) {
			$("#progSeq").val(id);
			$("#itemForm").attr("action", "${CONTEXT_PATH}/amg/programView.do");
			$("#itemForm").submit();
		}
		
		function getList(orderBy) {
			var orgSort = $("#sort").val();
			var orgOrderBy = $("#orderBy").val();
			var sort = "ASC";
			if(orgOrderBy == orderBy) {
				if(orgSort == "ASC") {
					sort = "DESC";
				} else {
					sort = "ASC";
				}
			}
			$("#orderBy").val(orderBy);
			$("#sort").val(sort);
			$("#itemForm").attr("action", "${CONTEXT_PATH}/amg/programList.do");
			$("#itemForm").submit();
		}
		
		function searchList() {
			
			var useYn  = $("#itemForm #useYn").val();
			var progNm = $("#itemForm #progNm").val();
			
			
			$("#searchForm #useYn").val(useYn);
			$("#searchForm #progNm").val(progNm);
			
			
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/programList.do");
			$("#searchForm").submit();
		}
		
		// 페이징에서 선택시 호출되는 스크립트 
		function goPage(page_no){
			$("#searchForm #pageNo").val(page_no);
			$("#searchForm").submit();
		}
		
        function changeOrderType(orderBy) {
            var oldOrderBy = $("#orderBy").val();
            var oldSort = $("#sort").val();
            var sort = "ASC";
            if(oldOrderBy == orderBy) {
                if(oldSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }

            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            
            searchList();
        }
        
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Program</h2>
        </div>

        <form name="searchForm" id="searchForm" method="GET">
            <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
            <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn }"/> 
            <input type="hidden" name="progNm" id="progNm" value="${condition.progNm }"/>
		</form>


        <form name="itemForm" id="itemForm" method="get" action="">
            <input type="hidden" name="orderBy" id="orderBy" value="${condition.orderColumn}" />
	        <input type="hidden" name="sort" id="sort" value="${condition.sort}" />
	        <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
	        <input type="hidden" name="progSeq" id="progSeq" value=""/> 
	                
        <!-- selectArea -->
        <div class="selectArea">
            <select name="useYn" id="useYn">
                <option value="" <c:if test="${condition.useYn eq ''}">selected</c:if>>Status</option>
                <option value="Y" <c:if test="${condition.useYn eq 'Y'}">selected</c:if>>Active</option>
                <option value="N" <c:if test="${condition.useYn eq 'N'}">selected</c:if>>Inactive</option>
            </select>

            <input type="text" class="text" name="progNm" id="progNm" value="${condition.progNm}" placeholder="Program" style="width: 120px;"/>

            <a href="javascript:void(0);" onclick="javascript: searchList();" class="btnGray"><span><img src="${CONTEXT_PATH }/images/common/icon/zoom.gif" alt="" />Search</span></a>
            <a href="javascript:void(0);" onclick="javascript: programInsert();" class="btnRed"><span>+ New</span></a>
        </div>
        <!-- //selectArea -->
        </form>
        
        <!-- 리스트 타입 -->
        <div class="tbList1">
            <table>
                <colgroup>
                    <col width="6%" />
                    <col width="12%" />
                    <col width="*" />
                    <col width="15%" />
                    <col width="10%" />
                    <col width="9%" />
                    <col width="9%" />
                    <col width="11%" />
                    <col width="6%" />
                </colgroup>

                <thead>
                    <tr>
                        <th rowspan="2">NO</th>
                        <th rowspan="2"><a href="javascript:void(0);" onclick="javascript: changeOrderType('progNm');">Program
		                        <c:if test="${condition.orderColumn ne 'progNm'}">
		                        ▲
		                        </c:if>
                                <c:if test="${condition.orderColumn eq 'progNm'}">
                                <c:choose>
                                    <c:when test="${condition.sort eq 'ASC'}">
                                        ▲
                                    </c:when>
                                    <c:otherwise>
                                        ▼
                                    </c:otherwise>
                                </c:choose>
                                </c:if>
                        </a></th>
                        <th colspan="3" class="gray">Program arrangement</th>
                        <th rowspan="2">Using<br />class</th>
                        <th rowspan="2">Status</th>
                        <th rowspan="2"><a href="javascript:void(0);" onclick="javascript: changeOrderType('modDate');">Last update 
                                <c:if test="${condition.orderColumn ne 'modDate'}">
                                ▲
                                </c:if>                        
                                <c:if test="${condition.orderColumn eq 'modDate'}">
                                <c:choose>
                                    <c:when test="${condition.sort eq 'ASC'}">
                                        ▲
                                    </c:when>
                                    <c:otherwise>
                                        ▼
                                    </c:otherwise>
                                </c:choose>
                                </c:if>                        
                        </a></th>
                        <th rowspan="2">View</th>
                    </tr>

                    <tr class="secLine">
                        <th  class="gray">Book(In class)</th>
                        <th  class="gray">Self-study</th>
                        <th  class="gray">Social</th>
                    </tr>
                </thead>

                <tbody>
                    <c:if test="${empty programList}">
                        <tr>
                            <td colspan="9">No data found.</td>
                        </tr>
                    </c:if>
                    <c:if test="${!empty programList}">                
	                <c:forEach items="${programList}" var="progItem" varStatus="status">
	                    <tr>
	                        <td>${progItem.rn }</td>
	                        <td>${progItem.progNm }</td>
	                        <td><!-- book -->
	                             <c:forEach items="${progItem.itemList}" var="itemList" varStatus="status">
	                                <c:if test="${itemList.itemType eq 'PT002' }">
	                                    <c:out value="${itemList.prodTitle }"/><br/>
	                                </c:if>
	                            </c:forEach>
	                        </td>
	                        <td> <!-- self-study (현재 정의된바 없음 으로 없는 코드 값인 PT000  으로 케이스 처리함 -->
	                             <c:forEach items="${progItem.itemList}" var="itemList" varStatus="status">
	                                <c:if test="${itemList.itemType eq 'PT007' }">
	                                    <c:out value="${itemList.prodTitle }"/><br/>
	                                </c:if>
	                            </c:forEach>
	                        </td>
	                        <td> <!-- social (현재 정의된바 없음 으로 없는 코드 값인 PT000  으로 케이스 처리함 -->
	                             <c:forEach items="${progItem.itemList}" var="itemList" varStatus="status">
	                                <c:if test="${itemList.itemType eq 'PT008' }">
	                                    <c:out value="${itemList.prodTitle }"/><br/>
	                                </c:if>
	                            </c:forEach>
	                        </td>
	                        <td>${progItem.useClassCnt }</td>
	                        <td>
	                            <c:choose>
	                                <c:when test="${progItem.useYn eq 'Y'}">
	                                    <span class="txtRed">Active</span>
	                                </c:when>
	                                <c:otherwise>
	                                    <span class="txtBlue">Inactive</span>
	                                </c:otherwise>
	                            </c:choose>                        
	                        </td>
	                        <td>${progItem.modDate }</td>
	                        <td><a href="javascript:void(0);" onclick="javascript: programView('${progItem.progSeq}');" class="btnBlue"><span>View</span></a></td>
	                    </tr>
	                                    
	                </c:forEach>
	                </c:if>
                


                </tbody>
            </table>
        </div>
        <!-- //리스트 타입 -->

		<div class="paging">
			<tree:pagination page_no="${condition.pageNo }" total_cnt="${totalCount}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>