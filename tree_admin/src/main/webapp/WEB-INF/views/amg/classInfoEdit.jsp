<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	var clicked = false;
    var checkUnload = true;
    $(window).on("beforeunload", function(){
        if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
    });
    
    $(document).ready(function() {
    	
    	$("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리
    	
    	   // 캠퍼스 콤보 생성
        getAjaxCampusCombo("${CONTEXT_PATH}", "${userDetails.insSeq}", "campSeq", "${classInfo.campSeq}", "Campus");
    	   
    	
    	
    	$("#classForm #progSeq").change(function(){
    		var classProgressYn = '${classProgress}'; // 수업 진행여부 
    		
    		var classProgSeq ='${classInfo.progSeq}';  // 이미 등록된 프로그램 SEQ 
    		
    		if(classProgSeq != $(this).val()){
    			if(classProgressYn == 'Y'){
    				// 이미 완료된 차시가 있으므로 프로그램을 수정 할수 없도록 했다.
    				alert('You can not change the program\nbecause this class has completed lesson at least one.');
    				$(this).val(classProgSeq);
    				return;
    			}
    			
    		}
    		
    	});
    	
    	
    	$("input[name^='schdlCheck']").click(function(){
            var ckId = $(this).val();
            
            if($(this).is(":checked")){
            	$("input[id='sub_dayNm"+ckId+"']").attr("checked", true);
                $("input[id='sub_sHH"+ckId+"']").attr("checked", true);
                $("input[id='sub_sMM"+ckId+"']").attr("checked", true);
                $("input[id='sub_eHH"+ckId+"']").attr("checked", true);
                $("input[id='sub_eMM"+ckId+"']").attr("checked", true);
            	
            }else{
            	$("input[id='sub_dayNm"+ckId+"']").attr("checked", false);
                $("input[id='sub_sHH"+ckId+"']").attr("checked", false);
                $("input[id='sub_sMM"+ckId+"']").attr("checked", false);
                $("input[id='sub_eHH"+ckId+"']").attr("checked", false);
                $("input[id='sub_eMM"+ckId+"']").attr("checked", false);
            }
    		
    	});
    	
    	$("#campSeq").change(function(){
    		campTchList('', "tch1");
    		campTchList('', "tch2");
    	});
    	
    	
    	campTchList('${tchList[0].cd}', "tch1");
    	campTchList('${tchList[1].cd}', "tch2");
    	
    	
    	$("input[name=schdlCheck]").click(function(){
    		var idx = $(this).attr("id").substr(10, 11);
    		
    		if(this.checked){
                
                $("#startHH"+idx).attr("disabled", false);
                $("#startMM"+idx).attr("disabled", false);
                $("#endHH"+idx).attr("disabled", false);
                $("#endMM"+idx).attr("disabled", false);
                
    		}else{
                
                $("#startHH"+idx).attr("disabled", true);
                $("#startMM"+idx).attr("disabled", true);
                $("#endHH"+idx).attr("disabled", true);
                $("#endMM"+idx).attr("disabled", true);
                
    		}
    		
    	});
    	
    	
        $("input[type='radio']").click(function(){
        	var useyn = $(":radio[name=status]:checked").val();
        	$("#classForm #useYn").val(useyn);
        });
        
        // set oldValue
    	$("#classForm :input").each(function(){
    		var input = $(this);
    		var pId = input.attr("id");
    		$("#"+pId).setOldValue();
    	});
        
    	   
    });
	
    // 수정된 값이 있는지 체크 
    function checkForm(){
    	// checkUnload 초기화 
    	checkUnload = false;
    	// form안의 모든 input을 뺑뺑이 돌려부러.
    	$("#classForm :input").each(function(){
    		
    		var check = true;
    		var pId = $(this).attr("id");
    		 //console.log(pId+"="+$("#"+pId).isEdited());
    		
    		if(check){
    			// 한번이라도 수정된게 있어불면, check값을 false로 해서 checkUnload값을 유지 해부러  
    			if($("#"+pId).isEdited()){
    				check = false;
    				checkUnload = true;
    			}
    		}
    	});
    	
    	// radio button은 따로 체크를 한다. 
    	// 이미 수정된 값이 있다면 할 필요가 없소만.
    	if(!checkUnload){
    		
    		if($("#classForm #useYn").val() != '${classInfo.useYn}'){
    			checkUnload = true;
    		}
    	}
    	
    	 //console.log("checkUnload="+checkUnload);
		
    }
    
    function timeSelectDraw(){
    	
        $("input[name='schdlCheck']:checkbox").each(function() {
        	
        	var idx = $(this).attr("id").substr(10, 11);
        	
             if(this.checked){
                 
                 $("#startHH"+idx).attr("disabled", false);
                 $("#startMM"+idx).attr("disabled", false);
                 $("#endHH"+idx).attr("disabled", false);
                 $("#endMM"+idx).attr("disabled", false);
                 
             }else{
                 $("#startHH"+idx).attr("disabled", true);
                 $("#startMM"+idx).attr("disabled", true);
                 $("#endHH"+idx).attr("disabled", true);
                 $("#endMM"+idx).attr("disabled", true);
                 
             }
        });
    }
    
    // 캠퍼스에 속한 선생님 목록 (combobox 용 )
    function campTchList(selecCd, targetId){
    	
    	var campSeq = $("#campSeq").val();
    	
    	$.ajax({            
            type : "post",
            url : "${CONTEXT_PATH}/amg/getCampTchList.do",
            beforeSend: function(req) {
                req.setRequestHeader("Accept", "application/json");
            },
            async : false,
            cache : false,
            data : {campSeq : campSeq},
            dataType: 'json',
            success : function(data){
                var code = data.code;
                var msg = data.massage;
                if ( code == "0000" ) {
                    
              	    var result = data.result;
              	    
              	    
              	    var option = "";
              	    
              	    if(result.length > 0){
              	    	
              	    	if(targetId == "tch1"){
              	    		option +="<option value=''>Teacher1</option>";	
              	    	}else{
              	    		option +="<option value=''>Teacher2</option>";
              	    	}
              	    	
              	    }else{
              	    	option +="<option value=''>Teacher</option>";
              	    }
                	    
              	    for(i=0; i< result.length; i++){
              	        var sel = "";
              	        if(selecCd == result[i].cd){
              	            sel = "selected";
              	        }
              	        option +="<option value='"+result[i].cd+"' "+sel+">"+result[i].cdSubject+"</option>";
              	    }
              	    $("."+targetId).html(option);
                	
                } 
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert("error");
            }, 
            complete:function (xhr, textStatus){
                isLogging = false;
            }  
        });
    	
    	
    }
    
    // 수정/ 저장 처리 
    function saveClassInfo() {
        var campSeq = $("#classForm #campSeq").val();
        var classNm = $("#classForm #classNm").val();
        var useYn = $(":radio[name=status]:checked").val();
        var classInfoSeq = $("#classForm #classSeq").val();
        var progSeq = $("#classForm #progSeq").val();
        
        var startDate = $("#sDate").val().split("-");
        var endDate = $("#eDate").val().split("-");
        var sDate = new Date(startDate[0], startDate[1], startDate[2]).valueOf();
        var eDate = new Date(endDate[0], endDate[1], endDate[2]).valueOf();
        
        var tch1 = $(".tch1").val();
        var tch2 = $(".tch2").val();
        
        if(campSeq == null || campSeq.length == 0) {
        	alert('Please choose Campus.');
            //alert("Campus를 입력해주세요.")
            $("#classForm #campSeq").focus();
            return false;
        }
        
        // classNm Name 입력값 체크.
        if(classNm == null || classNm.length == 0) {
        	alert('Please enter Class name.');
            //alert("Class name을 입력해주세요.")
            $("#classForm #classNm").focus();
            return false;
        }
        
        // 프로그램 입력 값 체크 
        if(progSeq == ''){
        	alert("Program choose program.");
        	$("#progSeq").focus();
        	return false;
        }
        
        
        // 선생님 한명은 기본 값 두번째 값은 선택임 체크
        if(tch1 == '' ){
        	alert('Please choose Teacher');
        	//alert("선생님을 선택해 주세요.");
        	$(".tch1").focus();
        	return false;
        }
        
        if(tch1 == tch2){
        	alert('Selected Same Teacher.');
        	//alert("같은 선생님을 선택 했네요.");
        	$(".tch2").focus();
        	return false;
        }
        
        if(startDate == '' || endDate == ''){
        	alert('Please enter startDate and EndDate');
        	//alert("Period를 입력하세요.");
        	return false;
        }
        
        //period check
        if(sDate > eDate){
        	alert('The start date is greater than end date.');
            //alert("시작일이 종료일 보다 큽니다.");
            $("#sDate").focus();
            return false;
        }
        
        // time 요일에 체크되면 숨겨진 시간 체크박스에 값을 할당함 (체크된 값들만 서버로 넘기기 위해서 숨겨진 체크박스를 활용함.)
        $("input[name='schdlCheck']:checkbox").each(function() {
        	
        	if($(this).is(":checked")){
        		
        		var sId = $(this).val();
        			
        		$("input[id='sub_sHH"+sId+"']:checkbox").val($("#startHH"+sId).val());
        		$("input[id='sub_sMM"+sId+"']:checkbox").val($("#startMM"+sId).val());
        		$("input[id='sub_eHH"+sId+"']:checkbox").val($("#endHH"+sId).val());
        		$("input[id='sub_eMM"+sId+"']:checkbox").val($("#endMM"+sId).val());
        	}
        });

        // Status 설정값 체크.
        if(useYn == null || useYn.length == 0) {
        	alert('Please choose status.');
            //alert("Status를 설정해주세요.")
            return false;
        }
        
        $("#eventBtn").hide(); // 더블클릭 방지로 이벤트 걸린 버튼 숨김 처리 
        $("#noEventBtn").show(); // 이벤트 없는 버튼을 보임. 
        
        var url = classInfoSeq == "" ? "/amg/classInfoAdd.do" : "/amg/classInfoModify.do";
        
        $("#classForm #useYn").val(useYn);
        $("#classForm").attr("action", "${CONTEXT_PATH}" + url);
        $("#classForm").attr("method", "POST");
        $("#classForm").attr("disabled", true);
        checkUnload = false;
        $("#classForm").submit();
        return false;
    }
    
	
	// 목록으로 가기 
    function classInfoList() {
    	checkForm();
        $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/classInfoList.do");
        $("#searchForm").submit();
    }
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Class</h2>
            <a href="javascript:classInfoList()" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
        </div>

        <h3 class="tit first">Basic Info</h3>
        <!-- tblForm -->
        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="startDate" value="${startDate }" />
            <input type="hidden" name="endDate" value="${endDate }" />
            <input type="hidden" name="campSeq" id="s_campSeq" value="${campSeq }" />
            <input type="hidden" name="useYn" value="${useYn }" />
            <input type="hidden" name="classNm" value="${classNm }" />
        </form>
        
                
        <form name="classForm" id="classForm" method="post" action="">
        <input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
        <input type="hidden" name="classSeq" id="classSeq" value="${classInfo.classSeq}"/>
       <input type="hidden" name="useYn" id="useYn" value="${classInfo.useYn }"/>
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Campus</strong></th>
                        <td>
                            <select name="campSeq" id="campSeq" style="" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><strong class="">Class code</strong></th>
                        <td>
                            ${classInfo.classSeq }
                        </td>
                    </tr>
                    <tr>
                        <th><strong class="star">Class name</strong></th>
                        <td>
                            <input type="text" name="classNm" id="classNm" class="text" style="width: 244px;" maxlength="10" value="${classInfo.classNm}" />
                           <!--  <label for="" class="type1">Class code</label> <input type="text" class="text" style="width: 244px;" value="class010103" /> -->
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Program</strong></th>
                        <td>
                            <select name="progSeq" id="progSeq" style="width: 250px;">
                            <option value="">Program</option>
                            <c:forEach var="progItem" items="${progList}" varStatus="status">
	                            <c:choose>
	                                <c:when test="${classInfo.progSeq eq progItem.cd }">
	                                    <option value="${progItem.cd }" selected >${progItem.cdSubject }</option>
	                                </c:when>
	                                <c:otherwise>
	                                    <option value="${progItem.cd }">${progItem.cdSubject }</option>
	                                </c:otherwise>
	                            </c:choose>
                                
                            </c:forEach>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Teacher</strong></th>
                        <td>
                            <select name="tchMbrId" id="tchMbrId" class="tch1" style="width: 250px;">
                                <option value="">Teacher1</option>
                            </select>

                            <select name="tchMbrId" id="tchMbrId" class="tch2" style="width: 250px;">
                                <option value="">Teacher2</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Period</strong></th>
                        <td>
                            <input type="text" name="startYmd" id="sDate" class="text datepicker"  value="${classInfo.startYmd }" style="width: 80px"/> ~ <input type="text" name="endYmd" id="eDate" class="text datepicker" value="${classInfo.endYmd }"  style="width: 80px"/>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Time</strong></th>
                        <td>
                            <ul class="week">
                                <input type="hidden" name="clsSchdlSeq" id="clsSchdlSeq" value=""/> <!-- 클래스 TIME seq -->
                                
                                <c:forEach var="schItem" items="${schdlList}" varStatus="status">
	                                <li>
	                                
	                                    <c:choose>
	                                        <c:when test="${!empty schItem.clsSeq}">
	                                            <input type="checkbox" class="chk" name="schdlCheck" id="schdlCheck${status.count}" value="${status.count}" checked /><label for="schdlCheck${status.count}">${schItem.wk}</label>
		                                        <input type="checkbox" class="chk" name="dayNm" id="sub_dayNm${status.count}" value="${schItem.wk}" style="display:none;" checked/>
		                                        <input type="checkbox" class="chk" name="startHH" id="sub_sHH${status.count}" value="" style="display:none;" checked/>
		                                        <input type="checkbox" class="chk" name="startMM" id="sub_sMM${status.count}" value="" style="display:none;" checked/>
		                                        <input type="checkbox" class="chk" name="endHH" id="sub_eHH${status.count}" value="" style="display:none;" checked/>
		                                        <input type="checkbox" class="chk" name="endMM" id="sub_eMM${status.count}" value="" style="display:none;" checked/>	                                            
	                                        </c:when>
	                                        <c:otherwise>
	                                            <input type="checkbox" class="chk" name="schdlCheck" id="schdlCheck${status.count}" value="${status.count}"/><label for="schdlCheck${status.count}">${schItem.wk}</label>
		                                        <input type="checkbox" class="chk" name="dayNm" id="sub_dayNm${status.count}" value="${schItem.wk}" style="display:none;"/>
		                                        <input type="checkbox" class="chk" name="startHH" id="sub_sHH${status.count}" value="" style="display:none;"/>
		                                        <input type="checkbox" class="chk" name="startMM" id="sub_sMM${status.count}" value="" style="display:none;"/>
		                                        <input type="checkbox" class="chk" name="endHH" id="sub_eHH${status.count}" value="" style="display:none;"/>
		                                        <input type="checkbox" class="chk" name="endMM" id="sub_eMM${status.count}" value="" style="display:none;"/>	                                            
	                                        </c:otherwise>
	                                    </c:choose>
	                                    
	                                    
	                                    <select name="sHH" id="startHH${status.count}" style="width: 60px;">
	                                        <c:forEach var="hh1" begin="05" end="24" step="1">
	                                            <c:choose>
	                                               <c:when test="${hh1 eq schItem.startHH }">
	                                                   <option value="<fmt:formatNumber value="${hh1 }" pattern="00"/>" selected><fmt:formatNumber value="${hh1 }" pattern="00"/></option>
	                                               </c:when>
	                                               <c:otherwise>
	                                                   <option value="<fmt:formatNumber value="${hh1 }" pattern="00"/>"><fmt:formatNumber value="${hh1 }" pattern="00"/></option>
	                                               </c:otherwise>
	                                            </c:choose>
	                                            
	                                        </c:forEach>
	                                    </select>
	                                    <select name="sMM" id="startMM${status.count}" style="width: 60px;">
	                                        <c:forEach var="mm1" begin="00" end="50" step="10">
                                                <c:choose>
                                                   <c:when test="${mm1 eq schItem.startMM }">
                                                       <option value="<fmt:formatNumber value="${mm1 }" pattern="00"/>" selected><fmt:formatNumber value="${mm1 }" pattern="00"/></option>
                                                   </c:when>
                                                   <c:otherwise>
                                                       <option value="<fmt:formatNumber value="${mm1 }" pattern="00"/>"><fmt:formatNumber value="${mm1 }" pattern="00"/></option>
                                                   </c:otherwise>
                                                </c:choose>
	                                            
	                                        </c:forEach>
	                                    </select>
	                                    ~
	                                    <select name="eHH" id="endHH${status.count}" style="width: 60px;">
	                                        <c:forEach var="hh2" begin="05" end="24" step="1">
	                                            
                                                <c:choose>
                                                   <c:when test="${hh2 eq schItem.endHH }">
                                                       <option value="<fmt:formatNumber value="${hh2 }" pattern="00"/>" selected><fmt:formatNumber value="${hh2 }" pattern="00"/></option>
                                                   </c:when>
                                                   <c:otherwise>
                                                       <option value="<fmt:formatNumber value="${hh2 }" pattern="00"/>"><fmt:formatNumber value="${hh2 }" pattern="00"/></option>
                                                   </c:otherwise>
                                                </c:choose>
                                                	                                            
	                                        </c:forEach>
	                                    </select>
	                                    <select name="eMM" id="endMM${status.count}" style="width: 60px;">
	                                        <c:forEach var="mm2" begin="00" end="50" step="10">
                                                <c:choose>
                                                   <c:when test="${mm2 eq schItem.endMM }">
                                                       <option value="<fmt:formatNumber value="${mm2 }" pattern="00"/>" selected><fmt:formatNumber value="${mm2 }" pattern="00"/></option>
                                                   </c:when>
                                                   <c:otherwise>
                                                       <option value="<fmt:formatNumber value="${mm2 }" pattern="00"/>"><fmt:formatNumber value="${mm2 }" pattern="00"/></option>
                                                   </c:otherwise>
                                                </c:choose>
                                                	                                            
	                                        </c:forEach>
	                                    </select>
	                                </li>                                      
                                </c:forEach> 
                            </ul>

                        </td>
                    </tr>

                    <tr>
                        <tr>
                            <th><strong class="">Comments</strong></th>
                            <td><textarea style="width: 98%; height: 148px;" name="detailContent" id="detailContent" class="textarea">${classInfo.detailContent }</textarea></td>
                        </tr>
                    </tr>

                    <tr>
                        <th><strong class="star">Status</strong></th>
                        <td>
	                        <input type="radio" class="rdo" name="status" value="Y" id="A" <c:if test="${classInfo.useYn eq 'Y' }"> checked </c:if> /><label for="A">Active</label>
	                        <input type="radio" class="rdo" name="status" value="N" id="B" <c:if test="${classInfo.useYn eq 'N' }"> checked </c:if> /><label for="B">Inactive</label>
                                                    
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->
        </form>
        <script type="text/javascript">timeSelectDraw();</script>
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript:void(0);" onclick="javascript: saveClassInfo();" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript:void(0);" onclick="javascript: void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
            </div>
        </div>

    </div></div>
</div>
</body>
</html>