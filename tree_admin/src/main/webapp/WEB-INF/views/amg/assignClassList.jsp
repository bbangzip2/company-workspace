<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
        $(document).ready(function() {
        	
            //getAjaxCampusCombo(contextPath, intInfoSeq, targetId, defaultValue, optionTag)
            getAjaxCampusCombo('${CONTEXT_PATH}', '${userDetails.insSeq}', 'campSeq','${listCondition.campSeq}', 'Campus'); // campus combobox make
            
            getAjaxClassCombo('${CONTEXT_PATH}', '${listCondition.campSeq}', 'classSeq', '${listCondition.classSeq}', 'Class');
            
            $("#campSeq").change(function(){
            	// 클래스 조회 
            	 getAjaxClassCombo('${CONTEXT_PATH}', $(this).val(),'classSeq', '', 'Class');
            });
        });
        
        function getList(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            $("#classForm").attr("action", "${CONTEXT_PATH}/amg/assignClassList.do");
            $("#classForm").submit();
        }
        
         function searchList() {
    	            
            var startDate = $("#classForm #sDate").val();
            var endDate = $("#classForm #eDate").val();
            var campSeq = $("#classForm #campSeq").val();
            var useYn = $("#classForm #useYn").val();
            var classSeq = $("#classForm #classSeq").val();
           
            $("#searchForm #startDate").val(startDate);
            $("#searchForm #endDate").val(endDate);
            $("#searchForm #s_campSeq").val(campSeq);
            $("#searchForm #useYn").val(useYn);
            $("#searchForm #s_classSeq").val(classSeq);
           
           
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/assignClassList.do");
            $("#searchForm").submit();
        }
       
        // 페이징에서 선택시 호출되는 스크립트 
        function goPage(page_no){
            $("#searchForm #pageNo").val(page_no);
            $("#searchForm").submit();
        }
        
        // 학생배정 팝업 열기 
        function openAssignStudentPop(campSeq, classSeq){
        	var option ="resizable=no,width=1020,height=708";
        	var pop = window.open("${CONTEXT_PATH}/amg/classSettingPop.do?campSeq="+campSeq+"&classSeq="+classSeq , "classSetting", option);
        	pop.focus();
        }
        
        //반배치 미정 학생 팝업 열기 
        function unassignStudentPop(){
        	
        	var campSeq = $("#campSeq").val();
        	
        	if(campSeq == ""){
        		alert('Please select campus');
        		return false;
        	}
        	
        	var option ="resizable=no,width=600,height=568";
            var pop = window.open("${CONTEXT_PATH}/amg/unassignStudentPop.do?campSeq="+campSeq , "unassignStudent", option);
            pop.focus();
        }
        
        function openExcelPop(){
            
            var w = 500;
            var h = 180;
            var left = (screen.width/2)-(w/2);
            var top = (screen.height/2)-(h/2);
            var option = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, width="+w+", height="+h+", top="+top+", left="+left+", resizable=no, copyhistory=no";

            var url = "${CONTEXT_PATH}/amg/assignExcelPop.do";
              
            var pop = window.open(url, 'excel' , option);
            pop.focus();
        }        
        
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Assign Students</h2>
            <a href="javascript:void(0);" onclick="javascript: openExcelPop();" class="btn btnGray"><span><img src="${CONTEXT_PATH }/images/common/icon/excel.png" alt="" />Excel import</span></a>
        </div>

        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            <%-- <input type="hidden" name="orderBy" id="orderBy" value="${listCondition.orderColumn}" /> --%>
            <%-- <input type="hidden" name="sort" id="sort" value="${listCondition.sort}" /> --%>
            <input type="hidden" name="pageNo" id="pageNo" value="${listCondition.pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="startDate" id="startDate" value="${listCondition.startDate }" />
            <input type="hidden" name="endDate" id="endDate" value="${listCondition.endDate }" />
            <input type="hidden" name="campSeq" id="s_campSeq" value="${listCondition.campSeq }" />
            <input type="hidden" name="useYn" id="useYn" value="${listCondition.useYn }" />
            <input type="hidden" name="classSeq" id="s_classSeq" value="${listCondition.classSeq }" />
        </form>
        
        <form name="classForm" id="classForm" method="get" action="">
	       <input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
        <!-- selectArea -->
        <div class="selectArea">
            <span class="datePicker">
                <label for="">Period</label>
                <input type="text" id="sDate" name="startDate" value="${listCondition.startDate }" class="text datepicker" style="width: 80px"/>
                ~
                <input type="text" id="eDate" name="endDate" value="${listCondition.endDate }"  class="text datepicker" style="width: 80px"/>
            </span>

            <select name="campSeq" id="campSeq">
            </select>

            <select name="classSeq" id="classSeq">
                <option value="">Class</option>
            </select>

            <select name="useYn" id="useYn">
                <option value="" <c:if test="${listCondition.useYn eq ''}">selected</c:if>>Status</option>
                <option value="Y" <c:if test="${listCondition.useYn eq 'Y'}">selected</c:if>>Active</option>
                <option value="N" <c:if test="${listCondition.useYn eq 'N'}">selected</c:if>>Inactive</option>
            </select>

            <a href="javascript:void(0);" onclick="javascript:searchList();"class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
            <!-- <a href="javascript:void(0);" class="btnGray"><span>Excel</span></a> -->
            <a href="javascript:void(0);" onclick="javascript: unassignStudentPop();" class="btnGray"><span>Unassigned students</span></a>
        </div>
        <!-- //selectArea -->

        <!-- 리스트 타입 -->
        <div class="tbList2">
            <table>
                <colgroup>
                    <col width="" />
                    <col width="14%" />
                    <col width="14%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="8%" />
                </colgroup>

                <thead>
                    <tr>
                        <th colspan="3">Class</th>
                        <th colspan="4">Student info</th>
                        <th rowspan="2">Assigned<br/>students</th>
                        <th rowspan="2">Status</th>
                    </tr>

                    <tr class="secLine">
                        <th>Name</th>
                        <th colspan="2">Period</th>
                        <th>Name</th>
                        <th>ID</th>
                        <th>Nickname</th>
                        <th>Male/Female</th>
                    </tr>
                </thead>

                <tbody>
                <c:if test="${empty classInfoList }">
                <tr><td  colspan="9">No data found.</td></tr>
                </c:if>
                <c:forEach items="${classInfoList}" var="classItem" varStatus="status">
                    <tr <c:if test="${status.count%2 eq 0}">class="suspense"</c:if> >
                        <td>
                            <strong class="lecture"><c:out value="${classItem.classNm }"/></strong>
                            <dl class="lecTime">
                                <dt>
			                    <c:forEach items="${classItem.teacherList}" var="tlist" varStatus="status">
			                        <c:out value="${tlist.teacherNm }"/>[<c:out value="${tlist.teacherId }"/>]<br/>
			                    </c:forEach>                                
                                </dt>
                                <dd><c:out value="${classItem.wkNm }"/>&nbsp;<c:out value="${classItem.startHm }"/>~<c:out value="${classItem.endHm }"/></dd>
                            </dl>
                        </td>
                        <td><c:out value="${classItem.startYmd }"/></td>
                        <td><c:out value="${classItem.endYmd }"/></td>
                        <td colspan="4" class="stInfo">
                            <table>
                            <c:if test="${empty classItem.studentList }">
                                <tr><td colspan="4"><br/>The class hasn't been assigned.<br/> Please assign the classes by cliking the right button.</td></tr>
                            </c:if>
			                    <c:forEach items="${classItem.studentList}" var="slist" varStatus="status">
	                                <tr>
	                                    <td><c:out value="${slist.studentNm }"/></td>
	                                    <td><c:out value="${slist.studentId }"/></td>
	                                    <td><c:out value="${slist.nickName }"/></td>
	                                    <td><c:out value="${slist.sexSect }"/></td>
	                                </tr>
			                    </c:forEach>
                            </table>
                        </td>
                        <td>
                            <p class="studentNum">총 <strong class="txtRed">${classItem.studentCount }</strong>명</p>
                            <a href="javascript:void(0);" class="btnBlue" onclick="javascript: openAssignStudentPop('${classItem.campSeq}','${classItem.classSeq}');"><span>View</span></a>
                        </td>
                        <td>
                           <c:choose>
                               <c:when test="${classItem.useYn eq 'Y'}">
                                   <span class="txtRed">Active</span>
                               </c:when>
                               <c:otherwise>
                                   <span class="txtBlue">Inactive</span>
                               </c:otherwise>
                           </c:choose>                        
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <!-- //리스트 타입 -->
        </form>
        
        <div class="paging">
            <tree:pagination page_no="${listCondition.pageNo }" total_cnt="${totalCount }" page_size="${listCondition.pageSize }" page_group_size="10" jsFunction="go_page"></tree:pagination>
        </div>

    </div></div>
</div>
</body>
</html>