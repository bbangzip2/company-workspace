<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	var clicked = false;
	
    $(document).ready(function() {
    	
    	   
    });
    
	
	// 목록으로 가기 
    function classInfoList() {
        $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/classInfoList.do");
        $("#searchForm").submit();
    }
	
    // 수정 화면 이동 
    function classEdit() {
        $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/classInfoEdit.do");
        $("#searchForm").submit();
    }
    
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Class</h2>
            <a href="javascript:classInfoList()" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
        </div>

        <h3 class="tit first">Basic Info</h3>
        <!-- tblForm -->
        
        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="startDate" id="startDate" value="${startDate }" />
            <input type="hidden" name="endDate" id="endDate" value="${endDate }" />
            <input type="hidden" name="campSeq" id="s_campSeq" value="${campSeq }" />
            <input type="hidden" name="useYn" id="useYn" value="${useYn }" />
            <input type="hidden" name="classNm" id="classNm" value="${classNm }" />
            <input type="hidden" name="classSeq" id="classSeq" value="${classInfo.classSeq}"/>
        </form>
        
                
        <form name="classForm" id="classForm" method="post" action="">
	        <input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
	        <input type="hidden" name="classSeq" id="classSeq" value="${classInfo.classSeq}"/>
	        <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="startDate" id="startDate" value="${startDate }" />
            <input type="hidden" name="useYn" id="useYn" value="${useYn }" />
            <input type="hidden" name="endDate" id="endDate" value="${endDate }" />        
            <input type="hidden" name="classNm" id="classNm" value="${classNm }" />
            <input type="hidden" name="campSeq" id="campSeq" value="${classInfo.campSeq }" />
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Campus</strong></th>
                        <td>${classInfo.campNm }</td>
                    </tr>
                    <tr>
                        <th><strong class="">Class code</strong></th>
                        <td>
                            ${classInfo.classSeq }
                        </td>
                    </tr>
                    <tr>
                        <th><strong class="star">Class name</strong></th>
                        <td>
                            ${classInfo.classNm }
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Program</strong></th>
                        <td>
                            <c:forEach var="progItem" items="${progList}" varStatus="status">
                                ${progItem.cdSubject }
                            </c:forEach>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Teacher</strong></th>
                        <td><c:forEach var="tchItem" items="${tchList}" varStatus="status">
		                        <c:choose>
		                            <c:when test="${status.last}"><c:out value="${tchItem.cdSubject }"/></c:when>
		                            <c:otherwise>&nbsp;<c:out value="${tchItem.cdSubject }"/>,</c:otherwise>
		                        </c:choose>
	                        </c:forEach></td>
                    </tr>

                    <tr>
                        <th><strong class="star">Period</strong></th>
                        <td>${classInfo.startYmd } ~ ${classInfo.endYmd }</td>
                    </tr>

                    <tr>
                        <th><strong class="">Time</strong></th>
                        <td>
                            <ul class="week">
                                
                                <c:forEach var="schItem" items="${schdlList}" varStatus="status">
	                                <li>
	                                    <c:choose>
	                                        <c:when test="${!empty schItem.clsSeq}">
	                                            <label>${schItem.wk}</label>
	                                            
	                                               <c:forEach var="hh1" begin="05" end="24" step="1">
                                                <c:choose>
                                                   <c:when test="${hh1 eq schItem.startHH }">
                                                       <fmt:formatNumber value="${hh1 }" pattern="00"/>
                                                   </c:when>
                                                   <c:otherwise>
                                                   </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        :
                                            <c:forEach var="mm1" begin="00" end="50" step="10">
                                                <c:choose>
                                                   <c:when test="${mm1 eq schItem.startMM }">
                                                       <fmt:formatNumber value="${mm1 }" pattern="00"/>
                                                   </c:when>
                                                   <c:otherwise>
                                                   </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        ~
                                            <c:forEach var="hh2" begin="05" end="24" step="1">
                                                
                                                <c:choose>
                                                   <c:when test="${hh2 eq schItem.endHH }">
                                                       <fmt:formatNumber value="${hh2 }" pattern="00"/>
                                                   </c:when>
                                                   <c:otherwise>
                                                   </c:otherwise>
                                                </c:choose>
                                                                                                
                                            </c:forEach>
                                            :
                                            <c:forEach var="mm2" begin="00" end="50" step="10">
                                                <c:choose>
                                                   <c:when test="${mm2 eq schItem.endMM }">
                                                       <fmt:formatNumber value="${mm2 }" pattern="00"/>
                                                   </c:when>
                                                   <c:otherwise>
                                                   </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
	                                        </c:when>
	                                        <c:otherwise>
	                                        </c:otherwise>
	                                    </c:choose>
	                                    
	                                     
	                                </li>                                      
                                </c:forEach> 
                            </ul>

                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Comments</strong></th>
                        <td>
	                     <c:if test="${!empty classInfo.detailContent}">
	                             <c:set var="newline" value="<%= \"\n\" %>" />
	                             ${fn:replace(classInfo.detailContent, newline, '<br/>')}
	                     </c:if>&nbsp;                                   
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Status</strong></th>
                        <td>
                            <c:if test="${classInfo.useYn eq 'Y' }"> Active </c:if>
                            <c:if test="${classInfo.useYn eq 'N' }"> Inactive </c:if>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->
        </form>
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript:void(0);" onclick="javascript: classEdit();" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
            </div>
        </div>

    </div></div>
</div>
</body>
</html>