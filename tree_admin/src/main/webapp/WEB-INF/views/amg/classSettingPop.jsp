<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>Class Setting</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
	
	<script type="text/javascript">
	
    $(document).ready(function() {
        
        $("#noEventBtn").hide();    // 더블클릭 방지를 위한 이벤트가 안걸린 Save버튼 숨김 처리 
        
        $('#totalChkAll').click(function(){
        	if(this.checked){
        		$("input[name=totCheck]").each(function(){
                    this.checked = true;
                });	
        	}else{
        		$("input[name=totCheck]").each(function(){
                    this.checked = false;
                });
        	}
        });
        
        $('#selectChkAll').click(function(){
            if(this.checked){
                $("input[name=selCheck]").each(function(){
                    this.checked = true;
                }); 
            }else{
                $("input[name=selCheck]").each(function(){
                    this.checked = false;
                });
            }
        });
        
        $("#sortName").click(function(){
        	orderChange();
        });
        
         
         var valid = true;
        $("#classForm").ajaxForm({
            beforeSubmit : function(){
      /*       	 if($("input[name=selCheck]").length < 1){
                     alert('select 1 or more students');
                     valid = false;
                     return false;
                 } */
            },
            dataType : "json",
            url: '${CONTEXT_PATH}/amg/classSettingAdd.do',
            success : function(data){
                var code = data.code;
                var msg = data.msg;

                if(code == "0000"){
                    alert('Saved.');
                    window.opener.searchList();
                    window.close();
                }else{
                    if((code == "1001") || (code == "1002")){
                        alert(msg);
                    }else{
                        alert("error");
                    }
                }
            }
            , error : function(request, status, error){
                //alert("code: "+request.status + "\r\nmessage : "+request.responseText);
            },complete : function(){
            }
        });
        
        
        
    });
    
	function closePop(){
		window.close();
	}
	
    //ADD
    function addList() {
        
        $("input[name=totCheck]").each(function(){
        	
        	var selectedCount = $('#selectItem > tr').length;
        	var samesame = false;
        	if(this.checked){
        		var arr = $(this).val().split("#");
        		var idx = selectedCount+1;
        		var name = arr[0];
        		var id = arr[1];
        		var nickname = arr[2];
        		var sexsect = arr[3];
        		
        		var html = "";
        		
        		if(!sameCheck(id)){
                    html += "<tr>";
                    html += "<td id=\"idx\">"+idx+"</td>";
                    html += "<td>"+name+"</td>";
                    html += "<td>"+id+"</td>";
                    html += "<td>"+nickname+"</td>";
                    html += "<td>"+sexsect+"</td>";
                    html += "<td><input type=\"checkbox\" name=\"selCheck\" class=\"chk\" id=\"selCheck\" value=\""+name+"#"+id+"#"+nickname+"#"+sexsect+"\"/>";
                    html += "<input type=\"hidden\" name=\"addId\" id=\"addId\" value=\""+id+"\"/></td>";
                    html += "</tr>";
        		}

        		
        		$("#selectItem").append(html);
        		
        	}
        }); 
        
    };

    //DEL
    function delList() {
    	$("input[name=selCheck]").each(function(idx){
    		if(this.checked){
                var arr = $(this).val().split("#");
                var id = arr[1];
    			$(this).parent().parent().remove();
    			makeDeleteInput(id);
    		}
    	});
    	
        $("td[id='idx']").each(function(idx){
            $(this).text(idx+1);
        });
    };
    
    // 이미 추가된 회원체크 
    function sameCheck(mid){
    	var rst = false;
    	$("input[name=selCheck]").each(function(idx){
    		var arr = $(this).val().split("#");
            var id = arr[1];
            
            if(id == mid){
            	rst = true;
            }
    	});
    	return rst;
    }
    
    function makeDeleteInput(val){
        var delInput = "<input type=\"text\" name=\"delId\"  id=\"delId\" value=\""+val+"\"/>"; //삭제된 레슨 SEQ  
        $("#deleteDiv").append(delInput);
        
    }
    
    // save 
    function saveAssignClass(){
    	$('#classForm').submit();
    	
    }
    
    function orderChange(){
    	var order = $("#order").val();
    	var thText = "";
    	if(order == 'ASC'){
    		$("#order").val("DESC")
    		thText= "Name▼";
    	}else{
    		$("#order").val("ASC")
    		thText= "Name▲";
    	}
    	
    	$("#sortName").text(thText);
    	searchClassMember();
    }
    
    function searchClassMember(){
    	var campSeq = $("#campSeq").val();
    	var clsSeq = $("#recentClass").val();
    	var searchWord = $("#searchWord").val();
    	var order = $("#order").val();
    	
    	   $.ajax({            
    	        type : "post",
    	        url : "${CONTEXT_PATH}/amg/getAssignClassSearchList.do",
    	        beforeSend: function(req) {
    	            req.setRequestHeader("Accept", "application/json");
    	        },
    	        async : false,
    	        cache : false,
    	        data : {campSeq: campSeq, clsSeq : clsSeq, searchWord: searchWord, order: order},
    	        dataType: 'json',
    	        success : function(data){
    	            var code = data.code;
    	            var msg = data.massage;
    	            var result = data.result;
    	            
    	            
    	            var html ="";
    	            if(result.length > 0){
    	            	 for(i=0 ; i < result.length; i++){
    	                        
    	                        html += "<tr>";
    	                        html += "<td>"+(i+1)+"</td>";
    	                        html += "<td>"+result[i].mbrNm+"</td>";
    	                        html += "<td>"+result[i].mbrId+"</td>";
    	                        html += "<td>"+result[i].nickName+"</td>";
    	                        html += "<td>"+result[i].sexSect+"</td>";
    	                        html += "<td>";
    	                        for(h=0; h < result[i].clsList.length; h++){
    	                            if(h == result[i].clsList.length -1 ){
    	                                html += result[i].clsList[h].classNm;
    	                            }else{
    	                                html += result[i].clsList[h].classNm+",";   
    	                            }
    	                                
    	                        }
    	                        html += "</td>"
    	                        html += "<td><input type=\"checkbox\" name=\"totCheck\" class=\"chk\" id=\"totCheck\" value=\""+result[i].mbrNm+"#"+result[i].mbrId+"#"+result[i].nickName+"#"+result[i].sexSect+"\"/>";
    	                        html += "</tr>";
    	                    }
    	                    
    	            }else{
    	            	html += "<tr><td colspan=\"7\">No data found.</td></tr>"
    	            }
    	           
    	            
    	            $("#totalItem").html(html);
    	        },
    	        error: function (xhr, ajaxOptions, thrownError){
    	            alert("error");
    	        }, 
    	        complete:function (xhr, textStatus){
    	        }  
    	    });
    	   
    }
    
	</script>
</head>
<!-- 1020*708 -->
<div id="popup">
    <!-- <h1>Class Setting</h1> -->

    <div id="cntSelect">
        <p class="popTxt"><strong>Please assign the students on the left to a class.</strong></p>

        <div class="section">
            <div class="sideL">
                <!-- selectArea -->
                <div class="selectArea">
                    <select name="recentClass" id="recentClass">
                        <option value="">Recent class</option>
                        <c:forEach items="${recentClassList}" var="recentClassList" varStatus="status">
                            <option value="${recentClassList.classSeq }">${ recentClassList.classNm}</option>
                        </c:forEach>
                    </select>

                    <input type="text" class="text" id="searchWord" placeholder="ID/Name"  style="width: 120px;"/>
                    <a href="javascript:void(0);" class="btnGray" onclick="javascript:searchClassMember();"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
                    <input type="hidden" class="text" id="order"  value="ASC"/>
                </div>
                <!-- //selectArea -->

                <!-- 리스트 타입 -->
                <div class="poptbList">
                    <table>
                        <colgroup>
                            <col width="10%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="*" />
                            <col width="13%" />
                            <col width="19%" />
                            <col width="9%" />
                        </colgroup>

                        <thead>
                            <tr>
                                <th>No.</th>
                                <th><a href="javascript:void(0);" id="sortName">Name<c:choose><c:when test="${order eq 'ASC' }">▲</c:when><c:otherwise>▼</c:otherwise></c:choose></a></th>
                                <th>ID</th>
                                <th>Nickname</th>
                                <th>M/F</th>
                                <th><a href="javascript:void(0);">Recent<br />class</a></th>
                                <th><input type="checkbox" id="totalChkAll" class="chk" /></th>
                            </tr>
                        </thead>

                        <tbody id="totalItem">
                        <c:forEach items="${campusStuList}" var="campusStuList" varStatus="status">
                            <tr>
                                <td>${status.count }</td>
                                <td>${campusStuList.mbrNm }</td>
                                <td>${campusStuList.mbrId }</td>
                                <td>${campusStuList.nickName }</td>
                                <td>${campusStuList.sexSect }</td>
                                <td>
                                <c:forEach items="${campusStuList.clsList}" var="clsList" varStatus="status">
			                        <c:choose>
			                            <c:when test="${status.last }"><c:out value="${clsList.classNm}"/></c:when>
			                            <c:otherwise><c:out value="${clsList.classNm}"/>,&nbsp;</c:otherwise>
			                        </c:choose>                                
                                </c:forEach>
                                
                                </td>
                                <td><input type="checkbox" name="totCheck" id="totCheck" class="chk" value="${campusStuList.mbrNm }#${campusStuList.mbrId }#${campusStuList.nickName }#${campusStuList.sexSect }"/></td>
                            </tr>
                            
                         </c:forEach>

                        </tbody>
                    </table>
                </div>
                <!-- //리스트 타입 -->
            </div>
            
            <form name="classForm" id="classForm" method="post" action="">
            <input type="hidden" name="campSeq" id="campSeq" value="${campSeq }"/> 
            <input type="hidden" name="classSeq" id="classSeq" value="${classSeq }"/>   
            <div class="sideR">
                <div class="selectArea">
                    <span><c:out value="${classDetailInfo[0].classNm }"/></span>

                    <p class="txtBlue">
                    <c:forEach items="${classDetailInfo}" var="classDetailInfo" varStatus="status">
	                    <c:forEach items="${classDetailInfo.teacherList}" var="teacherList" varStatus="status">
	                    <c:choose>
		                    <c:when test="${status.last }"><c:out value="${teacherList.teacherNm }"/>(<c:out value="${teacherList.teacherId }"/>)</c:when>
		                    <c:otherwise><c:out value="${teacherList.teacherNm }"/>(<c:out value="${teacherList.teacherId }"/>),</c:otherwise>
	                    </c:choose>
	                        
                        </c:forEach>
                    </c:forEach>
                    <br />${classDetailInfo[0].wkNm }&nbsp;${classDetailInfo[0].startHm }-${classDetailInfo[0].endHm }</p>

                </div>
                <!-- 리스트 타입 -->
                <div class="poptbList">
                    <table>
                        <colgroup>
                            <col width="10%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="*" />
                            <col width="13%" />
                            <col width="9%" />
                        </colgroup>

                        <thead>
                            <tr>
                                <th>No.</th>
                                <!-- <th><a href="">Name</a></th> -->
                                <th>Name</th>
                                <th>ID</th>
                                <th>Nickname</th>
                                <th>M/F</th>
                                <th><input type="checkbox" id="selectChkAll" class="chk" /></th>
                            </tr>
                        </thead>

                        <tbody id="selectItem">
                        
                        <c:forEach items="${classStuList}" var="classStuList" varStatus="status">
                            <tr>
                                <td id="idx">${status.count }</td>
                                <td>${classStuList.mbrNm }</td>
                                <td>${classStuList.mbrId }</td>
                                <td>${classStuList.nickName }</td>
                                <td>${classStuList.sexSect }</td>
                                <td>
                                <input type="checkbox" name="selCheck" id="selCheck" class="chk" value="${classStuList.mbrNm }#${classStuList.mbrId }#${classStuList.nickName }#${classStuList.sexSect }"/>
                                </td>
                            </tr>
                        </c:forEach>    

                        </tbody>
                    </table>
                </div>
                <!-- //리스트 타입 -->
            </div>
            <div id="deleteDiv" style="display:none;"></div>
            </form>
            
            <div class="control">
                <a href="javascript:void(0);" class="btnGray" id="add" onclick="addList(); return false;"><span>ADD▶</span></a>
                <a href="javascript:void(0);" class="btnGray" id="del" onclick="delList(); return false;"><span>◀DEL</span></a>
            </div>
        </div>

        <div class="btnArea">
            <p class="sideL">*After adding students, please click the [<strong class="txtRed">Save</strong>] button to complete the assignment of the classes.</p>
            <div class="sideR">
                <a href="javascript: saveAssignClass()" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
                <a href="javascript: void(0);" id="noEventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
            </div>
        </div>
    </div>

    <%-- <a href="javascript:void(0);" onclick="javascript: closePop();" class="close"><img src="${CONTEXT_PATH}/images/popup/close.gif" alt="창닫기" /></a> --%>
</div>

</body>
</html>