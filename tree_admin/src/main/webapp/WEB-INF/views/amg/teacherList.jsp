<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		$(document).ready(function() {
		    
			var camp = ${campList}
            var option = camp.result.length == 1 ? "" : "Campus";
            makeComboBox(camp,  "itemForm #campSeq", "${condition.campSeq}", option);
			
			$("#itemForm #searchKey").click(function(event) {
				$(this).select();
			});
			
			$("#itemForm #searchKey").keypress(function(event) {
				var keycode = (event.keyCode ? event.keyCode : event.which);
			    if(keycode == '13'){
			        searchList();
			    }
			});
		});
		
		function teacherInsert() {
			$("#itemForm").attr("action", "${CONTEXT_PATH}/amg/teacherInsert.do");
			$("#itemForm").submit();
		}
		
		function searchList() {
		    var campSeq = $("select[name=campSeq]").val();
		    var searchKey = $("#itemForm #searchKey").val().trim();
		    var useYn = $("select[name=useYn]").val();
		    
		    $("#searchForm #campSeq").val(campSeq);
		    $("#searchForm #searchKey").val(searchKey);
			$("#searchForm #useYn").val(useYn);
			$("#searchForm #pageNo").val(1);
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/teacherList.do");
			$("#searchForm").submit();
		}
		
		// 페이징에서 선택시 호출되는 스크립트 
		function goPage(page_no){
		   $("#searchForm #pageNo").val(page_no);
		   $("#searchForm").submit();
		}
		
		function teacherView(id) {
			$("#itemForm #mbrId").val(id);
			$("#itemForm").attr("action", "${CONTEXT_PATH}/amg/teacherView.do");
			$("#itemForm").submit();
		}
		
        function openExcelPop(){
            var w = 500;
            var h = 180;
            var left = (screen.width/2)-(w/2);
            var top  = (screen.height/2)-(h/2);
            var option = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, width="+w+", height="+h+", top="+top+", left="+left+", resizable=no, copyhistory=no";

            var url = "${CONTEXT_PATH}/amg/teacherExcelPop.do";
              
            var pop = window.open(url, 'excel' , option);
            pop.focus();
        }
		
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Teacher</h2>
			<a href="javascript:openExcelPop();" class="btn btnGray"><span><img src="${CONTEXT_PATH }/images/common/icon/excel.png" alt="" />Excel import</span></a>
		</div>
        
        <form name="searchForm" id="searchForm" method="GET"> 
            <input type="hidden" name="campSeq" id="campSeq" value="${condition.campSeq}"/>
            <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}"/>
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}"/> 
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
        </form>
        
		<form name="itemForm" id="itemForm" method="GET" action="">
		<input type="hidden" name="mbrId" id="mbrId"/>
		<input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${condition.insSeq}"/>
		<input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> <!-- 현재 페이지  -->
		<!-- selectArea -->
		<div class="selectArea">
			<select name="campSeq" id="campSeq">
			</select>
			<select name="useYn" id="useYn">
				<option value="" <c:if test="${condition.useYn eq ''}">selected</c:if>>Status</option>
				<option value="Y" <c:if test="${condition.useYn eq 'Y'}">selected</c:if>>Active</option>
				<option value="N" <c:if test="${condition.useYn eq 'N'}">selected</c:if>>Inactive</option>
			</select>

			<input type="text" class="text" name="searchKey" id="searchKey" style="width: 120px;" placeholder="Name/ID" value="<c:if test="${condition.searchKey ne null}">${condition.searchKey}</c:if>" />

			<a href="javascript:searchList()" class="btnGray"><span><img src="" alt="" />Search</span></a>
			<a href="javascript:teacherInsert()" class="btnRed"><span>+ New</span></a>
		</div>
		<!-- //selectArea -->

		<!-- 리스트 타입 -->
		<div class="tbList1">
			<table>
				<colgroup>
					<col width="6%" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<!-- <col width="" /> -->
					<col width="" />
				</colgroup>

				<thead>
					<tr>
						<th>NO</th>
						<th>Picture</th>
						<th>ID</th>
						<th>Name</th>
						<th>Nickname</th>
						<th>Campus</th>
						<th>Registration date</th>
						<th>Status</th>
						<!-- <th>Teacher’s<br />Admin</th> -->
						<th>View</th>
					</tr>
				</thead>

				<tbody>
					<c:if test="${empty teacherList}">
						<tr>
							<td colspan="9">No data found.</td>
						</tr>
					</c:if>
					<c:if test="${!empty teacherList}">
						<c:forEach items="${teacherList}" var="teacherItem" varStatus="status">
							<tr>
								<td>${teacherItem.rn}&nbsp;</td>
								<td>
                                <c:choose>
                                    <c:when test="${teacherItem.profilePhotopath eq null || teacherItem.profilePhotopath eq ''}">
                                        <img src="${CONTEXT_PATH}/images/common/no-image_2.png" alt="" class="pic" />
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, teacherItem.profilePhotopath)}" alt="" class="pic" />
                                    </c:otherwise>
                                </c:choose>
                                </td>
								<td>${teacherItem.mbrId}&nbsp;</td>
								<td>${teacherItem.name}(${teacherItem.sexSect})&nbsp;</td>
								<td>${teacherItem.nickName}&nbsp;</td>
								<td>
									<c:forEach items="${teacherItem.campList}" var="campItem" varStatus="status">
										<c:if test="${status.count ne 1}">, </c:if>${campItem.campNm}
									</c:forEach>
								</td>
								<td>${teacherItem.regDate}</td>
								<td>
									<c:choose>
										<c:when test="${teacherItem.useYn eq 'Y'}">
											<span class="txtRed">Active</span>
										</c:when>
										<c:otherwise>
											<span class="txtBlue">Inactive</span>
										</c:otherwise>
									</c:choose>
								</td>
								<!-- <td><a href="javascript:void(0)" class="btnBlue"><span>Go</span></a></td> -->
								<td><a href="javascript:teacherView('${teacherItem.mbrId}')" class="btnBlue"><span>View</span></a></td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //리스트 타입 -->

		<div class="paging">
			<tree:pagination page_no="${condition.pageNo}" total_cnt="${condition.totalCount}" page_size="${condition.pageSize}" page_group_size="10" jsFunction="goPage"></tree:pagination>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>