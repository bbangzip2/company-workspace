<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
	
	<script type="text/javascript">
	
	   function sendEmailToPwd() {
		   //alert(${student.mbrId});
           var pUrl = "${CONTEXT_PATH}/mail/sendUserPwdMail.do";
           var pData = {mbrId: '${student.mbrId}'};
           var rst ; // return result data

           $.ajax({
               type : "post",
               url : pUrl,
               beforeSend : function(req) {
                   req.setRequestHeader("Accept", "application/json");
               },
               async : false,
               cache : false,
               data : pData,
               dataType : 'json',
               success : function(data) {
                   var code = data.code;
                   var msg = data.message;
                   if (code == "0000") {
                      // return data;
                      alert(msg);
                   }
               },
               error : function(xhr, ajaxOptions, thrownError) {
                   alert("error");
               },
               complete : function(xhr, textStatus) {
                   isLogging = false;
               }
           });
           return rst;

	   }
	</script>
</head>

<body>
<!-- 750*750 -->
<div id="popup">
	<h1>Student Total Info</h1>
	<div id="infoCnt">
		<!-- userProfile -->
		<div class="userInfo">
			<div class="userProfile">
				<span class="thum">
                <c:choose>
                    <c:when test="${student.profilePhotopath eq null || student.profilePhotopath eq ''}">
                        <img id="logoImage" src="${CONTEXT_PATH}/images/common/no-image_2.png" alt=""/>
                    </c:when>
                    <c:otherwise>
                        <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, student.profilePhotopath)}" alt="" />
                    </c:otherwise>
                </c:choose>
                </span>
				<div class="name">
					<strong class="txtBlue">${student.name}</strong>(${student.mbrId})
				</div>
			</div>

			<div class="detail">
				<ul>
					<li>${student.insNm}</li>
					<li>${student.classInfo[0].campNm}</li>
					<li>Class : 
                    <c:forEach items="${student.classInfo}" var="clsItem" varStatus="clsStatus">
                        <c:if test="${clsStatus.count ne 1 && !empty clsItem.classNm}">, </c:if>
                        ${clsItem.classNm}
                    </c:forEach>
                    </li>
				</ul>

				<div class="btn">
					<a href="javascript:void(0)"><img src="${CONTEXT_PATH}/images/popup/myTree.gif" alt="My TREE" /></a>
					<a href="javascript:void(0)"><img src="${CONTEXT_PATH}/images/popup/portfolio.gif" alt="Portfolio" /></a>
					<a href="javascript:void(0)"><img src="${CONTEXT_PATH}/images/popup/social.gif" alt="Social learning" /></a>
				</div>
			</div>
		</div>
		<!-- //userProfile -->

		<div class="tabArea">
			<ul>
				<li><a href="javascript:void(0)" class="on">Student Info</a></li>
				<li><a href="javascript:void(0)">Study history</a></li>
				<li><a href="javascript:void(0)">Test</a></li>
				<li><a href="javascript:void(0)">Social learning</a></li>
				<li><a href="javascript:void(0)">Badge & point</a></li>
				<li><a href="javascript:void(0)">Question & inquires</a></li>
			</ul>
		</div>

		<div class="hGroup">
			<h2>Basic Info</h2>
		</div>

		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">ID</strong></th>
						<td>${student.mbrId}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="star">Password</strong></th>
						<td>
							<a href="javascript:sendEmailToPwd();" class="btnBlue"><span>extra password</span></a>
						</td>
					</tr>
					<tr>
						<th><strong class="star">Name</strong></th>
						<td>${student.name}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="star">Nickname</strong></th>
						<td>${student.nickName}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="star">E-mail</strong></th>
						<td>${student.email}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="">Male/Female</strong></th>
						<td>
							<c:if test="${student.sexSect eq 'M'}">Male</c:if>
							<c:if test="${student.sexSect eq 'F'}">Female</c:if>&nbsp;
						</td>
					</tr>
					<tr>
						<th><strong class="">Date of birth</strong></th>
						<td>${student.birthday}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="">Cell-phone</strong></th>
						<td>${student.telNo}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
							<c:if test="${student.useYn eq 'Y'}">Active</c:if>
							<c:if test="${student.useYn ne 'Y'}">Inactive</c:if>
							&nbsp;
						</td>
					</tr>
					<tr>
						<th><strong class="star">Member</strong></th>
						<td>${student.sect}&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="hGroup">
			<h2>Institute Info</h2>
		</div>

		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Institute</strong></th>
						<td>${student.insNm}&nbsp;</td>
					</tr>
					<tr>
						<th><strong class="star">Campus</strong></th>
						<td>${student.classInfo[0].campNm}&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="hGroup">
			<h2>Registration History</h2>
		</div>
		<ul>
			<li><strong>&middot;</strong> Registration date : <span class="txtRed">${student.regDate}</span></li>
            <c:if test="${!empty student.quitDate}">
    			<li><strong>&middot;</strong> Withdraw Date : <span class="txtRed">${student.quitDate}</span></li>
            </c:if>
		</ul>


	</div>
</div>
</body>
</html>