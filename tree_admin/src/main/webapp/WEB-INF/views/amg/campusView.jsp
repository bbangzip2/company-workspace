<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	    var redOk = false;
	    
	    $(document).ready(function() {
	        
	    });
    
		function campusList() {
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/campusList.do");
            $("#searchForm").submit();
		}
		
        function campusEdit() {
            $("#campusForm").attr("action", "${CONTEXT_PATH}/amg/campusEdit.do");
            $("#campusForm").submit();
        }
        
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

		<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Campus</h2>
			<a href="javascript:campusList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
		
        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="useYn" id="useYn" value="${useYn}" />
            <input type="hidden" name="searchKey" id="searchKey" value="${searchKey}" />
        </form>
        
        		
		<form name="campusForm" id="campusForm" method="post" action="">
			<input type="hidden" name="campSeq" id="campSeq" value="${campus.campSeq}"/>
			<input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
			<input type="hidden" name="useYn" id="useYn" value="${campus.useYn}"/>
			<input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
			<input type="hidden" name="searchKey" id="searchKey" value="${searchKey}" />
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->		
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Campus name</strong></th>
						<td>
						${campus.campNm}
						</td>
					</tr>

					<tr>
						<th><strong>Campus code</strong></th>
						<td>${campus.campId}</td>
					</tr>

					<tr>
						<th><strong class="">Province</strong></th>
						<td>${campus.areaNm}</td>
					</tr>

					<tr>
						<th><strong class="">Info</strong></th>
						<td>
                        <c:if test="${!empty campus.detailInfo}">
                                <c:set var="newline" value="<%= \"\n\" %>" />
                                ${fn:replace(campus.detailInfo, newline, '<br/>')}
                        </c:if>&nbsp;     						
						</td>
					</tr>

					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
						<c:if test="${campus.useYn eq 'Y' }"> Active </c:if>
						<c:if test="${campus.useYn eq 'N' }"> Inactive </c:if>
						</td>
					</tr>
					<c:if test="${campus.regDate ne null}">
					<tr>
						<th><strong class="star">Registration date</strong></th>
						<td>${campus.regDate}</td>
					</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript: campusEdit();" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>