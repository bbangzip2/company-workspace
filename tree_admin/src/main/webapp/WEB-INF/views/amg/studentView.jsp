<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
	
	<script type="text/javascript">
        function studentEdit() {
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/studentEdit.do");
            $("#searchForm").submit();
        }
    </script> 
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Student</h2>
			<a href="javascript:history.back(-1);" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
		
        <form name="searchForm" id="searchForm" method="GET"> 
            <input type="hidden" name="mbrId" id="mbrId" value="${condition.mbrId}"/>
            <input type="hidden" name="campSeq" id="campSeq" value="${condition.campSeq}"/>
            <input type="hidden" name="progSeq" id="progSeq" value="${condition.progSeq}"/>
            <input type="hidden" name="clsSeq" id="clsSeq" value="${condition.clsSeq}"/>
            <input type="hidden" name="searchColumn" id="searchColumn" value="${condition.searchColumn}"/>
            <input type="hidden" name="searchKey" id="searchKey" value="${condition.searchKey}"/>
            <input type="hidden" name="regStartDate" id="regStartDate" value="${condition.regStartDate}"/> 
            <input type="hidden" name="regEndDate" id="regEndDate" value="${condition.regEndDate}"/> 
            <input type="hidden" name="useYn" id="useYn" value="${condition.useYn}"/> 
            <input type="hidden" name="pageNo" id="pageNo" value="${condition.pageNo}"/> 
            <input type="hidden" name="pageSize" id="pageSize" value="${condition.pageSize}"/>
        </form>
        
        <form name="itemForm" id="itemForm" method="post" action="" enctype="multipart/form-data">
		<input type="hidden" name="mbrId" id="mbrId" value="${student.mbrId}"/>
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">ID</strong></th>
						<td>${student.mbrId}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Name</strong></th>
						<td>${student.name}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="">Image</strong></th>
						<td>
                            <div id="profile" class="profile">
                                <c:choose>
                                    <c:when test="${student.profilePhotopath eq null || student.profilePhotopath eq ''}">
                                        <img id="logoImage" src="${CONTEXT_PATH}/images/common/no-image_2.png" class="thum""/>
                                    </c:when>
                                    <c:otherwise>
                                        <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, student.profilePhotopath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </td>
					</tr>

					<tr>
						<th><strong class="star">Nickname</strong></th>
						<td>${student.nickName}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">E-mail</strong></th>
						<td>${student.email}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Male/Female</strong></th>
						<td>
                            <c:choose>
                                <c:when test="${student.sexSect eq 'M'}">Male</c:when>
                                <c:otherwise>Female</c:otherwise>
                            </c:choose>
                            &nbsp;
						</td>
					</tr>

					<tr>
						<th><strong class="">Date of birth</strong></th>
						<td>${student.birthday}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="">Cell-phone</strong></th>
						<td>${student.telNo}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
                            <c:choose>
                                <c:when test="${student.useYn eq 'Y'}">Active</c:when>
                                <c:otherwise>Inactive</c:otherwise>
                            </c:choose>
                            &nbsp;
						</td>
					</tr>
					<tr>
						<th><strong class="star">Member</strong></th>
						<td>${student.sect}&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->


        <h3 class="tit">Institute Info</h3>
        <!-- tblForm -->
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Institute</strong></th>
                        <td>${student.insNm}&nbsp;</td>
                    </tr>

                    <tr>
                        <th><strong class="star">Campus</strong></th>
                        <td>${student.classInfo[0].campNm}&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <c:if test="${!empty student}">
    		<h3 class="tit">Registration History</h3>
    		<!-- tblForm -->
    		<div class="tblForm">
    			<table>
    				<colgroup>
    					<col width="125px" />
    					<col width="" />
    				</colgroup>
    
    				<tbody>
    					<tr>
    						<th><strong class="">Registration date</strong></th>
    						<td>${student.regDate}&nbsp;</td>
    					</tr>
                        <c:if test="${!empty student.quitDate}">
        					<tr>
        						<th><strong class="">Withdraw Date</strong></th>
        						<td>${student.quitDate}&nbsp;</td>
        					</tr>
                        </c:if>
    				</tbody>
    			</table>
    		</div>
        </c:if>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:studentEdit()" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>