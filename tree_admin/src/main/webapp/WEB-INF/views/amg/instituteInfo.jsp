<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		function infoEdit (seq) {
			$("#infoForm").attr("action", "${CONTEXT_PATH}/amg/instituteEdit.do");
			$("#infoForm").submit();
		}
		
		function insList() {
            window.opener.reloadPage();
            self.close();
		}
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

		<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Institute Information</h2>
			<%-- <a href="javascript:insList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a> --%>
		</div>
		<form name="infoForm" id="infoForm" method="GET" action="">
		
        <h3 class="tit">Basic Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">Institute name</strong></th>
						<td>${insInfo.name}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Institute code</strong></th>
						<td>${insInfo.id}&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Nationality</strong></th>
						<td><c:if test="${insInfo.nationNm ne null}">${insInfo.nationNm}</c:if>&nbsp;</td>
					</tr>

					<tr>
						<th><strong class="star">Logo</strong></th>
						<td>
							<div class="insLogo">
								<c:choose>
                                    <c:when test="${insInfo.logoPath eq null || insInfo.logoPath eq ''}">
                                        <img src="${CONTEXT_PATH}/images/common/no-image0.png" alt="" class="thum"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${tree:thumbnailPath('Y',CONTEXT_PATH, insInfo.logoPath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
							</div>
						</td>
					</tr>

					<tr>
						<th><strong class="">Information</strong></th>
						<td><c:if test="${!empty insInfo.info}">
                                <c:set var="newline" value="<%= \"\n\" %>" />
                                ${fn:replace(insInfo.info, newline, '<br/>')}
                            </c:if>
                            &nbsp;
                        </td>
                        
					</tr>

					<tr>
						<th><strong class="star">Status</strong></th>
						<td>
							<c:choose>
								<c:when test="${insInfo.useYn eq 'Y'}"><span class="txtBlue">Active</span></c:when>
								<c:otherwise><span class="txtRed">Inactive</span></c:otherwise>
							</c:choose>
                            &nbsp;
						</td>
					</tr>

					<tr>
						<th><strong class="star">Registration date</strong></th>
						<td>${insInfo.regDate}&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:infoEdit()" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
			</div>
		</div>
	</form>
	</div></div>
</div>
</body>
</html>