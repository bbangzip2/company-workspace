<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
        $(document).ready(function() {
/*             $("#useYn").change(function(event) {
                $("#classForm #pageNo").val(1);
                $("#classForm").attr("action", "${CONTEXT_PATH}/amg/classInfoList.do");
                $("#classForm").submit();
            }); */
            $("#classNm").keypress(function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    searchList();
                }
            });
            
            
            $("#eDate").change(function(){
            	
            	
            	$( "#eDate" ).datepicker({
            		minDate : $("#sDate").val(),
            		dateFormat: "yy-mm-dd",
            		dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
            		showMonthAfterYear: false,
            		monthNames: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sep.", "Oct.", "Nov.", "Dec." ]
            	});
            })
            
            
        });
        
        // 등록 화면 이동  
        function classInsert() {
            $("#classForm").attr("action", "${CONTEXT_PATH}/amg/classInsert.do");
            $("#classForm").submit();
        }
        
        // 수정 화면 이동 
        function classView(id) {
            $("#searchForm #classSeq").val(id);
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/classInfoView.do");
            $("#searchForm").submit();
        }
        
        function getList(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            $("#classForm").attr("action", "${CONTEXT_PATH}/amg/classInfoList.do");
            $("#classForm").submit();
        }
        
        function searchList() {
/*             var searchKey = $("#searchKey").val().trim();
            if(searchKey == "") {
                alert("검색어를 입력해주세요.");
                return;
            } */
            
            var startDate = $("#classForm #sDate").val();
            var endDate = $("#classForm #eDate").val();
            var campSeq = $("#classForm #campSeq").val();
            var useYn = $("#classForm #useYn").val();
            var classNm = $("#classForm #classNm").val();
            
            
            $("#searchForm #startDate").val(startDate);
            $("#searchForm #endDate").val(endDate);
            $("#searchForm #campSeq").val(campSeq);
            $("#searchForm #useYn").val(useYn);
            $("#searchForm #classNm").val(classNm);
            
            
            $("#searchForm").attr("action", "${CONTEXT_PATH}/amg/classInfoList.do");
            $("#searchForm").submit();
        }
        
        // 페이징에서 선택시 호출되는 스크립트 
        function goPage(page_no){
            $("#searchForm #pageNo").val(page_no);
            $("#searchForm").submit();
        }
        
        // 학생 리스트로 이동 
        function goStudentList(classSeq, campSeq, progSeq){
        	
        	var url = "${CONTEXT_PATH}/amg/studentList.do?clsSeq="+classSeq+"&campSeq="+campSeq+"&progSeq="+progSeq;
        	location.href = url; 
        }
        
        function changeOrderType(orderBy) {
            var orgSort = $("#sort").val();
            var orgOrderBy = $("#orderBy").val();
            var sort = "ASC";
            if(orgOrderBy == orderBy) {
                if(orgSort == "ASC") {
                    sort = "DESC";
                } else {
                    sort = "ASC";
                }
            }
            $("#orderBy").val(orderBy);
            $("#sort").val(sort);
            searchList();
        }
        
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Class</h2>
        </div>
        
        <form name="searchForm" id="searchForm" method="get" action="">
            <input type="hidden" name="mbrId" id="mbrId" value="${userDetails.username}"/>
            <input type="hidden" name="orderBy" id="orderBy" value="${listCondition.orderColumn}" />
            <input type="hidden" name="sort" id="sort" value="${listCondition.sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${listCondition.pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="startDate" id="startDate" value="${listCondition.startDate }" />
            <input type="hidden" name="endDate" id="endDate" value="${listCondition.endDate }" />
            <input type="hidden" name="campSeq" id="campSeq" value="${listCondition.campSeq }" />
            <input type="hidden" name="useYn" id="useYn" value="${listCondition.useYn }" />
            <input type="hidden" name="classNm" id="classNm" value="${listCondition.classNm }" />
            <input type="hidden" name="classSeq" id="classSeq" value=""/> <!-- 선택한 클래스 아이디  -->
        </form>
        
        
        
        <form name="classForm" id="classForm" method="get" action="">
        <input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
        <input type="hidden" name="classSeq" id="classSeq" value=""/> <!-- 선택한 클래스 아이디  -->
        
        <!-- selectArea -->
        <div class="selectArea">
            <span class="datePicker">
                <label for="">Period</label>
                <input type="text" id="sDate" name="startDate" value="${listCondition.startDate }" class="text datepicker" style="width: 80px"/>
                ~
                <input type="text" id="eDate" name="endDate" value="${listCondition.endDate }"  class="text datepicker" style="width: 80px"/>
            </span>

            <select name="campSeq" id="campSeq">
                <option value="">Campus</option>
                <c:forEach items="${campList}" var="campInfo" varStatus="status">
                    <option value="${campInfo.cd }"  <c:if test="${campInfo.cd eq listCondition.campSeq }"> selected </c:if>>${campInfo.cdSubject }</option>
                </c:forEach>
            </select>

            <select name="useYn" id="useYn">
                <option value="" <c:if test="${listCondition.useYn eq ''}">selected</c:if>>Status</option>
                <option value="Y" <c:if test="${listCondition.useYn eq 'Y'}">selected</c:if>>Active</option>
                <option value="N" <c:if test="${listCondition.useYn eq 'N'}">selected</c:if>>Inactive</option>
            </select>

            <input type="text" class="text" name="classNm" id="classNm" style="width: 120px;" placeholder="Class" value="${listCondition.classNm}" />
            
            <a href="javascript:searchList()" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
            <a href="javascript:classInsert()" class="btnRed"><span>+ New</span></a>
            
        </div>
        <!-- //selectArea -->

        </form>
        
        <!-- 리스트 타입 -->
        <div class="tbList1">
            <table>
                <colgroup>
                    <col width="6%" />
                    <col width="*" />
                    <col width="12%" />
                    <col width="9%" />
                    <col width="10%" />
                    <col width="9%" />
                    <col width="9%" />
                    <col width="9%" />
                    <col width="6%" />
                    <col width="6%" />
                    <col width="6%" />
                </colgroup>

                <thead>
                    <tr>
                        <th>NO</th>
                        <th><a href="javascript:changeOrderType('campNm');">Campus
                        <c:if test="${listCondition.orderColumn ne 'campNm'}">
                        ▲
                        </c:if>                             
                        <c:if test="${listCondition.orderColumn eq 'campNm'}">
                        <c:choose>
                            <c:when test="${listCondition.sort eq 'ASC'}">
                                ▲
                            </c:when>
                            <c:otherwise>
                                ▼
                            </c:otherwise>
                        </c:choose>
                        </c:if>                        
                        </a></th>
                        <th>Class<br />name</th>
                        <th>Class<br />code</th>
                        <th>Program</th>
                        <th>Teacher</th>
                        <th><a href="javascript:changeOrderType('startYmd');">Period
                        <c:if test="${listCondition.orderColumn ne 'startYmd'}">
                        ▲
                        </c:if>                          
                        <c:if test="${listCondition.orderColumn eq 'startYmd'}">
                        <c:choose>
                            <c:when test="${listCondition.sort eq 'ASC'}">
                                ▲
                            </c:when>
                            <c:otherwise>
                                ▼
                            </c:otherwise>
                        </c:choose>
                        </c:if>                         
                        </a></th>
                        <th>Time</th>
                        <th>Students</th>
                        <th>Status</th>
                        <th>View</th>
                    </tr>
                </thead>

                <tbody>
                <c:if test="${empty classInfoList }">
                    <tr><td  colspan="11">No data found.</td></tr>
                
                </c:if>
                <c:forEach items="${classInfoList}" var="classInfo" varStatus="status"> 
                    <tr>
                        <td>${classInfo.rnum }</td>
                        <td class="aLeft">${classInfo.campNm }</td>
                        <td>${classInfo.classNm }</td>
                        <td>${classInfo.classSeq}</td>
                        <td>${classInfo.progNm }</td>
                        <td>
                        <c:forEach items="${classInfo.teacherList }" var="tchItem" varStatus="status">
                            ${tchItem.name }(${tchItem.mbrId })<br/><c:if test="${status.count ne 1}"></c:if> 
                        </c:forEach>
                        </td>
                        <td>${classInfo.startYmd }<br />~${classInfo.endYmd }</td>
                        <td>${classInfo.wkNm }<br/> ${classInfo.startHm }~${classInfo.endHm }</td>
                        <td><a href="javascript:void(0);" onclick="javascript: goStudentList(${classInfo.classSeq}, ${classInfo.campSeq }, ${classInfo.progSeq });" id="popStudentList" class="txtBlue"><u>${classInfo.studentCount }</u></a></td>
                        <td>
	                        <c:choose>
	                            <c:when test="${classInfo.useYn eq 'N' }"><span class="txtBlue">Inactive</span></c:when>
	                            <c:otherwise><span class="txtRed">Active</span></c:otherwise>
	                        </c:choose>
                        </td>
                        <td><a href="javascript:classView(${classInfo.classSeq});" id = "showClassInfo" class="btnBlue"><span>View</span></a></td>
                    </tr>
                                    
                </c:forEach>
                </tbody>
            </table>
        </div>
        <!-- //리스트 타입 -->

        <div class="paging">
            <tree:pagination page_no="${listCondition.pageNo }" total_cnt="${totalCount }" page_size="${listCondition.pageSize }" page_group_size="10" jsFunction="go_page"></tree:pagination>
        </div>

    </div></div>
</div>
</body>
</html>