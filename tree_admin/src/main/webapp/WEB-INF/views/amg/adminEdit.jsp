<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
	    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    
    <script type="text/javascript">
    
    	<c:choose>
    		<c:when test="${mode eq 'Edit'}">
    			var idCheck = true;
    			var url = "/adminModify.do";
    		</c:when>
    		<c:otherwise>
    			var idCheck = false;
    			var url = "/adminAdd.do";
    		</c:otherwise>
    	</c:choose>	
    	
    	var mbrGrade = "${mbrGrade}";
    	var iname = unescape('${iname}');
  
    	<c:choose>
		<c:when test="${mode ne 'View'}">
			var checkUnload = true;
	        $(window).on("beforeunload", function(){
	            if(checkUnload) return "The data haven't been saved yet. Do you still want to leave th page?";
	        });
        </c:when>
        </c:choose>

    	function adminEdit(mbrId) {
			$("#cmpusNm").val(iname);
			$("#mbrOrgId").val(mbrId);
			$("#adminForm").attr("action", "${CONTEXT_PATH}/amg/adminEdit.do");
			
			checkUnload = false;
			
			$("#adminForm").submit();
		}

		function adminSave() {
			
	    	<c:choose>
	    		<c:when test="${mode eq 'Edit'}">
	    			$("#mbrOrgId").val('${adminData.mbrId}');
	    		</c:when>
	    		<c:otherwise>
	    			var mbrId = $("#mbrId").val();
	    		</c:otherwise>
    		</c:choose>	

    		var pwd = $("#pwd").val();
			var pwdConfirm = $("#pwdConfirm").val();
			var name = $("#nm").val();
			var email = $("#email").val();
			var telNo = $("#telNo").val();
			
			<c:if test="${mode ne 'Edit' }">
				if(mbrId == undefined || mbrId == "") {
					alert("Please enter a ID.");
					return;
				}
				
	        	if (isValidId(mbrId)) {
	                return;
	            }
			</c:if>
			
			<c:if test="${mode ne 'Edit' }">
			if(pwd == undefined || pwd == "") {
				alert("Please enter a PassWord.");
				return;
			}
			
            if (pwd) {
            	if (pwd != pwdConfirm) {
            		alert("The passwords you typed don't match.");
            		$("#pwd").focus();
                    return false;
            	}
            	
            	if(pwd.length < 4) {
            		alert("Error: Password must contain at least four characters!");
            		$("#pwd").focus();
            	    return false;
            	}
            	
            	var format = /[0-9a-z.;\-]/;
            	if (format.test(pwd)) {
            		$("#passwordCheck").text(" Good!").css('color', '#39b54a');
            	} else {
            		$("#passwordCheck").text(" Bad. Try another password!").css('color', '#ef011e');
            		return false;
            	}
            	
            }
			</c:if>
			
			if(name == undefined || name == "") {
				alert("Please enter a Name.");
				return;
			}
			if(!checkEmail(email)) {
				alert("This is not a valid type Email.");
				return;
			}
			
			if(telNo == undefined || telNo == "") {
				alert("Please enter a PhoneNumber.");
				return;
			}
			
			if(!checkPhoneNum(telNo)) {
				alert("This is not a valid type PhoneNumber.")
				return;
			}
            if( !idCheck) {				
				alert("Please check double.");
				return;
			}

            var radioCheck = $(":radio:checked").val();
    		
    		if(radioCheck == "" || radioCheck == undefined ) {
    			alert("Please enter a State.");
    			return;
    		}
			$("#adminForm #memo").val($("textarea").val());

			<c:if test="${mode eq null}">
				if($("#adminForm #selectbox1").val() == "0" || $("#adminForm #selectbox1").val() == undefined){
					alert("Please enter a UserType.");
					return;
				}
				<c:choose>
					<c:when test="${mbrGrade == 'MG122'}">
					</c:when>
					<c:otherwise>
						if($("#adminForm #selectbox1").val() == "2" ){
						 	if($("#adminForm #selectbox2").val() == "0" || $("#adminForm #selectbox2").val() == undefined){
								alert("Please enter a CampusType");
								return;
							} 
						}
					</c:otherwise>
				</c:choose>
			</c:if>
			
			$("#adminForm #useYn").val(radioCheck);
			$("#adminForm").attr("action", "${CONTEXT_PATH}" + url);
			checkUnload = false;
			$("#adminForm").submit();
		}
		
		function checkEmail(email) {
			var format = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
			return format.test(email);
		}
		
		function checkPhoneNum(num) {
			var format = /^[0-9]*$/;
			return format.test(num);
		}
		
		function checkId() {
        	var id = $.trim($("#mbrId").val());
        	
        	if(id == undefined || id == "") {
        		alert("Please enter a ID.");
        		return;
        	}
        	
            $.ajax({            
                type : "post",
                url : "${CONTEXT_PATH}/member/checkMemberId.do",
                beforeSend: function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                datatype : "json",
                data : {userId : id},
                success : function(data){
                    var code = data.code;
                    var msg = data.message;
                    if ( code == "0000" ) {
                    	if(msg == "0") {
                    		alert("This ID is available.");
                    		idCheck = true;
                    	} else {
                    		alert("This ID has been used. Please use a different ID");
                    	}
                    }
                },
                error: function (xhr, ajaxOptions, thrownError){
                    alert("error");
                }, 
                complete:function (xhr, textStatus){
                }  
            });
		}
		
		function clickUploadFile() {
            $("#uploadFile").trigger('click');
        }
		
		function previewImage(targetObj, profile) {
	          var ua = window.navigator.userAgent;

	          if (ua.indexOf("MSIE") > -1) {//ie일때
	          } else { //ie가 아닐때
	              var files = targetObj.files;
	              for ( var i = 0; i < files.length; i++) {

	                  var file = files[i];

	                  if (window.FileReader) { // FireFox, Chrome, Opera 확인.
	                      var reader = new FileReader();
	                      reader.onloadend = (function(aImg) {
	                          return function(e) {
	                              /* aImg.src = e.target.result; */
	                              $("#logoImage").attr('src', e.target.result);
	                              $("#logoImage").css("display", "");
	                          };
	                      })($("#logoImage"));
	                      reader.readAsDataURL(file);
	                  } 
	              }
	         }
	    }

		$(document).ready(function()
   		{
	    	$("#iname").text(iname);
			
        	$("#selectbox2").append("<option value='0'>Campus</option>");
        	// Campus 정보 출력 
   		    $('#selectbox1').change(function(){
   		    	selectVal =  $('#selectbox1 option:selected').val();
   		    	// Intitute
   		    	if ((mbrGrade == "MG100"  || mbrGrade == "MG121"  ) && selectVal =="1" ){
   		    		$('#selectbox2 *').remove();
   		    		$("#selectbox2").append("<option value='0'>Campus</option>");
   		    		$('#selectbox2').attr('disabled',true);
   		    	// Campus 선택	
   		    	} else if ((mbrGrade == "MG100"  || mbrGrade == "MG121") && selectVal =="2" ){
   		    		    $.ajax({            
		                type : "post",
		                url : "${CONTEXT_PATH}/adminGetCampus.do",
		                beforeSend: function(req) {
		                    req.setRequestHeader("Accept", "application/json");
		                },
		                async : false,
		                cache : false,
		                datatype : "json",
		                data : {instituteSeq : ${instituteSeq}},
		                success : function(data){
		                    var code = data.code;
	                    	$('#selectbox2 *').remove();
	       		    		$('#selectbox2').attr('disabled',false);
		                    makeComboBox(data, "selectbox2", "", "Campus");
		                },
		                error: function (xhr, ajaxOptions, thrownError){
		                    alert("error");
		                }, 
		                complete:function (xhr, textStatus){
		                }  
		            }); 
   		    	}
   		    }); // end #selectbox1
   		}); // end document 
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Admin</h2>
			<a href="javascript:history.back();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>
		<form name="adminForm" id="adminForm" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="cSeq" id="cSeq" value="${cSeq}"/>
			<input type="hidden" name="mbrOrgId" id="mbrOrgId" value=""/>
			<input type="hidden" name="useYn" id="useYn" value=""/>
			<input type="hidden" name="cmpusNm" id="cmpusNm" value=""/>
			<input type="hidden" name="mbrGrade" id="mbrGrade" value="${mbrGrade}"/>
			<input type="hidden" name="instituteSeq" id="instituteSeq" value="<c:out value="${instituteSeq}" />"/>
		<h3 class="tit first">Basic Info</h3>
		<!-- tblForm -->
		

		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>
				<tbody>
					<tr>
						<th><strong class="star">User type</strong></th>
						<td id="userType">
								<c:choose>
								<c:when test="${mode eq 'View' or mode eq 'Edit' }">
									<c:choose>
										<c:when test="${adminData.mbrGrade=='MG100'}">
											SuperAdmin
										</c:when>
										<c:when test="${adminData.mbrGrade=='MG121'}">
											Institute Admin
										</c:when>
										<c:when test="${adminData.mbrGrade=='MG122'}">
											Campus Admin
										</c:when>
									</c:choose>
								</c:when>
								
								<c:otherwise>
								   <select id="selectbox1" name="userType">
								   	<option value="0">UserType</option>
								   <c:choose>
								      <c:when test="${mbrGrade== 'MG100'}">
										<option value="1">Institute Admin</option>
										<option value="2">Campus Admin</option>
								       </c:when>
								       <c:when test="${mbrGrade == 'MG121'}">
										<option value="1">Institute Admin</option>
										<option value="2">Campus Admin</option>
								       </c:when>
								       <c:when test="${mbrGrade == 'MG122'}">
						          		<option value="2">Campus Admin</option>
								       </c:when>
								   </c:choose>
								   	</select>
								</c:otherwise>  						
							</c:choose>	
						</td>
					</tr>
					<tr>
						<th><strong class="star">ID</strong></th>
						<td>
							<c:if test="${mode eq 'View' or mode eq 'Edit' }">
								<c:if test="${adminData.mbrId ne null}">${adminData.mbrId}</c:if> 
							</c:if>
							<c:if test="${mode eq null }">
								<input type="text" class="text"  name="mbrId" id="mbrId"  style="width: 140px;" value="" />
								<a href="javascript:checkId();" class="btnBlue"><span>Redundant Check</span></a>
							</c:if>
						</td>
					</tr>
					<c:choose>
						<c:when test="${mode eq 'View'}">
<!-- 						<tr>
							<th><strong class="star">Password</strong></th>
							<td>
							</td>
						</tr> -->
						</c:when>
						<c:otherwise>
						<tr>
							<th><strong class="star">Password</strong></th>
							<td>
								<input type="password" class="text" name="pwd" id="pwd" style="width: 140px;" value="" maxlength="12"/>
								<label for="" class="type1 star">Password check</label> <input type="password" class="text"  name="pwdConfirm" id="pwdConfirm" style="width: 140px;" value="" maxlength="12"  /><span id="passwordCheck"></span>
								</br>4~12 characters with letters(a-z), numbers, and special letters
							</td>
						</tr>
						</c:otherwise>
					</c:choose>
					
					<tr>
						<th><strong class="star">Name</strong></th>
						<c:choose>
							<c:when test="${mode eq 'View'}">
								<td>${adminData.nm}</td>
							</c:when>
							<c:otherwise>
								<td><input type="text" class="text"  name="nm" id="nm"  style="width: 140px;" value="<c:if test="${adminData.nm ne null}">${adminData.nm}</c:if>"   /></td>
							</c:otherwise>
						</c:choose>	
					</tr>
										
					<tr>
						<th><strong class="star">Campus</strong></th>
						<td>
						<c:choose>
							<c:when test="${mode ne null}">
								<span id="iname"></span>
							</c:when>
							<c:otherwise>
									<c:choose>
										<c:when test="${mbrGrade == 'MG122'}">
											${sector}	
										</c:when>
										<c:otherwise>
											<select id="selectbox2" name="sector">
										</c:otherwise>
									</c:choose>							
								</select>
							</c:otherwise>
						</c:choose>	
						</td>
					</tr>

					<tr>
						<th><strong class="">Image</strong></th>
						<td>
                            <div id="profile" class="profile">
                                <c:choose>
                                    <c:when test="${adminData.profilePhotoPath eq null}">
                                        <img id="logoImage" src="${CONTEXT_PATH}/images/common/no-image_2.png" class="thum"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img id="logoImage" src="${tree:thumbnailPath('Y',CONTEXT_PATH, adminData.profilePhotoPath)}" alt="" class="thum"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${mode ne 'View'  }">
	                                <a class="btnBlue" onclick="javascript:clickUploadFile();"><span>Search</span></a>
	                                <input type="file" id="uploadFile" name="uploadFile" onchange="previewImage(this,'profile')" style="display:none"/>
                                </c:if>
                            </div>
                        </td>
					</tr>
					<tr>
						<th><strong class="star">E-mail</strong></th>
						<c:choose>
							<c:when test="${mode eq 'View'}">
								<td>${adminData.email}</td>
							</c:when>
							<c:otherwise>
								<td><input type="text" class="text" name="email" id="email" style="width: 244px;" value="<c:if test="${adminData.email ne null}">${adminData.email}</c:if>" /></td>
							</c:otherwise>
						</c:choose>	
					</tr>

					<tr>
						<th><strong class="star">Cell-phone</strong></th>
						<c:choose>
							<c:when test="${mode eq 'View'}">
								<td>${adminData.telNo}</td>
							</c:when>
							<c:otherwise>
								<td><input type="text" class="text"  name="telNo" id="telNo" style="width: 244px;"  maxlength="15"  value="<c:if test="${adminData.telNo ne null}">${adminData.telNo}</c:if>" /></td>
							</c:otherwise>
						</c:choose>	
					</tr>

					<tr>
						<th><strong class="">Memo</strong></th>
						<c:choose>
							<c:when test="${mode eq 'View'}">
								<td>${adminData.memo}</td>
							</c:when>
							<c:otherwise>
								<td><textarea style="width: 98%; height: 148px;" class="textarea" name="memo" id="memo"  ><c:if test="${adminData.memo ne null}">${adminData.memo}</c:if></textarea></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">Other Info</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="">Status</strong></th>
						<td>
							<c:choose>
								<c:when test="${mode eq 'View'}">
									 <c:if test="${adminData.useYn eq 'Y'}"><label for="">Active</label></c:if>
									 <c:if test="${adminData.useYn eq 'N'}"><label for="">Inactive</label></c:if>
								</c:when>
								<c:otherwise>
									<input type="radio" class="rdo" name="status" value="Y"  <c:if test="${adminData.useYn eq 'Y'}">checked </c:if>/><label for="">Active</label>
									<input type="radio" class="rdo" name="status" value="N" <c:if test="${adminData.useYn eq 'N'}">checked </c:if> /><label for="">Inactive</label>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<c:if test="${mode eq 'View'}">
						<tr>
							<th><strong class="">Last update</strong></th>
							<td>
								${adminData.nm}&nbsp;${adminData.regDttm}
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

		<div class="btnArea">
			<div class="sideR">
				<c:choose>
					<c:when test="${mode eq null or mode eq 'Edit' }">
						<a href="javascript:adminSave();" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
					</c:when>
					<c:otherwise>
						<a href="javascript:adminEdit('${adminData.mbrId}')" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>