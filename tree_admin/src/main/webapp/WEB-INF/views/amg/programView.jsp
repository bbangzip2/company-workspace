<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<title>${INS_TITLE }</title>
    
    <!--################################## common js & css include ################################## --> 
    <%@ include file="../include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
	    var redOk = false;
	    
	    $(document).ready(function() {
	        
	    });
    
	    
		function programList() {
			$("#searchForm").attr("action", "${CONTEXT_PATH}/amg/programList.do");
			$("#searchForm").submit();
		}
		
        function programEdit() {
            $("#progForm").attr("action", "${CONTEXT_PATH}/amg/programEdit.do");
            $("#progForm").submit();
        }
        
		
	</script>
</head>

<body>
<div id="wrapper">
	<!-- header -->
	<%@ include file="../include/incHeader.jsp" %>
	<!-- //header -->

    <div id="container" class="infoView"><div id="contents">
        <div class="hGroup">
            <h2>Program</h2>
            <a href="javascript:void(0);" onclick="javascript:programList();" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
        </div>
        
        <form name="searchForm" id="searchForm" method="GET">
            <input type="hidden" name="orderBy" id="orderBy" value="${orderBy}" />
            <input type="hidden" name="sort" id="sort" value="${sort}" />
            <input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"/> <!-- 현재 페이지  -->
            <input type="hidden" name="useYn" id="useYn" value="${useYn }"/> 
            <input type="hidden" name="progNm" id="progNm" value="${progNm }"/>
        </form>
                
        <form name="progForm" id="progForm" method="post" action="">
        <input type="hidden" name="insInfoSeq" id="insInfoSeq" value="${userDetails.insSeq}"/>
        <input type="hidden" name="progSeq" id="progSeq" value="${progInfo.progSeq}"/>
        <input type="hidden" name="useYn" id="useYn" value="${campus.useYn}"/>
        
        <h3 class="tit first">Basic Info</h3>
        <!-- tblForm -->
        <div class="tblForm">
            <table>
                <colgroup>
                    <col width="125px" />
                    <col width="" />
                </colgroup>

                <tbody>
                    <tr>
                        <th><strong class="star">Program</strong></th>
                        <td>${progInfo.progNm }</td>
                    </tr>

                    <tr>
                        <th><strong class="">Comment</strong></th>
                        <td>
                        <c:if test="${!empty progInfo.progDescr}">
                                <c:set var="newline" value="<%= \"\n\" %>" />
                                ${fn:replace(progInfo.progDescr, newline, '<br/>')}
                        </c:if>&nbsp;                        
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="star">Status</strong></th>
                        <td>
	                        <c:if test="${progInfo.useYn eq 'Y' }"> Active </c:if>
	                        <c:if test="${progInfo.useYn eq 'N' }"> Inactive </c:if>
                        </td>
                    </tr>

                    <tr>
                        <th><strong class="">Last update</strong></th>
                        <td>${progInfo.modDate }</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //tblForm -->

        <!-- 학습구성 -->
        <h3 class="tit">Program arrangement</h3>
        <div class="studyMakeup">
            <div class="sideL">
                <strong class="item">Selected Items (<em class="txtRed" id="selectItemCnt">${fn:length(pItemList) }</em>)</strong>

                 <!-- listArea -->
                <div class="listArea">
                    <ul>
                    <c:forEach items="${pItemList}" var="insItem" varStatus="status">
                        <li id="${insItem.prodSeq }-${insItem.itemType }">${insItem.prodTitle }<input type="hidden" name="itemVal" id="${insItem.prodSeq }-${insItem.itemType }" value="${insItem.prodSeq }#${insItem.itemType }"/></li>
                    </c:forEach>
                    </ul>
                </div>
                <!-- //listArea -->
            </div>
            <!-- //sideR -->
        </div>
        <!-- //학습구성 -->
        </form>
                
        <div class="btnArea">
            <div class="sideR">
                <a href="javascript: programEdit();" id="eventBtn" class="btn btnRed"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Edit</strong></a>
            </div>
        </div>

    </div></div>
</div>
</body>
</html>