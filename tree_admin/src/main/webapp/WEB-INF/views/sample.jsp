<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<title>Tree</title>
	
    <!--################################## common js & css include ################################## --> 
    <%@ include file="./include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->
    	
	<script type="text/javascript">
		$(document).ready(function(){

			//console.debug(">> sessionScope mbrGrade="+'${sessionScope.VSUserSession.mbrGrade}');
			$(".btnRed").click(function(){

				$(location).attr('href',"${CONTEXT_PATH}/sampleInsert.do");
			});

		});


		function sampleInfo(id) {

			$("#mbrId").val(id);

			$("#listForm").attr("action", "${CONTEXT_PATH}/sampleInfo.do");
			$("#insertForm").attr("method", "POST");
			$("#listForm").submit();
		}
		
		// 페이징에서 선택시 호출되는 스크립트 
		function go_page(page_no){
		   $("#listForm #pageNo").val(page_no);
		   $("#listForm").submit();
		}
		
	</script>
</head>

<body>
<div id="wrapper">

	<!--################################## incHeader ################################## -->
	<%@ include file="include/incHeader.jsp" %>
	<!--################################## // incHeader ################################## -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Administrator</h2>
		</div>
		<form name="listForm" id="listForm" method="post" action="">
		<!-- selectArea -->
		<div class="selectArea">
			<select name="" id="">
				<option value="">User Type</option>
			</select>
			<select name="" id="">
				<option value="">대치 Campus</option>
			</select>
			<select name="" id="">
				<option value="">Status</option>
			</select>

			<input type="text" class="text" style="width: 120px;" value="Name/ID 검색" />

			<a href="javascript:void(0);" class="btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/zoom.gif" alt="" />Search</span></a>
			<a href="javascript:void(0);" class="btnRed"><span>+ New</span></a>
		</div>
		<!-- //selectArea -->

		<!-- 리스트 타입 -->
		<div class="tbList1">
			<table>
				<colgroup>
					<col width="6%" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="6%" />
				</colgroup>

				<thead>
					<tr>
						<th>NO</th>
						<th><a href="">User Type ▲</a></th>
						<th>ID</th>
						<th>소속</th>
						<th>Name</th>
						<th>연락처</th>
						<th>Status</th>
						<th><a href="">등록일▼</a></th>
						<th>View</th>
					</tr>
				</thead>

				<tbody>
					<c:if test="${empty sampleList}">
						<tr>
							<td colspan="9">조회된 데이터가 없습니다.</td>
						</tr>
					</c:if>
					<c:if test="${!empty sampleList}">
						<c:forEach var="item" items="${sampleList}" varStatus="status">
							<tr>
								<td>${item.rn}</td>
								<td><strong>SuperAdmin</strong></td>
								<td>${item.nickName}</td>
								<td>본사</td>
								<td>${item.nm}</td>
								<td>${item.telNo}</td>
								<td>
									<c:choose>
										<c:when test="${item.useYn == 'Y'}"><span class="txtBlue">Active</span></c:when>
										<c:otherwise><span class="txtRed">Inactive</span></c:otherwise>
									</c:choose>
								</td>
								<td>${item.regDttm}</td>
								<td><a href="javascript:sampleInfo('${item.mbrId}');" class="btnBlue"><span>view</span></a></td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //리스트 타입 -->

		<div class="paging">
		    <tree:pagination page_no="${listCondition.pageNo }" total_cnt="${listCnt}" page_size="${listCondition.pageSize }" page_group_size="10" jsFunction="go_page"></tree:pagination>
		</div>
		
		<input type="hidden" name="mbrId" id="mbrId" />
		<input type="hidden" name="pageNo" id="pageNo" value="${listCondition.pageNo }"/> <!-- 현재 페이지  -->
		<input type="hidden" name="pageSize" id="pageSize" value="${listCondition.pageSize }"/> <!-- 페이지당 갯수 (기본 10개 서버에서 셋팅함) -->
		
		</form>
	</div></div>
</div>
</body>
</html>