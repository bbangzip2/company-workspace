<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tree</title>
	
    <!--################################## common js & css include ################################## --> 
    <%@ include file="./include/include.jsp" %>
    <!--//################################## common js & css include ##################################-->

	<script type="text/javascript">
		$(document).ready(function(){

			$("#btnSave").click(function(){
				// pwd check
				if ($("#pwd").val() != $("#pwdCheck").val()) {
					return false;
				}

				$("#useYn").val($(':radio[name="status"]:checked').val());

				// target
				var url = $("#mbrId").val() == "" ? "/sampleAdd.do" : "/sampleModify.do";

				$("#insertForm").attr("action", "${CONTEXT_PATH}" + url);
				$("#insertForm").attr("method", "POST");
				$("#insertForm").submit();
			});
			
            $("#btnDel").click(function(){
                // pwd check
                if ($("#pwd").val() != $("#pwdCheck").val()) {
                    return false;
                }

                $("#useYn").val($(':radio[name="status"]:checked').val());

                $("#insertForm").attr("action", "${CONTEXT_PATH}/sampleRemove.do");
                $("#insertForm").attr("method", "POST");
                $("#insertForm").submit();
            });
            

		});
	</script>
</head>

<body>
<div id="wrapper">

	<!--################################## incHeader ################################## -->
	<%@ include file="include/incHeader.jsp" %>
	<!--################################## // incHeader ################################## -->

	<div id="container" class="infoView"><div id="contents">
		<div class="hGroup">
			<h2>Administrator</h2>
			<a href="${CONTEXT_PATH}/sample.do" class="btn btnGray"><span><img src="${CONTEXT_PATH}/images/common/icon/arrow.gif" alt="" />List</span></a>
		</div>

		<h3 class="tit first">기본정보</h3>
		<form name="insertForm" id="insertForm" method="post" action="${CONTEXT_PATH}/sampleAdd.do">
		<input type="hidden" name="mbrId" id="mbrId" value="${sampleInfo.mbrId}" />
		<input type="hidden" name="useYn" id="useYn" value="${sampleInfo.useYn}" />
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="star">User Type</strong></th>
						<td>
							<select>
								<option value="">Institute Admin</option>
							</select>
						</td>
					</tr>

					<tr>
						<th><strong class="star">ID</strong></th>
						<td>
							<input type="text" class="text" name="nickName" id="nickName" style="width: 140px;" value="${sampleInfo.nickName}" />
							<a href="javascript:void(0);" class="btnBlue"><span>Redundant Check</span></a>
						</td>
					</tr>

					<tr>
						<th><strong class="star">Password</strong></th>
						<td>
							<input type="password" class="text" name="pwd" id="pwd" style="width: 140px;" value="${sampleInfo.pwd}" />
							<label for="" class="type1 star">Password check</label> <input type="password" class="text" name="pwdCheck" id="pwdCheck" style="width: 140px;" value="${sampleInfo.pwd}" />
						</td>
					</tr>

					<tr>
						<th><strong class="star">Name</strong></th>
						<td><input type="text" class="text" name="nm" id="nm" style="width: 140px;" value="${sampleInfo.nm}" /></td>
					</tr>

					<tr>
						<th><strong class="star">소속</strong></th>
						<td>
							<select>
								<option value="">대치Campus</option>
							</select>
						</td>
					</tr>

					<tr>
						<th><strong class="star">E-mail</strong></th>
						<td><input type="text" class="text" name="eMail" id="eMail" style="width: 244px;" value="${sampleInfo.eMail}" /></td>
					</tr>

					<tr>
						<th><strong class="star">Cell-phone</strong></th>
						<td><input type="text" class="text" name="telNo" id="telNo" style="width: 244px;" value="${sampleInfo.telNo}" /></td>
					</tr>

					<tr>
						<th><strong class="">Memo</strong></th>
						<td><textarea name="memo" id="memo" style="width: 98%; height: 148px;" class="textarea">${sampleInfo.memo}</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->

		<h3 class="tit">기타정보</h3>
		<!-- tblForm -->
		<div class="tblForm">
			<table>
				<colgroup>
					<col width="125px" />
					<col width="" />
				</colgroup>

				<tbody>
					<tr>
						<th><strong class="">Status</strong></th>
						<td>
							<input type="radio" class="rdo" name="status" value="Y" /><label for="">Active</label>
							<input type="radio" class="rdo" name="status" value="N" /><label for="">Inactive</label>
						</td>
					</tr>

					<tr>
						<th><strong class="">Last Update</strong></th>
						<td>김실장(kimhj) / 2013.12.10 12:30</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //tblForm -->
		<div class="btnArea">
			<div class="sideR">
				<a href="javascript:void(0);" class="btn btnRed" id="btnSave"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Save</strong></a>
				<a href="javascript:void(0);" class="btn btnRed" id="btnDel"><strong><img src="${CONTEXT_PATH}/images/common/icon/arrow_w.gif" alt="" />Delete</strong></a>
			</div>
		</div>
		</form>
	</div></div>
</div>
</body>
</html>