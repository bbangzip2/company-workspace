<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%

 String root = "/tree_admin/html/";
%>
<head>
<title>- MAP -</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<style type="text/css">
    body, table, tr, td {font-family:돋움, Tahoma, verdana;font-size:12px;line-height:16px;}
    a:link, a:visited, a:active, a:hover {color: #002299;text-decoration: underline;}
    h1 {margin-top:40px;font-family:나눔고딕;}
    .listType1 {padding-left:14px;}
    .tbList {width:100%; border-collapse:collapse; padding:0; margin:0; border-top:3px solid #ccc; border-bottom:3px solid #ccc;}
    .tbList thead {background:#222; color:#fff; padding:4px; text-align:center;}
    .tbList thead th {border:1px solid #ccc;}
    .tbList tbody {background:#fff;text-align:left;}
    .tbList tbody td {border:1px solid #ccc;padding:8px 5px;}
    .tbList tbody td strong {color:red;font-size:11px;}
    .tbList tbody tr:hover {background-color:#EEE}
    .tbList tbody tr.listBar td {background:#eee; font-weight:bold; text-align:center; padding:5px;}
    .tbList tbody tr.listBar td strong {color:red;}
    .tbList tbody tr.link td {background:#ECF0FF;}
    .tbList tbody tr.coding td {background:#FFFFC4;}
    .tbList tbody tr.coding td.title {background:#fff;}
    .tbList tbody tr.end td {background:#D3B9FF;}
    .tbList tbody tr.cancel td {background:#F5F0EF;text-decoration:line-through;}
    .tbList tbody tr.cancel td.cmt {text-decoration:none;}
    .tbList tbody tr.wait td {background:#f3ffea;}
    .tbList tbody td.flash {background:#D3B9FF;}
</style>
</head>

<body>
<h1>admin</h1>
<%
for (Enumeration e = session.getAttributeNames(); e.hasMoreElements(); ) {     
    String attribName = (String) e.nextElement();
    Object attribValue = session.getAttribute(attribName);
%>
<BR><%= attribName %> - <%= attribValue %>

<%
}
%>
<table class="tbList" summary="개발 코딩 IA">
    <colgroup>
    <col width="%" />
    <col width="%" />
    <col width="%" />
    <col width="%" />
    <col width="%" />
    <col />
    <col width="%" />
    <col width="%" />
    <col width="%" />
    <col width="%" />
    </colgroup>
    <thead>
        <tr>
            <td>1Depth</td>
            <td width="194">2Depth</td>
            <td width="194">3Depth</td>
            <td width="240">4Depth</td>
            <td>기능 및 Tab</td>
            <td>Directory</td>
            <td>file name</td>
            <td>CODE</td>
            <td>완료일</td>
            <td>비고</td>
        </tr>
    </thead>
    <tbody>
        <tr class="listBar">
            <td colspan="10">common</td>
        </tr>
        <tr>
            <td>로그인</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>/</td>
            <td><a href="<%=root %>login.html">login.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td>공통 팝업</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>/</td>
            <td><a href="<%=root %>popup/application.html">application.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td>공통 팝업</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>/</td>
            <td><a href="<%=root %>popup/application_result.html">application_result.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr class="listBar">
            <td colspan="10">운영 시스템</td>
        </tr>
        <tr>
            <td>운영 시스템</td>
            <td>main</td>
            <td></td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/system.html">system.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Info</td>
            <td>view</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/info_view.html">info_view.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Info</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/info_write.html">info_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Program</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/program_list.html">program_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Program</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/program_write.html">program_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Campus</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/campus_list.html">campus_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Campus</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/campus_write.html">campus_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Class</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/class_list.html">class_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Class</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/class_write.html">class_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>반배정</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/assignClass_list.html">assignClass_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>반배정</td>
            <td>pop</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/pop_classSetting.html">pop_classSetting.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Students</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/student_list.html">student_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Students</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/student_write.html">student_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Students</td>
            <td>pop</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/pop_studentInfo.html">pop_studentInfo.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Teacher</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/teacher_list.html">teacher_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Teacher</td>
            <td>view</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/teacher_view.html">teacher_view.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Teacher</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/teacher_write.html">teacher_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>admin</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/admin_list.html">admin_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>admin</td>
            <td>write</td>
            <td></td>
            <td></td>
            <td>/system</td>
            <td><a href="<%=root %>system/admin_write.html">admin_write.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr class="listBar">
            <td colspan="10">LCMS</td>
        </tr>

        <tr>
            <td>LCMS</td>
            <td>Program</td>
            <td>list</td>
            <td></td>
            <td></td>
            <td>/lcms</td>
            <td><a href="<%=root %>lcms/program_list.html">program_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Program</td>
            <td>view</td>
            <td></td>
            <td></td>
            <td>/lcms</td>
            <td><a href="<%=root %>lcms/program_view.html">program_view.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>


        <tr>
            <td></td>
            <td>Book</td>
            <td></td>
            <td></td>
            <td></td>
            <td>/lcms</td>
            <td><a href="<%=root %>lcms/book_list.html">book_list.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Book</td>
            <td>book_both</td>
            <td></td>
            <td></td>
            <td>/lcms</td>
            <td><a href="<%=root %>lcms/book_info.html">book_info.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Book</td>
            <td>book_both</td>
            <td></td>
            <td></td>
            <td>/lcms</td>
            <td><a href="<%=root %>lcms/book_lesson.html">book_lesson.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>Book</td>
            <td>book_both</td>
            <td></td>
            <td></td>
            <td>/lcms</td>
            <td><a href="<%=root %>lcms/book_both.html">book_both.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>

        <tr class="listBar">
            <td colspan="10">LMS</td>
        </tr>
        <tr>
            <td>system</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>/lms</td>
            <td><a href="<%=root %>lms/formStyle.html">**.html</a></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
    </tbody>
    <tbody>
    </tbody>
</table>
</body>
</html>