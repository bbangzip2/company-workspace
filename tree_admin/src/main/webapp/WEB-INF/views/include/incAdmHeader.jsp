<%/*
--------------------------------------------------------------------------------
    PROJECT NAME : tree admin
--------------------------------------------------------------------------------
    - 단위업무명 : incAdmHeader
    - 파일명    : incAdmHeader.jsp
    - 서비스명   : 공통 프로그램
    - 처리설명   : super admin이 사용하는 헤더 jsp
    - 최초작성일 : 2014-01-23
    - 관련사항   : 
    - 작성자     : hong
    - 소속       : 비상ESL 
    - 비고       : 
--------------------------------------------------------------------------------
    - 수정일자   :  
    - 수정자     : 
    - 소속       : 
    - 수정내용   : 
--------------------------------------------------------------------------------
*/%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- header -->
<div id="header">
	<div class="header">
		<h1><img src="${CONTEXT_PATH}/images/common/logo.jpg" alt="tree"></h1>
		<div id="gnb">
			<c:forEach var="item" items="${userDetails.accessMenus}" varStatus="status">
				<c:if test="${item.depthSeq eq 0}">
				<c:if test="${status.first}"><c:if test="${empty sessionScope.gnbMenuSeq}"><c:set var="gnbMenuSeq" scope="session" value="${item.menuSeq}"/>########## : ${item.menuSeq }</c:if></c:if>
					<a href="<c:choose><c:when test='${item.linkSect eq "D" }'><c:url value="${item.linkUrl}"/></c:when><c:otherwise>javascript:void(0);</c:otherwise></c:choose>" <c:if test="${sessionScope.gnbMenuSeq eq item.menuSeq }">class="on"</c:if>><img src="${IMG_PATH}${tree:getMenuImage(item.menuSeq)}"></a>
				</c:if>
			</c:forEach>
		</div>
		<div class="userInfo">
			<span class="pic"><img src="<c:choose><c:when test="${empty userDetails.profilePhotoPath}">${CONTEXT_PATH}/images/common/thum.jpg</c:when><c:otherwise>${tree:thumbnailPath('Y',CONTEXT_PATH, userDetails.profilePhotoPath)}</c:otherwise></c:choose>" alt="${userDetails.nickname}"></span>	
			<strong><span class="txtBlue">${userDetails.name}</span>(${userDetails.username})</strong><span class="line">|</span><a href="${CONTEXT_PATH}/member/treeSignOut.do">Logout</a>
		</div>
	</div>
</div>
<!-- //header -->