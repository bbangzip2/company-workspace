
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

	<!-- css in include.jsp -->
    <link rel="stylesheet" type="text/css" href="<%=CSS_PATH%>/common.css" />
    <link rel="stylesheet" type="text/css" href="<%=CSS_PATH%>/popup.css" />
    <link rel="stylesheet" type="text/css" href="<%=CSS_PATH %>/reset.css" />

    <!--js  in include.jsp -->
    <script type="text/javascript" src="<%=JS_PATH %>/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery-form.js" ></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery.validate.js" ></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/spin.min.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery.cookie.js"></script>
    
    <script type="text/javascript" src="<%=webRoot%>/common/js/design.js"></script>
        
    <script type="text/javascript" src="<%=JS_PATH%>/tree_common.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery-blockUi-1.7.js"></script>

