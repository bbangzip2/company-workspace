<%/*
--------------------------------------------------------------------------------
    PROJECT NAME : tree admin
--------------------------------------------------------------------------------
    - 단위업무명 : incLeft
    - 파일명    : incLeft.jsp
    - 서비스명   : 공통 프로그램
    - 처리설명   : 사용자단 공통 LNB JSP 
    - 최초작성일 : 2014-01-23
    - 관련사항   : 
    - 작성자     : hong
    - 소속       : 비상ESL 
    - 비고       : 
--------------------------------------------------------------------------------
    - 수정일자   :  
    - 수정자     : 
    - 소속       : 
    - 수정내용   : 
--------------------------------------------------------------------------------
*/%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<script type="text/javascript">
		/**
		* contetn fetcher popup open 
		*/
	    function contentFetPop(){
	    	var option ="width=1020, height=708";
	    	var url ="${CONTEXT_PATH}/contents/fetcherList.do";
	    	var pop = window.open(url, "fetcherPop", option);
	           pop.focus();
	       }
	</script>


    <div id="lnb">
        <div class="menu">LCMS</div>
        <ul>
			<c:forEach var="item" items="${userDetails.accessMenus}" varStatus="status">
				<c:if test="${status.first}"><c:if test="${empty sessionScope.lnbMenuSeq}"><c:set var="lnbMenuSeq" scope="session" value="${item.menuSeq}"/></c:if></c:if>
				<c:if test="${item.grpSeq eq 3 and item.depthSeq eq 1}">
					<li><a href="<c:choose><c:when test='${item.linkSect eq "D" }'><c:url value="${item.linkUrl}"/></c:when><c:otherwise>javascript:void(0);</c:otherwise></c:choose>" <c:if test="${sessionScope.lnbMenuSeq eq item.menuSeq }">class="on"</c:if>>${item.menuNm}</a></li>
				</c:if>
			</c:forEach>        
        </ul>
		<div class="banner">
				<%-- <a href="#"><img src="${CONTEXT_PATH}/images/common/treeBanner1.jpg" alt="TReE Contents Creator " /></a> --%>
				<a href="#" onclick="javascript: contentFetPop();"><img src="${CONTEXT_PATH}/images/common/treeBanner2.jpg" alt="TReE Contents Fetcher" /></a>
		</div>
			        
    </div>
