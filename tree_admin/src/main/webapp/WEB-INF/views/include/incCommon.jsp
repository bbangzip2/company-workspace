<%/*
--------------------------------------------------------------------------------
    PROJECT NAME : tree admin
--------------------------------------------------------------------------------
    - 단위업무명 : incCommon
    - 파일명    : incCommon.jsp
    - 서비스명   : 공통 프로그램
    - 처리설명   : 모든 jsp에서 include되는 파일
    - 최초작성일 : 2014-01-23
    - 관련사항   : 
    - 작성자     : hong
    - 소속       : 비상ESL 
    - 비고       : 
--------------------------------------------------------------------------------
    - 수정일자   :  
    - 수정자     : 
    - 소속       : 
    - 수정내용   : 
--------------------------------------------------------------------------------
*/%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="org.springframework.web.bind.ServletRequestUtils"%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tree" uri="http://www.treeadmin.com/treeadminTlds" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication property="principal" var="userDetails"/>

<c:set var="CONTEXT_PATH" value="${pageContext.request.contextPath}"/>
<c:set var="JS_PATH" value="${CONTEXT_PATH}/js"/>
<c:set var="IMG_PATH" value="${CONTEXT_PATH}/images"/>
<c:set var="CSS_PATH" value="${CONTEXT_PATH}/common/css"/>
<c:set var="MAIN_TITLE" value="TreE administration"/>
<c:set var="INS_TITLE" value="Institute management system"/>
<c:set var="CDN_URL" value="http://tstcdn.treebooster.com"/>

<sec:authorize access="isAuthenticated()">
<c:set var="doneMenuLoop" value="false" />
<c:forEach var="item" items="${userDetails.accessMenus}" varStatus="status">
	<c:if test="${not doneMenuLoop}">
		<c:if test="${not empty item.linkUrl and fn:indexOf(requestScope['javax.servlet.forward.request_uri'],item.linkUrl) != -1}">
			<c:choose>
				<c:when test="${item.depthSeq eq 0}">
					<c:set var="gnbMenuSeq" scope="session" value="${item.menuSeq }"/>
				</c:when>
				<c:when test="${item.depthSeq eq 1}">
					<c:set var="lnbMenuSeq" scope="session" value="${item.menuSeq }"/>
					<c:set var="doneMenuLoop" value="true" />
				</c:when>
			</c:choose>
		</c:if>
	</c:if>
</c:forEach>
</sec:authorize>
<%
	String webRoot = request.getContextPath();
	request.setCharacterEncoding("UTF-8");

	String JS_PATH              = webRoot+"/js"; 
	String IMG_PATH             = webRoot+"/images";
	String CSS_PATH             = webRoot+"/common/css";
	String MAIN_TITLE           = "TreE administration";
	
%>
<!--
<%
for (Enumeration e = session.getAttributeNames(); e.hasMoreElements(); ) {     
    String attribName = (String) e.nextElement();
    Object attribValue = session.getAttribute(attribName);
%>
<BR><%= attribName %> - <%= attribValue %>

<%
}
%>
//-->