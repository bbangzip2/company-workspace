<%/*
--------------------------------------------------------------------------------
    PROJECT NAME : tree admin
--------------------------------------------------------------------------------
    - 단위업무명 : incheader
    - 파일명    : incHeader.jsp
    - 서비스명   : 공통 프로그램
    - 처리설명   : 사용자단 공통 헤더 JSP 
    - 최초작성일 : 2014-01-23
    - 관련사항   : 
    - 작성자     : hong
    - 소속       : 비상ESL 
    - 비고       : 
--------------------------------------------------------------------------------
    - 수정일자   :  
    - 수정자     : 
    - 소속       : 
    - 수정내용   : 
--------------------------------------------------------------------------------
*/%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- header -->
<div id="tabHeader">
	<div class="header">
		<div class="thumInfo">
            <c:choose>
                <c:when test="${userDetails.instituteLogoPath eq null || userDetails.instituteLogoPath eq ''}">
                    <img src="${CONTEXT_PATH}/images/common/no-image0.png" alt="" class="thum"/>
                </c:when>
                <c:otherwise>
                    <img src='${tree:thumbnailPath("Y",CONTEXT_PATH, userDetails.instituteLogoPath)}' alt="" class="thum">
                </c:otherwise>
            </c:choose>
            <strong>${userDetails.instituteName}</strong>
        </div>

		<div class="userInfo">
			<span class="pic"><img src="<c:choose><c:when test="${empty userDetails.profilePhotoPath}">${CONTEXT_PATH}/images/common/thum.jpg</c:when><c:otherwise>${tree:thumbnailPath('Y',CONTEXT_PATH, userDetails.profilePhotoPath)}</c:otherwise></c:choose>" alt="${userDetails.nickname}"></span>
			<strong><span class="txtBlue">${userDetails.name}</span>(${userDetails.username})</strong><span class="line">|</span><a href="${CONTEXT_PATH}/member/treeSignOut.do" class="logout">Logout</a>
		</div>
	</div>

	<div id="gnb">
		<ul>
			<c:forEach var="item" items="${userDetails.accessMenus}" varStatus="status">
				<c:if test="${status.first}"><c:if test="${empty sessionScope.lnbMenuSeq}"><c:set var="lnbMenuSeq" scope="session" value="${item.menuSeq}"/></c:if></c:if>
				<c:if test="${item.grpSeq eq 1 and item.depthSeq eq 1}">
					<li><a href="<c:choose><c:when test='${item.linkSect eq "D" }'><c:url value="${item.linkUrl}"/></c:when><c:otherwise>javascript:void(0);</c:otherwise></c:choose>" <c:if test="${sessionScope.lnbMenuSeq eq item.menuSeq }">class="on"</c:if>>${item.menuNm}</a></li>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</div>
<!-- //header -->