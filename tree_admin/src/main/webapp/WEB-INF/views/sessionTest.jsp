<%
    request.getSession().setAttribute("treeAdmin", "treeAdmin_VALUE");
%>

<html>
<head>
<title>Cluster JSP</title>
</head>

<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr bgcolor="#CCCCCC">
            <td width="13%">Tomcat treeAdmin Machine</td>
            <td width="87%">&nbsp;</td>
        </tr>
        <tr>
            <td>Session ID :</td>
            <td><%=session.getId()%></td>
        </tr>
        <tr>
            <td>Session Attribute (treeAdmin):</td>
            <td><%=session.getAttribute("treeAdmin")%>
        </tr>
        <tr>
            <td>Session Attribute (treeNewAPI):</td>
            <td><%=session.getAttribute("treeNewAPI")%>
        </tr>
        <tr>
            <td>Session Attribute (treeAPI):</td>
            <td><%=session.getAttribute("treeAPI")%>
        </tr>

    </table>
</body>
</html>