/* LNB 리사이징 */
$(function() {
	var header = $('#header').height();
	winResize();

	$(window).resize(function() {
		winResize();
	});

	function winResize() {
		var docH = $(document).height() - header;
		$('#lnb').css('height', docH+'px');
	}
})



$(function() {
	
	
    $("#sDate").datepicker({
		dateFormat: "yy.mm.dd",
		dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
		showMonthAfterYear: false,
		monthNames: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sep.", "Oct.", "Nov.", "Dec." ],
        onSelect: function(selected) {
          $("#eDate").datepicker("option","minDate", selected)
        }
    });
    $("#eDate").datepicker({
		dateFormat: "yy.mm.dd",
		dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
		showMonthAfterYear: false,
		monthNames: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sep.", "Oct.", "Nov.", "Dec." ],
        onSelect: function(selected) {
           $("#sDate").datepicker("option","maxDate", selected)
        }
    });


//	$( ".datepicker" ).datepicker({
//		dateFormat: "yy-mm-dd",
//		dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
//		showMonthAfterYear: false,
//		monthNames: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sep.", "Oct.", "Nov.", "Dec." ]
//	});
	
});





