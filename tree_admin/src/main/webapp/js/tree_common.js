/**
 * 트리 어드민 화면에서 사용하는 공통 스크립트
 * 
 */

/*
 * 코드 테이블 목록 조회 및 콤보박스 생성
 * 
 * relCd : 상위 코드 
 * targetId : 콤보 박스가 그려질 id 값 
 * defaultValue: 선택 되어질 option 값 
 * optionTag : 옵션 맨 처음에 보여질 문자를 입력함  ex) 'All'또는 '전체'를 넘기면 문구 그대로 옵션을 생성함. 안보내면 생성되지 않는다. 
 */
function getAjaxCodeCombo(contextPath, relCd, targetId, defaultValue, optionTag){
	
	$.ajax({            
        type : "post",
        url : contextPath+"/common/codeList.do",
        beforeSend: function(req) {
            req.setRequestHeader("Accept", "application/json");
        },
        async : false,
        cache : false,
        data : {relCd : relCd},
        dataType: 'json',
        success : function(data){
            var code = data.code;
            var msg = data.massage;
            if ( code == "0000" ) {
            	makeComboBox(data, targetId, defaultValue, optionTag)
            } 
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert("error");
        }, 
        complete:function (xhr, textStatus){
        	isLogging = false;
        }  
    });
	
} 


/*
 * 기관 전체  목록 조회 및 콤보박스 생성
 * 
 * targetId : 콤보 박스가 그려질 id 값 
 * defaultValue: 선택 되어질 option 값 
 * optionTag : 옵션 맨 처음에 보여질 문자를 입력함  ex) 'All'또는 '전체'를 넘기면 문구 그대로 옵션을 생성함. 안보내면 생성되지 않는다.
 */
function getAjaxInstituteCombo(contextPath, targetId, defaultValue, optionTag){
	
	$.ajax({            
        type : "post",
        url : contextPath+"/common/instituteList.do",
        beforeSend: function(req) {
            req.setRequestHeader("Accept", "application/json");
        },
        async : false,
        cache : false,
        data : {},
        dataType: 'json',
        success : function(data){
            var code = data.code;
            var msg = data.massage;
            if ( code == "0000" ) {
            	makeComboBox(data, targetId, defaultValue, optionTag)
            } 
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert("error");
        }, 
        complete:function (xhr, textStatus){
        	isLogging = false;
        }  
    });
	
} 


/*
 * 캠퍼스 목록 조회 및 콤보박스 생성
 * 
 * intInfoSeq : institute seq 
 * targetId : 콤보 박스가 그려질 id 값 
 * defaultValue: 선택 되어질 option 값 
 * optionTag : 옵션 맨 처음에 보여질 문자를 입력함  ex) 'All'또는 '전체'를 넘기면 문구 그대로 옵션을 생성함. 안보내면 생성되지 않는다.
 */
function getAjaxCampusCombo(contextPath, intInfoSeq, targetId, defaultValue, optionTag){
	
	$.ajax({            
        type : "post",
        url : contextPath+"/common/campusList.do",
        beforeSend: function(req) {
            req.setRequestHeader("Accept", "application/json");
        },
        async : false,
        cache : false,
        data : {intInfoSeq : intInfoSeq},
        dataType: 'json',
        success : function(data){
            var code = data.code;
            var msg = data.massage;
            if ( code == "0000" ) {
            	makeComboBox(data, targetId, defaultValue, optionTag)
            } 
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert("error");
        }, 
        complete:function (xhr, textStatus){
        	isLogging = false;
        }  
    });
	
} 

/*
 * 클래스 목록 조회 및 콤보박스 생성
 * 
 * campInfoSeq : campus seq 
 * targetId : 콤보 박스가 그려질 id 값 
 * defaultValue: 선택 되어질 option 값 
 * optionTag : 옵션 맨 처음에 보여질 문자를 입력함  ex) 'All'또는 '전체'를 넘기면 문구 그대로 옵션을 생성함. 안보내면 생성되지 않는다.
 */
function getAjaxClassCombo(contextPath, campInfoSeq, targetId, defaultValue, optionTag){
	
	$.ajax({            
        type : "post",
        url : contextPath+"/common/classList.do",
        beforeSend: function(req) {
            req.setRequestHeader("Accept", "application/json");
        },
        async : false,
        cache : false,
        data : {campInfoSeq : campInfoSeq},
        dataType: 'json',
        success : function(data){
            var code = data.code;
            var msg = data.massage;
            if ( code == "0000" ) {
            	makeComboBox(data, targetId, defaultValue, optionTag)
            } 
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert("error");
        }, 
        complete:function (xhr, textStatus){
        	isLogging = false;
        }  
    });
	
} 


// combo box making..
function makeComboBox(data, targetId, defaultValue, optionTag){
    
	var option = "";
	var result = data.result;
	
	// 첫번째 옵션을 선택 할수 있도록 처리한다. 
	if( (optionTag != undefined ) && (optionTag != '')){
		option += "<option value=''>"+optionTag+"</option>";
	}
		
	for(i=0; i< result.length; i++){
		var sel = "";
		if(defaultValue == result[i].cd){
			sel = "selected";
		}
		option +="<option value='"+result[i].cd+"' "+sel+">"+result[i].cdSubject+"</option>"
	}
	$("#"+targetId).html(option);
}


// 공통 서버 요청 함수
function commonServerRequest(pUrl, pData) {
    var result ; // return result data

    $.ajax({
        type : "post",
        url : pUrl,
        beforeSend : function(req) {
            req.setRequestHeader("Accept", "application/json");
        },
        async : false,
        cache : false,
        data : pData,
        dataType : 'json',
        success : function(data) {
            var code = data.code;
            var msg = data.message;
            if (code == "0000") {
            	result = data;
            }
        },
        error : function(xhr, ajaxOptions, thrownError) {
            alert("error");
        },
        complete : function(xhr, textStatus) {
            isLogging = false;
        }
    });
    return result;

}


function isValidId(text) {
    // var regexp = /[0-9a-zA-Z.;\-]/; // 숫자,영문,특수문자
    // var regexp = /[0-9]/; // 숫자만
    var regexp = /[0-9a-zA-Z]/; // 숫자,영문
    var regexpFirst = /[a-zA-Z]/; // 영문만
    for( var i = 0; i < text.length; i++){
        // 첫번째는 무조건 영문자 체크
        if (i == 0 && text.charAt(i) != " " && regexpFirst.test(text.charAt(i)) == false) {
            alert("The first character must be an alphabetic character.");
            return true;    
        }
        
        if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false){
            alert("Contain alphabet letters or numbers only.");
            return true;
        }
    }
    return false;
}

function isValidPassword(text) {
    //var regexp = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{3,11}$/;
    var regexp = /[0-9a-zA-Z.;\-]/;
    if (text.length < 4 || text.length > 12) {
        return true;
    }
    
    if (!regexp.test(text)){
        return true;
    }
    
    return false;
}


function checkPhoneNum(text) {
    // var format = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$/;
    // return format.test(num);
    
    // var regexp = /[0-9a-zA-Z.;\-]/; // 숫자,영문,특수문자
    var regexp = /[0-9]/; // 숫자만
    // var regexp = /[0-9a-zA-Z]/; // 숫자,영문
    // var regexpFirst = /[a-zA-Z]/; // 영문만
    for( var i = 0; i < text.length; i++){
        if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false){
            return true;
        }
    }
    return false;
}

/**
 * input type select, text, textarea등의 value값 변경여부 판단 plug-in 
 * 
 * $(document).ready(function() {
 * 		$('#test').setOldValue();
 * });
 * 
 * if ($('#test').isEdited() == true) { alert('변경됨'); } else { alert('변경되지 않음'); }
 */
(function($){
	$.fn.extend({
		setOldValue: function() {
			return this.each(function() {
				var eleType = $(this).prop('type').toLowerCase();
				var oldVal = '';
				
				switch (eleType) {
					case 'select' :
							oldVal = $(this).find('option:selected').map(function() {
								return $(this).val();
							}).get().join('!@#');
									break;
                    case 'checkbox' :
                        oldVal = ($(this).is(':checked'))?$(this).val():'';
                                break;
					default :
							oldVal = $(this).val();
							break;
				}
			    $(this).data('oldVal',oldVal);
			});
		},
		isEdited: function() {
			var isEditedResult = false;
			
			this.each(function() {
				var eleType = $(this).prop('type').toLowerCase();
				var newVal = '';
				
				switch (eleType) {
					case 'select' :
							newVal = $(this).find('option:selected').map(function() {
								return $(this).val();
							}).get().join('!@#');
									break;
                    case 'checkbox' :
                        newVal = ($(this).is(':checked'))?$(this).val():'';
                                break;
					default :
							newVal = $(this).val();
							break;
				}
						
			    if ($(this).data('oldVal') != newVal) {
				    if (!isEditedResult) isEditedResult = true;
				    return;
			    }
			});
			
			return isEditedResult;
		}
	
	});
})(jQuery);