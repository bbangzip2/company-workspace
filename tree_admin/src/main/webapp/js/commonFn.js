﻿/*
 * html : '<p>LogOut?</p>' 테그까지 사용
 * param :: type = 'select' , 'check'
 * */
//경고창 플러그인
(function($){

    $.fn.extend({

    	alertLayer : function( options ) {

			var defaults = {
				html : null
				,type : 'select'
				, okCallBack  : null
				, cancelCallBack  : null
      }

      var options =  $.extend(defaults, options);
			var callbacks = $.extend({},defaults,callbacks) ;

            return this.each(function() {
            	
            	var obj = $(this);
                var o = options;
                var call = callbacks ;
                
                var btnHtml;
                
            	switch (o.type) {
            		case 'select':
            			btnHtml = '' 
            				+ '<a href="javascript:void(0)" class="popBtn" id="cancelBtn">Cancel</a>'
            				+ ' <a href="javascript:void(0)" class="popBtn type1" id="okBtn">OK</a>';
            			break;
            		case 'check':
            			btnHtml = ''
            				+ '<a href="javascript:void(0)" class="popBtn type1" id="cancelBtn">OK</a>';
            			break;
            	}
            	
            	var alertLayer = ''
                + '<div class="dim" id="dimClick" style="display: block;"></div>'
            		+ '<div class="alertPop" id="alertPop">'
            		+ '<div class="cnt">'+ o.html +'</div>'
            		+ '<div class="popBtnArea">'+ btnHtml +'</div>'
            		+ '</div>';
            	
            	obj.append(alertLayer);
            	
            	//ok버튼클릭
            	$('#alertPop').delegate('#okBtn' , BIND_EVENT_TYPE ,function(){
            		if(call.okCallBack){
            			$('#alertPop').fadeOut(fadeSpeed,function(){
                    $('#alertPop').remove();
                    $('.dim').remove();
            				call.okCallBack();
            			});
            		}else{
            			alert('잘못된 데이터입니다.');
            		}
            	});
            	//켄슬버튼
            	$('#alertPop').delegate('#cancelBtn' , BIND_EVENT_TYPE ,function(){
            		if(call.cancelCallBack){
            			$('#alertPop').fadeOut(fadeSpeed,function(){
                    $('#alertPop').remove();
                    $('.dim').remove();
            				call.cancelCallBack();
            			});
            		}else{
            			$('#alertPop').remove();
                  $('.dim').remove();
            		}
            	});
              //dim 클릭
              $('#dimClick').on( BIND_EVENT_TYPE , function(){
                if(call.cancelCallBack){
                }else{
                  $('#alertPop').remove();
                  $('.dim').remove();
                  call.cancelCallBack();
                }
              });
            });
        }
    });
})(jQuery);



/*
 * pageLoad 플러그인
 * tree html load 목적
 * @param loadSrcHtml : html경로 : '경로'
 * @param loadSrcCss : css경로 : '경로'
 * @param loadSrcJs : js경로 : ['경로','경로']
 * @param callBack : callback function
 * */
(function($){
	
    $.fn.extend({ 
        pageLoad: function( options , callbacks ) {
			var defaults = {
          //교안 여부 확인
          teachingPlan : null
          ,loadSrcHtml : null
          ,loadSrcCss : null
          ,loadSrcJs : null
          ,loadCall : null
        }
            var options =  $.extend(defaults, options);
			      var callbacks = $.extend({},defaults,callbacks) ;
			
            return this.each(function() {
				
                var o = options;
                var obj = $(this);
				var call = callbacks;
				
				//경로
				var htmlSrc = o.loadSrcHtml;
				var cssSrc = o.loadSrcCss;
				var jsSrc = o.loadSrcJs;
				
				
				
				//교안인지 체크
				if(call.teachingPlan){
					//callBack
					call.teachingPlan();
				}else{
					loadHtml();
				}
				
				//돔 이벤트 오브젝트 처리
				//pageEventGc();
				
				//html load fn
				function loadHtml(){
					if(htmlSrc){
						obj.load( htmlSrc , function( response, status, xhr ) {
							if ( status == "error" ) {
								var msg = "error : " ;
								console.log(msg + xhr.status + " " + xhr.statusText) ;
							}else{
								loadCss();
							}
						});
					}else{
						loadCss();
					}
					//가비지처리 이벤트 테스트중
					//eventManager.removeAll();
				}
				
				//css load fn
				function loadCss(){
					
					var findCss = $('head').find('#loadCss').length;
					
					if(!findCss){
						$("<link/>", {
							   rel : "stylesheet",
							   type : "text/css",
							   href : cssSrc ,
							   id : "loadCss"
						}).appendTo("head");
					}else{
						if(cssSrc){
							$('head').find('#loadCss').attr('href' , cssSrc);			
						}
					}
					jsload();
				}
				
				//ajax js load 
				jQuery.cachedScript = function( url, options ) {
					options = $.extend( options || {}, {
						dataType: "script",
						cache : true,
						url : url
					});
					return jQuery.ajax( options );
				};
				
				var jsIdx = 0 ;
				
				//jsload
				function jsload(){
					
					if(jsSrc == null){
						if(call.loadCall){
							call.loadCall();
						}
						return jsIdx = null;
					}else{
						if(jsIdx >= jsSrc.length){
							if(call.loadCall){
								call.loadCall();
							}
							return jsIdx = null;
						}else{
							$.cachedScript( jsSrc[jsIdx] ).done();
							jsIdx++;
							jsload();
						}
					}
				}

            });
        }
    });
})(jQuery);
/*pageLoad 끝*/

/*
**추후 공통기능 
**이벤트 커스텀이 더 필요한 상황
**treeGesture
*/
(function(window, undefined) {
    'use strict';
/**
 * treeGesture
 * @param   {HTMLElement}   element
 * @param   {Object}        options
 * @returns {treeGesture.Instance}
 * @constructor
 */
 
var treeGesture = function(element, options) {
    return new treeGesture.Instance(element, options || {});
};

// default settings
treeGesture.defaults = {
    stop_browser_behavior: {
        userSelect: 'none',
        touchAction: 'none',
                touchCallout: 'none',
        contentZooming: 'none',
        userDrag: 'none',
        tapHighlightColor: 'rgba(0,0,0,0)'
    }
};

treeGesture.HAS_POINTEREVENTS = navigator.pointerEnabled || navigator.msPointerEnabled;
treeGesture.HAS_TOUCHEVENTS = ('ontouchstart' in window);

treeGesture.MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;
treeGesture.NO_MOUSEEVENTS = treeGesture.HAS_TOUCHEVENTS && navigator.userAgent.match(treeGesture.MOBILE_REGEX);

treeGesture.EVENT_TYPES = {};

treeGesture.DIRECTION_DOWN = 'down';
treeGesture.DIRECTION_LEFT = 'left';
treeGesture.DIRECTION_UP = 'up';
treeGesture.DIRECTION_RIGHT = 'right';

treeGesture.POINTER_MOUSE = 'mouse';
treeGesture.POINTER_TOUCH = 'touch';
treeGesture.POINTER_PEN = 'pen';

treeGesture.EVENT_START = 'start';
treeGesture.EVENT_MOVE = 'move';
treeGesture.EVENT_END = 'end';

treeGesture.DOCUMENT = document;

treeGesture.plugins = {};

treeGesture.READY = false;

function setup() {
    if(treeGesture.READY) {
        return;
    }
	
    treeGesture.event.determineEventTypes();
    
    for(var name in treeGesture.gestures) {
        if(treeGesture.gestures.hasOwnProperty(name)) {
            treeGesture.detection.register(treeGesture.gestures[name]);
        }
    }
	
    treeGesture.event.onTouch(treeGesture.DOCUMENT, treeGesture.EVENT_MOVE, treeGesture.detection.detect);
    treeGesture.event.onTouch(treeGesture.DOCUMENT, treeGesture.EVENT_END, treeGesture.detection.detect);
	
    treeGesture.READY = true;
}

/**
 * create new treeGesture instance
 * @param   {HTMLElement}       element
 * @param   {Object}            [options={}]
 * @returns {treeGesture.Instance}
 * @constructor
 */
 
treeGesture.Instance = function(element, options) {
    var self = this;
	
    setup();

    this.element = element;
	
    this.enabled = true;
    
    this.options = treeGesture.utils.extend(
        treeGesture.utils.extend({}, treeGesture.defaults),
        options || {});
    
    if(this.options.stop_browser_behavior) {
        treeGesture.utils.stopDefaultBrowserBehavior(this.element, this.options.stop_browser_behavior);
    }
	
    treeGesture.event.onTouch(element, treeGesture.EVENT_START, function(ev) {
        if(self.enabled) {
            treeGesture.detection.startDetect(self, ev);
        }
    });
	
    return this;
};


treeGesture.Instance.prototype = {
    /**
     * bind events to the instance
     * @param   {String}      gesture
     * @param   {Function}    handler
     * @returns {treeGesture.Instance}
     */
    on: function onEvent(gesture, handler){
        var gestures = gesture.split(' ');
        for(var t=0; t<gestures.length; t++) {
            this.element.addEventListener(gestures[t], handler, false);
        }
        return this;
    },


    /**
     * unbind events to the instance
     * @param   {String}      gesture
     * @param   {Function}    handler
     * @returns {treeGesture.Instance}
     */
    off: function offEvent(gesture, handler){
        var gestures = gesture.split(' ');
        for(var t=0; t<gestures.length; t++) {
            this.element.removeEventListener(gestures[t], handler, false);
        }
        return this;
    },


    /**
     * trigger gesture event
     * @param   {String}      gesture
     * @param   {Object}      eventData
     * @returns {treeGesture.Instance}
     */
    trigger: function triggerEvent(gesture, eventData){
      
        var event = treeGesture.DOCUMENT.createEvent('Event');
                event.initEvent(gesture, true, true);
                event.gesture = eventData;

      
        var element = this.element;
        if(treeGesture.utils.hasParent(eventData.target, element)) {
            element = eventData.target;
        }

        element.dispatchEvent(event);
        return this;
    },


    /**
     * enable of disable treeGesture.js detection
     * @param   {Boolean}   state
     * @returns {treeGesture.Instance}
     */
    enable: function enable(state) {
        this.enabled = state;
        return this;
    }
};

/**
 * this holds the last move event,
 * @type {Object}
 */
var last_move_event = null;


/**
 * when the mouse is hold down, this is true
 * @type {Boolean}
 */
var enable_detect = false;


/**
 * when touch events have been fired, this is true
 * @type {Boolean}
 */
var touch_triggered = false;


treeGesture.event = {
    /**
     * simple addEventListener
     * @param   {HTMLElement}   element
     * @param   {String}        type
     * @param   {Function}      handler
     */
    bindDom: function(element, type, handler) {
        var types = type.split(' ');
        for(var t=0; t<types.length; t++) {
            element.addEventListener(types[t], handler, false);
        }
    },


    /**
     * touch events with mouse fallback
     * @param   {HTMLElement}   element
     * @param   {String}        eventType        like treeGesture.EVENT_MOVE
     * @param   {Function}      handler
     */
    onTouch: function onTouch(element, eventType, handler) {
                var self = this;

        this.bindDom(element, treeGesture.EVENT_TYPES[eventType], function bindDomOnTouch(ev) {
            var sourceEventType = ev.type.toLowerCase();

            if(sourceEventType.match(/mouse/) && touch_triggered) {
                return;
            }


            else if( sourceEventType.match(/touch/) ||  
                sourceEventType.match(/pointerdown/) || 
                (sourceEventType.match(/mouse/) && ev.which === 1)   
            ){
                enable_detect = true;
            }
			
            if(sourceEventType.match(/touch|pointer/)) {
                touch_triggered = true;
            }
         
            var count_touches = 0;
			
       
            if(enable_detect) {
			
                if(treeGesture.HAS_POINTEREVENTS && eventType != treeGesture.EVENT_END) {
                    count_touches = treeGesture.PointerEvent.updatePointer(eventType, ev);
                }
                else if(sourceEventType.match(/touch/)) {
                    count_touches = ev.touches.length;
                }
                else if(!touch_triggered) {
                    count_touches = sourceEventType.match(/up/) ? 0 : 1;
                }
              
                if(count_touches > 0 && eventType == treeGesture.EVENT_END) {
                    eventType = treeGesture.EVENT_MOVE;
                }
                else if(!count_touches) {
                    eventType = treeGesture.EVENT_END;
                }
				
                if(!count_touches && last_move_event !== null) {
                    ev = last_move_event;
                }
                else {
                    last_move_event = ev;
                }
                
                handler.call(treeGesture.detection, self.collectEventData(element, eventType, ev));
				
                
                if(treeGesture.HAS_POINTEREVENTS && eventType == treeGesture.EVENT_END) {
                    count_touches = treeGesture.PointerEvent.updatePointer(eventType, ev);
                }
            }

            //debug(sourceEventType +" "+ eventType);
            
            if(!count_touches) {
                last_move_event = null;
                enable_detect = false;
                touch_triggered = false;
                treeGesture.PointerEvent.reset();
            }
        });
    },
	
    determineEventTypes: function determineEventTypes() {
	
        var types;
     
        if(treeGesture.HAS_POINTEREVENTS) {
            types = treeGesture.PointerEvent.getEvents();
        }      
        else if(treeGesture.NO_MOUSEEVENTS) {
            types = [
                'touchstart',
                'touchmove',
                'touchend touchcancel'];
        }       
        else {
            types = [
                'touchstart mousedown',
                'touchmove mousemove',
                'touchend touchcancel mouseup'];
        }

        treeGesture.EVENT_TYPES[treeGesture.EVENT_START]  = types[0];
        treeGesture.EVENT_TYPES[treeGesture.EVENT_MOVE]   = types[1];
        treeGesture.EVENT_TYPES[treeGesture.EVENT_END]    = types[2];
    },


    /**
     * create touchlist depending on the event
     * @param   {Object}    ev
     * @param   {String}    eventType 
     */
    getTouchList: function getTouchList(ev) {     
        if(treeGesture.HAS_POINTEREVENTS) {
            return treeGesture.PointerEvent.getTouchList();
        }
        else if(ev.touches) {
            return ev.touches;
        }
        else {
            return [{
                identifier: 1,
                pageX: ev.pageX,
                pageY: ev.pageY,
                target: ev.target
            }];
        }
    },


    /**
     * collect event data for treeGesture js
     * @param   {HTMLElement}   element
     * @param   {String}        eventType        like treeGesture.EVENT_MOVE
     * @param   {Object}        eventData
     */
    collectEventData: function collectEventData(element, eventType, ev) {
        var touches = this.getTouchList(ev, eventType);
        
        var pointerType = treeGesture.POINTER_TOUCH;
        if(ev.type.match(/mouse/) || treeGesture.PointerEvent.matchType(treeGesture.POINTER_MOUSE, ev)) {
            pointerType = treeGesture.POINTER_MOUSE;
        }

        return {
            center      : treeGesture.utils.getCenter(touches),
            timeStamp   : new Date().getTime(),
            target      : ev.target,
            touches     : touches,
            eventType   : eventType,
            pointerType : pointerType,
            srcEvent    : ev,
			
            preventDefault: function() {
                if(this.srcEvent.preventManipulation) {
                    this.srcEvent.preventManipulation();
                }

                if(this.srcEvent.preventDefault) {
                    this.srcEvent.preventDefault();
                }
            },
            stopPropagation: function() {
                this.srcEvent.stopPropagation();
            },
			
            stopDetect: function() {
                return treeGesture.detection.stopDetect();
            }
        };
    }
};

treeGesture.PointerEvent = {
    /**
     * holds all pointers
     * @type {Object}
     */
    pointers: {},
	
    getTouchList: function() {
        var self = this;
        var touchlist = [];
        
        Object.keys(self.pointers).sort().forEach(function(id) {
            touchlist.push(self.pointers[id]);
        });
        return touchlist;
    },
    updatePointer: function(type, pointerEvent) {
        if(type == treeGesture.EVENT_END) {
            this.pointers = {};
        }
        else {
            pointerEvent.identifier = pointerEvent.pointerId;
            this.pointers[pointerEvent.pointerId] = pointerEvent;
        }

        return Object.keys(this.pointers).length;
    },
    /**
     * check if ev matches pointertype
     * @param   {String}    
     * @param   {PointerEvent}  
     */
    matchType: function(pointerType, ev) {
        if(!ev.pointerType) {
            return false;
        }

        var types = {};
        types[treeGesture.POINTER_MOUSE] = (ev.pointerType == ev.MSPOINTER_TYPE_MOUSE || ev.pointerType == treeGesture.POINTER_MOUSE);
        types[treeGesture.POINTER_TOUCH] = (ev.pointerType == ev.MSPOINTER_TYPE_TOUCH || ev.pointerType == treeGesture.POINTER_TOUCH);
        types[treeGesture.POINTER_PEN] = (ev.pointerType == ev.MSPOINTER_TYPE_PEN || ev.pointerType == treeGesture.POINTER_PEN);
        return types[pointerType];
    },
    getEvents: function() {
        return [
            'pointerdown MSPointerDown',
            'pointermove MSPointerMove',
            'pointerup pointercancel MSPointerUp MSPointerCancel'
        ];
    },
    reset: function() {
        this.pointers = {};
    }
};


treeGesture.utils = {
    /**
     * extend method,
     * also used for cloning when dest is an empty object
     * @param   {Object}    dest
     * @param   {Object}    src
         * @parm        {Boolean}        merge                do a merge
     * @returns {Object}    dest
     */
    extend: function extend(dest, src, merge) {
        for (var key in src) {
                        if(dest[key] !== undefined && merge) {
                                continue;
                        }
            dest[key] = src[key];
        }
        return dest;
    },
    /**
     * find if a node is in the given parent
     * used for event delegation tricks
     * @param   {HTMLElement}   node
     * @param   {HTMLElement}   parent
     * @returns {boolean}       has_parent
     */
    hasParent: function(node, parent) {
        while(node){
            if(node == parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    },


    /**
     * get the center of all the touches
     * @param   {Array}     touches
     * @returns {Object}    center
     */
    getCenter: function getCenter(touches) {
        var valuesX = [], valuesY = [];

        for(var t= 0,len=touches.length; t<len; t++) {
            valuesX.push(touches[t].pageX);
            valuesY.push(touches[t].pageY);
        }

        return {
            pageX: ((Math.min.apply(Math, valuesX) + Math.max.apply(Math, valuesX)) / 2),
            pageY: ((Math.min.apply(Math, valuesY) + Math.max.apply(Math, valuesY)) / 2)
        };
    },


    /**
     * calculate the velocity between two points
     * @param   {Number}    delta_time
     * @param   {Number}    delta_x
     * @param   {Number}    delta_y
     * @returns {Object}    velocity
     */
    getVelocity: function getVelocity(delta_time, delta_x, delta_y) {
        return {
            x: Math.abs(delta_x / delta_time) || 0,
            y: Math.abs(delta_y / delta_time) || 0
        };
    },


    /**
     * calculate the angle between two coordinates
     * @param   {Touch}     touch1
     * @param   {Touch}     touch2
     * @returns {Number}    angle
     */
    getAngle: function getAngle(touch1, touch2) {
        var y = touch2.pageY - touch1.pageY,
            x = touch2.pageX - touch1.pageX;
        return Math.atan2(y, x) * 180 / Math.PI;
    },
    /**
     * angle to direction define
     * @param   {Touch}     touch1
     * @param   {Touch}     touch2
     * @returns {String}    direction constant, like treeGesture.DIRECTION_LEFT
     */
    getDirection: function getDirection(touch1, touch2) {
        var x = Math.abs(touch1.pageX - touch2.pageX),
            y = Math.abs(touch1.pageY - touch2.pageY);

        if(x >= y) {
            return touch1.pageX - touch2.pageX > 0 ? treeGesture.DIRECTION_LEFT : treeGesture.DIRECTION_RIGHT;
        }
        else {
            return touch1.pageY - touch2.pageY > 0 ? treeGesture.DIRECTION_UP : treeGesture.DIRECTION_DOWN;
        }
    },
    /**
     * calculate the distance between two touches
     * @param   {Touch}     touch1
     * @param   {Touch}     touch2
     * @returns {Number}    distance
     */
    getDistance: function getDistance(touch1, touch2) {
        var x = touch2.pageX - touch1.pageX,
            y = touch2.pageY - touch1.pageY;
        return Math.sqrt((x*x) + (y*y));
    },
    /**
     * calculate the scale factor between two touchLists (fingers)
     * no scale is 1, and goes down to 0 when pinched together, and bigger when pinched out
     * @param   {Array}     start
     * @param   {Array}     end
     * @returns {Number}    scale
     */
    getScale: function getScale(start, end) {
        // need two fingers...
        if(start.length >= 2 && end.length >= 2) {
            return this.getDistance(end[0], end[1]) /
                this.getDistance(start[0], start[1]);
        }
        return 1;
    },
    /**
     * calculate the rotation degrees between two touchLists (fingers)
     * @param   {Array}     start
     * @param   {Array}     end
     * @returns {Number}    rotation
     */
    getRotation: function getRotation(start, end) {
        // need two fingers
        if(start.length >= 2 && end.length >= 2) {
            return this.getAngle(end[1], end[0]) -
                this.getAngle(start[1], start[0]);
        }
        return 0;
    },
    /**
     * boolean if the direction is vertical
     * @param    {String}    direction
     * @returns  {Boolean}   is_vertical
     */
    isVertical: function isVertical(direction) {
        return (direction == treeGesture.DIRECTION_UP || direction == treeGesture.DIRECTION_DOWN);
    },
    /**
     * stop browser default behavior with css props
     * @param   {HtmlElement}   element
     * @param   {Object}        css_props
     */
    stopDefaultBrowserBehavior: function stopDefaultBrowserBehavior(element, css_props) {
        var prop,
            vendors = ['webkit','khtml','moz','ms','o',''];

        if(!css_props || !element.style) {
            return;
        }
        // with css properties for modern browsers
        for(var i = 0; i < vendors.length; i++) {
            for(var p in css_props) {
                if(css_props.hasOwnProperty(p)) {
                    prop = p;

                    // vender prefix at the property
                    if(vendors[i]) {
                        prop = vendors[i] + prop.substring(0, 1).toUpperCase() + prop.substring(1);
                    }

                    // set the style
                    element.style[prop] = css_props[p];
                }
            }
        }

        // also the disable onselectstart
        if(css_props.userSelect == 'none') {
            element.onselectstart = function() {
                return false;
            };
        }
    }
};

treeGesture.detection = {
    gestures: [],    
    current: null,
    previous: null,
    stopped: false,
	
    /**
     * start treeGesture.gesture detection
     * @param   {treeGesture.Instance}   inst
     * @param   {Object}            eventData
     */
    startDetect: function startDetect(inst, eventData) {
        if(this.current) {
            return;
        }

        this.stopped = false;

        this.current = {
            inst        : inst, 
            startEvent  : treeGesture.utils.extend({}, eventData),
            lastEvent   : false, 
            name        : '' 
        };
        this.detect(eventData);
    },


    /**
     * treeGesture.gesture detection
     * @param   {Object}    eventData
     * @param   {Object}    eventData
     */
    detect: function detect(eventData) {
        if(!this.current || this.stopped) {
            return;
        }
        eventData = this.extendEventData(eventData);
        var inst_options = this.current.inst.options;    
        for(var g=0,len=this.gestures.length; g<len; g++) {
            var gesture = this.gestures[g];       
            if(!this.stopped && inst_options[gesture.name] !== false) {            
                if(gesture.handler.call(gesture, eventData, this.current.inst) === false) {
                    this.stopDetect();
                    break;
                }
            }
        }       
        if(this.current) {
            this.current.lastEvent = eventData;
        }       
        if(eventData.eventType == treeGesture.EVENT_END && !eventData.touches.length-1) {
            this.stopDetect();
        }

        return eventData;
    },
    stopDetect: function stopDetect() {     
        this.previous = treeGesture.utils.extend({}, this.current);  
        this.current = null;
        this.stopped = true;
    },


    /**
     * extend eventData for treeGesture.gestures
     * @param   {Object}   ev
     * @returns {Object}   ev
     */
    extendEventData: function extendEventData(ev) {
        var startEv = this.current.startEvent;
        if(startEv && (ev.touches.length != startEv.touches.length || ev.touches === startEv.touches)) {      
            startEv.touches = [];
            for(var i=0,len=ev.touches.length; i<len; i++) {
                startEv.touches.push(treeGesture.utils.extend({}, ev.touches[i]));
            }
        }

        var delta_time = ev.timeStamp - startEv.timeStamp,
            delta_x = ev.center.pageX - startEv.center.pageX,
            delta_y = ev.center.pageY - startEv.center.pageY,
            velocity = treeGesture.utils.getVelocity(delta_time, delta_x, delta_y);

        treeGesture.utils.extend(ev, {
            deltaTime   : delta_time,

            deltaX      : delta_x,
            deltaY      : delta_y,

            velocityX   : velocity.x,
            velocityY   : velocity.y,

            distance    : treeGesture.utils.getDistance(startEv.center, ev.center),
            angle       : treeGesture.utils.getAngle(startEv.center, ev.center),
            direction   : treeGesture.utils.getDirection(startEv.center, ev.center),

            scale       : treeGesture.utils.getScale(startEv.touches, ev.touches),
            rotation    : treeGesture.utils.getRotation(startEv.touches, ev.touches),

            startEvent  : startEv
        });

        return ev;
    },


    /**
     * register new gesture
     * @param   {Object}    gesture object, see gestures.js for documentation
     * @returns {Array}     gestures
     */
    register: function register(gesture) {       
        var options = gesture.defaults || {};
        if(options[gesture.name] === undefined) {
            options[gesture.name] = true;
        }      
        treeGesture.utils.extend(treeGesture.defaults, options, true);
        gesture.index = gesture.index || 1000;     
        this.gestures.push(gesture);       
        this.gestures.sort(function(a, b) {
            if (a.index < b.index) {
                return -1;
            }
            if (a.index > b.index) {
                return 1;
            }
            return 0;
        });

        return this.gestures;
    }
};

treeGesture.gestures = treeGesture.gestures || {};

treeGesture.gestures.Hold = {
    name: 'hold',
    index: 10,
    defaults: {
        hold_timeout        : 500,
        hold_threshold        : 1
    },
    timer: null,
    handler : function holdGesture(ev, inst) {

      if (ev.target.nodeName == "canvas" || ev.target.nodeName == "CANVAS") {
        return false;
      }else{
        switch (ev.eventType) {
          case treeGesture.EVENT_START:
            // clear any running timers
            clearTimeout(this.timer);

            // set the gesture so we can check in the timeout if it still is
            treeGesture.detection.current.name = this.name;

            // set timer and if after the timeout it still is hold,
            // we trigger the hold event
            this.timer = setTimeout(function () {
              if (treeGesture.detection.current.name == 'hold') {
                inst.trigger('hold', ev);
              }
            }, inst.options.hold_timeout);
            break;

          // when you move or end we clear the timer
          case treeGesture.EVENT_MOVE:
            if (ev.distance > inst.options.hold_threshold) {
              clearTimeout(this.timer);
            }
            break;

          case treeGesture.EVENT_END:
            clearTimeout(this.timer);
            break;
        }
      }
    }
/*

    handler: function holdGesture(ev, inst) {
        switch(ev.eventType) {
            case treeGesture.EVENT_START:               
                clearTimeout(this.timer);              
                treeGesture.detection.current.name = this.name;               
                this.timer = setTimeout(function() {
                    if(treeGesture.detection.current.name == 'hold') {
                        inst.trigger('hold', ev);
                    }
                }, inst.options.hold_timeout);
                break;         
            case treeGesture.EVENT_MOVE:
                if(ev.distance > inst.options.hold_threshold) {
                    clearTimeout(this.timer);
                }
                break;

            case treeGesture.EVENT_END:
                clearTimeout(this.timer);
                break;
        }
    }*/
};

treeGesture.gestures.Tap = {
    name: 'tap',
    index: 100,
    defaults: {
        tap_max_touchtime        : 250,
        tap_max_distance        : 10,
                tap_always                        : true,
        doubletap_distance        : 20,
        doubletap_interval        : 300
    },
    handler: function tapGesture(ev, inst) {
        if(ev.eventType == treeGesture.EVENT_END) {          
            var prev = treeGesture.detection.previous,
                                did_doubletap = false;          
            if(ev.deltaTime > inst.options.tap_max_touchtime ||
                ev.distance > inst.options.tap_max_distance) {
                return;
            }        
            if(prev && prev.name == 'tap' &&
                (ev.timeStamp - prev.lastEvent.timeStamp) < inst.options.doubletap_interval &&
                ev.distance < inst.options.doubletap_distance) {
                                inst.trigger('doubletap', ev);
                                did_doubletap = true;
            }                    
                        if(!did_doubletap || inst.options.tap_always) {
                                treeGesture.detection.current.name = 'tap';
                                inst.trigger(treeGesture.detection.current.name, ev);
                        }
        }
    }
};

treeGesture.gestures.Swipe = {
    name: 'swipe',
    index: 40,
    defaults: {
        swipe_max_touches  : 1,
        swipe_velocity     : 0.7
    },
    handler: function swipeGesture(ev, inst) {
        if(ev.eventType == treeGesture.EVENT_END) {
            if(inst.options.swipe_max_touches > 0 &&
                ev.touches.length > inst.options.swipe_max_touches) {
                return;
            }
            if(ev.velocityX > inst.options.swipe_velocity ||
                ev.velocityY > inst.options.swipe_velocity) {
                inst.trigger(this.name, ev);
                inst.trigger(this.name + ev.direction, ev);
            }
        }
    }
};

treeGesture.gestures.Drag = {
    name: 'drag',
    index: 50,
    defaults: {
        drag_min_distance : 10,
        drag_max_touches  : 1,
        drag_block_horizontal   : false,
        drag_block_vertical     : false,
        drag_lock_to_axis       : false,
        drag_lock_min_distance : 25
    },
    triggered: false,
    handler: function dragGesture(ev, inst) {
        if(treeGesture.detection.current.name != this.name && this.triggered) {
            inst.trigger(this.name +'end', ev);
            this.triggered = false;
            return;
        }
        if(inst.options.drag_max_touches > 0 &&
            ev.touches.length > inst.options.drag_max_touches) {
            return;
        }
        switch(ev.eventType) {
            case treeGesture.EVENT_START:
                this.triggered = false;
                break;

            case treeGesture.EVENT_MOVE:           
                if(ev.distance < inst.options.drag_min_distance &&
                    treeGesture.detection.current.name != this.name) {
                    return;
                }               
                treeGesture.detection.current.name = this.name;
                if(treeGesture.detection.current.lastEvent.drag_locked_to_axis || (inst.options.drag_lock_to_axis && inst.options.drag_lock_min_distance<=ev.distance)) {
                    ev.drag_locked_to_axis = true;
                }
                var last_direction = treeGesture.detection.current.lastEvent.direction;
                if(ev.drag_locked_to_axis && last_direction !== ev.direction) {                
                    if(treeGesture.utils.isVertical(last_direction)) {
                        ev.direction = (ev.deltaY < 0) ? treeGesture.DIRECTION_UP : treeGesture.DIRECTION_DOWN;
                    }
                    else {
                        ev.direction = (ev.deltaX < 0) ? treeGesture.DIRECTION_LEFT : treeGesture.DIRECTION_RIGHT;
                    }
                }             
                if(!this.triggered) {
                    inst.trigger(this.name +'start', ev);
                    this.triggered = true;
                }            
                inst.trigger(this.name, ev);             
                inst.trigger(this.name + ev.direction, ev);             
                if( (inst.options.drag_block_vertical && treeGesture.utils.isVertical(ev.direction)) ||
                    (inst.options.drag_block_horizontal && !treeGesture.utils.isVertical(ev.direction))) {
                    ev.preventDefault();
                }
                break;

            case treeGesture.EVENT_END:            
                if(this.triggered) {
                    inst.trigger(this.name +'end', ev);
                }

                this.triggered = false;
                break;
        }
    }
};

treeGesture.gestures.Transform = {
    name: 'transform',
    index: 45,
    defaults: {
        transform_min_scale     : 0.01,
        transform_min_rotation  : 1,
        transform_always_block  : false
    },
    triggered: false,
    handler: function transformGesture(ev, inst) {
        if(treeGesture.detection.current.name != this.name && this.triggered) {
            inst.trigger(this.name +'end', ev);
            this.triggered = false;
            return;
        }       
        if(ev.touches.length < 2) {
            return;
        }      
        if(inst.options.transform_always_block) {
            ev.preventDefault();
        }

        switch(ev.eventType) {
            case treeGesture.EVENT_START:
                this.triggered = false;
                break;

            case treeGesture.EVENT_MOVE:
                var scale_threshold = Math.abs(1-ev.scale);
                var rotation_threshold = Math.abs(ev.rotation);          
                if(scale_threshold < inst.options.transform_min_scale &&
                    rotation_threshold < inst.options.transform_min_rotation) {
                    return;
                }
                treeGesture.detection.current.name = this.name;
                if(!this.triggered) {
                    inst.trigger(this.name +'start', ev);
                    this.triggered = true;
                }
                inst.trigger(this.name, ev); 
                if(rotation_threshold > inst.options.transform_min_rotation) {
                    inst.trigger('rotate', ev);
                }
                if(scale_threshold > inst.options.transform_min_scale) {
                    inst.trigger('pinch', ev);
                    inst.trigger('pinch'+ ((ev.scale < 1) ? 'in' : 'out'), ev);
                }
                break;

            case treeGesture.EVENT_END:             
                if(this.triggered) {
                    inst.trigger(this.name +'end', ev);
                }

                this.triggered = false;
                break;
        }
    }
};

treeGesture.gestures.Touch = {
    name: 'touch',
    index: -Infinity,
    defaults: {       
        prevent_default: false,
        prevent_mouseevents: false
    },
    handler: function touchGesture(ev, inst) {
        if(inst.options.prevent_mouseevents && ev.pointerType == treeGesture.POINTER_MOUSE) {
            ev.stopDetect();
            return;
        }

        if(inst.options.prevent_default) {
            ev.preventDefault();
        }

        if(ev.eventType ==  treeGesture.EVENT_START) {
            inst.trigger(this.name, ev);
        }
    }
};
treeGesture.gestures.Release = {
    name: 'release',
    index: Infinity,
    handler: function releaseGesture(ev, inst) {
        if(ev.eventType ==  treeGesture.EVENT_END) {
            inst.trigger(this.name, ev);
        }
    }
};
if(typeof module === 'object' && typeof module.exports === 'object'){
    module.exports = treeGesture;
}
else {
    window.treeGesture = treeGesture;
    if(typeof window.define === 'function' && window.define.amd) {
        window.define('treeGesture', [], function() {
            return treeGesture;
        });
    }
}
})(this);

(function($, undefined) {
    'use strict';

    // no jQuery or Zepto!
    if($ === undefined) {
        return;
    }
    treeGesture.event.bindDom = function(element, eventTypes, handler) {
        $(element).on(eventTypes, function(ev) {
            var data = ev.originalEvent || ev;
            // IE pageX 
            if(data.pageX === undefined) {
                data.pageX = ev.pageX;
                data.pageY = ev.pageY;
            }
            // IE target 
            if(!data.target) {
                data.target = ev.target;
            }
            // IE button 
            if(data.which === undefined) {
                data.which = data.button;
            }
            // IE preventDefault
            if(!data.preventDefault) {
                data.preventDefault = ev.preventDefault;
            }
            // IE stopPropagation
            if(!data.stopPropagation) {
                data.stopPropagation = ev.stopPropagation;
            }
            handler.call(this, data);
        });
    };

    treeGesture.Instance.prototype.on = function(types, handler) {
        return $(this.element).on(types, handler);
    };
    treeGesture.Instance.prototype.off = function(types, handler) {
        return $(this.element).off(types, handler);
    };

    treeGesture.Instance.prototype.trigger = function(gesture, eventData){
        var el = $(this.element);
        if(el.has(eventData.target).length) {
            el = $(eventData.target);
        }

        return el.trigger({
            type: gesture,
            gesture: eventData
        });
    };
	
    $.fn.treeGesture = function(options) {
        return this.each(function() {
            var el = $(this);
            var inst = el.data('treeGesture');
            // start new treeGesture instance
            if(!inst) {
                el.data('treeGesture', new treeGesture(this, options || {}));
            }
            // change the options
            else if(inst && options) {
                treeGesture.utils.extend(inst.options, options);
            }
        });
    };
    
})(window.jQuery || window.Zepto);
/*treeGesture 끝*/

//전자칠판 load fn 보드일경우 호출 js
/*
function boardType(){
	userType = 'B';
	commonLayer.pageLoad({
		teachingPlan : null
		,loadSrcHtml : './view/commonLayer/fnBoard.html'
		,loadSrcCss : './view/luncher/css/launcher.css'
		,loadSrcJs : ['./view/commonLayer/js/fnBoard.js']
	});
}
*/

/* *
 * jquery event 모바일 윈도우 체크체크
 * 이벤트 발생 jquery on으로 처리
 * $(selector).on( BIND_EVENT_TYPE , function(){})
 * */
if(navigator.userAgent.match(/iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || navigator.userAgent.match(/LG|SAMSUNG|Samsung/) != null){	
	var BIND_EVENT_TYPE = "touchend";
	$("head").append('<script src="./common/js/cordova-2.9.0.js"></script>');
  mobileCheck = true;
}else{
	var BIND_EVENT_TYPE = "click";
  mobileCheck = false;
}

/*
 * 공통기능 제스쳐 off
 * @param {selector} 셀렉트 div
 * */
function gestureOff( selector ){
	treeGesture( selector , {
		swipe : false
	   ,hold : false
	});
}
/*공통기능 제스쳐 end*/


//가비지
var eventModule = function(){
	this.eventArry = [];	
};
//js 이벤트 add
eventModule.prototype.add = function(eventFn){
	var eventObj = new eventFn();
	this.eventArry.push(eventObj);
};
//js add 선택 삭제
eventModule.prototype.remove = function(type){
	for(var i in this.eventArry){
		if(this.eventArry[i].type == type){
			this.eventArry[i].off();
			this.eventArry[i] = null;
			this.eventArry.splice(i, 1);
		}
	}
};
//js add 전체 삭제
eventModule.prototype.removeAll = function(){
	for(var i in this.eventArry){
			this.eventArry[i].off();
			this.eventArry[i] = null;
			this.eventArry.splice(i, 1);
	}
};

//js containsType 등록 여부
eventModule.prototype.containsType = function(type){
  for(var i in this.eventArry){
    if(this.eventArry[i].type == type){
     return true;
    }
  }
  return false;
};
//가비지 등록
var eventManager = new eventModule();


//window IF
/*
 *  test : 문자열
 * 	param : array ['data','data']
 * */
function sendToNative(func, param) {
  if(appConnect){
    console.log('sendToNative : treeMessage.' + func, param);
    app.sendMessage('treeMessage.' + func, param);
  }
}

/**
 * 선생님 노드IP 가져오기
 * @param param : 선생님 IP
 */
function callbackGetTeacherIP(param) {
  userIP = param;
}

//nodejs 접속
//노드 접속
function nodeConnection(){
	//socket접속
	nodeConnect(nodeURL,nodePORT);
}

//엑티비티 end시 카드맵으로 이동하는 fn
function goCardMap(){
	if(userType =="T" || userType == "S"){
		contentLayer.fadeOut(fadeSpeed ,  function(){
			mainLayer.pageLoad({
				 teachingPlan : null
				,loadSrcHtml : null
				,loadSrcCss : './view/luncher/css/launcher.css'
				,loadSrcJs : null
				,loadCall : function(){
					commonLayer.empty();
					contentLayer.empty();
					mainLayer.fadeIn(fadeSpeed);
				}
			});
		});
	}else{
		$("#cardMap").fadeIn(fadeSpeed);
	}
}

//유저 등록 fn
function getUserList(list){

   if(userType == TEACHERTYPE){

     stdList = new Array();

     var len = list.length;

     for(i = 0; i < len ; i++){

       var result = Get.selectMemberInfo(list[i]);

       stdList[i] = new Array();
       stdList[i].push(result.mbrId);
       stdList[i].push(result.nm);
       stdList[i].push("S");
       stdList[i].push(result.profilePhotopath);

       if(i == len-1){
         /*
           학생 커넥시 줄 정보
           stdList - 전자칠판
           선생님 id - 전자칠판, 학생
         */
         //전자칠판 data전달
         sendClassInfo();
         //학생 들어왔을시 센드
         //cardListSend();
         //선생님 아이디 전해주기
         sendTID();
       }
     }
   }
}


//전자칠판 정보 전달
function sendClassInfo(){
  var rid = null;
  var fnName = 'sendClassInfo';
  var data = {'stdList': stdList , 'fnName': fnName };
  var id = 'all';
  nodeDataSend('nodeDataProcessing', id , rid , data);

}


//안드로이드호출
/*키값
*  USER_INFO
*  DRAWING_TOOL_OPEN
*  NOTE_VISIBLE
*  IP_ADDRESS
* */


//수업중 포트폴리오 저장 이미지
function portfolioUpLoad(filepath){

  var appfilepath = LAUNCHER_DIR+filepath;

  cordova.exec(
    function(success) {
      console.debug('파일 업로드 성공');
    },    //success callback
    function(err) {
      console.debug('파일 업로드 실패');
    },        //error callback
    "TreePlugin",            //plugin name
    "UPLOAD_PORTFOLIO",      //action name
    ["12", "71", "9", appfilepath]
  );
}

 //유저인포 패스
function setUserInfo(data) {
  var jsonObj = JSON.stringify(data);
  cordova.exec(function(result) {},
  function(err) {},
    "TreePlugin",
    "USER_INFO",
    [jsonObj]
  );
}
//오픈 드로잉
function openDrawingTool() {
    /*cordova.exec(
       function(result) {},
      function(err) {},
      "TreePlugin",
      "DRAWING_TOOL_OPEN",
      []
    );*/
}

//북버전 체크를 위한 레슨cd 넘기기
function andLessonSend(lessonSeq){
  //var jsonObj = JSON.stringify(data);
  console.debug("안드로이드 다운로드 레슨정보 : " + lessonSeq);
  cordova.exec(
     function(result) {
       console.log('다운로드 성공');
       classLoad(main.selectLessonCd , main.selectDayNo);
     },
     function(err) {
       console.log('다운로드 실패');
       if(main.selectLessonCd == '2'){
         classLoad(main.selectLessonCd , main.selectDayNo);
       }
     },
     "TreePlugin",
     "SELECT_CONTENT",
      [lessonSeq]
  );
}

//노트 보이기
var visible = false;
function noteVisible() {
  visible = !visible;
  cordova.exec(function(result) {},
  function(err) {},
    "TreePlugin",
    "NOTE_VISIBLE",
    [visible]
  );
}
//ip넘겨주기
function getIpAddress() {
  cordova.exec(function(result) {},
  function(err) {},
    "TreePlugin",
    "IP_ADDRESS",
    []
  );
}

//시작 끝 호출
function learningStatus(status , cardIdx){
  //status = start , end
  console.log('status : ' + status);
  cordova.exec(
    function(success) {},
    function(err) {},
    "TreePlugin",
    "LEARNING_STATUS",
    [status, nowCardList[cardIdx]]
  );
}



//노드로 학생 카드 리스트 넘겨주기
function cardListSend(){
  var rid = null;
  var fnName = 'cardListSend';
  var data = {'cardList': nowCardList , 'userType' : userType ,'fnName': fnName };
  var id = 'all';
  nodeDataSend('nodeDataProcessing', id , rid , data);
}

function cardLoadSend(cardIdx){

  var cardIdx = Number(cardIdx);
  //페이지 로드
  var userFloder;
  var cardCode;
  if(cardIdx == null){
    cardCode = nowCardList[nowCardIdx];
  }else{
    nowCardIdx = cardIdx;
    cardCode = nowCardList[nowCardIdx];
  }

  var loacalUrl = Get.getCardLocalUrl(cardCode);
  //카드 상세정보
  var cardInfo = Get.getCardInfo(cardCode);

  //선생님일시 사용  카드 이름
  var cardTitle = cardInfo.prodTitle;

  if(userType == 'T'){
    userFloder = 'teacher';
    commonLayer.pageLoad({
      loadSrcHtml : './view/commonLayer/fnTeacher.html'
      ,loadSrcCss : null
      ,loadSrcJs : ['./view/commonLayer/js/T_fn.js']
      ,loadCall : function(){
        $('#lessonMap').hide();
        //cardTitle
        $('#header .sideA .bold').html(cardTitle);
      }
    });
  }

  if(userType == 'S'){
    userFloder = 'student';
    commonLayer.pageLoad({
       loadSrcHtml : './view/commonLayer/fnStudent.html'
      ,loadSrcCss : null
      ,loadSrcJs : ['./view/commonLayer/js/S_fn.js']
      ,loadCall : function(){
        $('#lessonMap').hide();
        if(mobileCheck){
          //시작
          console.log('시작 지점 스타드');
          learningStatus('start',nowCardIdx);
        }
      }
    });
  }
  if(userType == 'B'){
    userFloder = 'board';
    var commonLayerUse = commonLayer.find('.swiper-wrapper');
    if(!commonLayerUse){
      commonLayer.pageLoad({
        loadSrcHtml : './view/commonLayer/fnBoard.html'
        ,loadSrcJs : null
        ,loadSrcCss : null
        ,loadCall : function(){
          $('#cardMap').hide();
        }
      });
    }else{
      $('#cardMap').hide();
    }
  }




  var htmlLink = "./content"+loacalUrl+"/"+userFloder+"/pageLoad.html";
  var jsLink = ["./content"+loacalUrl+"/"+userFloder+"/pageLoad.js"];
  var cssLink = "./content"+loacalUrl+"/"+userFloder+"/pageLoad.css";

  console.log('htmlLink');
  console.log(htmlLink);
  console.log('jsLink');
  console.log(jsLink);
  console.log('cssLink');
  console.log(cssLink);

  destroyActy();//pageLoad 전에 destroyActy를 실행한다.
  destroyActy = function(){}

  contentLayer.pageLoad({
    teachingPlan : null
    ,loadSrcHtml :  htmlLink
    ,loadSrcCss : cssLink
    ,loadSrcJs : jsLink
    ,loadCall : function(){
      mainLayer.fadeOut(fadeSpeed ,  function(){
        contentLayer.fadeIn(fadeSpeed);
      });
    }
  });
}

function cardLoad(cardIdx){
  var rid = null;
  var fnName = 'cardLoadSend';
  var data = {'cardIdx': cardIdx ,'fnName': fnName };
  var id = 'all';
  nodeDataSend('nodeDataProcessing', id , rid , data);
  cardLoadSend(cardIdx);
}

//결과값 선생한테 주기
function tranceReslut(data){

}



