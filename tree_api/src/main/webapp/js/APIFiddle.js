/**
 * 
 */
 $(document).ready(function() {
	    	$.fn.serializeObject = function()
	    	{
	    	    var o = {};
	    	    var a = this.serializeArray();
	    	    $.each(a, function() {
	    	        if (o[this.name] !== undefined) {
	    	            if (!o[this.name].push) {
	    	                o[this.name] = [o[this.name]];
	    	            }
	    	            o[this.name].push(this.value || '');
	    	        } else {
	    	            o[this.name] = this.value || '';
	    	        }
	    	    });
	    	    return o;
	    	};
	    });
        
        function _callAPI() {
            var pType = apiType;
            var pCallUrl = CONTEXT_PATH + apiUrl;
            var pData = $('#FormTemplate').find('form').serializeObject();

            if(pType == 'ajaxForm'){ // file upload API use 
            	
            	callAjaxSubmit(pCallUrl);

            }else{
            	callAjaxData(pType,pCallUrl,pData);	
            } 
        	
        }
        
        /*
         * 파일 업로드에서 사용되는 ajax call method 
         */
        function callAjaxSubmit( callUrl){
            var frm = $('#frm');
            var stringData = frm.serialize();
            
            console.log("callUrl  = "+callUrl);

            frm.ajaxSubmit({
                type : "post",
                url : callUrl,
                beforeSend : function(req) {
                    req.setRequestHeader("Accept", "application/json");
                },
                async : false,
                cache : false,
                data : stringData,
                success : function(data) {
                    var code = data.code;
                    var msg = data.message;
                    if (code == "0000") {
                    	alert("등록 되었습니다.");
                    }else{
                    	alert('등록 실패 [msg]'+msg);
                    }
                },
                error : function(xhr, ajaxOptions, thrownError) {
                	console.log(thrownError);
                    alert("error");
                },
                complete : function(xhr, textStatus) {
                    isLogging = false;
                }
            });
            
        }
        
       
        
        function callAjaxData(type, callUrl, pData){
        	$("#RequestForm > div").text(JSON.stringify(pData));
            $.ajax({            
                type : type,
                xhrFields: {
                    withCredentials: true
                 },
                crossDomain : true,
                url : callUrl,
                async : false,
                cache : false,
                headers: {        
                	Accept : "application/json"      
                },
                datatype : "json",
                data : pData,
                success : function(data){
                	$("#ResultForm > div").text(JSON.stringify(data));
                },
                error: function (xhr, ajaxOptions, thrownError){
                	   alert("code:"+xhr.status+"\n"+"message:"+xhr.responseText+"\n"+"error:"+thrownError);
                }, 
                complete:function (xhr, textStatus){
                }  
            });
        }
        
        var apiType = '';
        var apiUrl = '';
        var targetForm = '';
        
        function _makeAPIForm(formId) {
        	if (formId == '') return;
        	targetForm = formId;
        	var divObj = $('#' + formId).find('div');
        	
        	var apiTitle = $(divObj).eq(0).html();
        	var apiDesc = $(divObj).eq(1).html();
        	apiType = $(divObj).eq(2).text();
        	apiUrl = $(divObj).eq(3).text();
        	var apiForm = $(divObj).eq(4).html();
            
            $('#ApiDesc').find('b').eq(0).html(apiTitle);
            $('#ApiDesc').find('pre').eq(0).html(apiDesc);
            $('#FormTemplate').find('form').eq(0).html(apiForm);
            
            // Form 초기화
            $("#RequestForm > div").text("No Request");
            $("#ResultForm > div").text("No Response");
        }