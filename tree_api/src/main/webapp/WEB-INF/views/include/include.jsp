    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <link rel="stylesheet" type="text/css" href="<%=CSS_PATH%>/APIFiddle.css" />
    
    <!--js  in include.jsp -->
    <script type="text/javascript" src="<%=JS_PATH %>/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery-form.js" ></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery.validate.js" ></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/spin.min.js"></script>
    <script type="text/javascript" src="<%=JS_PATH%>/jquery.cookie.js"></script>
    
    <script type="text/javascript" src="<%=JS_PATH%>/ajaxupload.js"></script>
    
    <script type="text/javascript" src="<%=JS_PATH%>/APIFiddle.js"></script>
