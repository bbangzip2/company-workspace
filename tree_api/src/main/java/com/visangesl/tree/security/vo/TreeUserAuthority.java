package com.visangesl.tree.security.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 사용자의 권한정보를 담는 VO
 * 
 * @author imac
 *
 */
public class TreeUserAuthority extends VSObject {
	private String role = null;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
