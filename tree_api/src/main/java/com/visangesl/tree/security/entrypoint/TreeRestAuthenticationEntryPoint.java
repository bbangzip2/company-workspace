package com.visangesl.tree.security.entrypoint;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Spring security 설정 중 url별 접근권한에 맞지 않는 접근을 하는 경우를 처리하는 class
 * 
 * @author imac
 *
 */
public final class TreeRestAuthenticationEntryPoint implements
		AuthenticationEntryPoint {

	/**
	 * HTTP 401 Status Code를 전송
	 */
	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {
		// 상수 정의를 해야하나 추후 spring security관련 모듈로 분리시 작업을 해야할듯 함
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
	}

}
