package com.visangesl.tree.security.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;

/**
 * 인증 성공시 처리를 위한 handler
 * 
 * @author imac
 *
 */
public class TreeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	protected Log logger = LogFactory.getLog(this.getClass());
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	        
    /**
     * 인증 성공시 호출되는 method
     */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		handle(request, response, authentication);
        clearAuthenticationAttributes(request);
	}
	
	/**
	 * 인증 성공시 client에 사용자 기본정보를 JSON으로 넘기기 위한 처리
	 * 
	 * @param request
	 * @param response
	 * @param authentication
	 * @throws IOException
	 */
	protected void handle(HttpServletRequest request, 
		HttpServletResponse response, Authentication authentication) throws IOException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		// 상수 정의를 해야하나 추후 spring security관련 모듈로 분리시 작업을 해야할듯 함
		map.put("code", "0000");
		map.put("message", "성공");
		map.put("paramMap",null);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("mbrGrade",TreeSpringSecurityUtils.getAuthoritiesToRoles(authentication));
		resultMap.put("campNm",null);
		resultMap.put("useYn","Y");
		resultMap.put("memberId",TreeSpringSecurityUtils.getPrincipalAuthorities().getUsername());
		resultMap.put("serverPath",null);
		resultMap.put("memberName",TreeSpringSecurityUtils.getPrincipalAuthorities().getName());
		resultMap.put("profileImgPath",TreeSpringSecurityUtils.getPrincipalAuthorities().getProfilePhotoPath());
		resultMap.put("nickname",TreeSpringSecurityUtils.getPrincipalAuthorities().getNickname());
		resultMap.put("todayYmd",null);
		resultMap.put("name",null);
		        
        response.setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.OK.value());
		
        map.put("result", resultMap);
        
        JSONObject jsonObject = JSONObject.fromObject(map);
        OutputStream out = response.getOutputStream();
        out.write(jsonObject.toString().getBytes());
	}

	/**
	 * 세션에 저장되었을지도 모를 인증실패관련 정보를 제거
	 * 
	 * @param request
	 */
	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	/**
	 * redirect관련 처리를 위한 method 인것 같음
	 * 
	 * @param redirectStrategy
	 */
	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}
	/**
	 * redirect관련 처리를 위한 method 인것 같음
	 *  
	 * @return
	 */
	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}
}