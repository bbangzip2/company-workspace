package com.visangesl.tree.security.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 인증정보 확인시 필요한 정보를 조회하기 위한 조건을 담을 VO
 * 
 * @author imac
 *
 */
public class UsernameCondition extends VSCondition {
	/**
	 * 사용자 ID
	 */
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
