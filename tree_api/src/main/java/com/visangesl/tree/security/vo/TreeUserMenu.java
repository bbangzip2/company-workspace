package com.visangesl.tree.security.vo;

import java.io.Serializable;

import com.visangesl.tree.vo.VSObject;

/**
 * 사용자가 접근가능한 메뉴정보를 담을 VO
 * 현재는 관리자에서만 사용됨
 * 
 * @author imac
 *
 */
public class TreeUserMenu extends VSObject implements Serializable {
	private static final long serialVersionUID = -2533142123119578112L;
	
	private int menuSeq;
	private String menuNm;
	private String linkUrl;
	private String linkSect;
	private int grpSeq;
	private int depthSeq;
	private int sortOrd;
	
	public int getMenuSeq() {
		return menuSeq;
	}
	public void setMenuSeq(int menuSeq) {
		this.menuSeq = menuSeq;
	}
	public String getMenuNm() {
		return menuNm;
	}
	public void setMenuNm(String menuNm) {
		this.menuNm = menuNm;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	public String getLinkSect() {
		return linkSect;
	}
	public void setLinkSect(String linkSect) {
		this.linkSect = linkSect;
	}
	public int getGrpSeq() {
		return grpSeq;
	}
	public void setGrpSeq(int grpSeq) {
		this.grpSeq = grpSeq;
	}
	public int getDepthSeq() {
		return depthSeq;
	}
	public void setDepthSeq(int depthSeq) {
		this.depthSeq = depthSeq;
	}
	public int getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(int sortOrd) {
		this.sortOrd = sortOrd;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
