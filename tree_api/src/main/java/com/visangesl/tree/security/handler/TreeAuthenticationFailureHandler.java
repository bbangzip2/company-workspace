package com.visangesl.tree.security.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * 인증 실패시 처리를 위한 handler
 * @author imac
 *
 */
public class TreeAuthenticationFailureHandler implements
		AuthenticationFailureHandler {

	/**
	 * 인증 실패시 오류정보를 JSON형태로 client에 response
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		Map<String, Object> map = new HashMap<String, Object>();
		// 상수 정의를 해야하나 추후 spring security관련 모듈로 분리시 작업을 해야할듯 함
		map.put("code", "9999");
		map.put("message", "로그인 오류");

        response.setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        
        JSONObject jsonObject = JSONObject.fromObject(map);
        OutputStream out = response.getOutputStream();
        out.write(jsonObject.toString().getBytes());
	}

}
