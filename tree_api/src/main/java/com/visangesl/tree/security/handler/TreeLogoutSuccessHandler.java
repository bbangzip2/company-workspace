package com.visangesl.tree.security.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * 로그아웃 처리 성공시 handler
 * 
 * @author imac
 *
 */
public class TreeLogoutSuccessHandler implements LogoutSuccessHandler {

	/**
	 * 로그아웃 성공시 호출될 method
	 */
	@Override
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		Map<String, Object> map = new HashMap<String, Object>();
		// 상수 정의를 해야하나 추후 spring security관련 모듈로 분리시 작업을 해야할듯 함.
		map.put("code", "0000");
		map.put("message", "로그아웃 성공");
		
        response.setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        
        JSONObject jsonObject = JSONObject.fromObject(map);
        OutputStream out = response.getOutputStream();
        out.write(jsonObject.toString().getBytes());
	}

}
