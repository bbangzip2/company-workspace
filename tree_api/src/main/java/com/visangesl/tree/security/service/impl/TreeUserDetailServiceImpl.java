package com.visangesl.tree.security.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.visangesl.tree.security.mapper.SecurityMapper;
import com.visangesl.tree.security.vo.TreeUserAuthority;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.security.vo.TreeUserInfo;
import com.visangesl.tree.security.vo.UsernameCondition;

/**
 * 인증확인을 위해 필요한 사용자 정보를 조회하는 Service
 * 
 * @author imac
 *
 */
public class TreeUserDetailServiceImpl implements UserDetailsService {
	@Autowired
	SecurityMapper securityMapper;
	
	/**
	 * 사용자 ID로 사용자 정보를 조회
	 */
	@Override
	public TreeUserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {		
		UsernameCondition condition = new UsernameCondition();
		condition.setUsername(username);
		
		TreeUserInfo userInfo = null;
		try {
			userInfo = securityMapper.getUserInfo(condition);
		} catch (Exception e) {
			throw new UsernameNotFoundException("UsernameNotFoundException", e);
		}
		
		if (userInfo == null) {
			throw new UsernameNotFoundException("UsernameNotFoundException");
		}
		
		TreeUserDetails user = new TreeUserDetails();
		user.setUsername(username);
		user.setName(userInfo.getName());
		user.setPassword(userInfo.getPassword());
		user.setNickname(userInfo.getNickname());
		user.setProfilePhotoPath(userInfo.getProfilePhotoPath());
		user.setAccessMenus(userInfo.getAccessMenus());
		
		Collection<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
		
		for (TreeUserAuthority authority : userInfo.getAuthorities()) {
			roles.add(new SimpleGrantedAuthority(authority.getRole()));
		}
		
		user.setAuthorities(roles);
		
		return user;
	}

}
