package com.visangesl.treeapi.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * API Test용 웹페이지관련 controller
 * 
 * @author imac
 *
 */
@Controller
@RequestMapping(value="/test")
public class TestController {

	/**
	 * API Test 페이지
	 * @param request
	 * @return
	 */
	@RequestMapping(value="APIFiddle.do")
	public String apiFiddle(HttpServletRequest request) {
		return "APIFiddle";
	}

	/**
	 * 세션 Test 페이지
	 * @param request
	 * @return
	 */
    @RequestMapping(value="sessionTest.do")
    public String sessionTest(HttpServletRequest request) {
        return "sessionTest";
    }
}
