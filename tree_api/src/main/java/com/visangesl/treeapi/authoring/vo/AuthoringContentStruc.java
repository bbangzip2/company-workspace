package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 저작도구 카드 컨텐츠 구조 등록을 위한 정보를 담을 VO
 * @author user
 *
 */
public class AuthoringContentStruc extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 상위 컨텐츠 코드
     */
    private String upperContentSeq;
    /**
     * 컨텐츠 코드
     */
    private String contentSeq;
    /**
     * 정렬 순서
     */
    private String sortOrd;
    /**
     * 배포 여부
     */
    private String distYn;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getUpperContentSeq() {
        return upperContentSeq;
    }
    public void setUpperContentSeq(String upperContentSeq) {
        this.upperContentSeq = upperContentSeq;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
    public String getDistYn() {
        return distYn;
    }
    public void setDistYn(String distYn) {
        this.distYn = distYn;
    }

}
