package com.visangesl.treeapi.authoring.vo;


public class AuthoringCardInfo extends AuthoringMbrId {

    private String pid;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
