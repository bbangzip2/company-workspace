package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 저작도구 카드 상세 설명 정보를 담을 VO
 * @author user
 *
 */
public class AuthoringContentMultiLang extends VSCondition {

    /**
     * 컨텐츠 코드
     */
    private String contentSeq;
    /**
     * 컨텐츠 타입 북/레슨/차시/카드
     */
    private String type;
    /**
     * 상세 내용
     */
    private String descr;
    /**
     * 언어 구분
     */
    private String langSect;
    /**
     * 정렬 순서
     */
    private String sortOrd;

    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDescr() {
        return descr;
    }
    public void setDescr(String descr) {
        this.descr = descr;
    }
    public String getLangSect() {
        return langSect;
    }
    public void setLangSect(String langSect) {
        this.langSect = langSect;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
}
