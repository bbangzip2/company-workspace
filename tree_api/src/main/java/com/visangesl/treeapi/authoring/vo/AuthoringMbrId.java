package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 회원 ID 정보를 담을 VO
 * @author user
 *
 */
public class AuthoringMbrId extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

}
