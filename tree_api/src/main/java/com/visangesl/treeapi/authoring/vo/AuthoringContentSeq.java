package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 저작도구 컨텐츠 코드를 담을 VO
 * @author user
 *
 */
public class AuthoringContentSeq extends VSCondition {

    /**
     * 컨텐츠 코드
     */
    private String contentSeq;

    public String getContentSeq() {
        return contentSeq;
    }

    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
}
