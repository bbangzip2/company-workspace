package com.visangesl.treeapi.authoring.service;

import org.springframework.stereotype.Service;

import com.visangesl.treeapi.authoring.vo.AuthoringCardPathParse;

/**
 * 저작도구 생성 카드 내부의 경로를 변환하기 위한 처리
 * @author user
 *
 */
@Service
public interface AuthoringCardPathParseService {

    /**
     * 저작도구 생성 카드의 경로를 변환한다.
     *      html 상단 pathesl 경로 추가
     *      pathesl 변환
     * @param condition
     * @return
     * @throws Exception
     */
    public boolean parseAuthoringCardPath(AuthoringCardPathParse condition) throws Exception;

}
