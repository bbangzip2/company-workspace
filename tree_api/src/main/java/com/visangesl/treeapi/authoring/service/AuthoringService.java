package com.visangesl.treeapi.authoring.service;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.authoring.vo.AuthoringAddCard;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 저작도구 관련 처리
 * @author user
 *
 */
public interface AuthoringService {

    /**
     * 컨텐츠 북 리스트 조회
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentBookList() throws Exception;

    /**
     * 상품 북 리스트 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getProdBookList(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 레슨 리스트 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentLessonList(VSCondition condition) throws Exception;

    /**
     * 상품 레슨 리스트 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getProdLessonList(VSCondition condition) throws Exception;


    /**
     * 카드 상세 정보 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public VSObject getAuthoringCardInfo(VSCondition condition) throws Exception;

    /**
     * 저작도구 카드 정보 등록
     * @param authoring
     * @return
     * @throws Exception
     */
    public VSResult<VSObject> addAuthoringCard(AuthoringAddCard authoring) throws Exception;


    /**
     * 저작도구 카드 정보 수정
     * @param condition
     * @return
     * @throws Exception
     */
    public VSResult<VSObject> modifyAuthoringCard(VSCondition condition) throws Exception;

}
