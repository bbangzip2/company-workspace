package com.visangesl.treeapi.authoring.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.visangesl.treeapi.authoring.service.AuthoringCardCopyService;
import com.visangesl.treeapi.authoring.vo.AuthoringCardCopy;

/**
 * 저작도구 카드 복사 관련 처리
 * @author user
 *
 */
@Service
public class AuthoringCardCopyServiceImpl implements AuthoringCardCopyService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 저작도구 생성 카드를 복사 한다.
     */
    @Override
    public boolean copyAuthoringCard(AuthoringCardCopy condition) throws Exception {
        // TODO Auto-generated method stub
        boolean result = false;

        String filePath = condition.getFilePath();
        String toBeFilePath = condition.getToBePath();
        String thmbPath = condition.getThmbPath();
        String toBeThmbPath = condition.getToBeThmbPath();

        logger.debug(filePath);
        logger.debug(toBeFilePath);
        logger.debug(thmbPath);
        logger.debug(toBeThmbPath);


        // 복사할 원본 폴더
        File asIsDir = new File(filePath);
        if (!asIsDir.exists()) {
            return false;
        }


        // 복사될 폴더
        File toBeDir = new File(toBeFilePath);
        if (!toBeDir.exists()) {
            toBeDir.mkdirs();
        }


        // 폴더 복사 실행
        result = copyFolder(asIsDir, toBeDir);
        logger.debug("copyFolderResult: " + result);

        // 카드 복사 완료 후 썸네일 파일 복사 진행
        if (result && thmbPath != null) {
//            logger.debug(thmbPath);
//            logger.debug(toBeThmbPath);
            File toBeThmbDir = new File(toBeThmbPath);
            if (!toBeThmbDir.exists()) {
//                logger.debug("mkdir - " + toBeThmbPath);
                toBeThmbDir.mkdirs();
            }

            downloadFile(thmbPath, toBeThmbPath);

        }

        return result;
    }


    /**
     * 폴더/파일 복사
     * @param asIsDir
     * @param toBeDir
     * @return
     */
    public boolean copyFolder(File asIsDir, File toBeDir) {

        boolean result = true;

        // 폴더 복사
        File[] ff = asIsDir.listFiles();

        for (File file : ff) {

//            logger.debug("asIsDir - " + file.getAbsolutePath() + File.separator);

            File temp = new File(toBeDir.getAbsolutePath() + File.separator + file.getName());
            if (file.isDirectory()) {
//                logger.debug("isDir - " + toBeDir.getAbsolutePath() + File.separator + file.getName());
                temp.mkdirs();
                copyFolder(file, temp);
            } else {

                result = copyFile(file, temp);
            }
        }

        // 예외 발생 시 복사 진행된 폴더를 삭제
        if (!result) {
            logger.debug("copyFolder error");
            deleteDir(toBeDir);
        }
        return result;
    }


    /**
     * 파일 복사
     * @param asIsPath
     * @param toBePath
     * @return
     */
    public boolean copyFile(File asIsPath, File toBePath) {
        boolean result = true;

        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {

            fis = new FileInputStream(asIsPath);
            fos = new FileOutputStream(toBePath);

            byte[] buffer = new byte[1024];

            int length = 0;
            while((length = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, length);
            }

        } catch (Exception e) {
            logger.debug("Exception " + e.toString());
            result = false;
        } finally {
            try {
                if (fis != null) fis.close();
                if (fos != null) fos.close();
            } catch (IOException e) {
                result = false;
            }
        }
        return result;
    }


    /**
     * 폴더/파일 삭제
     * @param toBeDir
     * @return
     */
    public boolean deleteDir(File toBeDir) {

        if (!toBeDir.exists()) {
            return false;
        }

        File[] files = toBeDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                file.delete();
            }
        }

        return toBeDir.delete();
    }


    /**
     * 파일 다운로드
     * @param fileUrl
     * @param localFileName
     * @param downloadDir
     * @return
     */
    public int downloadFile(String fileUrl, String downloadDir) {
        int bufferSize = 1024;
        OutputStream outStream = null;
        URLConnection uCon = null;

        InputStream is = null;
        int byteWritten = 0;
        try {

            logger.debug("fileUrl: " + fileUrl);
            logger.debug("downloadDir: " + downloadDir);

            if (!new File(downloadDir).exists()) {
                new File(downloadDir).mkdirs();
            }

            int slashIndex = fileUrl.lastIndexOf('/');

            // 파일 어드레스에서 마지막에 있는 파일이름을 취득
            String localFileName = fileUrl.substring(slashIndex + 1);


            URL Url;
            byte[] buf;
            int byteRead;
            Url = new URL(fileUrl);
            outStream = new BufferedOutputStream(new FileOutputStream(downloadDir + File.separator + URLDecoder.decode(localFileName, "UTF-8")));
            uCon = Url.openConnection();
            is = uCon.getInputStream();
            buf = new byte[bufferSize];
            while ((byteRead = is.read(buf)) != -1) {
                outStream.write(buf, 0, byteRead);
                byteWritten += byteRead;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return byteWritten;
    }
}
