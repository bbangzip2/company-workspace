package com.visangesl.treeapi.authoring.vo;

/**
 * 저작도구 생성 카드의 경로를 변환하기 위한 정보를 담을 VO
 * @author user
 *
 */
public class AuthoringCardPathParse extends AuthoringMbrId {

    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 카드 코드
     */
    private String cardCd;

    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getCardCd() {
        return cardCd;
    }
    public void setCardCd(String cardCd) {
        this.cardCd = cardCd;
    }
}
