package com.visangesl.treeapi.authoring.service;

import org.springframework.stereotype.Service;

import com.visangesl.treeapi.authoring.vo.AuthoringCardCopy;

/**
 * 저작도구 카드 복사 관련 처리
 * @author user
 *
 */
@Service
public interface AuthoringCardCopyService {

    /**
     * 저작도구 생성 카드를 복사 한다.
     * @param condition
     * @return
     * @throws Exception
     */
    public boolean copyAuthoringCard(AuthoringCardCopy condition) throws Exception;

}
