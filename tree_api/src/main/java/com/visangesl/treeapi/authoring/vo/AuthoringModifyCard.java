package com.visangesl.treeapi.authoring.vo;


/**
 * 저작도구 카드 수정 정보를 담을 VO
 * @author user
 *
 */
public class AuthoringModifyCard extends AuthoringMbrId {

    /**
     * 컨텐츠 코드
     */
    private String contentSeq;
    /**
     * 저작도구 관리 순번
     */
    private String pid;
    /**
     * 컨텐츠 타입 - 북/레슨/차시/카드
     */
    private String type;
    /**
     * 카드 구분
     */
    private String cardSect;
    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 카드 타이틀
     */
    private String prodTitle;
    /**
     * 썸네일 이미지 경로
     */
    private String thmbPath;
    /**
     * 카드 파일 경로
     */
    private String filePath;
    /**
     * 카드 경로
     */
    private String cardPath;
    /**
     * 카드 타입
     */
    private String cardType;
    /**
     * 카드 스킬
     */
    private String cardSkill;
    /**
     * 학습 모드
     */
    private String studyMode;
    /**
     * 카드 레벨
     */
    private String cardLevel;
    /**
     * 카드 시간
     */
    private String time;
    /**
     * 카드 수정 가능 여부
     */
    private String editYn;
    /**
     * 카드 공유 가능 여부
     */
    private String openYn;
    /**
     * 키워드
     */
    private String keyword;
    /**
     * 점수 산정 방식
     */
    private String grading;
    /**
     * 카드 상세 설명 언어1
     */
    private String direcLang1;
    /**
     * 카드 상세 설명1
     */
    private String direc1;
    /**
     * 카드 상세 설명 언어2
     */
    private String direcLang2;
    /**
     * 카드 상세 설명2
     */
    private String direc2;
    /**
     * 사용 여부
     */
    private String useYn;
    /**
     * 배포 여부
     */
    private String distYn;

    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getThmbPath() {
        return thmbPath;
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getStudyMode() {
        return studyMode;
    }
    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getEditYn() {
        return editYn;
    }
    public void setEditYn(String editYn) {
        this.editYn = editYn;
    }
    public String getOpenYn() {
        return openYn;
    }
    public void setOpenYn(String openYn) {
        this.openYn = openYn;
    }
    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getGrading() {
        return grading;
    }
    public void setGrading(String grading) {
        this.grading = grading;
    }
    public String getDirecLang1() {
        return direcLang1;
    }
    public void setDirecLang1(String direcLang1) {
        this.direcLang1 = direcLang1;
    }
    public String getDirec1() {
        return direc1;
    }
    public void setDirec1(String direc1) {
        this.direc1 = direc1;
    }
    public String getDirecLang2() {
        return direcLang2;
    }
    public void setDirecLang2(String direcLang2) {
        this.direcLang2 = direcLang2;
    }
    public String getDirec2() {
        return direc2;
    }
    public void setDirec2(String direc2) {
        this.direc2 = direc2;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getDistYn() {
        return distYn;
    }
    public void setDistYn(String distYn) {
        this.distYn = distYn;
    }
    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }

}
