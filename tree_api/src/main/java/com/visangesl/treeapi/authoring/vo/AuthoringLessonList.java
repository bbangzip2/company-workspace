package com.visangesl.treeapi.authoring.vo;


/**
 * 해당 북 코드의 하위 레슨 리스트 조회를 위한 조건을 담을 VO
 * @author user
 *
 */
public class AuthoringLessonList extends AuthoringMbrId {

    /**
     * 북 코드
     */
    private String bookCd;

    public String getBookCd() {
        return bookCd;
    }

    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }

}
