package com.visangesl.treeapi.authoring.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.visangesl.treeapi.authoring.service.AuthoringCardPathParseService;
import com.visangesl.treeapi.authoring.vo.AuthoringCardPathParse;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.util.TreeUtil;

/**
 * 저작도구 생성 카드 내부의 경로를 변환하기 위한 처리
 * @author user
 *
 */
@Service
public class AuthoringCardPathParseServiceImpl implements AuthoringCardPathParseService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 저작도구 생성 카드 내부의 경로를 변환한다.
     *      html 상단 pathesl 경로 추가
     *      pathesl 변환
     */
    @Override
    public boolean parseAuthoringCardPath(AuthoringCardPathParse condition)
            throws Exception {

//        logger.debug("parseAuthoringCardPath");

        // 기본 경로 설정
        String basePath = TreeProperties.getProperty("tree_save_filepath");
        String cardPath = TreeProperties.getProperty("tree_card_path");
        String editCardPath = TreeProperties.getProperty("tree_edit_card_path");
        String contentPath = TreeProperties.getProperty("tree_client_content_path");
        String bookCd = condition.getBookCd();
        String lessonCd = condition.getLessonCd();
        String cardCd = condition.getCardCd();
        String mbrId = condition.getMbrId();

        String prodPath = File.separator + bookCd + File.separator + lessonCd + File.separator + cardCd;
        String cardFilePath = basePath + cardPath + editCardPath + File.separator + mbrId + File.separator + prodPath;

        File sourceF = new File(cardFilePath);
        String replacePath = contentPath + prodPath;
        String pathESL = "pathesl='" + replacePath + "'";

        // 카드 변환 메소드 호출
        boolean result = htmlParser(sourceF, replacePath, pathESL, contentPath, bookCd, lessonCd, cardCd);

        logger.debug("parseAuthoringCardPath:" + result);

        return result;
    }


    /**
     * html 상단 pathesl 경로 추가 및 pathesl 변환
     * @param sourceF 카드 경로
     * @param replacePath 변환될 pathesl 경로
     * @param pathESL 추가될 pathesl 경로
     * @param contentPath 카드 최상위 폴더
     * @param bookCd 북 코드
     * @param lessonCd 레슨 코드
     * @param cardCd 카드 코드
     * @return
     * @throws IOException
     */
    public boolean htmlParser(File sourceF, String replacePath, String pathESL, String contentPath, String bookCd, String lessonCd, String cardCd) throws IOException {

        List<String> fileList = new ArrayList<String>();
        boolean result = true;

//        String filePath = "";    // 파일 경로
//        String fileName = "";    // 파일 명

        String keyAttr = "pathesl";                 // 파싱 대상 키 Attr
//        replaceUrl = "http://10.30.0.246:9090";     // 파싱 값 url


        // 대상 폴더의 html/js/css 파일 리스트 추출
        File[] listFiles = sourceF.listFiles();

        if (listFiles != null) {
            for (File file : listFiles) {
                generateFileList(fileList, file);
            }


            // html/js/css 변환
            for (String htmlFile : fileList) {
                File file = new File(htmlFile);

                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                Document doc = null;

                try {
    //logger.logger("file.getName(): " + file.getName());

                    // pageLoad.html 파일 여부 체크
                    if (file.getName().equals("pageLoad.html") || file.getName().equals("pageLoad.htm")) {
                        // doc에 pathesl 변수(카드 경로) 추가
                        doc = Jsoup.parse("<script>" + pathESL + "</script>");
                    } else {
                        doc = Jsoup.parse("");
                    }


                    // doc에 html 파일 읽어서 붙이기
                    StringBuffer sb = new StringBuffer();
                    String ch;
                    while((ch = bufferedReader.readLine()) != null) {

                        // pathesl 주석이 있는 줄이 있으면 다음줄을 읽어와 리플레이스 한다.
                        if (ch != null && ch.indexOf("//pathesl") > 0) {
                            // 주석이 있는 줄 저장
                            sb.append(ch + System.getProperty("line.separator"));
                            ch = bufferedReader.readLine();
    logger.debug("asis: " + ch);
                            ch = replacePath(ch, contentPath, bookCd, lessonCd, cardCd);
    logger.debug("tobe: " + ch);
                        }

                        if (TreeUtil.fileUploadExt(file.getName()).equals("json")) {
                            if (ch.indexOf("../") >= 0) {
                                String temp = contentPath + "/" + bookCd + "/" + lessonCd + "/" + cardCd + "/";
                                logger.debug(ch);
                                logger.debug(temp);
                                ch = ch.replaceAll("\\.\\./", temp);
                            }
                        }

                        sb.append(ch + System.getProperty("line.separator"));
                    }
                    doc.append(sb.toString());
    logger.debug(doc.toString());
                    doc.outputSettings().prettyPrint(false);


                    // html 파일 여부 체크
                    if (TreeUtil.fileUploadExt(file.getName()).equals("html") || TreeUtil.fileUploadExt(file.getName()).equals("htm")) {

                        // keyElement 항목을 가진 Elements 조회
                        Elements elements = doc.select("[" + keyAttr + "]");

                        // 조회 결과 카운트 체크
                        if (elements.size() > 0) {
                            String keylVal = "";    // src / href ...
                            String attrVal = "";    // 변경될 attr의 value (img src="attrVal")

                            for (Element e : elements) {

                                boolean pathFlag = false;

                                // keyAttr 의 값 조회(pathesl="src" / pathesl="href")
                                keylVal = e.attr(keyAttr);
                                logger.debug("keyAttr: " + keyAttr + ", keylVal: " + keylVal);

                                if (keylVal != null && !keylVal.equals("")) {
                                    // 변경할 Element의 attr(src/href) 내용 조회
                                    attrVal = e.attr(keylVal);
                                    logger.debug("replaceUrl:" + cardCd + ", attrVal: " + attrVal);

                                    String temp = "";
                                    if (attrVal.indexOf("../") >= 0) {
                                        Pattern p = Pattern.compile("\\.\\./");
                                        Matcher m = p.matcher(attrVal);

                                        int count = 0;
                                        while (m.find()) {
                                            count++;
                                        }

                                        temp = attrVal.substring(attrVal.lastIndexOf("../")+3, attrVal.length());

                                        switch (count) {
                                            case 1:
                                                temp = contentPath + File.separator + bookCd + File.separator + lessonCd + File.separator + cardCd + File.separator + temp;
                                                break;
                                            case 2:
                                                temp = contentPath + File.separator + bookCd + File.separator + lessonCd + File.separator + temp;
                                                break;
                                            case 3:
                                                temp = contentPath + File.separator + bookCd + File.separator + temp;
                                                break;
                                            case 4:
                                                temp = contentPath + File.separator + temp;
                                                break;
                                            default:
                                                break;
                                        }

                                        // 변경 값 저장
                                        e.attr(keylVal, temp);

                                        pathFlag = true;
                                    }

                                    if (attrVal.indexOf("./") >= 0) {
                                        Pattern p = Pattern.compile("\\./");
                                        Matcher m = p.matcher(attrVal);

                                        int count = 0;
                                        while (m.find()) {
                                            count++;
                                        }

                                        temp = attrVal.substring(attrVal.lastIndexOf("./")+2, attrVal.length());

                                        switch (count) {
                                            case 1:
                                                temp = contentPath + File.separator + bookCd + File.separator + lessonCd + File.separator + cardCd + File.separator + temp;
                                                break;
                                            case 2:
                                                temp = contentPath + File.separator + bookCd + File.separator + lessonCd + File.separator + temp;
                                                break;
                                            case 3:
                                                temp = contentPath + File.separator + bookCd + File.separator + temp;
                                                break;
                                            case 4:
                                                temp = contentPath + File.separator + temp;
                                                break;
                                            default:
                                                break;
                                        }

                                        // 변경 값 저장
                                        e.attr(keylVal, temp);

                                        pathFlag = true;
                                    }

//                                    // 변경 값 저장
//                                    e.attr(keylVal, temp);

                                    if (!pathFlag) {
                                        e.attr(keylVal, replacePath + attrVal);
                                    }
                                }
                            }
                        }

                    }


                    // head 내용이 없을 경우 자동으로 삽입되는 html/head/body를 삭제!
                    String head = doc.head().text();
                    if (head != null && head.equals("")) {
                        doc.select("html, head, body").unwrap();
                    }


                    // 변환 처리된 document를 string으로 반환
                    String resultHtml = HtmlUtils.htmlUnescape(doc.toString());
//                  logger.debug(resultHtml);


                    // 파일 저장
                    File output = new File(file.getAbsolutePath());
                    PrintWriter writer = new PrintWriter(output,"UTF-8");
                    writer.write(resultHtml);
                    writer.flush();
                    writer.close();


                } catch (IOException e) {
                    result = false;
                }
            }

        }

        logger.debug("htmlParser:" + result);

        return result;

    }


    /**
     * 특정 폴더 안의 pathesl 변환 대상 파일 검색
     * @param fileList
     * @param node
     */
    public void generateFileList(List<String> fileList, File node) {

        if (node.isFile()) {

            // 파일 확장자 확인
            String ext = TreeUtil.fileUploadExt(node.getName());

            // html 파일 경우 파싱 함수 호출
//            if (ext != null && (ext.equals("html") || ext.equals("htm") || ext.equals("js") || ext.equals("css") || ext.equals("json"))) {
            if (ext != null && (ext.equals("html") || ext.equals("htm") || ext.equals("css") || ext.equals("json"))) {
                fileList.add(node.getAbsolutePath().toString());
            }
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(fileList, new File(node, filename));
            }
        }
    }


    /**
     * pathesl을 가진 파일의 상대 경로를 컨텐츠 구조에 맞게 변환한다.
     * @param pathStr
     * @param contentPath
     * @param bookCd
     * @param lessonCd
     * @param resultStr
     * @return
     */
    public String replacePath(String pathStr, String contentPath, String bookCd, String lessonCd, String resultStr) {

        if (pathStr.indexOf("'pathesl'") > 0) {
            pathStr = pathStr.replace("'pathesl'", "pathesl");

        } else {

            String temp = "";

            Pattern p = Pattern.compile("\\.\\./");
            Matcher m = p.matcher(pathStr);

            int count = 0;
            while (m.find()) {
                count++;
            }

            switch (count) {
                case 1:
                    temp = contentPath + File.separator + bookCd + File.separator + lessonCd + File.separator + resultStr;
                    pathStr = pathStr.replace("..", temp);
                    break;
                case 2:
                    temp = contentPath + File.separator + bookCd + File.separator + lessonCd;
                    pathStr = pathStr.replace("../..", temp);
                    break;
                case 3:
                    temp = contentPath + File.separator + bookCd;
                    pathStr = pathStr.replace("../../..", temp);
                    break;
                case 4:
                    temp = contentPath;
                    pathStr = pathStr.replace("../../../..", temp);
                    break;
                default:
                    break;
            }

        }

        return pathStr;
    }
}
