package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSObject;

public class AuthoringLessonListVo extends VSObject {

    private String lessonCd;
    private String prodTitle;

    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
}
