package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 저작도구 카드/썸네일 복사를 위한 정보를 담는 VO
 * @author user
 *
 */
public class AuthoringCardCopy extends VSCondition {

    /**
     * 카드 원본 경로 (저작도구)
     */
    private String filePath;
    /**
     * 카드 복사할 경로
     */
    private String toBePath;
    /**
     * 썸네일 경로
     */
    private String thmbPath;
    /**
     * 썸네일 복사할 경로
     */
    private String toBeThmbPath;

    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getToBePath() {
        return toBePath;
    }
    public void setToBePath(String toBePath) {
        this.toBePath = toBePath;
    }
    public String getThmbPath() {
        return thmbPath;
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getToBeThmbPath() {
        return toBeThmbPath;
    }
    public void setToBeThmbPath(String toBeThmbPath) {
        this.toBeThmbPath = toBeThmbPath;
    }
}
