package com.visangesl.treeapi.authoring.vo;

import com.visangesl.tree.vo.VSObject;

public class AuthoringBookListVo extends VSObject {

    private String bookCd;
    private String prodTitle;

    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }

}