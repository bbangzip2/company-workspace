package com.visangesl.treeapi.mynote.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mapper.NoteMapper;
import com.visangesl.treeapi.mynote.service.MyNoteService;
import com.visangesl.treeapi.mynote.vo.MyNoteCondition;

/**
 * Note 관련 처리하는 class
 * 
 * @author hong
 *
 */
@Service
public class MyNoteServiceImpl implements MyNoteService {

    @Autowired
    NoteMapper noteMapper;

    /**
     * My Note > 레슨 목록 조회  
     */
    @Override
    public List<VSObject> getLessonList(MyNoteCondition myNoteCondition) throws Exception {
    	return noteMapper.getMyNoteLessonList(myNoteCondition);
    }

    /**
     * My note > Lesson 별 노트 목록 조회
     */
    @Override
    public List<VSObject> getMyNoteList(MyNoteCondition myNoteCondition) throws Exception {
    	return noteMapper.getMyNoteList(myNoteCondition);
    }
}