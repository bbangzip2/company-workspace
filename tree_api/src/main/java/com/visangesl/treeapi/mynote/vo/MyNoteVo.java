package com.visangesl.treeapi.mynote.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * note 정보 관련 Vo
 * 
 * @author hong
 *
 */
public class MyNoteVo extends VSObject {

	private String noteSeq;
	private String regDttm;
	private String prodSeq;
	private String noteText;
	private String cardNm;
//	private String imgNm;
	private String imgPath;
//	private String voiceNm;
	private String voicePath;
    private String mbrId;
    private String fileType;
    
    public String getNoteSeq() {
		return noteSeq;
	}

	public void setNoteSeq(String noteSeq) {
		this.noteSeq = noteSeq;
	}

	public String getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}

	public String getProdSeq() {
		return prodSeq;
	}

	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}

	public String getNoteText() {
		return noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getCardNm() {
		return cardNm;
	}

	public void setCardNm(String cardNm) {
		this.cardNm = cardNm;
	}

//	public String getImgNm() {
//		return imgNm;
//	}
//
//	public void setImgNm(String imgNm) {
//		this.imgNm = imgNm;
//	}

	public String getImgPath() {
        if (imgPath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + imgPath;
        } else {
            return imgPath;
        }
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

//	public String getVoiceNm() {
//		return voiceNm;
//	}
//
//	public void setVoiceNm(String voiceNm) {
//		this.voiceNm = voiceNm;
//	}

	public String getVoicePath() {
        if (voicePath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + voicePath;
        } else {
            return voicePath;
        }
	}

	public void setVoicePath(String voicePath) {
		this.voicePath = voicePath;
	}

	public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
