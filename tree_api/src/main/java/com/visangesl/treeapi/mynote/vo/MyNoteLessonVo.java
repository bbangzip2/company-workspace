package com.visangesl.treeapi.mynote.vo;

import com.visangesl.treeapi.vo.VSListCondition;

/**
 * Note의 lesson 정보 관련 Vo
 * 
 * @author hong
 *
 */
public class MyNoteLessonVo extends VSListCondition {

	private String lessonCd;
	private String lessonNm;

	public String getLessonCd() {
		return lessonCd;
	}
	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
}
