package com.visangesl.treeapi.mynote.service;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mynote.vo.MyNoteCondition;

/**
 * NOTE관련 처리하는 interface
 * 
 * @author hong
 *
 */
public interface MyNoteService {

	/**
	 * My Note > 레슨 목록 조회 
	 * 
	 * @param myNoteCondition
	 * @return
	 * @throws Exception
	 */
    public List<VSObject> getLessonList(MyNoteCondition myNoteCondition) throws Exception;
    
    /**
     * My note > Lesson 별 노트 목록 조회
     * 
     * @param myNoteCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getMyNoteList(MyNoteCondition myNoteCondition) throws Exception;
    
}
