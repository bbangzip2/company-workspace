package com.visangesl.treeapi.comment.service;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 코멘트와 좋아요 관련 처리
 * 
 * @author Claude
 *
 */
public interface CommentService {
    /**
     * 특정 콘텐츠의 코멘트를 등록
     * 
     * @param vsObject
     * @return
     */
	public VSResult<VSObject> addComment(VSObject vsObject);
	
	/**
	 * 특정 콘텐츠의 코멘트를 삭제
	 * 
	 * @param condition
	 * @return
	 */
	public VSResult<VSObject> removeComment(VSCondition condition);
	
	/**
	 * 특정 콘텐츠 속해 있는 코멘트 리스트 조회
	 * 
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getContentComment(VSCondition condition) throws Exception;
	
	/**
	 * 특정 콘텐츠의 좋아요 토글
	 * 
	 * @param condition
	 * @return
	 */
	public VSResult<Object> toggleLike(VSCondition condition);
	
	/**
	 * 특정 콘텐츠의 좋아요 유무 조회
	 * 
	 * @param condition
	 * @return
	 */
	public String existDoLike(VSCondition condition);
}
