package com.visangesl.treeapi.comment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.comment.service.CommentService;
import com.visangesl.treeapi.comment.vo.LikeCondition;
import com.visangesl.treeapi.comment.vo.LikeVo;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.exception.TreeRuntimeException;
import com.visangesl.treeapi.mapper.CommentMapper;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 코멘트와 좋아요를 처리
 * 
 * @author Claude
 *
 */
@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	CommentMapper commentMapper;
	
	/**
	 * 특정 콘텐츠의 코멘트를 등록
	 */
	@Override
	public VSResult<VSObject> addComment(VSObject vsObject) {
		VSResult<VSObject> result = new VSResult<VSObject>();
		
		int affectedRows = commentMapper.addComment(vsObject);
		
		if (affectedRows > 0) {
			result.setCode(TreeProperties.getProperty("error.success.code"));
			result.setMessage(TreeProperties.getProperty("error.success.msg"));
		} else {
			result.setCode(TreeProperties.getProperty("error.fail.code"));
			result.setMessage(TreeProperties.getProperty("error.fail.msg"));
		}
		
		return result;
	}

	/**
	 * 특정 콘텐츠의 코멘트를 삭제
	 */
	@Override
	public VSResult<VSObject> removeComment(VSCondition condition) {
		VSResult<VSObject> result = new VSResult<VSObject>();
		
		int affectedRows = commentMapper.removeComment(condition);
		
		if (affectedRows > 0) {
			result.setCode(TreeProperties.getProperty("error.success.code"));
			result.setMessage(TreeProperties.getProperty("error.success.msg"));
		} else {
			result.setCode(TreeProperties.getProperty("error.etc.code"));
			result.setMessage(TreeProperties.getProperty("error.fail.msg"));
		}
		
		return result;
	}

	/**
	 * 특정 콘텐츠 속해 있는 코멘트 리스트 조회
	 */
	@Override
	public List<VSObject> getContentComment(VSCondition condition) throws Exception {
		return commentMapper.getContentComment(condition);
	}

	/**
	 * 특정 콘텐츠의 좋아요 토글
	 */
	@Override
	public VSResult<Object> toggleLike(VSCondition condition) {
		VSResult<Object> result = new VSResult<Object>();
		
		try {
		    // 좋아요가 있는지 확인한다.
			String existDoLike = commentMapper.existDoLike(condition);
			if (existDoLike != null && existDoLike.equals("Y")) {
				// 좋아요 Y -> 좋아요 N
				int affectedRows = commentMapper.undoLike(condition);
				
				if (affectedRows > 0) {
					result.setCode(TreeProperties.getProperty("error.success.code"));
					result.setMessage(TreeProperties.getProperty("error.success.msg"));
					result.setResult("N");
				} else {
				    String code = TreeProperties.getProperty("error.fail.code");
				    String msg  = TreeProperties.getProperty("error.fail.msg");
					throw new TreeRuntimeException(code, msg);
				}

			} else {
                // 좋아요 N -> 좋아요 Y
			    LikeCondition likeCondition = (LikeCondition)condition;

			    LikeVo like = new LikeVo();
				like.setContentId(likeCondition.getContentId());
				like.setContentType(likeCondition.getContentType());
				like.setMbrId(likeCondition.getMbrId());
				
				int affectedRows = commentMapper.doLike(like);
				
				if (affectedRows > 0) {
					result.setCode(TreeProperties.getProperty("error.success.code"));
					result.setMessage(TreeProperties.getProperty("error.success.msg"));
					result.setResult("Y");
				} else {
				    String code = TreeProperties.getProperty("error.fail.code");
                    String msg  = TreeProperties.getProperty("error.fail.msg");
                    throw new TreeRuntimeException(code, msg);
				}

			}			
		} catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(TreeProperties.getProperty("error.etc.code"));
            result.setMessage(handler.getMessage());
		}
		
		return result;
	}
	
	/**
	 * 특정 콘텐츠의 좋아요 유무 조회
	 */
	@Override
	public String existDoLike(VSCondition condition) {
		return commentMapper.existDoLike(condition);
	}
}
