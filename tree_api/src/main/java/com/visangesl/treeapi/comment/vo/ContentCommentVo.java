package com.visangesl.treeapi.comment.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;

/**
 * 코멘트 리스트의 아이템을 담는 VO
 * 
 * @author Claude
 *
 */
public class ContentCommentVo extends VSObject {
    /**
     *  코멘트 순번
     */
	private int cmmtSeq;
	/**
	 *  코멘트 내용
	 */
	private String cmmtCntt;
	/**
	 *  맴버 아이디
	 */
	private String mbrId;
	/**
	 *  맴버 이름
	 */
	private String mbrNm;
	/**
	 *  맴버 등급
	 */
	private String mbrGrade;
	/**
	 *  맴버 사진
	 */
	private String profilePhotopath;
	/**
	 *  등록 일자
	 */
	private String regDttm;

    public int getCmmtSeq() {
        return cmmtSeq;
    }
    public void setCmmtSeq(int cmmtSeq) {
        this.cmmtSeq = cmmtSeq;
    }
    public String getCmmtCntt() {
        return cmmtCntt;
    }
    public void setCmmtCntt(String cmmtCntt) {
        this.cmmtCntt = cmmtCntt;
    }
    public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getMbrNm() {
		return mbrNm;
	}
	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}
	public String getMbrGrade() {
		return mbrGrade;
	}
	public void setMbrGrade(String mbrGrade) {
		this.mbrGrade = mbrGrade;
	}
	public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotopath;
        }
        
		return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
}
