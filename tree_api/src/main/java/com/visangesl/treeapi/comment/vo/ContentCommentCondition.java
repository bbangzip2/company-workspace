package com.visangesl.treeapi.comment.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 코멘트 리스트를 조회하기 위해 조건 정보를 담는 VO
 * 
 * @author Claude
 *
 */
public class ContentCommentCondition extends VSCondition {
    /**
     *  컨텐츠 타입
     */
	private String contentType;
	/**
	 *  컨텐츠 아이디
	 */
	private int contentId;
	
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	
	
}
