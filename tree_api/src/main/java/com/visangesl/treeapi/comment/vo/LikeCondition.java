package com.visangesl.treeapi.comment.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 좋아요를 조회하기 위한 조건 정보를 담는 VO
 * 
 * @author Claude
 *
 */
public class LikeCondition extends VSCondition {
    // 컨텐츠 타입
	private String contentType;
	// 컨텐츠 아이디
	private int contentId;
	// 맴버 아이디
	private String mbrId;
	
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	
	
}
