package com.visangesl.treeapi.comment.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 좋아요 정보를 담는 VO
 * 
 * @author Claude
 *
 */
public class LikeVo extends VSObject {
    /**
     *  컨텐트 타입
     */
	private String contentType;
	/**
	 *  컨텐츠 아이디
	 */
	private int contentId;
	/**
	 *  맴버 아이디
	 */
	private String mbrId;
	/**
	 *  등록 일자
	 */
	private String regDttm;
	
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
}
