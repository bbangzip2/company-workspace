package com.visangesl.treeapi.comment.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 코멘트를 삭제하기 위한 정보를 담는 VO
 * 
 * @author Claude
 *
 */
public class CommentCondition extends VSCondition {
    /**
     * 코멘트 순번
     */
	private int cmmtSeq;
	/**
	 * 맴버 아이디
	 */
	private String mbrId;
	
    public int getCmmtSeq() {
        return cmmtSeq;
    }
    public void setCmmtSeq(int cmmtSeq) {
        this.cmmtSeq = cmmtSeq;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
}
