package com.visangesl.treeapi.comment.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.comment.service.CommentService;
import com.visangesl.treeapi.comment.vo.CommentCondition;
import com.visangesl.treeapi.comment.vo.CommentVo;
import com.visangesl.treeapi.comment.vo.ContentCommentCondition;
import com.visangesl.treeapi.comment.vo.LikeCondition;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.util.TreeUtil;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 코멘트와 좋아요를 처리하는 Controller
 * 
 * @author Claude
 *
 */
@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;
    
    /**
     * 특정 콘텐츠의 코멘트를 등록
     * 
     * @param request
     * @param cmmtCntt     코멘트 내용
     * @param contentType  컨텐츠 타입
     * @param contentId    컨텐츠 아이디
     * @return
     */
    @RequestMapping(value="/comment/addComment", method=RequestMethod.POST)
    @ResponseBody
    public VSResult<VSObject> addComment(HttpServletRequest request
            , @RequestParam(value = "cmmtCntt", required = true, defaultValue = "") String cmmtCntt
            , @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
            , @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId) {
        
        // 세션에서 유저 아이디를 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();
        
        CommentVo comment = new CommentVo();
        comment.setCmmtCntt(cmmtCntt);
        comment.setContentType(contentType);
        comment.setContentId(contentId);
        comment.setMbrId(mbrId);
        
        return commentService.addComment(comment);
    }
    
    /**
     * 특정 콘텐츠의 코멘트를 삭제
     * 
     * @param request
     * @param cmmtSeq   코멘트 순번
     * @return
     */
    @RequestMapping(value="/comment/removeComment", method=RequestMethod.POST)
    @ResponseBody
    public VSResult<VSObject> removeComment(HttpServletRequest request
            , @RequestParam(value = "cmmtSeq", required = true, defaultValue = "") int cmmtSeq) {
        
        // 세션에서 유저 아이디를 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();
        
        CommentCondition condition = new CommentCondition();
        condition.setCmmtSeq(cmmtSeq);
        condition.setMbrId(mbrId);
        
        return commentService.removeComment(condition);
    }
    
    /**
     * 특정 콘텐츠 속해 있는 코멘트 리스트 조회
     * 
     * @param request
     * @param contentType
     * @param contentId
     * @return
     */
    @RequestMapping(value="/comment/getContentComment", method=RequestMethod.GET)
    @ResponseBody
    public VSResult<Object> getContentComment(HttpServletRequest request
            , @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
            , @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId) {
        
        ContentCommentCondition condition = new ContentCommentCondition();
        condition.setContentType(contentType);
        condition.setContentId(contentId);

        VSResult<Object> result = new VSResult<Object>();
        
        try {   
            result.setResult(commentService.getContentComment(condition));
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(TreeProperties.getProperty("error.queryerror.msg"));
        }
        
        return result;
    }
    
    /**
     * 특정 콘텐츠의 좋아요 토글
     * 
     * @param request
     * @param contentType
     * @param contentId
     * @return
     */
    @RequestMapping(value="/comment/toggleLike", method=RequestMethod.POST)
    @ResponseBody
    public VSResult<Object> toggleLike(HttpServletRequest request
            , @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
            , @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId) {
        
        // 세션에서 유저 아이디를 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();
        
        LikeCondition condition = new LikeCondition();
        condition.setContentType(contentType);
        condition.setContentId(contentId);
        condition.setMbrId(mbrId);
        
        return commentService.toggleLike(condition);
    }
    
    /**
     * 특정 콘텐츠의 좋아요 유무 조회
     * 
     * @param request
     * @param contentType
     * @param contentId
     * @return
     */
    @RequestMapping(value="/comment/existDoLike", method=RequestMethod.GET)
    @ResponseBody
    public VSResult<Object> existDoLike(HttpServletRequest request
            , @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
            , @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId) {
        
        // 세션에서 유저 아이디를 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();

        LikeCondition condition = new LikeCondition();
        condition.setContentType(contentType);
        condition.setContentId(contentId);
        condition.setMbrId(mbrId);
        
        VSResult<Object> result = new VSResult<Object>();
        
        try {
            result.setResult(TreeUtil.isNull(commentService.existDoLike(condition), "N"));
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(TreeProperties.getProperty("error.queryerror.msg"));
        }
        
        return result;
    }   
}
