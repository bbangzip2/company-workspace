package com.visangesl.treeapi.comment.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 코멘트를 등록할 때 담는 VO
 * 
 * @author Claude
 *
 */
public class CommentVo extends VSObject {
    /**
     *  코멘트 내용
     */
	private String cmmtCntt;
	/**
	 *  컨텐츠 타입
	 */
	private String contentType;
	/**
	 *  컨텐츠 아이디
	 */
	private int contentId;
	/**
	 *  맴버 아이디
	 */
	private String mbrId;
	/**
	 *  등록 일자
	 */
	private String regDttm;
	
    public String getCmmtCntt() {
        return cmmtCntt;
    }
    public void setCmmtCntt(String cmmtCntt) {
        this.cmmtCntt = cmmtCntt;
    }
    public String getContentType() {
        return contentType;
    }
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    public int getContentId() {
        return contentId;
    }
    public void setContentId(int contentId) {
        this.contentId = contentId;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(String regDttm) {
        this.regDttm = regDttm;
    }
}
