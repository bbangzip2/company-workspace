package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 컨텐츠 패키지 및 버전 정보를 담을 VO
 * @author user
 *
 */
public class ContentVersion extends VSCondition {
    /**
     * 사용자 ID
     */
    private String mbrId;
    /**
     * 클래스 코드
     */
    private String clsSeq;
    /**
     * 컨텐츠 코드 - 레슨 코드
     */
    private String contentSeq;
    /**
     * 컨텐츠 타입
     */
    private String type;
    /**
     * 패키지 파일 명
     */
    private String fileNm;
    /**
     * 패키지 파일 경로
     */
    private String filePath;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getFileNm() {
        return fileNm;
    }
    public void setFileNm(String fileNm) {
        this.fileNm = fileNm;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
