package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 레퍼런스 정보를 담는 Vo
 * 
 * @author hong
 *
 */
public class RefVo extends VSObject {
	private String refTitle;
	private String thmbPath;
    private String refSeq;
    private String curriCd;
    private String filePath;
    private String dtlCntt;
    private String refType;
    private String refCnt;
    
	public String getRefTitle() {
		return refTitle;
	}
	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}
	public String getRefSeq() {
		return refSeq;
	}
	public void setRefSeq(String refSeq) {
		this.refSeq = refSeq;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getThmbPath() {
        if (thmbPath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
        } else {
            return thmbPath;
        }
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getCurriCd() {
		return curriCd;
	}
	public void setCurriCd(String curriCd) {
		this.curriCd = curriCd;
	}
	public String getFilePath() {
		
        if (filePath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + filePath;
        } else {
            return filePath;
        }
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getDtlCntt() {
		return dtlCntt;
	}
	public void setDtlCntt(String dtlCntt) {
		this.dtlCntt = dtlCntt;
	}
	public String getRefCnt() {
		return refCnt;
	}
	public void setRefCnt(String refCnt) {
		this.refCnt = refCnt;
	}
}
