package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 카드맵 리스트 정보를 담을 VO
 * @author user
 *
 */
public class ContentCardMapList extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 차시 코드
     */
    private String dayNoCd;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
}
