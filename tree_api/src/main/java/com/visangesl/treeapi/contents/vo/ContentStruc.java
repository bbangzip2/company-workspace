package com.visangesl.treeapi.contents.vo;

/**
 * 컨텐츠 구조 정보 등록을 위한 정보를 담을 VO
 * @author user
 *
 */
public class ContentStruc extends ContentMbrId {

    /**
     * 상위 컨텐츠 코드
     */
    private String upperContentSeq;
    /**
     * 컨텐츠 코드
     */
    private String contentSeq;
    /**
     * 정렬 순서
     */
    private String sortOrd;

    public String getUpperContentSeq() {
        return upperContentSeq;
    }
    public void setUpperContentSeq(String upperContentSeq) {
        this.upperContentSeq = upperContentSeq;
    }
    public String getContentSeq() {
        return contentSeq;
    }
    public void setContentSeq(String contentSeq) {
        this.contentSeq = contentSeq;
    }
    public String getSortOrd() {
        return sortOrd;
    }
    public void setSortOrd(String sortOrd) {
        this.sortOrd = sortOrd;
    }
}
