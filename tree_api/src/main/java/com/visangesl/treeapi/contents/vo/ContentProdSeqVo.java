package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 컨텐츠 상품 코드 조회 정보를 담을 VO
 * @author user
 *
 */
public class ContentProdSeqVo extends VSObject {

    /**
     * 상품 코드
     */
    private String prodSeq;

    public String getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
}
