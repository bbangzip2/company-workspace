package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 차시 정보를 담을 VO
 * @author user
 *
 */
public class ContentDayNoListVo extends VSObject {

    /**
     * 상품 순번
     */
    private String prodSeq;
    /**
     * 상품 명
     */
    private String prodTitle;

    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
}
