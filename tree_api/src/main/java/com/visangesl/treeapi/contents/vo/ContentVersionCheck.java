package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 컨텐츠 패키지 버전 체크를 위한 정보를 담을 VO
 * @author user
 *
 */
public class ContentVersionCheck extends VSCondition {

    /**
     * 클래스 코드
     */
    private String clsSeq;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 버전
     */
    private String version;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

}
