package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 컨텐츠 다운로드 정보를 담을 VO
 * @author user
 *
 */
public class ContentDownloadVo extends VSObject {

    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 다운로드 URL
     */
    private String downloadUrl;
    /**
     * 서버 URI
     */
    private String serverUri;
    /**
     * 서버 PORT
     */
    private String serverPort;
    /**
     * 상품 폴더 (패키지 파일 루트 경로)
     */
    private String prodFolder;
    /**
     * 상품 코드
     */
    private String prodSeq;
    /**
     * 상품 타입
     */
    private String prodType;
    /**
     * 파일 경로
     */
    private String filePath;
    /**
     * 파일 명
     */
    private String fileName;
    /**
     * 파일 확장자
     */
    private String fileExt;
    /**
     * 컨텐츠 패키지 버전
     */
    private String version;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getDownloadUrl() {
        return downloadUrl;
    }
    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
    public String getServerUri() {
        return serverUri;
    }
    public void setServerUri(String serverUri) {
        this.serverUri = serverUri;
    }
    public String getServerPort() {
        return serverPort;
    }
    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }
    public String getProdFolder() {
        return prodFolder;
    }
    public void setProdFolder(String prodFolder) {
        this.prodFolder = prodFolder;
    }
    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdType() {
        return prodType;
    }
    public void setProdType(String prodType) {
        this.prodType = prodType;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileExt() {
        return fileExt;
    }
    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

}
