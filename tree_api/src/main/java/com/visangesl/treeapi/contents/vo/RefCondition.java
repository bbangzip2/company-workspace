package com.visangesl.treeapi.contents.vo;

import com.visangesl.treeapi.vo.VSListCondition;
/**
 * 레퍼런스 정보조회할 조건 정보를 담는 Vo
 * 
 * @author hong
 *
 */
public class RefCondition extends VSListCondition {
	private String prodType;
    private String prodSeq;
    
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
}
