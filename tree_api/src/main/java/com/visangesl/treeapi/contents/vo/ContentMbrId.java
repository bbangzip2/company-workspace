package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 회원 ID 정보를 담을 VO
 * @author user
 *
 */
public class ContentMbrId extends VSCondition {

    private String mbrId;

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
}
