package com.visangesl.treeapi.contents.vo;

/**
 * 차시 코드를 담을 VO
 * @author user
 *
 */
public class ContentDayNoCd extends ContentMbrId {

    /**
     * 차시 코드
     */
    private String dayNoCd;

    public String getDayNoCd() {
        return dayNoCd;
    }

    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
}
