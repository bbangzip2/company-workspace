package com.visangesl.treeapi.contents.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

/**
 * 패키징 대상 정보를 담을 VO
 * @author user
 *
 */
public class ContentPackageElement extends VSObject {

    /**
     * 클래스 코드
     */
    private String clsSeq;
    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 강사 ID
     */
    private String mbrId;
    /**
     * 카드 경로 리스트
     */
    private List<ContentCardPath> cardPathList;

    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public List<ContentCardPath> getCardPathList() {
        return cardPathList;
    }
    public void setCardPathList(List<ContentCardPath> cardPathList) {
        this.cardPathList = cardPathList;
    }


}
