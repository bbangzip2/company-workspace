package com.visangesl.treeapi.contents.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.contents.service.ContentsService;
import com.visangesl.treeapi.contents.vo.ContentDownloadVo;
import com.visangesl.treeapi.contents.vo.ContentVersionCheck;
import com.visangesl.treeapi.contents.vo.RefCondition;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.member.service.MemberService;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.util.TreeUtil;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 컨텐츠 관련 contoller
 *
 * @author hong
 *
 */
@Controller
public class ContentsController {

    @Autowired
    private ContentsService contentsService;

    @Autowired
    private MemberService memberService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 해당 레슨의 컨텐츠 패키지 버전 정보 체크
     *      버전 체크 후 하위 버전일때 최신 버전 정보를 리턴
     * @param request
     * @param response
     * @param lessonCd
     * @param version
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/versionCheck", method=RequestMethod.GET)
    public VSResult<Object> versionCheck(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "version", required = false, defaultValue = "") String version
            ) throws Exception {

        logger.debug("versionCheck");
        logger.debug("clsSeq: " + clsSeq);
        logger.debug("lessonCd: " + lessonCd);
        logger.debug("version: " + version);

        VSResult<Object> result = new VSResult<Object>();

        ContentVersionCheck contentVersionCheck = new ContentVersionCheck();
        contentVersionCheck.setClsSeq(clsSeq);
        contentVersionCheck.setLessonCd(lessonCd);
        contentVersionCheck.setVersion(version);

        // 1.컨텐츠 패키지 버전 정보 조회
        ContentDownloadVo contentDownloadVo = new ContentDownloadVo();
        contentDownloadVo = (ContentDownloadVo) contentsService.getContentPackageVersionInfo(contentVersionCheck);


        // 1-1.컨텐츠 패키지 버전 정보 조회 결과가 없을 경우 - 관리자 마스터 컨텐츠 패키지 버전 정보 조회
        if (contentDownloadVo == null) {
            contentVersionCheck.setClsSeq("");
            contentDownloadVo = (ContentDownloadVo) contentsService.getContentPackageVersionInfo(contentVersionCheck);
        }

        try {

            // 버전 정보 조회 결과 체크
            if (contentDownloadVo == null) {
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));

            } else {

                // 최신 컨텐츠 버전을 클라이언트의 컨텐츠 버전과 비교
                if (version.equals(contentDownloadVo.getVersion())) {
                    // 최신 컨텐츠 버전 == 클라이언트의 컨텐츠 버전
                    result.setMessage("SM");
                } else {

                    // 최신 컨텐츠 버전 != 클라이언트의 컨텐츠 버전
                    String cdnUrl = TreeProperties.getProperty("tree_cdn_url");
                    String downloadUrl = cdnUrl + contentDownloadVo.getFilePath() + File.separator + contentDownloadVo.getFileName();

                    contentDownloadVo.setBookCd(bookCd);
                    contentDownloadVo.setLessonCd(lessonCd);
                    contentDownloadVo.setDownloadUrl(downloadUrl);
                    contentDownloadVo.setServerUri(cdnUrl);
                    contentDownloadVo.setServerPort("80");
                    contentDownloadVo.setProdFolder(TreeProperties.getProperty("tree_package_path"));
                    contentDownloadVo.setFileExt("zip");

                    result.setResult(contentDownloadVo);
                    result.setMessage("JYP");
                }

                result.setCode(TreeProperties.getProperty("error.success.code"));
            }

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 전자칠판 공통기능 - 레퍼런스 파일 목록 조회
     * @param request
     * @param response
     * @param cid : 카드순번
     * @param refType : 레퍼런스 타입 코드
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/ReferenceList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getReferenceList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "cid", required = false, defaultValue = "") String cid
    		, @RequestParam(value = "refType", required = false, defaultValue = "") String refType) throws Exception {

        logger.debug("getReferenceList");

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {

        	String referType = TreeUtil.isNullCheck(refType);

            RefCondition refCondition = new RefCondition();
            refCondition.setProdSeq(cid);
            refCondition.setProdType(referType);

            List<VSObject> conList = contentsService.getReferenceList(refCondition);

            result.setResult(conList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 레퍼런스 총 갯수
     * @param request
     * @param response
     * @param cid : 카드 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/ReferenceListCnt", method=RequestMethod.GET)
    public VSResult<String> getReferenceListCnt(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "cid", required = false, defaultValue = "") String cid ) throws Exception {

        logger.debug("getReferenceListCnt");

        VSResult<String> result = new VSResult<String>();

        try {

            RefCondition refCondition = new RefCondition();
            refCondition.setProdSeq(cid);

            String refTypeCnt = contentsService.getReferenceListCnt(refCondition);

            result.setResult(refTypeCnt);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 레퍼런스 타입별 목록 조회
     * @param request
     * @param response
     * @param cid : 카드 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/contents/ReferenceTypeList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getReferenceTypeList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "cid", required = false, defaultValue = "") String cid ) throws Exception {

        logger.debug("getReferenceTypeList");

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {

            RefCondition refCondition = new RefCondition();
            refCondition.setProdSeq(cid);

            List<VSObject> conList = contentsService.getReferenceTypeList(refCondition);

            result.setResult(conList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }
}
