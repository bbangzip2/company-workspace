package com.visangesl.treeapi.contents.service;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.contents.vo.ContentCardMapVo;
import com.visangesl.treeapi.contents.vo.ContentCardSortList;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 컨텐츠 관련 처리 하는 interface
 *
 * @author hong
 *
 */
public interface ContentsService {

	/**
	 * 해당 카드의 상세 정보 조회
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public VSObject getContentsInfoData(VSCondition condition) throws Exception;

	/**
	 * 레퍼런스 파일 목록 조회
	 *
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getReferenceList(VSCondition condition) throws Exception;


	/**
	 * 해당 Lesson의 Day 데이터 리스트 조회
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getDayDataList(VSCondition condition) throws Exception;

	/**
	 * 해당 차시의 카드맵 데이터 리스트 조회.
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<ContentCardMapVo> getCardMapDataList(VSCondition condition) throws Exception;

	/**
	 * 해당 유저의 카드맵 정렬 값 저장 및 패키지 생성
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public VSResult<VSObject> createPackage(ContentCardSortList condition) throws Exception;

	/**
	 * 컨퍼런스 목록 총 갯수 조회
	 *
	 * @param vsObejct
	 * @return
	 * @throws Exception
	 */
	public String getReferenceListCnt(VSObject vsObejct) throws Exception;

	/**
	 * 레퍼런스 타입별 목록 조회
	 *
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception;


	/**
	 * 컨텐츠 패키지 버전 정보 조회
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public VSObject getContentPackageVersionInfo(VSCondition condition) throws Exception;

}

