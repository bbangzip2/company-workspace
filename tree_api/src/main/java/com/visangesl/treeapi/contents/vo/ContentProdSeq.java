package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 상품 코드 정보를 담을 VO
 * @author user
 *
 */
public class ContentProdSeq extends VSCondition {

    /**
     * 상품 코드
     */
    private String prodSeq;

    public String getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }

}
