package com.visangesl.treeapi.contents.vo;

/**
 * 카드 경로 정보를 담을 VO
 * @author user
 *
 */
public class ContentCardPath {

    /**
     * 카드 경로
     */
    private String cardPath;
    /**
     * 카드 파일 경로
     */
    private String filePath;

    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
