package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 컨텐츠 정보를 담을 VO
 * @author user
 *
 */
public class ContentVo extends VSObject {

    /**
     * 상품 코드
     */
    private String prodSeq;
    /**
     * 상품 명
     */
    private String title;
    /**
     * 상품 상세 내용
     */
    private String detailDescr;
    /**
     * 썸네일 이미지 경로
     */
    private String thmbPath;
    /**
     * 상품 파일 경로
     */
    private String filePath;
    /**
     * 카드 구분
     */
    private String cardSect;
    /**
     * 카드 타입
     */
    private String cardType;
    /**
     * 카드 스킬
     */
    private String cardSkill;
    /**
     * 스터디 모드
     */
    private String studyMode;
    /**
     * 카드 레벨
     */
    private String cardLevel;
    /**
     * 카드 시간
     */
    private String time;
    /**
     * 수정 가능 여부
     */
    private String updAbleYn;
    /**
     * 공유 가능 여부
     */
    private String openAbleYn;
    /**
     * 키워드
     */
    private String keyword;
    /**
     * 점수 산정 방식
     */
    private String grading;
    /**
     * 카드 설명 언어1
     */
    private String langSect1;
    /**
     * 카드 설명1
     */
    private String langDescr1;
    /**
     * 카드 설명 언어2
     */
    private String langSect2;
    /**
     * 카드 설명2
     */
    private String langDescr2;
    /**
     * 카드 경로
     */
    private String cardPath;

    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDetailDescr() {
        return detailDescr;
    }
    public void setDetailDescr(String detailDescr) {
        this.detailDescr = detailDescr;
    }
    public String getThmbPath() {
        if (thmbPath == null) {
            return thmbPath;
        } else {
            return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
        }
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getFilePath() {
        if (filePath == null) {
            return filePath;
        } else {
            return TreeProperties.getProperty("tree_cdn_url") + filePath;
        }
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getStudyMode() {
        return studyMode;
    }
    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getUpdAbleYn() {
        return updAbleYn;
    }
    public void setUpdAbleYn(String updAbleYn) {
        this.updAbleYn = updAbleYn;
    }
    public String getOpenAbleYn() {
        return openAbleYn;
    }
    public void setOpenAbleYn(String openAbleYn) {
        this.openAbleYn = openAbleYn;
    }
    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getGrading() {
        return grading;
    }
    public void setGrading(String grading) {
        this.grading = grading;
    }
    public String getLangSect1() {
        return langSect1;
    }
    public void setLangSect1(String langSect1) {
        this.langSect1 = langSect1;
    }
    public String getLangDescr1() {
        return langDescr1;
    }
    public void setLangDescr1(String langDescr1) {
        this.langDescr1 = langDescr1;
    }
    public String getLangSect2() {
        return langSect2;
    }
    public void setLangSect2(String langSect2) {
        this.langSect2 = langSect2;
    }
    public String getLangDescr2() {
        return langDescr2;
    }
    public void setLangDescr2(String langDescr2) {
        this.langDescr2 = langDescr2;
    }
    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }

}
