package com.visangesl.treeapi.contents.vo;

import com.visangesl.treeapi.property.TreeProperties;

/**
 * 카드맵 정보를 담을 VO
 * @author user
 *
 */
public class ContentCardMapVo {

    /**
     * 상품 코드
     */
    private String prodSeq;
    /**
     * 상품 멍
     */
    private String prodTitle;
    /**
     * 썸네일 이미지 경로
     */
    private String thmbPath;
    /**
     * 카드 파일 경로
     */
    private String filePath;
    /**
     * 상세 내용
     */
    private String dtlCntt;
    /**
     * 카드 구분
     */
    private String cardSect;
    /**
     * 카드 타입
     */
    private String cardType;
    /**
     * 카드 스킬
     */
    private String cardSkill;
    /**
     * 카드 레벨
     */
    private String cardLevel;
    /**
     * 학습 모드
     */
    private String studyMode;
    /**
     * 수정 가능 여부
     */
    private String editYn;
    /**
     * 공유 가능 여부
     */
    private String openYn;
    /**
     * 정렬 순서
     */
    private String sort;
    /**
     * 카드 경로
     */
    private String cardPath;
    /**
     * 카드 플레이 시간
     */
    private String time;

    public String getProdSeq() {
        return prodSeq;
    }
    public void setProdSeq(String prodSeq) {
        this.prodSeq = prodSeq;
    }
    public String getProdTitle() {
        return prodTitle;
    }
    public void setProdTitle(String prodTitle) {
        this.prodTitle = prodTitle;
    }
    public String getThmbPath() {
        if (thmbPath == null) {
            return thmbPath;
        } else {
            return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
        }
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getDtlCntt() {
        return dtlCntt;
    }
    public void setDtlCntt(String dtlCntt) {
        this.dtlCntt = dtlCntt;
    }
    public String getCardSect() {
        return cardSect;
    }
    public void setCardSect(String cardSect) {
        this.cardSect = cardSect;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardSkill() {
        return cardSkill;
    }
    public void setCardSkill(String cardSkill) {
        this.cardSkill = cardSkill;
    }
    public String getCardLevel() {
        return cardLevel;
    }
    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }
    public String getStudyMode() {
        return studyMode;
    }
    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }
    public String getEditYn() {
        return editYn;
    }
    public void setEditYn(String editYn) {
        this.editYn = editYn;
    }
    public String getOpenYn() {
        return openYn;
    }
    public void setOpenYn(String openYn) {
        this.openYn = openYn;
    }
    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }
    public String getCardPath() {
        return cardPath;
    }
    public void setCardPath(String cardPath) {
        this.cardPath = cardPath;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

}
