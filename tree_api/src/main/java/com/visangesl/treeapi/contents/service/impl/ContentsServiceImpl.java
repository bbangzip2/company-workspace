package com.visangesl.treeapi.contents.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.contents.service.ContentsService;
import com.visangesl.treeapi.contents.vo.ContentBookLessonCodeVo;
import com.visangesl.treeapi.contents.vo.ContentCardMapList;
import com.visangesl.treeapi.contents.vo.ContentCardMapVo;
import com.visangesl.treeapi.contents.vo.ContentCardPath;
import com.visangesl.treeapi.contents.vo.ContentCardSortList;
import com.visangesl.treeapi.contents.vo.ContentDayNoCd;
import com.visangesl.treeapi.contents.vo.ContentPackageElement;
import com.visangesl.treeapi.contents.vo.ContentProdSeq;
import com.visangesl.treeapi.contents.vo.ContentProdSeqVo;
import com.visangesl.treeapi.contents.vo.ContentStruc;
import com.visangesl.treeapi.contents.vo.ContentStrucVo;
import com.visangesl.treeapi.mapper.ContentsMapper;
import com.visangesl.treeapi.packaging.service.PackageService;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 컨텐츠 관련 처리
 * @author user
 *
 */
@Service
public class ContentsServiceImpl implements ContentsService {
	//private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	PackageService packageService;

	@Autowired
	ContentsMapper contentsMapper;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 해당 카드의 상세 정보 조회
     */
	@Override
    @Transactional(readOnly = true)
    public VSObject getContentsInfoData(VSCondition condition) throws Exception {

        return contentsMapper.getContentsInfoData(condition);
    }

	/**
	 * 레퍼런스 파일 목록 조회
	 */
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getReferenceList(VSCondition condition) throws Exception {
        return contentsMapper.getReferenceList(condition);
    }

	/**
	 * 해당 Lesson의 Day 데이터 리스트 조회
	 */
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDayDataList(VSCondition condition) throws Exception {
        return contentsMapper.getDayDataList(condition);
    }

    /**
     * 해당 차시의 카드맵 리스트 조회
     */
	@Override
    @Transactional(readOnly = true)
    public List<ContentCardMapVo> getCardMapDataList(VSCondition condition) throws Exception {
        return contentsMapper.getCardMapDataList(condition);
    }

	/**
	 * 패키지 생성
	 *     1. 컨텐츠 구조 정보 등록 - 개인화 북/레슨
	 *     2. 기존 컨텐츠 구조 삭제 - 개인화 차시 이하
	 *     3. 신규 컨텐츠 구조 등록
	 *     4. 패키징 대상 레슨 정보 생성
	 *     5. 패키징 메소드 호출
	 */
    @Override
    @Transactional
    public VSResult<VSObject> createPackage(ContentCardSortList condition) throws Exception {
        VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();

        String clsSeq = condition.getClsSeq();
        String cardSort = condition.getCardSort();
        String dayNoCd = condition.getDayNoCd();
        String mbrId = condition.getMbrId();

        String bookCd;
        String lessonCd;
        int affectedRows = 0;


        // ========================================================
        // 컨텐츠 구조 개인화 진행
        // ========================================================
        // 북/레슨/차시 관계 정보 조회
        ContentDayNoCd contentDayNoCdCondition = new ContentDayNoCd();
        contentDayNoCdCondition.setDayNoCd(dayNoCd);
        contentDayNoCdCondition.setMbrId(mbrId);
        List<VSObject> contentStrucList = contentsMapper.getContentStrucBookLessonDayNo(contentDayNoCdCondition);


        // 북/레슨/차시 구조 추가 등록 - 개인화
        for (VSObject strucCondition : contentStrucList) {
            ContentStrucVo contentStrucVo = (ContentStrucVo) strucCondition;

            ContentStruc ContentStrucCondition = new ContentStruc();
            ContentStrucCondition.setMbrId(mbrId);
            ContentStrucCondition.setUpperContentSeq(contentStrucVo.getUpperContentSeq());
            ContentStrucCondition.setContentSeq(contentStrucVo.getContentSeq());
            ContentStrucCondition.setSortOrd(contentStrucVo.getSortOrd());
            affectedRows = contentsMapper.addDayNoContentStrucWithDuplicateCheck(ContentStrucCondition);

            logger.debug("addDayNoContentStrucWithDuplicateCheck: " + affectedRows);
        }


        // 기존 카드 구조 정보 삭제
        ContentDayNoCd contentDayNoCd = new ContentDayNoCd();
        contentDayNoCd.setDayNoCd(dayNoCd);
        contentDayNoCd.setMbrId(mbrId);
        contentsMapper.deleteDayNoContentStruc(condition);


        // 신규 카드 구조 등록
        String[] cardSortArr = cardSort.split(",");

        ContentStruc contentStrucCondition = new ContentStruc();
        contentStrucCondition.setMbrId(mbrId);
        contentStrucCondition.setUpperContentSeq(dayNoCd);
        for (int i=0; i<cardSortArr.length; i++) {
            contentStrucCondition.setContentSeq(cardSortArr[i]);
            contentStrucCondition.setSortOrd(String.valueOf(i + 1));

            contentsMapper.addDayNoContentStruc(contentStrucCondition);
        }


        // 카드 맵의 카드 순만 변경되었을때는 패키징 처리를 진행 안함!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (!condition.isSortFlag()) {

            // ========================================================
            // 패키징 대상 조회 >>> 패키징 처리 (컨텐츠)
            // ========================================================
            // 북/레슨 코드 조회
            ContentDayNoCd contentDayNoCondition = new ContentDayNoCd();
            contentDayNoCondition.setDayNoCd(dayNoCd);
            ContentBookLessonCodeVo contentBookLessonCodeVo = (ContentBookLessonCodeVo) contentsMapper.getContentStrucBookLessonCd(contentDayNoCondition);
            bookCd = contentBookLessonCodeVo.getBookCd();
            lessonCd = contentBookLessonCodeVo.getLessonCd();


            // 레슨 하위 차시 코드 리스트 조회
            ContentProdSeq contentProdSeq = new ContentProdSeq();
            contentProdSeq.setProdSeq(lessonCd);
            List<VSObject> dayNoCdList = contentsMapper.getDayNoCdList(contentProdSeq);

            if (dayNoCdList != null && dayNoCdList.size() > 0) {

                List<ContentCardMapVo> dayNoCardMapList = new ArrayList<ContentCardMapVo>();  // 차시 카드맵 리스트 - 패키징 대상 카드맵 리스트 생성용
                List<ContentCardPath> cardPathList = new ArrayList<ContentCardPath>();        // 패키징 대상 카드의 경로 리스트 (카드경로, 카드파일경로)

                for (int i=0; i<dayNoCdList.size(); i++) {
                    ContentProdSeqVo contentProdSeqVo = (ContentProdSeqVo) dayNoCdList.get(i);

                    // 차시 하위 카드 리스트 조회
                    ContentCardMapList contentCardMapListCondition = new ContentCardMapList();
                    contentCardMapListCondition.setDayNoCd(contentProdSeqVo.getProdSeq());
                    contentCardMapListCondition.setMbrId(mbrId);
                    dayNoCardMapList = contentsMapper.getCardMapDataList(contentCardMapListCondition);


                    // 개인화된 카드맵 리스트가 없으면 관리자 ID로 재 조회 - mbrId가 비었을 경우
                    if (dayNoCardMapList == null || dayNoCardMapList.size() == 0) {
                        contentCardMapListCondition.setMbrId("");
                        dayNoCardMapList = contentsMapper.getCardMapDataList(contentCardMapListCondition);
                    }


                    // 레슨 카드 리스트에 카드 추가
                    for (ContentCardMapVo dayCardList : dayNoCardMapList) {

                        ContentCardPath contentCardPath = new ContentCardPath();
                        contentCardPath.setCardPath(dayCardList.getCardPath()); // 카드 경로
                        contentCardPath.setFilePath(dayCardList.getFilePath()); // 카드 파일 경로

                        // 카드 경로 저장
                        cardPathList.add(contentCardPath);
                    }

                }


                // 패키징 처리 호출 - 컨텐츠
                ContentPackageElement contentPackageElement = new ContentPackageElement();
                contentPackageElement.setClsSeq(clsSeq);
                contentPackageElement.setBookCd(bookCd);
                contentPackageElement.setLessonCd(lessonCd);
                contentPackageElement.setMbrId(mbrId);
                contentPackageElement.setCardPathList(cardPathList);

                boolean zipResult = false;
                zipResult = packageService.zipContentPackage(contentPackageElement);


                // ========================================================
                // 패키징 처리 - 오프라인 > 개발 연기됨
                // ========================================================


                if (zipResult) {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
                } else {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                }

            } else {
                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
            }

        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }

        return resultCodeMsg;
    }



    /**
     * 컨텐츠 패키지 버전 정보 조회
     */
    @Override
    public VSObject getContentPackageVersionInfo(VSCondition dataCondition) throws Exception {
        return contentsMapper.getContentPackageVersionInfo(dataCondition);
    }



    /**
     * 컨퍼런스 목록 총 갯수 조회
     */
    @Override
    public String getReferenceListCnt(VSObject vsObejct) throws Exception{
        return contentsMapper.getReferenceListCnt(vsObejct);
    }

    /**
     * 레퍼런스 타입별 목록 조회
     */
    @Override
    public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getReferenceTypeList(dataCondition);
    }

}


