package com.visangesl.treeapi.contents.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 북/레슨 코드 조회 정보를 담을 VO
 * @author user
 *
 */
public class ContentBookLessonCodeVo extends VSObject {

    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 레슨 코드
     */
    private String lessonCd;

    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
}
