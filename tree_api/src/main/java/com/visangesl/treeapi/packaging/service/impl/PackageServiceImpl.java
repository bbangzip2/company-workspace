package com.visangesl.treeapi.packaging.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.treeapi.contents.vo.ContentCardPath;
import com.visangesl.treeapi.contents.vo.ContentPackageElement;
import com.visangesl.treeapi.contents.vo.ContentVersion;
import com.visangesl.treeapi.mapper.ContentsMapper;
import com.visangesl.treeapi.packaging.service.PackageService;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.zip.service.ZipService;
import com.visangesl.treeapi.zip.vo.ZipElements;

/**
 * 패키지 관련 처리
 * @author user
 *
 */
@Service
public class PackageServiceImpl implements PackageService {

    @Autowired
    ZipService zipService;

    @Autowired
    ContentsMapper contentsMapper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 패키징 대상 컨텐츠를 압축하기 위한 처리 진행
     */
    @Override
    @Transactional(rollbackFor=Exception.class)
    public boolean zipContentPackage(ContentPackageElement condition) {

        boolean zipResult = false;
        boolean packageFlag = true;

        String clsSeq = condition.getClsSeq();
        String bookCd = condition.getBookCd();
        String lessonCd = condition.getLessonCd();
        String mbrId = condition.getMbrId();

        String rootBasePath = TreeProperties.getProperty("tree_save_filepath");                 // 패키징 대상 파일 경로 - 실제 저장 경로
//        String cardBasePath = TreeProperties.getProperty("tree_card_path");                     // 카드 기본 경로
        String packageBasePath = TreeProperties.getProperty("tree_package_path");               // 패키지 기본 경로
        String packageUserPath = TreeProperties.getProperty("tree_package_user_path");          // 패키지 사용자 기본 경로


        // 패키지 파일 경로 지정 - DB 저장 경로
        String packageFilePath = packageBasePath + packageUserPath + File.separator + mbrId + File.separator + bookCd;

        // 패키지 파일 경로 지정 - 실제 패키지 파일 저장 경로
        String packageFileRealPath = rootBasePath + packageFilePath;

        // 패키지 파일 명 지정
        String packageFileName = lessonCd + ".zip";

        // 패키지 압축 파일 내부 경로
        String zipEntryPath = File.separator + bookCd + File.separator + lessonCd;

        // 패키징 대상 카드의 경로 정보를 담은 리스트
        List<ContentCardPath> cardPathList = new ArrayList<ContentCardPath>();


        // 패키지 요소 카드 경로 체크
        for (ContentCardPath cardPathInfo : condition.getCardPathList()) {

            String cardPath = cardPathInfo.getCardPath();
            String cardFilePath = rootBasePath + cardPathInfo.getFilePath();

//            logger.debug("cardPath: " + cardPath);
//            logger.debug("cardFilePath: " + cardFilePath);

            // 대상 카드 폴더 유무 체크
            if (new File(cardFilePath).exists()) {
//                logger.debug("exists: " + cardPath);

                // 카드 경로 정보가 있을 경우 해당 경로를 사용
                if (cardPath != null && !cardPath.equals("")) {
                    int index = cardPath.lastIndexOf("/");

                    if (index > -1) {
                        zipEntryPath = File.separator + bookCd + cardPath.substring(0, index);
                    }
                }

                ContentCardPath contentCardPath = new ContentCardPath();
                contentCardPath.setCardPath(zipEntryPath);
                contentCardPath.setFilePath(cardFilePath);

//                logger.debug(zipEntryPath);
//                logger.debug(cardFilePath);

                // 카드 경로 저장
                cardPathList.add(contentCardPath);

            } else {
                logger.debug("not exists: " + cardPath);
//              logger.debug("폴더가 없네");
//              packageFlag = false;
//              break;
            }

        }


        packageFlag = true;
        if (packageFlag) {
            ZipElements zipElements = new ZipElements();
            zipElements.setZipFileSavePath(packageFileRealPath);
            zipElements.setZipFileName(packageFileName);
            zipElements.setZipPathList(cardPathList);

            try {

                // 패키지 요소 압축 처리 호출
                zipResult = zipService.zipFile(zipElements);


                // ========================================================
                // 패키징 버전 처리 진행
                // ========================================================
                if (zipResult) {

                    // 버전 처리 호출
                    ContentVersion contentVer = new ContentVersion();
                    contentVer.setMbrId(mbrId);
                    contentVer.setClsSeq(clsSeq);
                    contentVer.setContentSeq(lessonCd);
                    contentVer.setType("P");
                    contentVer.setFilePath(packageFilePath);
                    contentVer.setFileNm(packageFileName);

                    int count = contentsMapper.getContentVersionCount(contentVer);
                    if (count == 0) {
                        contentsMapper.addContentVersion(contentVer);    // 버전 등록
                    } else {
                        contentsMapper.updateContentVersion(contentVer); // 버전 업
                    }

                    logger.debug("컨텐츠 패키지 생성 완료");
                    logger.debug("컨텐츠 버전 업데이트 완료");

                }

            } catch (Exception e) {
                logger.error(e.toString());
            }

        } else {
            logger.error("패키징 실패");
        }

        return zipResult;
    }



}
