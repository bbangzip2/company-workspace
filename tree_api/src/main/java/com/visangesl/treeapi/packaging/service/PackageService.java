package com.visangesl.treeapi.packaging.service;

import com.visangesl.treeapi.contents.vo.ContentPackageElement;

/**
 * 패키지 관련 처리
 * @author user
 *
 */
public interface PackageService {

    /**
     * 패키징 처리
     * @param condition
     * @return
     * @throws Exception
     */
    public boolean zipContentPackage(ContentPackageElement condition) throws Exception;
}
