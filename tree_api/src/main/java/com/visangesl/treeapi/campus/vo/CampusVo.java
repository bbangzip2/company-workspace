package com.visangesl.treeapi.campus.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 캠퍼스 정보를 담을 VO
 * 
 * @author imac
 *
 */
public class CampusVo extends VSObject {
	private String campNm;

	public String getCampNm() {
		return campNm;
	}

	public void setCampNm(String campNm) {
		this.campNm = campNm;
	}


	
	
}
