package com.visangesl.treeapi.badge.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.badge.service.BadgeService;
import com.visangesl.treeapi.badge.vo.BadgeListCondition;
import com.visangesl.treeapi.badge.vo.BadgeTypeVo;
import com.visangesl.treeapi.badge.vo.BadgeVo;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 뱃지와 관련된 작업을 처리하는 Controller
 * 
 * @author imac
 *
 */
@Controller
public class BadgeController {

    @Autowired
    private BadgeService badgeService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 특정 사용자의 뱃지 목록을 조회 
     * 
     * @param request
     * @param response
     * @param recvMbrId 조회할 사용자ID
     * @param langSect 언어구분코드(CMM_CD에서 상위코드가 LA000인 코드를 참조)
     * @return @see VSResult<List<Object>>
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/badge/badgeList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getBadgeList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = true, defaultValue = "") String recvMbrId,
            @RequestParam(value = "langSect", required = false, defaultValue = "") String langSect
            ) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {
        	BadgeListCondition condition = new BadgeListCondition();
        	condition.setMbrId(recvMbrId);
        	if (langSect != null && !langSect.equals("")) {
        		condition.setLangSect(langSect);
        	}
        	
            List<VSObject> badgeList = badgeService.getBadgeList(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(badgeList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            logger.debug(e.toString());
        }

        return result;

    }

    /**
     * 특정 사용자에게 뱃지를 부여
     * 사용자는 컨텐츠 하나당 동일한 뱃지를 여러번 수여할 수 없다.
     * (여러번 부여하더라도 별도의 응답은 없이 성공값이 응답된다.)
     * 
     * @param request
     * @param response
     * @param memberId 뱃지를 부여하는 사용자 ID
     * @param targetId 뱃지를 수여하는 사용자 ID
     * @param type 뱃지 종류 (CMM_CD의 상위코드가 BG000인 코드를 참조)
     * @param code 뱃지 일련번호 (BADGE_INFO 테이블의 BADGE_CD값)
     * @param contentSeq 컨텐츠 일련번호
     * @param contentType 컨텐츠 종류 (CMM_CD의 상위코드가 CT000인 코드를 참조)
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/badge/setBadge", method=RequestMethod.POST)
    public VSResult<VSObject> setBadge(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String memberId,
            @RequestParam(value = "targetId", required = true, defaultValue = "") String targetId,
            @RequestParam(value = "type", required = true, defaultValue = "") String type,
            @RequestParam(value = "code", required = true, defaultValue = "") String code,
            @RequestParam(value = "contentSeq", required = false, defaultValue = "") String contentSeq,
            @RequestParam(value = "contentType", required = false, defaultValue = "") String contentType) throws Exception {
    	VSResult<VSObject> result = new VSResult<VSObject>();

		 try {
			 String regId = null;

			 if(memberId != null && memberId.length() > 0) {
				 regId = memberId;
			}

			 BadgeVo badgeVo = new BadgeVo();
			 badgeVo.setBadgeType(type);
			 badgeVo.setUseYn(TreeProperties.getProperty("tree_usable"));
			 badgeVo.setRegId(regId);
			 badgeVo.setRecvMbrId(targetId);
			 badgeVo.setBadgeCd(code);
			 badgeVo.setContentSeq(contentSeq);
			 badgeVo.setContentType(contentType);

			 badgeService.setBadge(badgeVo);

			 BadgeTypeVo badgeTypeVo = new BadgeTypeVo();
			 badgeTypeVo.setBadgeType(type);
			 result.setCode(TreeProperties.getProperty("error.success.code"));
			 result.setMessage(TreeProperties.getProperty("error.success.msg"));
			 result.setResult(badgeTypeVo);
		 } catch (Exception e) {
			 ExceptionHandler handler = new ExceptionHandler(e);
			 result.setCode(handler.getCode());
			 result.setMessage(handler.getMessage());
			 logger.debug(e.toString());
		}

		return result;
    }
}
