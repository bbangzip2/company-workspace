package com.visangesl.treeapi.badge.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

/**
 * 사용자가 수여한 뱃지 정보를 담는 VO
 * 
 * @author imac
 *
 */
public class TreasureBadgeInfoVo extends VSObject {
	private String badgeType;
	private int badgeSeq;
	private String badgeName;
	private String badgeMean;
	private List<BestowalMemberVo> bestowalMbr;
	
	public String getBadgeType() {
		return badgeType;
	}
	public void setBadgeType(String badgeType) {
		this.badgeType = badgeType;
	}
	public int getBadgeSeq() {
		return badgeSeq;
	}
	public void setBadgeSeq(int badgeSeq) {
		this.badgeSeq = badgeSeq;
	}
	public String getBadgeName() {
		return badgeName;
	}
	public void setBadgeName(String badgeName) {
		this.badgeName = badgeName;
	}
	public String getBadgeMean() {
		return badgeMean;
	}
	public void setBadgeMean(String badgeMean) {
		this.badgeMean = badgeMean;
	}
	public List<BestowalMemberVo> getBestowalMbr() {
		return bestowalMbr;
	}
	public void setBestowalMbr(List<BestowalMemberVo> bestowalMbr) {
		this.bestowalMbr = bestowalMbr;
	}	
}