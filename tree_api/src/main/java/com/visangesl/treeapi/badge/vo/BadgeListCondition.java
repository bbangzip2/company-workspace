package com.visangesl.treeapi.badge.vo;

import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSListCondition;

/**
 * 특정 사용자의 뱃지 목록을 조회하기위한 조건 VO
 * @author imac
 *
 */
public class BadgeListCondition extends VSListCondition {
	/**
	 * 사용자ID
	 */
	private String mbrId;
	/**
	 * 언어 구분 코드 (CMM_CD에서 상위코드가 LA000인 코드를 참조)
	 * 기본값으로 영어를 셋팅한다.
	 */
	private String langSect = TreeProperties.getProperty("tree_lang_usa");

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public String getLangSect() {
		return langSect;
	}

	public void setLangSect(String langSect) {
		this.langSect = langSect;
	}
	
	
}
