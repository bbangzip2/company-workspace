package com.visangesl.treeapi.badge.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 뱃지를 부여한 사용자 정보를 담을 VO
 * 
 * @author imac
 *
 */
public class BestowalMemberVo extends VSObject {
	private String bgSeq; //실제 배지 SEQ 
	private String mbrId;
	private String sexSect;
	private String nm;
	private String bestowalDate;
	
	
	public String getBgSeq() {
		return bgSeq;
	}
	public void setBgSeq(String bgSeq) {
		this.bgSeq = bgSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getSexSect() {
		return sexSect;
	}
	public void setSexSect(String sexSect) {
		this.sexSect = sexSect;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	public String getBestowalDate() {
		return bestowalDate;
	}
	public void setBestowalDate(String bestowalDate) {
		this.bestowalDate = bestowalDate;
	}
	
	
}
