package com.visangesl.treeapi.badge.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 뱃지 type값을 갖는 VO
 * 
 * @author imac
 *
 */
public class BadgeTypeVo extends VSObject {
	/**
	 * 뱃지종류 (CMM_CD에서 상위코드가 BG000인 코드 참조)
	 */
	private String badgeType;

	public String getBadgeType() {
		return badgeType;
	}

	public void setBadgeType(String badgeType) {
		this.badgeType = badgeType;
	}
}
