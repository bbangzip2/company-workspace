package com.visangesl.treeapi.badge.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.badge.service.BadgeService;
import com.visangesl.treeapi.mapper.BadgeMapper;

/**
 * 뱃지와 관련된 작업을 처리하는 Service Class
 * 
 * @author imac
 *
 */
@Service
public class BadgeServiceImpl implements BadgeService {

	@Autowired
	BadgeMapper badgeMapper;

	/**
	 * 특정 사용자의 뱃지 목록을 조회
	 */
	@Override
	public List<VSObject> getBadgeList(VSCondition condition) throws Exception {
		return badgeMapper.getBadgeList(condition);
	}

	/**
	 * 특정 사용자에게 뱃지를 부여
	 */
    @Override
    public int setBadge(VSObject vsObject)
            throws Exception {
        // TODO Auto-generated method stub
        return badgeMapper.setBadge(vsObject);
    }

}
