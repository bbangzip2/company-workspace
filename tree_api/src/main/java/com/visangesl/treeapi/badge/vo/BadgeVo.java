package com.visangesl.treeapi.badge.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 뱃지를 부여하기위한 정보를 담는 VO
 * 
 * @author imac
 *
 */
public class BadgeVo extends VSObject {

    private String badgeCd;
    private String badgeType;
    private String useYn;
    private String regId;

    private String recvMbrId;
    
    private String contentSeq;
    private String contentType;


    public String getBadgeCd() {
        return badgeCd;
    }
    public void setBadgeCd(String badgeCd) {
        this.badgeCd = badgeCd;
    }
    public String getBadgeType() {
        return badgeType;
    }
    public void setBadgeType(String badgeType) {
        this.badgeType = badgeType;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getRegId() {
        return regId;
    }
    public void setRegId(String regId) {
        this.regId = regId;
    }
    public String getRecvMbrId() {
        return recvMbrId;
    }
    public void setRecvMbrId(String recvMbrId) {
        this.recvMbrId = recvMbrId;
    }
	public String getContentSeq() {
		return contentSeq;
	}
	public void setContentSeq(String contentSeq) {
		this.contentSeq = contentSeq;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
