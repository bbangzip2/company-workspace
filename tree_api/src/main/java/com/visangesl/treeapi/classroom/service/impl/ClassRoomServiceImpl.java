package com.visangesl.treeapi.classroom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.classroom.service.ClassRoomService;
import com.visangesl.treeapi.classroom.vo.ClassLessonListCondition;
import com.visangesl.treeapi.classroom.vo.ClassRoomCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassVo;
import com.visangesl.treeapi.mapper.ClassRoomMapper;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 클래스 관련 정보 처리
 *
 * @author hong
 *
 */
@Service
public class ClassRoomServiceImpl implements ClassRoomService {
    @Autowired
    ClassRoomMapper classRoomMapper;

    /**
     * 티칭룸 - 클래스 목록 조회
     */
    @Override
    public List<VSObject> getClassSimpleList(ClassRoomCondition classRoomCondition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassSimpleList(classRoomCondition);
    }

    /**
     * 해당 클래스의 구성원 단순 목록을 조회
     */
    @Override
    public List<VSObject> getClassMemberSimpleList(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassMemberSimpleList(condition);
    }

    /**
     * 해당 클래스의 강사 PC IP Address를 조회
     */
    @Override
    public VSObject getClassRelayIp(VSCondition vsCondition) throws Exception {
        return classRoomMapper.getClassRelayIp(vsCondition);
    }

    /**
     * 해당 클래스의 강사 PC의 IP Address를 저장
     */
    @Override
    public VSResult<VSObject> mergeClassRelayIp(VSObject vsObject) throws Exception {
        VSResult<VSObject> result = new VSResult<VSObject>();

        if (vsObject == null) {
            result.setCode(TreeProperties.getProperty("error.etc.code"));
            result.setMessage(TreeProperties.getProperty("error.fail.msg"));
        } else {
            int affectedRows = classRoomMapper.mergeClassRelayIp(vsObject);

            if (affectedRows > 0) {
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } else {
                result.setCode(TreeProperties.getProperty("error.etc.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            }
        }

        return result;
    }

    /**
     * 해당일의 클래스 패널 목록 조회
     * @param condition
     * @return
     * @throws Exception
     */
    @Override
    public List<VSObject> getTodayClassesList(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getTodayClassesList(condition);
    }

    /**
     * 티칭룸 / 스터디룸 목록 조회
     */
    @Override
    public List<VSObject> getClassRoomList(ClassRoomCondition classRoomCondition) throws Exception {
        return classRoomMapper.getClassRoomList(classRoomCondition);
    }

    /**
     * 클래스의 첫번째 차시 목록 조회
     */
    @Override
    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.getFirstLessonList(tcCond);
    }

    /**
     * 다음 레슨의 첫 번째 차시 조회
     */
    @Override
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.getNextLessonDayNoCd(tcCond);
    }

    /**
     * 수업 완료 처리
     */
    @Override
    public int updateProgressStat(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.updateProgressStat(tcCond);
    }

    /**
     * 다음 다음 차시 정보 등록
     */
    @Override
    public int insertAfterNextDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.insertAfterNextDayNoCd(tcCond);
    }

    /**
     *  클래스의 차시 정보 등록 처리
     */
    @Override
    public int addDataTodayClass(String clsSeq, String dayNoCd, String sortOrd) throws Exception {
    	TodayClassVo tcv = new TodayClassVo();
    	tcv.setClsSeq(clsSeq);
    	tcv.setDayNoCd(dayNoCd);
    	tcv.setSortOrd(sortOrd);
        return classRoomMapper.addDataTodayClass(tcv);

    }

    /**
     * 클래스의 오늘 진행할 차시 조회
     */
    @Override
    public String getClassTodayDayNoCd(TodayClassCondition tcCond)
            throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassTodayDayNoCd(tcCond);
    }

    /**
     * 다음 차시 코드 조회
     */
    @Override
    public String getClassNextdayDayNoCd(TodayClassCondition tcCond)
            throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassNextdayDayNoCd(tcCond);
    }

    /**
     * 카드맵의 레슨 목록 조회
     */
    @Override
    public List<VSObject> getClassLessonList(
            ClassLessonListCondition classLessonListCondition) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getClassLessonList(classLessonListCondition);
    }

    /**
     * 등록되어 있는 차시인지 카운트로 확인 
     */
    @Override
    public int getProgressCnt(TodayClassCondition tcCond) throws Exception {
        // TODO Auto-generated method stub
        return classRoomMapper.getProgressCnt(tcCond);
    }
    
    /**
     * 진행 하지않은 차시 조회 
     * @param tcCond
     * @return
     * @throws Exception
     */
    @Override
    public String getNextDayNoCd(TodayClassCondition tcCond) throws Exception{
    	return classRoomMapper.getNextDayNoCd(tcCond);
    }
    
    /**
     * 현재 진행하는 클래스 숫자 (패널에서 사용) 
     */
    @Override
    public String getTodayClassesListCnt(VSCondition condition) throws Exception{
    	return classRoomMapper.getTodayClassesListCnt(condition);
    }
    
}
