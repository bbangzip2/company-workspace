package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;
/**
 *  티칭룸 - 클래스 목록 조회 정보를 담는 Vo
 *  
 * @author hong
 *
 */
public class ClassSimpleVo extends VSObject {

    private String clsSeq;
    private String clsNm;
    

	public String getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}

	public String getClsNm() {
		return clsNm;
	}

	public void setClsNm(String clsNm) {
		this.clsNm = clsNm;
	}



}
