package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 클래스 구성원 단순 목록 조회를 위한 조건을 담을 VO
 * @author user
 *
 */
public class ClassRoomMemberSimpleList extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 회원 등급
     */
    private String mbrGrade;
    /**
     * 클래스 순번
     */
    private String clsSeq;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }

}
