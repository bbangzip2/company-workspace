package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSCondition;


/**
 * 클래스 패널 정보를 조회하기 위한 VO
 * @author user
 *
 */
public class ClassTodayList extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 회원 등급
     */
    private String mbrGrade;
    /**
     * 클래스 수업 시작 시간
     */
    private String inclsYmd;

    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getInclsYmd() {
        return inclsYmd;
    }
    public void setInclsYmd(String inclsYmd) {
        this.inclsYmd = inclsYmd;
    }

}
