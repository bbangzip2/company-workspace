package com.visangesl.treeapi.classroom.vo;

import com.visangesl.treeapi.vo.VSListCondition;
/**
 * 클래스룸의 정보를 담는 Vo
 * 
 * @author hong
 *
 */
public class ClassRoomCondition extends VSListCondition {

    private String clsSeq;
    private String clsNm;
    private String mbrId;
    private String nm;
    private String mbrGrade;
    private String sortKey;

    private String inclsYmd;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getInclsYmd() {
        return inclsYmd;
    }
    public void setInclsYmd(String inclsYmd) {
        this.inclsYmd = inclsYmd;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getSortKey() {
        return sortKey;
    }
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }


}
