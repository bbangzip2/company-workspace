package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 클래스의 차시 정보를 구하기 위한 조건정보를 담는 Vo
 * @author hong
 *
 */
public class TodayClassCondition extends VSObject {

	private String mbrId;
    private String clsSeq;
    private String dayNoCd;
    private String sortOrd;
    private String nextDayNoCd;

	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String sortOrd) {
		this.sortOrd = sortOrd;
	}
    public String getNextDayNoCd() {
        return nextDayNoCd;
    }
    public void setNextDayNoCd(String nextDayNoCd) {
        this.nextDayNoCd = nextDayNoCd;
    }


}
