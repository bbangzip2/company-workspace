package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 클래스 패널 정보를 담기기 위한 VO
 * @author user
 *
 */
public class ClassTodayListVo extends VSObject {

    /**
     * 클래스 순번
     */
    private String clsSeq;
    /**
     * 클래스 명
     */
    private String clsNm;
    /**
     * 클래스 수업 시작 시간
     */
    private String inclsStartHm;
    /**
     * 북 코드
     */
    private String bookCd;
    /**
     * 북 명
     */
    private String bookNm;
    /**
     * 레슨 코드
     */
    private String lessonCd;
    /**
     * 레슨 명
     */
    private String lessonNm;
    /**
     * 썸네일 이미지 경로
     */
    private String thmbPath;
    /**
     * 차시 코드
     */
    private String dayNoCd;
    /**
     * 차시 순번 (차시 코드와 별개)
     */
    private String dayNo;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getInclsStartHm() {
        return inclsStartHm;
    }
    public void setInclsStartHm(String inclsStartHm) {
        this.inclsStartHm = inclsStartHm;
    }
    public String getBookCd() {
        return bookCd;
    }
    public void setBookCd(String bookCd) {
        this.bookCd = bookCd;
    }
    public String getBookNm() {
        return bookNm;
    }
    public void setBookNm(String bookNm) {
        this.bookNm = bookNm;
    }
    public String getLessonCd() {
        return lessonCd;
    }
    public void setLessonCd(String lessonCd) {
        this.lessonCd = lessonCd;
    }
    public String getLessonNm() {
        return lessonNm;
    }
    public void setLessonNm(String lessonNm) {
        this.lessonNm = lessonNm;
    }
    public String getThmbPath() {
        if (thmbPath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
        } else {
            return thmbPath;
        }
    }
    public void setThmbPath(String thmbPath) {
        this.thmbPath = thmbPath;
    }
    public String getDayNoCd() {
        return dayNoCd;
    }
    public void setDayNoCd(String dayNoCd) {
        this.dayNoCd = dayNoCd;
    }
    public String getDayNo() {
        return dayNo;
    }
    public void setDayNo(String dayNo) {
        this.dayNo = dayNo;
    }

}
