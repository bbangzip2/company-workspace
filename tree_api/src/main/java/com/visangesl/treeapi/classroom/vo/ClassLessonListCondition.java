package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;
/**
 * 클래스의 레슨 정보를 담는 Vo
 * @author hong
 *
 */
public class ClassLessonListCondition extends VSObject {

    private String clsSeq;


    public String getClsSeq() {
        return clsSeq;
    }

    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }



}
