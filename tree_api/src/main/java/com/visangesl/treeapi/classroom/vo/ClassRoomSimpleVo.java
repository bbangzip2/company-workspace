package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;

/**
 *  티칭룸 - 클래스의 회원 정보를 담는 Vo
 * 
 * @author hong
 *
 */
public class ClassRoomSimpleVo extends VSObject {

	private String mbrId;
	private String nm;
    private String clsSeq;
    private String clsNm;
    private String profilePhotoPath;
    

    public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public String getNm() {
		return nm;
	}

	public void setNm(String nm) {
		this.nm = nm;
	}

	public String getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}

	public String getClsNm() {
		return clsNm;
	}

	public void setClsNm(String clsNm) {
		this.clsNm = clsNm;
	}

	public String getProfilePhotoPath() {
        if (profilePhotoPath != null) {
            return Tree_Constant.SITE_FULL_URL + profilePhotoPath;
        } else {
            return profilePhotoPath;
        }		
	}

	public void setProfilePhotoPath(String profilePhotoPath) {
		this.profilePhotoPath = profilePhotoPath;
	}




}
