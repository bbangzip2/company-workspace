package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 클래스별 수업 PC IP 정보를 담는 VO
 * 
 * @author imac
 *
 */
public class ClassRelayIpVo extends VSObject {
	/**
	 * 클래스 순번
	 */
    private String clsSeq;
    /**
     * 강사 PC IP Address
     */
    private String ip;
    /**
     * 강사 PC 접속 가능 상태
     */
    private String connStat;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getConnStat() {
        return connStat;
    }
    public void setConnStat(String connStat) {
        this.connStat = connStat;
    }
}
