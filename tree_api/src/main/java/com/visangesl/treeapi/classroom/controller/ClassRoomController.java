package com.visangesl.treeapi.classroom.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.classroom.service.ClassRoomService;
import com.visangesl.treeapi.classroom.vo.ClassLessonListCondition;
import com.visangesl.treeapi.classroom.vo.ClassRelayIpCondition;
import com.visangesl.treeapi.classroom.vo.ClassRelayIpVo;
import com.visangesl.treeapi.classroom.vo.ClassRoomCondition;
import com.visangesl.treeapi.classroom.vo.ClassRoomMemberSimpleList;
import com.visangesl.treeapi.classroom.vo.ClassTodayList;
import com.visangesl.treeapi.classroom.vo.TodayClassCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassVo;
import com.visangesl.treeapi.contents.service.ContentsService;
import com.visangesl.treeapi.contents.vo.ContentCardMapList;
import com.visangesl.treeapi.contents.vo.ContentCardMapVo;
import com.visangesl.treeapi.contents.vo.ContentCardSortList;
import com.visangesl.treeapi.contents.vo.ContentProdSeq;
import com.visangesl.treeapi.contents.vo.ContentVo;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.member.service.MemberService;
import com.visangesl.treeapi.member.vo.MemberInfo;
import com.visangesl.treeapi.member.vo.MemberVo;
import com.visangesl.treeapi.message.service.MessageService;
import com.visangesl.treeapi.message.vo.MessageMbrId;
import com.visangesl.treeapi.packaging.service.PackageService;
import com.visangesl.treeapi.portfolio.service.PortfolioService;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 클래스 관련 정보 처리하는 Controller
 *
 * @author hong
 *
 */
@Controller
public class ClassRoomController {
    @Autowired
    private ClassRoomService classRoomService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ContentsService contentsService;
    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private PackageService packageService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 티칭룸 - 클래스 목록 조회
	 *
	 * @param request
	 * @param response
	 * @param mbrId
	 * @return
	 * @throws Exception
	 */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassSimpleList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getClassSimpleList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId) throws Exception {

        logger.debug("getClassRoomList");
        logger.debug(mbrId);

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

//         TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
    	String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

    	logger.debug(">>>>> mbrGrade : ==== "+mbrGrade);

        try {
            ClassRoomCondition classRoomCondition = new ClassRoomCondition();
            classRoomCondition.setMbrId(mbrId);
            classRoomCondition.setMbrGrade(mbrGrade);

            List<VSObject> classRoomList = classRoomService.getClassSimpleList(classRoomCondition);

            result.setResult(classRoomList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 해당 클래스의 구성원 단순 목록을 조회
     * @param request
     * @param response
     * @param mbrId 회원 ID
     * @param clsSeq 클래스 코드
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassMemberSimpleList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getClassMemberSimpleList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq
            ) throws Exception {

    	VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        // 로그인 정보 조회
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

        // 강사/학생 등급 조회
        String teacherGrade = TreeProperties.getProperty("tree_teacher");
        String studentGrade = TreeProperties.getProperty("tree_student");


        // 로그인 계정의 등급별로 조회되는 대상의 등급을 변경해 지정한다. (강사는 학생, 학생은 강사 정보를 조회하기 위해서 수정)
        if (mbrGrade.equals(teacherGrade)) {
            mbrGrade = studentGrade;
        } else {
            mbrGrade = teacherGrade;
        }

        try {
            ClassRoomMemberSimpleList condition = new ClassRoomMemberSimpleList();
            condition.setMbrId(mbrId);
            condition.setClsSeq(clsSeq);
            condition.setMbrGrade(mbrGrade);

            List<VSObject> classRoomList = classRoomService.getClassMemberSimpleList(condition);
            result.setResult(classRoomList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 런처 메인화면을 위한 모든 정보를 조회
     *      1. 클래스 패널 정보
     *      2. 회원 정보
     *      3. 미열람 메시지 카운트
     *      4. materials 카운트 - 추후 개발 예정
     *      5. 미확인 홈워크 카운트 - 추후 개발 예정
     * @param request
     * @param response
     * @param inclsYmd 오늘 날짜
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/launcher/mainInfo", method=RequestMethod.GET)
    public VSResult<Object> getLauncherMainInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "todayYmd", required = false, defaultValue = "") String inclsYmd
            ) throws Exception {

        logger.debug("/treeMain");
        logger.debug("inclsYmd: " + inclsYmd);

        VSResult<Object> result = new VSResult<Object>();
        ClassTodayList classTodayListCondition = new ClassTodayList();

        try {

            // 필수 파라미터 체크
            if (!inclsYmd.equals("")) {

                // 로그인 회원 정보 조회
                TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
                String mbrId = userDetail.getUsername();
                String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

                String teacherGrade = TreeProperties.getProperty("tree_teacher");
                String studentGrade = TreeProperties.getProperty("tree_student");
                String materialsCount = "0";
                String homeworkCount = "0";


                // 해당일의 클래스 패널 목록 조회
                classTodayListCondition.setMbrId(mbrId);
                classTodayListCondition.setMbrGrade(mbrGrade);
                classTodayListCondition.setInclsYmd(inclsYmd);
                List<VSObject> classRoomList = classRoomService.getTodayClassesList(classTodayListCondition);

                String classCnt = classRoomService.getTodayClassesListCnt(classTodayListCondition); //   진행할 클래스 숫자.

                // 회원 정보
                MemberInfo memberInfo = new MemberInfo();
                MemberVo memberVo = new MemberVo();

                memberInfo.setMbrId(mbrId);
                memberVo = (MemberVo)memberService.getMemberInfo(memberInfo);


                // 미열람 메시지 카운트
                MessageMbrId messageMbrIdCondition = new MessageMbrId();
                messageMbrIdCondition.setMbrId(mbrId);
                int messageCount = messageService.getNewMessageCount(messageMbrIdCondition);


                // ========================================================
                // 회원 등급별 데이터 조회
                // 관련 기능 추후 개발 예정 상태로 지금은 그냥 '0'으로 내려 줍니다.
                if (mbrGrade.equals(teacherGrade)) {        // 강사
                    // 리소스 센터 materials 카운트
                    materialsCount = "0";
                } else if (mbrGrade.equals(studentGrade)) { // 학생
                    // 미확인 홈워크 카운트
                    homeworkCount = "0";
                }


                
                
                // 결과 값 입력
                HashMap<String, Object> pMap = new HashMap<String, Object>();
                pMap.put("classes", classRoomList);         // 클래스 패널 정보
                pMap.put("member", memberVo);               // 회원 정보
                pMap.put("message", messageCount);          // 미열람 메시지 카운트
                pMap.put("classCnt", classCnt);          // 클래스 수 (lee hong added. 7/15)


                if (mbrGrade.equals(teacherGrade)) {
                    pMap.put("materials", materialsCount);  // 리소스 센터 materials 카운트
                } else {
                    pMap.put("homework", homeworkCount);    // 미확인 홈워크 카운트
                }


                result.setResult(pMap);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

            } else {
                result.setCode(TreeProperties.getProperty("error.parameternotfound.code"));
                result.setMessage(TreeProperties.getProperty("error.parameternotfound.msg"));
            }

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 카드맵 해당 Lesson의 Day 데이터 리스트 조회
     * @param request
     * @param response
     * @param lessonCd 레슨 코드
     * @return 차시 리스트
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/dayList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getLessonDayList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {

            if (!lessonCd.equals("")) {
                ContentProdSeq condition = new ContentProdSeq();
                condition.setProdSeq(lessonCd);

                result.setResult(contentsService.getDayDataList(condition));
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } else {
                // lessonCd 가 없는 경우
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            }

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }

    /**
     * 카드맵 카드 리스트 조회
     *      1. 강사 일 경우 IP 정보를 등록한다.
     *      2. 카드맵 리스트 정보를 조회한다.
     *          해당 레슨의 강사가 편집한 카드맵이 있으면 우선 조회, 없으면 기본 카드맵을 조회
     * @param request
     * @param response
     * @param mbrId 회원 ID - 강사
     * @param lessonCd 레슨 코드
     * @param dayNoCd 차시 코드
     * @param clsSeq 클래스 코드
     * @param classLocalIp 강사 로컬 IP
     * @return 카드맵 리스트
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/cardMapList", method=RequestMethod.GET)
    public VSResult<Object> getLessonCardMapList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNoCd,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "classLocalIp", required = false, defaultValue = "") String classLocalIp
            ) throws Exception {

        VSResult<Object> result = new VSResult<Object>();

        try {

            String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

            // 강사인 경우 강사의 local IP를 서버에 저장한다.
            if (mbrGrade != null && mbrGrade.equals(TreeProperties.getProperty("tree_teacher"))) {
                ClassRelayIpVo classRelayIp = new ClassRelayIpVo();
                if (!classLocalIp.equals("")) {
                    classRelayIp.setConnStat("C");
                    classRelayIp.setIp(classLocalIp);
                    classRelayIp.setClsSeq(clsSeq);
                    VSResult<VSObject> classRelayIpResult = new VSResult<VSObject>();
                    classRelayIpResult = classRoomService.mergeClassRelayIp(classRelayIp);
                    logger.debug("><><><><><><>< (카드맵) IP 저장 결과 = "+ classRelayIpResult.getCode() + " | " + classRelayIpResult.getMessage());
                }
            }


            // 카드맵 리스트 조회 - 강사가 편집한 카드맵
            ContentCardMapList contentCardMapList = new ContentCardMapList();
            contentCardMapList.setMbrId(mbrId);
            contentCardMapList.setLessonCd(lessonCd);
            contentCardMapList.setDayNoCd(dayNoCd);
            List<ContentCardMapVo> cardMapList = contentsService.getCardMapDataList(contentCardMapList);


            // 카드맵 리스트 조회 - 강사가 편집한 카드맵이 없을 경우 기본 카드맵 정보를 다시 조회한다.
            if (cardMapList == null || cardMapList.size() == 0) {
                contentCardMapList.setMbrId("");
                cardMapList = contentsService.getCardMapDataList(contentCardMapList);
            }

            result.setResult(cardMapList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }

    /**
     * 카드맵 서브 카드 리스트 조회
     *      해당 레슨의 강사가 편집한 카드맵이 있으면 우선 조회, 없으면 기본 카드맵을 조회
     * @param request
     * @param response
     * @param mbrId 강사 ID
     * @param lessonCd 레슨 코드
     * @return 카드맵 리스트 (서브)
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/cardMapSubList", method=RequestMethod.GET)
    public VSResult<List<ContentCardMapVo>> getLessonCardMapSubList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd
            ) throws Exception {

    	VSResult<List<ContentCardMapVo>> result = new VSResult<List<ContentCardMapVo>>();

        try {

            // 카드맵 리스트 조회 - 강사가 편집한 카드맵
            ContentCardMapList contentCardMapList = new ContentCardMapList();
            contentCardMapList.setMbrId(mbrId);
            contentCardMapList.setLessonCd(lessonCd);
            contentCardMapList.setDayNoCd("");
            List<ContentCardMapVo> cardMapList = contentsService.getCardMapDataList(contentCardMapList);


            // 카드맵 리스트 조회 - 강사가 편집한 카드맵이 없을 경우 기본 카드맵 정보를 다시 조회한다.
            if (cardMapList == null || cardMapList.size() == 0) {
                contentCardMapList.setMbrId("");
                cardMapList = contentsService.getCardMapDataList(contentCardMapList);
            }

            result.setResult(cardMapList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }

    /**
     * 티칭룸 / 스터디룸 목록 조회
     *
     * @param request
     * @param response
     * @param sortKey
     * @param pageNo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassroomList", method=RequestMethod.GET)
    public VSResult<Object> getClassroomList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "sortKey", required = false, defaultValue = "") String sortKey,
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo
            ) throws Exception {

        VSResult<Object> result = new VSResult<Object>();

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();

        try {

            String mbrId = userDetail.getUsername();

            ClassRoomCondition classRoomCondition = new ClassRoomCondition();
            //classRoomCondition.setMbrGrade(mbrGrade);
            classRoomCondition.setMbrId(mbrId);
            classRoomCondition.setSortKey(sortKey);
            classRoomCondition.setPageNo(pageNo);
            classRoomCondition.setPageSize(6);

            List<VSObject> classRoomList = classRoomService.getClassRoomList(classRoomCondition); // 티칭/스터디룸 공용
            List<VSObject> homeworkList = new ArrayList<VSObject>();

            // result
            HashMap<String, Object> pMap = new HashMap<String, Object>();
            pMap.put("classroom", classRoomList);
            pMap.put("homework", homeworkList);

            result.setResult(pMap);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }
        return result;

    }

    /**
     * 해당 카드의 상세 정보 조회
     * @param request
     * @param response
     * @param prodSeq - 카드 상품 코드
     * @param clsSeq - 클래스 코드
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/cardInfo", method=RequestMethod.GET)
    public VSResult<VSObject> getCardInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "prodSeq", required = false, defaultValue = "") String prodSeq
            ,@RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq) throws Exception {

        VSResult<VSObject> result = new VSResult<VSObject>();

        if (!prodSeq.equals("")) {

            try {
                ContentProdSeq condition = new ContentProdSeq();
                condition.setProdSeq(prodSeq);

                ContentVo contentVo = (ContentVo) contentsService.getContentsInfoData(condition);

                if (!clsSeq.equals("")) {
                    String cardPath = File.pathSeparator + clsSeq + contentVo.getCardPath();
                    contentVo.setCardPath(cardPath);
                }

                result.setResult(contentVo);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

            } catch (Exception e) {
                result.setCode(TreeProperties.getProperty("error.etc.code"));
                result.setMessage(TreeProperties.getProperty("error.etc.msg"));
            }

        } else {
            result.setCode(TreeProperties.getProperty("error.parameternotfound.code"));
            result.setMessage(TreeProperties.getProperty("error.parameternotfound.msg"));
        }

        return result;

    }

    /**
     * 카드맵 정보 저장
     *      1.  카드맵 정보 저장
     *      2.  패키지 파일 생성
     * @param request
     * @param response
     * @param dayNoCd 차시 코드
     * @param sortData 카드맵 정렬 데이터 (카드 코드를 정렬 순서대로 ','를 구분자로 나열) 99,100,1,2,3
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/cardSort", method=RequestMethod.POST)
    public VSResult<VSObject> modifyCardMapSortData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq,
            @RequestParam(value = "dayCd", required = false, defaultValue = "") String dayNoCd,
            @RequestParam(value = "sortData", required = false, defaultValue = "") String sortData) throws Exception {

        logger.debug("/classroom/cardSort");

        VSResult<VSObject> result = new VSResult<VSObject>();
        boolean newPackageFlag = false; // 패키징 진행 여부 구분자
        boolean sortModifyFlag = false; // 카드맵 내 카드 정렬 순서 변경 구분자

        // 로그인 정보 조회
        String mbrId = TreeSpringSecurityUtils.getPrincipalAuthorities().getUsername();
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();


        // 필수 파라미터, 계정 권한 체크
        if (mbrGrade.equals(TreeProperties.getProperty("tree_teacher")) && !mbrId.equals("") && !clsSeq.equals("") && !dayNoCd.equals("") && !sortData.equals("") && sortData.length() > 0) {


            // 기존 카드맵 리스트 조회
            ContentCardMapList cardMapListCondition = new ContentCardMapList();
            cardMapListCondition.setMbrId(mbrId);
            cardMapListCondition.setDayNoCd(dayNoCd);

            List<ContentCardMapVo> cardMapOldList = new ArrayList<ContentCardMapVo>();
            cardMapOldList = contentsService.getCardMapDataList(cardMapListCondition);


            // 기존 카드맵 리스트 데이터가 없을 경우 관리자가 등록한 마스터 데이터를 조회
            if (cardMapOldList == null || cardMapOldList.size() == 0) {
                cardMapListCondition.setMbrId("");
                cardMapOldList = contentsService.getCardMapDataList(cardMapListCondition);
            }


            // 기존 카드맵 정보 유무 체크
            if (cardMapOldList != null && cardMapOldList.size() > 0) {

                // 패키징 진행 여부 체크 진행
                // 기존 카드맵, 신규 카드맵 요소 비교
                String[] tempOldArr = new String[cardMapOldList.size()];
                String[] tempNewArr = sortData.split(",");

                int i = 0;
                for (ContentCardMapVo vo : cardMapOldList) {
                    tempOldArr[i] = vo.getProdSeq();
                    i++;
                }

                if (!Arrays.equals(tempOldArr, tempNewArr)) {
                    Arrays.sort(tempOldArr);
                    Arrays.sort(tempNewArr);

                    if (!Arrays.equals(tempOldArr, tempNewArr)) {
                        // 신규 패키징 진행
                        newPackageFlag = true;
                    } else {
                        // 기존과 순서만 다른 상태 - 카드맵 정렬 순서면 변경 진행한다.
                        sortModifyFlag = true;
                        newPackageFlag = true;
                    }
                } else {
                    // 기존과 모두 같은 상태
                    logger.debug("카드맵의 변경 사항 없음");
                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));
                }

            } else {
                // 신규 패키징 진행
                newPackageFlag = true;
            }


            // 패키징 처리 호출
            if (newPackageFlag) {

                ContentCardSortList contentCardSortList = new ContentCardSortList();
                contentCardSortList.setClsSeq(clsSeq);
                contentCardSortList.setDayNoCd(dayNoCd);
                contentCardSortList.setCardSort(sortData);
                contentCardSortList.setMbrId(mbrId);
                contentCardSortList.setSortFlag(sortModifyFlag);

                contentsService.createPackage(contentCardSortList);

                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

            }

        } else {
            result.setCode(TreeProperties.getProperty("error.parameternotfound.code"));
            result.setMessage(TreeProperties.getProperty("error.parameternotfound.msg"));
        }

        return result;

    }

    /**
     * 서버의 locap IP 조회 처리
     *
     * @param request
     * @param response
     * @param classSeq : 클래스 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassRelayIp", method=RequestMethod.GET)
    public VSResult<VSObject> getClassRelayIp(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "classSeq", required = true) String classSeq
            ) throws Exception {

        VSResult<VSObject> result = new VSResult<VSObject>();

        try {
        	String clsSeq = classSeq;

            // 현재 class의 local IP를 결과값에 포함한다.
            ClassRelayIpCondition classRelayIpCondition = new ClassRelayIpCondition();
            classRelayIpCondition.setClsSeq(clsSeq);
            classRelayIpCondition.setConnStat("C");
            ClassRelayIpVo classRelayIpInfo = (ClassRelayIpVo) classRoomService.getClassRelayIp(classRelayIpCondition);

            if (classRelayIpInfo == null) {
                result.setCode(TreeProperties.getProperty("error.etc.code"));
                result.setMessage(TreeProperties.getProperty("error.etc.msg"));
                result.setResult(null);
            } else {
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
    			result.setResult(classRelayIpInfo);
            }

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 클래스에 등록된 프로그램에 속한 책의 첫번째와 두번째 차시를 등록한다.
     *
     * @param request
     * @param response
     * @param clsSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/addDataTodayClass", method=RequestMethod.POST)
    public VSResult<VSObject> addDataTodayClass(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq) throws Exception {

        logger.debug("addDataTodayClass");

        VSResult<VSObject> result = new VSResult<VSObject>();

        try {


        	// 클래스에 등록된 책의 첫 레슨-첫차시 코드목록 조회
    		TodayClassCondition tcCond = new TodayClassCondition();

    		tcCond.setClsSeq(clsSeq);

        	List<TodayClassVo> lessonList = classRoomService.getFirstLessonList(tcCond);

        	// 책이 여러개인 경우 첫 레슨/첫차시는 여러건이 될수 있다.
        	for(int i =0; i < lessonList.size(); i ++){

        		// 레슨의 첫번째 차시 코드.
        		String dayNoCd = lessonList.get(i).getDayNoCd();

        		logger.debug("첫 레슨 첫 차시 코드는 ==="+dayNoCd);

        		// TODAY_CLASS insert
        		classRoomService.addDataTodayClass(clsSeq, dayNoCd, "1");

        		TodayClassCondition nextCond = new TodayClassCondition();

        		nextCond.setClsSeq(clsSeq);
        		nextCond.setDayNoCd(dayNoCd);


        		// 다음 레슨의 첫번째 차시 코드 조회
        		String nextDayNoCd = classRoomService.getNextLessonDayNoCd(nextCond);
        		logger.debug("다음 레슨 첫 차시 코드는  ==="+nextDayNoCd);

        		if(!nextDayNoCd.equals("")){
        			// TODAY_CLASS insert
            		classRoomService.addDataTodayClass(clsSeq, nextDayNoCd, "2");
        		}


        	}

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 클래스 종료 처리 (클래스의 차시 코드 정보를 업데이트)
     * @param request
     * @param response
     * @param clsSeq
     * @param dayNo : 완료 시킬 차시 순번 
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/modifyClassNextSchedule", method=RequestMethod.POST)
    public VSResult<VSObject> modifyClassNextSchedule(HttpServletRequest request, HttpServletResponse response
            ,@RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq
            ,@RequestParam(value = "dayNo", required = false, defaultValue = "") String dayNo) throws Exception {

        logger.debug("modifyClassNextSchedule");

        VSResult<VSObject> result = new VSResult<VSObject>();
        TodayClassCondition tcCond = new TodayClassCondition();

        try {

            if (!clsSeq.equals("")) {

                // 현재 등록된 차시 코드 조회
                tcCond.setClsSeq(clsSeq);

                // 현재 클래스에서 진행 되지 않은 첫번째 차시를 가져온다.
                // String dayNoCd = classRoomService.getClassTodayDayNoCd(tcCond);
                
                String afterNextDayNoCd = ""; // DB에 저장한 현재의 다음 다음 차시 순번

                if (dayNo != null && !dayNo.equals("")) {
                	logger.debug(" 넘어온 차시 순번은? dayNo = "+dayNo);
                	tcCond.setDayNoCd(dayNo);
                	// 현재 차시를 진행 완료 상태로 처리함.
                	classRoomService.updateProgressStat(tcCond);

                    // 조회해온 현재 차시코드로 다음 차시 코드 조회
                    String nextDayNoCd = classRoomService.getClassNextdayDayNoCd(tcCond);


            		
            		

                    // 다음 차시가 있다면 그놈을 가지고, 다음 다음 차시를 CLS_INCLS_PROGRESS TABLE에 저장해 준다. (이유는 기본적으로 CLS_INCLS_PROGRESS 테이블에는 현재 차시와 다음 차시가 INSERT 되어있어야 하기 때문에 그렇다. )
                    if (nextDayNoCd != null && !nextDayNoCd.equals("")) {
                    	
                    	logger.debug("다음 차시 코드가 있네요. nextDayNoCd = "+nextDayNoCd);
                    	// 다음 차시 인서트 
                    	
                    	
                        // 다음 차시가 등록 되어 있는지 확인. 
                        TodayClassCondition cntCond1 = new TodayClassCondition();
                        cntCond1.setClsSeq(clsSeq); //클래스 순번
                        cntCond1.setDayNoCd(nextDayNoCd); // 차시번호
                      
              		    int alreayCnt = classRoomService.getProgressCnt(cntCond1);
              		    
              		    if( alreayCnt <= 0 ){ //등록되지 않은 경우에만 등록함. 
              		    	
                            TodayClassCondition insCond1 = new TodayClassCondition();
                            insCond1.setClsSeq(clsSeq); //클래스 순번
                            insCond1.setDayNoCd(nextDayNoCd); // 차시번호
                        	classRoomService.insertAfterNextDayNoCd(insCond1);
                        	
              		    }
                    	
                    	
//                        TodayClassCondition temp1 = new TodayClassCondition();
//                        temp1.setClsSeq(clsSeq);
//                        temp1.setDayNoCd(nextDayNoCd);
//                        // 다음 다음 차시 코드를 조회한다.
//                        afterNextDayNoCd = classRoomService.getClassNextdayDayNoCd(temp1);


                    } else { // 다음차시가 없다면, 다음 레슨의 1차시를 구해서 그게 CLS_INCLS_PROGRESS에 있는지 확인후 ,
                    	
                    	
                    	// 진행 하지 않은 차시 코드 조회 
                    	String nStatDayno = classRoomService.getNextDayNoCd(tcCond);
                    	
                    	
                    	logger.debug("다음 차시 코드가 없어요. 다음 레슨의 1차시를 조회해봐요. 진행하지 않은 차시(nStatDayno)="+nStatDayno);

          		    	
                        TodayClassCondition nCond = new TodayClassCondition();
                        nCond.setClsSeq(clsSeq); //클래스 순번
                        nCond.setDayNoCd(nStatDayno); // 차시번호
                    	
                    	// 진행하지 않은 차시의 다음 레슨 차시를 조회한다. 
                    	String nextLessonDayNoCd = classRoomService.getNextLessonDayNoCd(nCond);

                    	
                    	if(nextLessonDayNoCd != null && !nextLessonDayNoCd.equals("")){
                    		
                    		logger.debug("다음레슨의 1차시 코드가 있네요. nextDayNoCd="+nextDayNoCd);
                    		
                    		// 다음 레슨의 1차시가 등록 되어 있나요? 
                            TodayClassCondition cntCond = new TodayClassCondition();
                            cntCond.setClsSeq(clsSeq); //클래스 순번
                            cntCond.setDayNoCd(nextLessonDayNoCd); // 차시번호
                            
                    		int alreayCnt = classRoomService.getProgressCnt(cntCond);
                    		
                    		if(alreayCnt <= 0){
                    			// 등록되어 있지 않다면, 등록처리   
                    			
                                TodayClassCondition insCond3 = new TodayClassCondition();
                                insCond3.setClsSeq(clsSeq); //클래스 순번
                                insCond3.setDayNoCd(nextLessonDayNoCd); // 차시번호

                            	classRoomService.insertAfterNextDayNoCd(insCond3);
                            	
                    		}

                    	}else{
                        	// 다음 레슨의 차시도 없다면, 그냥 끝.
                    	}

                    }


                    result.setCode(TreeProperties.getProperty("error.success.code"));
                    result.setMessage(TreeProperties.getProperty("error.success.msg"));

                } else {
                    // CLS_INCLS_PROGRESS TABLE 에 dayNoCd 없는 경우
                    result.setCode(TreeProperties.getProperty("error.fail.code"));
                    result.setMessage(TreeProperties.getProperty("error.fail.msg"));
                }
            } else {
                // clsSeq가 없는 경우
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 카드맵의 레슨 목록 조회
     * @param request
     * @param response
     * @param mbrId : 회원 아이디
     * @param clsSeq : 클래스 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/classroom/getClassLessonList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getClassLessonList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq
            ) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {

            if (!clsSeq.equals("")) {
                ClassLessonListCondition classLessonListCondition = new ClassLessonListCondition();
                classLessonListCondition.setClsSeq(clsSeq);

                List<VSObject> classLessonList = classRoomService.getClassLessonList(classLessonListCondition);
                result.setResult(classLessonList);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } else {
                // clsSeq가 없는 경우
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage(TreeProperties.getProperty("error.fail.msg"));
            }
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }
}
