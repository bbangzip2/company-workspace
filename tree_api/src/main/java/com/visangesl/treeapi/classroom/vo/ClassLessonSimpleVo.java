package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 클래스 의 단순 정보 용 Vo
 * 
 * @author hong
 *
 */
public class ClassLessonSimpleVo extends VSObject {

	private String lessonCd;
	private String lessonNm;
    private String dayNoCd;
    private String clsSeq;
    private String clsNm;
    
	public String getLessonCd() {
		return lessonCd;
	}
	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getClsNm() {
		return clsNm;
	}
	public void setClsNm(String clsNm) {
		this.clsNm = clsNm;
	}

}
