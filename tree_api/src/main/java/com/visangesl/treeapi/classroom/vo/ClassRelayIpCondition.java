package com.visangesl.treeapi.classroom.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 특정 클래스의 강사 PC IP Address를 조회하기 위한 조건정보를 담는 VO
 * 
 * @author imac
 *
 */
public class ClassRelayIpCondition extends VSCondition {
	/**
	 * 클래스 순번
	 */
    private String clsSeq;
    /**
     * 접속가능 상태
     */
    private String connStat;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getConnStat() {
        return connStat;
    }
    public void setConnStat(String connStat) {
        this.connStat = connStat;
    }
}
