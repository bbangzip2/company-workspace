package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.bookshelf.vo.BookshelfCondition;

/**
 * BookShelf 관련작업을 처리하는 Service Class
 * 
 * @author changs
 *
 */

public interface BookshelfMapper {

	/**
	 * 북 리스트 출력 
	 * @param vsObject
	 * @return
	 */
	public List<VSObject> getBookList(VSObject vsObject);
	
	/**
	 * 북 레슨 리스트 출력
	 * @param vsObject
	 * @return
	 */
	public List<VSObject> getLessonList(VSObject vsObject);
	
	/**
	 * 북 레슨 검색 리스트 춮력
	 * @param vsObject
	 * @return
	 */
	public List<VSObject> getSearchLessonList(VSObject vsObject);
	
	/**
	 * 북 레슨 정보 출력 
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public VSObject getLessonInfo(BookshelfCondition condition) throws Exception;

	/**
	 * 북 레슨 정보 출력 
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getBookshelfClassList(BookshelfCondition condition) throws Exception;
	

}
