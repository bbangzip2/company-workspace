package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.upload.vo.AttachFileListConditionVo;
import com.visangesl.treeapi.upload.vo.AttachFileVo;
import com.visangesl.treeapi.upload.vo.NoteInfoVo;

/**
 * 첨부파일 관련 mapper interface
 * 
 * @author hong
 *
 */
public interface AttachFileMapper {

	/**
	 * 노트 정보 저장
	 *  
	 * @param noteInfoVo
	 * @return
	 * @throws Exception
	 */
	public int cNoteData(NoteInfoVo noteInfoVo) throws Exception;
	
	/**
	 * 첨부 파일 저장
	 *  
	 * @param attachFileVo
	 * @return
	 * @throws Exception
	 */
	public int cTreeAttachFile(AttachFileVo attachFileVo) throws Exception;

	/**
	 * 첨부 파일 삭제
	 *  
	 * @param attachFileVo
	 * @return
	 * @throws Exception
	 */
    public int dTreeAttachFile(AttachFileVo attachFileVo) throws Exception;

	/** 
	 * 첨부 파일 목록 조회
	 *  
	 * @param afListCond
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getAttachFileList(AttachFileListConditionVo afListCond) throws Exception;

	/**
	 * 노트 첨부파일 목록 조회
	 *  
	 * @param afListCond
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getNoteAttachFileList(AttachFileListConditionVo afListCond) throws Exception;


}
