package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSObject;

/**
 * 노트정보를 조회하기 위한 mapper interface
 * 
 * @author hong
 *
 */
public interface NoteMapper {

	/**
	 * 노트 상세정보 조회 
	 * 
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
	public VSObject getContentsInfoData(VSObject vsObject) throws Exception;
	
	/**
	 * 노트 정보 업데이트 처리
	 * 
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
	public int uNoteData(VSObject vsObject) throws Exception;

	/**
	 * 노트 정보 있는지 확인 
	 * 
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
	public int getNoteCount(VSObject vsObject) throws Exception;

	/**
	 * 노트 키값 가져오기
	 * 
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
	public String getNoteSeq(VSObject vsObject) throws Exception;
	
	/**
	 * 노트가 등록된 레슨 목록 조회
	 *  
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
    public List<VSObject> getMyNoteLessonList(VSObject vsObject) throws Exception;
    
    /**
     * 레슨별 노트 목록 조회 
     *  
     * @param vsObject
     * @return
     * @throws Exception
     */
    public List<VSObject> getMyNoteList(VSObject vsObject) throws Exception;


	/**
	 * 노트 상세 정보 조회 
	 * 
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
    public List<VSObject> getMyNoteInfo(VSObject vsObject) throws Exception;
    
}
