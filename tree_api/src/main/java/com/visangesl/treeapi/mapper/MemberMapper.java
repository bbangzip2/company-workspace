package com.visangesl.treeapi.mapper;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

/**
 * 사용자 정보처리 관련 mapper interface
 *
 * @author hong
 *
 */
public interface MemberMapper {
	/*
 	 * 로그인 후 가까운 수업을 현재 기준에서 알 방법이 없어 클래스 순번을 가져올 수 없다.
         그렇기 때문에 강사의 local IP를 저장할 기준이 모호하다.
         강사의 local IP를 저장하는 이유는 수업시 node.js 에 연결되기 위함이니
         클래스 순번을 명확하게 정할 수 있는 순간에서 local IP저장을 수행하는 것이 맞는듯하다.

		 관련하여 개발팀 정리 후 구현 예정임.
	 */
	/*
    public Member getNearClassSeq(Member member) throws Exception;
    */

    /**
     * 사용자 기본정보 조회
     *
     * @param member
     * @return
     * @throws Exception
     */
    public VSObject getMemberInfo(VSCondition condition) throws Exception;
}
