package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

public interface CommentMapper {
    /**
     * 특정 콘텐츠의 코멘트를 등록
     * @param vsObject
     * @return
     */
	public int addComment(VSObject vsObject);
	/**
	 * 특정 콘텐츠의 코멘트를 삭제
	 * @param condition
	 * @return
	 */
	public int removeComment(VSCondition condition);
	/**
	 * 특정 콘텐츠 속해 있는 코멘트 리스트 조회
	 * @param condition
	 * @return
	 */
	public List<VSObject> getContentComment(VSCondition condition);
	/**
	 * 특정 콘텐츠의 좋아요 유무 조회
	 * @param condition
	 * @return
	 */
	public String existDoLike(VSCondition condition);
	/**
	 * 좋아요 등록
	 * @param vsObject
	 * @return
	 */
	public int doLike(VSObject vsObject);
	/**
	 * 좋아요 삭제
	 * @param condition
	 * @return
	 */
	public int undoLike(VSCondition condition);
}
