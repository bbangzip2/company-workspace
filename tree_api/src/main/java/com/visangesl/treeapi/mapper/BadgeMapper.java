package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

/**
 * 뱃지와 관련된 작업을 처리하는 Mapper
 * 
 * @author imac
 *
 */
public interface BadgeMapper {
	/**
	 * 특정 사용자의 뱃지 목록을 조회
	 * 
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getBadgeList(VSCondition condition) throws Exception;

	/**
	 * 특정 사용자에게 뱃지를 부여
	 * 
	 * @param vsObject
	 * @return
	 */
    public int setBadge(VSObject vsObject);
}
