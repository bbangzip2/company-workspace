package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.contents.vo.ContentCardMapVo;

/**
 * 컨텐츠 관련 처리 하는 mapper interface
 *
 * @author hong
 *
 */
public interface ContentsMapper {

	/**
	 * 해당 카드의 상세 정보 조회
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public VSObject getContentsInfoData(VSCondition condition) throws Exception;

	/**
	 * 엑티비티별 레퍼런스 컨텐츠 목록 조회
	 *
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getReferenceList(VSCondition condition) throws Exception;

	/**
	 * 해당 Lesson의 Day 데이터 리스트 조회
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getDayDataList(VSCondition condition) throws Exception;

	/**
	 * 해당 차시의 카드맵 리스트 조회
	 * @param vsObject
	 * @return
	 * @throws Exception
	 */
	public List<ContentCardMapVo> getCardMapDataList(VSCondition condition) throws Exception;

    /**
     * 해당 차시의 컨텐츠 코드 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentStrucBookLessonDayNo(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 구조 정보 등록 - 중복 체크(중복이 아닌 컨텐츠 구조 정보만 등록)
     * @param condition
     * @return
     * @throws Exception
     */
    public int addDayNoContentStrucWithDuplicateCheck(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 구조 정보 등록
     * @param condition
     * @return
     * @throws Exception
     */
    public int addDayNoContentStruc(VSCondition condition) throws Exception;

    /**
     * 해당 차시의 컨텐츠 구조 정보 삭제
     * @param vsObject
     * @return
     * @throws Exception
     */
    public int deleteDayNoContentStruc(VSCondition condition) throws Exception;

    /**
     * 해당 차시의 상위 북/레슨 코드 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public VSObject getContentStrucBookLessonCd(VSCondition condition) throws Exception;

    /**
     * 해당 레슨의 하위 차시 리스트 정보 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getDayNoCdList(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 패키지 버전 카운트 조회
     *      기존 등록된 버전이 있는지 체크하는 용도
     * @param condition
     * @return
     * @throws Exception
     */
    public int getContentVersionCount(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 패키지 버전 등록
     * @param condition
     * @return
     * @throws Exception
     */
    public int addContentVersion(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 패키지 버전 업
     * @param condition
     * @return
     * @throws Exception
     */
    public int updateContentVersion(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 패키지 버전 정보 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public VSObject getContentPackageVersionInfo(VSCondition condition) throws Exception;


    /**
     * 컨퍼런스 목록 총 갯수 조회
     *
     * @param vsObejct
     * @return
     * @throws Exception
     */
    public String getReferenceListCnt(VSObject vsObejct) throws Exception;


    /**
     * 레퍼런스 타입별 목록 조회
     *
     * @param dataCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception;

}
