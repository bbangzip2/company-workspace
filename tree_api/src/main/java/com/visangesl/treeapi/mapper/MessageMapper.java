package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

/**
 * 메시지 박스 관련 처리를 위한 mapper interface
 * @author user
 *
 */
public interface MessageMapper {


    /**
     * 해당 사용자의 메시지 수신함 목록을 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getMessageInBox(VSCondition condition) throws Exception;

    /**
     * 해당 사용자가 특정 사용자와 주고 받은 메시지 목록을 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getMessageList(VSCondition condition) throws Exception;

    /**
     * 해당 사용자의 미 열람 메시지를 읽음으로 처리
     * @param condition
     * @return
     * @throws Exception
     */
    public int updateMessageOkYn(VSCondition condition) throws Exception;

    /**
     * 해당 사용자의 정보로 특정 사용자에게 메시지를 발송
     * @param condition
     * @return
     * @throws Exception
     */
    public int addMessage(VSCondition condition) throws Exception;

    /**
     * 해당 사용자가 속한 클래스의 소속 회원 목록을 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassMemberMessageList(VSCondition condition) throws Exception;

    /**
     * 해당 사용자가 수신한 메시지 중 미열람 메시지의 카운트를 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public int getNewMessageCount(VSCondition condition) throws Exception;

}
