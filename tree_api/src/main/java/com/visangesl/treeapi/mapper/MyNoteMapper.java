package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mynote.vo.MyNoteCondition;

/**
 * note lesson 조회  관련 mapper interface
 * 
 * @author hong
 *
 */
public interface MyNoteMapper {
    public List<VSObject> getLessonList(MyNoteCondition myNoteCondition) throws Exception;
}
