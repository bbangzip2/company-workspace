package com.visangesl.treeapi.exception;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.BadSqlGrammarException;

import com.visangesl.treeapi.exception.TreeException;
import com.visangesl.treeapi.exception.TreeRuntimeException;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * Exception 처리를 하기 위한 Class
 * 
 * @author imac
 *
 */
public class ExceptionHandler {
    private String code;
    private String message;

    private Exception e;

    public ExceptionHandler(Exception e) {
        this.e = e;
        processException();

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Exception getE() {
        return e;
    }

    public void setE(Exception e) {
        this.e = e;
        processException();
    }

    /**
     * Exception별 error code와 message를 구분하여 사용한다
     */
    public void processException() {
        if (e instanceof DuplicateKeyException) {
            code = TreeProperties.getProperty("error.duplicatekey.code");
            message = TreeProperties.getProperty("error.duplicatekey.msg");

        } else if (e instanceof BadSqlGrammarException) {
            code = TreeProperties.getProperty("error.badsqlgrammar.code");
            message = TreeProperties.getProperty("error.badsqlgrammar.msg");

        } else if (e instanceof TreeException) {
            TreeException se = (TreeException) e;
            code = se.getCode();
            message = se.getMsg();
        } else if (e instanceof TreeRuntimeException) {
            TreeRuntimeException se = (TreeRuntimeException) e;
            code = se.getCode();
            message = se.getMsg();
        } else {
            code = TreeProperties.getProperty("error.etc.code");
            message = TreeProperties.getProperty("error.etc.msg");
        }
    }
}
