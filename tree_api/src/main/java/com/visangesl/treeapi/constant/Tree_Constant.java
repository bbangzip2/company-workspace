package com.visangesl.treeapi.constant;

import java.net.InetAddress;

/**
 * 상수 class
 * 
 * @author imac
 *
 */
public class Tree_Constant {
/*
 *        DEV1 사용안하는거면 지워주세요.

 
    public static final String RESULT_SUCCESS = "0000";
    public static final String RESULT_FAIL = "9999";

    // TEST

    //- 내부 10.
    public static final String TEST_IN_SITE_DOMAIN = "http://10.30.0.246";

    // 외부 106.
    public static final String TEST_OUT_SITE_DOMAIN = "http://106.241.5.246";

    public static final String TEST_SITE_PORT = "9090";
    public static final String TEST_SITE_CDN_PATH = "/VisangNAS/tree";


    // REAL
    /*
    public static final String SITE_DOMAIN = "http://api.treebooster.com";
//    public static final String SITE_URL = "http://" + SITE_DOMAIN;
    public static final String SITE_CDN_PATH = ""; // 다운로드 받을때 서버의 폴더명
    public static final String CDN_DOMAIN = "http://cdn.treebooster.com";
    */
    
    // China
    public static final String SERVER_IP = getServerIP();
    public static final String SITE_DOMAIN = "http://" + SERVER_IP;
    
    // *** API에서 리턴되는 썸네일 경로는 모두 아래 SITE_CDN_PATH를 VO 단에서 포함시켜서 리턴하고 있습니다. ***  
    public static final String SITE_CDN_PATH = "http://tstcdn.treebooster.com"; // 다운로드 받을때 서버의 폴더명
    public static final String TEST_CDN_DOMAIN = "http://tstcdn.treebooster.com";
    public static final String CDN_DOMAIN = TEST_CDN_DOMAIN;


    public static String SITE_URL = "";
    public static String SITE_FULL_URL = "";
    public static String SITE_PORT = "80";

    public static final String SECURE_SITE_URL = "https://" + SITE_DOMAIN;

    public static final String CDN_DOMAIN_STREAM = "";


    private static String getServerIP() {
    	String serverIP = null;
    	try {
    		serverIP = InetAddress.getLocalHost().getHostAddress();
    	} catch (Exception e) {
    		
    	}
    	
    	return serverIP;
    }
}
