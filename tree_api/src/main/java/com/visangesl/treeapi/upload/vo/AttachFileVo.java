package com.visangesl.treeapi.upload.vo;

import com.visangesl.tree.vo.VSObject;
/**
 * 첨부파일 정보를 담는 Vo
 * 
 * @author hong
 *
 */
public class AttachFileVo extends VSObject {
	// 첨부파일 정보 저장
	private String relSeq; // 첨부파일의 구분 키값 : noteSeq, portpolioSeq등의 순번 값 
	private String notePage; // 노트의 페이지  
	private String fileNm;
	private String saveFileNm;
	private String filePath;
	private String fileType; // 이미지, 동영상 의 파일 타입 구분자 ( 예 : FT001 : 이미지..)
	private String thmbPath;
	private String thmbPath_s;
	private String thmbPath_l;
	
	private String fileSect; // 첨부파일의 구분자 (노트:N , 포트폴리오: P) 
	

	public String getRelSeq() {
		return relSeq;
	}
	public void setRelSeq(String relSeq) {
		this.relSeq = relSeq;
	}
	public String getNotePage() {
		return notePage;
	}
	public void setNotePage(String notePage) {
		this.notePage = notePage;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getSaveFileNm() {
		return saveFileNm;
	}
	public void setSaveFileNm(String saveFileNm) {
		this.saveFileNm = saveFileNm;
	}
	public String getFilePath() {
        return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getThmbPath() {
		return thmbPath;
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getFileSect() {
		return fileSect;
	}
	public void setFileSect(String fileSect) {
		this.fileSect = fileSect;
	}
	public String getThmbPath_s() {
		return thmbPath_s;
	}
	public void setThmbPath_s(String thmbPath_s) {
		this.thmbPath_s = thmbPath_s;
	}
	public String getThmbPath_l() {
		return thmbPath_l;
	}
	public void setThmbPath_l(String thmbPath_l) {
		this.thmbPath_l = thmbPath_l;
	}


}
