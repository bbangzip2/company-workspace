package com.visangesl.treeapi.upload.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.upload.vo.AttachFileListConditionVo;

/**
 * 파일 업로드 관련 interface
 * 
 * @author hong
 *
 */
public interface FileUploadService {
	
	/**
	 * 노트 파일 업로드 처리 
	 * @param memberId
	 * @param cid : 카드 순번 
	 * @param noteText : 노트 텍스트 
	 * @param page : 노트 페이지 
	 * @param curPage : 노트 현재 페이지 
	 * @param uploadFileList : 파일 객체 
	 * @param realpath : 실제 파일경로 
	 * @param savepath : 저장 파일경로 
	 * @throws Exception
	 */
	public void noteFileUpload(String memberId, String cid, String noteText, String page, String curPage, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception;
	
	/**
	 * 포트폴리오 등록 처리 
	 * @param memberId
	 * @param clsSeq
	 * @param clsSchdlSeq
	 * @param prodSeq
	 * @param uploadFileList
	 * @param realpath
	 * @param savepath
	 * @throws Exception
	 */
	public void cPortFolioInsert(String memberId, String clsSeq,
			String dayNoCd, String prodSeq, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception;
	
	/**
	 * 첨부파일 목록 조회 
	 * @param afListCond
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getAttachFileList(AttachFileListConditionVo afListCond) throws Exception;
	
	/**
	 * 노트 첨부파일 목록 조회 
	 * @param afListCond
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getNoteAttachFileList(AttachFileListConditionVo afListCond) throws Exception;
	
	
	/**
	 * 파일 단건 업로드 
	 * @param uploadFileList
	 * @param realpath
	 * @param savepath
	 * @throws Exception
	 */
	public void onlyFileUpload(List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception;
	
}
