package com.visangesl.treeapi.upload.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mapper.AttachFileMapper;
import com.visangesl.treeapi.mapper.NoteMapper;
import com.visangesl.treeapi.mapper.PortFolioMapper;
import com.visangesl.treeapi.portfolio.vo.PortfolioVo;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.upload.service.FileUploadService;
import com.visangesl.treeapi.upload.vo.AttachFileListConditionVo;
import com.visangesl.treeapi.upload.vo.AttachFileReadVo;
import com.visangesl.treeapi.upload.vo.AttachFileVo;
import com.visangesl.treeapi.upload.vo.NoteInfoVo;
import com.visangesl.treeapi.util.FileRenamePolicy;

/**
 * 파일 업로드 관련 처리를 하는  클래스 
 * 
 * @author hong
 *
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {

	private final Log logger = LogFactory.getLog(this.getClass());
		
	@Autowired
	PortFolioMapper portfolioMapper;
	
	@Autowired
	AttachFileMapper attachFileMapper;
	
	@Autowired
	NoteMapper noteMapper;

	
	/**
	 * 노트 파일 저장 처리 
	 */
	@Override
	@Transactional
	public void noteFileUpload(String memberId , String cid, String noteText, String page, String curPage, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception {
		
		String org_file_name = "";
		String real_file_name = "";
		//long file_size = 0;
		
		try{
		
			NoteInfoVo ntVo = new NoteInfoVo();
			
			ntVo.setMbrId(memberId);
			ntVo.setProdSeq(cid); //
			ntVo.setNoteText(noteText); //
			ntVo.setPage(page); //  
			ntVo.setCurPage(curPage);
			  
			
			int noteCount = noteMapper.getNoteCount(ntVo);
			
			logger.debug("noteCount============"+noteCount);
			
			// 저장된 노트 키 값  
			String relSeq = "" ;
						
			if(noteCount > 0){
				noteMapper.uNoteData(ntVo);
				relSeq = noteMapper.getNoteSeq(ntVo);
				
				AttachFileListConditionVo attCon = new AttachFileListConditionVo();
				
				attCon.setRelSeq(relSeq);
				attCon.setMbrId(memberId);
				// 저장되어있던 파일 삭제 처리 함. 
				List<VSObject> savedFileList = attachFileMapper.getNoteAttachFileList(attCon);
				
				for(int i = 0 ; i < savedFileList.size(); i++){
					
					String filePath = ( (AttachFileReadVo) savedFileList.get(i)).getFilePath();
					
					// ( realpath+ filePath )
					File df = new File(TreeProperties.getProperty("tree_save_filepath")+ filePath);
					
					if(df.delete()){
						logger.debug("이미 등록된 파일 삭제 처리 성공: 파일경로 ="+TreeProperties.getProperty("tree_save_filepath")+filePath);
					}else{
						logger.debug("이미 등록된 파일 삭제 실패 : 파일경로 ="+TreeProperties.getProperty("tree_save_filepath")+filePath);
					}
				}
				
				
				
			}else{
				attachFileMapper.cNoteData(ntVo);
				relSeq = ntVo.getNoteSeq();
			}
			
			
			
			// 파일 INSERT  하기 전에 등록되었던 파일 데이터를 지워준다. 
       		AttachFileVo afConditionVo = new AttachFileVo();
       		
       		afConditionVo.setNotePage("1"); // 페이지는 2월 기준으로 1만 있다. 
       		afConditionVo.setRelSeq(relSeq);
       		
       		afConditionVo.setFileSect("N");
       		
       		attachFileMapper.dTreeAttachFile(afConditionVo);
       		
			
			// 업로드된 파일에 대한 작업을 진행한다
	        for (MultipartFile file : uploadFileList) {
	        	org_file_name = file.getOriginalFilename();
	        	//file_size = file.getSize();
	        	
	            if ("".equals(org_file_name)) {
	                continue;
	            }
	         // 이 어노테이션은 사용하지 않는 코드 관련 경고를 억제하는 어노테이션 입니다. 
	            @SuppressWarnings("unused")
	            String body = null;
	            String ext = null;
	            String fileType ="FT999";
	            
	            int dot = org_file_name.lastIndexOf(".");
	            
	            if (dot != -1) {                  
	                body = org_file_name.substring(0, dot);
	                ext = org_file_name.substring(dot);
	                
	                // FT001(IMAGE), FT002 (AUDIO), FT003 (VIDEO) 
	                if( ext.toUpperCase().equals(".JPG") || ext.toUpperCase().equals(".PNG") || ext.toUpperCase().equals(".GIF") || ext.toUpperCase().equals(".JPEG") ){
	                	fileType = "FT001";
	                }else if( ext.toUpperCase().equals(".MP3") || ext.toUpperCase().equals(".OGG") ){
	                	fileType = "FT002";
	                }else if( ext.toUpperCase().equals(".MP4") || ext.toUpperCase().equals(".AVI") ){
	                	fileType = "FT003";
	                }                
	            } else {
	                body = org_file_name;
	                ext = "";
	            }
	            
	            // 저장되는 파일명은 타임값에 랜덤 숫자를 더해서 만들어준다. 
	    		Random r = new Random();
	    		int k = r.nextInt(100000000);
	    		
	    		String saveFileName = Long.toString(new Date().getTime() /1000+ k)+ext;
	            
	            
	            File realFile = new File(realpath + File.separator + saveFileName);
	            realFile = new FileRenamePolicy().rename(realFile);
	            
	            logger.debug("[renamed filename]="+realFile.getName());
	            
	            real_file_name = realFile.getName();
	            
	            // 업로드된 파일을 다른 이름으로 복사(System 타임을 사용)
	            File destination = new File(realpath + File.separator + real_file_name);
	            
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	            
	            // file vo 
	       		AttachFileVo afVo = new AttachFileVo();
	       		
	       		afVo.setFileNm(org_file_name);
	       		afVo.setFilePath(savepath + File.separator + real_file_name);
	       		afVo.setNotePage("1"); // 페이지는 2월 기준으로 1만 있다. 
	       		afVo.setRelSeq(relSeq);
	       		afVo.setSaveFileNm(saveFileName);
	       		// 썸네일 사이즈에 대한 정의가 없어서 기본 파일경로에 썸네일 경로를 넣어준다. 
	       		afVo.setThmbPath_s(savepath + File.separator + real_file_name); 
	       		afVo.setThmbPath_l(savepath + File.separator + real_file_name); 
	       		afVo.setFileType(fileType);
	       		afVo.setFileSect("N");
	       		
	       		attachFileMapper.cTreeAttachFile(afVo);
	       	
	        }
	        
        }catch(Exception e){
        	logger.error(e.toString());
        	throw e;
        }
        
	}
	
	/**
	 * 포트폴리오 등록 처리 
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void cPortFolioInsert(String memberId, String clsSeq,
			String clsSchdlSeq, String prodSeq, List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception {
		
		
		String org_file_name = "";
		String real_file_name = "";
		//long file_size = 0;
		
		try{
		
			PortfolioVo portVo = new PortfolioVo();
			
			portVo.setMbrId(memberId);
			portVo.setClsSeq(clsSeq); // 클래스 순번 
			
			portVo.setClsSchdlSeq(clsSchdlSeq); // 수업 순번 
			portVo.setProdSeq(prodSeq); // 카드순번 
			
			portfolioMapper.insertPortfolioInfo(portVo);
			
			// 저장된 포트 폴리오의 키값. INCLS_RSLT_SEQ
			String refSeq = portVo.getInclsRsltSeq();
			
			// 업로드된 파일에 대한 작업을 진행한다
	        for (MultipartFile file : uploadFileList) {
	        	org_file_name = file.getOriginalFilename();
	        	//file_size = file.getSize();
	
	            if ("".equals(org_file_name)) {
	                continue;
	            }
	            
	         // 이 어노테이션은 사용하지 않는 코드 관련 경고를 억제하는 어노테이션 입니다. 
	            @SuppressWarnings("unused")
	            String body = null;
	            String ext = null;
	            String fileType = "FT999"; //파일형태 
	            
	            int dot = org_file_name.lastIndexOf(".");
	            if (dot != -1) {          
	                body = org_file_name.substring(0, dot);
	                ext = org_file_name.substring(dot);
	                // FT001(IMAGE), FT002 (AUDIO), FT003 (VIDEO) 
	                if( ext.toUpperCase().equals(".JPG") || ext.toUpperCase().equals(".PNG") || ext.toUpperCase().equals(".GIF") || ext.toUpperCase().equals(".JPEG") ){
	                	fileType = "FT001";
	                }else if( ext.toUpperCase().equals(".MP3") || ext.toUpperCase().equals(".OGG") ){
	                	fileType = "FT002";
	                }else if( ext.toUpperCase().equals(".MP4") || ext.toUpperCase().equals(".AVI") ){
	                	fileType = "FT003";
	                }   
	                
	            } else {
	                body = org_file_name;
	                ext = "";
	            }
	            
	            // 저장되는 파일명은 타임값에 랜덤 숫자를 더해서 만들어준다. 
	    		Random r = new Random();
	    		int k = r.nextInt(100000000);
	    		
	    		String saveFileName = Long.toString(new Date().getTime() /1000+ k)+ext;
	    		
	            //logger.debug("insertNoticeInfo file_size : " + file_size);
	            
	            File realFile = new File(realpath + "/" + saveFileName);
	            realFile = new FileRenamePolicy().rename(realFile);
	            real_file_name = realFile.getName();
	            
	            // 업로드된 파일을 다른 이름으로 복사(System 타임을 사용)
	            File destination = new File(realpath + "/" + real_file_name);
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	            
	            
	            
	         // file vo 
	    		AttachFileVo afVo = new AttachFileVo();
	    		
	    		afVo.setFileNm(org_file_name);
	    		afVo.setFilePath(savepath + "/" + real_file_name);
	    		afVo.setNotePage("1"); // 페이지는 2월 기준으로 1만 있다. 
	    		afVo.setRelSeq(refSeq);
	    		afVo.setSaveFileNm(saveFileName);
	    		afVo.setThmbPath_s(savepath + "/" + real_file_name); //썸네일 소자. 
	    		afVo.setThmbPath_l(savepath + "/" + real_file_name); //썸네일 대자.
	    		
	    		afVo.setFileType(fileType);
	    		afVo.setFileSect("P");
	    		
	    		attachFileMapper.cTreeAttachFile(afVo);
	    		
	    		
	        }
		
		}catch(Exception e){
			logger.error(e.toString());
			throw e;
		}       
	}
	
	/**
	 * 첨부파일 목록 조회 
	 */
	@Override
	public List<VSObject> getAttachFileList(AttachFileListConditionVo afListCond) throws Exception{
		return attachFileMapper.getAttachFileList(afListCond);
	}

	/**
	 * 노트에 첨부된 파일 목록 조회 
	 */
	@Override
	public List<VSObject> getNoteAttachFileList(AttachFileListConditionVo afListCond) throws Exception{
		return attachFileMapper.getNoteAttachFileList(afListCond);
	}
	
	/**
	 * 파일 단건 업로드 처리 
	 */
	public void onlyFileUpload(List<MultipartFile> uploadFileList, String realpath, String savepath) throws Exception{
		String org_file_name = "";
		String real_file_name = "";
		//long file_size = 0;
		
		try{
			
			// 업로드된 파일에 대한 작업을 진행한다
	        for (MultipartFile file : uploadFileList) {
	        	org_file_name = file.getOriginalFilename();
	        	//file_size = file.getSize();
	
	            if ("".equals(org_file_name)) {
	                continue;
	            }
	            
	            // 이 어노테이션은 사용하지 않는 코드 관련 경고를 억제하는 어노테이션 입니다. 
	            @SuppressWarnings("unused")
				String body = null;
	            @SuppressWarnings("unused")
	            String ext = null;
	            
	            int dot = org_file_name.lastIndexOf(".");
	            if (dot != -1) {          
	                body = org_file_name.substring(0, dot);
	                ext = org_file_name.substring(dot);
	            } else {
	                body = org_file_name;
	                ext = "";
	            }
	            
	            // 저장되는 파일명은 타임값에 랜덤 숫자를 더해서 만들어준다. 
//	    		Random r = new Random();
//	    		int k = r.nextInt(100000000);
//	    		
//	    		String saveFileName = Long.toString(new Date().getTime() /1000+ k)+ext;
	    		
	            //logger.debug("insertNoticeInfo file_size : " + file_size);
	            
	            File realFile = new File(realpath + File.separator + org_file_name);
	            //realFile = new FileRenamePolicy().rename(realFile);
	            real_file_name = realFile.getName();
	            
	            // 업로드된 파일을 다른 이름으로 복사(System 타임을 사용)
	            File destination = new File(realpath + File.separator + real_file_name);
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	            
	    		
	        }
		
		}catch(Exception e){
			logger.error(e.toString());
			throw e;
		}    
	}
	
}
