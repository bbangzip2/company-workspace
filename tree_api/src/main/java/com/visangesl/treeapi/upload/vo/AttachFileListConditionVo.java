package com.visangesl.treeapi.upload.vo;

import com.visangesl.tree.vo.VSObject;
/**
 * 첨부파일 정보를 담기 위한 Vo
 * 
 * @author hong
 *
 */
public class AttachFileListConditionVo extends VSObject {
	// 첨부파일 정보 저장 
	private String relSeq;
	private String notePage;
	private String prodSeq;
	private String fileNm;
	private String saveFileNm; 
	private String filePath; 
	private String fileType; 
	private String thmbPath;
	private String fileSect;
	private String mbrId;
	
	private String serverPath;
	
	
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getRelSeq() {
		return relSeq;
	}
	public void setRelSeq(String relSeq) {
		this.relSeq = relSeq;
	}
	public String getNotePage() {
		return notePage;
	}
	public void setNotePage(String notePage) {
		this.notePage = notePage;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getSaveFileNm() {
		return saveFileNm;
	}
	public void setSaveFileNm(String saveFileNm) {
		this.saveFileNm = saveFileNm;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getThmbPath() {
		return thmbPath;
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getFileSect() {
		return fileSect;
	}
	public void setFileSect(String fileSect) {
		this.fileSect = fileSect;
	}
	public String getServerPath() {
		return serverPath;
	}
	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}


}
