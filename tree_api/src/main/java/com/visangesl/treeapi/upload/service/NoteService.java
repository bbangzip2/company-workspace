package com.visangesl.treeapi.upload.service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.upload.vo.NoteInfoConditionVo;

/**
 * 노트 정보를 조회하기 윈한 interface
 * 
 * @author hong
 *
 */
public interface NoteService {

	/**
	 * 노트 정보 조회 
	 * 
	 * @param noteInfoConditionVo
	 * @return
	 * @throws Exception
	 */
    public VSObject getContentsInfoData(NoteInfoConditionVo noteInfoConditionVo) throws Exception;
    
}
