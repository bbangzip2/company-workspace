package com.visangesl.treeapi.upload.vo;

import com.visangesl.tree.vo.VSObject;
/**
 * 노트 정보를 조회하기 위해서 조건정보를 담는 Vo
 * 
 * @author hong
 *
 */
public class NoteInfoConditionVo extends VSObject {
 
	private String noteSeq;
	private String mbrId;
	private String curPage;
	private String prodSeq;
	
	public String getNoteSeq() {
		return noteSeq;
	}
	public void setNoteSeq(String noteSeq) {
		this.noteSeq = noteSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getCurPage() {
		return curPage;
	}
	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}


}
