package com.visangesl.treeapi.upload.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mapper.NoteMapper;
import com.visangesl.treeapi.upload.service.NoteService;
import com.visangesl.treeapi.upload.vo.NoteInfoConditionVo;

/**
 * 노트 정보를 조회하기 위한 서비스 클래스
 * 
 * @author hong
 *
 */
@Service
public class NoteServiceImpl implements NoteService {
    @Autowired
    NoteMapper noteMapper;

    /**
     * 노트 정보를 조회
     */
    @Override
    public VSObject getContentsInfoData(NoteInfoConditionVo noteInfoConditionVo) throws Exception{
        return noteMapper.getContentsInfoData(noteInfoConditionVo);
    }

}
