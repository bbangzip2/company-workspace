package com.visangesl.treeapi.zip.service;

import org.springframework.stereotype.Service;

import com.visangesl.treeapi.zip.vo.ZipElements;

/**
 * 파일 압축을 처리하는 interface
 * @author user
 *
 */
@Service
public interface ZipService {

    /**
     * 파일 압축 처리
     * @param condition
     * @return
     * @throws Exception
     */
    public boolean zipFile(ZipElements condition) throws Exception;
}
