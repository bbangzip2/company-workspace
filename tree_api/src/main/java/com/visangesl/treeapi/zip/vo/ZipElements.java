package com.visangesl.treeapi.zip.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.contents.vo.ContentCardPath;

/**
 * 파일 압축 처리를 위한 정보를 담을 VO
 * @author user
 *
 */
public class ZipElements extends VSObject {

    /**
     * 압축 파일 저장 경로
     */
    private String zipFileSavePath;
    /**
     * 압축 파일 명
     */
    private String zipFileName;
    /**
     * 압축 대상 파일 경로 리스트
     */
    private List<ContentCardPath> zipPathList;

    public String getZipFileSavePath() {
        return zipFileSavePath;
    }
    public void setZipFileSavePath(String zipFileSavePath) {
        this.zipFileSavePath = zipFileSavePath;
    }
    public String getZipFileName() {
        return zipFileName;
    }
    public void setZipFileName(String zipFileName) {
        this.zipFileName = zipFileName;
    }
    public List<ContentCardPath> getZipPathList() {
        return zipPathList;
    }
    public void setZipPathList(List<ContentCardPath> zipPathList) {
        this.zipPathList = zipPathList;
    }
}
