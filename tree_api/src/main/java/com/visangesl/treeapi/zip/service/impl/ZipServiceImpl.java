package com.visangesl.treeapi.zip.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipOutputStream;

import org.apache.tools.zip.ZipEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.visangesl.treeapi.contents.vo.ContentCardPath;
import com.visangesl.treeapi.zip.service.ZipService;
import com.visangesl.treeapi.zip.vo.ZipElements;

/**
 * 파일 압축을 처리
 * @author user
 *
 */
@Service
public class ZipServiceImpl implements ZipService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 파일 압축 처리
     */
    @Override
    public boolean zipFile(ZipElements condition) throws Exception {

        boolean zipResult = false;

        List<String> zipEntryList = new ArrayList<String>(); // 압축 대상 파일의 경로 (실제경로|내부경로)
        String entryPathKey; // 압축할 파일의 실제 경로에서 압축 파일 내부의 경로를 나눌 기준 키 값
        String packageFileRealPath = condition.getZipFileSavePath();


        // 패키지 경로 체크
        if (!new File(packageFileRealPath).exists()) {
            // 패키지가 생성될 경로를 미리 생성
            new File(packageFileRealPath).mkdirs();
        }


        // 압축 결과물 저장 경로/명
        String zipFile = packageFileRealPath + File.separator + condition.getZipFileName();


        byte[] buffer = new byte[1024];
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        FileInputStream in = null;

        try {

            // 압축 대상 파일 추출
            for (ContentCardPath cardPathInfo : condition.getZipPathList()) {

                String cardFilePath = cardPathInfo.getFilePath();
                String cardPath = cardPathInfo.getCardPath();

                logger.debug(cardFilePath);
                logger.debug(cardPath);

                entryPathKey = cardFilePath.substring(cardFilePath.lastIndexOf("/") + 1, cardFilePath.length());

                // 압축 대상 파일 리스트 추출 메소드 호출
                generateFileList(zipEntryList, entryPathKey, cardPath, new File(cardFilePath));
            }

//            logger.debug("zipFile: " + zipFile);
//            logger.debug("fileCount: " + zipEntryList.size());


            // 중복 제거 (HashSet 사용) - 카드맵에 중복 카드가 적용시 카드 하나만 패키징에 적용
            zipEntryList = new ArrayList<String>(new HashSet<String>(zipEntryList));

            // 압축 진행
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            for (int i=0; i<zipEntryList.size(); i++) {

                String[] file = zipEntryList.get(i).split("\\|"); // 압축 대상 파일의 경로 (0:실제경로 | 1:내부경로)
                String fileEntry = file[1];
//                logger.debug(i + file[0] + " @@@ " + file[1]);

                // 1.압축 파일 내부 경로를 먼저 등록
                ZipEntry ze = new ZipEntry(fileEntry);
                zos.putNextEntry(ze);

                // 2.지정된 내부 경로에 맞게 실제 파일을 읽은 후 write한다.
                in = new FileInputStream(file[0]);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zipResult = true;

        }catch(IOException e){
//            e.printStackTrace();
            zipResult = false;
        }catch(Exception e){
//            e.printStackTrace();
            zipResult = false;
        } finally {
            zos.closeEntry();
            zos.close();
            in.close();
        }

        return zipResult;

    }


    /**
     * @param zipEntryList - 압축할 파일의 경로를 담은 List
     * @param entryPathKey - 압축할 파일의 실제 경로와 압축파일 내부 경로 구분 키
     * @param zipEntryPath - 압축 내부 경로
     * @param node - 압축할 파일/폴더
     */
    public void generateFileList(List<String> zipEntryList, String entryPathKey, String zipEntryPath, File node) {

        if (node.isFile()) {
            String filePath = node.getAbsoluteFile().toString();
            String temp = ""; // 압축 내부 경로

            temp = zipEntryPath + filePath.substring(filePath.indexOf(File.separator + entryPathKey + File.separator), filePath.length());

//            logger.debug(filePath);
//            logger.debug(temp);

            zipEntryList.add(filePath + "|" + temp);
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(zipEntryList, entryPathKey, zipEntryPath, new File(node, filename));
            }
        }
    }

}
