package com.visangesl.treeapi.message.vo;

/**
 * 메시지 수신함 화면에서 필요한 정보를 조회하기 위한 조건을 담은 VO
 * @author user
 *
 */
public class MessageInBox extends MessageSearch {

    /**
     * 수신함 검색 대상 학생 명
     */
    private String nm;

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }
}
