package com.visangesl.treeapi.message.vo;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 메시지 상세 내용을 담을 VO
 * @author user
 *
 */
public class MessageVo extends VSCondition {

    /**
     * 메시지 순번
     */
    private String msgSeq;
    /**
     * 발신자 ID
     */
    private String sendMbrId;
    /**
     * 수신자 ID
     */
    private String recvMbrId;

    /**
     * 메시지 발송일
     */
    private String msgSendDt;
    /**
     * 메시지 확인 여부
     */
    private String msgOkYn;
    /**
     * 메시지 타입
     */
    private String msgType;
    /**
     * 메시지 제목
     */
    private String msgTitle;
    /**
     * 메시지 본문 내용
     */
    private String msgCntt;
    /**
     * 메시지 이미지 경로
     */
    private String msgImgPath;

    public String getMsgSeq() {
        return msgSeq;
    }
    public void setMsgSeq(String msgSeq) {
        this.msgSeq = msgSeq;
    }
    public String getSendMbrId() {
        return sendMbrId;
    }
    public void setSendMbrId(String sendMbrId) {
        this.sendMbrId = sendMbrId;
    }
    public String getRecvMbrId() {
        return recvMbrId;
    }
    public void setRecvMbrId(String recvMbrId) {
        this.recvMbrId = recvMbrId;
    }
    public String getMsgSendDt() {
        return msgSendDt;
    }
    public void setMsgSendDt(String msgSendDt) {
        this.msgSendDt = msgSendDt;
    }
    public String getMsgOkYn() {
        return msgOkYn;
    }
    public void setMsgOkYn(String msgOkYn) {
        this.msgOkYn = msgOkYn;
    }
    public String getMsgType() {
        return msgType;
    }
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }
    public String getMsgTitle() {
        return msgTitle;
    }
    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }
    public String getMsgCntt() {
        return msgCntt;
    }
    public void setMsgCntt(String msgCntt) {
        this.msgCntt = msgCntt;
    }
    public String getMsgImgPath() {
        if (msgImgPath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + msgImgPath;
        } else {
            return msgImgPath;
        }
    }
    public void setMsgImgPath(String msgImgPath) {
        this.msgImgPath = msgImgPath;
    }





}
