package com.visangesl.treeapi.message.vo;

/**
 * 메시지 목록을 조회하기 위한 조건을 담은 VO
 * @author user
 *
 */
public class MessageList extends MessageSearch {

    /**
     * 발신자 ID
     */
    private String sendMbrId;
    /**
     * 수신자 ID
     */
    private String recvMbrId;

    public String getSendMbrId() {
        return sendMbrId;
    }
    public void setSendMbrId(String sendMbrId) {
        this.sendMbrId = sendMbrId;
    }
    public String getRecvMbrId() {
        return recvMbrId;
    }
    public void setRecvMbrId(String recvMbrId) {
        this.recvMbrId = recvMbrId;
    }
}
