package com.visangesl.treeapi.message.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 메시지 발송을 위한 정보를 담은 VO
 * @author user
 *
 */
public class MessageCompose extends VSCondition {

    /**
     * 수신자 ID
     */
    private String sendMbrId;
    /**
     * 발신자 ID
     */
    private String recvMbrId;
    /**
     * 메시지 타입 (1:강사comment, 2:대화, 3:review학생 질문, 4:review강사 답변)
     */
    private String msgType;
    /**
     * 메시지 타이틀
     */
    private String msgTitle;
    /**
     * 메시지 본문 내용
     */
    private String msgCntt;
    /**
     * 메시지 이미지 경로 (강사comment, review학생 질문에만 해당)
     */
    private String msgImgPath;

    public String getSendMbrId() {
        return sendMbrId;
    }
    public void setSendMbrId(String sendMbrId) {
        this.sendMbrId = sendMbrId;
    }
    public String getRecvMbrId() {
        return recvMbrId;
    }
    public void setRecvMbrId(String recvMbrId) {
        this.recvMbrId = recvMbrId;
    }
    public String getMsgType() {
        return msgType;
    }
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }
    public String getMsgTitle() {
        return msgTitle;
    }
    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }
    public String getMsgCntt() {
        return msgCntt;
    }
    public void setMsgCntt(String msgCntt) {
        this.msgCntt = msgCntt;
    }
    public String getMsgImgPath() {
        return msgImgPath;
    }
    public void setMsgImgPath(String msgImgPath) {
        this.msgImgPath = msgImgPath;
    }

}
