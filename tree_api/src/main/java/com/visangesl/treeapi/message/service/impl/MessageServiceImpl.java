package com.visangesl.treeapi.message.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mapper.MessageMapper;
import com.visangesl.treeapi.message.service.MessageService;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 메시지 박스 관련 정보 처리
 * @author user
 *
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageMapper messageMapper;

    /**
     * 해당 사용자의 메시지 수신함 목록을 조회
     */
    @Override
    public List<VSObject> getMessageInBox(VSCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getMessageInBox(condition);
    }

    /**
     * 해당 사용자가 특정 사용자와 주고 받은 메시지 목록을 조회
     */
    @Override
    public List<VSObject> getMessageList(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getMessageList(condition);
    }

    /**
     * 해당 사용자의 미 열람 메시지를 읽음으로 처리
     */
    @Override
    public VSResult<VSObject> updateMessageOkYn(VSCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();

        int affectedRows = messageMapper.updateMessageOkYn(condition);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }
        return resultCodeMsg;
    }

    /**
     * 해당 사용자의 정보로 특정 사용자에게 메시지를 발송
     */
    @Override
    public VSResult<VSObject> addMessage(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        VSResult<VSObject> resultCodeMsg = new VSResult<VSObject>();
        int affectedRows = messageMapper.addMessage(condition);

        if (affectedRows < 1) {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
        } else {
            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
        }
        return resultCodeMsg;
    }

    /**
     * 해당 사용자가 속한 클래스의 소속 회원 목록을 조회
     */
    @Override
    public List<VSObject> getClassMemberMessageList(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getClassMemberMessageList(condition);
    }

    /**
     * 해당 사용자가 수신한 메시지 중 미열람 메시지의 카운트를 조회
     */
    @Override
    public int getNewMessageCount(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return messageMapper.getNewMessageCount(condition);
    }

}
