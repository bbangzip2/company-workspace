package com.visangesl.treeapi.message.vo;

import com.visangesl.treeapi.property.TreeProperties;

/**
 * 메시지 수신함 화면에서 필요한 정보를 담은 VO
 * @author user
 *
 */
public class MessageInBoxVo extends MessageMbrId {

    /**
     * 회원 명
     */
    private String nm;
    /**
     * 회원 프로필 이미지 경로
     */
    private String profilePhotoPath;
    /**
     * 클래스 명
     */
    private String clsNm;
    /**
     * 메시지 확인 여부
     */
    private String msgOkYn;

    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getProfilePhotoPath() {
        if (profilePhotoPath == null) {
            return profilePhotoPath;
        } else {
            return TreeProperties.getProperty("tree_cdn_url") + profilePhotoPath;
        }
    }
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }
    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getMsgOkYn() {
        return msgOkYn;
    }
    public void setMsgOkYn(String msgOkYn) {
        this.msgOkYn = msgOkYn;
    }
}
