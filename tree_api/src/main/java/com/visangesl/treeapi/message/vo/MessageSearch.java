package com.visangesl.treeapi.message.vo;

/**
 * 메시지 박스 검색을 위한 정보를 담을 VO
 * @author user
 *
 */
public class MessageSearch extends MessageMbrId {

    /**
     * 페이지 사이즈
     */
    private int pageSize;
    /**
     * 페이지 번호
     */
    private int pageNo;

    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    public int getPageNo() {
        return pageNo;
    }
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }
}
