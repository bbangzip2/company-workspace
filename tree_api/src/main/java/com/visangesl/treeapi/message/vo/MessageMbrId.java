package com.visangesl.treeapi.message.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 메시지 박스에서 회원 ID를 담을 VO
 * @author user
 *
 */
public class MessageMbrId extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

}
