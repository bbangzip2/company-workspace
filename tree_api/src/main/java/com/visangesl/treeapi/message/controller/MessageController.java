package com.visangesl.treeapi.message.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.message.service.MessageService;
import com.visangesl.treeapi.message.vo.MessageClassMemberList;
import com.visangesl.treeapi.message.vo.MessageCompose;
import com.visangesl.treeapi.message.vo.MessageInBox;
import com.visangesl.treeapi.message.vo.MessageList;
import com.visangesl.treeapi.message.vo.MessageMbrId;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;


/**
 * 메시지 박스와 관련된 작업을 처리하는 Controller
 * @author user
 *
 */
@Controller
public class MessageController {
    @Autowired
    private MessageService messageService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 메시지 박스 조회
     * @param request
     * @param response
     * @param mbrId 수신자 ID
     * @param nm 검색 회원 명
     * @param pageNo 페이지 번호
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/message/getInboxList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getInboxList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "name", required = false, defaultValue = "") String nm,
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo
            ) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        int pageSize = 5;

        try {
            MessageInBox messageInBoxCondition = new MessageInBox();
            messageInBoxCondition.setMbrId(mbrId);
            messageInBoxCondition.setNm(nm);
            messageInBoxCondition.setPageSize(pageSize);

            // 메시지 상세 내역 보기 일 경우 페이징 처리
            if (pageNo != 0) {
                messageInBoxCondition.setPageNo((pageNo - 1) * pageSize);
            }

            List<VSObject> messageList = messageService.getMessageInBox(messageInBoxCondition);

            result.setResult(messageList);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 메시지 리스트 조회
     * @param request
     * @param response
     * @param recvMbrId 수신자 ID
     * @param sendMbrId 발신자 ID
     * @param pageNo 페이지 번호
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/message/getMessageList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getMessageList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String recvMbrId,
            @RequestParam(value = "senderId", required = false, defaultValue = "") String sendMbrId,
            @RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo
            ) throws Exception {

    	VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        int pageSize = 15;

        try {
            MessageList messageListCondition = new MessageList();
            messageListCondition.setRecvMbrId(recvMbrId);
            messageListCondition.setSendMbrId(sendMbrId);
            messageListCondition.setPageSize(pageSize);

            // 페이징 처리
            if (pageNo != 0) {
                messageListCondition.setPageNo((pageNo - 1) * pageSize);
            }

            // 안읽은 메시지 읽음 처리
            if (!sendMbrId.equals("")) {
                messageService.updateMessageOkYn(messageListCondition);
            }

            result.setResult(messageService.getMessageList(messageListCondition));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 메시지 발송
     *      추후 리뷰 기능에서 메시지 타입 관련 사항이 적용되어야 함.
     *          (메시지 타입, 메시지 제목, 이미지 경로)
     * @param request
     * @param response
     * @param recvMbrId 수신자 ID
     * @param sendMbrId 발신자 ID
     * @param msgCntt 메시지 내용
     * @param msgType 메시지 타입(1:강사comment, 2:대화, 3:review학생 질문, 4:review강사 답변)
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/message/composeMessage", method=RequestMethod.POST)
    public VSResult<VSObject> composeMessage(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String recvMbrId,
            @RequestParam(value = "senderId", required = false, defaultValue = "") String sendMbrId,
            @RequestParam(value = "msgCntt", required = false, defaultValue = "") String msgCntt,
            @RequestParam(value = "msgType", required = false, defaultValue = "") String msgType
            ) throws Exception {

        VSResult<VSObject> result = new VSResult<VSObject>();

        try {

            logger.debug(msgType);

            MessageCompose messageComposeCondition = new MessageCompose();
            messageComposeCondition.setSendMbrId(sendMbrId);
            messageComposeCondition.setRecvMbrId(recvMbrId);
            messageComposeCondition.setMsgCntt(msgCntt);
            messageComposeCondition.setMsgType(msgType);

            messageService.addMessage(messageComposeCondition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 클래스 구성원 리스트 (미열람 메시지 유무 포함)
     * @param request
     * @param response
     * @param clsSeq 클래스 코드
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/message/getClassMemberMessageList", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getClassMemberMessageList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "classSeq", required = false, defaultValue = "") String clsSeq
            ) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        // 로그인 회원 정보 조회
        String mbrId = TreeSpringSecurityUtils.getPrincipalAuthorities().getUsername();
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();


        // 강사/학생 등급 코드 조회
        String teacherGrade = TreeProperties.getProperty("tree_teacher");
        String studentGrade = TreeProperties.getProperty("tree_student");


        // 로그인 계정권한별 조회 권한 변경 지정 (강사는 학생, 학생은 강사 정보를 조회하기 위해)
        if (mbrGrade.equals(teacherGrade)) {
            mbrGrade = studentGrade;
        } else {
            mbrGrade = teacherGrade;
        }

        try {

            // 클래스 구성원 리스트 조회
            MessageClassMemberList messageCondition = new MessageClassMemberList();
            messageCondition.setMbrId(mbrId);
            messageCondition.setClsSeq(clsSeq);
            messageCondition.setMbrGrade(mbrGrade);

            result.setResult(messageService.getClassMemberMessageList(messageCondition));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }


    /**
     * 미 열람 메시지 카운트 조회
     * @param request
     * @param response
     * @param recvMbrId 수신자 ID
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/message/getNewMessageCount", method=RequestMethod.GET)
    public VSResult<Object> getNewMessageCount(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String recvMbrId
            ) throws Exception {

        VSResult<Object> result = new VSResult<Object>();

        // 로그인 회원 정보 조회
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();

        try {
            MessageMbrId messageMbrIdCondition = new MessageMbrId();
            messageMbrIdCondition.setMbrId(mbrId);

            result.setResult(messageService.getNewMessageCount(messageMbrIdCondition));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

}
