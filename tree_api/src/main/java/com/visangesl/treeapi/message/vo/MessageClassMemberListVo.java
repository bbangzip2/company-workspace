package com.visangesl.treeapi.message.vo;

import com.visangesl.treeapi.property.TreeProperties;

/**
 * 메시지 박스에서 클래스 구성원 목록 정보를 담을 VO
 * @author user
 *
 */
public class MessageClassMemberListVo extends MessageMbrId {

    /**
     * 클래스 명
     */
    private String clsNm;
    /**
     * 사용자 명
     */
    private String nm;
    /**
     * 사용자 프로필 이미지 경로
     */
    private String profilePhotoPath;
    /**
     * 메시지 확인 여부
     */
    private String msgOkYn;

    public String getClsNm() {
        return clsNm;
    }
    public void setClsNm(String clsNm) {
        this.clsNm = clsNm;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getProfilePhotoPath() {
        if (profilePhotoPath == null) {
            return profilePhotoPath;
        } else {
            return TreeProperties.getProperty("tree_cdn_url") + profilePhotoPath;
        }
    }
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }
    public String getMsgOkYn() {
        return msgOkYn;
    }
    public void setMsgOkYn(String msgOkYn) {
        this.msgOkYn = msgOkYn;
    }

}
