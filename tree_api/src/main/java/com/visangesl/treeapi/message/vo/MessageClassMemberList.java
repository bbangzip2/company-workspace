package com.visangesl.treeapi.message.vo;

/**
 * 메시지 박스에서 클래스 구성원 목록을 조회하기 위한 정보를 담을 VO
 * @author user
 *
 */
public class MessageClassMemberList extends MessageMbrId {

    /**
     * 클래스 코드
     */
    private String clsSeq;
    /**
     * 회원 등급
     */
    private String mbrGrade;

    public String getClsSeq() {
        return clsSeq;
    }
    public void setClsSeq(String clsSeq) {
        this.clsSeq = clsSeq;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }

}
