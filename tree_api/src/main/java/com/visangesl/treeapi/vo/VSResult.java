package com.visangesl.treeapi.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * client로 응답을 처리하는 VO
 * 
 * @author imac
 *
 * @param <T>
 */
public class VSResult<T> extends VSObject {
	private String code;
	private String message;

	private T result;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

    public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
}
