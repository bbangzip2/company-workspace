package com.visangesl.treeapi.vo;

import java.util.HashMap;

import com.visangesl.tree.vo.VSObject;

/**
 * client로 응답시 HashMap을 포함한 VO
 * @author imac
 *
 * @param <T>
 * @param <U>
 */
public class VSResultMap<T, U> extends VSResult<T> {
    HashMap<String, VSObject> paramMap;

	public HashMap<String, VSObject> getParamMap() {
		return paramMap;
	}

	public void setParamMap(HashMap<String, VSObject> paramMap) {
		this.paramMap = paramMap;
	}
}
