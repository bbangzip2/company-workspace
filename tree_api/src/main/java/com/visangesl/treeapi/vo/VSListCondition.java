package com.visangesl.treeapi.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * schoolLvl, sortKey는 deprecated시킵니다.
 * 해당 조건이 필요하신 분은 VSListCondition을 상속받아 별도 VO를 생성하시기 바랍니다.
 * 
 * @author imac
 *
 */
public class VSListCondition extends VSCondition {
	private int pageNo;
	private int pageSize;

	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
