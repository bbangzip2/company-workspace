package com.visangesl.treeapi.member.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.classroom.service.ClassRoomService;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.member.service.MemberService;
import com.visangesl.treeapi.member.vo.MemberInfo;
import com.visangesl.treeapi.member.vo.MemberVo;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 회원 정보관련 처리를 하는 Controller
 *
 * @author imac
 *
 */
@Controller
public class MemberController {
    @Autowired
    MemberService service;

    @Autowired
    ClassRoomService classRoomService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 트리 어드민 로그인 페이지 이동 처리
     *
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/member/login", method = RequestMethod.GET)
    public String treeLogin(HttpServletRequest request, Model model)
            throws Exception {

        return "/member/login";
    }
    /**
     * 메인화면 정보 : 사용자 기본정보 조회
     * @param request
     * @param response
     * @param model
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/member/selectMemberInfo", method = RequestMethod.GET)
    public VSResult<VSObject> selectMemberInfo(HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {

        VSResult<VSObject> result = new VSResult<VSObject>();

        String userId = request.getParameter("userId");

        logger.debug("userId  : " + userId);

        MemberInfo memberInfo = new MemberInfo();
        MemberVo memberVo = new MemberVo();

        try {

            memberInfo.setMbrId(userId);
            memberVo = (MemberVo) service.getMemberInfo(memberInfo);

            result.setResult(memberVo);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 사용자 정보 조회 (선생님 정보)
     * @param request
     * @param response
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/member/selectTeacherInfo", method = RequestMethod.GET)
    public VSResult<VSObject> selectTeacherInfo(HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {

        VSResult<VSObject> result = new VSResult<VSObject>();

        String userId = request.getParameter("userId");

        logger.debug("userId  : " + userId);


        MemberInfo memberInfo = new MemberInfo();
        MemberVo memberVo = new MemberVo();

        try {

            memberInfo.setMbrId(userId);
            memberVo = (MemberVo) service.getMemberInfo(memberInfo);

            result.setResult(memberVo);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }
}
