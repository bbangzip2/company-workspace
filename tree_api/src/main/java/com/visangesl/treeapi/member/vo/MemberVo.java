package com.visangesl.treeapi.member.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 회원 상세 정보를 담기 위한 VO
 * @author user
 *
 */
public class MemberVo extends VSObject {

    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 회원 명
     */
    private String nm;
    /**
     * 회원 닉네임
     */
    private String nickname;
    /**
     * 이메일 주소
     */
    private String email;
    /**
     * 성별
     */
    private String sexsect;
    /**
     * 출생월일
     */
    private String bymd;
    /**
     * 전화 번호
     */
    private String telNo;
    /**
     * 사용 여부
     */
    private String useYn;
    /**
     * 탈퇴 일자
     */
    private String quitDt;
    /**
     * 프로필 이미지 경로
     */
    private String profilePhotopath;
    /**
     * 메모
     */
    private String memo;
    /**
     * 회원 등급
     */
    private String mbrGrade;
    /**
     * 책임자
     */
    private String director;
    /**
     * 홈페이지
     */
    private String homepage;
    /**
     * prorvince
     */
    private String prorvince;
    /**
     * 캠퍼스 명
     */
    private String campNm;

//  private String regDttm;
//  private String modDttm;
//  private String mbrStatus;
//  private String mbrGubun;
//  private String nation;
//  private String clsSeq;



    public String getMbrId() {
        return mbrId;
    }
    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }
    public String getNm() {
        return nm;
    }
    public void setNm(String nm) {
        this.nm = nm;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getSexsect() {
        return sexsect;
    }
    public void setSexsect(String sexsect) {
        this.sexsect = sexsect;
    }
    public String getBymd() {
        return bymd;
    }
    public void setBymd(String bymd) {
        this.bymd = bymd;
    }
    public String getTelNo() {
        return telNo;
    }
    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }
    public String getUseYn() {
        return useYn;
    }
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }
    public String getQuitDt() {
        return quitDt;
    }
    public void setQuitDt(String quitDt) {
        this.quitDt = quitDt;
    }
    public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + profilePhotopath;
        } else {
            return profilePhotopath;
        }
    }
    public void setProfilePhotopath(String profilePhotopath) {
        this.profilePhotopath = profilePhotopath;
    }
    public String getMemo() {
        return memo;
    }
    public void setMemo(String memo) {
        this.memo = memo;
    }
    public String getMbrGrade() {
        return mbrGrade;
    }
    public void setMbrGrade(String mbrGrade) {
        this.mbrGrade = mbrGrade;
    }
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }
    public String getHomepage() {
        return homepage;
    }
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }
    public String getProrvince() {
        return prorvince;
    }
    public void setProrvince(String prorvince) {
        this.prorvince = prorvince;
    }
    public String getCampNm() {
        return campNm;
    }
    public void setCampNm(String campNm) {
        this.campNm = campNm;
    }

}
