package com.visangesl.treeapi.member.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 회원 정보 조회를 위한 조건을 담을 VO
 * @author user
 *
 */
public class MemberInfo extends VSCondition {

    /**
     * 회원 ID
     */
    private String mbrId;

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

}
