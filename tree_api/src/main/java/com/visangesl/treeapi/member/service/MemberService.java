package com.visangesl.treeapi.member.service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
/**
 * 회원 정보처리 관련 interface
 *
 * @author hong
 *
 */
public interface MemberService {

	/**
	 * 사용자 기본정보 조회
	 *
	 * @param member
	 * @return
	 * @throws Exception
	 */
	public VSObject getMemberInfo(VSCondition condition) throws Exception;
}
