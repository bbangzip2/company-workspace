package com.visangesl.treeapi.member.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mapper.MemberMapper;
import com.visangesl.treeapi.member.service.MemberService;

/**
 * 회원 정보처리 관련 class
 *
 * @author hong
 *
 */
@Service
public class MemberServiceImpl implements MemberService {

	@Autowired
	MemberMapper memberMapper;

    /**
     * 사용자 기본정보 조회
     */
    @Override
    public VSObject getMemberInfo(VSCondition condition) throws Exception {
        return memberMapper.getMemberInfo(condition);
    }

}
