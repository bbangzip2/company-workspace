package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 포트폴리오 화면에서 필요한 학생 정보를 조회하기위한 조건을 담은 VO
 * @author imac
 *
 */
public class PortfolioStudentCondition extends VSCondition {
	/**
	 * 학생 ID
	 */
	private String mbrId;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
}
