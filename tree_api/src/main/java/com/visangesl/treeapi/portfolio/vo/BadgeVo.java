package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 뱃지정보를 담을 VO
 * @author imac
 *
 */
public class BadgeVo extends VSObject {
	/**
	 * 수여받은 뱃지 순번
	 */
	private int badgeSeq;
	/**
	 * 뱃지종류
	 */
	private String badgeType;
	/**
	 * 뱃지순번
	 */
	private int badgeCd;
	
	/**
	 * 뱃지이름
	 */
	private String badgeNm;
	
	public int getBadgeSeq() {
		return badgeSeq;
	}
	public void setBadgeSeq(int badgeSeq) {
		this.badgeSeq = badgeSeq;
	}
	public String getBadgeType() {
		return badgeType;
	}
	public void setBadgeType(String badgeType) {
		this.badgeType = badgeType;
	}
	public int getBadgeCd() {
		return badgeCd;
	}
	public void setBadgeCd(int badgeCd) {
		this.badgeCd = badgeCd;
	}
	public String getBadgeNm() {
		return badgeNm;
	}
	public void setBadgeNm(String badgeNm) {
		this.badgeNm = badgeNm;
	}
	
	
}
