package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 레슨 정보를 담는 VO
 * 
 * @author imac
 *
 */
public class ClassLessonInfo extends VSObject {
	private int lessonSeq;
	private String lessonNm;
	private String lessonThmb;

	public int getLessonSeq() {
		return lessonSeq;
	}
	public void setLessonSeq(int lessonSeq) {
		this.lessonSeq = lessonSeq;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getLessonThmb() {
        if (lessonThmb != null) {
            return TreeProperties.getProperty("tree_cdn_url") + lessonThmb;
        } else {
            return lessonThmb;
        }
//		return lessonThmb;
	}
	public void setLessonThmb(String lessonThmb) {
		this.lessonThmb = lessonThmb;
	}


}
