package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 포트폴리오 정보 조건을 조회할 정보를 담을 VO
 * 
 * @author imac
 *
 */
public class PortfolioListCondition extends VSCondition {
	private String mbrId;
	private int clsSeq;
	private int dayNo;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public int getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(int clsSeq) {
		this.clsSeq = clsSeq;
	}

	public int getDayNo() {
		return dayNo;
	}

	public void setDayNo(int dayNo) {
		this.dayNo = dayNo;
	}
	
}
