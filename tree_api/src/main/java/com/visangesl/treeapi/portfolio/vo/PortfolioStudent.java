package com.visangesl.treeapi.portfolio.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.campus.vo.CampusVo;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 포트폴리오의 학생정보를 담을 VO
 * @author imac
 *
 */
public class PortfolioStudent extends VSObject {
	private String mbrNm;
	private int postCnt = 0;
	private int likeCnt = 0;
	private int badgeCnt = 0;
	
    private String profileImgPath;
	
	List<CampusVo> campus;

	
	public String getMbrNm() {
		return mbrNm;
	}

	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}

	public int getPostCnt() {
		return postCnt;
	}

	public void setPostCnt(int postCnt) {
		this.postCnt = postCnt;
	}

	public int getLikeCnt() {
		return likeCnt;
	}

	public void setLikeCnt(int likeCnt) {
		this.likeCnt = likeCnt;
	}

	public int getBadgeCnt() {
		return badgeCnt;
	}

	public void setBadgeCnt(int badgeCnt) {
		this.badgeCnt = badgeCnt;
	}

	public List<CampusVo> getCampus() {
		return campus;
	}

	public void setCampus(List<CampusVo> campus) {
		this.campus = campus;
	}

	public String getProfileImgPath() {
        if (profileImgPath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + profileImgPath;
        } else {
            return profileImgPath;
        }
	}

	public void setProfileImgPath(String profileImgPath) {
		this.profileImgPath = profileImgPath;
	}
}
