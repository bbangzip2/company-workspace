package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 포트폴리오를 확인했다는 상태값을 변경하기 위한 처리를 위한 정보를 담은 VO
 * @author imac
 *
 */
public class InquiryPortfolioVo extends VSObject {
	/**
	 * 포트폴리오 순번
	 */
	private int inclsRsltSeq;

	public int getInclsRsltSeq() {
		return inclsRsltSeq;
	}

	public void setInclsRsltSeq(int inclsRsltSeq) {
		this.inclsRsltSeq = inclsRsltSeq;
	}
	
	
}
