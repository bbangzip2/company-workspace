package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 특정 학생이 수강한 특정 클래스의 정보를 조회하는 조건을 담은 VO
 * 
 * @author imac
 *
 */
public class PortfolioStudentClassworkCondition extends VSCondition {
	/**
	 * 학생 ID
	 */
	private String mbrId;
	/**
	 * 클래스 순번
	 */
	private int clsSeq;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public int getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(int clsSeq) {
		this.clsSeq = clsSeq;
	}
}
