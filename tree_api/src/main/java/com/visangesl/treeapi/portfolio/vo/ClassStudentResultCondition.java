package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 포트폴리오 정보를 조회하기위한 조건을 담은 VO
 * @author imac
 *
 */
public class ClassStudentResultCondition extends VSCondition {
	/**
	 * 포트폴리오 순번
	 */
	private int classStudentResultSeq;

	public int getClassStudentResultSeq() {
		return classStudentResultSeq;
	}

	public void setClassStudentResultSeq(int classStudentResultSeq) {
		this.classStudentResultSeq = classStudentResultSeq;
	}
}
