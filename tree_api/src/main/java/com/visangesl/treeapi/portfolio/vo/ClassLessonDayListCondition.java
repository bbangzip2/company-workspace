package com.visangesl.treeapi.portfolio.vo;

/**
 * Lesson 순번으로 해당 차시를 구하기위한 조건 정보를 담는 VO
 * 
 * @author imac
 *
 */
public class ClassLessonDayListCondition extends LastLessonListCondition {
	/**
	 * 레슨 순번
	 */
	private String lessonCd;

	public String getLessonCd() {
		return lessonCd;
	}

	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}
}
