package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 클래스에 해당하는 포트폴리오 정보 조회 
 * 
 * @author imac
 *
 */
public class ClassPortfolioListInfo extends PortfolioListInfo {
	private String mbrId;
	private String mbrNm;
	private String profilePhotopath;
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getMbrNm() {
		return mbrNm;
	}
	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}
	public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + profilePhotopath;
        } else {
            return profilePhotopath;
        }
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}

}
