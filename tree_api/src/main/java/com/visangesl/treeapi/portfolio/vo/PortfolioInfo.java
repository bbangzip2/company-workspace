package com.visangesl.treeapi.portfolio.vo;

import java.util.List;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 포트폴리오 상세정보를 담을 VO
 * 
 * @author imac
 *
 */
public class PortfolioInfo extends VSObject {
	/**
	 * 회원이름
	 */
	private String mbrNm;
	/**
	 * 회원ID
	 */
	private String mbrId;
	/**
	 * 회원 프로필 사진 경로
	 */
	private String profilePhotopath;
	/**
	 * 레슨명
	 */
	private String lessonNm;
	/**
	 * 카드명
	 */
	private String cardNm;
	/**
	 * 포트폴리오 파일 경로
	 */
	private String filePath;
	/**
	 * 포트폴리오 파일 종류
	 */
	private String fileType;
	/**
	 * 좋아요 여부
	 */
	private String likeYn;
	/**
	 * 포트폴리오 등록일시
	 */
	private String regDttm;
	
	/**
	 * 포트폴리오의 뱃지 정보
	 */
	List<BadgeVo> badge;

	public String getMbrNm() {
		return mbrNm;
	}
	public void setMbrNm(String mbrNm) {
		this.mbrNm = mbrNm;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getProfilePhotopath() {
        if (profilePhotopath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + profilePhotopath;
        } else {
            return profilePhotopath;
        }
//		return profilePhotopath;
	}
	public void setProfilePhotopath(String profilePhotopath) {
		this.profilePhotopath = profilePhotopath;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getCardNm() {
		return cardNm;
	}
	public void setCardNm(String cardNm) {
		this.cardNm = cardNm;
	}
	public String getFilePath() {
        if (filePath != null) {
            return TreeProperties.getProperty("tree_cdn_url") + filePath;
        } else {
            return filePath;
        }
//		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getLikeYn() {
		return likeYn;
	}
	public void setLikeYn(String likeYn) {
		this.likeYn = likeYn;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public List<BadgeVo> getBadge() {
		return badge;
	}
	public void setBadge(List<BadgeVo> badge) {
		this.badge = badge;
	}



}
