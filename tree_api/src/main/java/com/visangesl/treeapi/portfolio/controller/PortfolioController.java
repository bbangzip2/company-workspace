package com.visangesl.treeapi.portfolio.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.portfolio.service.PortfolioService;
import com.visangesl.treeapi.portfolio.vo.ClassLessonDayListCondition;
import com.visangesl.treeapi.portfolio.vo.ClassStudentResultCondition;
import com.visangesl.treeapi.portfolio.vo.InquiryPortfolioVo;
import com.visangesl.treeapi.portfolio.vo.LastLessonListCondition;
import com.visangesl.treeapi.portfolio.vo.PortfolioListCondition;
import com.visangesl.treeapi.portfolio.vo.PortfolioStudentClassworkCondition;
import com.visangesl.treeapi.portfolio.vo.PortfolioStudentCondition;
import com.visangesl.treeapi.portfolio.vo.TeacherCondition;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.util.TreeUtil;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 포트폴리오와 관련된 작업을 처리하는 Controller
 * 
 * @author imac
 *
 */
@Controller
public class PortfolioController {

    @Autowired
    private PortfolioService portfolioService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 포트폴리오의 레슨정보를 조회
     * 해당 클래스에서 진행된 레슨 목록을 조회
     * 
     * @param request
     * @param response
     * @param clsSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioLesson", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getPortfolioLesson(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = true, defaultValue = "") String clsSeq)
            throws Exception {
        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {
            LastLessonListCondition condition = new LastLessonListCondition();

            condition.setClsSeq(clsSeq);

            List<VSObject> PortfolioLesson = portfolioService.getPortfolioLesson(condition);
            result.setResult(PortfolioLesson);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 해당 클래스의 해당 레슨에 해당하는 차시 목록을 조회
     * 
     * @param request
     * @param response
     * @param lessonCd 레슨순번
     * @param clsSeq 클래스순번 (현재는 사용되지 않지만 추후 클래스별 차시정보가 달라지는 경우를 고려하여 남겨둠)
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioDay", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getPortfolioDay(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "lessonCd", required = true, defaultValue = "") String lessonCd,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "") String clsSeq)
            throws Exception {
        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {

            ClassLessonDayListCondition condition = new ClassLessonDayListCondition();
            condition.setLessonCd(lessonCd);

            List<VSObject> PortfolioDay = portfolioService.getPortfolioDay(condition);
            result.setResult(PortfolioDay);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 포트폴리오 목록정보를 조회
     * 
     * 다음의 4가지 경우에 대한 포트폴리오 목록을 제공한다.
     * 
     * - 사용자 ID에 해당하는 전체 포트폴리오 목록 [memberId]
     * - 사용자 ID에 해당하면서 특정 클래스 순번에 속하는 포트폴리오 목록 [memberId, clsSeq]
     * - 특정 클래스 순번에 속하는 전체 포트폴리오 목록 [clsSeq]
     * - 특정 클래스 순번에 속하면서 특정 차시 순번에만 해당하는 전체 포트폴리오 목록 [clsSeq, dayNo] 
     * 
     * @param request
     * @param response
     * @param tmpClsSeq 클래스 순번
     * @param tmpDayNo 차시 순번
     * @param mbrId 사용자 ID
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioList", method=RequestMethod.GET)
    public VSResult<Object> getPortfolioList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "clsSeq", required = false) String tmpClsSeq,
            @RequestParam(value = "dayNo", required = false, defaultValue = "") String tmpDayNo,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId)
            throws Exception {

        VSResult<Object> result = new VSResult<Object>();
        PortfolioListCondition condition = new PortfolioListCondition();

        int clsSeq = TreeUtil.isNumber(tmpClsSeq, -1);
        int dayNo = TreeUtil.isNumber(tmpDayNo, -1);
        logger.debug("clsSeq==" + clsSeq);

        try {

            condition.setClsSeq(clsSeq);
            condition.setMbrId(mbrId);
            condition.setDayNo(dayNo);

            List<VSObject> PortfolioList = null;

            if (clsSeq != -1 && mbrId.equals("")) {
                // 특정 클래스 순번에 속하는 전체 포트폴리오 목록
            	// 특정 클래스 순번에 속하면서 특정 차시 순번에만 해당하는 전체 포트폴리오 목록
                PortfolioList = portfolioService.getPortfolioClassList(condition);
            } else {
                // 사용자 ID에 해당하는 전체 포트폴리오 목록
            	// 사용자 ID에 해당하면서 특정 클래스 순번에 속하는 포트폴리오 목록
                PortfolioList = portfolioService.getPortfolioList(condition);
            }

            result.setResult(PortfolioList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }

    /**
     * 포트폴리오 화면에 노출할 학생정보를 조회
     * 
     * @param request
     * @param response
     * @param mbrId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioStudent", method=RequestMethod.GET)
    public VSResult<Object> getPortfolioStudentInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = true) String mbrId)
            throws Exception {
        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            PortfolioStudentCondition condition = new PortfolioStudentCondition();
            condition.setMbrId(mbrId);

            result.setResult(portfolioService.getPortfolioStudentInfo(condition));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }

    /**
     * 포트폴리오 화면에 노출할 학생의 클래스 정보를 조회
     * @param request
     * @param response
     * @param mbrId
     * @param tmpClsSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/portfolioStudentClasswork", method=RequestMethod.GET)
    public VSResult<Object> getPortfolioStudentClasswork(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = true) String mbrId,
            @RequestParam(value = "clsSeq", required = false, defaultValue = "-1") String tmpClsSeq)
            throws Exception {
    	
        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            int clsSeq = Integer.valueOf(tmpClsSeq);
            PortfolioStudentClassworkCondition condition = new PortfolioStudentClassworkCondition();
            condition.setMbrId(mbrId);
            condition.setClsSeq(clsSeq);

            result.setResult(portfolioService.getPortfolioStudentClassInfo(condition));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;

    }

    /**
     * 로그인한 선생이 학생들이 제출한 포트폴리오 중 확인하지않은 포트폴리오 갯수를 조회
     * 
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/getClassPortfolioNOKCount", method=RequestMethod.GET)
    public VSResult<Object> getClassPortfolioNOKCount(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String mbrId = null;

        if (TreeSpringSecurityUtils.getPrincipalAuthorities() != null) {
            mbrId = TreeSpringSecurityUtils.getPrincipalAuthorities().getUsername();
        }

        VSResult<Object> result = new VSResult<Object>();

        try {
            if (mbrId == null) {
                throw new Exception();
            }

            TeacherCondition condition = new TeacherCondition();
            condition.setMbrId(mbrId);

            result.setResult(Integer.valueOf(portfolioService.getClassPortfolioNOKCount(condition)));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

    /**
     * 해당 포트폴리오의 상세 정보를 조회
     * 
     * @param request
     * @param response
     * @param inclsRsltSeq 포트폴리오 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/portfolio/getPortfolioInfo", method=RequestMethod.GET)
    public VSResult<Object> getPortfolioInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "inclsRsltSeq", required = true) int inclsRsltSeq)
            throws Exception {

        VSResult<Object> result = new VSResult<Object>();

        try {
            ClassStudentResultCondition condition = new ClassStudentResultCondition();
            condition.setClassStudentResultSeq(inclsRsltSeq);

            result.setResult(portfolioService.getPortfolioInfo(condition));

            if (result.getResult() != null) {
            	// 포트폴리오 정보를 조회한 경우 포트폴리오 확인여부를 '확인'으로 변경하는 처리
                InquiryPortfolioVo inquiryPortfolio = new InquiryPortfolioVo();
                inquiryPortfolio.setInclsRsltSeq(inclsRsltSeq);
                portfolioService.inquiryPortfolio(inquiryPortfolio);
            }

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
        }

        return result;
    }

}