package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSCondition;

/**
 * 특정 선생을 기준으로 확인하지 않는 학생의 포트폴리오를 조회하기위한 조건을 담은 VO
 * 
 * @author imac
 *
 */
public class TeacherCondition extends VSCondition {
	/**
	 * 회원 ID
	 */
	private String mbrId;

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	
	
}
