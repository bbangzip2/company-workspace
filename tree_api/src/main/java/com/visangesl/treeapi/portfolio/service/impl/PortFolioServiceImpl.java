package com.visangesl.treeapi.portfolio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mapper.PortFolioMapper;
import com.visangesl.treeapi.portfolio.service.PortfolioService;
import com.visangesl.treeapi.portfolio.vo.ClassLessonDayListCondition;
import com.visangesl.treeapi.portfolio.vo.LastLessonListCondition;
import com.visangesl.treeapi.portfolio.vo.PortfolioVo;

/**
 * 포트폴리오 관련정보를 처리
 * 
 * @author imac
 *
 */
@Service
public class PortFolioServiceImpl implements PortfolioService{
    @Autowired
    PortFolioMapper portfolioMapper;

    /**
     * 해당 클래스에서 진행된 레슨 목록을 조회
     */
	@Override
	public List<VSObject> getPortfolioLesson(LastLessonListCondition condition) {
		 return portfolioMapper.getPortfolioLesson(condition);
	}
	
	/**
	 * 해당 클래스의 해당 레슨에 해당하는 차시 목록을 조회
	 */
	@Override
	public List<VSObject> getPortfolioDay(ClassLessonDayListCondition condition) {
		 return portfolioMapper.getPortfolioDay(condition);
	}	

	/**
	 * 해당 사용자와 관련된 포트폴리오 목록을 조회
	 * 
     * - 사용자 ID에 해당하는 전체 포트폴리오 목록 [memberId]
     * - 사용자 ID에 해당하면서 특정 클래스 순번에 속하는 포트폴리오 목록 [memberId, clsSeq]
	 */
	@Override
	public List<VSObject> getPortfolioList(VSCondition condition) {
	    return portfolioMapper.getPortfolioList(condition);
	}
	
	/**
	 * 해당 클래스와 관련된 포트폴리오 목록을 조회
	 * 
     * - 특정 클래스 순번에 속하는 전체 포트폴리오 목록 [clsSeq]
     * - 특정 클래스 순번에 속하면서 특정 차시 순번에만 해당하는 전체 포트폴리오 목록 [clsSeq, dayNo] 
	 */
	@Override
	public List<VSObject> getPortfolioClassList(VSCondition condition) {
	    return portfolioMapper.getPortfolioClassList(condition);
	}
	
	/**
	 * 포트폴리오 화면에 노출할 학생정보를 조회
	 */
	@Override
	public VSObject getPortfolioStudentInfo(VSCondition condition) {
		return portfolioMapper.getPortfolioStudentInfo(condition);
	}
	
	/**
	 * 포트폴리오 화면에 노출할 학생의 클래스 정보를 조회
	 */
	@Override
	public VSObject getPortfolioStudentClassInfo(VSCondition condition) {
		return portfolioMapper.getPortfolioStudentClassInfo(condition);
	}	

	/**
	 * 특정 선생이 확인하지 않은 학생들의 포트폴리오 수를 조회
	 */
	@Override
	public int getClassPortfolioNOKCount(VSCondition condition) {
		return portfolioMapper.getClassPortfolioNOKCount(condition);
	}

	/**
	 * 포트폴리오 정보 조회
	 */
	@Override
	public VSObject getPortfolioInfo(VSCondition condition) {
		return portfolioMapper.getPortfolioInfo(condition);
	}
	
	/**
	 * 포트폴리오 확인 여부를 '확인' 상태로 변경 처리
	 */
	@Override
	public int inquiryPortfolio(VSObject object) {
		return portfolioMapper.inquiryPortfolio(object);
	}
	
	/**
	 * 포트폴리오 등록
	 */
	@Override
	@Transactional
	public int insertPortfolioInfo(PortfolioVo portfolioVo)throws Exception{
		return portfolioMapper.insertPortfolioInfo(portfolioVo);
	}
}
