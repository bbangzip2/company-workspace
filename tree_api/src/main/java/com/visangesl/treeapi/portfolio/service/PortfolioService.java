package com.visangesl.treeapi.portfolio.service;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.portfolio.vo.ClassLessonDayListCondition;
import com.visangesl.treeapi.portfolio.vo.LastLessonListCondition;
import com.visangesl.treeapi.portfolio.vo.PortfolioVo;

/**
 * 포트폴리오 관련 처리
 * 
 * @author imac
 *
 */
public interface PortfolioService {
	/**
	 * 해당 클래스에서 진행된 레슨 목록을 조회
	 * 
	 * @param condition
	 * @return
	 */
	public List<VSObject> getPortfolioLesson(LastLessonListCondition condition);

	/**
	 * 해당 클래스의 해당 레슨에 해당하는 차시 목록을 조회
	 * 
	 * @param condition
	 * @return
	 */
	public List<VSObject> getPortfolioDay(ClassLessonDayListCondition condition);

	/**
	 * 해당 사용자와 관련된 포트폴리오 목록을 조회
	 * 
     * - 사용자 ID에 해당하는 전체 포트폴리오 목록 [memberId]
     * - 사용자 ID에 해당하면서 특정 클래스 순번에 속하는 포트폴리오 목록 [memberId, clsSeq]
     * 
	 * @param condition
	 * @return
	 */
	public List<VSObject> getPortfolioList(VSCondition condition);
	
	/**
	 * 해당 클래스와 관련된 포트폴리오 목록을 조회
	 * 
     * - 특정 클래스 순번에 속하는 전체 포트폴리오 목록 [clsSeq]
     * - 특정 클래스 순번에 속하면서 특정 차시 순번에만 해당하는 전체 포트폴리오 목록 [clsSeq, dayNo] 
     * 
	 * @param condition
	 * @return
	 */
	public List<VSObject> getPortfolioClassList(VSCondition condition);
	
	/**
	 * 포트폴리오 화면에 노출할 학생 정보를 조회
	 * 
	 * @param condition
	 * @return
	 */
	public VSObject getPortfolioStudentInfo(VSCondition condition);
	
	/**
	 * 포트폴리오 화면에 노출할 학생의 클래스 정보를 조회
	 * 
	 * @param condition
	 * @return
	 */
	public VSObject getPortfolioStudentClassInfo(VSCondition condition);
	
	/**
	 * 특정 선생이 확인하지 않은 학생들의 포트폴리오 수를 조회
	 * 
	 * @param condition
	 * @return
	 */
	public int getClassPortfolioNOKCount(VSCondition condition);
	
	/**
	 * 포트폴리오 정보 조회
	 * 
	 * @param condition
	 * @return
	 */
	public VSObject getPortfolioInfo(VSCondition condition);
	
	/**
	 * 포트폴리오 확인여부를 '확인' 상태로 변경처리
	 * 
	 * @param object
	 * @return
	 */
	public int inquiryPortfolio(VSObject object);
	
	/**
	 * 포트폴리오 등록
	 * 
	 * @param portfolioVo
	 * @return
	 * @throws Exception
	 */
	public int insertPortfolioInfo(PortfolioVo portfolioVo)throws Exception;
}
