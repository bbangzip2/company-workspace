package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.constant.Tree_Constant;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * 포트폴리오 정보를 담을 VO
 * 
 * @author imac
 *
 */
public class PortfolioListInfo extends VSObject {
	private int inclsRsltSeq;
	private String lessonNm;
	private String activityNm;
	private String inclsRsltThmb;
	private String inclsRsltBigThmb;
	private String inclsYmd;
	private String fileType;
	private int likeCnt;
	private int cmmtCnt;
	private String newYn;
	private int badgeSeq;
	private int badgeCd;
	private String badgeNm;


	public int getInclsRsltSeq() {
		return inclsRsltSeq;
	}
	public void setInclsRsltSeq(int inclsRsltSeq) {
		this.inclsRsltSeq = inclsRsltSeq;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getActivityNm() {
		return activityNm;
	}
	public void setActivityNm(String activityNm) {
		this.activityNm = activityNm;
	}
	public String getInclsRsltThmb() {
        if (inclsRsltThmb != null) {
            return TreeProperties.getProperty("tree_cdn_url") + inclsRsltThmb;
        } else {
            return inclsRsltThmb;
        }
//		return inclsRsltThmb;
	}
	public void setInclsRsltThmb(String inclsRsltThmb) {
		this.inclsRsltThmb = inclsRsltThmb;
	}


	public String getInclsRsltBigThmb() {
        if (inclsRsltBigThmb != null) {
            return TreeProperties.getProperty("tree_cdn_url") + inclsRsltBigThmb;
        } else {
            return inclsRsltBigThmb;
        }
//	    return inclsRsltBigThmb;
	}
	public void setInclsRsltBigThmb(String inclsRsltBigThmb) {
		this.inclsRsltBigThmb = inclsRsltBigThmb;
	}
	public String getInclsYmd() {
		return inclsYmd;
	}
	public void setInclsYmd(String inclsYmd) {
		this.inclsYmd = inclsYmd;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public int getLikeCnt() {
		return likeCnt;
	}
	public void setLikeCnt(int likeCnt) {
		this.likeCnt = likeCnt;
	}
	public int getCmmtCnt() {
		return cmmtCnt;
	}
	public void setCmmtCnt(int cmmtCnt) {
		this.cmmtCnt = cmmtCnt;
	}
	public String getNewYn() {
		return newYn;
	}
	public void setNewYn(String newYn) {
		this.newYn = newYn;
	}
	public int getBadgeSeq() {
		return badgeSeq;
	}
	public void setBadgeSeq(int badgeSeq) {
		this.badgeSeq = badgeSeq;
	}
	public int getBadgeCd() {
		return badgeCd;
	}
	public void setBadgeCd(int badgeCd) {
		this.badgeCd = badgeCd;
	}
	public String getBadgeNm() {
		return badgeNm;
	}
	public void setBadgeNm(String badgeNm) {
		this.badgeNm = badgeNm;
	}
}
