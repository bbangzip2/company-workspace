package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.tree.vo.VSObject;

/**
 * 포트폴리오 등록시 사용하는 VO
 * 
 * @author imac
 *
 */
public class PortfolioVo extends VSObject {
	/**
	 * 클래스 순번
	 */
    private String clsSeq;
    /**
     * 포트폴리오 등록처리 후 생성된 시퀀스 번호를 담는 변수
     */
    private String inclsRsltSeq;
    /**
     * 회원 ID
     */
    private String mbrId;
    /**
     * 카드 순번
     */
    private String prodSeq;
    /**
     * 차시 순번
     */
    private String clsSchdlSeq;

	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getInclsRsltSeq() {
		return inclsRsltSeq;
	}
	public void setInclsRsltSeq(String inclsRsltSeq) {
		this.inclsRsltSeq = inclsRsltSeq;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getClsSchdlSeq() {
		return clsSchdlSeq;
	}
	public void setClsSchdlSeq(String clsSchdlSeq) {
		this.clsSchdlSeq = clsSchdlSeq;
	}






}
