package com.visangesl.treeapi.portfolio.vo;

import com.visangesl.treeapi.vo.VSListCondition;

/**
 * 해당 클래스에서 진행된 레슨 목록을 조회하기위한 VO
 * 
 * @author imac
 *
 */
public class LastLessonListCondition extends VSListCondition {
	/**
	 * 클래스 순번
	 */
	private String clsSeq;

	public String getClsSeq() {
		return clsSeq;
	}

	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	
	
}
