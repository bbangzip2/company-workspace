package com.visangesl.treeapi.bookshelf.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.bookshelf.service.BookshelfService;
import com.visangesl.treeapi.bookshelf.vo.BookShelfLessonInfoVo;
import com.visangesl.treeapi.bookshelf.vo.BookshelfCondition;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 북쉘프 정보를 처리하는 Controller
 *
 * @author chang
 *
 */

@Controller
public class BookshelfController {

    @Autowired
    private BookshelfService bookshelfService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    VSResult<Object> result = new VSResult<Object>();
    /**
     * 북 정보 조회
     * @param request
     * @param response
     * @param mbrId  내 정보로 북 정보를 가져옴
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/bookshelf/bookList", method=RequestMethod.POST)
    public VSResult<List<VSObject>> getBookList(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId)
            throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        try {
                BookshelfCondition condition = new BookshelfCondition();
                condition.setMemberId(mbrId);
                List<VSObject> bookList = bookshelfService.getBookList(condition);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
                result.setResult(bookList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
       	 	logger.debug(e.toString());
        }

        return result;
    }
    /**
     * 북 별 레슨 조회
     * @param request
     * @param response
     * @param mbrId   memberID
     * @param prodSeq 상품 순번
     * @return
     * @throws Exception
     */

    @ResponseBody
    @RequestMapping(value = "/bookshelf/lessonList", method=RequestMethod.POST)
    public VSResult<List<VSObject>> getLessonList(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "prodSeq", required = false, defaultValue = "") String prodSeq)
            throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {
            BookshelfCondition condition = new BookshelfCondition();
            condition.setMemberId(mbrId);
            condition.setProdSeq(prodSeq);

            List<VSObject> bookLessonList = bookshelfService.getLessonList(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(bookLessonList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            logger.debug(e.toString());
        }

        return result;
    }


    /**
     * 북 레슨 정보
     * @param request
     * @param response
     * @param prodSeq  // 상품 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/bookshelf/bookLessonInfo", method=RequestMethod.POST)
    public VSResult<BookShelfLessonInfoVo> getBookLessonInfo(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "code", required = false, defaultValue = "") String prodSeq)
            throws Exception {

        VSResult<BookShelfLessonInfoVo> result = new VSResult<BookShelfLessonInfoVo>();

        try {
            BookshelfCondition condition = new BookshelfCondition();
            condition.setProdSeq(prodSeq);

            BookShelfLessonInfoVo bookshelfVo = (BookShelfLessonInfoVo) bookshelfService.getLessonInfo(condition);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(bookshelfVo);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            logger.debug(e.toString());
        }

        return result;

    }

    /**
     * 북 정보 조회
     * @param request
     * @param response
     * @param prodSeq   상품 순번
     * @param keyword   조회 키워드
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/bookshelf/bookshelfSearch", method=RequestMethod.GET)
    public VSResult<List<VSObject>> getBookshelfSearch(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "code", required = false, defaultValue = "") String prodSeq,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword)
            throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        try {


            BookshelfCondition condition = new BookshelfCondition();
            condition.setProdSeq(prodSeq);
            condition.setKeyword(keyword);

            List<VSObject> bookLessonList = bookshelfService
                    .getSearchLessonList(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(bookLessonList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            logger.debug(e.toString());
        }

        return result;

    }
    
    /**
     * book shelf 에서 레슨 선택시 보여질 현재 진행중인 클래스 목록 조회 (2014-07-16 추가함. )
     * @param request
     * @param response
     * @param mbrId
     * @param prodSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/bookshelf/getBookshelfClassList", method=RequestMethod.POST)
    public VSResult getBookshelfClassList(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "prodSeq", required = false, defaultValue = "") String prodSeq)
            throws Exception {

        VSResult result = new VSResult();

        String mbrGrade = "";

        try {
            BookshelfCondition condition = new BookshelfCondition();
            condition.setMemberId(mbrId);
            condition.setProdSeq(prodSeq);


            List<VSObject> bookClassList = bookshelfService.getBookshelfClassList(condition);

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
            result.setResult(bookClassList);
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(handler.getCode());
            result.setMessage(handler.getMessage());
            e.printStackTrace();
        }

        return result;
    }
    
    
    
    
}
