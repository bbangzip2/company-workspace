package com.visangesl.treeapi.bookshelf.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * BookShelf 레슨정보를 가져오는 VO
 * 
 * @author chang
 *
 */

public class BookShelfLessonInfoVo extends VSObject {


	private String prodSeq;      //상품순번
	private String prodTitle;    //상품명
	private String thmbPath;     //썸네일 경로
	private String regDttm;      //등록일시
	private String modDttm;      //수정일시
	private String comment;      //코멘트
	private String filePath;     //파일경로
		
	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	
/*	if (thmbPath == null) {
        return thmbPath;
    } else {
        return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
    }*/
	
	public String getThmbPath() {
		if (thmbPath == null) {
	        return thmbPath;
	    } else {
	        return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
	    }
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getModDttm() {
		return modDttm;
	}
	public void setModDttm(String modDttm) {
		this.modDttm = modDttm;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
