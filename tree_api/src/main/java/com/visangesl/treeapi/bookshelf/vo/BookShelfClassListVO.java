package com.visangesl.treeapi.bookshelf.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.vo.VSListCondition;

/**
 * BookShelf 클래스 목록을 받기 위한 VO
 * @author chang
  */
public class BookShelfClassListVO extends VSListCondition {

    private String clsSeq;
    private String clsNm;
    
    
	public String getClsSeq() {
		return clsSeq;
	}
	public void setClsSeq(String clsSeq) {
		this.clsSeq = clsSeq;
	}
	public String getClsNm() {
		return clsNm;
	}
	public void setClsNm(String clsNm) {
		this.clsNm = clsNm;
	}
}
