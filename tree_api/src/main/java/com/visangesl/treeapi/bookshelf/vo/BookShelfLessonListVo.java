package com.visangesl.treeapi.bookshelf.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.property.TreeProperties;

/**
 * BookShelfLesson 리스트 정보를 담는 VO
 * 
 * @author chang
 *
 */

public class BookShelfLessonListVo extends VSObject {

	private String inLessonYn;    //레슨 사용 유/무
	private String lessonNm;      //레슨명
	private String dayNoCd;       //차시 
	private String thmbPath;      //썸네일 경로 
	private String lessonCd;      //레슨 순번  
	
	public String getInLessonYn() {
		return inLessonYn;
	}
	public void setInLessonYn(String inLessonYn) {
		this.inLessonYn = inLessonYn;
	}
	public String getLessonNm() {
		return lessonNm;
	}
	public void setLessonNm(String lessonNm) {
		this.lessonNm = lessonNm;
	}
	public String getDayNoCd() {
		return dayNoCd;
	}
	public void setDayNoCd(String dayNoCd) {
		this.dayNoCd = dayNoCd;
	}
	public String getThmbPath() {
		if (thmbPath == null) {
	        return thmbPath;
	    } else {
	        return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
	    }
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getLessonCd() {
		return lessonCd;
	}
	public void setLessonCd(String lessonCd) {
		this.lessonCd = lessonCd;
	}
	
}
