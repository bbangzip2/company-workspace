package com.visangesl.treeapi.bookshelf.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.bookshelf.service.BookshelfService;
import com.visangesl.treeapi.bookshelf.vo.BookshelfCondition;
import com.visangesl.treeapi.mapper.BookshelfMapper;

/**
 * BookShelf 출력  class
 * 
 * @author chang
 *
 */


@Service
public class BookshelfServiceImpl implements BookshelfService {

	@Autowired
	BookshelfMapper bookshelfMapper;

    /**
     *  북 목록 조회  
     */
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getBookList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getBookList(dataCondition);
    }

	/**
     *  북 레슨 목록 조회  
     */
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getLessonList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getLessonList(dataCondition);
    }

	/**
     *  북 레슨 목록 검색  
     */
	
	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getSearchLessonList(VSCondition dataCondition) throws Exception {
        return bookshelfMapper.getSearchLessonList(dataCondition);
    }

	/**
     *  북 레슨 정보 출력  
     */
	
    @Override
    public VSObject getLessonInfo(BookshelfCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return bookshelfMapper.getLessonInfo(condition);
    }

	/**
     *  북 에 연결된 클래스 정보 출력   
	 * @throws Exception 
     */
    
	@Override
	public List<VSObject> getBookshelfClassList(BookshelfCondition condition) throws Exception {
		return bookshelfMapper.getBookshelfClassList(condition);
	}
}
