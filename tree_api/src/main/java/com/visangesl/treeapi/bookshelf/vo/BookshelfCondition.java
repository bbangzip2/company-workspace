package com.visangesl.treeapi.bookshelf.vo;

import com.visangesl.treeapi.vo.VSListCondition;

/**
 * BookShelf 목록을 받기 위한 VO
 * @author chang
 *
 */

public class BookshelfCondition extends VSListCondition {

	private String memberId;   // 회원ID
	private String prodSeq;    // 상품순번 
    private String code;       // 상품코드
    private String keyword;    // 조회코드
    
    public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
