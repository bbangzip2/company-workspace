package com.visangesl.treeapi.bookshelf.service;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.bookshelf.vo.BookshelfCondition;

/**
 * BookShelf 작업을 처리하는 Service interface
 * 
 * @author chang
 *
 */

public interface BookshelfService {
	/**
	 * 책의 데이터 리스트를 가져온다.
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getBookList(VSCondition dataCondition) throws Exception;

	/**
	 * 책의 Lesson 데이터 리스트 조회.
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getLessonList(VSCondition dataCondition) throws Exception;

	/**
	 * 특정 조건의 Lesson 데이터 리스트 조회.
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getSearchLessonList(VSCondition dataCondition) throws Exception;

	/**
	 * Lesson 정보를 가져 온다
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public VSObject getLessonInfo(BookshelfCondition condition) throws Exception;

	/**
	 * BookShelf 에서 클래스 정보를 가져와 연결한다.
	 * @param dataCondition
	 * @return
	 * @throws Exception
	 */
	public List<VSObject> getBookshelfClassList(BookshelfCondition condition) throws Exception;

}
