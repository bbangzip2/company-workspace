package com.visangesl.treeapi.bookshelf.vo;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.property.TreeProperties;
/**
 * BookShelf 리스트 정보를 담는 VO
 *
 * @author chang
 *
 */


public class BookshelfListVo extends VSObject {

	private String prodSeq;     //상품순번
    private String prodTitle;   //상품명
    private String thmbPath;    //썸네일 경로
    private String regDttm;     //등록일시
    private String modDttm;     //수정일시

	public String getProdSeq() {
		return prodSeq;
	}
	public void setProdSeq(String prodSeq) {
		this.prodSeq = prodSeq;
	}
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getThmbPath() {
        if (thmbPath == null) {
            return thmbPath;
        } else {
            return TreeProperties.getProperty("tree_cdn_url") + thmbPath;
        }
	}
	public void setThmbPath(String thmbPath) {
		this.thmbPath = thmbPath;
	}
	public String getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}
	public String getModDttm() {
		return modDttm;
	}
	public void setModDttm(String modDttm) {
		this.modDttm = modDttm;
	}
}
