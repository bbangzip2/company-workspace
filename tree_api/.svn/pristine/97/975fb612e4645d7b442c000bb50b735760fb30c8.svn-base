package com.visangesl.treeapi.comment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.comment.service.CommentService;
import com.visangesl.treeapi.comment.vo.LikeCondition;
import com.visangesl.treeapi.comment.vo.LikeVo;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.exception.TreeRuntimeException;
import com.visangesl.treeapi.mapper.CommentMapper;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	CommentMapper commentMapper;
	
	/**
	 * 코멘트 등록
	 */
	@Override
	public VSResult<VSObject> addComment(VSObject vsObject) {
		// TODO Auto-generated method stub
		VSResult<VSObject> result = new VSResult<VSObject>();
		
		int affectedRows = commentMapper.addComment(vsObject);
		
		if (affectedRows > 0) {
			result.setCode(TreeProperties.getProperty("error.success.code"));
			result.setMessage("댓글 쓰기 성공");
		} else {
			result.setCode(TreeProperties.getProperty("error.etc.code"));
			result.setMessage("댓글 쓰기 실패");
		}
		
		return result;
	}

	/**
	 * 코멘트 삭제
	 */
	@Override
	public VSResult<VSObject> removeComment(VSCondition condition) {
		// TODO Auto-generated method stub
		VSResult<VSObject> result = new VSResult<VSObject>();
		
		int affectedRows = commentMapper.removeComment(condition);
		
		if (affectedRows > 0) {
			result.setCode(TreeProperties.getProperty("error.success.code"));
			result.setMessage("댓글 삭제 성공");
		} else {
			result.setCode(TreeProperties.getProperty("error.etc.code"));
			result.setMessage("댓글 삭제 실패");
		}
		
		return result;
	}

	/**
	 * 코멘트 리스트 조회
	 */
	@Override
	public List<VSObject> getContentComment(VSCondition condition) throws Exception {
		// TODO Auto-generated method stub
		return commentMapper.getContentComment(condition);
	}

	/**
	 * 좋아요 토글
	 */
	@Override
	public VSResult<Object> toggleLike(VSCondition condition) {
		VSResult<Object> result = new VSResult<Object>();
		
		try {
		    // 좋아요가 있는지 확인한다.
			String existDoLike = commentMapper.existDoLike(condition);
			if (existDoLike != null && existDoLike.equals("Y")) {
				//현재 do like 상태 그러므로 undo like처리
				int affectedRows = commentMapper.undoLike(condition);
				
				if (affectedRows > 0) {
					result.setCode(TreeProperties.getProperty("error.success.code"));
					result.setMessage("LIKE 취소 성공");
					result.setResult("N");
				} else {
				    String code = TreeProperties.getProperty("error.fail.code");
				    String msg  = "LIKE 취소 처리 실패";
					throw new TreeRuntimeException(code, msg);
				}

			} else {
				//현재 undo like 상태 그러므로 do like 처리
			    LikeCondition likeCondition = (LikeCondition)condition;

			    LikeVo like = new LikeVo();
				like.setContentId(likeCondition.getContentId());
				like.setContentType(likeCondition.getContentType());
				like.setMbrId(likeCondition.getMbrId());
				
				int affectedRows = commentMapper.doLike(like);
				
				if (affectedRows > 0) {
					result.setCode(TreeProperties.getProperty("error.success.code"));
					result.setMessage("LIKE 성공");
					result.setResult("Y");
				} else {
				    String code = TreeProperties.getProperty("error.fail.code");
                    String msg  = "LIKE 처리 실패";
                    throw new TreeRuntimeException(code, msg);
				}

			}			
		} catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
            result.setCode(TreeProperties.getProperty("error.etc.code"));
            result.setMessage(handler.getMessage());
            e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 좋아요 조회
	 */
	@Override
	public String existDoLike(VSCondition condition) {
		return commentMapper.existDoLike(condition);
	}
}
