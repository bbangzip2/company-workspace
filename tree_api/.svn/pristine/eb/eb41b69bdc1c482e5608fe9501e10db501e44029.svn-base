package com.visangesl.treeapi.authoring.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.visangesl.treeapi.authoring.service.AuthoringCardCopyService;
import com.visangesl.treeapi.authoring.vo.AuthoringCardCopy;

@Service
public class AuthoringCardCopyServiceImpl implements AuthoringCardCopyService {

    /**
     * 저작도구 생성 카드를 복사 한다.
     */
    @Override
    public boolean copyAuthoringCard(AuthoringCardCopy condition) throws Exception {
        // TODO Auto-generated method stub
        boolean result = false;

        // 복사할 원본 폴더
        File asIsDir = new File(condition.getFilePath());
        if (!asIsDir.exists()) {
            asIsDir.mkdirs();
        }

        // 복사될 폴더
        File toBeDir = new File(condition.getToBePath());
        if (!toBeDir.exists()) {
            toBeDir.mkdirs();
        }

        // 폴더 복사 실행
        result = fileCopy(asIsDir, toBeDir);


        // 카드 복사 완료 후 썸네일 파일 복사 진행
        if (result && condition.getThmbPath() != null) {

            // 복사할 원본 폴더
            asIsDir = new File(condition.getThmbPath());
            if (!asIsDir.exists()) {
                asIsDir.mkdirs();
            }

            // 복사될 폴더
            toBeDir = new File(condition.getToBeThmbPath());
            if (!toBeDir.exists()) {
                toBeDir.mkdirs();
            }

            result = fileCopy(asIsDir, toBeDir);
        }

        return result;
    }


    /**
     * 폴더/파일 복사
     * @param asIsDir
     * @param toBeDir
     * @return
     */
    public boolean fileCopy(File asIsDir, File toBeDir) {

        boolean result = true;

        // 폴더 복사
        File[] ff = asIsDir.listFiles();
        for (File file : ff) {
            File temp = new File(toBeDir.getAbsolutePath() + File.separator
                    + file.getName());
            if (file.isDirectory()) {
                temp.mkdir();
                fileCopy(file, temp);
            } else {
                FileInputStream fis = null;
                FileOutputStream fos = null;
                try {
                    fis = new FileInputStream(file);
                    fos = new FileOutputStream(temp);
                    byte[] b = new byte[4096];
                    int cnt = 0;
                    while ((cnt = fis.read(b)) != -1) {
                        fos.write(b, 0, cnt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    result = false;
                    break;
                } finally {
                    try {
                        fis.close();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // 예외 발생 시 복사 진행된 폴더를 삭제
        if (!result) {
            deleteDir(toBeDir);
        }
        return result;
    }


    /**
     * 폴더/파일 삭제
     * @param toBeDir
     * @return
     */
    public boolean deleteDir(File toBeDir) {

        if (!toBeDir.exists()) {
            return false;
        }

        File[] files = toBeDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                file.delete();
            }
        }

        return toBeDir.delete();
    }
}
