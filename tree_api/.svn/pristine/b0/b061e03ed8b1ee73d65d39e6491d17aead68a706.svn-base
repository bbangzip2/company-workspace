<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//ibatis.apache.org//DTD Mapper 3.0//EN" "http://ibatis.apache.org/dtd/ibatis-3-mapper.dtd">
<mapper namespace="com.visangesl.treeapi.mapper.ContentsMapper">

    <!-- 레퍼런스 자료 목록   -->
    <select id="getDataList" parameterType="com.visangesl.treeapi.contents.vo.RefCondition" resultType="com.visangesl.treeapi.contents.vo.RefVo">
        SELECT
              PROD_SEQ as refSeq
            , PROD_TITLE as refTitle
            , THMB_PATH as thmbPath
            , FILE_PATH as filePath
            , DTL_CNTT as dtlCntt
            , PROD_TYPE as refType
            , CURRI_CD as curriCd
         FROM PRODINFO
         WHERE PROD_SECT =  'R'
          AND USE_YN = 'Y'
          <if test='prodType != ""'>
            AND PROD_TYPE = #{prodType} 
          </if>
          AND CURRI_CD = (SELECT DAYNO_CD FROM PRODINFO WHERE PROD_SEQ = #{prodSeq})
          ORDER BY PROD_TYPE ASC
    </select>
    
    <!-- 레퍼런스 자료 총 숫자  -->
    <select id="getReferenceListCnt" parameterType="com.visangesl.treeapi.contents.vo.RefCondition" resultType="string">
        SELECT
              count(*) as CNT
         FROM PRODINFO
         WHERE PROD_SECT =  'R'
          AND USE_YN = 'Y'
          AND CURRI_CD = (SELECT DAYNO_CD FROM PRODINFO WHERE PROD_SEQ = #{prodSeq})
    </select>
        
    <!-- 레퍼런스 자료 타입별 데이터 유무 목록  -->
    <select id="getReferenceTypeList" parameterType="com.visangesl.treeapi.contents.vo.RefCondition" resultType="com.visangesl.treeapi.contents.vo.RefVo">
        SELECT 
              AA.refType
            , sum(AA.CNT) AS refCnt
            from (
                    SELECT
                      PROD_TYPE refType
                     , 1 AS CNT
                     , PROD_TYPE
             FROM PRODINFO
             WHERE PROD_SECT =  'R'
              AND USE_YN = 'Y'
              AND CURRI_CD = (SELECT DAYNO_CD FROM PRODINFO WHERE PROD_SEQ = #{prodSeq})
            )AA GROUP BY PROD_TYPE
            
    </select>
            
    
    <!-- 해당 컨텐츠의 상세 데이터 조회.  -->
    <select id="getContentsInfoData" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT
            PROD_SEQ AS prodSeq,
            PROD_TITLE AS prodTitle,
            THMB_PATH AS thmbPath,
            USE_YN AS useYn,
            DTL_CNTT AS dtlCntt,
            BOOK_CD AS bookCd,
            LESSON_CD AS lessonCd,
            DAYNO_CD AS daynoCd,
            SORT AS sort,
            PROD_SECT AS prodSect,
            PROD_TYPE AS prodType,
            CURRI_CD AS curriCd,
            FILE_PATH AS filePath,
            CARD_PATH AS cardPath
            ,CARD_FULL_PATH AS cardFullPath
            ,BOOK_PATH AS bookPath
            ,LESSON_PATH AS lessonPath
            , TIME as time
         FROM PRODINFO
         WHERE PROD_SEQ = #{prodSeq}
    </select>
    


    <select id="getNewContentsInfo" parameterType="com.visangesl.treeapi.contents.vo.ContentsVo" resultType="com.visangesl.treeapi.contents.vo.ContentsDownloadVo">
        SELECT
            CV.MBR_ID AS mbrId
            ,CV.VER AS version
            ,PFI.FILE_PATH AS filePath
            ,PFI.FILE_NAME AS fileName
            ,PFI.FILE_TYPE AS prodType
            ,PFI.PROD_SEQ AS prodSeq
            ,PI.BOOK_CD AS bookCd
            ,PFI.PROD_SEQ AS lessonCd
        FROM CONTENT_VER CV INNER JOIN PROD_FILE_INFO PFI ON CV.MBR_ID = PFI.MBR_ID AND PFI.PROD_SEQ=#{lessonCd}
        INNER JOIN PRODINFO PI ON PFI.PROD_SEQ = PI.PROD_SEQ
        <where>
            <if test='mbrGrade == "MG004"'>
                CV.MBR_ID = #{mbrId} AND PFI.MBR_ID = #{mbrId}
            </if>
            <if test='mbrGrade == "MG005"'>
                CV.MBR_ID = (
                     SELECT
			          CTI.MBR_ID
			        FROM CLS_STDNT_INFO CSI INNER JOIN CLS_INFO CI ON CI.CLS_SEQ = CSI.CLS_SEQ
			        INNER JOIN PROG_ITEM P ON CI.CURRI_NO = P.PROG_SEQ 
			        INNER JOIN PRODINFO PI ON PI.PROD_SEQ = #{lessonCd} AND P.PROD_SEQ = PI.BOOK_CD 
			        INNER JOIN CLS_TCHR_INFO CTI ON CTI.CLS_SEQ = CSI.CLS_SEQ
                    WHERE CSI.MBR_ID = #{mbrId}
                )
            </if>
        AND CV.CONTENT_SEQ = #{lessonCd}
        </where>
    </select>

    <!-- 레슨 리스트 -->
    <select id="getLessonList" parameterType="com.visangesl.treeapi.contents.vo.ContentsVo" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT
            DISTINCT(LESSON_CD) AS lessonCd
        FROM PRODINFO
        WHERE BOOK_CD = #{bookCd}
        AND (LESSON_CD IS NOT NULL AND LESSON_CD != '')
        AND (DAYNO_CD IS NULL OR DAYNO_CD = '')
        AND USE_YN = 'Y'
        ORDER BY ABS(LESSON_CD) ASC
    </select>

    <!-- 해당 Lesson의 Day 데이터 리스트 조회. -->
    <select id="getDayDataList" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT
            dayInfo.PROD_SEQ AS prodSeq,
            dayInfo.PROD_TITLE AS prodTitle,
            (SELECT PROD_TITLE FROM PRODINFO WHERE PROD_SEQ = #{lessonCd}) as lessonNm
        FROM vs_tree.CONTENT_STRUC lessonStruc
        INNER JOIN vs_tree.PRODINFO dayInfo
        ON lessonStruc.CONTENT_SEQ = dayInfo.PROD_SEQ
        <where>
            <if test='mbrGrade == "MG004"'>
              AND  lessonStruc.MBR_ID = #{mbrId}
            </if>
            <if test='mbrGrade == "MG005"'>
              <!-- AND  lessonStruc.MBR_ID = (SELECT MBR_ID FROM CLS_TCHR_INFO WHERE CLS_SEQ = (SELECT CLS_SEQ FROM vs_tree.CLS_STDNT_INFO WHERE MBR_ID = #{mbrId}))  -->
              AND  lessonStruc.MBR_ID = #{tMbrId}
            </if>
            AND lessonStruc.UPPER_CONTENT_SEQ = #{lessonCd};
        </where>
    </select>
    
    <!-- 해당 차시의 카드맵 데이터 리스트 조회. -->
    <select id="getCardMapDataList" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT 
              cardInfo.PROD_SEQ AS prodSeq
            , cardInfo.PROD_TITLE AS prodTitle
            , cardInfo.THMB_PATH AS thmbPath
            , cardInfo.USE_YN AS useYn
            , cardInfo.DTL_CNTT AS dtlCntt
            , cardInfo.BOOK_CD AS bookCd
            , cardInfo.LESSON_CD AS lessonCd
            , cardInfo.DAYNO_CD AS daynoCd
            , cardInfo.MDL_CD AS mdlCd
            , cardInfo.CURRI_CD AS curriCd
            , cardInfo.SORT AS sort
            , cardInfo.PROD_SECT AS prodSect
            , cardInfo.PROD_TYPE AS prodType
            , cardInfo.FILE_PATH AS filePath 
            , cardInfo.CARD_GUBUN  AS cardGubun
            , (SELECT CDSUBJECT FROM CMM_CD WHERE REF_CODE ='MT000' AND CD = cardInfo.CARD_GUBUN ) AS cardGubunNm
            , cardInfo.TIME AS time
        FROM CONTENT_STRUC lessonStruc
        INNER JOIN CONTENT_STRUC dayNoStruc ON dayNoStruc.UPPER_CONTENT_SEQ = lessonStruc.CONTENT_SEQ
        INNER JOIN PRODINFO cardInfo ON cardInfo.PROD_SEQ = dayNoStruc.CONTENT_SEQ
        INNER JOIN CMM_CD CC ON cardInfo.CARD_GUBUN = CC.CD
        WHERE dayNoStruc.mbr_id = #{mbrId}
        AND lessonStruc.mbr_id = #{mbrId}
        AND lessonStruc.UPPER_CONTENT_SEQ = #{lessonCd}
        AND dayNoStruc.UPPER_CONTENT_SEQ = #{dayNoCd}
        AND cardInfo.USE_YN = 'Y'
        AND dayNoStruc.SORT_ORD IS NOT NULL
        ORDER BY lessonStruc.SORT_ORD ASC, dayNoStruc.SORT_ORD ASC, CC.ORDER_NO ASC

    <!-- 위 쿼리로 수정함 02-23 이홍  
        SELECT
            cardInfo.PROD_SEQ AS prodSeq,
            cardInfo.PROD_TITLE AS prodTitle,
            cardInfo.THMB_PATH AS thmbPath,
            cardInfo.USE_YN AS useYn,
            cardInfo.DTL_CNTT AS dtlCntt,
            cardInfo.BOOK_CD AS bookCd,
            cardInfo.LESSON_CD AS lessonCd,
            cardInfo.DAYNO_CD AS daynoCd,
            cardInfo.MDL_CD AS mdlCd,
            cardInfo.CURRI_CD AS curriCd,
            cardInfo.SORT AS sort,
            cardInfo.PROD_SECT AS prodSect,
            cardInfo.PROD_TYPE AS prodType,
            cardInfo.FILE_PATH AS filePath
        FROM vs_tree.CONTENT_STRUC lessonStruc
        INNER JOIN vs_tree.CONTENT_STRUC dayStruc
        ON lessonStruc.CONTENT_SEQ = dayStruc.UPPER_CONTENT_SEQ
        LEFT JOIN vs_tree.PRODINFO cardInfo
        ON dayStruc.CONTENT_SEQ = cardInfo.PROD_SEQ
        WHERE lessonStruc.MBR_ID = #{mbrId} AND lessonStruc.UPPER_CONTENT_SEQ = #{lessonCd} AND dayStruc.UPPER_CONTENT_SEQ = #{dayNoCd}
        ORDER BY dayStruc.SORT_ORD ASC;
         -->
    </select>

    <!-- 해당 레슨의 서브 카드맵 데이터 리스트 조회. -->
    <select id="getCardMapDataSubList" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT 
              cardInfo.PROD_SEQ AS prodSeq
            , cardInfo.PROD_TITLE AS prodTitle
            , cardInfo.THMB_PATH AS thmbPath
            , cardInfo.USE_YN AS useYn
            , cardInfo.DTL_CNTT AS dtlCntt
            , cardInfo.BOOK_CD AS bookCd
            , cardInfo.LESSON_CD AS lessonCd
            , cardInfo.DAYNO_CD AS daynoCd
            , cardInfo.MDL_CD AS mdlCd
            , cardInfo.CURRI_CD AS curriCd
            , cardInfo.SORT AS sort
            , cardInfo.PROD_SECT AS prodSect
            , cardInfo.PROD_TYPE AS prodType
            , cardInfo.FILE_PATH AS filePath 
            , cardInfo.CARD_GUBUN  AS cardGubun
            , (SELECT CDSUBJECT FROM CMM_CD WHERE REF_CODE ='MT000' AND CD = cardInfo.CARD_GUBUN ) AS cardGubunNm
             , cardInfo.TIME AS time
        FROM PRODINFO cardInfo INNER JOIN CMM_CD CC ON cardInfo.CARD_GUBUN = CC.CD
        WHERE cardInfo.LESSON_CD = #{lessonCd}
            AND cardInfo.CARD_GUBUN IN ('MT002', 'MT003')
            OR (cardInfo.reg_id = #{mbrId} AND cardInfo.LESSON_CD = #{lessonCd} AND cardInfo.CARD_GUBUN = 'MT005')
        AND cardInfo.USE_YN = 'Y'
        ORDER BY CC.ORDER_NO ASC, cardInfo.PROD_SEQ ASC
    </select>
    
    
    <!-- 해당 유저의 특정 카드의 정렬 값 초기화~ Update -->
    <update id="modifyCardMapSortDataReset" parameterType="com.visangesl.treeapi.contents.vo.ContentsVo">
        UPDATE CONTENT_STRUC
        SET
            SORT_ORD = NULL
        WHERE MBR_ID = #{mbrId} AND UPPER_CONTENT_SEQ = #{daynoCd}
    </update>

    <!-- 해당 유저의 특정 카드의 정렬 값 Update -->
    <update id="modifyCardMapSortData" parameterType="com.visangesl.treeapi.contents.vo.ContentsVo">
        UPDATE CONTENT_STRUC
        SET
            SORT_ORD = #{sort}
        WHERE MBR_ID = #{mbrId} AND UPPER_CONTENT_SEQ = #{daynoCd} AND CONTENT_SEQ = #{cardCd} AND SORT_ORD IS NULL
    </update>

    <!-- 해당 유저의 특정 카드의 정렬 값 insert  -->
    <insert id="insertCardMapSortData" parameterType="com.visangesl.treeapi.contents.vo.ContentsVo">
        INSERT INTO CONTENT_STRUC (
            MBR_ID
            ,UPPER_CONTENT_SEQ
            ,CONTENT_SEQ
            ,SORT_ORD
        ) VALUES (
            #{mbrId}
            ,#{daynoCd}
            ,#{cardCd}
            ,#{sort}
        )
    </insert>

    <!-- 해당 유저의 특정 카드의 기존 정렬 값 삭제 -->
    <update id="deleteCardMapSortData" parameterType="com.visangesl.treeapi.contents.vo.ContentsVo">
        DELETE FROM CONTENT_STRUC
        WHERE MBR_ID = #{mbrId} AND UPPER_CONTENT_SEQ = #{daynoCd} AND SORT_ORD IS NULL
    </update>


    <!-- prodinfo table insert  -->
    <insert id="cProdInfoData" parameterType="com.visangesl.treeapi.contents.vo.ProdInfoVo" useGeneratedKeys="true" keyProperty="prodSeq">
        INSERT INTO PRODINFO (
              PROD_TITLE
            , THMB_PATH
            , USE_YN
            , REG_DTTM
            , REG_ID
            , DTL_CNTT
            , BOOK_CD
            , LESSON_CD
            , DAYNO_CD
            , MDL_CD
            , CARD_CD
            , SORT
            , PROD_SECT
            , PROD_TYPE
            , CURRI_CD
            , CARD_TYPE
            , CARD_LEVEL
            , USE_TYPE
            , FILE_PATH
            , DIREC_ENG
            , DIREC_KOR
        )VALUES (
              #{prodTitle}
            , #{thmbPath}
            , 'Y'
            , now()
            , #{mbrId}
            , #{dtlCntt}
            , #{bookCd}
            , #{lessonCd}
            , #{daynoCd}
            , #{mdlCd}
            , #{cardCd}
            , #{sort}
            , #{prodSect}
            , #{prodType}
            , #{curriCd}
            , #{cardType}
            , #{cardLevel}
            , #{useType}
            , #{filePath}
            , #{direcEng}
            , #{direcKor}
        )
    </insert>
    
    <update id="uProdSeqCard" parameterType="String">
        UPDATE PRODINFO
        SET
            CARD_CD = #{prodSeq}
        WHERE PROD_SEQ = #{prodSeq}
    </update>

    <!-- 해당 차시의 카드맵 데이터 리스트 조회. -->
    <select id="getPackageDataList" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT
            STRUC.*
            ,PI.BOOK_CD AS bookCd
            ,PI.LESSON_CD AS lessonCd
            ,PI.FILE_PATH AS filePath
            ,PI.CARD_FULL_PATH AS cardFullPath
            ,PI.BOOK_PATH AS bookPath
            ,PI.LESSON_PATH AS lessonPath
            ,PI.CARD_PATH AS cardPath
        FROM (
            SELECT
                CS_DAYNO.SORT_ORD AS dayNo
                ,CS_CARD.UPPER_CONTENT_SEQ AS daynoCd
                ,CS_CARD.CONTENT_SEQ AS cardCd
            FROM CONTENT_STRUC CS_DAYNO
            INNER JOIN CONTENT_STRUC CS_CARD ON CS_DAYNO.CONTENT_SEQ = CS_CARD.UPPER_CONTENT_SEQ
            WHERE CS_DAYNO.MBR_ID = #{mbrId} AND CS_CARD.MBR_ID = #{mbrId}
                AND CS_DAYNO.UPPER_CONTENT_SEQ = (SELECT UPPER_CONTENT_SEQ FROM CONTENT_STRUC WHERE CONTENT_SEQ = #{dayNoCd} AND MBR_ID = #{mbrId} LIMIT 1)
                AND CS_CARD.SORT_ORD IS NOT NULL
            ORDER BY CS_DAYNO.SORT_ORD ASC, CS_CARD.SORT_ORD ASC
        )STRUC INNER JOIN PRODINFO PI ON STRUC.cardCd = PI.PROD_SEQ
        WHERE PI.USE_YN = 'Y'
    </select>


    <!-- 해당 차시의 북/레슨 코드 조회. -->
    <select id="getBookLessonCd" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT 
            BOOK_CD AS bookCd
            ,LESSON_CD AS lessonCd
        FROM PRODINFO
        WHERE DAYNO_CD = #{dayNoCd}
        LIMIT 1
    </select>


    <!-- 패키지 버전 체크 -->
    <select id="getPackageVersion" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition" resultType="com.visangesl.treeapi.contents.vo.ContentsVo">
        SELECT
            VER AS version
        FROM CONTENT_VER
        WHERE MBR_ID = #{mbrId}
        AND CONTENT_SEQ = #{lessonCd}
    </select>

    <!-- 패키지 버전 등록  -->
    <insert id="insertPackageVersion" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition">
        INSERT INTO CONTENT_VER (
            MBR_ID
            ,CONTENT_SEQ
            ,VER
        ) VALUES (
            #{mbrId}
            ,#{lessonCd}
            ,1
        )
    </insert>

    <!-- 패키지 버전 업 -->
    <update id="updatePackageVersion" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition">
        UPDATE CONTENT_VER
        SET VER = VER + 1
        WHERE MBR_ID = #{mbrId}
        AND CONTENT_SEQ = #{lessonCd}
    </update>

    <insert id="insertPackageFileInfo" parameterType="com.visangesl.treeapi.contents.vo.ContentsCondition">
        INSERT INTO PROD_FILE_INFO (
            FILE_NAME
            ,FILE_PATH
            ,FILE_TYPE
            ,PROD_SEQ
            ,MBR_ID
        ) VALUES (
            #{lessonCd}
            ,#{packageFilePath}
            ,'PACKAGE'
            ,#{lessonCd}
            ,#{mbrId}
        )
    </insert>

</mapper>
