package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.classroom.vo.ClassLessonListCondition;
import com.visangesl.treeapi.classroom.vo.ClassRoomCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassVo;

/**
 * 클래스 관련 mapper interface
 *
 * @author hong
 *
 */
public interface ClassRoomMapper {

	/**
	 * 티칭룸 - 클래스 목록 조회
	 *
	 * @param classRoomCondition
	 * @return
	 * @throws Exception
	 */
    public List<VSObject> getClassSimpleList(ClassRoomCondition classRoomCondition) throws Exception;


    /**
     * 해당 클래스의 구성원 단순 목록을 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassMemberSimpleList(VSCondition condition) throws Exception;

    /**
     * 해당 클래스의 강사 PC IP Address를 조회
     *
     * @param vsCondition
     * @return
     * @throws Exception
     */
    public VSObject getClassRelayIp(VSCondition vsCondition) throws Exception;

    /**
     * 해당 클래스의 강사 PC IP Address를 저장
     *
     * @param vsObject
     * @return
     * @throws Exception
     */
    public int mergeClassRelayIp(VSObject vsObject) throws Exception;

    /**
     * 해당일의 클래스 패널 목록 조회
     * @param classRoomCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getTodayClassesList(VSCondition condition) throws Exception;

    /**
     * 티칭룸 / 스터디룸 목록 조회
     *
     * @param classRoomCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<VSObject> getTeachingRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<VSObject> getStudyRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception;

    /**
     * 다음 레슨의 첫 번째 차시 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception;

    
    /**
     * 클래스의 차시 정보 등록 처리 
     * 
     * @param tcv
     * @return
     * @throws Exception
     */
    public int addDataTodayClass(TodayClassVo tcv) throws Exception;

    /**
     * 클래스의 오늘 진행할 차시 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getClassTodayDayNoCd(TodayClassCondition tcCond) throws Exception;

    /**
     * 다음 차시 코드 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getClassNextdayDayNoCd(TodayClassCondition tcCond) throws Exception;

    public int modifyClassTodayDayNoCd(TodayClassCondition tcCond) throws Exception;

    public int modifyClassNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception;

    public int deleteClassDayNoCd(TodayClassCondition tcCond) throws Exception;

    /**
     * 카드맵 레슨 목록 조회
     *
     * @param classLessonListCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassLessonList(ClassLessonListCondition classLessonListCondition) throws Exception;

    /**
     * 수업 완료 처리
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public int updateProgressStat(TodayClassCondition tcCond) throws Exception;

    /**
     * 다음 다음 차시 정보 등록
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public int insertAfterNextDayNoCd(TodayClassCondition tcCond) throws Exception;
}
