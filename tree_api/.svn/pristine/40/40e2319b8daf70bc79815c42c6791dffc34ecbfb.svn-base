package com.visangesl.treeapi.authoring.service.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.authoring.service.AuthoringCardCopyService;
import com.visangesl.treeapi.authoring.service.AuthoringCardPathParseService;
import com.visangesl.treeapi.authoring.service.AuthoringService;
import com.visangesl.treeapi.authoring.vo.AuthoringAddCard;
import com.visangesl.treeapi.authoring.vo.AuthoringCardCopy;
import com.visangesl.treeapi.authoring.vo.AuthoringCardPathParse;
import com.visangesl.treeapi.authoring.vo.AuthoringContentMultiLang;
import com.visangesl.treeapi.authoring.vo.AuthoringContentStruc;
import com.visangesl.treeapi.authoring.vo.AuthoringModifyCard;
import com.visangesl.treeapi.mapper.AuthoringMapper;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;


@Service
public class AuthoringServiceImpl implements AuthoringService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AuthoringCardCopyService authoringCardCopyService;

    @Autowired
    AuthoringCardPathParseService authoringCardPathParseService;

    @Autowired
    AuthoringMapper authoringMapper;


    /**
     * 컨텐츠 북 리스트 조회
     */
    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getContentBookList()
            throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getContentBookList();
    }

    /**
     * 상품 북 리스트 조회
     */
    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getProdBookList(VSCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getProdBookList(condition);
    }

    /**
     * 컨텐츠 레슨 리스트 조회
     */
    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getContentLessonList(VSCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getContentLessonList(condition);
    }

    /**
     * 상품 레슨 리스트 조회
     */
    @Override
    @Transactional(readOnly = true)
    public List<VSObject> getProdLessonList(VSCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getProdLessonList(condition);
    }

    /**
     * 카드 상세 정보 조회
     */
    @Override
    @Transactional(readOnly = true)
    public VSObject getAuthoringCardInfo(VSCondition condition) throws Exception {
        // TODO Auto-generated method stub
        return authoringMapper.getAuthoringCardInfo(condition);
    }

    /**
     * 저작도구 카드 정보 등록
     */
    @Override
    @Transactional(rollbackFor = {Exception.class}, readOnly = false)
    public VSResult<Object> addAuthoringCard(AuthoringAddCard authoring) throws Exception {
        VSResult<Object> resultCodeMsg = new VSResult<Object>();
        AuthoringAddCard authoringCondition = authoring;

        // 등록/수정 구분
        String contentSeq = authoring.getContentSeq();

        // 계정 권한 조회
        boolean adminCheck = false;
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        if (mbrGrade.equals(TreeProperties.getProperty("tree_superadmin")) || mbrGrade.equals(TreeProperties.getProperty("tree_contentadmin"))) {
            adminCheck = true;
        }

        try {

            int affectedRows = 0;
            if (contentSeq != null) {

                //==================================================================
                // 카드 정보 수정
                affectedRows = authoringMapper.modifyAuthoringCard(authoring);

            } else {

                //==================================================================
                // 카드 정보 등록 (계정 권한 별 분기)
                if (adminCheck) {

                    // CONTENT_INFO SEQ 조회
//                    AuthoringAddCard cardCondition = new AuthoringAddCard();
//                    authoringMapper.getContentInfoAutoIncrementVal(cardCondition);
//                    contentSeq = authoring.getContentSeq();
//
//                    logger.debug(contentSeq);
//                    logger.debug(contentSeq);

                    affectedRows = authoringMapper.addContentInfoTemp(authoring);
                } else {
                    affectedRows = authoringMapper.addContentInfo(authoring);
                }

                contentSeq = authoring.getContentSeq();
                if (contentSeq != null) {


                    // ==================================================================
                    // 카드 언어별 상세 정보 등록
                    String direcLang1 = authoringCondition.getDirecLang1();
                    String direcLang2 = authoringCondition.getDirecLang2();
                    String direc1 = authoringCondition.getDirec1();
                    String direc2 = authoringCondition.getDirec2();

                    AuthoringContentMultiLang multiLangCondition = new AuthoringContentMultiLang();
                    multiLangCondition.setContentSeq(contentSeq);
                    multiLangCondition.setType(TreeProperties.getProperty("tree_content_card"));
                    if (direcLang1 != null) {
                        String langSect = getLanguageSect(direcLang1.trim()); // 국가명으로 언어 코드 조회
                        multiLangCondition.setLangSect(langSect);
                        multiLangCondition.setDescr(direc1);
                        multiLangCondition.setSortOrd("1");

                        authoringMapper.addContentMultiLang(multiLangCondition);
                    }
                    if (direcLang2 != null) {
                        String langSect = getLanguageSect(direcLang1.trim()); // 국가명으로 언어 코드 조회
                        multiLangCondition.setLangSect(langSect);
                        multiLangCondition.setDescr(direc2);
                        multiLangCondition.setSortOrd("2");

                        authoringMapper.addContentMultiLang(multiLangCondition);
                    }


                    // ==================================================================
                    // 카드 - 레슨 관계 정보 등록
                    AuthoringContentStruc contentStruc = new AuthoringContentStruc();
                    contentStruc.setMbrId(authoringCondition.getMbrId());
                    contentStruc.setUpperContentSeq(authoringCondition.getLessonCd());
                    contentStruc.setContentSeq(contentSeq);
                    contentStruc.setSortOrd("1");
                    contentStruc.setDistYn(authoringCondition.getDistYn());

                    if (adminCheck) {
                        affectedRows = authoringMapper.addContentStrucTemp(contentStruc);
                    } else {
                        affectedRows = authoringMapper.addContentStruc(contentStruc);
                    }


                    // 카드 / 썸네일 경로 설정
                    String toBeDirPath = TreeProperties.getProperty("tree_card_path")
                        + TreeProperties.getProperty("tree_edit_card_path")
                        + File.separator + authoringCondition.getBookCd()
                        + File.separator + authoringCondition.getLessonCd()
                        + File.separator + contentSeq;
                    String thmbPath = authoringCondition.getThmbPath();
                    if (thmbPath == null) {
                        thmbPath = TreeProperties.getProperty("tree_thumbnail_path")
                            + TreeProperties.getProperty("tree_card_path")
                            + File.separator + "no-image_1.png";
                    }
                    String toBeThmbPath = TreeProperties.getProperty("tree_thumbnail_path")
                        + TreeProperties.getProperty("tree_card_path")
                        + File.separator + contentSeq;


                    // ==================================================================
                    // 카드 / 썸네일 경로 업데이트
                    AuthoringModifyCard modifyCardCondition = new AuthoringModifyCard();
                    modifyCardCondition.setContentSeq(contentSeq);
                    modifyCardCondition.setFilePath(toBeDirPath);
                    modifyCardCondition.setThmbPath(thmbPath);

                    affectedRows = authoringMapper.modifyAuthoringCard(modifyCardCondition);
                    if (affectedRows > 0) {


                        // ==================================================================
                        // 카드 복사 실행 - 저작도구에서 생성한 카드를 복사 해온다.
                        AuthoringCardCopy cardCopyCondition = new AuthoringCardCopy();
                        cardCopyCondition.setFilePath(authoringCondition.getFilePath()); // 복사할 원본 경로
                        cardCopyCondition.setToBePath(TreeProperties.getProperty("tree_save_filepath") + toBeDirPath);             // 복사될 경로 지정
                        cardCopyCondition.setThmbPath(TreeProperties.getProperty("tree_save_filepath") + thmbPath);                // 복사할 썸네일 원본 경로
                        cardCopyCondition.setToBeThmbPath(TreeProperties.getProperty("tree_save_filepath") + toBeThmbPath);        // 복사될 썸네일 경로 지정

                        boolean cardCopyResult = authoringCardCopyService.copyAuthoringCard(cardCopyCondition);
                        if (cardCopyResult) {


                            // ==================================================================
                            // 카드 경로 파싱
                            AuthoringCardPathParse cardPathParseCondition = new AuthoringCardPathParse();
                            cardPathParseCondition.setBookCd(authoringCondition.getBookCd());
                            cardPathParseCondition.setLessonCd(authoringCondition.getLessonCd());
                            cardPathParseCondition.setCardCd(contentSeq);

                            // 카드 경로 파싱 실행 - html 상단 pathesl 경로 추가 및 pathesl 변환
                            boolean cardPathParseResult = authoringCardPathParseService.parseAuthoringCardPath(cardPathParseCondition);
                            if (cardPathParseResult) {

                                resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));

                            } else {
                                resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                                throw new Exception();
                            }
                        } else {
                            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                            throw new Exception();
                        }
                    } else {
                        resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                    }

                } else {
                    resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
                }
            }

        } catch (Exception e) {
            resultCodeMsg.setCode("9999");
            resultCodeMsg.setMessage("조회 오류");
            logger.debug("catch");
        }


        return resultCodeMsg;
    }


    /**
     * 저작도구 카드 수정 - 추후 개발 예정?
     */
    @Override
    public VSResult<Object> modifyAuthoringCard(VSCondition condition)
            throws Exception {
        // TODO Auto-generated method stub
        return null;
    }


    /** 국가명으로 별 언어 코드 조회
     * @param direcLang
     * @return 국가 언어 코드
     */
    public String getLanguageSect(String direcLang) {

        String langSect = "";

        if (direcLang.equals("USA")) {
            langSect = TreeProperties.getProperty("tree_lang_usa");
        } else if (direcLang.equals("CHINA")) {
            langSect = TreeProperties.getProperty("tree_lang_china");
        } else if (direcLang.equals("KOREA")) {
            langSect = TreeProperties.getProperty("tree_lang_korea");
        } else if (direcLang.equals("MONGOLIA")) {
            langSect = TreeProperties.getProperty("tree_lang_mongolia");
        } else if (direcLang.equals("SINGAPORE")) {
            langSect = TreeProperties.getProperty("tree_lang_singapore");
        } else if (direcLang.equals("THAILAND")) {
            langSect = TreeProperties.getProperty("tree_lang_thailand");
        }

        return langSect;
    }

}
