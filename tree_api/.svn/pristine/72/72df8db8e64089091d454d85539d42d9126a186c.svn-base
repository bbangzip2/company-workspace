<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.visangesl.treeapi.mapper.ContentsMapper">

    <!-- 레퍼런스 자료 목록   -->
    <select id="getReferenceList" parameterType="com.visangesl.treeapi.contents.vo.RefCondition" resultType="com.visangesl.treeapi.contents.vo.RefVo">
		 SELECT
              #{prodSeq} as refSeq
            , RF.FILE_NM as refTitle
            , RF.FILE_PATH as thmbPath
            , RF.FILE_PATH as filePath
            , R.CONTENT as dtlCntt
            , R.TYPE as refType
            , R.DAYNO_CD as curriCd
         FROM REFERENCE_INFO R LEFT JOIN REFERENCE_FILE_INFO RF ON R.REFERENCE_SEQ = RF.REFERENCE_SEQ
         WHERE 1=1
          AND R.USE_YN = 'Y'
          <if test='prodType != ""'>
            AND R.TYPE = #{prodType} 
          </if>          
          AND R.DAYNO_CD = (SELECT DAYNO_CD FROM PROD_SERVICE WHERE CARD_CD = #{prodSeq} AND DAYNO_CD IS NOT NULL)
          ORDER BY R.TYPE ASC
              
    </select>
    
    <!-- 레퍼런스 자료 총 숫자  -->
    <select id="getReferenceListCnt" parameterType="com.visangesl.treeapi.contents.vo.RefCondition" resultType="string">
        SELECT
              count(*) as CNT
         FROM REFERENCE_INFO
         WHERE 1 = 1
          AND USE_YN = 'Y'
          AND DAYNO_CD = (SELECT DAYNO_CD FROM PROD_SERVICE WHERE CARD_CD = #{prodSeq} AND DAYNO_CD IS NOT NULL)
    </select>
        
    <!-- 레퍼런스 자료 타입별 데이터 유무 목록  -->
    <select id="getReferenceTypeList" parameterType="com.visangesl.treeapi.contents.vo.RefCondition" resultType="com.visangesl.treeapi.contents.vo.RefVo">
		SELECT 
			  AA.refType
			, sum(AA.CNT) AS refCnt
		FROM (
				SELECT
					   R.TYPE refType
					 , 1 AS CNT
					 , R.TYPE
				 FROM REFERENCE_INFO R
				 WHERE 1=1
				  AND USE_YN = 'Y'
				  AND DAYNO_CD = (SELECT DAYNO_CD FROM PROD_SERVICE WHERE CARD_CD = #{prodSeq} AND DAYNO_CD IS NOT NULL)
		)AA GROUP BY refType
            
    </select>
            
    
    <!-- 해당 컨텐츠의 상세 데이터 조회.  -->
    <select id="getContentsInfoData" parameterType="com.visangesl.treeapi.contents.vo.ContentProdSeq" resultType="com.visangesl.treeapi.contents.vo.ContentVo">
        SELECT
            PROD_SEQ AS prodSeq
            ,PROD_TITLE AS prodTitle
            ,DETAIL_CONTENT AS dtlCntt
            ,THMB_PATH AS thmbPath
            ,FILE_PATH AS filePath
            ,CARD_SECT AS cardSect
            ,CARD_TYPE AS cardType
            ,CARD_SKILL AS cardSkill
            ,STUDY_MODE AS studyMode
            ,CARD_LEVEL AS cardLevel
            ,TIME AS time
            ,EDIT_YN AS editYn
            ,OPEN_YN AS openYn
            ,KEYWORD AS keyword
            ,GRADING AS grading
            ,LANG1.LANG_SECT AS langSect1
            ,LANG1.DESCR AS langDescr1
            ,LANG2.LANG_SECT AS langSect2
            ,LANG2.DESCR AS langDescr2
            ,IF(CARD_PATH IS NULL OR CARD_PATH = '', FILE_PATH, CARD_PATH) AS cardPath
        FROM PROD_INFO INFO
        LEFT JOIN CONTENT_MULTI_LANG LANG1 ON INFO.PROD_SEQ = LANG1.CONTENT_SEQ AND LANG1.SORT_ORD = 1
        LEFT JOIN CONTENT_MULTI_LANG LANG2 ON INFO.PROD_SEQ = LANG2.CONTENT_SEQ AND LANG2.SORT_ORD = 2
        WHERE PROD_SEQ = #{prodSeq}
    </select>


    <!-- Lesson의 차시 리스트 조회 -->
    <select id="getDayDataList" parameterType="com.visangesl.treeapi.contents.vo.ContentProdSeq" resultType="com.visangesl.treeapi.contents.vo.ContentDayNoListVo">
        SELECT
            DISTINCT(INFO.PROD_SEQ) AS prodSeq
            ,INFO.PROD_TITLE AS prodTitle
        FROM PROD_SERVICE SERVICE
        INNER JOIN PROD_INFO INFO ON SERVICE.DAYNO_CD = INFO.PROD_SEQ AND SERVICE.TYPE = 'PT004' AND SERVICE.LESSON_CD = #{prodSeq}
    </select>

    <!-- 해당 차시의 카드맵 데이터 리스트 조회. -->
    <select id="getCardMapDataList" parameterType="com.visangesl.treeapi.contents.vo.ContentCardMapList" resultType="com.visangesl.treeapi.contents.vo.ContentCardMapVo">
        SELECT
            INFO.PROD_SEQ AS prodSeq
            ,INFO.PROD_TITLE AS prodTitle
            ,INFO.THMB_PATH AS thmbPath
            ,INFO.FILE_PATH AS filePath
            ,INFO.DETAIL_CONTENT AS dtlCntt
            ,INFO.CARD_SECT AS cardSect
            ,INFO.CARD_TYPE AS cardType
            ,INFO.CARD_SKILL AS cardSkill
            ,INFO.CARD_LEVEL AS cardLevel
            ,INFO.STUDY_MODE AS studyMode
            ,INFO.EDIT_YN AS editYn
            ,INFO.OPEN_YN AS openYn
            ,INFO.SORT AS sort
            ,INFO.CARD_PATH AS cardPath
        FROM PROD_SERVICE SERVICE
        INNER JOIN PROD_INFO INFO ON SERVICE.CARD_CD = INFO.PROD_SEQ
            <choose>
                <when test="mbrId != null and mbrId != ''">
                    AND SERVICE.MBR_ID = #{mbrId}
                </when>
                <otherwise>
                    AND SERVICE.MBR_ID IN (
                        SELECT
                            MBR_ID
                        FROM MBR_INFO
                        WHERE MBR_GRADE IN ('MG100', 'MB111')
                    )
                </otherwise>
            </choose>
            <if test="lessonCd != null and lessonCd != ''">
                AND SERVICE.LESSON_CD = #{lessonCd}
            </if>
            <choose>
                <when test="dayNoCd != null and dayNoCd != ''">
                    AND SERVICE.DAYNO_CD = #{dayNoCd}
                </when>
                <otherwise>
                    AND (SERVICE.DAYNO_CD IS NULL OR SERVICE.DAYNO_CD = '')
                </otherwise>
            </choose>
            AND SERVICE.TYPE = 'PT006'
    </select>

    <!-- 해당 레슨의 서브 카드맵 데이터 리스트 조회. -->
    <select id="getCardMapDataSubList" parameterType="com.visangesl.treeapi.contents.vo.ContentCardMapList" resultType="com.visangesl.treeapi.contents.vo.ContentCardMapVo">
        SELECT
            INFO.PROD_SEQ AS prodSeq
            ,INFO.PROD_TITLE AS prodTitle
            ,INFO.THMB_PATH AS thmbPath
            ,INFO.FILE_PATH AS filePath
            ,INFO.DETAIL_CONTENT AS dtlCntt
            ,INFO.CARD_SECT AS cardSect
            ,INFO.CARD_TYPE AS cardType
            ,INFO.CARD_SKILL AS cardSkill
            ,INFO.CARD_LEVEL AS cardLevel
            ,INFO.STUDY_MODE AS studyMode
            ,INFO.EDIT_YN AS editYn
            ,INFO.OPEN_YN AS openYn
            ,INFO.SORT AS sort
        FROM PROD_SERVICE SERVICE
        INNER JOIN PROD_INFO INFO ON SERVICE.CARD_CD = INFO.PROD_SEQ
            AND SERVICE.MBR_ID = #{mbrId}
            AND SERVICE.LESSON_CD = #{lessonCd}
            <if test="dayNoCd == null or dayNoCd =='' ">
                AND (SERVICE.DAYNO_CD IS NULL OR SERVICE.DAYNO_CD = '')
            </if>
            AND SERVICE.TYPE = 'PT006'
    </select>

    <!-- 해당 차시의 컨텐츠 코드 조회 -->
    <select id="getContentStrucBookLessonDayNo" parameterType="com.visangesl.treeapi.contents.vo.ContentDayNoCd" resultType="com.visangesl.treeapi.contents.vo.ContentStrucVo">
        SELECT
            STRUC.MBR_ID AS mbrId
            ,STRUC.UPPER_CONTENT_SEQ AS upperContentSeq
            ,STRUC.CONTENT_SEQ AS contentSeq
            ,STRUC.SORT_ORD AS sortOrd
        FROM (
            SELECT
                STRUC.*
            FROM CONTENT_STRUC STRUC
            INNER JOIN CONTENT_SERVICE SERVICE ON STRUC.CONTENT_SEQ = SERVICE.BOOK_CD AND SERVICE.DAYNO_CD = #{dayNoCd}
          UNION ALL
            SELECT
                STRUC.*
            FROM CONTENT_STRUC STRUC
            INNER JOIN CONTENT_SERVICE SERVICE ON STRUC.CONTENT_SEQ = SERVICE.LESSON_CD AND SERVICE.DAYNO_CD = #{dayNoCd}
          UNION ALL
            SELECT
                STRUC.*
            FROM CONTENT_STRUC STRUC
            INNER JOIN CONTENT_SERVICE SERVICE ON STRUC.CONTENT_SEQ = SERVICE.DAYNO_CD AND SERVICE.DAYNO_CD = #{dayNoCd}
        )STRUC
        GROUP BY STRUC.STRUC_SEQ
    </select>

    <!-- 해당 차시의 카드 구조 등록 - 중복 체크 후 없을때만 등록 처리 -->
    <insert id="addDayNoContentStrucWithDuplicateCheck" parameterType="com.visangesl.treeapi.contents.vo.ContentStruc">
        INSERT INTO CONTENT_STRUC (
            MBR_ID
            ,UPPER_CONTENT_SEQ
            ,CONTENT_SEQ
            ,SORT_ORD
            ,DIST_YN
        )
        SELECT #{mbrId}, #{upperContentSeq}, #{contentSeq}, #{sortOrd}, 'Y' FROM DUAL
        WHERE NOT EXISTS (
            SELECT
                MBR_ID
                ,UPPER_CONTENT_SEQ
                ,CONTENT_SEQ
                ,SORT_ORD
            FROM CONTENT_STRUC
            WHERE CONTENT_SEQ = #{contentSeq}
            AND MBR_ID = #{mbrId}
        )
    </insert>

    <!-- 해당 차시의 카드 구조 등록 -->
    <insert id="addDayNoContentStruc" parameterType="com.visangesl.treeapi.contents.vo.ContentStruc">
        INSERT INTO CONTENT_STRUC (
            MBR_ID
            ,UPPER_CONTENT_SEQ
            ,CONTENT_SEQ
            ,SORT_ORD
            ,DIST_YN
        ) VALUES (
            #{mbrId}
            ,#{upperContentSeq}
            ,#{contentSeq}
            ,#{sortOrd}
            ,'Y'
        )
    </insert>

    <!-- 해당 차시의 카드 구조를 삭제한다. -->
    <update id="deleteDayNoContentStruc" parameterType="com.visangesl.treeapi.contents.vo.ContentDayNoCd">
        DELETE FROM CONTENT_STRUC
        WHERE MBR_ID = #{mbrId} AND UPPER_CONTENT_SEQ = #{dayNoCd}
    </update>

    <!-- 해당 차시의 북/레슨 코드 조회 -->
    <select id="getContentStrucBookLessonCd" parameterType="com.visangesl.treeapi.contents.vo.ContentDayNoCd" resultType="com.visangesl.treeapi.contents.vo.ContentBookLessonCodeVo">
        SELECT
            BOOK_CD AS bookCd
            ,LESSON_CD AS lessonCd
        FROM CONTENT_SERVICE
        WHERE DAYNO_CD = #{dayNoCd}
        LIMIT 1
    </select>

    <!-- 해당 레슨의 차시 코드 리스트 조회 -->
    <select id="getDayNoCdList" parameterType="com.visangesl.treeapi.contents.vo.ContentProdSeq" resultType="com.visangesl.treeapi.contents.vo.ContentProdSeqVo">
        SELECT
            DISTINCT(DAYNO_CD) AS prodSeq
        FROM PROD_SERVICE
        WHERE TYPE = 'PT004'
        AND LESSON_CD = #{prodSeq}
        ORDER BY SORT_ORD ASC
    </select>


    <!-- 패키지 파일 버전 조회 -->
    <select id="getContentVersionCount" parameterType="com.visangesl.treeapi.contents.vo.ContentVersion" resultType="integer">
        SELECT
            VER_SEQ
        FROM CONTENT_VER
        <where>
            AND MBR_ID = #{mbrId}
            AND CLS_SEQ = #{clsSeq}
            AND CONTENT_SEQ = #{contentSeq}
            AND TYPE = #{type}
        </where>
    </select>

    <!-- 패키지 파일 버전 등록 처리 -->
    <insert id="addContentVersion" parameterType="com.visangesl.treeapi.contents.vo.ContentVersion">
        INSERT INTO CONTENT_VER (
            MBR_ID
            ,CLS_SEQ
            ,CONTENT_SEQ
            ,TYPE
            ,FILE_NM
            ,FILE_PATH
            ,PKGING_YN
            ,DIST_YN
        ) VALUES (
            #{mbrId}
            ,#{clsSeq}
            ,#{contentSeq}
            ,#{type}
            ,#{fileNm}
            ,#{filePath}
            ,'Y'
            ,'Y'
        )
    </insert>

    <!-- 패키지 파일 버전 업 처리 -->
    <insert id="updateContentVersion" parameterType="com.visangesl.treeapi.contents.vo.ContentVersion">
        UPDATE CONTENT_VER
            SET
            VER = VER + 1
            ,PKGING_YN = 'Y'
            ,DIST_YN = 'Y'
            ,DIST_DATE = NOW()
        <where>
            AND MBR_ID = #{mbrId}
            AND CLS_SEQ = #{clsSeq}
            AND CONTENT_SEQ = #{contentSeq}
            AND TYPE = #{type}
        </where>
    </insert>

    <!-- 컨텐츠 패키징 정보 조회 -->
    <select id="getContentPackageVersionInfo" parameterType="com.visangesl.treeapi.contents.vo.ContentVersionCheck" resultType="com.visangesl.treeapi.contents.vo.ContentDownloadVo">
        SELECT
            MBR_ID AS mbrId
            ,CLS_SEQ AS clsSeq
            ,CONTENT_SEQ AS prodSeq
            ,TYPE AS prodType
            ,VER AS version
            ,FILE_NM AS fileName
            ,FILE_PATH AS filePath
        FROM CONTENT_VER
        <where>
            <choose>
                <when test="clsSeq != null and clsSeq != ''">CLS_SEQ = #{clsSeq}</when>
                <otherwise>
                    MBR_ID IN (
                        SELECT
                            MBR_ID
                        FROM MBR_INFO
                        WHERE MBR_GRADE IN ('MG100', 'MB111')
                    )
                </otherwise>
            </choose>
            AND CONTENT_SEQ = #{lessonCd}
            AND TYPE = 'P'
        </where>
    </select>

</mapper>
