<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.visangesl.treeapi.mapper.BadgeMapper">
    <!--
        특정 사용자가 수여받은 뱃지 정보를 담을 resultMap 
     -->
	<resultMap id="TreasureBadgeInfoResultMap" type="com.visangesl.treeapi.badge.vo.TreasureBadgeInfoVo">
		<result property="badgeType" column="badgeType" />
		<result property="badgeSeq" column="badgeSeq" />
		<result property="badgeName" column="badgeName" />
		<result property="badgeMean" column="badgeMean" />
		<collection property="bestowalMbr" ofType="com.visangesl.treeapi.badge.vo.BestowalMemberVo">
			<id property="mbrId" column="mbrId" />
			<result property="sexSect" column="sexSect" />
			<result property="nm" column="nm" />
			<result property="bestowalDate" column="bestowalDate" />
		</collection>
	</resultMap>
	
	<!--
	   특정 사용자의 뱃지 목록을 조회 
	 -->
	<select id="getBadgeList" parameterType="com.visangesl.treeapi.badge.vo.BadgeListCondition" resultMap="TreasureBadgeInfoResultMap">
		SELECT
		    `BADGE`.`TYPE` as badgeType
		    ,`BADGE`.`CD` as badgeSeq
		    ,`BADGE_INFO`.`NAME` as badgeName
		    ,`BADGE_MULTI_LANG`.`DESCR` as badgeMean
		    ,`BADGE`.`REG_ID` as mbrId
		    ,`MBR_INFO`.`SEX_SECT` as sexSect
		    ,`MBR_INFO`.`NAME` as nm
		    ,DATE_FORMAT(`BADGE`.`REG_DATE`,'%y.%m.%d') as bestowalDate
		FROM
		    `BADGE`
		    INNER JOIN `BADGE_INFO` ON (`BADGE_INFO`.`TYPE`=`BADGE`.`TYPE` AND `BADGE_INFO`.`BADGE_INFO_SEQ`=`BADGE`.`CD`)
		    LEFT OUTER JOIN `BADGE_MULTI_LANG` ON (`BADGE_MULTI_LANG`.`BADGE_INFO_SEQ`=`BADGE_INFO`.`BADGE_INFO_SEQ` AND `BADGE_MULTI_LANG`.`LANG_SECT`=#{langSect})
		    LEFT OUTER JOIN `MBR_INFO` ON (`MBR_INFO`.`MBR_ID`=`BADGE`.`REG_ID`)
		WHERE
		    `BADGE`.`RECV_MBR_ID`=#{mbrId}
		ORDER BY
		    `BADGE`.`TYPE` ASC
		    ,`BADGE`.`CD` ASC
		    ,bestowalDate DESC
		;
	</select>
	
	<!--
	   특정 사용자에게 뱃지를 부여
	   사용자는 컨텐츠 하나당 동일한 뱃지를 여러번 수여할 수 없다. 
	 -->
    <insert id="setBadge" parameterType="com.visangesl.treeapi.badge.vo.BadgeVo">
		<!-- 
		INSERT INTO vs_tree.BADGE (
			BADGE_TYPE,
			USE_YN,
			REG_ID,
			REG_DTTM,
			RECV_MBR_ID,
			BADGE_CD,
			CONTENT_SEQ,
			CONTENT_TYPE
		) VALUES (
			#{badgeType},
			#{useYn},
			#{regId},
			NOW(),
			#{recvMbrId},
			#{badgeCd},
			#{contentSeq},
			#{contentType}
		)
		 -->
        INSERT INTO BADGE (
            `TYPE`,
            USE_YN,
            REG_ID,
            REG_DATE,
            RECV_MBR_ID,
            CD,
            CONTENT_SEQ,
            CONTENT_TYPE
        ) 		
		SELECT 
            #{badgeType},
            #{useYn},
            #{regId},
            NOW(),
            #{recvMbrId},
            #{badgeCd},
            #{contentSeq},
            #{contentType}
		 FROM DUAL
		WHERE NOT EXISTS 
		  (SELECT BADGE_SEQ FROM BADGE WHERE `TYPE` = #{badgeType} AND RECV_MBR_ID = #{recvMbrId} AND `CD` = #{badgeCd}  AND CONTENT_SEQ = #{contentSeq} AND CONTENT_TYPE = #{contentType})
	</insert>
</mapper>
