package com.visangesl.treeapi.comment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.comment.service.CommentService;
import com.visangesl.treeapi.comment.vo.CommentVo;
import com.visangesl.treeapi.comment.vo.CommentCondition;
import com.visangesl.treeapi.comment.vo.ContentCommentCondition;
import com.visangesl.treeapi.comment.vo.LikeCondition;
import com.visangesl.treeapi.exception.ExceptionHandler;
import com.visangesl.treeapi.member.vo.UserSession;
import com.visangesl.treeapi.util.TreeUtil;
import com.visangesl.treeapi.vo.VSResult;

@Controller
public class CommentController {
	@Autowired
	private CommentService commentService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 코멘트 등록
	 * @param request
	 * @param content      코멘트 내용
	 * @param contentType  컨텐츠 타입
	 * @param contentId    컨텐츠 아이디
	 * @return
	 */
	@RequestMapping(value="/comment/addComment", method=RequestMethod.POST)
	@ResponseBody
	public VSResult<VSObject> addComment(HttpServletRequest request
			, @RequestParam(value = "cmmtCntt", required = true, defaultValue = "") String content
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId) {
		
		// 세션에서 유저 아이디를 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();
        
        CommentVo comment = new CommentVo();
        comment.setContent(content);
        comment.setContentType(contentType);
        comment.setContentId(contentId);
        comment.setRegId(mbrId);
		
		return commentService.addComment(comment);
	}
	
	/**
	 * 코멘트 삭제
	 * @param request
	 * @param commentSeq   코멘트 순번
	 * @return
	 */
	@RequestMapping(value="/comment/removeComment", method=RequestMethod.POST)
	@ResponseBody
	public VSResult<VSObject> removeComment(HttpServletRequest request
			, @RequestParam(value = "cmmtSeq", required = true, defaultValue = "") int commentSeq) {
		
		// 세션에서 유저 아이디를 가져온다.
        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        String mbrId = userDetail.getUsername();
        
        CommentCondition condition = new CommentCondition();
        condition.setCommentSeq(commentSeq);
        condition.setRegId(mbrId);
		
		return commentService.removeComment(condition);
	}
	
	@RequestMapping(value="/comment/getContentComment", method=RequestMethod.GET)
	@ResponseBody
	public VSResult<Object> getContentComment(HttpServletRequest request
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId) {
		
	    ContentCommentCondition condition = new ContentCommentCondition();
        condition.setContentType(contentType);
        condition.setContentId(contentId);

        VSResult<Object> result = new VSResult<Object>();
		
        try {	
	        result.setResult(commentService.getContentComment(condition));
	        result.setCode("0000");
			result.setMessage("성공");
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("조회 오류");
            e.printStackTrace();
        }
        
		return result;
	}
	
	@RequestMapping(value="/comment/toggleLike", method=RequestMethod.POST)
	@ResponseBody
	public VSResult<Object> toggleLike(HttpServletRequest request
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId
			) {
		LikeCondition vsCondition = new LikeCondition();
		
        String mbrId = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        }
        
        vsCondition.setContentType(contentType);
        vsCondition.setContentId(contentId);
        vsCondition.setMbrId(mbrId);
		
		return commentService.toggleLike(vsCondition);
	}
	
	@RequestMapping(value="/comment/existDoLike", method=RequestMethod.GET)
	@ResponseBody
	public VSResult<Object> existDoLike(HttpServletRequest request
			, @RequestParam(value = "contentType", required = true, defaultValue = "") String contentType
			, @RequestParam(value = "contentId", required = true, defaultValue = "") int contentId
			) {
		LikeCondition vsCondition = new LikeCondition();
		
        String mbrId = null;
        HttpSession session = request.getSession();
        UserSession userSession = (UserSession) session.getAttribute("VSUserSession");
        if (userSession != null) {
        	mbrId = userSession.getMemberId();
        }

        vsCondition.setContentType(contentType);
        vsCondition.setContentId(contentId);
        vsCondition.setMbrId(mbrId);
        
        VSResult<Object> result = new VSResult<Object>();
		
        try {	
	        result.setResult(TreeUtil.isNull(commentService.existDoLike(vsCondition), "N"));
	        result.setCode("0000");
			result.setMessage("성공");
        } catch (Exception e) {
            ExceptionHandler handler = new ExceptionHandler(e);
             result.setCode(handler.getCode());
             result.setMessage("조회 오류");
            e.printStackTrace();
        }
        
		return result;
	}	
}
