package com.visangesl.tree.security.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.classroom.service.ClassRoomService;
import com.visangesl.treeapi.classroom.vo.ClassRelayIp;
import com.visangesl.treeapi.member.service.MemberService;
import com.visangesl.treeapi.member.vo.Member;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

public class TreeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	protected Log logger = LogFactory.getLog(this.getClass());
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
    @Autowired
    MemberService service;
    
    @Autowired
    ClassRoomService classRoomService;
    
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		handle(request, response, authentication);
        clearAuthenticationAttributes(request);
	}
	
	protected void handle(HttpServletRequest request, 
		HttpServletResponse response, Authentication authentication) throws IOException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", TreeProperties.getProperty("error.success.code"));
		map.put("message", TreeProperties.getProperty("error.success.msg"));
		map.put("paramMap",null);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("mbrGrade",TreeSpringSecurityUtils.getAuthoritiesToRoles(authentication));
		resultMap.put("campNm",null);
		resultMap.put("useYn","Y");
		resultMap.put("memberId",TreeSpringSecurityUtils.getPrincipalAuthorities().getUsername());
		resultMap.put("serverPath",null);
		resultMap.put("memberName",TreeSpringSecurityUtils.getPrincipalAuthorities().getName());
		resultMap.put("profileImgPath",TreeSpringSecurityUtils.getPrincipalAuthorities().getProfilePhotoPath());
		resultMap.put("nickname",TreeSpringSecurityUtils.getPrincipalAuthorities().getNickname());
		resultMap.put("todayYmd",null);
		resultMap.put("name",null);
		        
        response.setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.OK.value());
		
        try {
	        // 로그인 후 가까운 수업의 클래스 코드를 조회
	        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles(authentication);
	        String mbrId = TreeSpringSecurityUtils.getPrincipalAuthorities().getUsername();
	
	        Member member = new Member();
	        member.setMbrGrade(mbrGrade);
	        member.setMbrId(mbrId);
	        member = service.getNearClassSeq(member);
	
	        // 로그인 성공한 경우 relay 서버의 IP를 배포하기 위한 작업을 진행한다.
	        ClassRelayIp classRelayIp = new ClassRelayIp();
	        if (member != null) {
		        classRelayIp.setClsSeq(member.getClsSeq());
	        }
	
	        //----------------------------------------------
	        // 카드맵 리스트 호출 페이지에서 다시 한번 IP 정보를 저장함.
	        // 강사인 경우 강사의 local IP를 서버에 저장한다.
	        if (mbrGrade != null && mbrGrade.equals("MG210")) {
	            String classLocalIp = request.getParameter("classLocalIp");
	
	            if (classLocalIp != null && !classLocalIp.equals("")) {
	                classRelayIp.setConnStat("C");
	                classRelayIp.setIp(classLocalIp);
	
	                VSResult<VSObject> classRelayIpResult = new VSResult<VSObject>();
	                classRelayIpResult = classRoomService.mergeClassRelayIp(classRelayIp);
	
	                logger.debug("><><><><><><>< IP 저장 결과 = "+ classRelayIpResult.getCode() + " | " + classRelayIpResult.getMessage());

	                resultMap.put("classRelayIp",classLocalIp);
	            }
	        }
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        map.put("result", resultMap);
        
        JSONObject jsonObject = JSONObject.fromObject(map);
        OutputStream out = response.getOutputStream();
        out.write(jsonObject.toString().getBytes());

/*		
		String targetUrl = determineTargetUrl(authentication);
		
		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);

*/	}

	/** Builds the target URL according to the logic defined in the main class Javadoc. */
	protected String determineTargetUrl(Authentication authentication) {		
		String targetUrl = null;
		
		
		
		String role = TreeSpringSecurityUtils.getAuthoritiesToRoles(authentication);
		
		logger.debug("role == " + role);
		
		if (role.equals("MG100")) {
			targetUrl = "/amg/instituteList.do";
		} else if (role.equals("MG121")) {
			targetUrl = "/amg/instituteInfo.do";
		} else if (role.equals("MG122")) {
			targetUrl = "/amg/instituteInfo.do";
//			targetUrl = "/amg/campusList.do";
		}		
	
		if (targetUrl != null) {
			return targetUrl;
		} else {
			throw new IllegalStateException();
		}
	}

	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}
	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}
}