package com.visangesl.treeapi.classroom.service;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.classroom.vo.ClassLessonListCondition;
import com.visangesl.treeapi.classroom.vo.ClassRoomCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassCondition;
import com.visangesl.treeapi.classroom.vo.TodayClassVo;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 클래스에 관련된 처리
 * @author hong
 *
 */
public interface ClassRoomService {

	/**
	 * 티칭룸 - 클래스 목록 조회
	 *
	 * @param classRoomCondition
	 * @return
	 * @throws Exception
	 */
    public List<VSObject> getClassSimpleList(ClassRoomCondition classRoomCondition) throws Exception;

    /**
     * 해당 클래스의 구성원 단순 목록을 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassMemberSimpleList(VSCondition condition) throws Exception;

    /**
     * 해당 클래스의 강사 PC의 IP Address를 조회
     *
     * @param vsCondition
     * @return
     * @throws Exception
     */
    public VSObject getClassRelayIp(VSCondition vsCondition) throws Exception;

    /**
     * 해당 클래스의 강사 PC의 IP Address를 저장
     *
     * @param vsObject
     * @return
     * @throws Exception
     */
    public VSResult<VSObject> mergeClassRelayIp(VSObject vsObject) throws Exception;

    /**
     * 해당일의 클래스 패널 목록 조회
     * @param classRoomCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getTodayClassesList(VSCondition condition) throws Exception;

    /**
     * 티칭룸 / 스터디룸 목록 조회
     *
     * @param classRoomCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassRoomList(ClassRoomCondition classRoomCondition) throws Exception;

    /**
     * 클래스의 첫번째 차시 목록 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public List<TodayClassVo> getFirstLessonList(TodayClassCondition tcCond) throws Exception;

    /**
     * 다음 레슨의 첫 번째 차시 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getNextLessonDayNoCd(TodayClassCondition tcCond) throws Exception;

    /**
     * 클래스의 차시 정보 등록 처리
     *
     * @param clsSeq
     * @param dayNoCd
     * @param sortOrd
     * @return
     * @throws Exception
     */
    public int addDataTodayClass(String clsSeq, String dayNoCd, String sortOrd) throws Exception;

    /**
     * 클래스의 오늘 진행할 차시 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getClassTodayDayNoCd(TodayClassCondition tcCond) throws Exception;

    /**
     * 다음 차시 코드 조회
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getClassNextdayDayNoCd(TodayClassCondition tcCond) throws Exception;

    /**
     * 카드맵의 레슨 목록 조회
     *
     * @param classLessonListCondition
     * @return
     * @throws Exception
     */
    public List<VSObject> getClassLessonList(ClassLessonListCondition classLessonListCondition) throws Exception;

    /**
     * 수업 완료 처리
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public int updateProgressStat(TodayClassCondition tcCond) throws Exception;

    /**
     * 다음 다음 차시 정보 등록
     *
     * @param tcCond
     * @return
     * @throws Exception
     */
    public int insertAfterNextDayNoCd(TodayClassCondition tcCond) throws Exception;
    
    /**
     * 이미등록된 차시인지 카운트로 확인 
     * @param tcCond
     * @return
     * @throws Exception
     */
    public int getProgressCnt(TodayClassCondition tcCond) throws Exception ;
    
    /**
     * 진행하지 않은 차시 조회 
     * @param tcCond
     * @return
     * @throws Exception
     */
    public String getNextDayNoCd(TodayClassCondition tcCond) throws Exception;
    
    /**
     * 오늘 진행하는 클래스 숫자 (패널에서 사용) 
     * @param condition
     * @return
     * @throws Exception
     */
    public String getTodayClassesListCnt(VSCondition condition) throws Exception;
}
