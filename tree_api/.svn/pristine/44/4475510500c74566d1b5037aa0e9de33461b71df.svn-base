package com.visangesl.treeapi.mynote.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.mynote.service.MyNoteService;
import com.visangesl.treeapi.mynote.vo.MyNoteCondition;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;

/**
 * 노트 관련 처리하는 controller 
 * 
 * @author hong
 *
 */
@Controller
public class MyNoteController {

    @Autowired
    private MyNoteService myNoteService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


	/**
	 * My Note > 레슨 목록 조회 
	 * @param request
	 * @param response
	 * @param note_memberId : 회원 아이디 
	 * @return
	 * @throws Exception
	 */
    @ResponseBody
    @RequestMapping(value="/mynote/getLessonList", method=RequestMethod.POST)
    public VSResult<List<VSObject>> getLessonList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "note_memberId", required = false) String memberId ) throws Exception {

        logger.debug("getLessonList");

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        
        try {

            MyNoteCondition myNote = new MyNoteCondition();
            myNote.setMbrId(memberId);

            List<VSObject> lessonList  = myNoteService.getLessonList(myNote);

            result.setResult(lessonList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
        }
        return result;
    }
    
    /**
     * 레슨별 노트 목록 조회
     * @param request
     * @param response
     * @param memberId : 회원 아이디 
     * @param lessonCd : 레슨 코드 
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/mynote/getMyNoteList", method=RequestMethod.POST)
    public VSResult<List<VSObject>> getMyNoteList(HttpServletRequest request, HttpServletResponse response
    		, @RequestParam(value = "note_memberId", required = false) String memberId 
    		, @RequestParam(value = "note_lessonCd", required = false) String lessonCd ) throws Exception {

        logger.debug("getMyNoteList");

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();
        
        try {

            MyNoteCondition myNote = new MyNoteCondition();
            myNote.setMbrId(memberId);
            myNote.setLessonCd(lessonCd);

            List<VSObject> NoteList  = myNoteService.getMyNoteList(myNote);

            result.setResult(NoteList);
            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
        }
        return result;
    }
    
}
