package com.visangesl.treeapi.contents.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.contents.service.ContentsService;
import com.visangesl.treeapi.contents.vo.ContentBookLessonCode;
import com.visangesl.treeapi.contents.vo.ContentBookLessonCodeVo;
import com.visangesl.treeapi.contents.vo.ContentCardMapList;
import com.visangesl.treeapi.contents.vo.ContentCardMapVo;
import com.visangesl.treeapi.contents.vo.ContentCardSortList;
import com.visangesl.treeapi.contents.vo.ContentDayNoCd;
import com.visangesl.treeapi.contents.vo.ContentProdSeq;
import com.visangesl.treeapi.contents.vo.ContentProdSeqVo;
import com.visangesl.treeapi.contents.vo.ContentStruc;
import com.visangesl.treeapi.contents.vo.ContentStrucVo;
import com.visangesl.treeapi.contents.vo.ContentsCondition;
import com.visangesl.treeapi.contents.vo.ProdInfoVo;
import com.visangesl.treeapi.mapper.ContentsMapper;
import com.visangesl.treeapi.packaging.service.PackageService;
import com.visangesl.treeapi.vo.VSResult;

@Service
public class ContentsServiceImpl implements ContentsService {
	private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	PackageService packageService;

	@Autowired
	ContentsMapper contentsMapper;


    @Override
    public VSObject getNewContentsInfo(ContentsCondition contentsCondition)
            throws Exception {
        // TODO Auto-generated method stub
        return contentsMapper.getNewContentsInfo(contentsCondition);
    }

	@Override
    @Transactional(readOnly = true)
    public VSObject getContentsInfoData(VSObject vsObject) throws Exception {

        return contentsMapper.getContentsInfoData(vsObject);
    }

	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getReferenceList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getReferenceList(dataCondition);
    }

	@Override
    @Transactional(readOnly = true)
    public List<VSObject> getDayDataList(VSObject vsObject) throws Exception {
        return contentsMapper.getDayDataList(vsObject);
    }

	@Override
    @Transactional(readOnly = true)
    public List<ContentCardMapVo> getCardMapDataList(VSObject vsObject) throws Exception {
        return contentsMapper.getCardMapDataList(vsObject);
    }

    @Override
    @Transactional
    public VSResult createPackage(ContentCardSortList condition) throws Exception {
        VSResult resultCodeMsg = new VSResult();

        String cardSort = condition.getCardSort();
        String dayNoCd = condition.getDayNoCd();
        String mbrId = condition.getMbrId();

        String bookCd;
        String lessonCd;
        int affectedRows = 0;


        // ========================================================
        // 컨텐츠 구조 개인화 진행
        // ========================================================
        // 북/레슨/차시 관계 정보 조회
        ContentDayNoCd contentDayNoCdCondition = new ContentDayNoCd();
        contentDayNoCdCondition.setDayNoCd(dayNoCd);
        contentDayNoCdCondition.setMbrId(mbrId);
        List<VSObject> contentStrucList = contentsMapper.getContentStrucBookLessonDayNo(contentDayNoCdCondition);


        // 북/레슨/차시 구조 추가 등록 - 개인화
        for (VSObject strucCondition : contentStrucList) {
            ContentStrucVo contentStrucVo = (ContentStrucVo) strucCondition;

            ContentStruc ContentStrucCondition = new ContentStruc();
            ContentStrucCondition.setMbrId(mbrId);
            ContentStrucCondition.setUpperContentSeq(contentStrucVo.getUpperContentSeq());
            ContentStrucCondition.setContentSeq(contentStrucVo.getContentSeq());
            ContentStrucCondition.setSortOrd(contentStrucVo.getSortOrd());
            affectedRows = contentsMapper.addDayNoContentStrucWithDuplicateCheck(ContentStrucCondition);
        }


        // 기존 카드 구조 정보 삭제
        ContentDayNoCd contentDayNoCd = new ContentDayNoCd();
        contentDayNoCd.setDayNoCd(dayNoCd);
        contentDayNoCd.setMbrId(mbrId);
        contentsMapper.deleteDayNoContentStruc(condition);


        // 신규 카드 구조 등록
        String[] cardSortArr = cardSort.split(",");

        ContentStruc contentStrucCondition = new ContentStruc();
        contentStrucCondition.setMbrId(mbrId);
        contentStrucCondition.setUpperContentSeq(dayNoCd);
        for (int i=0; i<cardSortArr.length; i++) {
            contentStrucCondition.setContentSeq(cardSortArr[i]);
            contentStrucCondition.setSortOrd(String.valueOf(i + 1));

            contentsMapper.addDayNoContentStruc(contentStrucCondition);
        }


        // 카드 맵의 카드 순만 변경되었을때는 패키징 처리를 진행 안함!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (!condition.isSortFlag()) {

            // ========================================================
            // 패키징 대상 조회 >>> 패키징 처리 (컨텐츠)
            // ========================================================
            // 북/레슨 코드 조회
            ContentDayNoCd contentDayNoCondition = new ContentDayNoCd();
            ContentBookLessonCodeVo contentBookLessonCodeVo = (ContentBookLessonCodeVo) contentsMapper.getContentStrucBookLessonCd(contentDayNoCondition);
            bookCd = contentBookLessonCodeVo.getBookCd();
            lessonCd = contentBookLessonCodeVo.getLessonCd();


            // 레슨 하위 차시 코드 리스트 조회
            ContentProdSeq contentProdSeq = new ContentProdSeq();
            contentProdSeq.setProdSeq(lessonCd);
            List<VSObject> dayNoCdList = contentsMapper.getDayNoCdList(contentProdSeq);

            if (dayNoCdList != null && dayNoCdList.size() > 0) {

                List<ContentCardMapVo> lessonCardMapList = new ArrayList<ContentCardMapVo>(); // 레슨 카드맵 리스트 - 패키징용
                List<ContentCardMapVo> dayNoCardMapList = new ArrayList<ContentCardMapVo>();  // 차시 카드맵 리스트 - 패키징 대상 카드맵 리스트 생성용

                for (int i=0; i<dayNoCdList.size(); i++) {
                    ContentProdSeqVo contentProdSeqVo = (ContentProdSeqVo) dayNoCdList.get(i);

                    // 차시 하위 카드 리스트 조회
                    ContentCardMapList contentCardMapListCondition = new ContentCardMapList();
                    contentCardMapListCondition.setDayNoCd(contentProdSeqVo.getProdSeq());
                    contentCardMapListCondition.setMbrId(mbrId);
                    dayNoCardMapList = contentsMapper.getCardMapDataList(contentCardMapListCondition);


                    // 개인화된 카드맵 리스트가 없으면 관리자 ID로 재 조회 - mbrId가 비었을 경우
                    if (dayNoCardMapList == null || dayNoCardMapList.size() == 0) {
                        contentCardMapListCondition.setMbrId("");
                        dayNoCardMapList = contentsMapper.getCardMapDataList(contentCardMapListCondition);
                    }

                    // 레슨 카드맵에 차시 카드맵 추가
                    lessonCardMapList.add((ContentCardMapVo) dayNoCardMapList);
                }


                // 패키징 처리 호출 - 컨텐츠
                ContentBookLessonCode contentPackageElement = new ContentBookLessonCode();
                contentPackageElement.setBookCd(bookCd);
                contentPackageElement.setLessonCd(lessonCd);
                contentPackageElement.setMbrId(mbrId);
                packageService.zipContentPackage(contentPackageElement, lessonCardMapList);

            }



            // ========================================================
            // 패키징 처리 - 오프라인
            // ========================================================



            // ========================================================
            // 패키징 버전 처리 진행
            // ========================================================
            // 버전 처리 호출

        }





//        // 카드 정렬 정보 업데이트
//        int affectedRows = contentsMapper.modifyCardMapSortData(vsObject);
//
//        if (affectedRows < 1) {
//
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }

        return resultCodeMsg;
    }

//	@Override
//    @Transactional
//    public VSResult modifyDataWithResultCodeMsg(VSObject vsObject) throws Exception {
//    	VSResult resultCodeMsg = new VSResult();
//
//        int affectedRows = contentsMapper.modifyDataWithResultCodeMsg(vsObject);
//
//        if (affectedRows < 1) {
//
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }
//
//	@Override
//    @Transactional
//    public VSResult addDataWithResultCodeMsg(VSObject vsObject) throws Exception {
//    	VSResult resultCodeMsg = new VSResult();
//
//        int affectedRows = contentsMapper.addDataWithResultCodeMsg(vsObject);
//
//        if (affectedRows < 1) {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }
//
//	@Override
//    @Transactional
//    public VSResult deleteDataWithResultCodeMsg(VSObject vsObject) throws Exception {
//    	VSResult resultCodeMsg = new VSResult();
//
//    	int affectedRows = contentsMapper.deleteDataWithResultCodeMsg(vsObject);
//
//        if (affectedRows < 1) {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }

	/**
	 * 카드코드 업데이트 처리
	 */
	@Override
    @Transactional
	public int InsertProdInfo(ProdInfoVo prodInfoVo) throws Exception{

		// 상품 테이블에 인서트 후
		contentsMapper.cProdInfoData(prodInfoVo);

		// 저장된 키값을 읽어서
		String prodSeq = prodInfoVo.getProdSeq();

		// CARD_CD 컬럼에 값을 채워준다.
		int result = contentsMapper.uProdSeqCard(prodSeq);


		return result;
	}

    @Override
    public List<VSObject> getPackageDataList(VSCondition dataCondition) throws Exception {

        return contentsMapper.getPackageDataList(dataCondition);

    }

    @Override
    public VSObject getBookLessonCd(VSCondition dataCondition) throws Exception {
        // TODO Auto-generated method stub
        return contentsMapper.getBookLessonCd(dataCondition);
    }

//    @Override
//    @Transactional
//    public int modifyCardMapSortDataReset(VSObject vsObject) throws Exception {
//        // TODO Auto-generated method stub
//        return contentsMapper.modifyCardMapSortDataReset(vsObject);
//    }

    @Override
    @Transactional
    public int InsertPackageVersion(ContentsCondition condition) throws Exception {

        return contentsMapper.insertPackageVersion(condition);
    }

    @Override
    public List<VSObject> getCardMapDataSubList(VSObject vsObject)
            throws Exception {
        return contentsMapper.getCardMapDataSubList(vsObject);
    }

//    @Override
//    @Transactional
//    public VSResult insertCardMapSortData(VSObject vsObject) throws Exception {
//        VSResult resultCodeMsg = new VSResult();
//
//        // 카드 정렬 정보 업데이트
//        int affectedRows = contentsMapper.insertCardMapSortData(vsObject);
//
//        if (affectedRows < 1) {
//
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }
//
//    @Override
//    @Transactional
//    public VSResult deleteCardMapSortData(VSObject vsObject) throws Exception {
//        VSResult resultCodeMsg = new VSResult();
//
//        // 카드 정렬 정보 삭제
//        int affectedRows = contentsMapper.deleteCardMapSortData(vsObject);
//
//        if (affectedRows < 1) {
//
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.fail.code"));
//        } else {
//            resultCodeMsg.setCode(TreeProperties.getProperty("error.success.code"));
//        }
//
//        return resultCodeMsg;
//    }

    @Override
 	public String getReferenceListCnt(VSObject vsObejct) throws Exception{
    	return contentsMapper.getReferenceListCnt(vsObejct);
    }

    @Override
    public List<VSObject> getReferenceTypeList(VSCondition dataCondition) throws Exception {
        return contentsMapper.getReferenceTypeList(dataCondition);

    }

}


