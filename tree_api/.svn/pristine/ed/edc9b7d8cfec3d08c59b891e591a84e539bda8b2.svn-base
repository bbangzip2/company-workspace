package com.visangesl.treeapi.mapper;

import java.util.List;

import com.visangesl.tree.vo.VSCondition;
import com.visangesl.tree.vo.VSObject;

/**
 * 저작도구 관련 mapper interface
 * @author user
 *
 */
public interface AuthoringMapper {

    /**
     * 컨텐츠 북 리스트 조회
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentBookList() throws Exception;

    /**
     * 상품 북 리스트 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getProdBookList(VSCondition condition) throws Exception;

    /**
     * 컨텐츠 레슨 리스트 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getContentLessonList(VSCondition condition) throws Exception;

    /**
     * 상품 리스트 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public List<VSObject> getProdLessonList(VSCondition condition) throws Exception;

    /**
     * 저작도구 카드 정보 등록
     * @param condition
     * @return
     */
    public int addContentInfo(VSCondition condition);

    /**
     * 저작도구 카드 정보 등록 - 임시 테이블
     * @param condition
     * @return
     */
    public int addContentInfoTemp(VSCondition condition);

    /**
     * 저작도구 카드 구조 정보 등록
     * @param condition
     * @return
     */
    public int addContentStruc(VSCondition condition);

    /**
     * 저작도구 카드 구조 정보 등록 - 임시 테이블
     * @param condition
     * @return
     */
    public int addContentStrucTemp(VSCondition condition);

    /**
     * 저작도구 카드 상세 설명 정보 저장
     * @param condition
     * @return
     */
    public int addContentMultiLang(VSCondition condition);

    /**
     * 컨텐츠 정보 테이블 AutoIncrement 값 조회
     * @param condition
     * @return
     */
    public VSObject getContentInfoAutoIncrementVal(VSCondition condition);

    /**
     * 컨텐츠 정보 테이블 AutoIncrement 값 +1
     * @param condition
     * @return
     */
    public int updateContentInfoAutoIncrementVal(VSCondition condition);

    /**
     * 저작도구 카드 상세 정보 조회
     * @param condition
     * @return
     * @throws Exception
     */
    public VSObject getAuthoringCardInfo(VSCondition condition) throws Exception;

    /**
     * 저작도구 카드 정보 수정
     * @param condition
     * @return
     * @throws Exception
     */
    public int modifyAuthoringCard(VSCondition condition) throws Exception;
}
