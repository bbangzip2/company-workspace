package com.visangesl.treeapi.authoring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.visangesl.tree.security.helper.TreeSpringSecurityUtils;
import com.visangesl.tree.security.vo.TreeUserDetails;
import com.visangesl.tree.vo.VSObject;
import com.visangesl.treeapi.authoring.service.AuthoringService;
import com.visangesl.treeapi.authoring.vo.AuthoringAddCard;
import com.visangesl.treeapi.authoring.vo.AuthoringCardInfo;
import com.visangesl.treeapi.authoring.vo.AuthoringLessonList;
import com.visangesl.treeapi.authoring.vo.AuthoringMbrId;
import com.visangesl.treeapi.property.TreeProperties;
import com.visangesl.treeapi.vo.VSResult;


/**
 * 저작도구 관련 처리 Contoller
 * @author user
 *
 */
@Controller
public class AuthoringController {

    @Autowired
    private AuthoringService authoringService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 저작도구 북 리스트 조회
     * @param request
     * @param response
     * @param mbrId
     * @return 북 리스트
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/authoring/bookList", method=RequestMethod.POST)
    public VSResult<List<VSObject>> getBookList(HttpServletRequest request, HttpServletResponse response) throws Exception {

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        // =================================================
        // 요청한 사용자의 세션정보 조회 필요 (계정 권한)
        // =================================================
        String mbrId = "";
        String mbrGrade = "";

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        if (userDetail != null) {
            mbrId = userDetail.getUsername();
            mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        }

        // =========================================
        // 테스트용 데이터
        logger.debug(mbrId);
        logger.debug(mbrGrade);
        mbrId = "vsadmin";
        mbrGrade = "MG100";

        try {

            AuthoringMbrId condition = new AuthoringMbrId();

            List<VSObject> list = null;

            if (mbrGrade.equals(TreeProperties.getProperty("tree_superadmin")) || mbrGrade.equals(TreeProperties.getProperty("tree_contentadmin"))) {

                list = authoringService.getContentBookList();

                result.setResult(list);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

            } else if (mbrGrade.equals(TreeProperties.getProperty("tree_teacher"))) {

                condition.setMbrId(mbrId);
                list = authoringService.getProdBookList(condition);

                result.setResult(list);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));
            } else {
                result.setCode("9001");
                result.setMessage("권한 없음");
            }

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
        }

//        request.getSession().getServletContext().getAttribute("sessionTest", "TEST");

        return result;
    }

    /**
     * 저작도구 레슨 리스트 조회
     * @param request
     * @param response
     * @param mbrId
     * @param bookCd
     * @return 레슨 리스트
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/authoring/lessonList", method=RequestMethod.POST)
    public VSResult<List<VSObject>> getLessonList(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "memberId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd
            ) throws Exception {

        logger.debug("getLessonList");
        logger.debug(mbrId);
        logger.debug(bookCd);

        VSResult<List<VSObject>> result = new VSResult<List<VSObject>>();

        // =================================================
        // 요청한 사용자의 세션정보 조회 필요 (계정 권한)
        // =================================================
        String mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();

        // =========================================
        // 테스트용 데이터
        logger.debug(mbrGrade);
        mbrGrade = "MG100";

        try {

            AuthoringLessonList condition = new AuthoringLessonList();
            condition.setMbrId(mbrId);
            condition.setBookCd(bookCd);

            List<VSObject> list = null;

            if (mbrGrade.equals(TreeProperties.getProperty("tree_superadmin")) || mbrGrade.equals(TreeProperties.getProperty("tree_contentadmin"))) {

                list = authoringService.getContentLessonList(condition);

                result.setResult(list);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

            } else if (mbrGrade.equals(TreeProperties.getProperty("tree_teacher"))) {

                list = authoringService.getProdLessonList(condition);

                result.setResult(list);
                result.setCode(TreeProperties.getProperty("error.success.code"));
                result.setMessage(TreeProperties.getProperty("error.success.msg"));

            } else {
                result.setCode("9001");
                result.setMessage("권한 없음");
            }

        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
        }

        return result;
    }


    /**
     * 저작도구 카드 등록
     * @param request
     * @param response
     * @param mbrId
     * @param pid
     * @param bookCd
     * @param lessonCd
     * @param prodTitle
     * @param thmbPath
     * @param filePath
     * @param cardType
     * @param cardSkill
     * @param studyMode
     * @param cardLevel
     * @param time
     * @param editYn
     * @param openYn
     * @param keyword
     * @param grading
     * @param direcLang1
     * @param direc1
     * @param direcLang2
     * @param direc2
     * @param useYn
     * @param model
     * @return 카드 등록 결과
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/authoring/addProdCard", method=RequestMethod.POST)
    public VSResult<Object> addProdCard(HttpServletRequest request, HttpServletResponse response
            , @RequestParam(value = "pid", required = false, defaultValue = "") String cardEditCd
            , @RequestParam(value = "bookCd", required = false, defaultValue = "") String bookCd
            , @RequestParam(value = "lessonCd", required = false, defaultValue = "") String lessonCd
            , @RequestParam(value = "prodTitle", required = false, defaultValue = "") String prodTitle
            , @RequestParam(value = "thmbPath", required = false, defaultValue = "") String thmbPath
            , @RequestParam(value = "filePath", required = false, defaultValue = "") String filePath
            , @RequestParam(value = "cardType", required = false, defaultValue = "") String cardType
            , @RequestParam(value = "cardSkill", required = false, defaultValue = "") String cardSkill
            , @RequestParam(value = "studyMode", required = false, defaultValue = "") String studyMode
            , @RequestParam(value = "cardLevel", required = false, defaultValue = "") String cardLevel
            , @RequestParam(value = "time", required = false, defaultValue = "") String time
            , @RequestParam(value = "editYn", required = false, defaultValue = "") String editYn
            , @RequestParam(value = "openYn", required = false, defaultValue = "") String openYn
            , @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword
            , @RequestParam(value = "grading", required = false, defaultValue = "") String grading
            , @RequestParam(value = "direcLang1", required = false, defaultValue = "") String direcLang1
            , @RequestParam(value = "direc1", required = false, defaultValue = "") String direc1
            , @RequestParam(value = "direcLang2", required = false, defaultValue = "") String direcLang2
            , @RequestParam(value = "direc2", required = false, defaultValue = "") String direc2
            , @RequestParam(value = "useYn", required = false, defaultValue = "Y") String useYn
            , Model model) throws Exception {

        logger.debug("addProdCard");

        VSResult<Object> result = new VSResult<Object>();
        AuthoringAddCard authoring = new AuthoringAddCard();

        String mbrId = "";
        String mbrGrade = "";

        TreeUserDetails userDetail = TreeSpringSecurityUtils.getPrincipalAuthorities();
        if (userDetail != null) {
            mbrId = userDetail.getUsername();
            mbrGrade = TreeSpringSecurityUtils.getAuthoritiesToRoles();
        }

        // =========================================
        // 테스트용 데이터
        logger.debug(mbrId);
        logger.debug(mbrGrade);
        mbrId = "vsadmin";
        mbrGrade = "MG100";
        filePath = "D:\\sia\\work\\PROJECT\\TREE\\contents\\boosterBook\\L001\\C0001";


        // 필수 파라미터 확인
        if (mbrId != null && !cardEditCd.equals("") && !bookCd.equals("") && !lessonCd.equals("") && !filePath.equals("")) {

            // 로그인 계정 권한 체크
            if (mbrGrade.equals(TreeProperties.getProperty("tree_superadmin")) || mbrGrade.equals(TreeProperties.getProperty("tree_contentadmin"))) {
                authoring.setDistYn("N");
            } else if (mbrGrade.equals(TreeProperties.getProperty("tree_teacher"))) {
                authoring.setDistYn("Y");
            }

            authoring.setMbrId(mbrId);
            authoring.setType(TreeProperties.getProperty("tree_content_card"));
            authoring.setCardSect(TreeProperties.getProperty("tree_edited_card"));
            authoring.setPid(cardEditCd);
            authoring.setBookCd(bookCd);
            authoring.setLessonCd(lessonCd);

            authoring.setProdTitle(prodTitle);
            authoring.setThmbPath(thmbPath);
            authoring.setFilePath(filePath);

            authoring.setCardType(cardType);
            authoring.setCardSkill(cardSkill);
            authoring.setStudyMode(studyMode);
            authoring.setCardLevel(cardLevel);
            authoring.setTime(time);
            authoring.setEditYn(editYn);
            authoring.setOpenYn(openYn);
            authoring.setKeyword(keyword);
            authoring.setGrading(grading);
            authoring.setDirecLang1(direcLang1);
            authoring.setDirec1(direc1);
            authoring.setDirecLang2(direcLang2);
            authoring.setDirec2(direc2);
            authoring.setUseYn(useYn);


            // 에디트 카드 등록
            result.setResult(authoringService.addAuthoringCard(authoring));

            if (result.getCode().equals("0000")) {
                result.setCode(TreeProperties.getProperty("error.success.code"));

            } else {
                logger.debug("카드 복사 실패");
                result.setCode(TreeProperties.getProperty("error.fail.code"));
                result.setMessage("등록 실패"); // - 카드 복사 실패
            }

        } else {
            logger.debug("등록 실패");
            result.setCode(TreeProperties.getProperty("error.fail.code"));
            result.setMessage("등록 실패 - 필수 파라미터 확인 (pid, bookCd, lessonCd, prodTitle, filePath)");
        }

        return result;
    }

    /**
     * 카드 상세 정보 조회
     * @param request
     * @param response
     * @param mbrId
     * @param pid 저작도구 관리 순번
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/authoring/cardInfo", method=RequestMethod.POST)
    public VSResult<Object> getData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "mbrId", required = false, defaultValue = "") String mbrId,
            @RequestParam(value = "pid", required = false, defaultValue = "") String pid
            ) throws Exception {

        logger.debug("cardInfo");
        logger.debug(mbrId);
        logger.debug(pid);

        VSResult<Object> result = new VSResult<Object>();

        try {

            // 카드 상세 정보 조회 - pid 기준
            AuthoringCardInfo condition = new AuthoringCardInfo();
            condition.setMbrId(mbrId);
            condition.setPid(pid);

            result.setResult(authoringService.getAuthoringCardInfo(condition));

            result.setCode(TreeProperties.getProperty("error.success.code"));
            result.setMessage(TreeProperties.getProperty("error.success.msg"));
        } catch (Exception e) {
            result.setCode("9999");
            result.setMessage("조회 오류");
        }

        return result;
    }

}
