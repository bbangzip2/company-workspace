<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.visangesl.treeapi.mapper.ClassRoomMapper">

	<select id="getDataList" parameterType="com.visangesl.treeapi.classroom.vo.ClassRoomVo" resultType="com.visangesl.treeapi.classroom.vo.ClassRoomSimpleVo">
		SELECT
			CI.CLS_SEQ AS clsSeq
			,CI.NAME AS clsNm
		FROM ICM_REL IR 
        INNER JOIN CLS_INFO CI ON IR.REL_SEQ = CI.CLS_SEQ AND IR.REL_SECT = 'RE003'
		WHERE IR.ICM_SEQ = #{mbrId}
		ORDER BY CI.CLS_SEQ ASC
	</select>

	<select id="getClassSimpleList" parameterType="com.visangesl.treeapi.classroom.vo.ClassRoomVo" resultType="com.visangesl.treeapi.classroom.vo.ClassRoomSimpleVo">
		SELECT
			  CLS_SEQ AS clsSeq
			, NAME AS clsNm
		FROM CLS_INFO
        WHERE CLS_SEQ IN (
						SELECT CLS_SEQ FROM ICM_REL_INFO
						WHERE MBR_ID = #{mbrId}
						AND CLS_SEQ IS NOT NULL
        )
		ORDER BY CLS_SEQ ASC
	</select>

	<select id="getClassMemberSimpleList" parameterType="com.visangesl.treeapi.classroom.vo.ClassRoomVo" resultType="com.visangesl.treeapi.classroom.vo.ClassRoomSimpleVo">
        SELECT
            CI.CLS_SEQ AS clsSeq
            ,CI.NAME AS clsNm
            ,IRI.MBR_ID AS mbrId
            ,MI.NAME AS nm
            ,MI.PROFILE_PHOTO_PATH AS profilePhotoPath
        FROM ICM_REL_INFO IRI
        INNER JOIN MBR_INFO MI ON IRI.MBR_ID = MI.MBR_ID AND MI.MBR_GRADE = #{mbrGrade} AND MI.USE_YN = 'Y'
        INNER JOIN CLS_INFO CI ON IRI.CLS_SEQ = CI.CLS_SEQ AND CI.CLS_SEQ = #{clsSeq}
        ORDER BY CI.CLS_SEQ ASC, MI.MBR_ID ASC
	</select>
	
	<!--
	   해당 클래스의 강사 PC IP Address를 조회 
	 -->
	<select id="getClassRelayIp" parameterType="com.visangesl.treeapi.classroom.vo.ClassRelayIpCondition" resultType="com.visangesl.treeapi.classroom.vo.ClassRelayIp">
		SELECT
			CLS_SEQ as clsSeq
			,IP as ip
			,CONN_STAT as connStat
		FROM
			CLS_RELAY_IP_INFO AS CRII
		WHERE
			CRII.CLS_SEQ=#{clsSeq}
			AND CRII.CONN_STAT=#{connStat}
	</select>
	
	<!--
	   강사 PC의 IP Address를 서버에 저장 
	 -->
	<insert id="mergeClassRelayIp" parameterType="com.visangesl.treeapi.classroom.vo.ClassRelayIp">
		INSERT INTO CLS_RELAY_IP_INFO
		(
			CLS_SEQ
			,IP
			,CONN_STAT
		)
		VALUES
		(
			#{clsSeq}
			,#{ip}
			,#{connStat}
		)
		ON DUPLICATE KEY
		UPDATE
			IP=#{ip}
			,CONN_STAT=#{connStat}
	</insert>

    <select id="getTodayClassesList" parameterType="com.visangesl.treeapi.classroom.vo.ClassMbrId" resultType="com.visangesl.treeapi.classroom.vo.ClassTodayListVo">
        SELECT
            #MIN(CIP.SORT_ORD)
            DISTINCT(CIP.DAYNO_CD)
            ,CI.CLS_SEQ AS clsSeq
            ,CI.NAME AS clsNm
            ,CI.START_HM AS inclsStartHm
            ,PB.PROD_SEQ AS bookCd
            ,PB.PROD_TITLE AS bookNm
            ,PL.PROD_SEQ AS lessonCd
            ,PL.PROD_TITLE AS lessonNm
            ,PL.THMB_PATH AS thmbPath
            ,CIP.DAYNO_CD AS dayNoCd
        FROM ICM_REL_INFO IRI
        INNER JOIN MBR_INFO MI ON IRI.MBR_ID = MI.MBR_ID AND MI.MBR_ID = #{mbrId}
        INNER JOIN CLS_INFO CI ON IRI.CLS_SEQ = CI.CLS_SEQ
            AND DATE(CI.START_YMD) <![CDATA[ <= ]]> DATE(#{inclsYmd})
            AND DATE(CI.END_YMD)  <![CDATA[  >= ]]> DATE(#{inclsYmd})
        INNER JOIN CLS_INCLS_PROGRESS CIP ON CI.CLS_SEQ = CIP.CLS_SEQ AND PROGRESS_STAT = 'N'
        INNER JOIN PROD_SERVICE PS ON PS.SEQ = CIP.DAYNO_CD
        INNER JOIN PROD_INFO PB ON PS.BOOK_CD = PB.PROD_SEQ
        INNER JOIN PROD_INFO PL ON PS.LESSON_CD = PL.PROD_SEQ
        ORDER BY CI.WK ASC, CI.START_HM ASC, PS.SORT_ORD ASC
<!-- 
		SELECT
		  CI.CLS_SEQ AS clsSeq
		, CI.CLS_NM AS clsNm
		, CI.START_HM AS inclsStartHm
		, PB.BOOK_CD AS bookCd
		, PB.PROD_TITLE AS bookNm
		, PL.LESSON_CD AS lessonCd
		, PL.PROD_TITLE AS lessonNm
		, PL.THMB_PATH AS thmbPath
		, TC.DAYNO_CD AS dayNoCd
		, (SELECT SORT FROM PRODINFO WHERE PRODINFO.PROD_SEQ = TC.DAYNO_CD) AS dayNo
		FROM CLS_INFO CI 
		    <if test='mbrGrade == "MG004"'>
			    INNER JOIN CLS_TCHR_INFO CR1 ON CR1.CLS_SEQ = CI.CLS_SEQ
			    INNER JOIN TODAY_CLASS TC ON TC.CLS_SEQ = CI.CLS_SEQ  AND TC.SORT_ORD ='1' 
		    </if>
		    <if test='mbrGrade == "MG005"'>
			    INNER JOIN CLS_STDNT_INFO CR1 ON CR1.CLS_SEQ = CI.CLS_SEQ
			    INNER JOIN TODAY_CLASS TC ON TC.CLS_SEQ = CI.CLS_SEQ 
		    </if>
		    INNER JOIN PRODINFO P ON P.PROD_SEQ = TC.DAYNO_CD
		    INNER JOIN PRODINFO PB ON PB.PROD_SEQ = P.BOOK_CD
		    INNER JOIN PRODINFO PL ON PL.PROD_SEQ = P.LESSON_CD 
		  
		WHERE 1 = 1
		AND CR1.MBR_ID = #{mbrId}
		AND DATE(CI.START_YMD) <![CDATA[ <= ]]> DATE(#{inclsYmd})
		AND DATE(CI.END_YMD)  <![CDATA[  >= ]]> DATE(#{inclsYmd})
		ORDER BY CI.WK ASC, START_HM ASC, PB.BOOK_CD, PB.SORT ASC,PL.LESSON_CD, PL.SORT
 -->
    </select>

    <!-- 티칭룸/스터디룸 목록 조회  -->
    <select id="getClassRoomList" parameterType="com.visangesl.treeapi.classroom.vo.ClassRoomCondition" resultType="com.visangesl.treeapi.classroom.vo.ClassRoomListVo">
        SELECT
            LIST.*
        FROM (
				SELECT 
					PDI.PROD_SEQ AS prodSeq
					,PDI.THMB_PATH AS thmbPath
					,CI.CLS_SEQ AS  clsSeq
					,CI.NAME AS clsNm
					,CI.WK AS inclsWk
					,CI.WK_NM AS inclsWkNm
					,CI.START_HM AS inclsStartHm
					,CI.END_HM AS inclsEndHm
					,IF (DATE_FORMAT(CI.END_YMD, '%Y-%m-%d')  <![CDATA[ < ]]>  DATE_FORMAT(NOW(), '%Y-%m-%d'), 'Y', 'N') AS inclsIngYn
					,'CNT' AS hWorkCnt
					,LES.LESSON_CD AS lessonCd
					,(SELECT PROD_TITLE FROM PROD_INFO WHERE PROD_SEQ = LES.LESSON_CD )AS lessonNm
				    , DAYNO.DAYNO_CD AS dayNocd
				FROM CLS_INFO CI 
				INNER JOIN PROG_ITEM PGI  ON CI.PROG_SEQ = PGI.PROG_SEQ
				INNER JOIN PROD_INFO PDI ON PGI.ITEM_SEQ = PDI.PROD_SEQ
				INNER JOIN PROD_SERVICE LES ON LES.BOOK_CD = PDI.PROD_SEQ AND LES.SEQ = LES.LESSON_CD AND LES.TYPE = 'PT003' AND LES.SORT_ORD='1'
				INNER JOIN PROD_SERVICE DAYNO ON DAYNO.LESSON_CD = LES.LESSON_CD AND DAYNO.DAYNO_CD = DAYNO.SEQ AND DAYNO.SORT_ORD ='1' 
            WHERE CI.USE_YN ='Y'
            AND CI.CLS_SEQ IN (
								SELECT CLS_SEQ FROM ICM_REL_INFO
								WHERE MBR_ID = #{mbrId}
								AND CLS_SEQ IS NOT NULL
            )
            <if test='sortKey != "ALL"'>
                AND CI.END_YMD <![CDATA[ >= ]]> NOW()
            </if>
        )LIST
        <where>
            <if test='sortKey != "ALL" and pageNo == 0'>
                LIST.inclsIngYn = 'N'
            </if>
        </where>
        ORDER BY inclsWk ASC, inclsStartHm ASC
        <if test="pageNo > 0">
            LIMIT ${pageNo}, ${pageSize}
        </if>
    </select>

    <select id="getTeachingRoomList" parameterType="com.visangesl.treeapi.classroom.vo.ClassRoomCondition" resultType="com.visangesl.treeapi.classroom.vo.ClassRoomVo">
        SELECT
            LIST.*
        FROM (
	        SELECT
	            PDI.PROD_SEQ AS prodSeq
	            ,PDI.THMB_PATH AS thmbPath
	            ,CI.CLS_SEQ AS  clsSeq
	            ,CI.CLS_NM AS clsNm
	            ,CI.INCLS_WK AS inclsWk
	            ,CI.INCLS_START_HM AS inclsStartHm
	            ,CI.INCLS_END_HM AS inclsEndHm
	            ,IF (DATE_FORMAT(CI.INCLS_END_YMD, '%Y-%m-%d') <![CDATA[ < ]]> DATE_FORMAT(NOW(), '%Y-%m-%d'), 'Y', 'N') AS inclsIngYn
	            ,'CNT' AS hWorkCnt
	        FROM CLS_TCHR_INFO CTI INNER JOIN CLS_INFO CI ON CTI.CLS_SEQ = CI.CLS_SEQ
	        INNER JOIN PROG P ON CI.CURRI_NO = P.PROG_SEQ
	        INNER JOIN PROG_ITEM PGI ON P.PROG_SEQ = PGI.PROG_SEQ
	        INNER JOIN PRODINFO PDI ON PGI.PROD_SEQ = PDI.PROD_SEQ
	        WHERE CTI.MBR_ID = #{mbrId}
	        AND PDI.PROD_SEQ = (SELECT BOOK_CD FROM PRODINFO WHERE PROD_SEQ = (
	            SELECT DAYNO_CD FROM CLS_SCHDL WHERE INCLS_YMD = (
	                SELECT MIN(INCLS_YMD) FROM CLS_SCHDL WHERE USE_YN = 'Y' AND INCLS_ING_YN = 'N')))
        )LIST
        <where>
	        <if test="pageNo == 0">
	            LIST.inclsIngYn = 'N'
	        </if>
        </where>
        ORDER BY inclsWk ASC, inclsStartHm ASC
        <if test="pageNo == 0">
            <!-- LIMIT ${pageNo}, ${pageSize} -->
        </if>
        <if test="pageNo > 0">
            LIMIT ${pageNo}, ${pageSize}
        </if>
    </select>

    <select id="getStudyRoomList" parameterType="com.visangesl.treeapi.classroom.vo.ClassRoomCondition" resultType="com.visangesl.treeapi.classroom.vo.ClassRoomVo">
        SELECT
            LIST.*
        FROM (
            SELECT
                PDI.PROD_SEQ AS prodSeq
                ,PDI.THMB_PATH AS thmbPath
                ,CI.CLS_SEQ AS  clsSeq
                ,CI.CLS_NM AS clsNm
                ,CI.INCLS_WK AS inclsWk
                ,CI.INCLS_START_HM AS inclsStartHm
                ,CI.INCLS_END_HM AS inclsEndHm
                ,IF (DATE_FORMAT(CI.INCLS_END_YMD, '%Y-%m-%d') <![CDATA[ < ]]> DATE_FORMAT(NOW(), '%Y-%m-%d'), 'Y', 'N') AS inclsIngYn
                ,'CNT' AS hWorkCnt
            FROM CLS_STDNT_INFO CSI INNER JOIN CLS_INFO CI ON CSI.CLS_SEQ = CI.CLS_SEQ
            INNER JOIN PROG P ON CI.CURRI_NO = P.PROG_SEQ
            INNER JOIN PROG_ITEM PGI ON P.PROG_SEQ = PGI.PROG_SEQ
            INNER JOIN PRODINFO PDI ON PGI.PROD_SEQ = PDI.PROD_SEQ
            WHERE CSI.MBR_ID = #{mbrId}
            AND PDI.PROD_SEQ = (SELECT BOOK_CD FROM PRODINFO WHERE PROD_SEQ = (
                SELECT DAYNO_CD FROM CLS_SCHDL WHERE INCLS_YMD = (
                    SELECT MIN(INCLS_YMD) FROM CLS_SCHDL WHERE USE_YN = 'Y' AND INCLS_ING_YN = 'N')))
        )LIST
        <where>
            <if test="pageNo == 0">
                LIST.inclsIngYn = 'N'
            </if>
        </where>
        ORDER BY inclsWk ASC, inclsStartHm ASC
        <if test="pageNo == 0">
            <!-- LIMIT ${pageNo}, ${pageSize} -->
        </if>
        <if test="pageNo > 0">
            LIMIT ${pageNo}, ${pageSize}
        </if>
    </select>
    
    <!-- 클래스의 첫번째 레슨 첫번째 차시 코드 조회  -->
    <select id="getFirstLessonList" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition" resultType="com.visangesl.treeapi.classroom.vo.TodayClassVo">
		SELECT 
		    DAYNO.DAYNO_CD AS dayNoCd
		FROM CLS_INFO CI 
		INNER JOIN PROG_ITEM PI ON CI.CLS_SEQ = #{clsSeq} AND CI.PROG_SEQ = PI.PROG_SEQ AND CI.USE_YN='Y'
        INNER JOIN PROD_SERVICE LES ON LES.BOOK_CD = PI.ITEM_SEQ AND LES.SEQ = LES.LESSON_CD AND LES.SORT_ORD ='1' AND LES.MBR_ID = CI.STRUC_MBR_ID
		INNER JOIN PROD_SERVICE DAYNO ON DAYNO.BOOK_CD = PI.ITEM_SEQ AND DAYNO.LESSON_CD = LES.LESSON_CD AND DAYNO.SEQ = DAYNO.DAYNO_CD AND DAYNO.SORT_ORD ='1' AND DAYNO.MBR_ID = CI.STRUC_MBR_ID
    </select>
    
    <!-- 패널에서 사용할 클래스의 차시 정보 등록함.-->
    <insert id="addDataTodayClass" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassVo">
        INSERT INTO CLS_INCLS_PROGRESS (
              CLS_SEQ
			, DAYNO_CD
			, SORT_ORD
			, PROGRESS_STAT
        ) VALUES (
              #{clsSeq}
            , #{dayNoCd}
            , #{sortOrd}
            , 'N'
        )
        ON DUPLICATE KEY
		UPDATE
			  CLS_SEQ=#{clsSeq}
			, SORT_ORD=#{sortOrd}
			, PROGRESS_STAT = 'N'
    </insert>
    
    <!-- 현재 차시로 다음 레슨의 첫째 차시 코드를 조회 함. -->
    <select id="getNextLessonDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition" resultType="String">
		SELECT
			  NDAY.DAYNO_CD
		 FROM CLS_INFO CI INNER JOIN PROD_SERVICE DAYNO ON CI.STRUC_MBR_ID = DAYNO.MBR_ID
		INNER JOIN PROD_SERVICE LES ON DAYNO.BOOK_CD = LES.BOOK_CD AND DAYNO.LESSON_CD = LES.LESSON_CD AND LES.SEQ = LES.LESSON_CD AND LES.MBR_ID = CI.STRUC_MBR_ID
		INNER JOIN PROD_SERVICE NLES ON NLES.BOOK_CD = LES.BOOK_CD AND NLES.SEQ = NLES.LESSON_CD AND LES.SORT_ORD+1 = NLES.SORT_ORD AND NLES.MBR_ID = CI.STRUC_MBR_ID
		INNER JOIN PROD_SERVICE NDAY ON NDAY.BOOK_CD = NLES.BOOK_CD AND NDAY.LESSON_CD = NLES.LESSON_CD AND NDAY.SEQ = NDAY.DAYNO_CD AND NDAY.SORT_ORD = '1' AND NDAY.MBR_ID = CI.STRUC_MBR_ID
		WHERE DAYNO.SEQ = #{dayNoCd}
		 AND CI.CLS_SEQ = #{clsSeq}
    </select>

    <!-- 클래스 코드로 현재 등록된 차시 정보를 조회함. TODAY_CLASS 에서 CLS_INCLS_PROGRESS로 변경 함 - 이홍 -->
    <select id="getClassTodayDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition" resultType="String">
		SELECT DAYNO_CD AS dayNocd
		  FROM CLS_INCLS_PROGRESS
		 WHERE CLS_SEQ = #{clsSeq}
		   AND PROGRESS_STAT = 'N'
		 ORDER BY SORT_ORD
		 LIMIT 1
    </select>
    
    <!-- 다음 차시 코드를 조회함. -->
    <select id="getClassNextdayDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition" resultType="String">
		SELECT
			IF(COUNT(NLD.SEQ) > 0
			  ,NLD.SEQ
			  ,(SELECT DAYNO_CD FROM TODAY_CLASS WHERE CLS_SEQ = #{clsSeq} AND DAYNO.SORT_ORD+1 = SORT_ORD)
			) AS dayNoCd
		FROM PROD_SERVICE DAYNO INNER JOIN PROD_SERVICE NLD ON DAYNO.BOOK_CD = NLD.BOOK_CD AND DAYNO.LESSON_CD = NLD.LESSON_CD AND NLD.SEQ = NLD.DAYNO_CD AND NLD.SORT_ORD = DAYNO.SORT_ORD + 1
		WHERE DAYNO.SEQ = #{dayNoCd}

    <!-- 
		SELECT
		    IF(COUNT(NEXT.CONTENT_SEQ) > 0
		      ,NEXT.CONTENT_SEQ
		      ,(SELECT DAYNO_CD FROM TODAY_CLASS WHERE CLS_SEQ = #{clsSeq} AND DAYNO.SORT_ORD+1 = SORT_ORD )
		    ) AS dayNoCd
		FROM CONTENT_STRUC THIS
		INNER JOIN CONTENT_STRUC NEXT ON THIS.UPPER_CONTENT_SEQ = NEXT.UPPER_CONTENT_SEQ AND NEXT.MBR_ID = #{mbrId} AND NEXT.SORT_ORD = THIS.SORT_ORD + 1
		WHERE THIS.MBR_ID = #{mbrId} AND THIS.CONTENT_SEQ = #{dayNoCd}
		 -->
    </select>

    <!-- 수업 종료 시 다음 차시 코드로 수정. -->
    <update id="modifyClassTodayDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition">
		UPDATE TODAY_CLASS
		SET DAYNO_CD = #{nextDayNoCd}
		WHERE CLS_SEQ = #{clsSeq}
		AND SORT_ORD = 1
    </update>

    <!-- 현재 차시 완료 처리 -->
    <update id="updateProgressStat" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition">
		UPDATE CLS_INCLS_PROGRESS
		SET PROGRESS_STAT = 'Y'
		WHERE CLS_SEQ = #{clsSeq}
		AND DAYNO_CD = #{dayNoCd}
    </update>
    
    <!-- 클래스의 다음 차시 등록 -->
    <insert id="insertAfterNextDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition">
	    INSERT INTO CLS_INCLS_PROGRESS (
		      CLS_SEQ
		    , DAYNO_CD
		    , SORT_ORD
		    , PROGRESS_STAT
	    )VALUES(
		      #{clsSeq}
		    , #{dayNoCd}
		    , (SELECT MAX(T1.SORT_ORD)+1 FROM CLS_INCLS_PROGRESS T1 WHERE T1.CLS_SEQ = #{clsSeq})
		    , 'N'
	    )
    </insert>
    
    
    <!-- 다음 레슨의 첫째 차시 코드 수정 -->
    <update id="modifyClassNextLessonDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition">
		UPDATE TODAY_CLASS
		SET DAYNO_CD = (
		
			SELECT
	            NLD.DAYNO_CD
	        FROM PROD_SERVICE DAYNO INNER JOIN PROD_SERVICE NLD ON DAYNO.BOOK_CD = NLD.BOOK_CD AND DAYNO.LESSON_CD = NLD.LESSON_CD AND NLD.SEQ = NLD.DAYNO_CD AND NLD.SORT_ORD = DAYNO.SORT_ORD + 1
	        WHERE DAYNO.SEQ = #{nextDayNoCd}
		    )
		WHERE CLS_SEQ = #{clsSeq}
		AND SORT_ORD = 2
		    
    <!--
		UPDATE TODAY_CLASS
		SET DAYNO_CD = (
		    SELECT
		        NLD.CONTENT_SEQ
		    FROM CONTENT_STRUC DAYNO INNER JOIN CONTENT_STRUC LES ON LES.CONTENT_SEQ = DAYNO.UPPER_CONTENT_SEQ AND LES.MBR_ID = #{mbrId}
		    INNER JOIN CONTENT_STRUC NL ON LES.UPPER_CONTENT_SEQ = NL.UPPER_CONTENT_SEQ AND NL.MBR_ID = #{mbrId} AND NL.SORT_ORD = LES.SORT_ORD + 1
		    INNER JOIN CONTENT_STRUC NLD ON NL.CONTENT_SEQ = NLD.UPPER_CONTENT_SEQ AND NLD.MBR_ID = #{mbrId} AND NLD.SORT_ORD = 1
		    WHERE DAYNO.MBR_ID = #{mbrId}
		    AND DAYNO.CONTENT_SEQ = #{nextDayNoCd}
		    )
		WHERE CLS_SEQ = #{clsSeq}
		AND SORT_ORD = 2
	  -->
	  	
    </update>

    <!-- 클래스 차시 코드 정보 삭제 -->
    <delete id="deleteClassDayNoCd" parameterType="com.visangesl.treeapi.classroom.vo.TodayClassCondition">
        DELETE FROM TODAY_CLASS WHERE CLS_SEQ = #{clsSeq}
    </delete>

    <!-- 클래스 코드로 클래스의 레슨 리스트를 조회 -->
    <select id="getClassLessonList" parameterType="com.visangesl.treeapi.classroom.vo.ClassLessonListCondition" resultType="com.visangesl.treeapi.classroom.vo.ClassLessonSimpleVo">
		SELECT 
		      CI.CLS_SEQ AS clsSeq
		    , CI.NAME AS clsNm
			, PROD.PROD_SEQ AS lessonCd
			, PROD.PROD_TITLE AS lessonNm
			, DAYNO.DAYNO_CD AS dayNoCd
		FROM CLS_INFO CI INNER JOIN PROG_ITEM PI ON CI.PROG_SEQ = PI.PROG_SEQ
		INNER JOIN PROD_SERVICE LES ON PI.ITEM_SEQ = LES.BOOK_CD AND LES.TYPE ='PT003' AND LES.SEQ = LES.LESSON_CD 
		INNER JOIN PROD_SERVICE DAYNO ON DAYNO.TYPE = 'PT004' AND DAYNO.BOOK_CD = PI.ITEM_SEQ AND LES.LESSON_CD = DAYNO.LESSON_CD AND DAYNO.SORT_ORD ='1'
		INNER JOIN PROD_INFO PROD ON LES.LESSON_CD = PROD.PROD_SEQ
		WHERE CI.CLS_SEQ = #{clsSeq}
		ORDER BY LES.BOOK_CD ASC, LES.SORT_ORD ASC, PROD.PROD_SEQ ASC
    </select>


</mapper>